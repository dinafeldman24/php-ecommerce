<?php
        require_once "application.php";
		$meta_description = $CFG->homepage_desc;
		$this_page_type = "homepage";
		include 'includes/header.php';
?>
		
<div class="intro"><div class="inner">
	<div><h3>Browse by Color</h3>
		<ul>
			<li><a href="/color.html?value=gold">Gold</a></li>
			<li><a href="/color.html?value=silver">Silver</a></li>
			<li><a href="/color.html?value=black">Black</a></li>
			<li><a href="/color.html?value=red">Red</a></li>
			<li><a href="/color.html?value=blue">Blue</a></li>
			<li><a href="/color.html?value=white">White</a></li>
			<li><a href="/color.html?value=purple">Purple</a></li>
			<li><a href="/color.html?value=brown">Brown</a></li>
			<li><a href="/color.html?value=pink">Pink</a></li>
			<li><a href="/products.html">All Colors</a></li>
		</ul>
	</div>
	<div><h3>Browse by Material</h3>
		<ul>
			<li><a href="/material.html?value=acrylic">Acrylic</a></li>
			<li><a href="/material.html?value=metal">Metal</a></li>
			<li><a href="/material.html?value=glass">Glass</a></li>
			<li><a href="/material.html?value=rattan">Rattan</a></li>
			<li><a href="/products.html">All Materials</a></li>
		</ul>
	</div>
	<div><h3>Browse by Shape</h3>
		<ul>
			<li><a href="/shape.html?value=round">Round</a></li>
			<li><a href="/shape.html?value=square">Square</a></li>
			<li><a href="/shape.html?value=octagon">Octagonal</a></li>
		</ul>
	</div>
</div></div>
<!----------------------->
<?$pop_prods = Products::getHomePageProducts();
if($pop_prods && count($pop_prods)){?>
<section class="popular-products">
	<h2>Popular Chargers</h2>
		<ul id="" class="slider">
<?foreach($pop_prods as $p){			
			$p['name'] = (str_replace('®','&reg;',$p['name']));
			$p_link = Catalog::makeProductLink_( $p );
			$p_path = Catalog::makeProductImageLink( $p['id']);
			$prices_to_print_array = Products::getPriceToDisplayForProduct($p['id']);
			
			if ($prices_to_print_array['sale_price_to_print'])
			{
				$output_price = $sale_price_to_print = $prices_to_print_array['sale_price_to_print'];
			}
			else $output_price = $prices_to_print_array['reg_price_to_print'];
			$prod_for_tracking['id']= $p['id'];
			$prod_for_tracking['name']= preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($p['name'], ENT_QUOTES));
			$prod_for_tracking['brand_name']= $p['brand_name'];
			$prod_for_tracking['price']= $output_price;
			?>
			<li class="item">
				<a title="" href="<?=$p_link?>" onClick='select_content_track(<?=json_encode($prod_for_tracking)?>,"Popular Products Homepage")'><span class="lazy_image" data-lazy="<?= $p_path?>" alt="<?=htmlentities($cs_name, ENT_QUOTES)?>"></a>
				<h2><a title="" href="<?=$p_link?>" onClick='select_content_track(<?=json_encode($prod_for_tracking)?>,"Popular Products Homepage")'><?echo StdLib::addEllipsis($p['name'],65, false, false);?></a></h2>
				<!--<strong>Box of  12</strong>-->
				<?php 
				$schema_itemtype = ($prices_to_print_array['option_prices']) ? "AggregateOffer" : "Offer" ;
				$product_for_schema[$p['id']]['schema_itemtype']=$schema_itemtype;
				$product_for_schema[$p['id']]['url']=rtrim($CFG->baseurl , "/")."#popular".$p['id'];
				 ?>
				<?php
				 if($prices_to_print_array['option_prices']){
					?>
					<span class="price">$<?=number_format($prices_to_print_array['option_prices']['min'], 2)?> - <?=number_format($prices_to_print_array['option_prices']['max'], 2)?></span>
					<?php $product_for_schema[$p['id']]['lowPrice']=number_format($prices_to_print_array['option_prices']['min'], 2);
						  $product_for_schema[$p['id']]['highPrice']=number_format($prices_to_print_array['option_prices']['max'], 2);
					 } else {
					 ?>
					 <span class="price">$<?=number_format($output_price, 2)?></span>
					 <?}
					 $product_for_schema[$p['id']]['price']=number_format($output_price, 2);
				 ?>
				<a href="<?=$p_link?>" onClick='select_content_track(<?=json_encode($prod_for_tracking)?>,"Popular Products Homepage")' class="button-brown more-info">More Info</a>
			</li>
<?}?>
	</ul>
</section>
<?}?>
<?include 'includes/footer.php';?>