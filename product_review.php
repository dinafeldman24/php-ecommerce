<?php 
include_once ('application.php');

// THIS SECTION IS SINCE WE CHANGED TO YOTPO REVIEWS - OLD REVIEW FORM COMES AFTER - TE 12/15/2014

$product = Products::get1 ( $_GET [product_id] );
$large_image_link		= Catalog::makeProductImageLink( $product['id'], false );
$plink					= Catalog::makeProductLink_($product);


// FROM HERE ON IS OLD REVIEW FORM --- TE 12/15/2014
global $status_msg;
global $cust;
	
	if ($_SESSION ['cust_acc_id'] > 0){ 
		$cust = Customers::get1 ( $_SESSION ['cust_acc_id'] );
	}	
	else {
	$cid=Customers::getIdByEmail($_REQUEST ['info']['email']);
	if ($cid) $cust = Customers::get1 ($cid);
	}

	
if (MyAccount::logged_in ()) {
  $plink = $CFG->base_url .'/account.php#review-section'; 
}
else {
   $plink = Catalog::makeProductLink_ ( $product );
}


	switch ($_REQUEST ['action']) {
		case "edit_review" : 
			
			// ///////////Don't let robots fill this from!///////////////
			$robotest = $_POST ['robotest'];
			if ($robotest) {
				include ($CFG->redesign_dirroot . '/includes/header.php');
				$status_msg = "You are a gutless robot.";
				header ( "Location: $plink" );
				exit ( 0 );
				break;
			}    // ///////////End of robot check///////////////
			
			else {   
				$status_msg = editReview ( $_POST ['review'], $_POST ['info'], $_POST ['productid'], $_POST ['terms1'], $_POST ['terms2'], $_POST ['terms3'], $_POST ['product_page'] );
				$pendingterms = $status_msg ['pending_terms'];
				unset ( $status_msg ['pending_terms'] );
				if ($status_msg) {
					include ($CFG->redesign_dirroot . '/includes/header.php');
					
				/*	if ($status_msg ['img_url']) {
						echo "<div class='errorBox'>There's a problem with the file you uploaded: " . $status_msg ['img_url'] . "</div>";
						if (count ( $status_msg ) > 1) {
							echo "<div class='errorBox'>Some fields are missing or invalid.</div>";
						}
					} else {
						echo "<div class='errorBox'>Some fields are missing or invalid.</div>";
						if ($status_msg ['has_image'] == 1)
							echo "<br/><div class='errorBox'>Please re-upload your image.</div>";
					}*/
		      			
					showform ( $status_msg, $_POST['review']);
				} 				// display error message and form
				
				else {
				 	$review=Product_Reviews::get1_all_review_info($_POST ['review']);
					
					if (($review ['approved'] == 'P') || (! $_POST ['review'])) {
					  $message="Thank you! Your review has been submitted and is pending moderation.";
						if ($cust['id']> 0 && ($review ['approved'] == 'P')) $message .=" Once your review has been approved, your reward points will become available. "; 
						?>
						<script language="JavaScript" type="text/javascript">
							alert('<?=$message?>');
						</script>
						<?
					
					} else {
						$pending = Pending_Review::get ( '', $review ['id'] );
						if ($pending) {
							?>
							<script language="JavaScript" type="text/javascript">
								alert('Thank you for editing your review. Your review will be updated in 3-5 days, pending approval.');
							</script>
							<?
						} else {
							if ($pendingterms == 1) {
								?>
								<script language="JavaScript" type="text/javascript">
									alert('Thank you for editing your review! Some of your changes are pending approval.');
								</script>
							<?} else {?>
								<script language="JavaScript" type="text/javascript">
									alert('Thank you for your review!<?=$pendingterms?>');
								</script>
							<? }?>	
						<? }?>
					<? } ?>
					<script language="JavaScript" type="text/javascript">
						window.location = '<?=$plink?>';
					</script>
				<?
				}
			}
			break;
		
		case "delete" :
			// ///////////Don't let robots fill this from!///////////////
			$robotest = $_POST ['robotest'];
			if ($robotest) {
				include ($CFG->redesign_dirroot . '/includes/header.php');
				$status_msg = "You are a gutless robot.";
				header ( "Location: $plink" );
				exit ( 0 );
				break;
			} 			// ///////////End of robot check///////////////
			
			else {
				$status_msg = deleteReview ( $_POST ['review'], $_POST ['info'], $_POST ['productid'], $_POST ['terms1'], $_POST ['terms2'], $_POST ['terms3'], $_POST ['product_page'] );
				
				if ($status_msg) {
					include ($CFG->redesign_dirroot . '/includes/header.php');
					showform ( $status_msg );
				} 				// display error message and form
				
				else {
					header ( "Location: $plink" );
					exit ( 0 );
				} // finished editing return to product page
			}
			break;
		case "fastdelete":
	        	    $status_msg=fastdelete($_REQUEST['review_id'],$product);
					  
						 if ($status_msg) { include($CFG->dirroot . '/includes/header.php');
                                showform($status_msg);} //display error message and form
						 
						 else  {
						       header("Location: $CFG->base_url/account.php#review_edit_frm");
                   exit(0);} //finished editing return to product page			 

			   break;
			
		default :
			include ($CFG->redesign_dirroot . '/includes/header.php');
			showform ("", $_REQUEST['review_id']);
			break;
	}?>
	</div>
</div>	

<?	include ($CFG->redesign_dirroot . '/includes/footer.php');
/*} 

else {
	/* if someone tried accessing this w/o being in the account, needs to login first 
	header ( sprintf ( "Location: %s%s?redirect=%s", $CFG->sslurl, "account.php", urlencode ( $_SERVER ['REQUEST_URI'] ) ) );
	exit ( 0 );
}*/

function showform($status_msg, $review_id) {
	global $CFG;
	global $cart;
	global $info;
	global $product;
	global $cust;
	
	if ($cust['id']> 0 && !$review_id) 
	    $review = Product_Reviews::get_review_by_product_and_customer ( $_REQUEST ['product_id'], $cust['id'] );
	else
	$review=Product_Reviews::get1_all_review_info($review_id);
	
	if ($review) {
		$pendingreview = Pending_Review::get ( '', $review ['id'] );
	}
	$info = $_REQUEST ['info'];
	if ($pendingreview) {
		$review ['title'] = $pendingreview [0] ['title'];
		$review ['comment'] = $pendingreview [0] ['comment'];
	}
	
	?>
	

<div class="wrapper white share-opinion main-section">
	<div id="content">
		<? echo $status_msg['extracomment'];?>
		 <form method="post" id="product_review_frm"
			action="<?=$_SERVER['HTTPS'] ? $CFG->sslurl . "product_review.php?product_id=".$product['id'] : $CFG->baseurl . "product_review.php?product_id=".$product['id']?>"
			enctype="multipart/form-data"
			class="form share-opinion">
	
			<input type="hidden" name="productid" value="<?=$product['id']?>" /> 
			<input type="hidden" name="product_page" value="<?=Catalog::makeProductLink_($product)?>" /> 
			<input type="hidden" name="info[rating]" id="star_rating" value="<?=$review['rating']?>" /> 
			<input type="hidden" name="review" value="<?=$review['id'] ?>" /> 
			<input type="hidden" name="action" value="edit_review" />
			
			<div class="content-title">
				<h1>Share your opinion</h1>
			</div>
			
			 <? if (($review[approved])&&(!$status_msg)){ ?>
			 <span class="review"> Your review is 
			 <?php
					
			if ($review [approved] == 'Y')
						echo 'approved';
					else if ($review [approved] == 'N')
						echo 'not approved';
					else if ($review [approved] == 'P')
						echo 'pending approval';
					?>
			 <br />
			 <? 	if ($pendingreview){echo "This latest version of your review is pending approval<br/>";} ?>
			 <br />
			</span> 
		 <?	
		}
			if ($info) {
				$review ['title'] = $info ['title'];
				$review ['comment'] = $info ['comment'];
				$review ['recommend']= $info ['recommend'];
				$review ['name']= $info ['name'];
				$review ['email']= $info ['email'];
				$review ['rating']= ($info ['rating']) ? $info ['rating'] : $review ['rating'];


			//	$review [img_caption] = $info [img_caption];
			//	$review ['video_caption'] = $info ['video_caption'];
				$review ['video_url'] = $info ['video_url'];
				if (isset($info [notify_comment]) && $info [notify_comment]=='Y')
					$review [notify_comment] = 'Y';
				else
					$review [notify_comment] = 'N';
			}
			
			?>
     <?/*if (MyAccount::logged_in ()) {?>
		   <p>Earn 200 points ($2.00) for leaving a review! Earn an additional
					100 points ($1.00) for uploading an image with your review and earn
					an additional 800 points ($8.00) for uploading a video with your
					review. Subject to approval.</p>
		<? } else { ?>
		   <p>Login and earn points for leaving this review. <a href="#" onclick="TINY.box.show({url:'popuplogin.php',post:'link=<?=$CFG->baseurl?>product_review.php?product_id=<?=$product['id']?>',fixed:false});return false;">Click here to login</a></p>
		<? }*/ ?>			
				<div class="product-section">
					<div class="product-image">
						<div class="box">
							<a href="<?=Catalog::makeProductLink_($product)?>">
								<img src="<?=Catalog::makeProductImageLink( $product['id'], false )?>" alt="<?=$product['name']?>" border="0" />
							</a>
						</div>
					</div>
		<? 
		// Cleaning description//
			$product ['description'] = str_replace ( "<ul>", "", $product ['description'] );
			$product ['description'] = str_replace ( "</ul>", "", $product ['description'] );
			$product ['description'] = str_replace ( "<li>", " ", $product ['description'] );
			$product ['description'] = str_replace ( "</li>", ",", $product ['description'] );
			$product ['description'] = trim ( preg_replace ( '/\s\s+/', ' ', $product ['description'] ) );
			if (substr ( $product ['description'], - 1 ) == ',') {
				$product ['description'] = substr ( $product ['description'], 0, - 1 );
			}
			$product ['description'] = StdLib::strip_html_tags ( $product ['description'] );
			?>
		
			<div class="product-description">
				<div class="box">
					<h4>
						<a href="<?=Catalog::makeProductLink_($product)?>"><span class="pr_name"><?=$product['name']?></span></a>
					</h4>
					<p><?php echo StdLib::addEllipsis($product['description'],480, false, false);?></p>
				</div>
			</div>
		</div>
		<div class="required">
			<span><sup class="required-star">*</sup> = Required</span>
		</div>
		
		<div class="row">
			<strong class="row-name">Rating <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<p>Click to rate:</p>
					<ul class="rating <?=$status_msg['rating']?'review_error_field':''?>">
						<li class="<?= (($review[rating]==1) ? 'setted' : '')?>">
							<a href="#1" onclick="update_rate_value(1);return false;" title="POOR" class="one-star">1</a>
						</li> 
						<li class="<?= (($review[rating]==2) ? 'setted' : '')?>">
							<a href="#2"  update_rate_value(2);return false; title="FAIR" class="two-stars">2</a>
						</li>
						<li class="<?= (($review[rating]==3) ? 'setted' : '')?>">
							<a href="#3" update_rate_value(3);return false; title="AVERAGE" class="three-stars">3</a>
						</li>
						<li class="<?= (($review[rating]==4) ? 'setted' : '')?>">
							<a href="#4" update_rate_value(4);return false; title="GOOD" class="four-stars">4</a>
						</li>
						<li class="<?= (($review[rating]==5) ? 'setted' : '')?>">
							<a href="#5" update_rate_value(5);return false; title="EXCELLENT" class="five-stars">5</a>
						</li>
					</ul>
					<div class="rating_legend"></div>
					<br><span class="error"><?=$status_msg['rating']?></span>
				</div>
			</div>
		</div>
    <? if(!$_SESSION ['cust_acc_id']) { ?>		
    <div class="row" id="name">
			<strong class="row-name">Your Name <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<input class="text <?=$status_msg['name']?'review_error_field':''?>" type="text" placeholder="Please enter your name..." name="info[name]" id="name_value"
							type="text" maxlength="42" size="42"
							value="<? echo stripslashes($review['name']);?>">
					<span class="error"><?=$status_msg['name']?></span>
				</div>
			</div>
		</div>

		<div class="row" id="email">
			<strong class="row-name">Your Email <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<input class="text <?=$status_msg['email']?'review_error_field':''?>" type="text" placeholder="Please enter your email..." name="info[email]" id="email_value"
							type="text" maxlength="42" size="42"
							value="<? echo stripslashes($review['email']);?>">
					<span class="error"><?=$status_msg['email']?></span>
				</div>
			</div>
		</div>
		<? } ?>
		<div class="row" id="title">
			<strong class="row-name">Review Title <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<input class="text <?=$status_msg['title']?'review_error_field':''?>" type="text" placeholder="Please enter your title..." name="info[title]" id="title_value"
							type="text" maxlength="42" size="42"
							value="<? echo stripslashes($review['title']);?>">
					<span class="error"><?=$status_msg['title']?></span>
				</div>
			</div>
		</div>
		<div class="row" id="comment">
			<?
			$review ['comment'] = stripslashes ( $review ['comment'] );
			$review ['comment'] = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $review ['comment'] );
			$review ['comment'] = str_replace ( "<br/><br/>", "<br/>", $review ['comment'] );
			$review ['comment'] = str_replace ( "<br/>", "&#10;", $review ['comment'] );
			?>
			<strong class="row-name">Review Comments: <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<textarea cols="30" rows="10" id="comments" class="comments <?=$status_msg['title']?'review_error_field':''?>" name="info[comment]" placeholder="Please enter your comment..."><?echo ($review['comment']);?></textarea>
					<span class="error"><?=$status_msg['comment']?></span>
				</div>
			</div>
		</div>
<!--		<div class="row">
			<strong class="row-name">Recommend <sup class="required-star">*</sup></strong>
			<div class="row-content">
				<div class="box">
					<p>I would recomend this product</p>
					<ul class="controls-list">
						<li>
							<input type="radio" name="info[recommend]" value="Y" <? if ($review[recommend]=='Y'){echo "checked";}?>>
							<label for="rad1">Yes</label>
						</li>
						<li>
							<label>
								<input type="radio" name="info[recommend]" value="N" <? if ($review[recommend]=='N'){echo "checked";}?>>
								<span>No</span>
							</label>
						</li>
					</ul>
				</div>
			</div>
		</div> 
		
		<div class="row">
			<strong class="row-name">Pros</strong>
			<div class="row-content">
				<div class="box">
					<div class="row-area">
					
						<em>Please add a pro or select one from the box on the right, if relevant</em>
						<div class="field-area">
							<input class="text small" type="text" id="proterm" type="text"/>
							<button class="orange" onclick="useraddterm(<?=$product['id']?>,proterm,1,<?=($cust['id'])? $cust['id']:'0';?>);return false;">Add</button>
						</div>
						<small>Examples: Easy to Use, Sturdy, as described...</small>
					</div>
					<div class="row-area">
						<em>Shift + Click for multiple selections</em>
						<?
						$allterms = Review_Terms::getProductTerms ( $product ['id'], 1 );
						if ($review [id])
							$used1 = Review_Terms::getReviewsUsed ( $review [id], 1 );
						
						if ($_POST ['terms1']) {
							unset ( $used1 );
							for($i = 0; $i < count ( $_POST ['terms1'] ); $i ++)
								$used1 [$i] = Review_Terms::get1 ( $_POST ['terms1'] [$i] );
						}
						?>
						<select class="choice" multiple="multiple" title="" name="terms1[]" id="probox">
							<?
							foreach ( $allterms as $term ) {
								if (($term [approved] == 'Y') || (($term ['customer_id'] == $cust['id']) && ($term [approved] != 'N'))) {
									echo $term ['customer_id'];
									echo "<option value=";
									echo $term ['id'] . ' ';
									
									if ($used1) {
										foreach ( $used1 as $terreview ) {
											if ($terreview ['id'] == $term ['id']) {
												echo 'SELECTED ';
											}
										}
									}
									echo ">";
									echo stripslashes ( $term ['description'] );
									echo "</option>";
								} // end if
							} // end foreach  
							?>
						</select>
						<?
						if ($used1) {
							?>
							Your selections:<br />
							<?
							foreach ( $used1 as $term ) {
								if ($term [approved] != 'N') {
									echo stripslashes ( $term [description] );
									echo "<br/>";
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<strong class="row-name">Cons</strong>
			<div class="row-content">
				<div class="box">
					<div class="row-area">
						<em>Please add a con or select one from the box on the right, if relevant </em>
						<div class="field-area">
							<input class="text small" type="text" id="con" maxlength="42" />
							<button class="orange" onclick="useraddterm(<?=$product['id']?>,con,2,<?=($cust['id'])? $cust['id']:'0';?>);return false;">Add</button>
						</div>
						<small>Examples: Hard to use, flimsy, too bright...</small>
					</div>
					<div class="row-area">
						<em>Shift + Click for multiple selections</em>
						<?
						$allterms = Review_Terms::getProductTerms ( $product ['id'], 2 );
						if ($review [id])
							$used2 = Review_Terms::getReviewsUsed ( $review [id], 2 );
						if ($_POST ['terms2']) {
							unset ( $used2 );
							for($i = 0; $i < count ( $_POST ['terms2'] ); $i ++)
								$used2 [$i] = Review_Terms::get1 ( $_POST ['terms2'] [$i] );
						}
						?>
						<select class="choice" multiple="multiple" title="" name="terms2[]" id="conbox">
							<?
							foreach ( $allterms as $term ) {
								if (($term [approved] == 'Y') || (($term ['customer_id'] == $cust['id']) && ($term [approved] != 'N'))) {
									echo $term ['customer_id'];
									echo "<option value=";
									echo $term ['id'] . ' ';
									
									if ($used2) {
										foreach ( $used2 as $terreview ) {
											if ($terreview ['id'] == $term ['id']) {
												echo 'SELECTED ';
											}
										}
									}
									echo ">";
									echo stripslashes ( $term ['description'] );
									echo "</option>";
								} // end if
							} // end foreach
							?>
						</select>
						<?
						if ($used2) {
							?>
							Your selections:<br />
							<?
							foreach ( $used2 as $term ) {
								if ($term [approved] != 'N') {
									echo stripslashes ( $term [description] );
									echo "<br/>";
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<strong class="row-name">Best Uses</strong>
			<div class="row-content">
				<div class="box">
					<div class="row-area">
						<em>Please add a best use or select one from the box on the right, if relevant</em>
						<div class="field-area">
							<input class="text small" type="text" id="best" maxlength="42" />
							<button class="orange" onclick="useraddterm(<?=$product['id']?>,best,3,<?=($cust['id'])? $cust['id']:'0';?>);return false;">Add</button>
						</div>
<small>Examples: dining, cook chicken, for rice...</small>
					</div>
					<div class="row-area">
						<em>Shift + Click for multiple selections</em>
						<?
						$allterms = Review_Terms::getProductTerms ( $product ['id'], 3 );
						if ($review [id])
							$used3 = Review_Terms::getReviewsUsed ( $review [id], 3 );
						
						if ($_POST ['terms3']) {
							unset ( $used3 );
							for($i = 0; $i < count ( $_POST ['terms3'] ); $i ++)
								$used3 [$i] = Review_Terms::get1 ( $_POST ['terms3'] [$i] );
						}
						?>
						<select class="choice" multiple="multiple" title="" name="terms3[]" id="bestbox">
							<?
							foreach ( $allterms as $term ) {
								if (($term [approved] == 'Y') || (($term ['customer_id'] == $cust['id']) && ($term [approved] != 'N'))) {
									echo $term ['customer_id'];
									echo "<option value=";
									echo $term ['id'] . ' ';
									
									if ($used3) {
										foreach ( $used3 as $terreview ) {
											if ($terreview ['id'] == $term ['id']) {
												echo 'SELECTED ';
											}
										}
									}
									echo ">";
									echo stripslashes ( $term ['description'] );
									echo "</option>";
								} // end if
							} // end foreach
							?>
						</select>
						<?
						if ($used3) {
							?>
							Your selections:<br />
							<?
							foreach ( $used3 as $term ) {
								if ($term [approved] != 'N') {
									echo stripslashes ( $term [description] );
									echo "<br/>";
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<strong class="row-name">Add an image</strong>
			<div class="row-content">
				<div class="box">
					<div class="row-area">
						<input type="file" value="<?=$review['img_url'];?>" name="img_url" id="file"/>
						<input type="hidden" name="info[img_approved]" value="<?= ($review['img_approved']) ? $review['img_approved'] : 'P' ?>">
						<em>We accept gif, jpg, or png files under 2MB.</em>
					</div>
				<!-- 	<div class="row-area">
						<p>Image Caption:</p>
						<input class="text small" type="text"
								name="info[img_caption]"
								id="img_caption" type="text" maxlength="30" 
								value="<? if ($review['img_url']){ echo stripslashes($review['img_caption']);}?>">
						<span class="error"><?=$status_msg['img_caption']?></span>
					</div>-->
<!--					<div id="imageremoved"></div>
				</div>
				<?php 
				if ($review['img_url']){?>
					<div class="box" id="reviewimag">
						<div class="row-area">
							<p>Your Current Image:</p>
							<img src="upload/<?=$review['img_url'];?>" height="90">
							  <? if ($review['img_approved']=='P') echo "<span>Your image is pending approval</span>"?>
							  <? if ($review['img_approved']=='N') echo "<span>Your image is not approved</span>"?>
								<?  
								if (($review['img_approved']!='Y')||(!$review['img_approved'])){?>
								<input type="button" onclick="removeReviewImage('<?=$review['img_url']?>','<?=$review['id']?>');return false;"
									id="removeimage" value="Remove Image" />
								<?}?>
			          	</div>
					</div>
			 	<?php 
				}?>
			</div>
		</div>

		<div class="row">
			<strong class="row-name">Share Your YouTube Video</strong>
			<div class="row-content">
				<div class="box">
					<? $video = getYoutubeIdFromUrl($review['video_url']); ?>
					<div class="row-area">
						<label class="label left" for="video">To share your YouTube video with us, <br /> paste your youtube link here:</label>
						<input class="text small" type="text" name="info[video_url]" maxlength="70" size="20" id="youtubevideo" 
								<?=($video)?'value="http://www.youtube.com/embed/'.$video.'?rel=0"':''?>/> 
						<input type="hidden" name="info[video_approved]" value="<?= ($review['video_approved']) ? $review['video_approved'] : 'P' ?>">
					</div>
			<!-- 		<div class="row-area">
						<p>Video Caption:</p> 
						<input class="text small" name="info[video_caption]" id="video_caption" type="text" maxlength="30" size="20"
							value="<? if ($review['video_url']){ echo stripslashes($review['video_caption']);}?>">
					</div>-->
<!--				</div>
		   		<?php if (($review['video_url'])&&(!$status_msg['video_url'])){?>
					<div class="box" id="reviewvideo">
						<div class="row-area">
							<p>Your Current Video:</p>
							<iframe width="150" height="100"
								src="http://www.youtube.com/embed/<?=$video?>?rel=0"
								frameborder="0" allowfullscreen></iframe>
				  			<? if ($review['video_approved']=='P') echo "<span>Your video is pending approval</span>"?>
							<? if ($review['video_approved']=='N') echo "<span>>Your video is not approved</span>"?>
							<? if (($review['video_approved']=='P')){ ?>
						 		<input type="button"
									onclick="youtubevideo.value='';reviewvideo.style.display='none';jQuery('.row').sameHeight({elements: '.row-name',flexible: true});"
									value="Remove Video" />
			       			<? } ?>
          				</div>
					</div>
				<?php }?>
			</div>
		</div>
		<div class="row" id="notify">
			<strong class="row-name"></strong>
			<div class="row-content">
				<div class="box">
					<input type="checkbox" name="info[notify_comment]" value="Y"
							<? if ($review[notify_comment]=='Y'){echo "checked";}?>>
					<span>Email me when a comment is added to this review.</span>
				</div>
			</div>
		</div>
-->
		<!-- The following field is for robots only, invisible to humans: -->
		<p class="robotic" id="pot">
			<label>If you're human leave this blank:</label> <input
				name="robotest" type="text" id="robotest" class="robotest" />
		</p>
		<div id="newterm"></div>
		
		<div class="row buttons-holder">
			<div class="box">
				<?
				if ($review[id]) {
					?>
            <input class="button-brown" value="Update Review" name="submit_review" type="button"
							onclick="$('product_review_frm').action.value='edit_review';$('product_review_frm').submit();" />
					<input class="button-brown" value="Delete Review" name="delete_review" type="button"
							onclick="if (confirm('Are you sure you want to delete this review?')){$('product_review_frm').action.value='delete'; $('product_review_frm').submit();}" />
					<? 
				} else {
					?>
                    <input class="button-brown" value="Submit Review" name="submit_review" type="button"
							onclick="$('product_review_frm').action.value='edit_review';$('product_review_frm').submit();" />
					<?
				}
				?>	
				<button class="button-brown" name="cancel_button" onclick="window.location.assign('<?=Catalog::makeProductLink_($product)?>');return false;">Cancel</button>
			</div>
		</div>
	</form>
</div>
</div>
 
<?php
}
function editReview($reviewid, $info, $productid, $terms1, $terms2, $terms3, $productpage) {
	global $CFG;
	global $cust;
	
	
	// Validate reqired fields
	foreach ( $info as $key => $value ) {
		if ((! $value) && ($key != 'video_url') && ($key != 'img_caption') && ($key != 'video_caption') && ($key != 'img_url')) {
			$status_msg [$key] = 'This is a required field';
			$error = 1;
		}
	}
	if (!$info[rating]) {
		$status_msg['rating'] = 'Please rate this product';
		$error = 1;
	}
	/*if (!$info['recommend']) {
		$status_msg['recommend'] = 'Please rate this product';
		$error = 1;
	}*/
  if (!$_SESSION['cust_acc_id'])	
	   if (!filter_var($info['email'], FILTER_VALIDATE_EMAIL)) {

			$status_msg['email'] = 'Invalid email';
			$error = 1;
     }
		
	if ($_FILES ['img_url'] ['name']) {
		$allowedExts = array (
				"jpg",
				"jpeg",
				"gif",
				"png",
				"bmp",
				"JPG",
				"JPEG",
				"GIF",
				"PNG",
				"BMP" 
		);
		$extension = end ( explode ( ".", $_FILES ["img_url"] ["name"] ) );
		if ((($_FILES ["img_url"] ["type"] == "image/gif") || ($_FILES ["img_url"] ["type"] == "image/jpeg") || ($_FILES ["img_url"] ["type"] == "image/png") || ($_FILES ["img_url"] ["type"] == "image/pjpeg")) && ($_FILES ["img_url"] ["size"] / 1024 < 2000) && in_array ( $extension, $allowedExts )) {
			$status_msg ['has_image'] = 1;
			if ($_FILES ["img_url"] ["error"] > 0) {
				$status_msg ['img_url'] = "There is a problem with your uploaded file:" . $_FILES ["img_url"] ["error"] . "<br>";
				$error = 1;
			} else {
				if (file_exists ( "upload/" . $_FILES ["img_url"] ["name"] )) {
					$status_msg ['img_url'] = $_FILES ["img_url"] ["name"] . " already exists. Choose a file with a different name.<br/>";
					$error = 1;
				}
			}
			$newname = time ();
			$newname .= "." . $extension;
			$info ['img_url'] = $newname;
			//$info ['img_approved'] = 'P';
			move_uploaded_file ( $_FILES ["img_url"] ["tmp_name"], "upload/" . $info ['img_url'] );
		} 

		else {
			$status_msg ['img_url'] = "Invalid file <br>";
			if ($_FILES ["img_url"] ["size"] / 1024 > 2000)
				$status_msg ['img_url'] = "File is too large, please upload a file less then 2MB";
			$error = 1;
		}
	}
	
	if ($info ['video_url']) {
		$info ['video_url'] = getYoutubeIdFromUrl ( $info ['video_url'] );
		//$info ['video_approved'] = 'P';
		$video = isYoutubeVideo ( $info ['video_url'] );
		
		if (! $video) {
			$status_msg ['video_url'] = "Your video is invalid";
			$error = 1;
			unset ( $info ['video_url'] );
		}
	}
	
	if (isset($info ['notify_comment']) && $info ['notify_comment']=='Y') {
		$info [notify_comment] = 'Y';
	} else {
		$info [notify_comment] = 'N';
	}
	
// 	print_ar($_POST);
// 	print_ar($status_msg);
	
	if ($error == 1)
		return ($status_msg);
	unset ( $status_msg ['has_image'] );
	$info ['comment'] = preg_replace ( '#(\r\n)#', '\\n', $info ['comment'] );
	if ($_FILES ["img_url"] ["name"]) {
		$newname = time ();
		$newname .= "." . $extension;
		$info ['img_url'] = $newname;
		//$info ['img_approved'] = 'P';
		move_uploaded_file ( $_FILES ["img_url"] ["tmp_name"], "upload/" . $info ['img_url'] );
		$_POST ['img_url'] = $info ['img_url'];
	}
	
	if ($info ['video_url']) {
		$info ['video_url'] = getYoutubeIdFromUrl ( $info ['video_url'] );
		//$info ['video_approved'] = 'P';
		$video = isYoutubeVideo ( $info ['video_url'] );
		
		if (! $video) {
			unset ( $info ['video_url'] );
		}
	}
	$reviewbythisemail=Product_Reviews::get_review_by_email($productid ,$info['email']);
	if ($reviewbythisemail) $reviewid=$reviewbythisemail[0]['id'];

	$oldreview = Product_Reviews::get1 ( $reviewid );
	// if (($oldreview['title']!=$info['title'])||($oldreview['comment']!=$info['comment']))
	// $info['approved']='P';
  
	if ($_SESSION['cust_acc_id']) { $info['email']=$cust['email']; $info['name']=$cust['first_name']." " .$cust['last_name'];}	

	$item = Products::get1 ( $productid );
	$itemName = $item ['name'];
	$itemUrl = $item ['url_name'];
	$info['customer_id']=$cust['id'];
	$custFullName = $info['name'];
	$custEmail = $info['email'];
	$reviewTitle = $info ['title'];
	$reviewComment = $info ['comment'];
		if ($info ['img_url']) {
			$reviewImage = "<img src='" . $CFG->baseurl . "upload/" . $info ['img_url'] . "' width='200'>";
			$imageCaption = $info ['img_caption'];
		}
		if ($info ['video_url']) {
			$reviewVideo = $info ['video_url'];
			$videoCaption = $info ['video_caption'];
		}

	if (! $reviewid) 	// This is a new review
	{
		$info ['approved'] = 'P';
		$resp = Product_Reviews::insert ( $info, $productid, $cust['id'] );
		$reviewid = $resp;
		
		// give pending reward points - will be available when the review is approved
		if ($CFG->review_reward_points > 0 && $cust) {
			
			$reward_info = Array (
					'customer_id' => $cust['id'],
					'points_qty' => $CFG->review_reward_points,
					'action_type' => 'earned',
					'action' => 'review',
					'date' => date ( 'Y-m-d H:i:s' ),
					'review_id' => $reviewid,
					'active' => 'N' 
			);
			
			Rewards::insertReward ( $reward_info );
		}
		
		// Send an email notification that a review has been submitted
		
		$msg = "A review for ";
		$msg .= "<a href='$CFG->baseurl$itemUrl.html'>$itemName</a> ";
		$msg .= "has just been submitted by ";
		$msg .= "<a href='mailto:$custEmail'>$custFullName</a>.<br />";
		
		$to_email = ($CFG->in_testing) ? $CFG->testing_email : "estee@tigerchef.com";
		
		$vars ['body'] = $msg;
		$E = TigerEmail::sendOne ( $CFG->company_name, $to_email, "blank", $vars, "Product Review Submitted for " . $itemName, true, $CFG->company_name, $CFG->company_email );
	} else {
		
		if ($oldreview ['approved'] != 'P') { // if review is pending don't create pending version
			$pendingreview = Pending_Review::get ( '', $reviewid ); // Check if we already have an edited unapproved version
			
			$cmp1 = stripslashes ( $oldreview ['comment'] );
			$cmp1 = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $cmp1 );
			// $cmp1=str_replace("<br/><br/>","<br/>",$cmp1);
			
			$cmp2 = $info ['comment'];
			$cmp2 = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $cmp2 );
			// $cmp2=str_replace("<br/><br/>","<br/>",$cmp2);
			
			// Create a pending review
			if (! $pendingreview) {
				$oldreview = Product_Reviews::get1 ( $reviewid );
				
				if (($oldreview ['title'] != $info ['title']) || (strcmp ( $cmp1, $cmp2 ) != 0)) {
					$newinfo = array ();
					$newinfo ['review_id'] = $reviewid;
					$newinfo ['title'] = $info ['title'];
					unset ( $info ['title'] );
					$newinfo ['comment'] = $info ['comment'];
					unset ( $info ['comment'] );
					Pending_Review::insert ( $newinfo );
				}
			} 			

			// update a pending review
			else if ($pendingreview) {
				
				if (($pendingreview ['title'] != $info ['title']) || ($pendingreview ['content'] != $info ['content'])) {
					$newinfo ['review_id'] = $reviewid;
					$newinfo ['title'] = $info ['title'];
					unset ( $info ['title'] );
					$newinfo ['comment'] = $info ['comment'];
					unset ( $info ['comment'] );
					Pending_Review::update ( $pendingreview [0] [id], $newinfo );
				}
			}
		}
		Product_Reviews::update ( $reviewid, $info );
		
		// Send an email notification to admin that a review is edited
		
		$msg = "A review for ";
		$msg .= "<a href='$CFG->baseurl$itemUrl.html'>$itemName</a> ";
		$msg .= "was edited by ";
		$msg .= "<a href='mailto:$custEmail'>$custFullName</a>.<br />";
		$msg .= "Here is a copy of the edited review:<br />";
		if ($info[rating]) {
			$msg .= "$custFullName rated this product as: $info[rating] out of 5 <br />";
		}
		$msg .= "Title: $reviewTitle <br/>";
		$msg .= "Comment: $reviewComment <br/>";
		
		$to_email = ($CFG->in_testing) ? $CFG->testing_email : "estee@tigerchef.com";
		
		$vars ['body'] = $msg;
		$E = TigerEmail::sendOne ( $CFG->company_name, $to_email, "blank", $vars, "Product Review was edited for " . $itemName, true, $CFG->company_name, $CFG->company_email );
	}
	
	$allterms = array ();
	foreach ( $terms1 as $term ) {
		$term1 = Review_Terms::get ( $term );
		if ($term1 [0] [approved] != 'Y')
			$status_msg ['pending_terms'] = 1;
		array_push ( $allterms, $term );
	}
	
	foreach ( $terms2 as $term ) {
		$term1 = Review_Terms::get ( $term );
		array_push ( $allterms, $term );
		if ($term1 [0] [approved] != 'Y')
			$status_msg ['pending_terms'] = 1;
	}
	
	foreach ( $terms3 as $term ) {
		$term1 = Review_Terms::get ( $term );
		if ($term1 [0] [approved] != 'Y')
			$status_msg ['pending_terms'] = 1;
		array_push ( $allterms, $term );
	}
	
	Review_Terms::associate2Review ( $allterms, $reviewid, 'Y' );
	
	if ($info ['img_url']) {
		/* check if user already got points for adding an image */
		$reward = Rewards::getRewards ( '', '', '', '', '', '', '', '', '', 'image', '', $reviewid );
		if (! $reward) {
			/* give pending reward points for adding an image */
			if ($CFG->review_reward_points > 0 && $cust) {
				
				$reward_info = Array (
						'customer_id' => $cust['id'],
						'points_qty' => $CFG->review_reward_image_points,
						'action_type' => 'earned',
						'action' => 'image',
						'date' => date ( 'Y-m-d H:i:s' ),
						'review_id' => $reviewid,
						'active' => 'N' 
				);
				
				Rewards::insertReward ( $reward_info );
			}
		}
	}
	
	if ($info ['video_url']) {
		/* check if user already got points for adding an video */
		$reward = Rewards::getRewards ( '', '', '', '', '', '', '', '', '', 'video', '', $reviewid );
		if (! $reward) {
			/* give pending reward points for adding an image */
			if ($CFG->review_reward_points > 0 && $cust) {
				
				$reward_info = Array (
						'customer_id' => $cust['id'],
						'points_qty' => $CFG->review_reward_video_points,
						'action_type' => 'earned',
						'action' => 'video',
						'date' => date ( 'Y-m-d H:i:s' ),
						'review_id' => $reviewid,
						'active' => 'N' 
				);
				
				Rewards::insertReward ( $reward_info );
			}
		}
	}
	
	$review = Product_Reviews::get1 ( $reviewid );
	
	return ($status_msg);
}
function deleteReview($reviewid, $info, $productid, $terms1, $terms2, $terms3, $productpage) {
	if (Product_Reviews::delete ( $reviewid ) === false) {
		include ($CFG->redesign_dirroot . '/includes/header.php');
		$status_msg ['extracomment'] = "Error deleting review.";
		break;
	} else 	// succeeded in deleting the review; delteting now extra info
	{
		/* cancel rewards */
		Rewards::cancelRewards ( '', 'Y', '', $reviewid );
		
		/* delete terms relationship to review */
		unset ( $allterms );
		Review_Terms::associate2Review ( $allterms, $reviewid, 'Y' );
	}
}
function fastdelete($review_id, $product)
	{ 
          $review=Product_Reviews::get1($review_id);
 		 	    if (Product_Reviews::delete($review[id]) === false)
                    {
                        include($CFG->dirroot . '/includes/header.php');
                        $status_msg['extracomment'] = "Error deleting review.";
                        break;
                    }
                    else // succeeded in deleting the review; delteting now extra info
                    {
			              /* cancel rewards */
			              Rewards::cancelRewards('','Y','',$review[id]);
										
										/*delete terms relationship to review*/
										Review_Terms::associate2Review('',$review[id],'Y');
										
	                echo "Your Review was deleted";
                    }            		
            	}
     
function getYoutubeIdFromUrl($url) {
	$parts = parse_url ( $url );
	if (isset ( $parts ['query'] )) {
		parse_str ( $parts ['query'], $qs );
		if (isset ( $qs ['v'] )) {
			return $qs ['v'];
		} else if ($qs ['vi']) {
			return $qs ['vi'];
		}
	}
	if (isset ( $parts ['path'] )) {
		$path = explode ( '/', trim ( $parts ['path'], '/' ) );
		return $path [count ( $path ) - 1];
	}
	return false;
}
function isYoutubeVideo($video_id) {
	$isValid = false;
	$headers = get_headers ( 'http://gdata.youtube.com/feeds/api/videos/' . $video_id );
	if (strpos ( $headers [0], '200' )) {
		$isValid = true;
	}
	
	return $isValid;
}

?>
<script type="text/javascript" src="js/rating.js"></script>