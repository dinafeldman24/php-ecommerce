<?

      require_once "application.php";
		global $CFG;
		
	
		if( !$product )
        {
	         include('404.php'); 
	         exit(); 
        }
		
		$title = $product['title_tag']? $product['title_tag'] : $product['name'];
		$this_page_type = 'product';
        $desc_for_meta = preg_replace('/</',' <',$product['description']);
        $desc_for_meta = preg_replace('/>/','> ',$desc_for_meta);
        $desc_for_meta = preg_replace('/[\n\r\t]/',' ',$desc_for_meta);
        $desc_for_meta = preg_replace('/  /',' ',$desc_for_meta);
        $description = $product['meta_desc']?$product['meta_desc']:str_replace('"', ' ', $product['name']) . ' ' . str_replace( '"', ' ', $desc_for_meta );
        $keywords = $product['keywords'];
        $all_option_script = '';
        $product['name'] = (str_replace('�','&reg;',$product['name']));
        $product['description'] = (str_replace('�','&reg;',$product['description']));
        $product				= Products::get1( $product['id'] ); //Get the detailed info. 
        //ProductOptionCache::setProductOptionCache($product['id'], true);
        $brand					= Brands::get1( $product['brand_id'] );
        $image_link 			= Catalog::makeProductImageLink( $product['id'], false );
        $xlarge_image_link		= Catalog::makeProductImageLink( $product['id'], false, false, false, false);
        $discounts				= PromoCodes::getProductAuto($product['id']);
        $plink					= Catalog::makeProductLink_($product);
        $videos					= Youtube::get ( 0, '', '', 0, 0, false, 'product',$product['id'], 'Y' );
		$filters 				= Filters::getFiltersForProduct($product['id']);
        //$recently_viewed = Products::recently_viewed_products( $product['id'] );

        if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $product['essensa_price'] != '0.00' && $product['essensa_price'] < $product['price']){
	          $product['price'] = $product['essensa_price'];
        }
		ob_start();
		?>
		<script>
		gtag('event', 'view_item', {
		  "items": [
			{
			  "id": "<?=$product['id']?>",
			  "name": "<?=preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($product['name'], ENT_QUOTES));?>",
			  "list_name": "Product Page",
			  "brand": "<?=$product['brand_name']?>",
			  "price": "<?=$product['price']?>"
			}
		  ]
		});

		</script>
		<?
		$google_tag_mgr_data_layer = ob_get_contents();
		ob_end_clean();
		include 'includes/header.php';


	global $can_buy;
	global $prices_to_print_array;
	global $output_price;
	global $all_option_script;
	$can_buy_array = Products::getCanBuyInfo($product, $CFG);
	
	$can_buy = $can_buy_array['can_buy'];
	$can_buy_limit = $can_buy_array['can_buy_limit'];
	if( $can_buy_limit && ($can_buy_limit <= $CFG->show_can_buy_limit) )
	{
		$show_can_buy_limit = $can_buy_limit;
	}
	$product_options = Products::getProductOptions( 0, $product['id'] );
	if ($product_options)
	{
		$sale_for_options_data = Sales::see_if_all_options_on_sale($product['id']);
	}
	
	$prices_to_print_array = Products::getPriceToDisplayForProduct($product['id']);

	if ($prices_to_print_array['old_price_to_print'])
	{
		$old_price = $prices_to_print_array['old_price_to_print'];
	}
	if ($prices_to_print_array['sale_price_to_print'])
	{
		$output_price = $sale_price_to_print = $prices_to_print_array['sale_price_to_print'];
	}
	else $output_price = $prices_to_print_array['reg_price_to_print'];
	
	if ($product['case_discount'] == 'Y')
	{
		$case_price = $prices_to_print_array['case_price'];
		$case_price_per_piece = $prices_to_print_array['case_price_per_piece'];
	}
	if (Deals::seeIfCurrentDealForProd ($product['id']) && $_SERVER['SCRIPT_URL'] == '/discount_restaurant_supply.php')
	{
		$deals_get = Deals::get(0, true, $product['id']);
		$current_deal =  $deals_get[0];
		$per_price = $current_deal['deal_price'];
	}
	else if( $product['per_price'] > 0.00 && !$product_options) $per_price = $product['per_price'];
	//else $per_price = $product['price'];
    else $per_price = $output_price;	

	if( $product['retail_price'] > 0.00 )
	{
		$per_saved_dollars = number_format($product['retail_price'] - $per_price, 2);
		$per_saved = $per_price / $product['retail_price'];
		$per_saved = number_format( 100 - ($per_saved * 100 ), 0 );
	}
	
	if (Deals::seeIfCurrentDealForProd ($product['id']) && !Deals::seeIfCurrentNotSoldOutDealForProd ($product['id']) && $_SERVER['SCRIPT_URL'] != '/discount_restaurant_supply.php')
	{
		$deals_get = Deals::get(0, true, $product['id']);
		$deal_info =  $deals_get[0];
		$sold_out_deal_text = "<div class='sale_deal_na'>Deal is no longer available.<br/>Sold out at $" . $deal_info['deal_price']."</div>";
	}
	else $sold_out_deal_text = "";
	
	if (Sales::seeIfCurrentSaleForProd ($product['id'], 0, true) && !Sales::seeIfCurrentNotSoldOutSaleForProd ($product['id'], 'none'))
	{
		$sales_get = Sales::get(0, true, $product['id'], '', 'none', true);
		$sale_info =  $sales_get[0];
		$sold_out_sale_text = "<div class='sale_deal_na'>Sale is no longer available.<br/>Sold out at $" . $sale_info['sale_price'] . "</div>";
		
		$per_saved_dollars = number_format($product['retail_price'] - $sale_info['sale_price'], 2);
		$per_saved = $sale_info['sale_price'] / $product['retail_price'];
		$per_saved = number_format( 100 - ($per_saved * 100 ), 0 );
	}
	else $sold_out_sale_text = "";
	if (Deals::seeIfCurrentDealForProd ($product['id']) && ($_SERVER['SCRIPT_URL'] == '/discount_restaurant_supply.php' || Deals::seeIfCurrentNotSoldOutDealForProd ($product['id']))) {
		$price_label_text = "Deal of the Week! ";
	}
	else if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product['id'], 'none') && (!$product_options || ($product_options && $sale_for_options_data)))
	{
//		$old_price_label_text = "<div class='sale_price'>Our Price: $" . $old_price . "</div>";
		$price_label_text .= "<span class='sale-item-img'></span><div class='sale_price_label'>Sale Price: </div>";
	}
	else $price_label_text = "";
	
	if( trim( strtoupper( $product['priced_per']) ) != '' &&
	trim( strtoupper( $product['priced_per']) ) != 'EACH' &&
	trim( strtoupper( $product['priced_per']) ) != 'EA' && ($product['case_discount'] != 'Y') && trim($product['priced_per']) != '1')
	{
		$per_packaged_text = "<span id='priced_per'>per ".$product['priced_per']."</span>";
	}
	?>
		<div class="main-section" id="product-info">
		   <div class="product_view">
		       <h1><?php echo $product['name']?></h1>
		   
		    <div class="inner-product-view">
		       <div class="product-images">
			   
			   <? if ($filters)
				   foreach ($filters as $filter)
			          if ($filter['name']=='Quantity'){
						    $box_desc=$filter['product_filter_value']; 
			          }  
			   if ($box_desc) {?>		
		        <div class="boxofview">
				      <?php echo $box_desc ?>
			    </div>
			 <?}?>			 
				<div class="product-images" id="zoom-holder">				
					<?if(strpos($image_link,$CFG->no_image_name)===false){?>
					<div id="spotlight_holder">
					 <div id="spotlight-wrapper">
							<div id="spotlight">
								<img itemprop="image" align="middle" id="spotlight-img" src="" />
							</div>
						</div>
						<div class="zoomContainer img_box" id="zoom-div">
							<span class="zoom-icon hidden" id="zoom-icon"></span>
							<img itemprop="image" align="middle" id="img_zoom" src="<?=$xlarge_image_link?>" data-zoom-image="<?=$xlarge_image_link?>" alt="<?=$product['name']?>"/>
							<iframe class="hidden" id="video_iframe" src="" width="100%" height="100%"></iframe>  
						 </div>
						 </div>
					<?} else { ?>
						<div class="no-image-container">
							<? if ($product['energy_star'] == 'Y') {?>
								<span class="energy-star"></span>
							<? } ?> 
							<img src="<?=$image_link?>" alt="<?=$product['name']?>"/>
						</div>
					<?}?>
				
				</div>
			    </div>
		        <div class="product-desc">
				<div class="price-info-holder">
								<div class="price-holder<?=$product['case_discount'] == 'Y'? ' case':''?>">
									<div class="price-box">
										<?php 																				
										if ($product['map_price'] > 0 && $product['map_price'] >= $per_price)
										   	{ 
										   		if( $product['retail_price'] > 0.00)
										   		{
										   		?>										   		
										   		<!--<span class="old-price">Reg. Price $<?=number_format( $product['retail_price'], 2 )?></span>-->										   		
										   	 <? }
										   	 	echo $per_packaged_text;
												?>
											 	<span class="map-price">Add to cart to see price</span>
											 	<?php 
										 	}
											else 
											{
												
												if ($product['case_discount'] == 'N' &&  $per_saved > 0) {?>
													     
						 							 	<span class="del_price">$<?=$product['retail_price']?></span><span class="you_save"> Save <?=$per_saved?>%!</span>
						 						<?php }
												$schema_itemtype = ($prices_to_print_array['option_prices']) ? "http://schema.org/AggregateOffer" : "http://schema.org/Offer" ;
												if ($product['retail_price'] > 0 && $product['retail_price'] > $product['price'])
												{?>
												<!--<span class="old-price">Reg. Price $<?=number_format( $product['retail_price'], 2 )?></span>-->
												<?php }
												?>
												<div id="one_pc_price_<?=$product['id']?>" class="current-price" itemprop="offers" itemscope itemtype="<?=$schema_itemtype?>">
													 
													 <?php
												 	 if($prices_to_print_array['option_prices'] && $product['case_discount'] !== 'Y'){
														list($min_dollar_part,$min_cent_part) = explode('.', $prices_to_print_array['option_prices']['min']);
														list($max_dollar_part,$max_cent_part) = explode('.', $prices_to_print_array['option_prices']['max']);													
													 	?>
													  <?=$price_label_text?>
													 	<span id="the-price">
														<span itemprop="lowPrice">$<?=$min_dollar_part?>.<?=$min_cent_part?></span> - <span itemprop="highPrice">$<?=$max_dollar_part?>.<?=$max_cent_part?></span></span>
													 	<?php 
													 } else 
														
														{ 
														list($dollar_part,$cent_part) = explode('.', $output_price);
														if ($product['case_discount'] == 'Y')
														{?>
														<table cellspacing="0" class="case-discount">
														<tr class="heading">
														<td class="first">Price each</td>
														<td>Case of <?=Products::getSoldAsCaseText($product['id'])?></td></tr>
														<tbody><tr><td class="first">
														<? }?>
														<span id="the-price"><?=$price_label_text?><span itemprop="price">
														<?php 
														if($product['case_discount'] == 'Y' && $prices_to_print_array['option_prices']){
															echo '<span class="from-tag">From</span> $' . $prices_to_print_array['option_prices']['min']; 
														}else{
															?>
															$<?=number_format($dollar_part)?>.<?	
															if ($product['case_discount'] != 'Y') {?><? } ?><?=$cent_part?><?	
															if ($product['case_discount'] != 'Y') {?><? } ?></span>
															<?php
															if ($per_packaged_text != '') echo " / " .$per_packaged_text; 
														}?></span>
														<meta itemprop="pricecurrency" content="USD"/>
		
														<?	if ($product['case_discount'] == 'Y')
														{?>
															<div class='case_each'> per <?= Products::getSoldAsText( $product['id'] ) ?></div></td>
															<td><span id="one_pc_case_price_<?=$product['id']?>" class="one_piece_price">
															<?=($prices_to_print_array['option_prices'])?'<span class="from-tag">From</span>':''?>
															$<?=number_format( $case_price_per_piece, 2 )?></span> <div class='case_each'> per <?= Products::getSoldAsText( $product['id'] ) ?></div></td></tr></tbody></table>
						 							    <? }
													 
														}?>
                                                        <span id='additional_charges'></span><span id='total_charges'></span>
												</div>
												<?php
												
												
												if( $product['retail_price'] > 0.00 && ($product['map_price'] == 0 || $product['map_price'] <= $per_price) && $product['retail_price'] > $per_price && $product['case_discount'] != 'Y') 
												{  
													echo $old_price_label_text;
													?>    
													<?php 
													if($product['show_map_message'] == 'Y'){
													?>
													<span class="map_message">This item has MAP pricing.</span>
													<span class="map_message"> Please <a href="<?=$CFG->baseurl?>info-desk.html" target=_blank>contact us</a> for a better price. </span>
													<?php 
													}
												}
												?>
											 	<div id="sold_out_text">
 													<?= $sold_out_deal_text ?>
 													<?= $sold_out_sale_text ?>
 												</div>
										 	<?php } 
		 									 ?>
									</div>
								</div>
								
								<?
								$second_price = false;
								if( trim( strtoupper( $product['per_order']) ) != '' &&
									trim( strtoupper( $product['per_order']) ) != 'EACH' &&
									trim( strtoupper( $product['priced_per']) ) != 'EA') 
								{
									$second_price = true;
								}
								
								if($second_price || !$can_buy )
								{
									?>
									<div class="caution">
										<div class="caution-box">
											<? 
											if($second_price){ ?>
												<span>
												<?=ucwords($product['per_order'])?> purchase required. <br/>
												<?
												if( $product['unit_case'] )
												{
													?>
													Packed <?=$product['unit_case']?> <?=$product['priced_per']?> per case.
													<?
												}?>
												</span>
												<?
											}																					
											?>
										</div>
									</div>
									<? 
								} 
							
								$img_link = Catalog::makeProductImageLink( $product['id']);
								$param_sku = addcslashes($product['mfr_part_num'], '"');
								$param_price = number_format( $product['price'], 2 );								
								if ($product['group_id']) showVariations();
								if($can_buy){
									?>
									<input type="hidden" name="qty_case_<?=$product['id']?>" value="<?=$product['unit_case']?>" id="<?=$product['id']?>_case_amount" />
			
									<div class="cart-box">
										<div class="cart-holder">
											<?php 
											if( $product_options )
											{
												Products::displayDynamicOptionDropdown($product['id']);
											}
											?>
											<div class="qty-add-holder">
												<?php $qty = $product['min_qty'] ? $product['min_qty'] : 1; ?>
												<?php $input_qty = $qty>1 ? '' : 1; ?>
												<?php 
												if($qty > 1){
													?>
													<div class="qty_msg">You must buy a minimum of <?=$qty?> packs at a time.</div> 
													<?php 
												}
												?>
												<label class="qty">Qty.</label>
												<input type="tel" id="quantity" value="<?=$input_qty?>" name="qauntity" data-min-qty="<?=$qty?>" class="quantity-input"/>
        <script type='text/javascript'>
        function add_this_product_to_cart(){
            add_to_cart( <?=$product['id']?>, null, 
                <?=$can_buy_limit?>, '<?=$param_sku?>', 
                '<?=$output_price?>', '<?=$img_link?>',
				'<?=$this_page_type?>', '<?=htmlspecialchars(str_replace("'","\'",$product['name']), ENT_QUOTES, 'UTF-8')?>');
			/*gtag('event', 'add_to_cart', {
			  "items": [
				{
				  "id": "<?=$product['id']?>",
				  "name": "<?=preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($product['name'], ENT_QUOTES));?>",
				  "brand": "<?=$product['brand_name']?>",
				  "quantity":document.getElementById("quantity").value,
				  "price": "<?=$output_price?>"
				}
			  ]
			});*/
			add_to_cart_track("<?=$product['id']?>","<?=preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($p['name'], ENT_QUOTES))?>","<?=$product['brand_name']?>","<?=$output_price?>");
			return;
            };
            </script> 
											<button class="button-brown add_to_cart_b" id='add_to_cart_btn'>ADD TO CART</button>
            		<?php 
                    $all_option_script .= "<script type='text/javascript'>\$j(document).ready(function(){ \$j('#add_to_cart_btn').click(add_this_product_to_cart);});</script>";
					if($can_buy && ($show_can_buy_limit || (strtotime($product['restock_date'])> strtotime(date("Y-m-d"))  && $product['restock_date_show_on_website'] == 'Y'))){
						
						?>
						<div class="inner-box message">
						<?php 
						if($show_can_buy_limit )
						{
							?>
							<meta itemprop="availability"  itemtype="http://schema.org/LimitedAvailability/"/>
							<div id="remaining_inv">
						    Only <?=$show_can_buy_limit?> Remaining!
							</div>
						    <?
						}
						if(strtotime($product['restock_date'])> strtotime(date("Y-m-d"))  && $product['restock_date_show_on_website'] == 'Y'){
						?>
							<meta itemprop="availability"  itemtype="http://schema.org/PreOrder/"/>
							<span>BACKORDERED</span>
							<p>This item will be back in stock <strong><?=date('F jS', strtotime($product['restock_date']))?></strong>. Order now to reserve.</p>
							
						<?
						}
						?> 
						</div>
						<?php
					}
					?>
					                	</div>
					                	
					                	</div>
									</div>
									<?
								} 
								?>
				   </div>
				   
				   <h2 class="details">Details:</h2>
						<?
						if($_SESSION['id']){
							//verify the admin
							require_once("$CFG->libdir/admin.php");
							$admin_info = db_get1(Admin::get('',$_SESSION['id'],0,'','Y'));
							if(trim($_SESSION['email']) == trim($admin_info['email'])){
								//display the edit button
								echo "<div><a href='{$CFG->baseurl}edit/products.php?action=view&id={$product['id']}' target='_blank'><button>EDIT PRODUCT</button></a></div>" ;
							}
						}
						?>
				   	<?= trim($product['description']);?>
					<?php show_specifications($product);?>
				
				</div>
		     </div>
		  </div>	
	   </div>
<input type="hidden" id="wishlist_customer_id" value="<?=(MyAccount::logged_in()) ? $_SESSION['cust_acc_id'] : 0?>">
	
<?php    include 'includes/footer.php';

function show_specifications($product){
	$filter_values = Products::getProductFilterValues($product['id']);	
	?>
	<div class="spec-table">
		<div class="specs">
			<ul class="line-holder" itemprop="brand" itemscope itemtype="http://schema.org/Brand">
				<li class="column-left" >Brand:</li>
				<li class="column-right" itemprop="name"><?=$product['brand_name']?></li>
			</ul>
			<ul class="line-holder">
				<li class="column-left">Model #:</li>
				<li class="column-right" itemprop="sku"><?= $product['mfr_part_num']?></li>
			</ul>
			<ul class="line-holder">
				<li class="column-left">Ship weight:</li>
				<li class="column-right"><?=Weight::display($product['weight'])?></li>			
			</ul>
			<ul class="line-holder">
				<li class="column-left">Sold As:</li>
				<li class="column-right"><?= Products::getSoldAsText( $product['id'] ) ?></li>
			</ul>
			<?php if ($filter_values)
			{
				$counter = 0;
				foreach ($filter_values as $one_filter)
				{
					if ($one_filter['name'] == "Label" || $one_filter['name'] == "Abstract" || ($one_filter['name'] == "Made in Germany" && $one_filter['value'] == "No") || $one_filter['value'] == '') continue;
					?>
					<ul class="line-holder">
					<li class="column-left"><?=$one_filter['name']?></li>
					<li class="column-right"><?= $one_filter['value'] ?> </li>
					<li class="column-right viewall"><a href="<? echo $CFG->baseurl . lcfirst($one_filter['name']).".html?value=".$one_filter['value']?>">View all <span><?=$one_filter['value']?></span></a></li>
					</ul>
					<?php 
					$counter++;
				}
			}
			if ($product['length'] > 0)
			{
					?>
					<ul class="line-holder">
					<li class="column-left">Length:</li>
					<li class="column-right"><?= $product['display_length'] ?>&quot;</li>
					</ul>
					<?php 
					$counter++;
			}
			if ($product['width'] > 0)
			{
					?>
					<ul class="line-holder">
					<li class="column-left">Width:</li>
					<li class="column-right"><?= $product['display_width'] ?>&quot;</li>
					</ul>
					<?php 
					$counter++;
			}
			if ($product['height'] > 0)
			{
					?>
					<ul class="line-holder">
					<li class="column-left">Height:</li>
					<li class="column-right"><?= $product['display_height'] ?>&quot;</li>
					</ul>
					<?php 
					$counter++;
			}?>
		</div>
	</div>
	<?php 
}

?>
<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>