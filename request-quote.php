<?
include_once('application.php');
$title = "Request a Quote";
$description = "Request a Quote";

$errors = "";

$info = $_REQUEST['info'];
$product_id = $_REQUEST['product_id'];
$qty = $_REQUEST['qty'];

if($_POST){

	$email_field_border_color = $name_field_border_color = $message_field_border_color = '';
	if($info['name'] == ""){
		$errors .= "Please enter your name <br>";
		$name_field_border_color = 'red';		
	}

	if(!validate_email($info['email'])){
		$errors .= "Please enter a valid e-mail address<br>";
		$email_field_border_color = 'red';
	}

	if(strlen($info['message']) < 1){
		$errors .= "Please enter a message";
		$message_field_border_color = 'red';		
	}
	if ($info['quote_other'] != '')
	{
		$errors .= "You may not submit this form.";
		$message_field_border_color = 'red';
	}

	if(!$errors){
		$msg = "Someone used the Request a Quote Form to send a question:<br /><br>";
		$msg .= "<b>From:</b> ".$info['name'] . ", <b>company:</b> " . $info['company'] . ", <b>email:</b> " . $info['email'] . ", <b>phone:</b> " . $info['phone']."<br /><br />";
		$num=count($product_id);
		$msg .= ($num>0) ? "<b>Products(ID) - Quantity: </b><br>" : "";
		for ($i=0; $i<$num; $i++)
		   $msg .= $product_id[$i] . " - ". $qty[$i] ."<br>";
		$msg .= "<b>Message: </b>".$info['message'];
		$msg .= "<br /><br /> User IP: " . $_SERVER['REMOTE_ADDR'] . "<br />User Browser: " . $_SERVER['HTTP_USER_AGENT'];

		$to_email = $CFG->company_email;
		$to_email = 'dina.websites@gmail.com';

		$vars['body'] = $msg;
		$E = TigerEmail::sendOne($CFG->company_name,$to_email,"blank", $vars, 'Request a Quote (Visitor Form)',true,$info['name'],$info['email']);

		$success = true;
		unset($info);
		unset($product_id);
		unset($qty);
	} else {

	}
} else {
	if($_SESSION['cust_acc_id'] > 0){
		$cust = Customers::get1($_SESSION['cust_acc_id']);
		$info['name'] = $cust['first_name'] . " " . $cust['last_name'];
		$info['email'] = $cust['email'];
	}
}

ValidateData::safeOutputArray($info);

include( $CFG->dirroot . '/includes/header.php');
?>

 <div class="main-section">

				<div class="content-title">
					<h1>Request a Quote</h1>
				</div>
                <br>Please fill out the following form and our product and pricing experts will provide you with a quote for your item(s)..
				<div class="form-box fieldset">
				   <h2 class='legend'>Request Quote</h2>
				<form action="<?=$_SERVER['PHP_SELF']."#contact_form"?>" method="post" class="contact-form container request-quote">
					<fieldset>
											
						<? if($errors){ ?>
						<div class="errorBox"><?=$errors?></div>
						<? } else if($success ) {?>
						<div class="niceMessage">Thank you for contacting us! A customer service representative will respond within 24 business hours.</div>
						<? } ?>
		                <ul class="form-list ">			
						<li>
							<label>Name</label>
						    <input type="text" id="name" placeholder="Name" name="info[name]" value="<?=($info['name'])?>" <?= $name_field_border_color ? "style='border-color: $name_field_border_color'": ""?>/>
						</li>
						<li>
							<label>Company</label>
							<input type="text" id="company" placeholder="Company" name="info[company]" value="<?=($info['company'])?>" />
						</li>
						<li>
							<label>Email</label>
                            <input type="email" id="email" placeholder="Email" name="info[email]" value="<?=($info['email'])?>" <?= $email_field_border_color ? "style='border-color: $email_field_border_color'": ""?>/>
						</li>
						<li>
							<label>Phone</label>
							<input type="phone" id="phone" placeholder="Phone" name="info[phone]" value="<?=($info['phone'])?>">
						</li>
						<? 
							
							for ($i=0; $i<5; $i++){?>
							<li>
							<div style="float:left;margin-right:15px;">
								<label>Product ID</label>
								<input type="text" id="product_id_<?=$i?>" placeholder="Item #" name="product_id[<?=$i?>]" value="<?=($product_id[$i])?>" >
							</div>
							<div style="float:left;">
								<label>Quantity</label>	
								<input type="text" id="qty_id_<?=$i?>" placeholder="Qty" name="qty[<?=$i?>]" value="<?=($qty[$i])?>">
							</div>
							</li>
							<div style="clear:both;"></div>
							<?}?>
							<input type="hidden" name="info[quote_other]" value="">
							<li>
							     <label>Message/Questions</label>	
								 <textarea id="message" name="info[message]" placeholder="Message/Questions" cols="30" rows="10" <?= $message_field_border_color ? "style='border-color: $message_field_border_color'": ""?>><?= ($info['message'])?></textarea>
							</li>
							<li style="margin-top:20px;">
								<input type="submit" class="button-brown add_to_cart_b" value="send" />
							</li>
							</ul>
						</div>
					</fieldset>
				</form>
	</div>
</div>		

<?
include( $CFG->redesign_dirroot . '/includes/footer.php');

?>