<?
include('application.php');

$errors = "";

$info = $_REQUEST['info'];

if($_POST){

	$email_field_border_color = $name_field_border_color = $message_field_border_color = '';
	if($info['name'] == ""){
		$errors .= "Please enter your name <br>";
		$name_field_border_color = 'red';		
	}

	if(!validate_email($info['email'])){
		$errors .= "Please enter a valid e-mail address<br>";
		$email_field_border_color = 'red';
	}

	if(strlen($info['message']) < 1){
		$errors .= "Please enter a message";
		$message_field_border_color = 'red';		
	}

	if(!$errors){
		$msg = "Someone used the Contact Form to send a comment or question:<br />";
		$msg .= "From: ".$info['name'] . ", email: " . $info['email'] . "<br /><br />";
		$msg .= $info['message'];
		$msg .= "<br /><br /> User IP: " . $_SERVER['REMOTE_ADDR'] . "<br />User Browser: " . $_SERVER['HTTP_USER_AGENT'];

		$to_email = $CFG->company_email;
		

		$vars['body'] = $msg;
		$E = TigerEmail::sendOne($CFG->company_name,$to_email,"blank", $vars, $info['topic']." (Visitor Contact Form)",true,$info['name'],$info['email']);

		$success = true;
		unset($info);
	} else {

	}
} else {
	if($_SESSION['cust_acc_id'] > 0){
		$cust = Customers::get1($_SESSION['cust_acc_id']);
		$info['name'] = $cust['first_name'] . " " . $cust['last_name'];
		$info['email'] = $cust['email'];
	}
}

ValidateData::safeOutputArray($info);

include( $CFG->redesign_dirroot . '/includes/header.php');

?>
<div class="main-section">
<div class="wrapper">
	<div class="twocolumns">
		<div id="content">
			<div class="content-wrap">
				<div class="content-title">
					<h1>Our Return Policy</h1>
				</div>
				<div class="contact-block container">
                     <p>At justchargerplates.com your complete satisfaction is our top priority!! If you are unhappy with your purchase for any reason simply contact us so we can issue you a RA (Return Authorization) number and provide you with all the proper return instructions. Please be advised that there will always be at least a 25% restocking fee on all returns (other than the exceptions listed below) and that outgoing and incoming freight charges are never refunded. On items that qualified for free shipping, the customer will be responsible to pay the initial shipping fees incurred by justchargerplates.com. Since we deal with so many vendors and products, a RA number is ALWAYS required for any return to be processed. Please be aware that if you fail to contact us and get a proper RA number, return any merchandise along with your RA that you did not receive authorization to return, or if your merchandise is returned in unsatisfactory condition, you will not be credited for your return. You may only return items up to 10 business days after receipt and we can only accept merchandise repackaged in its original packing, and received as<br>satisfactory and re-sellable goods deemed by justchargerplates.com, or the vendor. Also, after you have gotten your return authorization number, you must ship your merchandise back within 10 business days or the return authorization number will expire.<br>If the merchandise you receive is defective or damaged according to industry standards , we will rectify the situation immediately. Although we do all we can at justchargerplates.com to ensure that the right product arrives at your doorstep in perfect condition, sometimes things go wrong and an item must be returned. The manufacturer has final say in determining if a product is defective. If a purchase arrives defective, damaged, or in error, you will not be charged for return shipping or a restocking fee. If you have received damaged, improper*, or defective merchandise, contact us immediately and we will do everything possible to reconcile the situation as quickly as possible.
					 </p>
					
				</div>
				<form action="<?=$_SERVER['PHP_SELF']."#contact_form"?>" method="post" class="contact-form container">
					<fieldset>
						
						<p>To receive an RA number and instructions to return or exchange an item, please contact us by filling out the form below. <? 	if(!(time() < strtotime($CFG->remove_live_chat_end_date) && time() > strtotime($CFG->remove_live_chat_start_date)))
			{
		?>

		<? } ?></p>
						
						<? if($errors){ ?>
						<p class="errorBox"><?=$errors?></p>
						<? } else if($success ) {?>
						<div class="niceMessage">Thank you for contacting us! A customer service representative will respond within 24 business hours.</div>
						<? } ?>
						<div class="form-box fieldset">
						<h2 class="legend">Contact Us</h2>
							<ul class="form-list ">
							<li>
								<label for="name" class="required">Name:</label>
								<input type="text" id="name" name="info[name]" value="<?=($info['name'])?>" <?= $name_field_border_color ? "style='border-color: $name_field_border_color'": ""?>/>
							</li>
							<li>
								<label for="email" class="required">Email:</label>
									<input type="email" id="email" name="info[email]" value="<?=($info['email'])?>" <?= $email_field_border_color ? "style='border-color: $email_field_border_color'": ""?>/>
							</li>
							<li>
								<label for="topic" class="required">Topic:</label>
						 <select name="info[topic]" id="topic">
						<?
							$topics = split(',',$CFG->contact_us_topics);
							if(is_array($topics))
								foreach($topics as $topic){
									$s = ($info['topic'] == trim($topic) ? "selected='selected'" : '');
									echo "<option $s>".trim($topic)."</option>";
								}
						?>
						</select>

							</li>
							<li>
								<label for="message" class="required">Message:</label>
									<textarea id="message" name="info[message]" cols="30" rows="10" <?= $message_field_border_color ? "style='border-color: $message_field_border_color'": ""?>><?= ($info['message'])?></textarea>
							</li>
							<li>
								<input type="submit" class="button-brown add_to_cart_b" value="submit" />
							</li>
						 </ul>	
						</div>
					</fieldset>
				</form>
			</div>
		</div>
<? 		include ($CFG->redesign_dirroot . "/includes/content_page_sidebar.php"); ?>
	</div>
</div>		
</div>
<?
include( $CFG->redesign_dirroot . '/includes/footer.php');

?>