<?

echo "<!doctype html>";
$doctype_included=true;

require_once('application.php');


if ($page_id == "order-status")
{
	include "order-status.php";
	exit;
}

if( $_REQUEST['id'] && is_numeric($_REQUEST['id']) && !isset($page_id) )
{
	$page = Content::get1( $_REQUEST['id'] );
	$page_id = $page['page_id'];
	if ($page_id)
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("location: /".$page_id.".html"); 
	}
	else 
	{
		include('404.php');
	}
	
}


if(!isset($page_id)) exit();

if($page_id == "thank-you"){
	// Page Arrived after Google Checkout or Amazon Checkout.
	// Empty the Cart
	$cart->emptyCart();
}

$page = Content::getByPageId($page_id);
$title = $page['title'];
$description = $page['description'];
$keywords = $page['keywords'];

include( $CFG->redesign_dirroot . '/includes/header.php');

	//echo("<h1>".$page['h1']."</h1>\n");
	//echo '<div class="cms">';
	?>
	<div class="content-pages">
	<div class="main-section">
 
	<div class="content-title">
	<h1><?=$page['h1']?></h1>
	</div>
	
	<?  
	echo($page['content']);
	?>
	</div></div>
	
	
<? 	

include( $CFG->redesign_dirroot . '/includes/footer.php');

?>