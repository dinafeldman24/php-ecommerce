<?
include('application.php');
$title = "Have Questions? Ask a JustChargerPlates Restaurant Equipment Expert.";
$description = "Our product experts will be happy to answer your questions and help you with your purchase.";

$errors = "";

$info = $_REQUEST['info'];

if($_POST){

	$email_field_border_color = $name_field_border_color = $message_field_border_color = '';
	if($info['name'] == ""){
		$errors .= "Please enter your name <br>";
		$name_field_border_color = 'red';		
	}

	if(!validate_email($info['email'])){
		$errors .= "Please enter a valid e-mail address<br>";
		$email_field_border_color = 'red';
	}

	if(strlen($info['message']) < 1){
		$errors .= "Please enter a message";
		$message_field_border_color = 'red';		
	}
	
	if(!$errors){
		
		$msg = "Someone used the Question Form to ask a question about a product:<br />";
		$msg .= "From: ".$info['name'] . ", email: " . $info['email'] . "<br /><br />";
		$msg .= "Company: ".$info['company']. "<br /><br />";
		$msg .= "Phone: ".$info['phone']. "<br /><br />";
		$msg .= "Item SKU: ".$info['item_sku']. "<br /><br />";
		$msg .= $info['message'];
		$msg .= "<br /><br /> User IP: " . $_SERVER['REMOTE_ADDR'] . "<br />User Browser: " . $_SERVER['HTTP_USER_AGENT'];

		$to_email = $CFG->company_email;

		
		$vars['body'] = $msg;
		$E = TigerEmail::sendOne($CFG->company_name,$to_email,"blank", $vars, $info['topic']." (Visitor Contact Form)",true,$info['name'],$info['email']);

		$success = true;
		unset($info);
	} else {

	}
} else {
	if($_SESSION['cust_acc_id'] > 0){
		$cust = Customers::get1($_SESSION['cust_acc_id']);
		$info['name'] = $cust['first_name'] . " " . $cust['last_name'];
		$info['email'] = $cust['email'];
	}
}

ValidateData::safeOutputArray($info);

include( $CFG->redesign_dirroot . '/includes/header.php');

?>

           <div class="main-section">
				<div class="content-title">
					<h1>Have Questions? Ask a JustChargerPlates Restaurant Equipment Expert.</h1>
				</div>

<p>Please fill the following form; our product experts will be happy to answer your questions and help you with your purchase.</p>

				<form action="<?=$_SERVER['PHP_SELF']."#contact_form"?>" method="post" class="contact-form container">
					<fieldset>
				
						
						<? if($errors){ ?>
						<div class="errorBox"><?=$errors?></div>
						<? } else if($success ) {?>
						<div class="niceMessage">Thank you for contacting us! A customer service representative will respond within 24 business hours.</div>
						<? } ?>
                        <div class="form-box fieldset">
							<h2 class="legend">Product Questions</h2>
							<ul class="form-list ">
							<li>
								<label for="name">Name:</label>
								<input type="text" id="name" name="info[name]" value="<?=($info['name'])?>" <?= $name_field_border_color ? "style='border-color: $name_field_border_color'": ""?>/>
	                        </li>						
							<li>
								<label for="email">Email:</label>
								<input type="email" id="email" name="info[email]" value="<?=($info['email'])?>" <?= $email_field_border_color ? "style='border-color: $email_field_border_color'": ""?>/>
							</li>	
							<li>
								<label for="company">Company:</label>
								<input type="text" id="company" name="info[company]" value="<?=($info['company'])?>" />
	                        </li>	
							<li>
								<label for="phone">Phone:</label>
								<input type="tel" id="phone" name="info[phone]" value="<?=($info['phone'])?>" />
	                        </li>	
							<li>
								<label for="item_sku">Item Sku:</label>
								<input type="text" id="item_sku" name="info[item_sku]" value="<?=($info['item_sku'])?>" />
	                        </li>	
						<li>	
								<label for="message">Message/Question:</label>
								<textarea id="message" name="info[message]" cols="30" rows="10" <?= $message_field_border_color ? "style='border-color: $message_field_border_color'": ""?>><?= ($info['message'])?></textarea>
                        </li>
						<li>	
								<input type="submit" class="button-brown add_to_cart_b"  value="submit" />
						</li>
                        </ul>
						</div>
					</fieldset>
				</form>
			</div>
		

<?
include( $CFG->redesign_dirroot . '/includes/footer.php');

?>