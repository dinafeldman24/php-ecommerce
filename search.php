<?php
// PLEASE NOTE: I (R. Sunness) reset mysql variable  ft_min_word_len to 3 from the default of 4 to make this search more effective
// This search functionality relies on autoRun/generate_prod_summary_for_search.php to run regularly
 
include('application.php');

function trim_value(&$value)
{
    $value = trim($value);    // this removes whitespace and related characters from the beginning and end of the string
}
array_filter($_REQUEST, 'trim_value'); 
$_REQUEST = filter_var_array($_REQUEST,FILTER_SANITIZE_STRING);
$original_query = $_REQUEST['q']; 
$query_string = addslashes(str_replace("--", "-", $_REQUEST['q']));
$query_string = str_replace("&#39;", "", $query_string);
$query_string = str_replace("&#34;", " inch", $query_string);
//echo $query_string;

// if query is empty, show messsage and exit (whether because the user entered nothing, or the sanitizing cleared out everything...)
if ($query_string == '')
{
	include 'includes/header.php';?>
	<div class="main-section"> 
	 <div class="category-heading"><?php 
	 echo "<h3>Please enter a valid search term.</h3>";?>
	 </div></div><?php 
	exit;
}
// if query is 1 word, look for exact part number match or sku match
if(strpos(trim($query_string), ' ') === false)
{
	// First see if it's exact sku match or an exact mfr part number match.  If it is, just go STRAIGHT there!
	$mfr_part_num_result = Products::get1ByMfrPartNum($query_string);
	if ($mfr_part_num_result && !$mfr_part_num_result['warning'])
	{
		$url_for_single_result = Catalog::makeProductLink_($mfr_part_num_result);
		SearchTerms::insert(array('term'=>$original_query, 'date'=>date("Y-m-d H:i:s"),'results_found_exact'=>'1'));
		header("location:$url_for_single_result");
		exit;
	}
	$vendor_sku_result = Products::get1ByVendorSkuName($query_string);
	if ($vendor_sku_result)
	{
		$url_for_single_result = Catalog::makeProductLink_($vendor_sku_result);
		SearchTerms::insert(array('term'=>$original_query, 'date'=>date("Y-m-d H:i:s"),'results_found_exact'=>'1'));
		header("location:$url_for_single_result");
		exit;
	}
}
$title = 'Search Results for ' . $query_string;
	
$order ='';
$order_asc='';

switch( $_REQUEST['sortby'] )
{
	case 1:
		$order ='products.price ASC';		
		break;
	case 2:
		$order = 'products.price DESC';
		break;
	case 3:
		$order = 'products.name ASC';
		break;
	case 5:
		$order = 'products.name DESC';
		break;
	case 6:
		$order = 'products.priority ASC,best_selling_products.num_orders ASC, products.price ASC ';	
	case 4:
	default:
	    $order = 'products.priority DESC,relevance DESC, best_selling_products.num_orders DESC, products.price ASC ';
}

if ($_REQUEST['sortby'] != "") 
{
	$urlParams .= "&sortby=".$_REQUEST['sortby'];         			
}
if ($_REQUEST['per_page'] != "") 
{
	$urlParams .= "&per_page=".$_REQUEST['per_page'];
}
if ($_REQUEST['setPerPage'] != "") 
{
	$urlParams .= "&per_page=".$_REQUEST['setPerPage'];	
	$_REQUEST['per_page'] = $_REQUEST['setPerPage']; 						            		            		
}
if ($query_string != "") 
{
	$urlParams .= "&q=".$query_string;
}
if (substr($urlParams,0,1) == "&") $urlParams = substr_replace($urlParams, '?', 0, 1);

if( $_REQUEST['per_page'] ) $per_page = $_REQUEST['per_page'];
else $per_page = 25;
	
$color_addition = $material_addition = $shape_addition = $quantity_addition = $brand_addition = "";
$avail_colors = explode(",",$CFG->color_filter);
$avail_materials = explode(",",$CFG->material_filter);
$avail_shapes = explode(",", $CFG->shape_filter);
$avail_quantities = explode(",", $CFG->quantity_filter);
$avail_brand_phrases = explode(",", $CFG->brand_names_for_search);
$keywords_array = preg_split('/\s+/', $query_string);
foreach($keywords_array as &$one_word)
{
	$one_word = Inflector::singularize($one_word);
	$one_word = str_replace("-inch", " inch", $one_word);
}
$reconstructed_search_phrase = implode(' ', $keywords_array);
foreach($avail_colors as $one_color)
{		
	if (strpos($reconstructed_search_phrase, $one_color)!== false)
	{
		$color_addition = " AND (color = '$one_color' OR psfs.product_name LIKE '%$one_color%')";
		if($one_color != trim($reconstructed_search_phrase)) $reconstructed_search_phrase = str_replace($one_color, "", $reconstructed_search_phrase);
		break;			
	}			
}	
foreach($avail_materials as $one_material)
{
	if (strpos($reconstructed_search_phrase, $one_material)!== false)
	{
		$material_addition = " AND material = '$one_material'";
		if($one_material != trim($reconstructed_search_phrase)) $reconstructed_search_phrase = str_replace($one_material, "", $reconstructed_search_phrase);
		break;
	}			
}
foreach($avail_shapes as $one_shape)
{
	if (strpos($reconstructed_search_phrase, $one_shape)!== false)
	{
		$shape_addition = " AND shape = '$one_shape'";
		if($one_shape != trim($reconstructed_search_phrase)) $reconstructed_search_phrase = str_replace($one_shape, "", $reconstructed_search_phrase);
		break;
	}			
}
foreach($avail_quantities as $one_qty)
{
	if (strpos($reconstructed_search_phrase, $one_qty)!== false)
	{
		$quantity_addition = " AND quantity = '$one_qty'";
		if($one_qty != trim($reconstructed_search_phrase)) $reconstructed_search_phrase = str_replace($one_qty, "", $reconstructed_search_phrase);
		break;
	}			
}
foreach($avail_brand_phrases as $one_brand_phrase)
{
	if (strpos($reconstructed_search_phrase, $one_brand_phrase)!== false)
	{
		$brand_addition = " AND brand_name LIKE '%$one_brand_phrase%'";
		if($one_brand_phrase != trim($reconstructed_search_phrase) )$reconstructed_search_phrase = str_replace($one_brand_phrase, "", $reconstructed_search_phrase);
		break;
	}			
}

// Remove the word "Charger" from the search phrase unless it is the only word left
if(strtolower(trim($reconstructed_search_phrase)) != "charger") $reconstructed_search_phrase = str_ireplace("charger", "", $reconstructed_search_phrase);

$reconstructed_search_phrase = trim($reconstructed_search_phrase);
$keywords_array = preg_split('/\s+/', $reconstructed_search_phrase);

//$keywords_array = array_diff($keywords_array, array("inch", "in.", "in", "inches", "inche"));
//array_map($keywords_array);
$search_str = '\'' . implode(' ', $keywords_array) . '\'';
$search_str3 = '\'"' . implode(' ', $keywords_array) . '"\'';
$search_str2 = '\'+' . implode(' +', $keywords_array) . '\'';
$search_str4 = implode(' ', $keywords_array);
$search_str5 = '\'' . implode(' ', $keywords_array) . '*\'';

$query = 'SELECT psfs.*,products.brand_id, products.price, products.priority, products.url_name, 
IF (best_selling_products.num_orders is null,0, best_selling_products.num_orders) AS num_orders_final , 
(
((MATCH(psfs.product_name) AGAINST ('.$search_str.' IN BOOLEAN MODE))) + ';		
if (count($keywords_array) > 1) $query .= '((MATCH(psfs.product_name) AGAINST ('.$search_str3.' IN BOOLEAN MODE))) + ';
$query .= '	
((MATCH(psfs.brand_name) AGAINST ('.$search_str.' IN BOOLEAN MODE))) +
((MATCH(psfs.aka_sku) AGAINST ('.$search_str.' IN BOOLEAN MODE))) +
((MATCH(psfs.product_vendor_sku) AGAINST ('.$search_str.' IN BOOLEAN MODE))) +
((MATCH(psfs.mfr_part_num) AGAINST (\''.$query_string.'*\' IN BOOLEAN MODE))) +
((MATCH(psfs.product_options_mfr_part_num) AGAINST (\''.$query_string.'*\' IN BOOLEAN MODE))) +	
((MATCH(psfs.product_options_vendor_sku) AGAINST ('.$search_str.' IN BOOLEAN MODE))) +
((MATCH(psfs.color) AGAINST ('.strtolower($search_str).' IN BOOLEAN MODE))) +	
((MATCH(psfs.material) AGAINST ('.strtolower($search_str).' IN BOOLEAN MODE))) +
((MATCH(psfs.quantity) AGAINST ('.strtolower($search_str).' IN BOOLEAN MODE))) +
((MATCH(psfs.shape) AGAINST ('.strtolower($search_str).' IN BOOLEAN MODE))) +	
((MATCH(psfs.product_name, psfs.brand_name, psfs.product_vendor_sku, psfs.aka_sku, psfs.description, psfs.product_options_vendor_sku, psfs.mfr_part_num, psfs.product_options_mfr_part_num, psfs.color, psfs.shape, psfs.material, psfs.quantity) AGAINST ('.$search_str3.' IN BOOLEAN MODE)))';
//$query .= $demotion_string; 
$query .= ') AS relevance 
FROM (prod_summary_for_search psfs, products)
LEFT JOIN best_selling_products ON psfs.the_product_id = best_selling_products.product_id	
WHERE (MATCH(psfs.product_name, psfs.brand_name, psfs.product_vendor_sku, psfs.aka_sku, psfs.description, psfs.product_options_vendor_sku, psfs.mfr_part_num, psfs.product_options_mfr_part_num, psfs.color, psfs.shape, psfs.material, psfs.quantity) AGAINST ('.$search_str5.' IN BOOLEAN MODE) )
AND products.id = psfs.the_product_id';

if (in_array("inch", $keywords_array))
{
	foreach ($keywords_array as $one_keyword) 
	{
		if (is_numeric($one_keyword))
		{						
			$search_str6 = '\'*' . $one_keyword . '*\'';
			$query .=  " AND ((MATCH(psfs.description) AGAINST ($search_str6 IN BOOLEAN MODE)))";
			//break;
		}
		else if ($one_keyword != "inch")
		{
			$search_str6 = '\'*' . $one_keyword . '*\'';
			$query .=  " AND ((MATCH(psfs.description) AGAINST ($search_str6 IN BOOLEAN MODE)))";
			//break;
		}
	}
}

$query .= $color_addition . $shape_addition . $material_addition . $quantity_addition . $brand_addition;
$query .= '
AND products.is_active = "Y" AND products.is_deleted = "N"	
GROUP BY product_vendor_sku
ORDER BY ' . $order;
	
//echo $query; 
//mail("rachel@tigerchef.com", "query", $query);

$products_ind = db_query_array($query);

// If only one result, take user directly to the product page
$total_num_prods = count($products_ind);

if ($total_num_prods == 1) 
{
	$url_for_single_result = Catalog::makeProductLink_($products_ind[0]);
	header("location:$url_for_single_result");
}   
    
include 'includes/header.php';

$reset_per_page = false;
if($per_page == 'All')
{  
	$per_page = count($products_ind);
	$reset_per_page = true;
}
 
$params = array(
	'totalItems' => count($products_ind),
    'perPage' => $per_page,
    'delta' => 2,             // for 'Jumping'-style a lower number is better
    'append' => true,
    'clearIfVoid' => false,
    'urlVar' => 'entrant',
    'useSessions' => false,
    'closeSession' => true,
    'mode'  => 'Sliding',
    'curPageLinkClassName' => 'active',
    'nextImg' => '<img src="/images/right-arrow.png" alt="Next" />',
    'prevImg' => '<img src="/images/left-arrow.png" alt="Prev" />',
	'separator' => '',
	'spacesAfterSeparator'=>0,
	'spacesBeforeSeparator'=>0         
);

$pager = Pager::factory($params);
$page_data = $pager->getPageData();
$links = $pager->getLinks();
$start = $pager->getOffsetByPageId();
$per_page = $pager->_perPage;
$start = $start[0]-1;

$force_no_results = false;
if (strpos($query_string, "dishwasher") !== false) $force_no_results = true;
?>            		
<div class="main-section"> 
	 <div class="category-heading">	 
	 <?php if (!$force_no_results) {?>
	 <h1><?=$title ?></h1>
			<h3><?=count($products_ind)?> Result<?=count($products_ind) != 1?'s': '' ?> Found</h3>
		<?php } ?>
			<?if (count($products_ind) == 0 || $force_no_results){?>
			<h3>We are sorry your search did not return any results.  Please try another search term.</h3>
			<?}
			else {
			if (!$_REQUEST['entrant'] && !$_REQUEST['sortby'] && !$_REQUEST['setPerPage'] && !$_REQUEST['per_page']) SearchTerms::insert(array('term'=>$original_query, 'date'=>date("Y-m-d H:i:s"),'results_found_exact'=>count($products_ind)));
			?>
			<div class="form-box"></div>
			<div class="head">
				<div class="pager">
				      <p class="amount">Items <?=$start + 1?> to <?=(count($products_ind) < $per_page+$start) ? count($products_ind): $per_page+$start ?> of <?=count($products_ind)?> total</p>
					 
					  <p class="pages">&nbsp;
					    <? if (count($products_ind) > $per_page) { ?>
					       Page:&nbsp;
						   <?=$links['back']?>
                	       <?=$links['pages']?>
                	       <?=$links['next']?>
                	    <? } ?>
					  </p>	
                      <p class="selectpage">
				      <form action="<?=$_SERVER['PHP_SELF']?>" method="GET" class="from sort-form" id="sort">
					  <label class="mobile-hidden" for="view-field">Show:</label>
						<select class="mobile-hidden" id="view-field" name="setPerPage" onchange="this.form.submit();">
                    <?
                            if( $reset_per_page ) $per_page = $_REQUEST['per_page'];
                    		$page_options = array('5','15','25','50', 'All');
                    		foreach( $page_options as $po )
                    		{
                    			if( $po == 'All' )
                    			{
								?>
									<option value="<?=count($products_ind)?>" <?=count($products_ind) == $pager->_perPage?'selected':''?>>All</option>
								<?
                		    	}
                    			else {
                                ?>
                                <option <?=$po == $pager->_perPage?'selected':''?>><?=$po?></option>
                                <?
		                    	}
        		            }                    
                    ?>
                    	</select> Per Page
						</p>
			    </div>
                <div class="sorter">				
				  <ul class="sort-list">
					<li class="grid"><a href="#" onClick="sendToDataLayer('Search Results', 'Grid View');setListOrGridView('grid'); return false;">grid</a></li>
					<li class="list"><a href="#" onClick="sendToDataLayer('Search Results', 'List View');setListOrGridView('list'); return false;">list</a></li>
				  </ul>
				  <input type="hidden" name="q" value="<?=$query_string?>">				  
				  <input type="hidden" name="sortby" value="<?=$_REQUEST['sortby']?>">
				  <input type="hidden" name="per_page" value="<?=$_REQUEST['per_page']?>">
				  <input type="hidden" name="entrant" value="<?=$_REQUEST['entrant']?>">
				  <div class="sorting">
						<span for="sort-field" >Sort By:</span>
						<ul>
						   <li><a onClick="$j('input[name=sortby]').val(4);$j('#sort').submit();"><span></span></a><span>Best Selling </span><a onClick="$j('input[name=sortby]').val(6);$j('#sort').submit();"><span class="desc"></span></a></li>
                           <li><a onClick="$j('input[name=sortby]').val(3);$j('#sort').submit();"><span></span></a><span>Name      </span><a onClick="$j('input[name=sortby]').val(5);$j('#sort').submit();"><span class="desc"></span></a></li>						   
						   <li><a onClick="$j('input[name=sortby]').val(1);$j('#sort').submit();"><span></span></a><span>Price        </span><a onClick="$j('input[name=sortby]').val(2);$j('#sort').submit();"><span class="desc"></span></a></li>
						</ul>   
					</div>
				</form>
				</div>
			</div>
		</div>
        <div class="category-container">
		  <div id="category-list" <?if ($_SESSION['list_style'] == "list") echo "class='list'"; else echo "class='grid'";?>>
<? 
        $query_with_limit = $query . " LIMIT $start, $per_page";		
		$products = db_query_array($query_with_limit);
    	if ($products)
    	{                  
	   		$show_compare = false;
	    	foreach( $products as &$p )
	    	{
			  $p['id']=$p['the_product_id'];	
			  $can_buy_info = Products::getCanBuyInfo($p, $CFG);
			  $p['checked_can_buy']=true;
              $p['can_buy'] = $can_buy_info['can_buy'];
              $p['can_buy_limit'] = $can_buy_info['can_buy_limit'];
			}			
			unset($p);
            $product_for_schema=array(); 
			foreach( $products as $a_prod )
        	{
				if ($a_prod['can_buy'])
				{				
					$p = Products::get1($a_prod['the_product_id']);
					$p['product_id'] = $p['id'];
					include( $CFG->redesign_dirroot . '/includes/listing_page_show_one_prod.php');
				}
        	}
			foreach( $products as $a_prod)
        	{
				if (!$a_prod['can_buy'])
				{
					$p = Products::get1($a_prod['the_product_id']);
					$p['product_id'] = $p['id'];
					include( $CFG->redesign_dirroot . '/includes/listing_page_show_one_prod.php');
				}	
        	}
    	}
?>									
		</div>
      </div>	
      <div class="category-heading">
			<div class="form-box"></div>
			<div class="head">
				<div class="pager">
				      <p class="amount">Items <?=$start + 1?> to <?=(count($products_ind) < $per_page+$start) ? count($products_ind): $per_page+$start ?> of <?=count($products_ind)?> total</p>					 
					  <p class="pages">&nbsp;
					    <? if (count($products_ind) > $per_page) { ?>
					       Page:&nbsp;
						   <?=$links['back']?>
                	       <?=$links['pages']?>
                	       <?=$links['next']?>
                	    <? } ?>
					  </p>	
                      <p class="selectpage">
				      <form action="<?=$_SERVER['PHP_SELF']?>" method="GET" class="from sort-form" id="sort">
					  <label class="mobile-hidden" for="view-field">Show:</label>
						<select class="mobile-hidden" id="view-field" name="setPerPage" onchange="this.form.submit();">
                    <?
                            if( $reset_per_page ) $per_page = $_REQUEST['per_page'];
                    		$page_options = array('5','15','25','50', 'All');
                    		foreach( $page_options as $po )
                    		{
                    			if( $po == 'All' )
                    			{
								?>
									<option value="<?=count($products_ind)?>" <?=count($products_ind) == $pager->_perPage?'selected':''?>>All</option>
								<?
                		    	}
                    			else {
                                ?>
                                <option <?=$po == $pager->_perPage?'selected':''?>><?=$po?></option>
                                <?
		                    	}
        		            }                    
                    ?>
                    	</select> Per Page
						</p>
			    </div>
                <div class="sorter">				
				  <ul class="sort-list">
					<li class="grid"><a href="#" onClick="sendToDataLayer('Search Results', 'Grid View');setListOrGridView('grid'); return false;">grid</a></li>
					<li class="list"><a href="#" onClick="sendToDataLayer('Search Results', 'List View');setListOrGridView('list'); return false;">list</a></li>
				  </ul>							
					<input type="hidden" name="q" value="<?=query_string?>">
				  <input type="hidden" name="sortby" value="<?=$_REQUEST['sortby']?>">
				  <input type="hidden" name="per_page" value="<?=$_REQUEST['per_page']?>">
				  <input type="hidden" name="entrant" value="<?=$_REQUEST['entrant']?>">
		    	
				  <div class="sorting">
						<span for="sort-field" >Sort By:</span>
						<ul>
						   <li><a onClick="$j('input[name=sortby]').val(4);$j('#sort').submit();"><span></span></a><span>Best Selling </span><a onClick="$j('input[name=sortby]').val(6);$j('#sort').submit();"><span class="desc"></span></a></li>
                           <li><a onClick="$j('input[name=sortby]').val(3);$j('#sort').submit();"><span></span></a><span>Name      </span><a onClick="$j('input[name=sortby]').val(5);$j('#sort').submit();"><span class="desc"></span></a></li>						   
						   <li><a onClick="$j('input[name=sortby]').val(1);$j('#sort').submit();"><span></span></a><span>Price        </span><a onClick="$j('input[name=sortby]').val(2);$j('#sort').submit();"><span class="desc"></span></a></li>
						</ul>   
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
    <? } 
    include 'includes/footer.php';
?>

<?php 
// +----------------------------------------------------------------------+
// | Akelos PHP Application Framework                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2002-2006, Akelos Media, S.L.  http://www.akelos.com/  |
// | Released under the GNU Lesser General Public License                 |
// +----------------------------------------------------------------------+
// | You should have received the following files along with this library |
// | - COPYRIGHT (Additional copyright notice)                            |
// | - DISCLAIMER (Disclaimer of warranty)                                |
// | - README (Important information regarding this library)              |
// +----------------------------------------------------------------------+

/**
* Inflector for pluralize and singularize English nouns.
*
* This Inflector is a port of Ruby on Rails Inflector.
*
* It can be really helpful for developers that want to
* create frameworks based on naming conventions rather than
* configurations.
*
* It was ported to PHP for the Akelos Framework, a
* multilingual Ruby on Rails like framework for PHP that will
* be launched soon.
*
* @author Bermi Ferrer Martinez 
* @copyright Copyright (c) 2002-2006, Akelos Media, S.L. http://www.akelos.org
* @license GNU Lesser General Public License 
* @since 0.1
* @version $Revision 0.1 $
*/
class Inflector
{
    // ------ CLASS METHODS ------ //

    // ---- Public methods ---- //

    // {{{ pluralize()

    /**
    * Pluralizes English nouns.
    *
    * @access public
    * @static
    * @param    string    $word    English noun to pluralize
    * @return string Plural noun
    */
    function pluralize($word)
    {
        $plural = array(
        '/(quiz)$/i' => '1zes',
        '/^(ox)$/i' => '1en',
        '/([m|l])ouse$/i' => '1ice',
        '/(matr|vert|ind)ix|ex$/i' => '1ices',
        '/(x|ch|ss|sh)$/i' => '1es',
        '/([^aeiouy]|qu)ies$/i' => '1y',
        '/([^aeiouy]|qu)y$/i' => '1ies',
        '/(hive)$/i' => '1s',
        '/(?:([^f])fe|([lr])f)$/i' => '12ves',
        '/sis$/i' => 'ses',
        '/([ti])um$/i' => '1a',
        '/(buffal|tomat)o$/i' => '1oes',
        '/(bu)s$/i' => '1ses',
        '/(alias|status)/i'=> '1es',
        '/(octop|vir)us$/i'=> '1i',
        '/(ax|test)is$/i'=> '1es',
        '/s$/i'=> 's',
        '/$/'=> 's');

        $uncountable = array('equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep');

        $irregular = array(
        'person' => 'people',
        'man' => 'men',
        'child' => 'children',
        'sex' => 'sexes',
        'move' => 'moves');

        $lowercased_word = strtolower($word);

        foreach ($uncountable as $_uncountable){
            if(substr($lowercased_word,(-1*strlen($_uncountable))) == $_uncountable){
                return $word;
            }
        }

        foreach ($irregular as $_plural=> $_singular){
            if (preg_match('/('.$_plural.')$/i', $word, $arr)) {
                return preg_replace('/('.$_plural.')$/i', substr($arr[0],0,1).substr($_singular,1), $word);
            }
        }

        foreach ($plural as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                return preg_replace($rule, $replacement, $word);
            }
        }
        return false;

    }

    // }}}
    // {{{ singularize()

    /**
    * Singularizes English nouns.
    *
    * @access public
    * @static
    * @param    string    $word    English noun to singularize
    * @return string Singular noun.
    */
    function singularize($word)
    {
        $singular = array (
        '/(quiz)zes$/i' => '\1',
        '/(matr)ices$/i' => '\1ix',
        '/(vert|ind)ices$/i' => '\1ex',
        '/^(ox)en/i' => '\1',
        '/(alias|status)es$/i' => '\1',
        '/([octop|vir])i$/i' => '\1us',
        '/(cris|ax|test)es$/i' => '\1is',
        '/(shoe)s$/i' => '\1',
        '/(o)es$/i' => '\1',
        '/(bus)es$/i' => '\1',
        '/([m|l])ice$/i' => '\1ouse',
        '/(x|ch|ss|sh)es$/i' => '\1',
        '/(m)ovies$/i' => '\1ovie',
        '/(s)eries$/i' => '\1eries',
        '/([^aeiouy]|qu)ies$/i' => '\1y',
        '/([lr])ves$/i' => '\1f',
        '/(tive)s$/i' => '\1',
        '/(hive)s$/i' => '\1',
        '/([^f])ves$/i' => '\1fe',
        '/(^analy)ses$/i' => '\1sis',
        '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
        '/([ti])a$/i' => '\1um',
        '/(n)ews$/i' => '\1ews',
        '/ss$/i' => 'ss',
        '/s$/i' => '',
        );

        $uncountable = array('equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep');

        $irregular = array(
        'person' => 'people',
        'man' => 'men',
        'child' => 'children',
        'sex' => 'sexes',
        'move' => 'moves');

        $lowercased_word = strtolower($word);
        foreach ($uncountable as $_uncountable){
            if(substr($lowercased_word,(-1*strlen($_uncountable))) == $_uncountable){
                return $word;
            }
        }

        foreach ($irregular as $_plural=> $_singular){
            if (preg_match('/('.$_singular.')$/i', $word, $arr)) {
                return preg_replace('/('.$_singular.')$/i', substr($arr[0],0,1).substr($_plural,1), $word);
            }
        }

        foreach ($singular as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                return preg_replace($rule, $replacement, $word);
            }
        }

        return $word;
    }

    // }}}
    // {{{ titleize()

    /**
    * Converts an underscored or CamelCase word into a English
    * sentence.
    *
    * The titleize function converts text like "WelcomePage",
    * "welcome_page" or  "welcome page" to this "Welcome
    * Page".
    * If second parameter is set to 'first' it will only
    * capitalize the first character of the title.
    *
    * @access public
    * @static
    * @param    string    $word    Word to format as tile
    * @param    string    $uppercase    If set to 'first' it will only uppercase the
    * first character. Otherwise it will uppercase all
    * the words in the title.
    * @return string Text formatted as title
    */
    function titleize($word, $uppercase = '')
    {
        $uppercase = $uppercase == 'first' ? 'ucfirst' : 'ucwords';
        return $uppercase(Inflector::humanize(Inflector::underscore($word)));
    }

    // }}}
    // {{{ camelize()

    /**
    * Returns given word as CamelCased
    *
    * Converts a word like "send_email" to "SendEmail". It
    * will remove non alphanumeric character from the word, so
    * "who's online" will be converted to "WhoSOnline"
    *
    * @access public
    * @static
    * @see variablize
    * @param    string    $word    Word to convert to camel case
    * @return string UpperCamelCasedWord
    */
    function camelize($word)
    {
        return str_replace(' ','',ucwords(preg_replace('/[^A-Z^a-z^0-9]+/',' ',$word)));
    }

    // }}}
    // {{{ underscore()

    /**
    * Converts a word "into_it_s_underscored_version"
    *
    * Convert any "CamelCased" or "ordinary Word" into an
    * "underscored_word".
    *
    * This can be really useful for creating friendly URLs.
    *
    * @access public
    * @static
    * @param    string    $word    Word to underscore
    * @return string Underscored word
    */
    function underscore($word)
    {
        return  strtolower(preg_replace('/[^A-Z^a-z^0-9]+/','_',
        preg_replace('/([a-zd])([A-Z])/','1_2',
        preg_replace('/([A-Z]+)([A-Z][a-z])/','1_2',$word))));
    }

    // }}}
    // {{{ humanize()

    /**
    * Returns a human-readable string from $word
    *
    * Returns a human-readable string from $word, by replacing
    * underscores with a space, and by upper-casing the initial
    * character by default.
    *
    * If you need to uppercase all the words you just have to
    * pass 'all' as a second parameter.
    *
    * @access public
    * @static
    * @param    string    $word    String to "humanize"
    * @param    string    $uppercase    If set to 'all' it will uppercase all the words
    * instead of just the first one.
    * @return string Human-readable word
    */
    function humanize($word, $uppercase = '')
    {
        $uppercase = $uppercase == 'all' ? 'ucwords' : 'ucfirst';
        return $uppercase(str_replace('_',' ',preg_replace('/_id$/', '',$word)));
    }

    // }}}
    // {{{ variablize()

    /**
    * Same as camelize but first char is underscored
    *
    * Converts a word like "send_email" to "sendEmail". It
    * will remove non alphanumeric character from the word, so
    * "who's online" will be converted to "whoSOnline"
    *
    * @access public
    * @static
    * @see camelize
    * @param    string    $word    Word to lowerCamelCase
    * @return string Returns a lowerCamelCasedWord
    */
    function variablize($word)
    {
        $word = Inflector::camelize($word);
        return strtolower($word[0]).substr($word,1);
    }

    // }}}
    // {{{ tableize()

    /**
    * Converts a class name to its table name according to rails
    * naming conventions.
    *
    * Converts "Person" to "people"
    *
    * @access public
    * @static
    * @see classify
    * @param    string    $class_name    Class name for getting related table_name.
    * @return string plural_table_name
    */
    function tableize($class_name)
    {
        return Inflector::pluralize(Inflector::underscore($class_name));
    }

    // }}}
    // {{{ classify()

    /**
    * Converts a table name to its class name according to rails
    * naming conventions.
    *
    * Converts "people" to "Person"
    *
    * @access public
    * @static
    * @see tableize
    * @param    string    $table_name    Table name for getting related ClassName.
    * @return string SingularClassName
    */
    function classify($table_name)
    {
        return Inflector::camelize(Inflector::singularize($table_name));
    }

    // }}}
    // {{{ ordinalize()

    /**
    * Converts number to its ordinal English form.
    *
    * This method converts 13 to 13th, 2 to 2nd ...
    *
    * @access public
    * @static
    * @param    integer    $number    Number to get its ordinal value
    * @return string Ordinal representation of given string.
    */
    function ordinalize($number)
    {
        if (in_array(($number % 100),range(11,13))){
            return $number.'th';
        }else{
            switch (($number % 10)) {
                case 1:
                return $number.'st';
                break;
                case 2:
                return $number.'nd';
                break;
                case 3:
                return $number.'rd';
                default:
                return $number.'th';
                break;
            }
        }
    }

    // }}}

}
?>
