<?php
require_once "application.php";
		global $CFG;
	
		if( !$filter )
        {
	         include('404.php'); 
	         exit(); 
        }
	$value=mysql_real_escape_string($_REQUEST['value']);
    $info[$filter['name']]=$value;
	if (strtoupper($value)=='ALL' || !$value){
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$CFG->baseurl."products.html");
		exit();
	}	
	
	$extra_info=Filters::getAllValues('', $value, $filter['name']);
	
	$title=$extra_info[0]['title_tag'];
	$this_page_type = 'filter';
	$description=$extra_info[0]['meta_desc'];
	
	include 'includes/header.php';
    

	
	
	$order ='';
    $order_asc='';

	switch( $_REQUEST['sortby'] )
	{
		case 1:
			$order ='products.price';
			$order_asc =true;			
			break;
		case 2:
			$order = 'products.price';
			$order_asc = false;
			break;
		case 3:
			$order = 'products.name';
			$order_asc = true;
			break;
		case 5:
			$order = 'products.name';
			$order_asc = false;
			break;
		case 6:
			$order = 'products.priority ASC,best_selling_products.num_orders ASC, products.price ASC ';
		
		case 4:
		default:
		    $order = 'products.priority DESC,best_selling_products.num_orders DESC, products.price ASC ';
	}
	
	
	if ($_REQUEST['per_page'] != "") 
    {
	    $urlParams .= "&per_page=".$_REQUEST['per_page'];
	    $extraVarsArray['per_page'] = $_REQUEST['per_page'];
    }
    
	if ($_REQUEST['setPerPage'] != "") 
   {
	   $urlParams .= "&per_page=".$_REQUEST['setPerPage'];
	   $extraVarsArray['per_page'] = $_REQUEST['setPerPage'];
	   $_REQUEST['per_page'] = $_REQUEST['setPerPage']; 						            		            		
    }
	
	if (substr($urlParams,0,1) == "&") $urlParams = substr_replace($urlParams, '?', 0, 1);
    
	if( $_REQUEST['per_page'] ) $per_page = $_REQUEST['per_page'];
    else $per_page = 25;
    
	$products_ind=Products::getproductsforfilterCat('', '','','', $info,true);
	
   $available_brands=array();
   $price_buckets = array();            
   $prices_array = array();
   $lengths_array = array();
   $widths_array = array();
   $heights_array = array();
   $item_ids = array();

	foreach($products_ind as $prod)
{
	
   	$the_id = $prod['brand_id'];
   	if (!array_key_exists($the_id, $available_brands)) 
	{            		
    	$available_brands[$the_id]['name'] = $prod['brand_name'];
    	$available_brands[$the_id]['recommended_vendor']=$prod['is_recommended'];              		
    }
    
    $available_brands[$the_id]['num_prods']++;
				
    $the_length = $prod['length'];
    if ($the_length != 0 && $the_length != "N/A")
    {
    	if (!array_key_exists($the_length, $lengths_array)) 
        {            		
        	$lengths_array[$the_length]['display_length'] = $prod['display_length'];            		
        }
        $lengths_array[$the_length]['num_prods']++;
    }

   	$the_width = $prod['width'];
   	if ($the_width != 0 && $the_width != "N/A")
    {
    	if (!array_key_exists($the_width, $widths_array)) 
    	{            		
    		$widths_array[$the_width]['display_width'] = $prod['display_width'];            		
        }
        $widths_array[$the_width]['num_prods']++;
    }
    $the_height = $prod['height'];
    if ($the_height != 0 && $the_height != "N/A")
    {
    	if (!array_key_exists($the_height, $heights_array)) 
    	{            		
        	$heights_array[$the_height]['display_height'] = $prod['display_height'];            		
        }
        $heights_array[$the_height]['num_prods']++;
    }            	
           	
    $prices_array[] = $prod['price'];
    $item_ids[] = $prod['product_id'];

}   



    
	$reset_per_page = false;
    if($per_page == 'All')
   {  
 	$per_page = count($products_ind);
	$reset_per_page = true;
   }

   $extraVarsArray['value']=$value;
  $params = array(
	'totalItems' => count($products_ind),
    'perPage' => $per_page,
    'delta' => 2,             // for 'Jumping'-style a lower number is better
    'append' => true,
    'clearIfVoid' => false,
    'urlVar' => 'entrant',
    'useSessions' => false,
    'closeSession' => true,
    'mode'  => 'Sliding',
    'curPageLinkClassName' => 'active',
    'nextImg' => '<img src="/images/right-arrow.png" alt="Next" />',
    'prevImg' => '<img src="/images/left-arrow.png" alt="Prev" />',
    'extraVars' => $extraVarsArray,
	'separator' => '',
	/*'separator' => '</li><li>',
    'firstPagePre'=>'<li>',
	'lastPagePost'=>'</li>',*/	
	'spacesAfterSeparator'=>0,
	'spacesBeforeSeparator'=>0         
);

     $pager = Pager::factory($params);
     $page_data = $pager->getPageData();
     $links = $pager->getLinks();
	 $start = $pager->getOffsetByPageId();
     $per_page = $pager->_perPage;
     $start = $start[0]-1;
	 
?>
	<div class="main-section"> 
	 <div class="category-heading">
			<div class="form-box"></div>
			<div class="head">
				<div class="pager">
                    <div class="description"><?php echo $extra_info[0]['description']?> </div>
				      <p class="amount">Items <?=$start + 1?> to <?=(count($products_ind) < $per_page+$start) ? count($products_ind): $per_page+$start ?> of <?=count($products_ind)?> total</p>
					 
					  <p class="pages">&nbsp;
					    <? if (count($products_ind) > $per_page) { ?>
					       Page:&nbsp;
						   <?=$links['back']?>
                	       <?=$links['pages']?>
                	       <?=$links['next']?>
                	    <? } ?>
					  </p>	
                      <p class="selectpage">
				      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="from sort-form" id="sort">
					  <label class="mobile-hidden" for="view-field">Show:</label>
						<select class="mobile-hidden" id="view-field" name="setPerPage" onchange="this.form.submit();">
                    <?
                            if( $reset_per_page ) $per_page = $_REQUEST['per_page'];
                    		$page_options = array('5','15','25','50', 'All');
                    		foreach( $page_options as $po )
                    		{
                    			if( $po == 'All' )
                    			{
								?>
									<option value="<?=count($products_ind)?>" <?=count($products_ind) == $pager->_perPage?'selected':''?>>All</option>
								<?
                		    	}
                    			else {
                                ?>
                                <option <?=$po == $pager->_perPage?'selected':''?>><?=$po?></option>
                                <?
		                    	}
        		            }                    
                    ?>
                    	</select> Per Page
						</p>
			    </div>
                <div class="sorter">				
				  <ul class="sort-list">
					<li class="grid"><a href="#" onClick="sendToDataLayer('Category Lower', 'Grid View');setListOrGridView('grid'); return false;">grid</a></li>
					<li class="list"><a href="#" onClick="sendToDataLayer('Category Lower', 'List View');setListOrGridView('list'); return false;">list</a></li>
				  </ul>
				
				
				  <input type="hidden" name="item_keyword" value="<?=$_REQUEST['item_keyword']?>">
				  <input type="hidden" name="value" value="<?=$_REQUEST['value']?>">
				  <input type="hidden" name="sortby" value="<?=$_REQUEST['sortby']?>">
				  <input type="hidden" name="per_page" value="<?=$_REQUEST['per_page']?>">
				  <input type="hidden" name="entrant" value="<?=$_REQUEST['entrant']?>">
		    	
				  <div class="sorting">
						<span for="sort-field" >Sort By:</span>
						<ul>
						   <li><a onClick="$j('input[name=sortby]').val(4);$j('#sort').submit();"><span></span></a><span>Best Selling </span><a onClick="$j('input[name=sortby]').val(6);$j('#sort').submit();"><span class="desc"></span></a></li>
                           <li><a onClick="$j('input[name=sortby]').val(3);$j('#sort').submit();"><span></span></a><span>Name      </span><a onClick="$j('input[name=sortby]').val(5);$j('#sort').submit();"><span class="desc"></span></a></li>						   
						   <li><a onClick="$j('input[name=sortby]').val(1);$j('#sort').submit();"><span></span></a><span>Price        </span><a onClick="$j('input[name=sortby]').val(2);$j('#sort').submit();"><span class="desc"></span></a></li>
						</ul>   
					</div>
				</form>
				</div>
			</div>
		</div>
        <div class="category-container">
		  <div id="category-list" <?if ($_SESSION['list_style'] == "list") echo "class='list'"; else echo "class='grid'";?>>
        <? 
		   $products=Products::getproductsforfilterCat('', '',0,0, $info,false, 0,$start,$per_page,$order, $order_asc);
	       if ($products)
    	{                  
	       
	   	   /* if( !Products::shouldTheItemsHaveCompare($item_ids) ) $show_compare = false;
       		else $show_compare = true; */
			$show_compare = false;

            foreach( $products as &$p ){
			  $p['id']=$p['product_id'];	
			  $can_buy_info = Products::getCanBuyInfo($p, $CFG);
			  $p['checked_can_buy']=true;
              $p['can_buy'] = $can_buy_info['can_buy'];
              $p['can_buy_limit'] = $can_buy_info['can_buy_limit'];
			}			
			unset($p);
            $product_for_schema=array(); 
			foreach( $products as $p )
        	{
				if ($p['can_buy'])
				include( $CFG->redesign_dirroot . '/includes/listing_page_show_one_prod.php');
        	}
			
			foreach( $products as $p )
        	{
				if (!$p['can_buy'])
				include( $CFG->redesign_dirroot . '/includes/listing_page_show_one_prod.php');
        	}
		}
		
		else  //no products
            echo "<h3>No ".$value." items found. </h3>";?>	
	      
		</div>
      </div>	
      <div class="category-heading">
			<div class="form-box"></div>
			<div class="head">
				<div class="pager">
				      <p class="amount">Items <?=$start + 1?> to <?=(count($products_ind) < $per_page+$start) ? count($products_ind): $per_page+$start ?> of <?=count($products_ind)?> total</p>
					 
					  <p class="pages">&nbsp;
					    <? if (count($products_ind) > $per_page) { ?>
					       Page:&nbsp;
						   <?=$links['back']?>
                	       <?=$links['pages']?>
                	       <?=$links['next']?>
                	    <? } ?>
					  </p>	
                      <p class="selectpage">
				      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="from sort-form" id="sort">
					  <label class="mobile-hidden" for="view-field">Show:</label>
						<select class="mobile-hidden" id="view-field" name="setPerPage" onchange="this.form.submit();">
                    <?
                            if( $reset_per_page ) $per_page = $_REQUEST['per_page'];
                    		$page_options = array('5','15','25','50', 'All');
                    		foreach( $page_options as $po )
                    		{
                    			if( $po == 'All' )
                    			{
								?>
									<option value="<?=count($products_ind)?>" <?=count($products_ind) == $pager->_perPage?'selected':''?>>All</option>
								<?
                		    	}
                    			else {
                                ?>
                                <option <?=$po == $pager->_perPage?'selected':''?>><?=$po?></option>
                                <?
		                    	}
        		            }                    
                    ?>
                    	</select> Per Page
						</p>
			    </div>
                <div class="sorter">				
				  <ul class="sort-list">
					<li class="grid"><a href="#" onClick="sendToDataLayer('Category Lower', 'Grid View');setListOrGridView('grid'); return false;">grid</a></li>
					<li class="list"><a href="#" onClick="sendToDataLayer('Category Lower', 'List View');setListOrGridView('list'); return false;">list</a></li>
				  </ul>
				
				
				  <input type="hidden" name="item_keyword" value="<?=$_REQUEST['item_keyword']?>">
				  <input type="hidden" name="value" value="<?=$_REQUEST['value']?>">
				  <input type="hidden" name="sortby" value="<?=$_REQUEST['sortby']?>">
				  <input type="hidden" name="per_page" value="<?=$_REQUEST['per_page']?>">
				  <input type="hidden" name="entrant" value="<?=$_REQUEST['entrant']?>">
		    	
				  <div class="sorting">
						<span for="sort-field" >Sort By:</span>
						<ul>
						   <li><a onClick="$j('input[name=sortby]').val(4);$j('#sort').submit();"><span></span></a><span>Best Selling </span><a onClick="$j('input[name=sortby]').val(6);$j('#sort').submit();"><span class="desc"></span></a></li>
                           <li><a onClick="$j('input[name=sortby]').val(3);$j('#sort').submit();"><span></span></a><span>Name      </span><a onClick="$j('input[name=sortby]').val(5);$j('#sort').submit();"><span class="desc"></span></a></li>						   
						   <li><a onClick="$j('input[name=sortby]').val(1);$j('#sort').submit();"><span></span></a><span>Price        </span><a onClick="$j('input[name=sortby]').val(2);$j('#sort').submit();"><span class="desc"></span></a></li>
						</ul>   
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
    <?include 'includes/footer.php';
	if (!$links['next']) 
	 {?>
	  <script>
		$j("#nextpage").remove();
	  </script>
	 <?}else {?>
	     <script>
		   var has_next=document.querySelector('link[rel="next"]');
		   if(!has_next)
		     {
			    var link = document.createElement('link');
                link.rel = 'next';
				link.id = 'nextpage';
                link.href = '<?=$nextlink?>';
                document.head.appendChild(link);
		     }
		 </script>
	 <?}?>
      
   