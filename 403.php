<?php

$page_id = '403';
header('HTTP/1.1 403 Forbidden');

require_once $_SERVER['DOCUMENT_ROOT'] . "/application.php";
?>
<div style="text-align:center;">
	<br/><br/><br/>
	<a href="<?=$CFG->baseurl?>"><div style=""><img src="<?= $CFG->base_url . '/images/logo_bg.png'?>"></div></a><br/><br/>
	<div>
        You are seeing this page because you indicated that you do not agree to the use of cookies for analytics and personalized content as specified in our privacy policy.
		<br/><br/> Feel free to reach out to us at <?=$CFG->company_phone?>.
    </div>
</div>
