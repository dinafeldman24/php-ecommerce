<?php
include_once ('application.php');

if (! $_SESSION ['ORDER_ID']) {
	redirectClean ( $CFG->baseurl );
}
/* make invoice */
$invoice = Orders::makeInvoiceForDisplayRedesign ( $_SESSION ['ORDER_ID'], false, true );
$email_invoice = Orders::makeInvoiceForEmail ( $_SESSION ['ORDER_ID'], false, true );
$order = Orders::get1 ( $_SESSION ['ORDER_ID'] );
//$customer = Customers::get1 ( $order ['customer_id'] );
$order_id = $order ['id'];
$order_items = Orders::getItems ( 0, $order_id );
if (! $CFG->in_testing) {
	$order_profit_not_counting_shipping = Orders::getProfit($order_id, $CFG, true);
	ob_start(); 
	$order_promo_codes = Orders::getPromoCodes($order['id']);
		if ($order_promo_codes)
		{
			$opc_array = array();
			foreach ($order_promo_codes as $opc)
			{
				$opc_array[] = $opc['promo_code_code'];
			}
			$opc_string = implode(",", $opc_array);
		}
		else $opc_string = "";
	?>

<script>

    function checkIsSent(e) {
        var t = "gaSaleSent_" + e;
        var n = document.cookie.split(";");
        for (var r = 0; r < n.length; r++) {
            var i = n[r];
            while (i.charAt(0) == " ") i = i.substring(1);
            if (i.indexOf(t + "=") != -1) {
                return true
            }
        }
        var s = new Date;
        s.setTime(s.getTime() + 90 * 24 * 60 * 60 * 1e3);
        var o = "expires=" + s.toGMTString();
        document.cookie = t + "=1; " + o;
        return false
    }
    window.dataLayer = window.dataLayer || [];
 
    if (checkIsSent('<?=$_SESSION [ORDER_ID]?>')) {
        dataLayer.push({
              'duplicateTransaction' : 'true'
        });
    } else {
 
    	gtag('event', 'purchase', {
  		  "transaction_id": "<?=$order['id']?>",
  		  "affiliation": "Online Store",
  		  "value": <?=number_format($order['subtotal'],2, '.', '')?>,
  		  "currency": "USD",
  		  "tax": <?=number_format($order['order_tax'],2, '.', '')?>,
  		  "shipping": <?=number_format($order['shipping'],2, '.', '')?>,
    	  'coupon': <?=number_format($order['promo_discount'] + $order['shipping_discount'],2)?>,
  		  "items": [
<?$i=0;                   
foreach ($order_items as $item){
	$product              = Products::get1($item['product_id']);
	$product_sku          = $product['mfr_part_num'];
    $product_name         = $product['name'];
    ?>
      {
        'name': '<? echo htmlspecialchars(str_replace("'","",$product_name), ENT_QUOTES)?>',     // Name or ID is required.
		'id': '<?=$item['product_id']?>',
		'price': '<?=$item['price']?>',
		'quantity': '<?=$item['qty']?>',
        'category': '<?=str_ireplace(array('"','/'),'',$item['cat_name'])?>',
        'brand': '<?=str_ireplace(array('"','/'),'',$item['brand_name'])?>',
        'list_position':<?=$i+1?>
	  }
   <?
	$i++;
	if ($i<count($order_items)) echo ",";
	}
	?>
	]
    	});
        // bing
        window.uetq = window.uetq || [];
        window.uetq.push({
            'ec': 'conversion',
            'gv': '<?=number_format($order['subtotal'], 2, '.', '')?>'
        });
    
    }

</script>
	
<?
	$google_tag_mgr_data_layer = ob_get_contents();
    ob_end_clean(); 
}
$this_page_type = "invoice";
include_once ($CFG->redesign_dirroot . '/includes/header.php');
?>
<div class="invoice">
	<div class="main-section">
		<h1 class="invoice">Thank you for your order!</h1>
				<div class="invoice-subh">An e-mail containing your receipt has been sent to <?=$order['email']?>, Order Number <strong><?=$order_id?></strong></div>
	
		<div class="invoice-top"></div>
		<?
		echo $invoice;
		?>
	
		
		<?php
		if ($CFG->holiday_message) {
			?>
			<div class="holiday_msg">
				<?=$CFG->holiday_message?>
			</div>
			<?
		}
		?>
	</div>
</div>
<?

// email invoice is in apps/checkout.php

if (! $CFG->in_testing) {
	$order_items = Orders::getItems ( 0, $order_id );
	?>			
	
<!-- Google Code for Online Sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1002097823;
var google_conversion_label = "RZgLCJHDuwQQn5nr3QM";
var google_conversion_value = <?=number_format($order['subtotal'],2,'.','' )?>;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1002097823/?value=<?=number_format($order['subtotal'],2,'.','' )?>&amp;label=RZgLCJHDuwQQn5nr3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?
//include ($CFG->redesign_dirroot . '/includes/tracking_pixel_include.php');
}

/*if (! $CFG->in_testing) {	
   $mailchimp= new tcMailchimp();
   $order_info=array();
   $order_info['id']=$order['id'];
   if (isset($_COOKIE['mc_cid'])) 
        $order_info['campaign_id']=$_COOKIE['mc_cid'];
	if (isset($_COOKIE['mc_eid'])) 
        $order_info['email_id']=$_COOKIE['mc_eid'];
   $order_info['email']=$order['email'];
   $order_info['total']=$order['order_total'];
   $order_info['order_date']=$order['date'];
   $order_info['shipping']=$order['shipping'];
   $order_info['tax']=$order['order_tax'];
   $order_info['store_id']=$order['sales_account_id'];
   $order_info['store_id']=1;
   $order_info['store_name']='Tigerchef';
   $order_info['items']=array();
   $i=1;
   foreach ($order_items as $item){
	 $this_item=array();
	 $this_item['line_num']=$i;
     $this_item['product_id']=$item['product_id'];
	 $product = Products::get1($item['product_id']);
	 $this_item['product_name']=$product['name'];
	 $this_item['category_name']=str_ireplace(array('"','/'),'',$item['cat_name']);
	 $cat=Cats::get(0,$this_item['categroy_name']);
	 $this_item['product_category_id']=$cat[0]['id'];
	 $this_item['category_id']=$cat[0]['id'];
     $this_item['qty']=$item['qty'];
	 $this_item['cost']=$item['price'];
	 $i++;
	 $order_info['items'][]=$this_item;
   }	
    $result=$mailchimp->ecommOrderAdd($order_info);
	setcookie("mc_cid", "", time()-3600);
	setcookie("mc_eid", "", time()-3600);
}
*/
	
include_once ($CFG->redesign_dirroot . '/includes/footer.php');
?>