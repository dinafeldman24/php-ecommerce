<?php

include_once('application.php');
$action = $_REQUEST['action'];
$tax_rate = 0.00;
$sales_tax      = 0.00;
$shipping_cost  = 0.00;
$shipping_discount = 0.00;
$sub_total      = 0.00;
$grand_total    = 0.00;
$coupon_gift    = 0.00;
$liftgate   	= 0.00;
$promoCodeError  = "";
$essensaMessage = "";
$adjusted_rewards_message = "";
$addError	= "";
$applied_coupons = null;
$coupon_gift_arr = null;
$checkout_obj = new Checkout();
$this_page_type = 'cart';
if( $_REQUEST['add_error'] )
    $addError = urldecode( $_REQUEST['add_error'] );

$min_charge = 0;

function update_cart()
{
    global $cart, $addError;
	global $promoCodeError;
	
    if ($_REQUEST['cart'])
    { 
        $addError = $cart->update($_REQUEST['cart']);
        return true;
    }

    return false;
}

function remove_from_cart()
{
    global $cart;

    if ($_REQUEST['item'])
    {
        $cart->delete($_REQUEST['item']);
        return true;
    }
    else
    {
        return false;
    }
}

function add_to_cart()
{
    global $cart, $addError;

    if (isset($_REQUEST['product_id']) && isset($_REQUEST['qty']))
    {
        $error = $cart->add($_REQUEST['product_id'], $_REQUEST['qty'], $_REQUEST['options']);
	if( !is_numeric($error) )
	    $addError = $error;
        return true;
    }

    return false;
}

/**
 * @deprecated use Checkout::make_continue_shopping_link() instead
 * @return string|unknown_type
 */
function make_continue_shopping_link()
{
    global $CFG;
    $product    = null;

    if ($_REQUEST['product_id'] != '')
    {
        $product = Products::get1($_REQUEST['product_id']);
        if ($product)
        {
            return Catalog::makeProductLink_($product);
        }
    }
    else if ($_SESSION['prev_prod_page_link'] != "" && $_SESSION['prev_prod_page_link'] != '/cart.php' && $_SESSION['prev_prod_page_link'] != 'checkout.php') return $_SESSION['prev_prod_page_link'];

//	$referer = $_SERVER['HTTP_REFERER'];
//	if ($referer != '') return $referer; 
//	else return "javascript:history.go(-1)"; 
	else return "/";
}

function build_shipping_method_dropdown()
{
	global $cart;
	global $shipping_discount;
	global $tax_rate;
	global $checkout_obj;

    $tax_rate = $checkout_obj->tax_rate;
	echo $cart->build_shipping_method_dropdown($shipping_discount,$tax_rate,$cart->data['zipcode'],true,$checkout_obj->shipping_cost_options);
	
}

function remove_coupon($id){
	global $cart;

	$cart->deletePromoCode((int)$id);
}

function calculate_sales_tax_and_shipping()
{
    global $tax_rate;
    global $sales_tax;
    global $shipping_cost;
    global $cart;
    global $shipping_discount;
    global $coupon_gift;
	global $coupon_gift_arr;    
    global $liftgate;
	global $checkout_obj;

    if(strlen($_REQUEST['zip_code'])>4){
        $zip = $_REQUEST['zip_code'];
    } else if($cart->data['zipcode']){
        $zip = $cart->data['zipcode'];
    }
        
    $cart->updateFreight();
    $checkout_obj->calculate_coupon_gift();

    if ($zip && $zip != 'Enter Zip Code' && ZipCodesUSA::is_zip_code_valid($zip))
    {
    	$cart->data['zipcode'] = htmlentities_utf($zip);
    	$checkout_obj->cart2['shipping_zip'] = $cart->data['zipcode'];
    	
    	$checkout_obj->calculate_sales_tax_and_shipping(); 
    }
    
    $tax_rate = $checkout_obj->tax_rate;
    $sales_tax = $checkout_obj->sales_tax;
    $shipping_cost = $checkout_obj->shipping_cost;
    $shipping_discount = $checkout_obj->shipping_discount;
    $coupon_gift = $checkout_obj->coupon_gift;
    $coupon_gift_arr = $checkout_obj->coupon_gift_arr;
 
    $liftgate = Shipping::calcLiftgateFee('','cart');
    
    return ;
}

function calculate_grand_total()
{
    global $cart, $coupon_gift;
    global $sales_tax;
    global $shipping_cost;
    global $sub_total;
    global $grand_total;
    global $min_charge;
    global $liftgate;
    
    global $adjusted_rewards_message;
        
    $liftgate_charge = $cart->data['charge_liftgate_fee'] ? $liftgate : 0.0 ;
    
    $adjusted_rewards_message = '';
    if($cart->data['rewards_using']>($sub_total-$coupon_gift)){
    	$cart->data['rewards_using'] = round($sub_total-$coupon_gift,2) ;
    	$adjusted_rewards_message = 'Your rewards have been adjusted to the maximum redeemable rewards.';
    }
    
    $min_charge = $cart->calcMinCharges();

    $grand_total  = ($sub_total + $sales_tax + $shipping_cost + $liftgate_charge) - $coupon_gift + $min_charge - $cart->data['rewards_using'];

    if( $grand_total < 0 )
	$grand_total = 0;

    return ;
}

function calculate_sub_total()
{
    global $cart;
    global $sub_total;

    $sub_total = $cart->getCartTotal();
}

function calculate_all()
{
	calculate_sales_tax_and_shipping();
    calculate_sub_total();
    calculate_grand_total();
}

function display_cart_items()
{
    global $cart,$CFG;

    $items = $cart->get();

    $show_title = true;
    $this_option = $this_product = $prev_option = $prev_product = $prev_cart_item = $prev_b_type = "";
	$b_reg_function = array($cart, "showProductLineB_charger");
	$b_case_function = array($cart, "showProductLineBCase_charger");
 	$a_function = array($cart, "showProductLineA_charger");        		
		
 	?>
 	<div class="cart-items-section">
 	<?php 	
 		include $CFG->dirroot."/includes/case_cart_include.php";
    ?>
    	</ul>
    </div>
    <? 
}

function get_cost_summary(){

	global $CFG;
	global $cart;
	global $coupon_gift;
	global $sales_tax;
	global $shipping_cost;
	global $sub_total;
	global $grand_total;
	global $min_charge;
	global $liftgate;
	global $tax_rate;
	global $shipping_discount;
	global $applied_coupons;
	global $checkout_obj;

	/*$vars = new object();
	$vars->sub_total = $sub_total;
	$vars->min_charge = $min_charge;
	$vars->shipping_cost = $shipping_cost;
	$vars->liftgate = $cart->data['charge_liftgate_fee'] ? $liftgate : 0.0 ;
	$vars->coupon_gift = $coupon_gift;
	$vars->sales_tax = $sales_tax;
	$vars->grand_total = $grand_total;
	$vars->tax_rate = $tax_rate;
	$vars->shipping_discount = $shipping_discount;
	*/
	?>
	<input type='hidden' id='current_zipcode' value='<?php echo $_SESSION['cart_data']['zipcode'];?>'/>
	<div class="cost-summary-wrapper cost-summary-wrapper-v2">
		
		<div class="bottom-section-cart">
		<div class="bottom-cart-box">			
		<h3 class="coupon acc-header">Discount Codes</h3>
		<div class="acc-sub">
						<fieldset>
								<div class="holder">
								Enter your coupon code if you have one.
									<div class="frame">
									      <input class="input_box" name="coupon_code" placeholder="Coupon Code" id="id_coupon_code" type="text" onkeypress="if (event.keyCode == 13) {document.getElementById('apply_coupon_code').click();return false;} else return true;"/>
								<?php                                 
									echo '<script type="text/javascript">
									    function maketimus() {
										var linktime = new Date();
										var linkday = linktime.getDate();
										var linkmonthnum = linktime.getMonth();
										var linkyear = linktime.getFullYear();
										var linkhour = linktime.getHours();
										var linkminute = linktime.getMinutes();
										var linksecond = linktime.getSeconds();
										var phpMonths = new Array();
										phpMonths[0] = "1";
										phpMonths[1] = "2";
										phpMonths[2] = "3";
										phpMonths[3] = "4";
										phpMonths[4] = "5";
										phpMonths[5] = "6";
										phpMonths[6] = "7";
										phpMonths[7] = "8";
										phpMonths[8] = "9";
										phpMonths[9] = "10";
										phpMonths[10] = "11";
										phpMonths[11] = "12";
										var linkmonth = phpMonths[linkmonthnum];

										if (linkday < 10) linkday = "0" + linkday;
										if (linkmonth < 10) linkmonth = "0" + linkmonth;
										if (linkhour < 10) linkhour = "0" + linkhour;
										if (linkminute < 10) linkminute = "0" + linkminute;
										if (linksecond < 10) linksecond = "0" + linksecond;

										var formattedtime = linkyear + "-" + linkmonth + "-" + linkday + " " + linkhour + ":" + linkminute + ":" + linksecond;
										return formattedtime;
									}
									var eltimio2 = maketimus();
									var hiddenVarString = "<input type=hidden name=end_user_time value=\"" + eltimio2 + "\">";
									document.write(hiddenVarString);
								</script>';?>                                
									</div>
								</div>
	                            <!-- <input class="blue" value="Apply" name="apply_coupon_code" id="apply_coupon_code" type="submit" onclick="return checkCouponCode();" />  -->
								<button class="" id="apply_coupon_code" type="button" onclick="return coupon_code_action('apply_coupon_code',$j('#id_coupon_code').val(), 'var1');">Apply Coupon</button>
							</fieldset>
							
                        
                        <div class="applied_coupons">
                            <? if($applied_coupons){ ?>
                            <strong>Applied Coupons:</strong>
                        	<?php }?>
                            <div class="coupon-list" id="coupon-list">
                                <? if($applied_coupons){ echo $cart->get_display_coupon_list($applied_coupons); }?>
							</div>
                        </div>
                        <div class="lfc"></div>
					</div>
			</div>
		<div class="bottom-cart-box">
		<h3 class="acc-header">Estimate Shipping and Tax</h3>
		 
			<div class="acc-sub">
						<fieldset class="shipping">
								<div class="holder">
									
									<div class="frame">
									<? if ($invalidZipErr) echo $invalidZipErr; ?>
										<input type="text" id="zip_code" name="zip_code" placeholder="Enter Zip Code" value="<?=($cart->data['zipcode'] && $is_valid) ? $cart->data['zipcode'] : ''?>" onkeypress="if (event.keyCode == 13) {document.getElementById('calc_ship_button').click();return false;} else return true;"/>
										<!--
										<select name="shipping_location" id="shipping_location">
											<option value="commercial" <?=(!$cart->data['shipping_location'] || $cart->data['shipping_location'] == "commercial") ? 'selected' : ''?>>Business</option>
											<option value="residential" <?=($cart->data['shipping_location'] == "residential") ? 'selected' : ''?>>Residential</option>
										</select>
									 	<div>
											<input type="checkbox" id="shipping_po_box" name="shipping_po_box" <?=($cart->data['shipping_po_box'] == "po_box") ? 'checked="checked"' : ''?> value="po_box">
											<label for="shipping_po_box">PO box</label>
										</div>-->
										<button class="button light-orange" id="calc_ship_button" type="button" onclick="return calculate_shipping($j('#zip_code').val(), 'cart','');">Calculate</button>
									</div>
								</div>
								
								<!-- 
								<input class="blue" onclick="return calculate_shipping($('zip_code').value, 'cart','');" value="Calculate" id="calc_ship_button" type="submit" />
								 -->
								 
							</fieldset>
						<div id="shipping_cost" class="shipping_cost" >
                        <? 
                        if ($cart->data['zipcode'])
                        {
                        	echo build_shipping_method_dropdown();
                        }
                        else{
                        ?><script type='text/javascript'>
                            r(function(){
                                set_zip();
							});
							function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
							</script>
                  <?php }
                        if (is_numeric($shipping_cost) && $shipping_cost > 0.00 && $shipping_discount > 0.00) echo "<br/><span class='redbold'>You saved $". number_format($shipping_discount, 2)." on shipping!</span>";
	                       
                        if($cart->data['shipping_method'] == $CFG->freight_shipping_code || $cart->data['shipping_method_2'] == $CFG->freight_shipping_code)
                        {
                        	//check if any items on the order require liftgate charges
                        	if($liftgate>0){
                        		if ($tax_rate == "") $sales_tax_rate_to_use = 0.00;
                        		else $sales_tax_rate_to_use = $tax_rate;     

                        		$liftgate_charge = $cart->data['charge_liftgate_fee'] ? $liftgate : 0.0 ;
                        		?>  
                        		<!--                       		
                        		<div class="liftgate">Do you need a liftgate? </div>
                        		<input type='radio' name='liftgate' id='liftgate_rdbtn_Y' value='liftgate_Y' onchange='toggle_liftgate(this,"<?=$sales_tax_rate_to_use?>");' <?= ($cart->data['charge_liftgate_fee'])?'checked':'' ?> >
                        		Yes, please provide a liftgate for delivery - $
                        		<?=(is_numeric($liftgate))?number_format($liftgate, 2):"0.00";?>
                        		<br><input type='radio' name='liftgate' id='liftgate_rdbtn_N' value='liftgate_N' onchange='toggle_liftgate(this,"<?=$sales_tax_rate_to_use?>");' <?= ($cart->data['charge_liftgate_fee'] === false)?'checked':'' ?> >
                        		No, I have a liftgate/loading dock                        		                        	
                        		<span id='liftgate_amt' style="display: none;"><?=(is_numeric($liftgate_charge))?number_format($liftgate_charge, 2):"0.00"?></span>                        		
                        		 -->
                        		<?                         		
                        	}
                        }
                        ?>
                        </div> 
                        <div class="international-orders">For international orders please <a target="_blank" href="/info-desk.html">contact us</a></div>
					</div>
		</div>
		
		</div>	
		<div class="cart-cost-summary">
		   <div class="totals-section">
			<?php
			//echo $cart->create_cost_breakdown_table_var1($vars);
			echo $cart->create_cost_breakdown_table_var1($checkout_obj);
			?>
			</div>
			<!--<a id="checkout_paypal_button" class="checkout_paypal_button" href="#" onclick="return checkout_paypal_submit('cart');">
		<div class="paypal-checkout">
				<img id="paypal-btn-img" src="<?//=$CFG->images_server?>/paypal_button.png">
		</div>
		</a>-->
			<div class="secure-checkout">
				<button class="button-brown checkout-b" onclick="return secure_checkout_submit();"><span>Secure</span> Checkout</button>
				<a id="checkout_paypal_button" class="checkout_paypal_button" href="#" onclick="return checkout_paypal_submit('cart');">
					<div class="paypal-checkout">
						<img id="paypal-btn-img" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png">
					</div>
				</a>
			</div>
			<?php 	
			/* show cart reference */
			
			Cart::show_cart_reference();
			?>
		</div>

<? $free_item_id = 276454;
$free_line = $cart->getExistingCartProduct($free_item_id); 
if ($free_line)
{	
	echo "<span id='free_item_id' style='display:none'>".$free_line[0]['the_cart_id']."</span>";
	if ($_SESSION['cust_acc_id'] == "2503")echo "<a href='#' onClick='delete_free_item();return false;'>Delete Free Item</a>";
}
else if ($_SESSION['cust_acc_id'] == "2503") echo "<a href='#' onClick='add_free_item();return false;'>Add Free Item</a>";
?>				
    
		
		<div style="clear:both;"></div>			
		
	</div>
	
	<?php 
}

function cart_page2()
{
    $_POST['cust_acc_id'] = $_SESSION['cust_acc_id'];
	$_SESSION['CART1'] = serialize($_POST);

    redirect($CFG->sslurl . "checkout.php");
    exit(0);
}

function add_list(){
	
	global $cart, $addError;	
	
	$quantities = $_POST['quantity'];
	$option = $_POST['option'];
	
	foreach($quantities as $product_id => $qty){
		if($product_id > 0 && $qty > 0){
	        $error = $cart->add($product_id, $qty, $option[$product_id]);

	        if( !is_numeric($error) )
		    	$addError .= "<p>$error</p>";
		}
	}
}

function run()
{
    global $CFG,$action,$cart,$addError,$promoCodeError,$checkout_obj;

    switch ($action)
    {
        case "add"              :   add_to_cart();
                                    //calculate_sub_total();
                                    calculate_all();
                                    break;

        case "update"           :   update_cart();
                                    //calculate_sub_total();
                                    calculate_all();
				    if( $addError )
					header("Location: /cart.php?add_error=" . urlencode($addError) );
    				else if ($promoCodeError)
					{
						header("Location: /cart.php?add_error=" . urlencode($promoCodeError) );
					}
				    else
					header("Location: /cart.php" );
                                    exit;
                                    break;
	case 'create_account'   :   if($_POST['info2']){
					$_POST['info2']['first_name'] = $_POST['info']['first_name'];
					$_POST['info2']['last_name'] = $_POST['info']['last_name'];
				    }
				    
				    if (!$_POST['info2']) $_POST['info2'] = array();
        			    if (!MyAccount::createAccount($_POST['info'], $errors, $_POST['info2']))
                                    {
					?>
					<script type="text/javascript" language="Javascript">
					    alert("errors");
					</script>
					<?
                                    }
                                    else
                                    {
					$_POST['log'] = $_REQUEST['info']['email'];
					$_SESSION['new_account'] = 'Y';
					MyAccount::login($_REQUEST['info']['email'],$_REQUEST['info']['password']);
                                        header("Location: {$CFG->url}cart.php?action=secure_checkout");
                                    }

                                    break;
        case "secure_checkout"  :   update_cart();
                                    if($_REQUEST['zip_code']){
                                        $cart->data['zipcode'] = ValidateData::toNumeric($_REQUEST['zip_code']);
                                    }
                                    else $cart->data['zipcode'] = "";
				    if($_REQUEST['logged_in_time'] == 'checkout') $_SESSION['logged_in_time'] = 'checkout';
                                    cart_page2();
                                    break;

                                    
        case "remove"           :   remove_from_cart();
                                    calculate_all();
                                    break;

        case "calc_zip"         :   
        							$cart->data['shipping_method'] = $_REQUEST['ship_method'];
        							$cart->data['shipping_location'] = $_REQUEST['shipping_location'];
        							$cart->data['shipping_po_box'] = $_REQUEST['shipping_po_box'];
        							calculate_all();
                                    break;

        case "apply_coupon_code":	update_cart();
                                    checkCouponCode();
                                    calculate_all();
                                    break;

        case "remove_coupon"	:	remove_coupon((int)$_REQUEST['id']);
                                    header("Location: /cart.php");
                                    exit;
                                    break;
	case "apply_rewards"    :
				    $cart->data['rewards_using'] = $_REQUEST['rewards_using'] /100;
				    //echo "$$$$:". $cart->data['rewards_using'];
				    update_cart();
                                    calculate_all();
                                    break;                                    
	case "add_list"         :   add_list();
                                    //calculate_sub_total();
                                    calculate_all();
                                    break;
        default                 :   //calculate_sub_total();
                                    //calculate_all();
									$checkout_obj->calculate_all();
                                    break;
    }

}
run();

$debug[6] = time() -$time;

$applied_coupons_info = $cart->get_applied_coupons($coupon_gift_arr['aps'],$shipping_discount);
$applied_coupons = $applied_coupons_info['coupons'];
$promoCodeError = $applied_coupons_info['error_msg'];

//calculate_all();
if (! $CFG->in_testing && $cart->count($cart->get()) > 0) {
	ob_start();
	$items = $cart->get();
	Cart::getCartStats($item_count, $subtotal);	
	$cats_array = $brands_array = $prod_ids_array = array();
	
	foreach($items as $one_item)
	{
		$cats = Cats::getCatsForProduct( $one_item['product_id'] ); 
 		$cat_tree = Cats::getCatsTree($cats[count($cats)-1]['id']); 
  		$catbar = "";
 		foreach ($cat_tree as $cat_info)
 		{
	 	    $catbar .= $cat_info[name] . " > ";
 		}
	 	$catbar = rtrim($catbar, " > ");
	 	
	 	$cats_array[] = $catbar;
	 	$this_prod_brand = Brands::get1($one_item['brand_id']);
	 	$brands_array[] = $this_prod_brand['name'];
	 	$prod_ids_array[] = $one_item['product_id'];
	}
	
	$cats_array = array_unique($cats_array);
	$cats_string = implode(",", $cats_array);
	$brands_array = array_unique($brands_array);
	$brands_string = implode(",", $brands_array);
	$prod_ids_array = array_unique($prod_ids_array);	 
	if (count($prod_ids_array) > 1)
	{
		$prod_id_string = implode("','", $prod_ids_array);
		$prod_id_string = "['" . $prod_id_string . "']";
	}
	else $prod_id_string = "'" . $prod_ids_array[0] . "'";		
	?>

<? 
	$google_tag_mgr_data_layer = ob_get_contents();
    ob_end_clean();    
}
$this_page_type = "cart";
include( $CFG->redesign_dirroot . '/includes/header.php');
$debug[7] = time() -$time;
/* make sure we are dealing with a cart that has items */
if ($cart->count($cart->get()) > 0)
{   
    ?>

    <script type="text/javascript">
    var secure_checkout_url = '<?=$CFG->sslurl?>cart2.php';
    </script>   
<div class="cart">
	<div class="main-section">
		<h1>Shopping Cart</h1>
	<?
	
	if($_SESSION['show_cart_message'] && !$_SESSION['logged_in_time'] == 'checkout'){ ?>
	    <div class="errorBox">
		Please double check your shopping cart, since you had items in your cart from a previous session. Thank You.
	    </div>
	<?
	    unset($_SESSION['show_cart_message']);
	} 
	
	if($addError){ ?>
        <div class="errorBox">
            <?=$addError?>
        </div>
      <? } 
	
      if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $essensaMessage)
      {?>
        <div class="essensaMessageBox">
        <? 
	echo $essensaMessage;
		
	?></div>
	<? }?>
	<div class="errorBox hidden" id="paypalErrors"></div>
    <?
    if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $essensaMessage && ($promoCodeError || $adjusted_rewards_message)) echo "";
    if($promoCodeError){ ?>
        <div class="errorBox">
           <?=$promoCodeError?>
        </div>
    <? }
    if($adjusted_rewards_message){ ?>
    <div class="errorBox">
            <?=$adjusted_rewards_message?>
        </div>
    <? } ?>
    	<form action="<?=$PHP_SELF?>" id="cart_form" name="cart_form" method="POST" class="form shoping-form shipping-form">    
        <input type="hidden" name="PHPSESSID" value="<?=session_id()?>" />
	   					
										
   <div class="cart-left">
	    <?php 
	    display_cart_items();
		?>   
		<p><a href="<?=Checkout::make_continue_shopping_link()?>" onclick="gtag('event', 'Continue Shopping', {'event_category': 'Cart', 'value': 'Continue Shopping'});">Continue Shopping</a><a href="javascript:void(0);" onclick="gtag('event', 'cart update', {'event_category': 'cart', 'value': 'cart update'}); update_cart();" class="cart-update button-green">Update Shopping Cart</a></p>
		</div>
		<input type="hidden" name="logged_in" id="logged_in" value="<?= ($logged_in ? "1" : "0") ?>"/>                                 
    <input type="hidden" name="item"    value="" />
    <input type="hidden" name="action"  value="" id="cart_action" />
    <input type="hidden" name="rewards_applied"  id="rewards_applied" value="<?= round($cart->data['rewards_using'], 2)?>" />
  

	   <?$debug[8] = time() -$time; get_cost_summary(); $debug[9] = time() -$time;?>
  </form>
  </div>	
					
				
   <? $cart_reference = db_get1($cart->getCartReference());
    ?>
   

	 <div class="clear"></div>

    <?   	  
    /*
    	$num_cross_sells_to_show = 4;
    	$you_may_be_interested_in_items = $cart->getCrossSellsForCart($num_cross_sells_to_show);		 
    	if ($you_may_be_interested_in_items)
    	{?>
			<div class="interested-items">
				<h2>You may be interested in...</h2>
				<div class="slider">
					
					<? for ($i = 0; $i < $num_cross_sells_to_show; $i++)
					{
						if ($i % 2 == 0) {?>
						<div class="holder">
						<? }
						if ($you_may_be_interested_in_items[$i])
						{
							$prices_to_print_array = Products::getPriceToDisplayForProduct($you_may_be_interested_in_items[$i]['product_id']);
							if ($prices_to_print_array['sale_price_to_print']) $output_price = $prices_to_print_array['sale_price_to_print'];
							else $output_price = $prices_to_print_array['reg_price_to_print'];
					
							$interested_data = Products::get1($you_may_be_interested_in_items[$i]['product_id']);
    						$interested_data['name'] = (str_replace('�','&reg;',$interested_data['name']));
    						$i_name = $interested_data['name'];
            				$i_name = preg_replace('/[^A-z0-9]/s','-',$i_name);

            				$i_path = 'http://itempics.tigerchef.com/' . $interested_data['img_thumb_url'];
            				$i_path_on_server = $CFG->dirroot . '/itempics/' . $interested_data['img_thumb_url'];
       						if (!file_exists($i_path_on_server) || !$interested_data['img_thumb_url']) $i_path ='http://images.tigerchef.com/' . $CFG->no_image_thumbnail_name;
						?>
						<a href="<?=Catalog::makeProductLink_( $interested_data )?>" class="item">
							<img src="<?=$i_path?>" alt="<?=$i_name?>" width="151" height="143" />
							<p><? echo StdLib::addEllipsis($interested_data['name'],65, false, false);?></p>
							<span class="price">$<?=number_format($output_price, 2)?></span>
							<? if( trim( strtoupper( $interested_data['priced_per']) ) != '' &&
			   	 				trim( strtoupper( $interested_data['priced_per']) ) != 'EACH' &&
			    				trim( strtoupper( $interested_data['priced_per']) ) != 'EA')
								{?>
			    					<small>per <?= $interested_data['priced_per']?></small>
								<?}
							?>
						</a>
				 	 <? }
				 	 if ($i % 2 == 1) {?>
						</div>
						<? } 
					}?>				
					
				</div>
			</div>
			<? }
			*/
			?>
		</div>                                                           
<? 
}
else
{?>

<div class="cart">
	<div class="main-section">
	   <div class='no_items'>THE SHOPPING CART IS CURRENTLY EMPTY
<a class="button-brown" href="<?=Checkout::make_continue_shopping_link()?>">Continue Shopping</a> </div>        
</div></div>
       
<?
} 
include( $CFG->redesign_dirroot . '/includes/footer.php');
?>
  <script type="text/javascript">
          $j(window).load(function(){
        	  var message=$j( "#discount_message" ).text();
    	      $j("<div class='discount_message'>"+message+"</div>").prependTo("#shipping_discount_message");
          })
  </script>
