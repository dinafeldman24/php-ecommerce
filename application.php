<?
class object {
	/**
	 * without trailing slash
	 *
	 * @var unknown_type
	 */
	public $dirroot;
	/**
	 * with trailing slash
	 *
	 * @var unknown_type
	 */
	public $baseurl;
	/**
	 * without trailing slash
	 *
	 * @var unknown_type
	 */
	public $libdir;
};
/**************
For benchmarking
*****************/
$start_time = '';

function start_time(){
    global $start_time;
    $r = explode( ' ', microtime() );
    $start_time = $r[1] + $r[0];
}

function endtime() {
    global $start_time;
    $r = explode( ' ', microtime() );
    $r = $r[1] + $r[0];
    $r = round($r - $start_time,4);
    return '<strong>Execution Time</strong>: '.$r.' seconds<br />';
}
start_time();
/******************
End Benchmarking
*****************/
/* setup the configuration object */
$CFG = new object;

/* DEV ONLY, SETTING COOKIE TO PROFILE SESSION */
if( $_REQUEST['start_xdebug'] == 1 )
    setcookie( 'XDEBUG_PROFILE', '1' );

if( $_REQUEST['end_xdebug'] == 1 )
    setcookie ("XDEBUG_PROFILE", "", time() - 3600);
/* END COOKIE SETTING */

//defines db/server specific settings
include_once 'configuration.php';

$CFG->site_name = 'justchargerplates';
$CFG->itempics_root = (isset( $_SERVER['HTTPS'] )?$CFG->sslurl."itempics/":$CFG->baseurl."itempics/");

if ("http://" . $_SERVER['SERVER_NAME'] . "/" == $CFG->mobile_baseurl || "https://" . $_SERVER['SERVER_NAME'] . "/" == $CFG->mobile_sslurl)
{
	//mail("rachel@tigerchef.com", "here", "");
	$CFG->cur_host_dirroot = $CFG->mobile_dirroot;	
	$CFG->cur_host_baseurl = $CFG->mobile_baseurl;
	$CFG->cur_host_sslurl = $CFG->mobile_sslurl;
	$CFG->on_mobile = true;
} 
else 
{
	$CFG->cur_host_dirroot = $CFG->dirroot;
	$CFG->cur_host_baseurl = $CFG->baseurl;
	$CFG->cur_host_sslurl = $CFG->sslurl;
	$CFG->on_mobile = false;
}

$CFG->libdir      = "$CFG->dirroot/apps";
$CFG->sharedjs_dir = "/home/tigerchef/public_html/shared_js"; // ... please point to shared_js dir on production!!! Without Trailing Slash


$CFG->assets_dir =  '/home/tigerchef/dev1-justchargerplates/public_html/assets';
$CFG->css_assets_dir = $CFG->assets_dir . '/css';
$CFG->js_assets_dir = $CFG->assets_dir . '/js';
$CFG->fonts_assets_dir = $CFG->assets_dir . '/fonts';
$CFG->fonts_assets_url = "http://dev1-justchargerplates.tigerchef.com/assets/fonts";
$CFG->js_assets_url = "https://dev1-justchargerplates.tigerchef.com/assets/js";
$CFG->assets_cdn = 'http://dev1-justchargerplates.tigerchef.com/assets';
$CFG->ssl_assets_cdn = 'https://dev1-justchargerplates.tigerchef.com/assets';

$CFG->blog_write_path = $CFG->dirroot . "/blog.xml";
$CFG->blog_rss        = $CFG->baseurl . "blog.xml";

$CFG->unsubscribe_url = $CFG->baseurl . "unsubscribe.php";	

$CFG->webmaster_email = 'webmaster@justchargerplates.com';
$CFG->error_email = 'rachel@tigerchef.com,tova@tigerchef.com,dina.websites@gmail.com';

$CFG->company_name = 'JustChargerPlates';
$CFG->company_email = 'support@justchargerplates.com';

$CFG->additionalpics_server = $CFG->baseurl . 'additional_pics';
$CFG->homepageimages_server = $CFG->baseurl . 'homepage_images';
$CFG->brandlogos_server = $CFG->baseurl . 'brand_logos';
$CFG->catpics_server = $CFG->baseurl . 'catpics';
$CFG->itempics_server = $CFG->baseurl . 'itempics';
$CFG->images_server = $CFG->baseurl . 'images';

$CFG->thumbnail_suffix = '_thumb.jpg';
$CFG->landscape_thumbnail_width = 130;
$CFG->landscape_thumbnail_height = 130;

$CFG->portrait_thumbnail_width = 130;
$CFG->portrait_thumbnail_height = 130;

$CFG->thumbnail_width = $CFG->portrait_thumbnail_width;
$CFG->thumbnail_height = $CFG->portrait_thumbnail_height;

$CFG->additional_img_thumbnail_width = 50;
$CFG->additional_img_thumbnail_height = 50;


$CFG->preview_suffix = "_med.jpg";
$CFG->full_suffix = "_full.jpg";

$CFG->catpic_suffix = '-cat.jpg';
$CFG->catpic_width = 33;
$CFG->catpic_height = 33;

$CFG->catpic_hp_suffix = '-hp-cat.jpg';
$CFG->catpic_hp_width = 150;
$CFG->catpic_hp_height = 150;

$CFG->catpic_featured_suffix = '-featured-cat.jpg';
$CFG->catpic_featured_width = 230;
$CFG->catpic_featured_height = 186;

$CFG->catpic_large_suffix = '-large-cat.jpg';
$CFG->catpic_large_width = 350;
$CFG->catpic_large_height = 350;

$CFG->catpic_medium_suffix = '-medium-cat.jpg';
$CFG->catpic_medium_width = 130;
$CFG->catpic_medium_height = 130;

$CFG->catpic_business_top_suffix = '-business-top-cat.jpg';

$CFG->catpic_icon_suffix = '-icon-cat.png';

$CFG->swatch_suffix = '_swatch.jpg';
$CFG->swatch_width = 20;
$CFG->swatch_larger_suffix = '_swatch_larger.jpg';
$CFG->swatch_width_larger = 80;

$CFG->tab_icon_suffix = '_tab_icon.png';

//$CFG->catpic_width = 127;
//$CFG->catpic_height = 121;

$CFG->no_image_name = 'no-image.png';
$CFG->no_image_thumbnail_name = 'no-image_t.png';

$CFG->home_suffix = '_home.jpg';
$CFG->home_width = 50;
$CFG->home_height = 38;

$CFG->large_suffix = '_large.jpg';
$CFG->large_width = 1000;
$CFG->large_height = 1000;

$CFG->xlarge_suffix = '_xlarge.jpg';
$CFG->xlarge_width = 1000;
$CFG->xlarge_height = 1000;

$CFG->ebay_suffix = '_ebay.jpg';
$CFG->ebay_width = 500;
$CFG->ebay_height = 500;

$CFG->occ_icon_width = 20;
$CFG->occ_icon_height = 20;
$CFG->occ_icon_suffix = '_occ_icon.png';

$CFG->contact_us_url = 'info-desk.html'; 

// For YContent system
$CFG->image_upload_dir = $CFG->dirroot . "/assets";
$CFG->image_upload_url = "/assets/";

// For Bulk Upload
$CFG->bulk_image_upload_dir = $CFG->dirroot . "/temp_images/";
$CFG->bulk_image_upload_url = "temp_images/";

$CFG->testing_email = "dina.websites@gmail.com";

$CFG->sales_group_id = 2;
$CFG->admin_group_id = 1;
// user id eli made on the live server for the google checkout 'salesman'
$CFG->google_salesman_id = 52;


//Use this field to shed weight off the products for the BOL and UPS shipping
$CFG->weight_modifier = .95;

$CFG->dimensions_header_id = 1;

$CFG->website_store_id = 1;
$CFG->purchasing_company_name = "JustChargerPlates";
$CFG->purchasing_company_address1 = "PO Box 583";
$CFG->purchasing_company_city = "Mahwah";
$CFG->purchasing_company_state = "NJ";
$CFG->purchasing_company_zip = "07430";
$CFG->able_kitchen_store_id = 9999;
$CFG->able_kitchen_shipper_id = 8;
$CFG->able_kitchen_new_order_query_string = 0;
/*$CFG->able_kitchen_new_order_query_string = " order_tax_option = $CFG->able_kitchen_store_id AND (CAST(orders.`3rd_party_order_number` AS UNSIGNED) > 104497 OR orders.`3rd_party_order_number` LIKE '%FGAK%'
OR (orders.`3rd_party_order_number` = ''))
AND orders.id not in (204252, 218339, 217366, 193683, 223400)";

$CFG->able_kitchen_paid_orders_string = " order_tax_option = $CFG->able_kitchen_store_id AND (CAST(orders.`3rd_party_order_number` AS UNSIGNED) < 104497 AND orders.`3rd_party_order_number` NOT LIKE '%FGAK%'
AND (orders.`3rd_party_order_number` <> ''))
OR orders.id IN (204252, 218339, 217366, 193683, 223400)";

$CFG->able_kitchen_packaging_materials_fee = 2.50;

$CFG->able_kitchen_authorize_dot_net_live = strtotime("2013-10-17 13:00:00");
*/

/***************SHOULD BE UPDATED TO CORRECT NUMBERS**************************/
$CFG->all_specials_cat_id = 213;

$CFG->related_link_id = 1;
$CFG->recommended_link_id = 3;

$CFG->warranty_cat_id = 114;

$CFG->gift_wrap_product_id = 3081;
$CFG->gift_card_product_id = 40512;
$CFG->gift_card_custom_option_id = 16214;

$CFG->contact_us_page_id = 7;
$CFG->blog_template_id = 1;
$CFG->blog_cat_template_id = 2;

$CFG->default_from_shipper = 1; //  UPS Shipper
$CFG->ups_worldship_shipper_id = 1;
$CFG->item_in_stock_status = 6;
$CFG->item_drop_ship_status = 1;

// Winco, Thunder Group, Rubbermaid, Tiger Chef, CDN, Royal Industries, Vollrath, EdgeCraft, Weston,
// San Jamar, Update International, VacuVin, M. Rothman
$CFG->po_pickup_vendors = "('21')";
$CFG->vendors_shipping_on_own_account = array ('10', '232'); // flash furniture 
$CFG->vendors_shipping_free_freight_to_us = array(); // update intl
//$CFG->vendors_shipping_own_fedex = array('676'); // johnson rose
$CFG->vendors_shipping_own_fedex = array();
$CFG->usps_shipper_id = 5;
/*$CFG->winco_po_pattern = "-5";
$CFG->update_po_pattern = "-4";
$CFG->rubbermaid_po_pattern = "-3";*/
$CFG->winco_po_pattern = "WI";
$CFG->update_po_pattern = "U";
$CFG->rubbermaid_po_pattern = "RU";
$CFG->thunder_group_po_pattern = "TG";
$CFG->johnson_rose_po_pattern = "JR";

$CFG->m_rothman_supplier_id = 403;
$CFG->m_rothman_table = "product_feed_MR";
$CFG->m_rothman_fee = 2.75;

$CFG->flash_furniture_supplier_id = 1093;
$CFG->flash_furniture_table = "product_feed_FF";

$CFG->weston_supplier_id = 398;
$CFG->weston_table = "product_feed_WS";

$CFG->johnson_rose_supplier_id = 676;
$CFG->johnson_rose_table = "product_feed_JR";

$CFG->chroma_supplier_id = 1129;
$CFG->chroma_table = "product_feed_CH";

$CFG->student_role_id = "10";
$CFG->culinary_school_id = "10";

$CFG->dedicated_rep_1 = 69;
$CFG->dedicated_rep_2 = 69;
$CFG->dedicated_rep_3 = 69;

$CFG->statuses_to_exclude_from_order_reports = array(8,10,21,23,24);
/* define database error handling behavior, since we are in development stages
 * we will turn on all the debugging messages to help us troubleshoot */
$DB_DEBUG = false;
$DB_DIE_ON_FAIL = true;

set_include_path(".:/home/tigerchef/shared_apps/:" . get_include_path());

/* load up standard libraries*/ 
require_once('RB/stdlib.php');
require_once('RB/dblib.php');
require_once('RB/zip_codes.php');
require_once("RB/send_email.php");
require_once('RB/misspelled_corrections.php');
require_once('RB/Search.php');
require_once('RB/Format.php');
require_once("RB/JControls.php");
require_once("RB/email_notify.php");
require_once('Pager/Pager.php');
require_once("Numbers/Words.php");
require_once('RB/html_table_heading.php');
require_once('RB/buttons.php');
require_once('RB/states.php');
require_once('RB/authorize.net.php');
include_once('RB/cc_check.php');
include_once('RB/validateData.php');
require_once('RB/random_password.php');


require_once("$CFG->libdir/session.php");
require_once("$CFG->libdir/cart.php");
require_once("$CFG->libdir/settings.php");
require_once("$CFG->libdir/products.php");
require_once("$CFG->libdir/brands.php");
require_once("$CFG->libdir/cats.php");
require_once("$CFG->libdir/orders.php");
require_once("$CFG->libdir/order_statuses.php");
require_once("$CFG->libdir/ships_in.php");
require_once("$CFG->libdir/content.php");
require_once("$CFG->libdir/catalog.php");
require_once("$CFG->libdir/schema.php");
require_once("$CFG->libdir/filters.php");
require_once("$CFG->libdir/taxes.php");

require_once("$CFG->libdir/misc.php");
require_once("$CFG->libdir/truckers.php");
require_once("$CFG->libdir/customers.php");
require_once("$CFG->libdir/cc_message_rewording.php");
require_once("$CFG->libdir/checkout.php");
require_once("$CFG->libdir/payments.php");
require_once("$CFG->libdir/commission_history.php");
require_once("$CFG->libdir/my_account.php");
require_once("$CFG->libdir/product_ratings.php");
require_once("$CFG->libdir/product_recommends.php");
require_once("$CFG->libdir/product_options_dynamic.php");
require_once("$CFG->libdir/option_font_map.php");
require_once("$CFG->libdir/preset_options.php");
require_once("$CFG->libdir/preset_option_values.php");
require_once("$CFG->libdir/option_values.php");
require_once("$CFG->libdir/product_available_options.php");
require_once("$CFG->libdir/product_available_option_groups.php");
require_once("$CFG->libdir/product_available_option_labels.php");
require_once("$CFG->libdir/product_option_cache.php");
require_once("$CFG->libdir/product_reviews.php");
require_once("$CFG->libdir/product_variations.php");
require_once("$CFG->libdir/product_showcase.php");
require_once("$CFG->libdir/review_terms.php");
require_once("$CFG->libdir/review_comments.php");
require_once("$CFG->libdir/pending_reviews.php"); 
require_once("$CFG->libdir/product_subscribers.php");
require_once("$CFG->libdir/product_questions.php");
require_once("$CFG->libdir/brands_contacts.php");
require_once("$CFG->libdir/brands_page.php");
require_once("$CFG->libdir/product_suppliers.php");
require_once("$CFG->libdir/homepage_tabs.php");
require_once("$CFG->libdir/order_notes.php");
require_once("$CFG->libdir/shipping.php");
require_once("$CFG->libdir/shipping_methods.php");
require_once("$CFG->libdir/shipping.php");
require_once("$CFG->libdir/site_reviews.php");
require_once("$CFG->libdir/gift_wrapping.php");
require_once("$CFG->libdir/invoices.php");
require_once("$CFG->libdir/wishlist.php");
require_once("$CFG->libdir/inventory.php");
require_once("$CFG->libdir/youtube.php"); 
require_once("$CFG->libdir/wistia.php");
require_once("$CFG->libdir/refund_codes.php");
require_once("$CFG->libdir/specs.php");
require_once("$CFG->libdir/store.php");
require_once("$CFG->libdir/reward_gifts.php");
require_once("$CFG->libdir/search_terms.php");

require_once("$CFG->libdir/product_options.php");
require_once("$CFG->libdir/order_item_statuses.php");
require_once("$CFG->libdir/ebay_api.php");
require_once("$CFG->libdir/ebay_listing.php");
require_once("$CFG->libdir/eBaySession.php");
require_once("$CFG->libdir/promo_codes.php");

require_once("$CFG->libdir/occasions.php");
require_once("$CFG->libdir/app.purchase_order.php");

require_once("$CFG->libdir/TigerEmail.php");
require_once("$CFG->libdir/email_template.php");
require_once("$CFG->libdir/util.php");
require_once("$CFG->libdir/ycontent.php");
require_once("$CFG->libdir/app.blog.php");
require_once("$CFG->libdir/app.blog_cat.php");
require_once("$CFG->libdir/app.blog_link.php");
require_once("$CFG->libdir/app.blog_link_cat.php");
require_once("$CFG->libdir/image.php");
require_once("$CFG->libdir/templates.php");
require_once("$CFG->libdir/app.newsletter_list.php");

require_once("$CFG->libdir/amazon.php");
require_once("$CFG->libdir/ups_shipper.php");
require_once("$CFG->libdir/freight_shippers.php");

require_once("$CFG->libdir/amazon_report.php");
require_once("$CFG->libdir/cat_filters.php");
require_once("$CFG->libdir/ebay_event_report.php");
require_once("$CFG->libdir/app.resource_loader.php");
require_once("$CFG->libdir/ebay_log.php");
require_once("$CFG->libdir/keyword_match.php");
require_once("$CFG->libdir/brand_discount.php");
require_once("$CFG->libdir/taxes.php");


require_once("$CFG->libdir/side_banners.php");
require_once("$CFG->libdir/promo_code_combine.php");
require_once("$CFG->libdir/tracking.php");

require_once("$CFG->libdir/homepage_image.php");

require_once("$CFG->libdir/weight.php");

require_once("$CFG->libdir/option_color_map.php");
require_once("$CFG->libdir/sweepstakes_entries.php");
require_once("$CFG->libdir/supplier_feed.php");
require_once("$CFG->libdir/zip_codes_usa.php");
require_once("$CFG->libdir/amazon_inventory_feed.php");
require_once("$CFG->libdir/stock_notification_requests.php");
require_once("$CFG->libdir/cc_gateways.php");
require_once("$CFG->libdir/paypal_cc_functions.php");
require_once("$CFG->libdir/usaepay.php");
require_once("$CFG->libdir/emerchant_cc_functions.php");
require_once("$CFG->libdir/amex_cc_functions.php");
require_once("$CFG->libdir/amazon_fba_inventory.php");
require_once("$CFG->libdir/url_redirects.php");
require_once("$CFG->libdir/channelmax_settings.php");



require_once("$CFG->libdir/newegg_import.php");
require_once("$CFG->libdir/cross_sell_overrides.php");
require_once("$CFG->libdir/holiday_dates.php");
require_once("$CFG->libdir/website_order_cancellations.php");
require_once("$CFG->libdir/deals.php");
require_once("$CFG->libdir/fixPhone.php");
require_once("$CFG->libdir/inv_scan.php");
require_once("$CFG->libdir/rewards.php");
require_once("$CFG->libdir/sales.php");
require_once("$CFG->libdir/able_kitchen_extra_charges.php");
require_once("$CFG->libdir/amazon_commission_history.php");
require_once("$CFG->libdir/bulk_sales.php");
require_once("$CFG->libdir/shipments.php");
require_once("$CFG->libdir/cat_content_mapping.php");
require_once("$CFG->libdir/order_assignments.php");
require_once("$CFG->libdir/brands_ordered_by_case.php");
require_once("$CFG->libdir/fraud_score_responses.php");
require_once("$CFG->libdir/amazon_order_lines.php");
require_once("$CFG->libdir/redeem_requests.php");
require_once("$CFG->libdir/three_step_slider.php");



/*

require_once("$CFG->libdir/customers.php");
require_once("$CFG->libdir/my_account.php");
require_once("$CFG->libdir/checkout.php");


require_once("$CFG->libdir/occasions.php");
require_once("$CFG->libdir/packages.php");

require_once("$CFG->libdir/shipping.php");

require_once("$CFG->libdir/tax.php");

require_once("$CFG->libdir/order_item_statuses.php");
require_once("$CFG->libdir/stock_levels.php");

require_once("$CFG->libdir/misc.php");

require_once("$CFG->libdir/testimonials.php");
require_once("$CFG->libdir/calendar.php");
require_once("$CFG->libdir/TimeWrapper.php");
require_once("$CFG->libdir/shipping_methods.php");
require_once("$CFG->libdir/vendors.php");
require_once("$CFG->libdir/rebates.php");

require_once("$CFG->libdir/payments.php");
require_once("$CFG->libdir/truckers.php");
require_once("$CFG->libdir/email_templates.php");
require_once("$CFG->libdir/user_directory.php");
require_once("$CFG->libdir/product_reviews.php");
require_once("$CFG->libdir/review_notifications.php");

require_once("$CFG->libdir/wbp_categories.php");
require_once("$CFG->libdir/order_inquiries.php");
require_once("$CFG->libdir/wbp_pages.php");
require_once("$CFG->libdir/JCSV.php");
require_once("$CFG->libdir/promo_codes.php");


*/

/* connect to the database */
db_connect($CFG->dbhost, $CFG->dbname, $CFG->dbuser, $CFG->dbpass);


if ($_GET['PHPSESSID'])
	session_id($_GET['PHPSESSID']);


/* Create new object of class */
$ses_class = new Session();



//session_set_cookie_params(60*60*24*7*4);

/* Change the save_handler to use the class functions */
session_set_save_handler (array(&$ses_class, '_open'),
                          array(&$ses_class, '_close'),
                          array(&$ses_class, '_read'),
                          array(&$ses_class, '_write'),
                          array(&$ses_class, '_destroy'), 
                          array(&$ses_class, '_gc')); 




session_start();

//for google 
/*
define('BASE_URL', filter_var('http://dev1.new.tigerchef.com/', FILTER_SANITIZE_URL));
define('GOOGLE_CLIENT_ID','114375800526-3hg8o49iiqc1pjgvjviuce95acvfljgm.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET','RYWA6-UqviAZReIDparz1E8O');
define('GOOGLE_REDIRECT_URI','http://dev1.new.tigerchef.com/sociallogin.php?type=google');
define('APPROVAL_PROMPT','auto');
define('ACCESS_TYPE','offline');

//For facebook
define('FACEBOOK_APP_ID','399778700209027');
define('FACEBOOK_APP_SECRET','768b33db1b7ba755066ae2a4d16c5518');
*
//for Twitter
define ('TWITTER_CONSUMER_KEY','frfCfMr7eH6GUsKHu09Uy68ko');
define ('TWITTER_CONSUMER_SECRET','h8NFmhA8552fGytOTipRGWHmUSBTCKgvh2EIXx3gcrJMmt5E2H');
define ('TWITTER_CALLBACK','http://dev1.new.tigerchef.com/sociallogin.php?type=twitter');

//For Linkedin
define ('LINKEDIN_ACCESS','77l00zngxpsnzy');
define ('LINKEDIN_SECRET','uoYFhPpvlrLlD25E');
define ('LINKEDIN_CALLBACK','http://dev1.new.tigerchef.com/sociallogin.php?type=linkedin');

require_once("$CFG->libdir/social.php");
*/

$cart = new Cart();

Settings::assign($CFG);

$CFG->use_lang = 'eng';
$CFG->languages = array("eng");

/* ==========================================================
   ========     eBay    =====================================
   ========================================================== */
//$CFG->ebay_paypal_email = 'testpaypal@tigerchef.com';
$CFG->ebay_compat_level = 551;

if($_GET['prod'] == 1 || false == $CFG->in_testing ){
	$CFG->ebay_server_url = 'https://api.ebay.com/ws/api.dll';
} else if($CFG->in_testing ){
	$CFG->ebay_server_url = 'https://api.sandbox.ebay.com/ws/api.dll';
}

if( !$CFG->in_testing ) {
    $CFG->ebay_dev_id = '196dc9aa-f65b-4e86-8cdf-d8a784a6838e';
    $CFG->ebay_app_id = 'RustyBri-c923-41ec-84cc-f2591693c5d9';
    $CFG->ebay_cert_id = '63bbe589-319a-47ac-bbf9-ff7b5091a26a';
    $CFG->ebay_token = "AgAAAA**AQAAAA**aAAAAA**1OG8Sw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4CoAJGDpwWdj6x9nY+seQ**TUwBAA**AAMAAA**yZjRsngKilEUwMbXU2MhdkWdZkTa3zohCwb2RtNn1ixUdFXVQRvCBn3oAIdIVNlYce/Xcpcjc9NQ+xZgDps1/Q3WjYktQplxMhcCiQJ4GgymZ1CtlnM5G4SGEci6L8Q4RiBjxKRKKeKqyFbhfcZWc43rDyqTDoXxiEK9MpQf2K+nF6mFY5bXx1xT9vg1C807xkE4pRgQqu+lTQK5V/0yvUK2F+NjsI47kTwEYOibjJXmo1pPveF3oUJFRlwJAQE4OvsswpcxIg3Twdd4ao5xOaxi/FuyE95fk6BPLixMANXwO894qM9PV9cOx+S+7VgZnc//BytFr7jmnukBe9FQGR7divjiJtMrFVuT6PqK0VvrCpOjlzxOA5pWT5aE6o1LZDIEotGBQYUT9YYO/SzXfevemX0SZKiVf8xAGm3djzNw6dH9PpvMC931E3WUH40Rj5ugUROsfjp5ZWkwMYC/0CxyI+NrrBQ49EM8xM1ROPgnoeqgi+nkX3IFK0+zKMDRFYOGSvq5cUfIToyaxZ3T9mAvzHFXn4rYJkvQx7vM7gaKPi3RwaZqkLvlJRR0ejxg+agr7qxY9vY9UcNvKKvbsMeSdDoSl+OUe+IK8fUuI5K1UQ9gYVfVgZbtrLdXaUYoqwqEqNZGjSEJwbOtDueHruFT+pTfgp2QbnRdOhd9npRGFGrwXxHMWBI4297a53aB0vB6htiJx80DTqOxwtrBpaGBzSZwRIaX2bE//RHk+nNa9Uuf2XMqXOVL7vkyM2k0";
    $CFG->ebay_store_id = 6;
    $CFG->ebayStoreIds = array(3,4,5,6);
	$CFG->ebayStoreIdsString = "3,4,5,6";    
}
else {
    $CFG->ebay_dev_id = 'ef541ca6-315e-46d6-a058-a684d661def4';
    $CFG->ebay_app_id = 'RustyBri-2f8f-4642-8849-1611e38015d3';
    $CFG->ebay_cert_id = '5f02f52a-d8c4-4d5d-9604-4d3e645a7960';
    $CFG->ebay_token = "AgAAAA**AQAAAA**aAAAAA**xG4WTA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4CoAJKFowmdj6x9nY+seQ**4VQBAA**AAMAAA**qREup0CiSv8fI0eC8RNXeQqELmqkfpYFFwC/Sp67AuIAHZjf4UQjAClYqtQGwFtU9gDXIJ6K/woIY6x10gqyl/DTtHch8E0YupCHKrlr8+giGOZCkqiRtingjuNaagPKxj/7eptSabNUpbUWFFa9l5nxiFsFBBccuxrhepxZGskLyM289mjyLCyxa9GApwZnVzhmoaZyoyxmNg7vX0VCmNp9QT4DcpUraYkun++00T2+f42udRU0ztwTaHUt8UPNVZuLvaMApnlQgVKF1LH/RbUqvh5sQI5qO0oEbCYPd3uiYu+07PCdsn5Ljwo26MAQmJ3cfO5Vm4H7fGau9fPbfeysMezWXmwwPhJVD4F1y+rchU2i71qow/pKxHZ/PW2LQSXaDk/5qzFXVmOT/mK7Ol302NITlHZiX9G/MylkNDmvdjR/jZbf5QyITcwF4AmAegqxWCmncvdeBzwkiNFQx7+UGMMPDw8/ddplrSs8mrfD8n11WQCA0IMjdC1Sg4nOqYwc/uo0SK3v7kS2233Y4zjBGNNRx0LD6/Cy1BaytQuzw7O3lIdKja3/KusIpa8lpAAEzsjQmAkUv2wGchr0Dxst1QJ4VWVMG35O/C05rNzNEyIT7DPNuRt8LrG05bagMpO78EqjcLDI0WBp/C+3jmjS19Hvdi+Ybmwsv+brCFWEiJ/2XCxLFYerNAFzoLVh3xhoIoAPQdsaMNEBMx4C4HusdYmDlVpBjuQEjpmj54apaFFIgoT/p8sccRjsBdXk";
    $CFG->ebay_store_id = 6;
    $CFG->ebayStoreIds = array(3,4,5,6);
	$CFG->ebayStoreIdsString = "3,4,5,6";    
}

/* ==========================================================
   ========     Amazon    ===================================
   ========================================================== */
/* Start AMAZON SETTINGS -- These will replace the .config.inc.php file that comes in the default amazon code */

$CFG->amazon_dir = "MarketplaceWebService/";
$CFG->amazon_store_id = 2;
$CFG->amazon_fba_store_id = 6;
$CFG->amazon_canada_store_id = 16;

// These are for Amazon US
/* define('AWS_ACCESS_KEY_ID', 'AKIAJDPZ45SKNA6QZMWA');
 define('AWS_SECRET_ACCESS_KEY', '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');

 define('APPLICATION_NAME', 'JustChargerPlates');
 define('APPLICATION_VERSION', '1');

 define ('MERCHANT_ID', 'A39I6Q854UA4ZU');
 define ('MARKETPLACE_ID', 'ATVPDKIKX0DER');*/

 // IMPORTANT: Uncomment the approiate line for the country you wish to
// sell in:
// United States:
 $CFG->amazon_serviceUrl = "https://mws.amazonservices.com";

  // These are for Amazon Canada
 define('AWS_ACCESS_KEY_ID_CANADA', 'AKIAJFGXY34ULHMRLOHQ');
 define('AWS_SECRET_ACCESS_KEY_CANADA', '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');

 define ('MERCHANT_ID_CANADA', 'A1009013APYKZA77FWD8');
 define ('MARKETPLACE_ID_CANADA', 'A2EUQ1WTGCTBG2');

 $CFG->amazon_serviceUrlCanada = "https://mws.amazonservices.ca"; 
 
 require_once($CFG->amazon_dir . "Client.php");
 require_once($CFG->amazon_dir . "Exception.php");
 require_once($CFG->amazon_dir . "Interface.php");
 require_once($CFG->amazon_dir . "Mock.php");
 require_once($CFG->amazon_dir . "Model.php");
 require_once($CFG->amazon_dir . "RequestType.php");

// United Kingdom
//$serviceUrl = "https://mws.amazonservices.co.uk";
// Germany
//$serviceUrl = "https://mws.amazonservices.de";
// France
//$serviceUrl = "https://mws.amazonservices.fr";

/* End AMAZON SETTINGS */


/* ==========================================================
   ========     RECAPTCHA    ================================
   ========================================================== */

if($CFG->in_testing){
	// for rustybrick.net domain
	$CFG->recaptcha_public = "6Lf4JAgAAAAAAPzUaATI4rQuNamePPEOuTSOOl8_";
	$CFG->recaptcha_private = "6Lf4JAgAAAAAALEfZX_xq0ZHdm0mMxwaHNxTtnW_";
} else {
	$CFG->recaptcha_private = "6LceDLsSAAAAAKd_Q3IZPI40B5GUndtublak55IA";
	$CFG->recaptcha_public = "6LceDLsSAAAAAFf9JSRz4gG5E4W1R4kIZgEFfX6e";
}


/* ------------------------------------------------------------
/* ------------- DISQUS COMMENTS ------------------------------
/* ------------------------------------------------------------ */
// Refers to the Disqus Admin which owns both forum_ids. Not sure if this is needed in the code but here it is:
$CFG->disqus_user_id = "gVW9zpXLbAFOhbPJRkcot0WIEO2YOyYM5rnCqaUpgaOctstkNJ7QqkUB6VE83yX1";

if( $CFG->in_testing ){
	$CFG->disqus_site_name = "tigerchefdevsite";
	$CFG->disqus_api_key = "s0W2ogDGh798Lnftw5bM674AE52eqOj2NicchTbaw6EZDi5a5HfyCUqhjU8NxrRe";
	$CFG->disqus_forum_id = "358614";
}
else{
	$CFG->disqus_site_name = "tigerchefmain";
	$CFG->disqus_api_key = "5ejWgA0gdCLWfpnoj0f2YgI52JATkIS3C31djuGx0fGqddIWU5ddXiZ5fqB9TEcE";
	$CFG->disqus_forum_id = "358616";
}

// Amazon Payments.

$CFG->awsAccessKey = 'AKIAJL5GIMXPW73TP3EA';
$CFG->awsSecretAccessKey = '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';
$CFG->merchantId = 'A2PJJSI2PFJUV';

// Google Checkout

if( $CFG->in_testing )
{
	// Use mbutler+googcheckout@rustybrick.com Password: Red32Bull
    $CFG->google_checkout_merchant_id = '240224985328952'; // '422765575223324';
    $CFG->google_checkout_merchant_key= 'Q3yjyKZj_v2XcV2tNBKiAw'; // 'TTe0n_bHCLUPjGQkiTNhOw';

    //https://240224985328952:Q3yjyKZj_v2XcV2tNBKiAw@sandbox.google.com/checkout/api/checkout/v2/request/Merchant/240224985328952

    $CFG->google_checkout_in_testing = true;
}
else
{
    $CFG->google_checkout_in_testing = false;
    $CFG->google_checkout_merchant_id = '243289876086867';
    $CFG->google_checkout_merchant_key = 'cSPl2YKBov_KYjdQR-yGUw';
}

//print_ar( $CFG );

$CFG->tigerchef_store_id = 1;
$CFG->buy_dot_com_store_id = 10;
$CFG->sears_store_id = 19;

$CFG->newegg_store_id = 11;
$CFG->suburban27_store_id = 3;
$CFG->newegg_ftp_server = "ftp03.newegg.com";
$CFG->newegg_ftp_username = "A0P5";
$CFG->newegg_ftp_pw = "tiger1811";
$CFG->newegg_order_file_dir = "/Outbound/OrderList";

$CFG->recalc_shipping_stores = array( $CFG->tigerchef_store_id );
$CFG->recalc_tax_stores = array( $CFG->tigerchef_store_id, 7 );

$CFG->test_email = $CFG->testing_email;
$CFG->test_email_email = $CFG->testing_email;

if( $_REQUEST['list_style'] == 'grid' ){
	$_SESSION['list_style'] = 'grid';
} elseif( $_REQUEST['list_style'] == 'list'){
	$_SESSION['list_style'] = 'list';
}

if(!$_SESSION['list_style']){
	$_SESSION['list_style'] = 'grid';
}

$CFG->show_can_buy_limit = 10;


$CFG->brand_logo_dir = '/brand_logos/';

// Google Merchant Search
// Google directory
$CFG->gcs_dir = "google/";
// cx value 
$CFG->gcs_cx = "008425701155046391024:3dpqpe7c06g";
// dev key
$CFG->gcs_dev_key = "AIzaSyC8spqs1A6jUXRQhLdVw_h0T9_rn_RTg3o";
// account Email address
$CFG->gcs_account_email = "estee@suburbanrestequipment.com";
// account Password
$CFG->gcs_account_pwd = "subbow100";
// account ID
$CFG->gcs_account_id = '4009079';

// Search engine 
$CFG->search_engine = 'Solr'; // Set to 'GCS' for google search

// Solr Search
// Solr host
$CFG->solr_server = "64.65.60.176";
// Solr directory
$CFG->solr_dir = "/solr";
// Port to connect to solr
$CFG->solr_port = "8983";
// Solr Core
$CFG->solr_core = "Tigerchef";
// Username & password
$CFG->solr_username = "solr";
$CFG->solr_password = "solr";

// Paypal info.
$CFG->paypal_environment = "sandbox";	// 'beta-sandbox' or 'live' or 'sandbox'
/*
// Paypal API credentials, PayPal end point, and API version.
if($CFG->paypal_environment == "sandbox" || $CFG->paypal_environment == "beta-sandbox") 
{
		//rachel's sandbox
// 		$CFG->Paypal_API_UserName = urlencode('rachel_1321295023_biz_api1.tigerchef.com');
// 		$CFG->Paypal_API_Password = urlencode('1321295064');
// 		$CFG->Paypal_API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31ABnrNAENoa3jueMi02HqzWXVZuby');	

		//tova's sandbox
		$CFG->Paypal_API_UserName = urlencode('tova-facilitator_api1.tigerchef.com');
		$CFG->Paypal_API_Password = urlencode('1395136209');
		$CFG->Paypal_API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31APri-VjHratUwTRhxGh89QmrOomR');
	
		$CFG->Paypal_API_Endpoint = "https://api-3t.".$CFG->paypal_environment.".paypal.com/nvp";
		$CFG->Paypal_API_Express_Checkout_Redirect = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout";

}
else 
{
		$CFG->Paypal_API_UserName = urlencode('paypal1_api1.lionsdeal.com');
		$CFG->Paypal_API_Password = urlencode('Z52FY7M8682ER24D');
		$CFG->Paypal_API_Signature = urlencode('AV3pSoxg5fPcnFlVsUp8Tea6ZUv1AiMSO1iAii2cGptKpZ9P1t0SunWW');
		$CFG->Paypal_API_Endpoint = "https://api-3t.paypal.com/nvp";	
		$CFG->Paypal_API_Express_Checkout_Redirect = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout";
}*/
// for suburban27 paypal
$CFG->sub27_Paypal_API_UserName = urlencode('info_api1.suburbanrestequipment.com');
$CFG->sub27_Paypal_API_Password = urlencode('JAMAFYLETEXRHP25');
$CFG->sub27_Paypal_API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31ABHLTX1AG.y-9eZHKBtnxzsfJkAd');

$CFG->Paypal_version = urlencode('116.0');
$CFG->paypal_cc_live = strtotime("2012-04-23 10:30:00");

//Express checkout settings
$CFG->Paypal_API_Express_Checkout_Return_Url = 'checkout.php?pageAction=paypal';
$CFG->Paypal_API_Express_Checkout_Callback_Url = 'ajax/ajax.checkout_functions.php?action=paypal_callback';
$CFG->Paypal_API_Express_Checkout_Logo_Img = 'images/paypal-logo.png'; //maximum size of 190x60 pixels
$CFG->Paypal_API_Express_Checkout_Logo_ImgHDR = 'images/paypal-logo-HDR.png';  //maximum size of 750x90 pixels
$CFG->Paypal_API_Express_Checkout_Border_Color = 'fdb815';


/* ==========================================================
 ========     Amex    ===================================
========================================================== */
/* Start AMEX API SETTINGS -- These will replace the configuration.php file that comes in the example amex code */

// If using certificate validation, modify the following configuration settings

// alternate trusted certificate file
// leave as "" if you do not have a certificate path
//$CFG->Amex_API_certificatePath = "C:/ca-cert-bundle.crt";

// possible values:
// FALSE = disable verification
// TRUE = enable verification
$CFG->Amex_API_certificateVerifyPeer = FALSE;

// possible values:
// 0 = do not check/verify hostname
// 1 = check for existence of hostname in certificate
// 2 = verify request hostname matches certificate hostname
$CFG->Amex_API_certificateVerifyHost = 0;

// Base URL of the Payment Gateway. Do not include the version.
$CFG->Amex_API_gatewayUrl = "https://gateway-na.americanexpress.com/api/nvp";

$CFG->Amex_test_environment = true;	// set as true for testing mode

if($CFG->Amex_test_environment){
	// Merchant ID supplied by your payments provider
	$CFG->Amex_API_merchantId = "TEST1310449632";
	
	// API username in the format below where Merchant ID is the same as above
	$CFG->Amex_API_apiUsername = "merchant." . $CFG->Amex_API_merchantId;
	
	// API password which can be configured in Merchant Administration
	$CFG->Amex_API_password = "ca4e8f430f4e95cd41e73c3ea603e673";
} else {
	// Merchant ID supplied by your payments provider
	$CFG->Amex_API_merchantId = "6317286193";
	
	// API username in the format below where Merchant ID is the same as above
	$CFG->Amex_API_apiUsername = "merchant." . $CFG->Amex_API_merchantId;
	
	// API password which can be configured in Merchant Administration
	$CFG->Amex_API_password = "b94b6ca2818500a480eaa7c22598c68e"; 
}

// The debug setting controls displaying the raw content of the request and
// response for a transaction.
// In production you should ensure this is set to FALSE as to not display/use
// this debugging information
$CFG->Amex_API_debug = FALSE;

// Version number of the API being used for your integration
// this is the default value if it isn't being specified in process.php
$CFG->Amex_API_version = "16";

$CFG->Amex_cc_live = strtotime("2113-08-28 10:30:00");

/* End AMEX API SETTINGS */

$CFG->searchterm = "Search entire store here...";
$CFG->searchterm_mobile = "Search entire store here...";

$CFG->mailchimp_api_key = "fe05ce66c6a273f2feb92e8fc7b028b6-us9";
$CFG->list_id_val = "6218380d3b";

$CFG->maxMindLicenseKey = "10zQRASnHSbE";
$CFG->fraudScoreFraudThreshold = 4;

$CFG->WistiaAPIEndpoint = "http://fast.wistia.com/oembed.json";

$CFG->YotpoAPPKey = "YqZvcyjl8cWorOctkJOx1WlX4pUxAqpfLwueIdmp";
$CFG->YotpoSecret = "4WgQgNaf5LuEKghK9nmrrqWrO77BYKAPUDlLrlFq";
$CFG->YotpoAppUrl = "https://api.yotpo.com/" ;

$CFG->unishippersURL = "http://www.sgiapps.com/api/integration/customerRate.cfc";
$CFG->unishippersSGICN = "95f3790a-a89e-43f4-aafe-33f31288a076";

$CFG->status_to_exclude_from_sales_reports = array(0,8,10,21,23,24);
$CFG->status_to_exclude_from_sales_reports_string = "(" . implode(",", $CFG->status_to_exclude_from_sales_reports) . ")";
$CFG->amazon_pre_shipped_statuses = array(1,2,3,13,25);
$CFG->qb_new_start_date = '2015-04-21';
$CFG->open_balances_report_start = '2015-04-22';
$CFG->invoice_terms_custs_from_ship_date_start = '2019-02-01';
$CFG->qbxml_version = "12.0";
$CFG->first_real_order_num = 172917;
$CFG->first_amazon_order_that_can_be_credited = $CFG->first_real_order_num;
$CFG->po_email_subject_start = "Your Purchase Order, PO # ";
$CFG->company_po_email = $CFG->company_email;

// FOR Winco EDI, via EDIOptions
/*$CFG->edi_options_shared_mysql_server_data = array(
	'hostname'=>'sqls.rm.edioptions.com:53307',
	'username'=>'LionUser#1',
	'password'=>'v3DbcBp@B',
	'dbname'=>'lion'	
);

$CFG->edi_RECVID = "WINCO";
$CFG->edi_SENDID = "LIONSD";
$CFG->edi_PARTNER = "LION";
$CFG->edi_VENDID = "115169595";
$CFG->edi_VENDORNAME = "LIONSDEAL";*/
?>
