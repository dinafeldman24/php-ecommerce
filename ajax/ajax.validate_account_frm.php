<?php
require_once("../application.php");

switch ($_POST['action']) {
    case 'new_account' :
		newAccount();
	    break;
    case 'validate_password' :
		validatePassword();
	    break;
}

function validatePassword(){
	
	$errors = '';
	$type = $_POST['type'];
    
    if($type === 'checkout'){
    	$_SESSION['logged_in_time'] = 'checkout';
    }
    
    if (!MyAccount::login($_POST['log'], $_POST['pwd'], true)) 
	{
		unset($_SESSION['logged_in_time']);
	    if (MyAccount::login($_POST['log'], $_POST['pwd'], false))
	    {
		    MyAccount::logout();
		    $errors .= "Your account has been created but not activated.  Please activate your account by clicking on the link that was sent to you by email upon completion of account creation.<br>";
	    }
	    else 
	    {
		    $errors .= "We're sorry, that login is not valid.<br>";
	    }
	    $loggedin = false;
	}
    $return = array(
	"errors" => $errors,
    );
    
    if($type === 'checkout' && !$errors){
    	Checkout::populate_address_fields();
    }elseif($type === 'wishlist'){
    	$ret = WishList::get1_by_customer_and_product($_SESSION['cust_acc_id'], $_POST['product_id']);
    	if (!$ret)
    	{
    		WishList::insert(array('customer_id' => $_SESSION['cust_acc_id'], 'product_id' => $_POST['product_id']));    		
    	}    	 
    }
    
    echo json_encode($return);
}

function newAccount(){
	global $CFG;

	$info['first_name']    = $_POST["first_name"];
	$info['email']         = $_POST["email"];
	$info['password']      = $_POST["password"];
	$info['password_conf'] = $_POST["password_conf"];

	$errors     = '';
	$error_fields = array();
	$num_errors = 0;
	/* validate information */

	$num_errors         = 0;
	$validation_fields  = array
	(
			'first_name'    => 'info[first_name]',
	);
	
	$num_errors         = 0;
	
	foreach ($validation_fields as $field => $field_name)
	{
		if ($info[$field] == '')
		{
			$error_fields[] = $field_name;
			//$errors .= sprintf("%s is missing.<br/>", $field_name);
			++$num_errors;
		}
	}

	//=====================
	//  VALIDATE EMAIL
	//=====================
	if ($info['email'])
	{
		$existing = Customers::get(0,'','','','','',$info['email']);

		if(is_array($existing) && count($existing) > 0)
		{
			$error_fields[] = 'info[email]';
			$errors .= 'That email address is already in use.<br/>';
			++$num_errors;
		}
		if (validate_email($info['email'],true)=== false || strstr($info['email'], "adulttoys") !== false || strstr($info['email'], "feeladult.com") !== false ||
		strstr($info['email'], "sex.com") !== false)
		{
			$error_fields[] = 'info[email]';
			$errors .= 'Your email is invalid.<br/>';
			++$num_errors;
		}
	}
	else
	{
		$error_fields[] = 'info[email]';
		//$errors .= 'Your email is missing.<br/>';
		++$num_errors;
	}

	//=====================
	//  VALIDATE PASSWORDS
	//=====================
	if ($info['password'])
	{
		if ($info['password'] != $info['password_conf'])
		{
			$error_fields[] = 'info[password]';
			$error_fields[] = 'info[password_conf]';
			$errors .= 'Your passwords do not match.<br/>';
			++$num_errors;
		}

		if (strlen($info['password']) < 6)
		{
			$error_fields[] = 'info[password]';
			$errors .= 'Your password must be at least 6 characters long.<br/>';
			++$num_errors;
		}
	}
	else
	{
		$error_fields[] = 'info[password]';
		$error_fields[] = 'info[password_conf]';
		//$errors .= 'Password is missing.<br/>';
		++$num_errors;
	}

	if($error_fields){
		$errors = "Please fill in all required fields.<br>" . $errors;
	}

	$return = array(
			"errors" => $errors,
			"error_fields" => $error_fields,
	);
	//$return = $errors;
	echo json_encode($return);
}

function newAccountOld(){
    global $CFG;
       
    $info['first_name']    = $_POST["first_name"];
    $info['last_name']     = $_POST["last_name"];
    $info['address1']      = $_POST["address1"];
    $info['city']          = $_POST["city"];
    $info['state']         = $_POST["state"];
    $info['zip']           = $_POST["zip"];
    $info['phone']         = $_POST["phone"];
    $info['location_type'] = $_POST["location_type"];
    $info['email']         = $_POST["email"];
    $info['password']      = $_POST["password"];
    $info['password_conf'] = $_POST["password_conf"];
    $info['role_id']       = $_POST["role_id"];
    $info['institution_type_id'] = $_POST["institution_type_id"];
    
    $info['same_as_billing'] = $_POST["same_as_billing"];

    $info2['address1']      = $_POST["info2_address1"];
    $info2['city']          = $_POST["info2_city"];
    $info2['state']         = $_POST["info2_state"];
    $info2['zip']           = $_POST["info2_zip"];
    $info2['phone']         = $_POST["info2_phone"];
    $info2['location_type'] = $_POST["info2_location_type"];
    
    $errors     = '';
    $error_fields = array();
    $num_errors = 0;
    /* validate information */

    $validation_fields  = array
    (
	'first_name'    => 'info[first_name]',
	'last_name'     => 'info[last_name]',
	'address1'      => 'info[address1]',
	'city'          => 'info[city]',
	'state'         => 'info[state]',
	'zip'           => 'info[zip]',
	'phone'         => 'info[phone]',
	'location_type' => 'info[location_type]'
    );

    $num_errors         = 0;

    foreach ($validation_fields as $field => $field_name)
    {
	if ($info[$field] == '')
	{
	    $error_fields[] = $field_name;
	    //$errors .= sprintf("%s is missing.<br/>", $field_name);
	    ++$num_errors;
	}
    }
    
    if($info['location_type'] == ''){
	$error_fields[] = 'location_residential';
	$error_fields[] = 'location_commercial';
    }
    
    
    //=====================
    //  VALIDATE SHIPPING
    //=====================
    if ($info['same_as_billing'] != "Y") // if shipping not the same as billing
    {
	foreach ($validation_fields as $field => $field_name)
	{
	    if ($field == "first_name" || $field == "last_name") continue;
	    if ($info2[$field] == '')
	    {
		$field_name = str_replace("info", "info2", $field_name);
		$error_fields[] = $field_name;
		//$errors .= sprintf("%s is missing.<br/>", $field_name);
		++$num_errors;
	    }
	}
	if($info2['location_type'] == ''){
	    $error_fields[] = 'location_residential2';
	    $error_fields[] = 'location_commercial2';
	}
    }
    //=====================
    //  VALIDATE EMAIL
    //=====================
    if ($info['email'])
    {
	$existing = Customers::get(0,'','','','','',$info['email']);

	if(is_array($existing) && count($existing) > 0)
	{
	    $error_fields[] = 'info[email]';
	    $errors .= 'That email address is already in use.<br/>';
	    ++$num_errors;
	}
	if (strstr($info['email'], "adulttoys") !== false || strstr($info['email'], "feeladult.com") !== false ||
	    strstr($info['email'], "sex.com") !== false)
	{
	    $error_fields[] = 'info[email]';
	    $errors .= 'Your email is invalid.<br/>';
	    ++$num_errors;
	}
    }
    else
    {
	$error_fields[] = 'info[email]';
	//$errors .= 'Your email is missing.<br/>';
	++$num_errors;
    }

    //=====================
    //  VALIDATE PASSWORDS
    //=====================
    if ($info['password'])
    {
	if ($info['password'] != $info['password_conf'])
	{
	    $error_fields[] = 'info[password]';
	    $error_fields[] = 'info[password_conf]';
	    $errors .= 'Your passwords do not match.<br/>';
	    ++$num_errors;
	}
    
	if (strlen($info['password']) < 6)
	{
	    $errors .= 'Your password must be at least 6 characters long.<br/>';
	    ++$num_errors;
	}
    }
    else
    {
	$error_fields[] = 'info[password]';
	$error_fields[] = 'info[password_conf]';
	//$errors .= 'Password is missing.<br/>';
	++$num_errors;
    }

    //=====================
    //  VALIDATE INSTITUTION
    //=====================
    if (!$info['role_id'])
    {
	$error_fields[] = 'info[role_id]';
	//$errors .= 'Your Role is missing.<br/>';
	++$num_errors;
    }
    if (!$info['institution_type_id'])
    {
	$error_fields[] = 'info[institution_type_id]';
	//$errors .= 'Your Company or Institution Type is missing.<br/>';
	++$num_errors;
    }
    // enforce that if choose student, must choose culinary school
    if ($info['role_id'] == $CFG->student_role_id && $info['institution_type_id']!= $CFG->culinary_school_id)		
    {
	$error_fields[] = 'info[role_id]';
	$error_fields[] = 'info[institution_type_id]';
	$errors .= "You must choose \"Culinary School\" as your Insitution Type if you have chosen \"Student\" as your role";
	++$num_errors;			
    }
    
    if($error_fields){
	$errors = "Please fill in all required fields.<br>" . $errors;
    }
  
$return = array(
    "errors" => $errors,
    "error_fields" => $error_fields,
);
//$return = $errors;
echo json_encode($return);
}