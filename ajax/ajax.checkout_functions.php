<?php

if(end(explode('/', getcwd()))==='ajax'){
	require_once ("../application.php");
}
//mail('tova@tigerchef.com', 'checkout', var_export($_POST,true));
//$_POST = $_REQUEST;
//mail('mtelkin@gmail.com', 'checkout2', var_export($_POST,true));
global $cart;
$debug['breakdown']['start_time'] = time();
$action = $action ? $action : $_REQUEST['action'];
$paypal_checkout = ($action == 'paypal_callback' || $action == 'paypal_return' || $_POST['paypal_checkout']=='Y' || $action=='setup_paypal_express_checkout') ? true : false ;
$checkout_obj = new Checkout($paypal_checkout);

switch ($action) {
	case 'debug_duplicate_orders'          :
		mail('tova@tigerchef.com,rachel@tigerchef.com','Ajax Fail - Place Order','jscript error object:'.var_export($_REQUEST,true).'session var'.var_export($_SESSION,true));
		echo json_encode('debug email sent');
		break;
	case 'forgot_password'          :
		forgotPassword();
		break;
	case 'email_action'				:
		emailAction();
    case 'shipping_billing'			:
    case 'shipping_billing'			:
    	ShippingBilling();
    	break;
    case 'shipping_method_options'	:
    	buildShippingMethodOptions();
    	break;
    case 'save_shipping_methods'	:
    	saveShippingMethods();
    	break;
    case 'save_payment_method'		:
    	savePaymentMethod();
    	break;
    case 'validate_account_and_payment':
    	validateAccountAndPayment();
    	break;
    case 'create_confirm_order'		:
    	createConfirmOrder();
    	break;
    case 'place_order'		   		:
    	placeOrder();
    	break;
	case 'place_order_open'			:
		$debug['place_order_open'] = time();
    	placeOrderOpen();
    	break;
    case 'setup_paypal_express_checkout':
    	setupPaypalExpressCheckout($_POST['type']);
    	break;
    case 'paypal_callback':
    	paypalCallback();
    	break;
    case "paypal_return"		:
        if(in_array($_REQUEST['shipping_email'],$CFG->sandbox_checkout_emails)){
            $CFG->paypal_environment = "sandbox";
        } 
    	$checkout_obj->get_paypal_transaction_info($_REQUEST);
    	$checkout_obj->cart2 = unserialize($_SESSION['CART2']);
    	break;
    case "continue_shopping"    :
		$this_page_type = 'cart';
        ob_start();
        include ($CFG->redesign_dirroot . '/includes/pop_products.php');
        $reMarketing = ob_get_contents(); 
        ob_end_clean();
        echo json_encode( array('link'=> Checkout::make_continue_shopping_link(true),'reMarketing'=>$reMarketing) );
        exit;
        break;
}

// ========  STEP 2  =========

function ShippingBilling(){

	global $CFG;
	global $checkout_obj;
	global $cart;
	global $ses_class;

	if(filter_var($_REQUEST['shipping_email'], FILTER_SANITIZE_EMAIL)){
		//save email in session in order to send abandoned cart reminder
		if(!$_SESSION['cust_acc_id']){
			//$buyer_name = $_REQUEST['shipping_fname'] . " " . $_REQUEST['shipping_lname'];
			//$ses_class->updateCustomerInfo(session_id(), $_REQUEST['shipping_email'], $buyer_name);
		}

		//subscribe to newsletter
		if( $_REQUEST['email_offer'] )		{
			if(!tcMailchimp::is_email_on_list($_REQUEST['shipping_email'])){
                $industry=($_REQUEST['business_type'])?MyAccount::getInstitutionTypeInfo(0,$_REQUEST['business_type']):array('caption'=>'');


                $fname=($_REQUEST['billing_fname'])?$_REQUEST['billing_fname']:'';
				$lname=($_REQUEST['billing_lname'])?$_REQUEST['billing_lname']:'';
				$company=($_REQUEST['billing_company'])?$_REQUEST['billing_company']:'';

				$merge_fields = array( 'INDUSTRY'=>$industry['caption'], 'FNAME'=>$fname, 'LNAME'=>$lname, 'SOURCE' => 'checkout',
					'COMPANY'=>$company,'ZIP'=>$_REQUEST['billing_zip']);
				$interests = array($CFG->mc_promotion_group_val => true); //Promotions id
				if($industry['mailchimp_interest_id']){
					$interests[$industry['mailchimp_interest_id']]=true;
				}else{
					$interests[$CFG->mc_default_industry_id]=true; //default industry id
				}
				$response = tcMailchimp::add_or_update($_REQUEST['shipping_email'], 'subscribed',$merge_fields,null,'',$interests);
			}
		}
	}

	if ($_REQUEST['stop_for_no_match_billing'] == "no")
	{
		$_SESSION['bad_addresses_okayed']['billing'][] = $_REQUEST['billing_city'].", ".$_REQUEST['billing_state']." ".$_REQUEST['billing_zip'];
		$_REQUEST['stop_for_no_match_billing'] = $_POST['stop_for_no_match_billing'] = "yes";
	}
	if ($_REQUEST['stop_for_no_match_shipping'] == "no")
	{
		$_SESSION['bad_addresses_okayed']['shipping'][] = $_REQUEST['shipping_city'].", ".$_REQUEST['shipping_state']." ".$_REQUEST['shipping_zip'];
		$_REQUEST['stop_for_no_match_shipping'] = $_POST['stop_for_no_match_shipping'] = "yes";
	}

	validate_required_fields($_POST['type']);

	// check if they are trying to send to a PO BOX and there are items in the cart that can't be sent to PO BOXES
	if($_POST['type'] == 'shipping'){
		$validate_po_box = Shipping::validate_po_box($_POST['shipping_address']);
		if(!$validate_po_box['can_ship']){
			$checkout_obj->err_msgs[] =  array('field'=>'shipping_address','msg'=>$validate_po_box['msg']);
			$checkout_obj->passed_validation  = false;
		}
	}

	if ($checkout_obj->passed_validation)
	{
		$passed_address_validation = check_address_validation();

		if(!$passed_address_validation && $checkout_obj->err_msgs){
			$return['errors']  = $checkout_obj->err_msgs;
		}

		if ($passed_address_validation){

			$checkout_obj->cart2 = array_merge($checkout_obj->cart2,$_POST);
			$_SESSION['CART2'] = serialize($checkout_obj->cart2);
			$cart->data['shipping_location'] = $_POST['shipping_location'];

			//make the shipping address string to display
			$shipping_address  = '<ul id="first"><li>' . $_POST['shipping_fname'].' '.$_POST['shipping_lname'].'</li>';
			$shipping_address .= '<li>'.$_POST['shipping_company'].'</li>';
			$shipping_address .= '<li>'.$_POST['shipping_address'].'</li>';
			if ($_POST['shipping_address2'])
			{
				$shipping_address .= '<li>'. $_POST['shipping_address2'] .'</li>';
			}
			$shipping_address .= '<li>'.$_POST['shipping_city'].', '.$_POST['shipping_state'].' '.$_POST['shipping_zip'].'</li></ul>';
			$shipping_address .= '<ul><li>'.FixPhone::displayPhone($_POST['shipping_home_phone']).'</li>';
			$shipping_address .= '<li>'.ucfirst($_POST['shipping_location']).' location</li>';
			$shipping_address .= '<li>' . $_POST['shipping_email'] . '</li></ul>';


			//make the billing address string to display
			$billing_address  = '<ul id="first"><li>' . $_POST['billing_fname']. ' ' . $_POST['billing_lname'] . '</li>';
			$billing_address .= '<li>'. $_POST['billing_address']. '</li>';
            if ($_POST['billing_address2'])
            {
            	$billing_address .= '<li>'. $_POST['billing_address2'] .'</li>';
            }
            $billing_address .= '<li>'.$_POST['billing_city'] . ', ' . $_POST['billing_state']. ' ' . $_POST['billing_zip'].'</li></ul>';
            $billing_address .= '<ul><li>' . FixPhone::displayPhone($_POST['billing_home_phone']).'</li>';
            $billing_address .= '<li>' . $_POST['shipping_email'] . '</li></ul>';

			$return['shipping_addr'] = $shipping_address;
			$return['billing_addr']  = $billing_address;
			$return['result'] = 'passed_validation';
		}
	}else{
		$return = array(
				"errors" => $checkout_obj->err_msgs,
				"error_fields" => $checkout_obj->invalid_fields
		);
	}

	if($_POST['type'] == 'shipping'){
		echo json_encode($return);
	}else{
		return $return;
	}
}

// ========  STEP 2 & 3 =========
function validate_required_fields($type)
{
	global $checkout_obj;

	if( !$checkout_obj->open ){
		$checkout_obj->passed_validation = true;
		$checkout_obj->err_msgs = array();
	}

	$is_field_empty = create_function('$field', 'return (isset($field) && trim($field) != null) ? true : false;');

	$special_validation_rules = array
	(
			'shipping_email'        => 'validate_email',
			'shipping_zip'          => 'validate_us_zipcode',
			'billing_zip'           => 'validate_us_zipcode',
			'billing_home_phone'	=> 'validate_us_phone',
			'shipping_home_phone'	=> 'validate_us_phone',
			'default'               => $is_field_empty
	);

	$checkout_obj->validation = array
	(
			$type.'_fname'         => array('NAME' => "First Name",    'VALIDATED' => true),
			$type.'_lname'         => array('NAME' => "Last Name",     'VALIDATED' => true),
			$type.'_address'       => array('NAME' => "Address",       'VALIDATED' => true),
			$type.'_city'          => array('NAME' => 'City',          'VALIDATED' => true),
			$type.'_state'         => array('NAME' => 'State',         'VALIDATED' => true),
			$type.'_zip'           => array('NAME' => 'Zip',           'VALIDATED' => true),
			//$type.'_home_phone'    => array('NAME' => 'Home Phone',    'VALIDATED' => true)
	);

	if($type == 'shipping'){
		$checkout_obj->validation['shipping_location'] = array('NAME' => 'Shipping Location', 'VALIDATED' => true);
		$checkout_obj->validation['shipping_email'] = array('NAME' => 'Email', 'VALIDATED' => true);
		$checkout_obj->validation['shipping_home_phone'] = array('NAME' => 'Home Phone', 'VALIDATED' => true);
	}
	if($type == 'email'){
		unset($checkout_obj->validation);
		$checkout_obj->validation['shipping_email'] = array('NAME' => 'Email', 'VALIDATED' => true);
	}

	if($checkout_obj->open){
		unset($checkout_obj->validation['shipping_lname'],$checkout_obj->validation['billing_lname']);
	}

	/* now we validate */
	foreach ($checkout_obj->validation as $v_key => $v_val)
	{
		$is_special_rule    = array_key_exists($v_key, $special_validation_rules);
		$msg_string         = null;
		$function_to_call   = $special_validation_rules[($is_special_rule) ? $v_key : 'default'];

		$_REQUEST['shipping_email'] = trim( $_REQUEST['shipping_email'] );

		$result = call_user_func($function_to_call, $_REQUEST[$v_key]);
		if (!$result)
		{
			$checkout_obj->validation[$v_key]['VALIDATED'] = false;
			if ($v_key == "billing_zip" || $v_key == "shipping_zip"){ $msg_string = "is invalid";}
			else {$msg_string = ($_REQUEST[$v_key] && $is_special_rule) ? 'is in an invalid format' : 'is empty.';}

			if($v_key == 'shipping_location'){
				$field = 'shipping-location-holder';
			}else{
				$field = $v_key;
			}
			$msg = sprintf('<div class="error_msg">%s %s</div>', $v_val['NAME'], $msg_string);

			$checkout_obj->err_msgs[] =  array('field'=>$field,'msg'=>$msg);
			$checkout_obj->passed_validation  = false;
		}
	}
	return true;
}

function see_if_city_state_and_zip_match()
{
	global $checkout_obj;
	$checkout_obj->text_to_print = $stop_vars_string = "";

	$addr_type = $_POST['type'];

	if($addr_type){
		$address_types[] = $addr_type;
	}else{
		$address_types = array('shipping','billing');
	}

	foreach ($address_types as $addr_type){

		$request_city = $addr_type . "_city";
		$request_state = $addr_type . "_state";
		$request_zip = $addr_type . "_zip";
		$passed_var = "passed_validation_" . $addr_type;
		$stop_for_no_match_var = "stop_for_no_match_".$addr_type;
		$request_first_name = $addr_type . "_fname";
		$request_last_name = $addr_type . "_lname";
		$request_address_1 = $addr_type . "_address";
		$request_address_2 = $addr_type . "_address2";

		// don't print it if they already said to ignore it
		if ($_REQUEST[$stop_for_no_match_var] == "no" || in_array(($_REQUEST[$request_city].", ".$_REQUEST[$request_state]." ".$_REQUEST[$request_zip]),$_SESSION['bad_addresses_okayed'][$addr_type])){ return true; }

		$is_valid_for_shipping = ZipCodesUSA::see_if_city_state_valid_for_zip($_REQUEST[$request_city], $_REQUEST[$request_state], $_REQUEST[$request_zip]);


		//  if the combination they gave is not valid, get ones that are valid
		if (!$is_valid_for_shipping)
		{
			$city_state = ZipCodesUSA::get_city_state_from_zip($_REQUEST[$request_zip]);

			if(strtolower($_REQUEST[$request_city])!== strtolower($city_state['CityName']) && strtolower($_REQUEST[$request_state])!== strtolower($city_state['StateAbbr'])){
				$checkout_obj->err_msgs[] = array('field' => $request_city,'msg' => '');
				$checkout_obj->err_msgs[] = array('field' => $request_state,'msg' => '');
				$checkout_obj->err_msgs[] = array('field' => $request_zip, 'msg' => '<div class="error_msg">The zip code does not match the city and state.  Did you mean ' . $city_state['CityName'] .', ' .$city_state['StateName'].'?<button class="yn-link orange" onclick="return change_city_state_to_match_zip (\''.$addr_type.'\', \''.$city_state['CityName'].'\', \''.$city_state['StateAbbr'].'\')">yes</button><button class="yn-link orange" onClick="return valid_shipping(\''.$addr_type.'\')">no</button></div>');
			}else{
				if(strtolower($_REQUEST[$request_city])!== strtolower($city_state['CityName'])){
					$checkout_obj->err_msgs[] = array('field' => $request_city,
														'msg' => '<div class="error_msg">The city does not match the zipcode. Did you mean ' . $city_state['CityName'].'?<button class="yn-link orange" onclick="return change_city_state_to_match_zip (\''.$addr_type.'\', \''.$city_state['CityName'].'\', \'\')">yes</button><button class="yn-link orange" onClick="return valid_shipping(\''.$addr_type.'\')">no</button></div>');
				}
				if(strtolower($_REQUEST[$request_state])!== strtolower($city_state['StateAbbr'])){
					$checkout_obj->err_msgs[] = array('field' => $request_state,
														'msg' => '<div class="error_msg">The state does not match the zipcode. Did you mean ' . $city_state['StateName'].'?<button class="yn-link orange" onclick="return change_city_state_to_match_zip (\''.$addr_type.'\', \'\', \''.$city_state['StateAbbr'].'\')">yes</button><button class="yn-link orange" onClick="return valid_shipping(\''.$addr_type.'\')">no</button></div>');
				}
				$checkout_obj->err_msgs[] = array('field' => $request_zip, 'msg' => '');
			}

			$checkout_obj->{$passed_var} = false;
		}
	}


	return false;
// 	mail('tova@tigerchef.com', 'checkout text', var_export($checkout_obj->text_to_print,true));


}


// ========  STEP 4  =========

function buildShippingMethodOptions()
{
	global $cart;
	global $checkout_obj;
	global $CFG;

	//$checkout_obj->cart2 = unserialize($_SESSION['CART2']);
	if($_POST['shipping_zip'] !== $checkout_obj->cart2['shipping_zip']){

		$zip_code = $_POST['shipping_zip'];

		$valid = validate_us_zipcode($zip_code);
		if(!$valid){
			$checkout_obj->err_msgs[] =  array('field'=>'shipping_zip','msg'=>'Zipcode is invalid.');
			$return = array(
					"errors" => $checkout_obj->err_msgs,
			);
			echo json_encode($return);
		}
	} else {

		$zip_code = $checkout_obj->cart2['shipping_zip'];
	}

	$shipping_costs = $checkout_obj->get_shipping_info($zip_code);
	$checkout_obj->liftgate = Shipping::calcLiftgateFee('','cart');
	$str = Shipping::get_shipping_display($shipping_costs,$cart->data['shipping_method'],$checkout_obj->shipping_discount,$checkout_obj->tax_rate,true,true);
	echo json_encode($str);
}

function saveShippingMethods(){
	global $cart;
	global $checkout_obj;

	//save shipping method
	$cart->data['shipping_method'] = $_POST['shipping_method'];
	$cart->updateFreight();

	//save liftgate choice
	if($_POST['charge_liftgate']=='true'){
		$cart->data['charge_liftgate_fee'] = true;
	} else {
		$cart->data['charge_liftgate_fee'] = false;
	}

	//save special comments
	if(isset($_POST['special_comments'])){
		$cart->data['special_comments'] = $_POST['special_comments'];
	}

	//make the shipping method string to display
	$str = getShippingMethodString();

	if($cart->data['special_comments']){
		$str .= "<ul><li>Special Comments: ".$cart->data['special_comments']."</li></ul>";
	}

	$cart_cost_summary = $checkout_obj->create_cost_breakdown_table();

	//mail('tova@tigerchef.com', 'cart cost', var_export($cart_cost_summary,true));

	echo json_encode(array('shipping_method_str' => $str, 'cart_cost_summary' => $cart_cost_summary));
}

function getShippingMethodString(){

	global $cart;
	global $checkout_obj;

	$str = $checkout_obj->getShippingMethodString();

	return $str;
}

// ========  STEP 5  =========

function validateAccountAndPayment(){
	global $checkout_obj;

	//mail('tova@tigerchef.com','post',var_export($_REQUEST,true));

	$return = ShippingBilling();

	//mail('tova@tigerchef.com','return 1',var_export($return,true));


	$checkout_obj->validate_required_cc_fields();

	if($checkout_obj->passed_validation){

		unset($_POST['cc_number']);
		$_SESSION['CART3'] = serialize($_POST);

		$str = '<ul><li>Card Holder: ' . $_REQUEST['cc_name'] . '</li>';
		$str .= ' <li>Payment Method: ' . strtoupper (getCCType($_REQUEST['cc_number'])) .'</li>';
		$str .= ' <li>' . ex_out_cc_num($_REQUEST['cc_number']) .'</li>';
		$str .= ' <li>Expiration: ' . sprintf("%02d", $_REQUEST['cc_expiration_month']) . '/' . $_REQUEST['cc_expiration_year'] .'</li></ul>';

		$return['result'] = 'passed_validation';
		$return['summary'] = $str;
		if(!$_SESSION['cust_acc_id']){
			$return['sign_in'] = $checkout_obj->create_signin();
		}
	}else{
		$return = array(
				"errors" => $checkout_obj->err_msgs,
		);
	}

	//mail('tova@tigerchef.com','return 2',var_export($return,true));

	echo json_encode($return);
}


// function savePaymentMethod(){

// 	global $checkout_obj;

// 	$checkout_obj->validate_required_cc_fields();

// 	if($checkout_obj->passed_validation){

// 		$_SESSION['CART3'] = serialize($_POST);

// 		$str .= '<ul><li>Card Holder: ' . $_REQUEST['cc_name'] . '</li>';
// 		$str .= ' <li>Payment method is: ' . strtoupper (getCCType($_REQUEST['cc_number'])) .'</li>';
// 		$str .= ' <li>' . ex_out_cc_num($_REQUEST['cc_number']) .'</li>';
// 		$str .= ' <li>Expiration: ' . sprintf("%02d", $_REQUEST['cc_expiration_month']) . '/' . $_REQUEST['cc_expiration_year'] .'</li></ul>';

// 		$return['result'] = 'passed_validation';
// 		$return['summary'] = $str;
// 	}else{
// 		$return = array(
// 				"errors" => $checkout_obj->err_msgs,
// 				"error_fields" => $checkout_obj->invalid_fields
// 		);
// 	}

// 	echo json_encode($return);
// }

function ex_out_cc_num($cc_number)
{
	return preg_replace('/(?:[\d]+)([\d]{4})$/', str_repeat("XXXX-", 3) . '${1}', format_cc_number($cc_number));
}

function format_cc_number($cc_number)
{
	return preg_replace('/[^\d]+/', '', $cc_number);
}

// ========  STEP 6  =========

function createConfirmOrder(){

	global $cart;
	global $checkout_obj;

	$return = $checkout_obj->createConfirmOrder();

	if($return){
		return $return;
	}
}

function format_exp_date_for_authorizenet()
{
	global $checkout_obj;
	$exp_date = "";

	return sprintf("%02d%d", $checkout_obj->cart3['cc_expiration_month'], $checkout_obj->cart3['cc_expiration_year']);
}

function format_exp_date_for_authorizenet_cim()
{
	global $checkout_obj;
	$exp_date = "";

	return sprintf("%d-%02d", $checkout_obj->cart3['cc_expiration_year'], $checkout_obj->cart3['cc_expiration_month']);
}

function clear_session_cart()
{
	global $cart;

	$cart->emptyCart();

	//mail('tova@tigerchef.com', 'session b4', var_export($_SESSION,true));

	if($_SESSION['logged_in_time'])
		unset($_SESSION['logged_in_time']);
	if($_SESSION['show_cart_message'])
		unset($_SESSION['show_cart_message']);

	unset($_SESSION['CART1']);
	unset($_SESSION['CART2']);
	unset($_SESSION['CART3']);

	//mail('tova@tigerchef.com', 'session after', var_export($_SESSION,true));

	return true;
}

function authorize_order(&$paypal_obj, &$amex_obj, &$emerchant_obj, $cc_num, $exp_date, $cc_type,$cur_gateway)//GW function changed
{
	global $CFG;
	global $cart;
	global $checkout_obj;
	global $debug;

	if(in_array($_POST['shipping_email'],$CFG->sandbox_checkout_emails)){
		$CFG->tc_emerchant_use_live = 'No';
		$CFG->tc_fidelity_use_live = 'No';
		$CFG->cardknox_usaepay_emulation_use_live = 'No';
        $CFG->paypal_environment = "sandbox";
	}

	$auth_fields = array();
	$resp        = 0;
/**************set gateway be new code**************/
	if($checkout_obj->paypal_checkout){
 //       echo 'goign to use paypal';
		/* create a paypal cc object */
		$paypal_obj = new PaypalCc();
			/* finish express checkout payment */
			$paypal_obj->setTOKEN($cart->data['paypal_ec_token']);
			$paypal_obj->setPAYERID($cart->data['paypal_ec_payerid']);

			$shipping_method = db_get1(ShippingMethods::get('','','',$cart->data['shipping_method']));
			$shipping_name = $shipping_method['descr'];
			if($cart->data['shipping_method_2']){
				$shipping_method = db_get1(ShippingMethods::get('','','',$cart->data['shipping_method_2']));
				$shipping_name .= " / " . $shipping_method['descr'];
			}
			$fields = array(
					'grand_total' 	=> $checkout_obj->grand_total,
					'subtotal'		=> $checkout_obj->sub_total,
					'tax'			=> $checkout_obj->sales_tax,
					'shipping_cost'	=> $checkout_obj->shipping_cost,
					'shipping_name'	=> $shipping_name,
					'liftgate_fee'	=> $checkout_obj->liftgate,
					'discount'		=> $checkout_obj->coupon_gift,
					'min_charge'	=> $checkout_obj->min_charge,
					'rewards'		=> $cart->data['rewards_using'],
					'notes'			=> $cart->data['special_comments'],
					'shipping_info' => $checkout_obj->cart2
			);

			$cart_items = $cart->get();

			$resp = $paypal_obj->do_express_checkout_payment($fields,$cart_items);

		}
		else{
			switch($cur_gateway){
			case 'amex':
				$amex_obj = new AmexCC();

				$exp_month = $checkout_obj->cart3['cc_expiration_month'];
				$exp_year  = substr($checkout_obj->cart3['cc_expiration_year'], 2);
				$order_num = substr(md5(session_id().time()), 0, 17);


				$auth_fields = array
				(
						'first_name'        => $checkout_obj->cart2['billing_fname'],
						'last_name'         => $checkout_obj->cart2['billing_lname'],
						'streetaddress'     => $checkout_obj->cart2['billing_address'],
						'city'              => $checkout_obj->cart2['billing_city'],
						'state'             => $checkout_obj->cart2['billing_state'],
						'zipcode'           => substr($checkout_obj->cart2['billing_zip'], 0, 5),
						'ordernum'          => $order_num,
						'shipto_zipcode'    => substr($checkout_obj->cart2['shipping_zip'], 0, 5),
						'cvv2'              => $checkout_obj->cart3['cc_ccv']
				);

				// save card token
				$resp = $amex_obj->saveCard($cc_num, $exp_month, $exp_year, $auth_fields);

				if($resp == 'SUCCESS'){
					// authorize card
					$resp = $amex_obj->authorize('','','',$checkout_obj->grand_total,$auth_fields,$amex_obj->getTOKEN());
					//$resp = $amex_obj->authorize($cc_num, $exp_month, $exp_year, $grand_total, $auth_fields);
				}
			break;
			case 'paypal':
				// setup fields for paypal
				$paypal_obj = new PaypalCc();
				$auth_fields = array
				(
						'first_name'        => urlencode($checkout_obj->cart2['billing_fname']),
						'last_name'         => urlencode($checkout_obj->cart2['billing_lname']),
						'streetaddress'     => urlencode($checkout_obj->cart2['billing_address']),
						'city'              => urlencode($checkout_obj->cart2['billing_city']),
						'state'             => urlencode($checkout_obj->cart2['billing_state']),
						'zipcode'           => urlencode(substr($checkout_obj->cart2['billing_zip'], 0, 5)),

						'shipto_name'      	=> urlencode($checkout_obj->cart2['shipping_fname'] .' '.$checkout_obj->cart2['shipping_lname']),
						'shipto_street'    	=> urlencode($checkout_obj->cart2['shipping_address']),
						'shipto_street2'   	=> urlencode($checkout_obj->cart2['shipping_address2']),
						'shipto_city'      	=> urlencode($checkout_obj->cart2['shipping_city']),
						'shipto_state'     	=> urlencode($checkout_obj->cart2['shipping_state']),
						'shipto_zipcode'   	=> urlencode(substr($checkout_obj->cart2['shipping_zip'], 0, 5)),

						'ordernum'          => urlencode(session_id().time()),
						'cvv2'              => urlencode($checkout_obj->cart3['cc_ccv'])
				);

				// authorize card
				$resp = $paypal_obj->authorize($cc_num, $exp_date, $cc_type, $checkout_obj->grand_total, $auth_fields);
			break;
			case 'fidelity':
			case 'americard':
			case 'emerchant':
			case 'cardknox-usaepay-emulator':
					$debug['place_order7.1'] = time();
						$emerchant_obj = new eMerchantCc($cur_gateway);
						$debug['place_order7.2'] = time();
						$resp = $emerchant_obj->getAuthAtCheckout($cc_num, $exp_date, $checkout_obj);
						$debug['place_order7.3'] = time();
						$debug['breakdown']['cc_authorization'] = $debug['place_order7.3'] - $debug['place_order7.2'];
			break;
			}
		}
	if (!$resp)
	{
		return null;
	}

	return $resp;
}
function insert_payment($transaction_id, $order_id, $cc_type, $cc_last4, $token=0, $amex_orderid='', $cur_gateway='')//GW
{
	global $checkout_obj;

	if ($checkout_obj->grand_total > 0)
	{
		$charge_info = array
		(
				'order_id'         => $order_id,
				'action'           => 'authorize', //GW
				'transaction_id'   => $transaction_id,
				'amount'           => $checkout_obj->grand_total,
				'response_code'    => "1",
				'response_msg'     => "This transaction has been approved.",
				'last4'            => $cc_last4,
				'card_type'        => $cc_type,
				'token'			   => $token,
				'amex_orderid'	   => $amex_orderid,
				'gateway'	   => $cur_gateway //GW
				//                    'avs_address'      => $authorize_net->getAVSAddress(),
		//                    'avs_zip'          => $authorize_net->getAVSZip()
		);

		if($checkout_obj->paypal_checkout){
			$charge_info['payment_method'] = 'Paypal Checkout';
		} else {
			$charge_info['payment_method'] = ucfirst($cc_type);
		}

		/**
		 * Insert Payment (Saves info about the payment method)
		*/
		$payment_id = Payments::insert( $charge_info );

		/**
		 * Insert Charge (Saves info for current transaction)
		*/
		$charge_info = array(
				'order_id'         => $order_id,
				'action'           => 'authorize', //GW
				'transaction_id'   => $transaction_id,
				'amount'           => $checkout_obj->grand_total,
				'response_code'    => "1",
				'response_msg'     => "This transaction has been approved.",
				'order_payment_id' => $payment_id,
				'card_type'        => $cc_type,
				'amex_orderid'	   => $amex_orderid
		);

		if($checkout_obj->paypal_checkout){
			$charge_info['payment_method'] = 'paypal';
			$charge_info['notes'] = 'via paypal express checkout';
		} else {
			$charge_info['payment_method'] = 'cc';
			$charge_info['notes'] = 'via website';
		}


		$charge_id = Orders::insertCharge($charge_info);

		return $payment_id;
	}
	else return true;
}

function store_order($paypal_obj, $cc_type, $cc_last4,$amex_obj=NULL, $emerchant_obj=NULL)
{
	global $cart;
	global $CFG;
	global $checkout_obj;
	global $debug;

	$debug['place_order8.1'] = time();
	$coupon_gift_arr = $cart->discountCalc($checkout_obj->shipping_cost);
	$debug['place_order8.2'] = time();
	$debug['breakdown']['discountCalc'] = $debug['place_order8.2'] - $debug['place_order8.1'];
	$order_obj = new object();

	/* see if this order is supposed to be gift wrapped.  All we need to do is check
	 the cart for the gift wrap product */
	$gift_wrapped   = 'N';
	$tigerchef_cart = $cart->get();
	$debug['place_order8.3'] = time();
	/* Store order information */
	if ($checkout_obj->grand_total > 0)
	{
		/* Store order information */
		$order_obj->cc_type             = $cc_type;
		$order_obj->last4               = $cc_last4;
		if(!is_null($amex_obj)){
			$order_obj->transaction_id      = $amex_obj->getTRANSACTIONID();
			$order_obj->cvv_match           = $amex_obj->getCVV2MATCH();
			$order_obj->avs_address         = $amex_obj->getAVSCODE();
			$order_obj->payment_type 		= 'cc';
		}
		elseif(!is_null($emerchant_obj)) {
            //emerchant
            $order_obj->transaction_id = $emerchant_obj->getRefNum();
            $order_obj->cvv_match = $emerchant_obj->getCvv2Result();
            $order_obj->avs_address = $emerchant_obj->getAvsResult();
            $order_obj->payment_type = 'cc';
        }
		else {
			$order_obj->transaction_id      = $paypal_obj->getTRANSACTIONID();

			if($checkout_obj->paypal_checkout){
				$order_obj->payment_type = 'paypal';
			}else{
				$order_obj->payment_type 		= 'cc';
				$order_obj->cvv_match           = $paypal_obj->getCVV2MATCH();
				$order_obj->avs_address         = $paypal_obj->getAVSCODE();
				//$order_obj->iavs                = $authorize_net->getIAVS();
				//$order_obj->avs_zip             = $authorize_net->getAVSZip();
			}
		}
	}
	$order_obj->sub_total           = $checkout_obj->sub_total;
	$order_obj->tax_rate            = $checkout_obj->tax_rate;
	$order_obj->shipping_cost       = $checkout_obj->shipping_cost;
	$order_obj->original_shipping_cost = $checkout_obj->original_shipping_cost;
	$order_obj->min_charges		    = $checkout_obj->min_charge;
	$order_obj->liftgate_fee	    = $checkout_obj->liftgate;
	$order_obj->liftgate_required	= $cart->data['charge_liftgate_fee'] ? 'Y' : 'N';
	$order_obj->reward_points_used  = $cart->data['rewards_using'];
	$order_obj->discount            = $checkout_obj->coupon_gift - $coupon_gift_arr['shipping_discount'];
	$order_obj->shipping_discount	= $coupon_gift_arr['shipping_discount'];
	$order_obj->notes               = $cart->data['special_comments'];
	$order_obj->prods_to_wrap       = $checkout_obj->cart2['prods_to_wrap'];
	$order_obj->gift_wrap_msg       = $checkout_obj->cart2['gift_wrap_message'];
	$order_obj->gift_wrapping_id    = $checkout_obj->cart2['gift_wrap_opt'];
	$order_obj->gift_wrapped        = $gift_wrapped;
	$order_obj->sales_tax		    = $checkout_obj->sales_tax;

	$order_obj->shipping            = array
	(
			'first_name'    => $checkout_obj->cart2['shipping_fname'],
			'last_name'     => $checkout_obj->cart2['shipping_lname'],
			'address1'      => $checkout_obj->cart2['shipping_address'],
			'address2'      => $checkout_obj->cart2['shipping_address2'],
			'city'          => $checkout_obj->cart2['shipping_city'],
			'state'         => $checkout_obj->cart2['shipping_state'],
			'zip'           => $checkout_obj->cart2['shipping_zip'],
			'phone'         => $checkout_obj->cart2['shipping_home_phone'],
			'location_type' => $checkout_obj->cart2['shipping_location'],
			'company'       => $checkout_obj->cart2['shipping_company']
	);

	$order_obj->billing             = array
	(
			'email'         => $checkout_obj->cart2['shipping_email'],
			'first_name'    => $checkout_obj->cart2['billing_fname'],
			'last_name'     => $checkout_obj->cart2['billing_lname'],
			'address1'      => $checkout_obj->cart2['billing_address'],
			'address2'      => $checkout_obj->cart2['billing_address2'],
			'company'       => $checkout_obj->cart2['billing_company'],
			'city'          => $checkout_obj->cart2['billing_city'],
			'state'         => $checkout_obj->cart2['billing_state'],
			'zip'           => $checkout_obj->cart2['billing_zip'],
			'phone'         => $checkout_obj->cart2['billing_home_phone']
	);

	$order_obj->paypal_address_status = $checkout_obj->paypal_address_status;
	$debug['place_order8.3'] = time();
	/* insert the order */

	$order_id = Checkout::insertOrder($cart, $order_obj);
	$debug['place_order8.4'] = time();
	$order_profit = Orders::getProfit($order_id, $CFG);
	$debug['place_order8.5'] = time();
	if ($order_profit[0] < 0 && !$CFG->in_testing) mail("rachel@tigerchef.com,joe@tigerchef.com,estee@tigerchef.com,dilan@tigerchef.com,ezra@tigerchef.com", "Profit for ".$CFG->company_name ." WEBSITE order $order_id is $order_profit[0]", $order_profit[1]);
	$the_order = Orders::get1($order_id);
	$cust_id = $the_order['customer_id'];

	$customer = Customers::get1($cust_id);
	$debug['place_order8.6'] = time();
	return $order_id;
}


function verify_session(){
	global $cart;
	global $checkout_obj;

	if(!$checkout_obj->paypal_checkout){
		if( !$checkout_obj->cart2['billing_fname'] || !$checkout_obj->cart2['billing_address'] || !$checkout_obj->cart2['billing_city']
		|| !$checkout_obj->cart2['billing_state'] || !$checkout_obj->cart2['billing_zip'] )
		{
			return false;
		}

		if(!$checkout_obj->open){
			if(!$checkout_obj->cart2['shipping_lname'] || !$checkout_obj->cart2['billing_lname'] ){
				return false;
			}
		}
	}

	if( !$checkout_obj->cart2['shipping_fname'] || !$checkout_obj->cart2['shipping_address'] || !$checkout_obj->cart2['shipping_city']
	|| !$checkout_obj->cart2['shipping_state'] || !$checkout_obj->cart2['shipping_zip'] || !$checkout_obj->cart2['shipping_email'] )
	{
		return false;
	}

	return true;
}

function placeOrder()
{
	global $CFG;
	global $cart;
	global $checkout_obj;
	global $debug;

	$invoice                = null;
	$resp                   = false;
	$payment_id             = 0;
	$order_id               = 0;

	//mail('rachel@tigerchef.com', 'cc', var_export($_POST['cc_number'],true));


	$checkout_obj->cart3['cc_number'] = ($_POST['cc_number'])? $_POST['cc_number'] : $checkout_obj->cc_num;

	/* make sure there are items in the cart */
	if( $cart->count($cart->get()) == 0 ) {
		$checkout_obj->top_err_msgs[]         = sprintf('<span>%s</span>', "There are no items in your cart.");
		$checkout_obj->cc_transaction_ok  = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit();
	}

	$checkout_obj->top_err_msgs = array();
	//mail('rachel@tigerchef.com', 'checkout0', var_export($checkout_obj,true));
	if(!$checkout_obj->passed_validation){
		$checkout_obj->cc_transaction_ok = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit();
	}
//mail('rachel@tigerchef.com', 'checkout1', var_export($checkout_obj,true));

		$debug['place_order4'] = time();

	if($checkout_obj->paypal_checkout){

		$return = $checkout_obj->get_paypal_transaction_info($_REQUEST,true);
		$paypal_obj = $checkout_obj->paypal_ec_obj;
		$cur_gateway='paypal'; //GW

		if($return['error']){
			$checkout_obj->err_msgs['main'][] = $checkout_obj->top_err_msgs[] = sprintf('<span>%s</span>', $return['error']);
			$checkout_obj->cc_transaction_ok  = false;
			$paypal_fail = true;
		}

		if($_POST['special_comments']){
			$cart->data['special_comments'] = htmlentities($_POST['special_comments']);
		}
	}
	$debug['place_order5'] = time();
	$debug['breakdown']['paypal_get_transaction'] = $debug['place_order5'] - $debug['place_order4'];

	$checkout_obj->calculate_all();
	$debug['place_order6'] = time();

	if(!$checkout_obj->paypal_checkout){
		$exp_date               = format_exp_date_for_authorizenet();
		$cc_num                 = format_cc_number($checkout_obj->cart3['cc_number']);
		$cc_type                = getCCType($cc_num);
		$cc_last4               = substr($cc_num, -4);

		if($cc_type == 'unknown'){
			$checkout_obj->top_err_msgs[] = sprintf('<span>%s<br>Please <a href="#" onclick="%s">click here</a> to check your card info, use a different card, or check out with PayPal. </span>', 'Invalid card number',"return edit_checkout_step('account-details');");
			$checkout_obj->err_msgs[] = array('field'=> 'cc_number','msg'=>'<div class="error_msg">Invalid card number</div>');
			$checkout_obj->cc_transaction_ok  = false;
			echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
			exit();
		}
	}

	//if there are errors - these are the links to direct them where they need to fix
	if($checkout_obj->paypal_checkout){
		$link_part = "href='". $CFG->Paypal_API_Express_Checkout_Redirect . "&token=" .$checkout_obj->paypal_token."'";
	}else{
		$link_part = 'href="#" onclick="return edit_checkout_step(\'shipping-details\');"';
	}

	/* Verify Session Data */
	if( !verify_session() ) {
		$checkout_obj->top_err_msgs[]         = sprintf('<span>%s<br><a %s>Edit Shipping Details</a></span>', "Your Billing or Shipping information is missing, please go back and make sure everything is filled out correctly.",$link_part);
		$checkout_obj->cc_transaction_ok  = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit();
	}

	/* make sure all the items being shipped to a po box can be shipped to one */
	$validate_po_box = Shipping::validate_po_box($checkout_obj->cart2['shipping_address'],'cart','','','',$checkout_obj->paypal_token);
	if(!$validate_po_box['can_ship']){
		$checkout_obj->top_err_msgs[] = sprintf('%s<a %s>Edit Shipping Details</a>', $validate_po_box['msg'],$link_part);
		$checkout_obj->cc_transaction_ok = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit();
	}

	//make sure zip is in the continental US


	$destination_ok = Shipping::check_shipping_destination($checkout_obj->cart2['shipping_zip']);
	if(!$destination_ok){
		$checkout_obj->top_err_msgs[] = sprintf('%s<a %s>Edit Shipping Details</a>', 'We are sorry, but we can only ship to continental US.',$link_part);
		$checkout_obj->cc_transaction_ok = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit();
	}
	


	//block specific address
	if(Shipping::block_address($checkout_obj->cart2['shipping_zip'],$checkout_obj->cart2['shipping_address'],$checkout_obj->cart2['shipping_city'],$checkout_obj->cart2['shipping_state'])){
		$checkout_obj->top_err_msgs[] = sprintf('%s<a %s>Edit Shipping Details</a>', 'We are sorry, but we do not ship to that address.',$link_part);
		$checkout_obj->cc_transaction_ok = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		exit(); 
	}


	/* make sure it's not free overnight shipping */
	if($checkout_obj->shipping_cost == 0 && $cart->data['shipping_method'] != 'GND' && $cart->data['shipping_method'] != 'FRT'){
		$checkout_obj->top_err_msgs[] = sprintf('%s<a %s>Edit Shipping Details</a>', 'There seems to be something wrong with the shipping option chosen, please try again.',$link_part);
		$checkout_obj->cc_transaction_ok = false;
		echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
		if(!$CFG->in_testing){
			mail($CFG->error_email,'order attempt - free expedited shipping', var_export($cart,true).var_export($checkout_obj,true));
		}else{
			mail('tova@tigerchef.com','order attempt - free expedited shipping', var_export($cart,true).var_export($checkout_obj,true).var_export($cart->get(),true).var_export($_SESSION,true));
		}
		exit();
	}
	$signed_in_now = "";
	/*
	//if there is a password - create account, or signin
	if($_POST['account_step_password']){
		checkout_create_account();
		if(!$checkout_obj->passed_validation){
			echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
			exit();
		}else{
			$_SESSION['logged_in_time'] = 'checkout';
			$checkout_obj->calculate_all();
			$signed_in_now = MyAccount::get_header_account_text();
		}
	}
	*/
	$debug['place_order7'] = time();
	if ($checkout_obj->grand_total > 0 && $checkout_obj->cc_transaction_ok)
	{
		//GW
		if(!$cur_gateway){
			$gateway=CcGateways::get(0,date('Y-m-d H:i:s'),$cc_type);
			if (!$gateway[0]||count($gateway)>1){
				if($cc_type == 'unknown'){
				$checkout_obj->top_err_msgs[] = sprintf('<span>%s<br>Please <a href="#" onclick="%s">click here</a> to check your card info, use a different card, or check out with PayPal. </span>', 'Invalid card number',"return edit_checkout_step('account-details');");
				$checkout_obj->err_msgs[] = array('field'=> 'cc_number','msg'=>'<div class="error_msg">Invalid card number</div>');
				}else{
					$checkout_obj->top_err_msgs[]  = sprintf('<span>%s</span>', "NO gateway found");
				}
				$checkout_obj->cc_transaction_ok  = false;
				echo json_encode(array("errors" => $checkout_obj->err_msgs, "top_errors" => $checkout_obj->top_err_msgs, "cc_transaction_ok" => $checkout_obj->cc_transaction_ok));
				exit();
			}else{
				$cur_gateway=$gateway[0]['gateway'];
			}
		}
		// authorize the order
		$resp = authorize_order($paypal_obj, $amex_obj, $emerchant_obj, $cc_num, $exp_date, $cc_type, $cur_gateway);
		$token = 0;
		switch($cur_gateway){
			case 'amex':
				if( $resp != "SUCCESS"){
					if ($resp == 'cvv_fail'){
						$checkout_obj->top_err_msgs[]         = sprintf('<span>%s<br>Please <a href="#" onclick="%s">click here</a> to check your card info, use a different card, or check out with PayPal. </span>', 'Invalid card security code',"return edit_checkout_step('account-details');");
					}else{
						$checkout_obj->top_err_msgs[]         = sprintf('<span>%s<br>Please <a href="#" onclick="%s">click here</a> to check your card info, use a different card, or check out with PayPal. </span>', $amex_obj->getNiceErrorMessage(),"return edit_checkout_step('account-details');");
					}
					$checkout_obj->cc_transaction_ok  = false;
				}
				$transaction_id = $amex_obj->getTRANSACTIONID();
				$token = $amex_obj->getTOKEN();
				$amex_orderid = $amex_obj->getORDERID();
			break;

			case 'paypal':
				if ($resp != "success")
					{
						//if there are errors - these are the links to direct them where they need to fix
						if($checkout_obj->paypal_checkout){
							$link_part = "href='". $CFG->Paypal_API_Express_Checkout_Redirect . "&token=" .$checkout_obj->paypal_token."'";
							$message_part = "edit your PayPal details";
						}else{
							$link_part = 'href="#" onclick="return edit_checkout_step(\'shipping-details\');"';
							$message_part = "check your card info, use a different card, or check out with PayPal";
						}

						if ($paypal_obj->getL_ERRORCODE0() == "11611" || $paypal_obj->getL_ERRORCODE0() == "11610") $err_msgs[]         = sprintf('<span>%sPlease <a href="#" onclick="%s">click here</a> to %s. </span>', "Transaction Failed: " . $paypal_obj->getNicelyFormattedFmfError(),$link_part,$message_part);
						else $checkout_obj->top_err_msgs[]         = sprintf('<span>%s<br>Please <a href="#" onclick="%s">click here</a> to %s. </span>', CCMessageRewording::reword($paypal_obj->getL_LONGMESSAGE0()),$link_part,$message_part);

						$checkout_obj->cc_transaction_ok  = false;
					}
					$transaction_id = $paypal_obj->getTRANSACTIONID();
				break;
		    case 'fidelity':
		    case 'americard':
		    case 'emerchant':
			case 'cardknox-usaepay-emulator':		    	
				if($resp != "success"){
					$original_emerchant_error = $emerchant_obj->getError();
                    $emerchant_error = CCMessageRewording::reword($original_emerchant_error);

					if($emerchant_obj->getErrorCode()==10160){
						$return_error_message = $emerchant_error;
					}else{
						//if(strpos(strtolower($emerchant_error),'do not honor')!==false){$emerchant_error = 'Credit Card Declined';}
						$return_error_message = sprintf(
							'<span>%s<br/> Please <a href="#" onclick="%s">click here</a> to check your card info, use a different card, or check out with PayPal. </span>',
							$emerchant_error,
							"return edit_checkout_step('account-details');");
					}
					$checkout_obj->top_err_msgs[] = $err_msgs[] = $return_error_message;
					$checkout_obj->cc_transaction_ok = false;
				}else{
					$transaction_id = $emerchant_obj->getRefNum();
					$token = $emerchant_obj->getCardRef();
				}
			break;
		}
		//GW END
	}

	//else $checkout_obj->cc_transaction_ok = true;
	if ($checkout_obj->grand_total == 0){
		$checkout_obj->cc_transaction_ok = true;
	}
	$debug['place_order8'] = time();

	/* store the order */
	if($checkout_obj->cc_transaction_ok){

		$order_id = store_order($paypal_obj, $cc_type, $cc_last4,$amex_obj, $emerchant_obj);
		if ($order_id === false)
		{
			$checkout_obj->top_err_msgs[]        = sprintf('<span>%s</span>', "Failed to store order.");
			$checkout_obj->cc_transaction_ok  = false;
		}
	}
 	//mail('rachel@tigerchef.com', 'checkout', var_export($checkout_obj,true));

	/* insert charge information */
	if($checkout_obj->cc_transaction_ok){
		$payment_id = insert_payment($transaction_id, $order_id, $cc_type, $cc_last4 ,$token, $amex_orderid, $cur_gateway);//GW
		if ($payment_id === false)
		{
			$checkout_obj->top_err_msgs[]         = sprintf('<span>%s</span>', "Failed to insert payment.");
			$checkout_obj->cc_transaction_ok  = false;
		}

		if ($order_id !== false)
		{
			clear_session_cart();
			/* store in session */
			$_SESSION['ORDER_ID'] = $order_id;
		}

		$checkout_obj->redirect = $CFG->sslurl . "invoice.php";
	}

	$return = array(
			"errors" => $checkout_obj->err_msgs,
			"top_errors" => $checkout_obj->top_err_msgs,
			"cc_transaction_ok" => $checkout_obj->cc_transaction_ok,
			"order_id" => $order_id,
			"redirect" => $checkout_obj->redirect,
			"sign_in_checkout" => $signed_in_now
	);
	$debug['place_order9'] = time();
	$debug['breakdown']['final_time'] = time() - $debug['breakdown']['start_time'];
	$debug['breakdown']['order_id'] = $order_id;

	//mail('tova@tigerchef.com,rachel@tigerchef.com','debugging time on live',var_export($debug,true));


	echo json_encode($return);
}

function placeOrderOpen(){

	global $checkout_obj;
	global $cart;
	global $ses_class;
	global $CFG;
	global $debug;

    $params = array();
    parse_str($_POST['form_info'], $params);
    $_POST = array_merge($_POST,$params);
	// add fields from form_info to $_POST
	$elems = explode('&', $_POST['form_info']);
	foreach( $elems as $val ) {
		$val = trim($val);
		list($val_name,$val_vlaue) = explode('=', $val);
		$_POST[trim(strtolower($val_name))] = trim(urldecode($val_vlaue));
	}
	

	//subsribe to newsletter
	/*if( $_POST['email_offer'] && $_POST['shipping_email'])
	{
		if(!tcMailchimp::is_email_on_list($_POST['shipping_email'])){

			$industry=($_POST['institution_type_id'])?MyAccount::getInstitutionTypeInfo($_POST['institution_type_id']):array('caption'=>'');
			$fname=($_POST['billing_fname'])?$_POST['billing_fname']:'';
			$lname=($_POST['billing_lname'])?$_POST['billing_lname']:'';
			$company=($_POST['billing_company'])?$_POST['billing_company']:'';

			$merge_fields = array('INDUSTRY'=>$industry['caption'], 'FNAME'=>$fname, 'LNAME'=>$lname, 'SOURCE' => 'checkout',
				'COMPANY'=>$company,'ZIP'=>$_POST['billing_zip']);
			$interests = array($CFG->mc_promotion_group_val => true); //Promotions id
			if($industry['mailchimp_interest_id']){
				$interests[$industry['mailchimp_interest_id']]=true;
			}else{
				$interests[$CFG->mc_default_industry_id]=true; //default industry id
			}
			$response = tcMailchimp::add_or_update($_POST['shipping_email'], 'subscribed',$merge_fields,null,'',$interests);
		}
	}*/
	
	//save shipping method
	$cart->data['shipping_method'] = $_POST['shipping_method'];
	$cart->data['shipping_method_2'] = $_POST['shipping_method_2'];
	//$cart->updateFreight();

	//save liftgate choice
	if($_POST['charge_liftgate']=='true'){
		$cart->data['charge_liftgate_fee'] = true;
	} else {
		$cart->data['charge_liftgate_fee'] = false;
	}

	//save shipping location
	$cart->data['shipping_location'] = $_POST['shipping_location'];

	//save special comments
	if(isset($_POST['special_comments'])){
		$cart->data['special_comments'] = $_POST['special_comments'];
	}
	
	if($_POST['paypal_checkout'] == 'Y'){
		placeOrder();
		exit();
	}

	$checkout_obj->open = true;


	if($_POST['billing-same-as-shipping'] == 'yes'){
		$_POST['billing_home_phone'] = $_POST['shipping_home_phone'];
	}

	//$_POST['billing_fname'] = $_POST['cc_name'];
	$_POST['cc_name'] = $_POST['billing_fname'];
	$checkout_obj->cc_num = $_POST['cc_num'];
	unset($_POST['cc_num'],$_POST['cc_number'],$_POST['account_step_password']);
	$_REQUEST = array_merge($_POST,$_REQUEST);

	//mail('tova@tigerchef.com', 'checkout open', var_export($checkout_obj,true));

	if(filter_var($_REQUEST['shipping_email'], FILTER_SANITIZE_EMAIL)){
		//save email in session in order to send abandoned cart reminder
		if(!$_SESSION['cust_acc_id']){
			$buyer_name = $_REQUEST['shipping_fname'] . " " . $_REQUEST['shipping_lname'];
			$ses_class->updateCustomerInfo(session_id(), $_REQUEST['shipping_email'], $buyer_name);
		}
	}

	// stop for no match
	if ($_REQUEST['stop_for_no_match_billing'] == "no")
	{
		$_SESSION['bad_addresses_okayed']['billing'][] = $_REQUEST['billing_city'].", ".$_REQUEST['billing_state']." ".$_REQUEST['billing_zip'];
		$_REQUEST['stop_for_no_match_billing'] = $_POST['stop_for_no_match_billing'] = "yes";
	}
	if ($_REQUEST['stop_for_no_match_shipping'] == "no")
	if ($_REQUEST['stop_for_no_match_shipping'] == "no")
	{
		$_SESSION['bad_addresses_okayed']['shipping'][] = $_REQUEST['shipping_city'].", ".$_REQUEST['shipping_state']." ".$_REQUEST['shipping_zip'];
		$_REQUEST['stop_for_no_match_shipping'] = $_POST['stop_for_no_match_shipping'] = "yes";
	}

	// disabling stop for no match - because of google address city names
	$_REQUEST['stop_for_no_match_shipping'] = "no";
	$_REQUEST['stop_for_no_match_billing'] = "no";

	if(!is_array($checkout_obj->err_msgs)){ $checkout_obj->err_msgs = array(); }
	$debug['place_order_open1'] = time();

	$_POST['billing_email'] = $_REQUEST['billing_email'] = $_REQUEST['shipping_email'];

	// validate shipping info
	if($checkout_obj->passed_validation !== false){
		validate_required_fields('shipping');
	}

	// validate billing info
	validate_required_fields('billing');
	$debug['place_order_open2'] = time();


	$passed_address_validation = check_address_validation();
	if ($passed_address_validation){
		$checkout_obj->cart2 = array_merge($checkout_obj->cart2,$_POST);
		$_SESSION['CART2'] = serialize($checkout_obj->cart2);
	}
	$debug['place_order_open2.1'] = time();
	// validate cc
	$_POST['cc_number'] = $_REQUEST['cc_number'] = $checkout_obj->cc_num;
	$checkout_obj->validate_required_cc_fields();

	if($checkout_obj->passed_validation){

		unset($_POST['cc_number'],$_REQUEST['cc_number']);
		$checkout_obj->cart3 = $_POST;
		$_SESSION['CART3'] = serialize($_POST);
	}

	if(!$checkout_obj->top_err_msgs){$checkout_obj->top_err_msgs = '';}
	$debug['place_order_open2.2'] = time();
		
	$debug['place_order_open3'] = time();
	$debug['breakdown']['mailchimp'] = $debug['place_order_open3'] - $debug['place_order_open2.2'];
	//mail('tova@tigerchef.com','mailchimp',var_export($debug,true));




	placeOrder();

	/*
	$return = array(
			"errors" => $checkout_obj->err_msgs,
			"top_errors" => $checkout_obj->top_err_msgs,
			"cc_transaction_ok" => $checkout_obj->cc_transaction_ok,
			"order_id" => $order_id,
			"redirect" => $checkout_obj->redirect,
			"sign_in_checkout" => $signed_in_now
	);

	echo json_encode($return);
	*/
}

// paypal express checkout functions

function setupPaypalExpressCheckout($type = 'checkout'){


	global $CFG;
	global $cart;
	global $checkout_obj;


	$checkout_obj->calculate_sub_total();

	$fields = array(

		'subtotal'		=> $checkout_obj->sub_total,


	);

    if(in_array($_REQUEST['shipping_email'],$CFG->sandbox_checkout_emails)){
        $CFG->paypal_environment = "sandbox";
    }


	$cancel_url = $CFG->sslurl .$type.'.php';


	$paypal_ec_obj = new PaypalCc();
	$resp = $paypal_ec_obj->set_express_checkout($fields,$cancel_url);


	if ($resp != "success")
	{
		$err_msgs = sprintf('<span>%s</span>', $paypal_ec_obj->getL_LONGMESSAGE0());


	} else {
		$cart->data['paypal_ec_token'] = $paypal_ec_obj->getTOKEN();
	}


	$return = array(
			"paymentID"=> $paypal_ec_obj->getTOKEN(),
			"errors" => $err_msgs,
			"redirect" => $CFG->Paypal_API_Express_Checkout_Redirect . "&token=" . $cart->data['paypal_ec_token']
	);




	echo json_encode($return);

}

/*
function paypalCallback(){



	global $cart;
	global $checkout_obj;


	foreach ($_POST as $key => $field){
		$info[$key] = htmlentities($field);
	}


	$checkout_obj->paypal_token = $info['TOKEN'];
	$session_info = PaypalCc::getSessionInfo($checkout_obj->paypal_token);


	session_decode ( $session_info['session_value'] );
	$cart->data = $_SESSION['cart_data'];


	//Check if it is a residential/commercial shipping address 1-residential, 2-commercial
	$cart->data['shipping_location'] = ($info['L_ITEMWEIGHTVALUE0'] == '1') ? 'residential' : 'commercial';


	// make sure we ship to the destination
	$destination_ok = Shipping::check_shipping_destination($info['SHIPTOZIP']);
	// check if sending to PO box, and meets our requirment if they are
	$validate_po_box = Shipping::validate_po_box($info['SHIPTOSTREET'],'cart','','','',$checkout_obj->paypal_token);


	if($validate_po_box['can_ship'] && $destination_ok){
		$shipping_methods = $checkout_obj->get_shipping_info($info['SHIPTOZIP']);

		//only charging sales tax to NY

		if ($info['SHIPTOSTATE'] == 'NY')
		{
			$taxes		= new Taxes(($info['SHIPTOZIP']), $info['SHIPTOCITY']);
			$tax_rate	= $taxes->getRate();


			//calculate total from callback (sum of items*qty)
			$sum = 0;
			foreach ($info as $key => $item){
				if(strpos($key, 'L_AMT')!==false){
					list($type,$index) = explode('L_AMT', $key);
					$sum += $info[$key] * $info['L_QTY'.$index];
				}
			}


			//need to calculate tax for each option
			foreach ($shipping_methods as $key => $currMethod) {
				//$adjusted_sales_tax = ($this->sub_total - $this->coupon_gift + $currMethod['cost'] + $this->min_charge + $this->liftgate - $cart->data['rewards_using']) * ($tax_rate * .01);
				$adjusted_sales_tax = ($sum + $currMethod['cost_unformated']) * ($tax_rate * .01);
				$adjusted_sales_tax = round($adjusted_sales_tax, 2);
				$shipping_methods[$key]['adjusted_sales_tax'] = $adjusted_sales_tax;
			}
		}
	}
	else{
		if(!$destination_ok){
			$name = 'We do not ship to the destination';
		}else{
			$name = 'Not PO Box eligible';
		}
		$cart->data['shipping_method'] = 'NOSHIP';
		$shipping_methods = array (
								array (
										'descr' => '<span>Ground</span><span class="price">$0.00</span>',
										'descr_str' => 'Ground - $0.00',
										'code' => 'NOSHIP',
										'cost' => '0.00',
										'name' => $name,
										'id' => '1',
										'adjusted_sales_tax' => '0.00',
										'cost_unformated' => 0,
										'info' => '1:0',
								)


							);
	}
	$paypal_ec_obj = new PaypalCc();
	$paypal_ec_obj->callback_response($shipping_methods);
}*/




function checkout_create_account(){
	global $CFG;
	global $checkout_obj;

	$customer = Customers::getIdByEmail($checkout_obj->cart2['shipping_email']);
	if($customer){

		/* Validate Passwords */
		if ($_POST['account_step_password'])
		{
			if (strlen($_POST['account_step_password']) < 6)
			{
				$err_msgs[] =  array('field'=>'password','msg'=>'Your password must be at least 6 characters long.');
			}
		} else {
			$err_msgs[] =  array('field'=>'password','msg'=>'Password is missing.');
		}

		if(!$err_msgs){
			//try loging in
			if(!MyAccount::login($checkout_obj->cart2['shipping_email'], $_POST['account_step_password'], true)){
				$err_msgs[] =  array('field'=>'password','msg'=>'The login is invalid. You can:<ul><li>Try using a different password</li><li>Use the forgot password link</li><li>Leave the password field blank and continue checkout without signing in.</li></ul>');
			}
		}
	} else {
		$account_info = array(
				'first_name'    => $checkout_obj->cart2['billing_fname'],
				'last_name'     => $checkout_obj->cart2['billing_lname'],
				'company' 		=> $checkout_obj->cart2['billing_company'],
				'address1'      => $checkout_obj->cart2['billing_address'],
				'address2'      => $checkout_obj->cart2['billing_address2'],
				'city'          => $checkout_obj->cart2['billing_city'],
				'state'         => $checkout_obj->cart2['billing_state'],
				'zip'           => $checkout_obj->cart2['billing_zip'],
				'phone'         => $checkout_obj->cart2['billing_home_phone'],
				'location_type' => $checkout_obj->cart2['shipping_location'],
				'email'			=> $checkout_obj->cart2['shipping_email'],
				'password'		=> $_POST['account_step_password'],
				'password_conf' => $_POST['account_step_password_conf']
		);
		$shipping_info = array(
				'first_name'    => $checkout_obj->cart2['shipping_fname'],
				'last_name'     => $checkout_obj->cart2['shipping_lname'],
				'company' 		=> $checkout_obj->cart2['shipping_company'],
				'address1'      => $checkout_obj->cart2['shipping_address'],
				'address2'      => $checkout_obj->cart2['shipping_address2'],
				'city'          => $checkout_obj->cart2['shipping_city'],
				'state'         => $checkout_obj->cart2['shipping_state'],
				'zip'           => $checkout_obj->cart2['shipping_zip'],
				'phone'         => $checkout_obj->cart2['shipping_home_phone'],
				'location_type' => $checkout_obj->cart2['shipping_location']
		);
		MyAccount::createAccount($account_info,$error,$shipping_info,$err_msgs,false);
	}
	if($err_msgs){
		foreach ($err_msgs as $error){
			switch ($error['field']) {
				case 'email':
					$checkout_obj->err_msgs[] = array('field'=>'shipping_email','msg'=>'<div class="error_msg">'.$error['msg'].'</div>');
					break;
				case 'password':
					$checkout_obj->err_msgs[] = array('field'=>'account_step_password','msg'=>'<div class="error_msg">'.$error['msg'].'</div>');
					break;
				case 'password_conf':
					$checkout_obj->err_msgs[] = array('field'=>'account_step_password_conf','msg'=>'<div class="error_msg">'.$error['msg'].'</div>');
					break;
			}
		}
		$checkout_obj->passed_validation = false;
	}
}

function emailAction(){
	global $checkout_obj;
	global $ses_class;
    
	$result = call_user_func('validate_email', $_REQUEST['shipping_email']);
	if($result){
		if($_SESSION['cust_acc_id']){
			$return['email_status'] = 'logged_in';
		}else{
			$checkout_obj->cart2['shipping_email'] = $_REQUEST['shipping_email'];
			$customer = Customers::getIdByEmail($checkout_obj->cart2['shipping_email']);
			if($customer){
				$return['email_status'] = 'customer';
				$ses_class->updateCustomerInfo(session_id(), '');
			}else{
				$return['email_status'] = 'not_customer';
				$ses_class->updateCustomerInfo(session_id(), $_REQUEST['shipping_email']);

			}
		}
	}else{
		$return['email_status'] = 'not_valid';
	}
	echo json_encode($return);
}



function check_address_validation(){

	global $checkout_obj;

	$debug['checkout_obj'] = $checkout_obj;

	see_if_city_state_and_zip_match();

	$this_shipping_addr = $_POST['shipping_city'].", ".$_POST['shipping_state']." ".$_POST['shipping_zip'];
	$this_billing_addr = $_POST['billing_city'].", ".$_POST['billing_state']." ".$_POST['billing_zip'];

	$debug['this_shipping']=$this_shipping_addr;
	$debug['this_billing']=$this_billing_addr;

	$billing_bad_addresses_okayed = in_array($this_billing_addr, $_SESSION['bad_addresses_okayed']['billing']);
	$shipping_bad_addresses_okayed = in_array($this_shipping_addr, $_SESSION['bad_addresses_okayed']['shipping']);

	$debug['shipping_bad_address_okayed']=$shipping_bad_addresses_okayed;
	$debug['billing_bad_address_okayed']=$billing_bad_addresses_okayed;

	$passed_address_billing = $checkout_obj->passed_validation_billing || $billing_bad_addresses_okayed ;
	$passed_address_shipping = $checkout_obj->passed_validation_shipping || $shipping_bad_addresses_okayed ;

	$debug['passed_billing'] = $passed_address_billing;
	$debug['passed_shipping'] = $passed_address_shipping;

	$debug['session'] = $_SESSION;
	$debug['request'] = $_REQUEST;

	if(($_POST['billing_only']==='true' && $passed_address_billing) || ($passed_address_billing && $passed_address_shipping)){
		$passed_address_validation = true;
	}

	//mail('dina.websites@gmail.com','checkout',var_export($debug,true));


	return $passed_address_validation;
}