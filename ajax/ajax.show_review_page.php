<?php
if(end(explode('/', getcwd()))==='ajax'){
	require_once ("../application.php");
}

$num = $_GET ['num'];
$total = $_GET ['total'];
$reviews = $_GET ['reviews'];

show_review_page( $num, $total, $reviews);

  function show_review_page($num, $total, $reviews){
 	   $aReviews = explode ( ",", $reviews );
     for($i = $num; $i < ($num + 5); $i ++){
	     if ($aReviews [$i]) {
			         $verified=false;
							 $rev = Product_Reviews::get1_all_review_info ( $aReviews [$i] );
								 if (($rev['order_num'])||((($rev['customer_id']>1) && Customers::isVerifiedBuyer($rev['customer_id'],$rev['product_id']) && $rev['verified_buyer'] == 'A') || $rev['verified_buyer'] == 'Y'))
							     $verified=true;?>


							 <div class="one_review" itemprop="review" itemscope itemtype="http://schema.org/Review" >
							 <script>
                   var fp_options<?=$rev['id']?> = {path: 'sociallogin.php?type=facebook_post&review_id=<?=$rev[id]?>',width:450,height:350};
                   var tp_options<?=$rev['id']?> = {path: 'sociallogin.php?type=twitter_post&review_id=<?=$rev[id]?>',width:450,height:350};
                   var lp_options<?=$rev['id']?> = {path: 'sociallogin.php?type=linkedin_post&review_id=<?=$rev[id]?>',width:450,height:350};
                   var gp_options<?=$rev['id']?> = {path: 'sociallogin.php?type=google_post&review_id=<?=$rev[id]?>',width:450,height:350};

              </script>
									<?if ($rev['name']){ ?>
									<?
								   $r_names = explode(" ", trim($rev['name']));	
	$reviewer_l_name = array_pop($r_names);
	$reviewer_f_name = implode(" ", $r_names);
	$reviewer_initial = substr($reviewer_l_name, 0, 1);
	if ($reviewer_f_name && strpos($reviewer_f_name, "&") === false && count($r_names) <= 2) $reviewer_display_name = $reviewer_f_name . " ". $reviewer_initial . ".";
	else $reviewer_display_name = $rev['name'];?>
									<meta itemprop="author" content="<?=$reviewer_display_name?>">
								   <span class="author-details"><span class="author"><?
			                  echo $reviewer_display_name;
												if ($verified) echo " <span>Verified Buyer</span><span class='verified'></span>
                     <div class='v_info'>
										    <div class='verified-tip-header'>
											      What is a <span>Verified Buyer</span>
											  </div>
                        <div class='verified-content'>A Verified Buyer is a user who has purchased the reviewed product through our store. </div>
										 </div>";?>
								   </span></span>
							 <? } ?>

				         <div class="rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
					       <meta itemprop="worstRating" content = "1">
					       <meta itemprop="ratingValue" content="<?=$rev['rating']?>">
					       <meta itemprop="bestRating" content="5">
								  <ul class="rating" >
								       <li class="setted" style="width:<?= $rev[rating] * 18 ?>px;"></li>
								 </ul></div>
								 <span class="review_date"><?=strftime("%Y-%m-%d", strtotime($rev['ts']))?></span>
								 <br/><strong><span itemprop="name"><?= stripslashes($rev['title'])?></span></strong>
								 <meta itemprop="datePublished" content="<?=strftime("%Y-%m-%d", strtotime($rev['ts']))?>">
							   <meta itemprop="itemReviewed" content="<?=$product['name']?>"/>
							    <p itemprop="reviewBody"><? echo nl2br(stripcslashes($rev['comment']));?></p>
								 <div class="share">
							       <a href="#"  onclick="toggle_review('social_share<?=$rev['id']?>');return false;">Share |</a>
								     <ul id="social_share<?=$rev['id']?>" class="hidden">
										       <li><a href="#" onclick="social_open(fp_options<?=$rev['id']?>);return false;">Facebook</a></li>
													 <li><a href="#" onclick="social_open(tp_options<?=$rev['id']?>);return false;">Twitter</a></li>
													 <li><a href="#" onclick="social_open(lp_options<?=$rev['id']?>);return false;">linkedin</a></li>
													 <li class="last"><a href="#" onclick="social_open(gp_options<?=$rev['id']?>);return false;">Google</a></li>
										 </ul>
								 </div>
								 <div id="helpfulthanks<?=$rev['id']?>" class="helpful">
								   <div id="refresh_helpful<?=$rev['id']?>">
										  <div class="label-helpful">Was this review helpful?</div>
								      <div class="helpful-count">(<?=$rev['not_helpful']?>)</div>
											<a href="#" onclick="helpful(<?=$rev['id']?>,'N');return false;"><div class="helpful_down"></div></a>
											<div class="helpful-count">(<?=$rev['helpful']?>)</div>
								      <a href="#" onclick="helpful(<?=$rev['id']?>,'Y');return false;"><div class="helpful_up"></div></a>
									</div>
							  </div>


					   </div>
					<?} //if review[$i]
				} //end of review loop
			} //end function

 			?>
