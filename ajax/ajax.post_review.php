<?php
require_once("../application.php");
global $CFG;
if (!$_SESSION['User']['name'])$_SESSION['User']['name']=$_SESSION['User']['first_name']." ".$_SESSION['User']['last_name'];
$customer_id=$_REQUEST['customer_id'];
$product_id=$_REQUEST['product_id'];
$info=array();
$info['rating']=$_REQUEST['rating'];
$info['title']=$_REQUEST['title'];
$info['comment']=$_REQUEST['description'];
$info['product_id']=$product_id;
$info['customer_id']=$customer_id;
$info['name']=$_REQUEST['full_name'];
$info['email']=$_REQUEST['email'];


if ($info['customer_id']){
	$cus=Customers::get1($customer_id);
	$info['name']=$cus['first_name']." ".$cus['last-name'];
	$info['email']=$cus['email'];
}
if (!($info['name']) || !($info['email'])){
    if (!$_SESSION['User']['name'])$_SESSION['User']['name']=$_SESSION['User']['first_name']." ".$_SESSION['User']['last_name'];
		if ($_SESSION['User']['name']==' ')$_SESSION['User']['name']=$_SESSION['User']['first-name']." ".$_SESSION['User']['last-name'];
		if (!$_SESSION['User']['email'])$_SESSION['User']['email']=$_SESSION['User']['email-address'];		
    $info['name']=$_SESSION['User']['name'];
    $info['email']=$_SESSION['User']['email'];
}		

if (!$info['customer_id'] && $info['email']){
	 $info['customer_id']=Customers::getIdByEmail($info['email']);
}

if ($info['title'] && $info['comment'] && $info['rating']){
/*  if ($customer_id) $alreadyReviewed=Product_Reviews::get_review_by_product_and_customer($product_id,$customer_id);
  if ($info['email'])
     $alreadyReviewed=Product_Reviews::get_review_by_email($product_id,$info['email']);

	 if ($alreadyReviewed){
			     echo "<h2>You already submitted a review for this product.</h2>";
			   }	
				 
			    else */if (save_review($info)) {echo "<h2>Thank you for your review!</h2><span><a href='#' onclick='$j(this).closest(\".product_rev_response\").hide();return false;'>&#xf00d;</a></span>";}
                else echo "<h2>There was a problem saving your review. Please try again later.</h2><span><a href='#' onclick='$j(this).closest(\".product_rev_response\").hide();return false;'>&#xf00d;</a></span>";

}			  

function save_review($info){
  global $CFG;
  if ($info['title'] && !$info['comment'])
	         $info['comment']=$info['title'];
					 

  if (!$info['title'] && $info['comment'])
	    {
			  if (strlen($info['comment']) > 70) 
           { $info['title'] = wordwrap($info['comment'], 70);
             $info['title'] = substr($info['title'], 0, strpos($info['title'], "\n"))."...";
          }
					 else $info['title']=$info['comment'];                                   //comment has less than 150 chars
			}		 
					 
    $reviewid=Product_Reviews::insert($info, $info['product_id'], $info['customer_id']);
    //give pending reward points - will be available when the review is approved
		if($CFG->review_reward_points > 0 && $info['customer_id']){
		    $reward_info = Array(
								'customer_id'  => $info['customer_id'],
								'points_qty'   => $CFG->review_reward_points,
								'action_type'  => 'earned',
								'action'       => 'review',
								'date'         => date('Y-m-d H:i:s'),
								'review_id'     => $reviewid,
								'active'       => 'N'
							    );
								    
			Rewards::insertReward($reward_info);
		}
	return ( $reviewid);

 }
?>
