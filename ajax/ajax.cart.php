<?php
include_once('../application.php');

global $CFG;
global $cart;

$addError ='';
$promoCodeError ='';
$essensaMessage ='';

$checkout_obj = new Checkout();
$checkout_obj->cart2['shipping_zip'] = $cart->data['zipcode'];

switch ($_REQUEST['action']) {
	case 'apply_coupon_code':
		apply_coupon_code($_REQUEST['coupon_code'],$_REQUEST['variation']);
		break;
	case 'remove_coupon':
		remove_coupon($_REQUEST['coupon_code'],$_REQUEST['variation']);
		break;
	case 'apply_rewards':
		apply_rewards($_REQUEST['variation']);
		break;
	case 'update_cart':
		update_cart($_REQUEST['variation']);
		break;
	case 'remove_from_cart':
		remove_from_cart($_REQUEST['variation']);
		break;
	case 'get_zip':
		get_zip_for_user();
		break;
	default:
		exit;
}

function update_cart($variation='')
{
	global $cart;
	global $checkout;
	
	$params = array();
	parse_str($_REQUEST['cart'], $params);
	if ($params['cart'])
	{
		$addError = $cart->update($params['cart']);
		$return = get_elements_for_display('cart', $variation);
		$return['error'] .= $addError;
	}
	echo json_encode($return);
}

function remove_from_cart($variation='')
{
	global $cart;

	if ($_REQUEST['item'])
	{
		$cart->delete($_REQUEST['item']);
		$return = get_elements_for_display('cart', $variation);
	}
	echo json_encode($return);
}

function get_elements_for_display($type='', $variation=''){
	global $CFG;
	global $cart;
	global $checkout_obj;
	global $addError;
	global $promoCodeError;
	global $essensaMessage;
	
	$cost_info = get_cost_info();
	if ($variation == "var1") $cost_summary = $cart->create_cost_breakdown_table_var1($checkout_obj);
	else $cost_summary = $cart->create_cost_breakdown_table($checkout_obj);
	
	$earn_rewards = round(($checkout_obj->sub_total - $checkout_obj->coupon_gift - $cart->data['rewards_using'])* $CFG->rewards_pts_awarded_per_dollar)/100 ;
	
	$return = array('cost_info' => $cost_info,
					'cost_summary' => $cost_summary,
					'essensaMessage' => $essensaMessage,
					'promoCodeError' => $promoCodeError,
					'earnRewards' => $earn_rewards,
					'addError' => $addError);
	
	if($type == 'coupon' || $type == 'cart'){
		$applied_coupon_info = $cart->get_applied_coupons($checkout_obj->coupon_gift_arr['aps'],$checkout_obj->shipping_discount);
		$applied_coupons = $cart->get_display_coupon_list($applied_coupon_info['coupons'], $variation);
		
		if($cart->data['zipcode']){
			$return['shipping_options'] = $cart->build_shipping_method_dropdown($checkout_obj->shipping_discount,$checkout_obj->tax_rate,$cart->data['zipcode'],true,$checkout_obj->shipping_cost_options,$_REQUEST['show_liftgate']);
		}
		$return['error'] .= $applied_coupon_info['error_msg'];
		$return['coupon_list'] = ($applied_coupons) ? $applied_coupons : '';
		
		//if($type == 'cart'){
		$return['shopping_list'] = $checkout_obj->display_cart_items(false,$item_count, $subtotal);
		$return['item_count'] = $item_count ? $item_count : 0;
		$return['subtotal'] = $subtotal ? $subtotal : 0;				
		//}
	}
	
	return $return;
}

function apply_coupon_code($coupon_code, $variation=''){
	
	global $cart;
	global $checkout_obj;
	
	$result = PromoCodes::checkCouponCode('',$coupon_code);

	if($result['result']){
		$cart->addPromoCode($result['promo_id']);
		$checkout_obj->calculate_coupon_gift();
		$return = get_elements_for_display('coupon', $variation);
		
	}else{
		$return['error'] = $result['status'];
	}

	echo json_encode($return);
}


function remove_coupon($id, $variation = ''){
	global $cart;
	global $checkout_obj;

	$cart->deletePromoCode((int)$id);
	$checkout_obj->calculate_coupon_gift();
	$return = get_elements_for_display('coupon', $variation);
	
	echo json_encode($return);
}

function apply_rewards($variation=""){
	global $cart;
	
	$rewards_using = (int) filter_var($_REQUEST['rewards_using'], FILTER_SANITIZE_NUMBER_INT);
	
	$max_rewards = Rewards::getAvailableRewards($_SESSION['cust_acc_id']) ;
	
	//make sure they aren't applying more rewards than they have
	
	if($rewards_using > 0 && $rewards_using > $max_rewards){	
		$error = "You do not have " . $rewards_using . " points to apply. Your available point balance is "
				. $max_rewards . " points.";
		$return = array('error' => $error);
	}
	
	if(!$error){
		//apply the rewards
		$cart->data['rewards_using'] = $rewards_using /100;
		
		//calculate all the charges
		$return = get_elements_for_display('rewards', $variation);
		
		//make rewards message
		if($cart->data['rewards_using']>0){
			$display_reward_points = $cart->data['rewards_using']*100 . ' ($' ;
			$display_reward_points .= (is_numeric($cart->data['rewards_using']))?number_format($cart->data['rewards_using'], 2):"0.00";
			$display_reward_points .= ')';
			 
			$rewards_message =  "You redeemed " . $display_reward_points . " points.";
			 
			$remaining_points = $max_rewards - $cart->data['rewards_using']*100 ;
			
			if($remaining_points > 0){
				$rewards_message .= "<br>You have " . number_format($remaining_points) . " ($";
				$rewards_message .= number_format($remaining_points/100, 2) .") points remaining. ";
			} 
			 
			$rewards_message .= "<br><span><a href='#' onclick='applyRewards(\"undo\",\"".$variation."\");return false;'>Click here to undo.</a></span>";
		}else{
			$display_reward_points = $max_rewards . ' ($' ;
			$display_reward_points .= (is_numeric($max_rewards))? number_format($max_rewards/100, 2):"0.00";
			$display_reward_points .= ')';
			 
			$rewards_message =  $_SESSION['cust_name'] . "'s available point balance: " . $display_reward_points ;
		}
		$return['rewards_message'] = $rewards_message;
		
		$subtotal = str_replace( ',', '', $return['cost_info']['sub_total'] );
		$coupon_gift = str_replace( ',', '', $return['cost_info']['coupon_gift'] );
		
		//make sure they didn't apply more than (sub_total - discount)
		if($rewards_using > (($subtotal - $coupon_gift) *100)){
			$error = "You can not apply more points than the (subtotal - discounts).";
			$cart->data['rewards_using'] = 0;
			$return = array('error' => $error);
		} 
	}
	
	echo json_encode($return);
}

function get_cost_info(){
	
	global $cart;
	global $checkout_obj;

	$checkout_obj->calculate_all();
	
	$cost_info = array('sub_total' => number_format($checkout_obj->sub_total,2),
			'shipping_amt' => number_format($checkout_obj->shipping_cost,2),
			'liftgate_amt' => number_format($checkout_obj->liftgate,2),
			'coupon_gift' => number_format($checkout_obj->coupon_gift,2),
			'ship_and_liftgate_amt' => number_format( ($checkout_obj->shipping_cost+$checkout_obj->liftgate) ,2),
			'sales_tax' => number_format($checkout_obj->sales_tax,2),
			'grand_total' => number_format($checkout_obj->grand_total,2),
			'rewards_redeemed' => number_format($cart->data['rewards_using'],2)
	);

	return $cost_info;
	
}

function get_zip_for_user(){
    global $cart;
	$zip = $cart->get_zip_from_ip();
	echo json_encode(array('zip' => $zip));
}


?>