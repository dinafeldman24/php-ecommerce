<?php
require_once("../application.php");
global $cart;

$product_id = $_POST["product_id"];
$quantity = $_POST["quantity"];
$option = $_POST["option"];
    $selected_optional_options = trim(rtrim($_POST['selected_optional_options'], ','));
    

    
$addError = "";

    $customizations = $_POST['customizations'];
    $presets = $_POST['presets'];
if ($product_id > 0 && $quantity > 0) {
//        echo 'the selected_optional_options: ' . $selected_optional_options;
	    $return = $cart->add($product_id, $quantity, $option,0,0,'', 0,'', '', '', '', 0, 0, '', $selected_optional_options, $customizations, $presets);
	if(!is_array($return) && !is_numeric($return) ) {
		if(!is_array(json_decode($return,true))) {
				$error = array('msg'=>$return);
				$is_srt=1;
				$addError .= json_encode($error);
			}
	        else{
				$is_str=0;
				$addError .= "$return";
			}
	}

	if(!$addError || !$is_str ){
	    	$cart_items = Cart::getExistingCartProduct($product_id, 0, $option, '', $selected_optional_options, $presets);
		if(count($cart_items) == 1){
			$cart_price = '$' . $cart_items[0]['cart_price'];
		}else {
			$cart_price .= '$' . $cart_items[0]['cart_price'] . ' - $' . $cart_items[1]['cart_price'];
		}
	}
}

	Cart::getCartStats($item_count, $subtotal, $all_items_data, "cart_redesign");
	$return = array(
	    "count" => $item_count,
	    "subtotal" => $subtotal,
	    "errors" => $addError,
		"ret_str" => $all_items_data,
		"cart_price" => $cart_price
	);
   

    echo json_encode($return);

   

?>
