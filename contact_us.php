<?
include('application.php');
$title = "Contact Us";
$this_page_type = 'contact';
$description = "We want our customers to be satisfied. Feel free to contact us at any of the following methods";

$errors = "";

$info = $_REQUEST['info'];

if($_POST){

	$email_field_border_color = $name_field_border_color = $message_field_border_color = '';
	if($info['name'] == ""){
		$errors .= "Please enter your name <br>";
		$name_field_border_color = 'red';		
	}

	if(!validate_email($info['email'])){
		$errors .= "Please enter a valid e-mail address<br>";
		$email_field_border_color = 'red';
	}

	if(strlen($info['message']) < 1){
		$errors .= "Please enter a message";
		$message_field_border_color = 'red';		
	}
	if($info['hours_of_operation'] != ""){
		$errors .= "You are not allowed to use this form.";
	}

	if(!$errors){
		
		$msg = "Someone used the Contact Form to send a comment or question:<br />";
		$msg .= "From: ".$info['name'] . ", email: " . $info['email'] . "<br /><br />";
		$msg .= "Phone: ".$info['phone']. "<br /><br />";
		$msg .= "Order Number: ".$info['order_number']. "<br /><br />";
		$msg .= $info['message'];
		$msg .= "<br /><br /> User IP: " . $_SERVER['REMOTE_ADDR'] . "<br />User Browser: " . $_SERVER['HTTP_USER_AGENT'];

		$to_email = $CFG->company_email;

		$vars['body'] = $msg;
		$E = TigerEmail::sendOne($CFG->company_name,$to_email,"blank", $vars, $info['topic']." (Visitor Contact Form)",true,$info['name'],$info['email']);

		$success = true;
		unset($info);
	} else {

	}
} else {
	if($_SESSION['cust_acc_id'] > 0){
		$cust = Customers::get1($_SESSION['cust_acc_id']);
		$info['name'] = $cust['first_name'] . " " . $cust['last_name'];
		$info['email'] = $cust['email'];
	}
}

ValidateData::safeOutputArray($info);

include( $CFG->redesign_dirroot . '/includes/header.php');

?>

           <div class="main-section">
				<div class="content-title">
					<h1>Help and Customer Service</h1>
				</div>
				<div style="padding-top: 10px; padding-bottom: 10px;">
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr class="line">
<td class="line" width="300"><a href="<?=Catalog::makeContentLink ( 'about-us' )?>">About us</a></td>
<td class="line" width="300"><a href="<?=Catalog::makeContentLink ( 'store-policy' )?>">Store Policy</a></td>
</tr>
<tr class="line">
<td class="line"><a href="<?=$CFG->baseurl?>questions.php">Product Questions</a></td>
<td class="line"><a href="<?=$CFG->baseurl?>returns.php">Return Policy</a></td>
</tr>
<tr class="line">
<td class="line"><a href="<?=$CFG->baseurl?>request-quote.php">Request a quote</a></td>
<td class="line"><a href="<?=Catalog::makeContentLink ( 'privacy-policy' )?>">Privacy Policy</a></td>
</tr>
<tr>
<td class="line"><a href="<?=Catalog::makeContentLink ( 'shipping' )?>">Shipping Terms</a></td>
<td class="line"><a href="<?=Catalog::makeContentLink ( 'secure-shopping' )?>">Security</a></td>
</tr>
</tbody>
</table>
</div>
<p>Your questions, requests and comments are important to us. Call us toll-free at <a href="tel:877-747-4979" target="_blank">877-747-4979</a>&nbsp;or fill out the following form and we will get back to you within 24 business hours.</p>
				<form action="<?=$_SERVER['PHP_SELF']."#contact_form"?>" method="post" class="contact-form container">
					<fieldset>
				
						
						<? if($errors){ ?>
						<div class="errorBox"><?=$errors?></div>
						<? } else if($success ) {?>
						<div class="niceMessage">Thank you for contacting us! A customer service representative will respond within 24 business hours.</div>
						<? } ?>
                        <div class="form-box fieldset">
							<h2 class="legend">Contact Us</h2>
							<ul class="form-list ">
							<li>
								<label for="name">Name:</label>
								<input type="text" id="name" name="info[name]" value="<?=($info['name'])?>" <?= $name_field_border_color ? "style='border-color: $name_field_border_color'": ""?>/>
	                        </li>						
							<li>
								<label for="email">Email:</label>
								<input type="email" id="email" name="info[email]" value="<?=($info['email'])?>" <?= $email_field_border_color ? "style='border-color: $email_field_border_color'": ""?>/>
							</li>	
							<li>
								<label for="phone">Phone:</label>
								<input type="tel" id="phone" name="info[phone]" value="<?=($info['phone'])?>" />
	                        </li>	
							<li>
								<label for="order_number">Order Number:</label>
								<input type="text" id="order_number" name="info[order_number]" value="<?=($info['order_number'])?>" />
	                        </li>	
							<li>
								<label for="topic">Topic:</label>
						        <select name="info[topic]" id="topic">
						<?
							$topics = split(',',$CFG->contact_us_topics);
							if(is_array($topics))
								foreach($topics as $topic){
									$s = ($info['topic'] == trim($topic) ? "selected='selected'" : '');
									echo "<option $s>".trim($topic)."</option>";
								}
						?>
						</select>
						</li>		
						<li>	
								<label for="message">Message:</label>
								<textarea id="message" name="info[message]" cols="30" rows="10" <?= $message_field_border_color ? "style='border-color: $message_field_border_color'": ""?>><?= ($info['message'])?></textarea>
                        </li>
						<li>	
								<input type="submit" class="button-brown add_to_cart_b"  value="submit" />
						</li>
                        </ul>
						</div>
					</fieldset>
				</form>
			</div>
		

<?
include( $CFG->redesign_dirroot . '/includes/footer.php');

?>