<? 
include_once ('application.php');

$info = explode ( "|", $_POST ['link'] );
$link = $info [0];
$sort = $info [1];

$action = htmlentities($_REQUEST ['action']);
$product_id = htmlentities($_POST ['product_id']);

if($action != 'recover_password'){echo "<META NAME='ROBOTS' CONTENT='NOINDEX, NOFOLLOW'>";}
 
switch ($action) {
	case 'create_account':
		createAccount($link);
		break;
	case 'sign_up':
		signUpForm();
		break;
	case 'forgot_password':
		?>
		<h2 class="title">Forgot Password</h2>
		<div class="forgot-password-form">
			<?
			MyAccount::forgotPassword('',true);
			?>
		</div>
		<?
		break;
	case 'recover_password' : 
		//MyAccount::forgotPassword($_REQUEST['email'],true);
		recoverPassword();
		break;
	default:
		showLoginForm();
		break;
}

function recoverPassword(){
	if(!$_REQUEST['email']){
		$return['error'] = 'Please enter an email address';
	} else {
		$return = MyAccount::recoverPassword($_REQUEST['email']);	
	}
	echo json_encode( $return );
}

function createAccount($link=''){
	if ($_POST ['info2']) { 
		$_POST ['info2'] ['first_name'] = $_POST ['info'] ['first_name'];
		$_POST ['info2'] ['last_name'] = $_POST ['info'] ['last_name'];
	}

	if (! $_POST ['info2'])
		$_POST ['info2'] = array ();
	if (! MyAccount::createAccount ( $_POST ['info'], $errors, $_POST ['info2'] )) {
		?>
		<script type="text/javascript" language="Javascript">
			alert('error');
		</script>
		<?
	} else {
		$_POST ['log'] = $_REQUEST ['info'] ['email'];
		$_SESSION ['new_account'] = 'Y';
		MyAccount::login ( $_REQUEST ['info'] ['email'], $_REQUEST ['info'] ['password'] );
		
		if (($link) && (strpos ( $link, '#review' ) !== false)) {
			$id = end ( explode ( '#review', $link ) );
			$link = preg_replace ( '/#review' . $id . '/', "?link=" . $id . '&sort=' . $sort, $link );
			$link =  $link . '#review' . $id;
		} else if($_POST['type'] === 'wishlist'){
			/* just make sure this person didn't add it before */
			$ret = WishList::get1_by_customer_and_product($_SESSION['cust_acc_id'], $_POST['product_id']);
			if (!$ret)
			{
				WishList::insert(array('customer_id' => $_SESSION['cust_acc_id'], 'product_id' => $_POST['product_id']));
			}
		}    
		//mail('tova@tigerchef.com','popuplogin',var_export($link,true));
		
		if ($link) {
			header ( "Location:" . $link );
		} else {
			?>
			<script type="text/javascript" language="Javascript">
				window.history.go(-2);
			</script>
			<?
		}
	}
}

function showLoginForm(){

	global $link;
	global $sort;
	global $CFG;
	?>
	<!--  BEGIN POPUP  -->
	<? $id=end(explode('#review', $link));?>
	<div id="login-box" class="login-popup">
		<div class="cart_popup"> 
			<form name="account_login_form" id="account_login_form" method="POST" action="">
				<input type="hidden" name="href" id="href" value="<?=$link?>">
				<?
				
				if($_REQUEST['type'] === 'checkout_login' || strpos($link,'checkout')!==false ){
					$checkout_login = true;
					?>
					<input type="hidden" name="checkout_page_login" id="checkout_page_login" value="Y">
					<?php
				}
				
				if($_REQUEST['type'] === 'answer_question'){
					?>
					<input type="hidden" name="base_url" id="base_url" value="<?=$CFG->baseurl?>">
					<input type="hidden" name="answer_question_login" id="answer_question_login" value="Y">
					<input type="hidden" name="answer_question_id" id="answer_question_id" value="<?=$_POST['question_id']?>">
					<?php
				}
				
				if($_POST['type'] === 'wishlist'){
					?>
					<input type="hidden" id="popup_product_id" value="<?=$_POST['product_id']?>">
					<input type="hidden" id="wishlist_login" value="Y">
					<h2>Please log in to add this item to your wishlist</h2>
					<?
				}elseif($_REQUEST['type'] === 'login' || $_REQUEST['type'] === 'checkout_login'){
					?>
					<h2>Sign In</h2>
					<?
				}else{
					?>
					<input type="hidden" name="link" value="<?=$id?>">
					<input type="hidden" name="sort" value="<?=$sort?>">
					<?php 
					if($_REQUEST['type'] === 'question'){
						?>
						<h2>Please log in to ask a question</h2>
						<?
					}else{
						?>
						<h2>Please log in to share your opinion</h2>
						<?
					}
				}
				?>
			
				<div class="cart_popup_box">
					<div class="errorBox" id="password_errors"
						style="display: none; width: 220px; font-size: 12px;"></div>
					<div class="form-input"><label>Email:</label> 
						<input class="input_box" name="log" id="log" type="email" />
					</div>
					<div class="form-input"><label>Password:</label>
						<input class="input_box" name="pwd" id="pwd" type="password" onkeypress="if (event.keyCode == 13) {document.getElementById('popup_sign_in_btn').click();return false;} else return true;" />
					</div>
					<div class="forget-link"><a href="#" onclick="TINY.box.fill('popuplogin.php?action=forgot_password&link=<?=$link?>&request=<?=$link?>',1,0,1);return false;">Forgot your password?</a>	</div>
					<div class="btn-holder">
						<a class="sign_in" id="popup_sign_in_btn" href="#"
							onclick="account_login();return false;">Sign In</a>
						<?php 
						if($_REQUEST['type'] !== 'checkout_login'){
						?>
							<a class="create_account popup-btn" id="popup_create_account_btn" href="#" onclick="$j('#login-box').css('display','none');$j('#login-new').css('display','block');TINY.box.size($j('#login-new').width(),$j('#login-new').height());return false">
								Create Account</a>
							<a class="create_account popup-btn mobile" id="popup_create_account_btn" href="/account.php?action=new_user">
								Create Account</a>
						<?php }?>
					</div>
					<div style="clear:both;"></div>
				</div>
			</form>
			
		</div>
	</div> 
	<?php signUpForm(true); ?>
	
	<!--  END POPUP  -->
	<?php 
}

function signUpForm($hidden = false){
?>
	<div id="login-new" class="login-popup cart_popup_box" <?=($hidden)?'style="display: none"':''?>> 
		<div class="title">Create Account</div>
		<div class="errorBox" id="signup_errors" style="display: none"></div>  
			<? MyAccount::newAccount('','','',true,$_POST['link']); ?>
	 	<?php 
	 	if($hidden){
		 	?>
		 	<div class="guest">
				<a id="guest" onclick="$j('#login-new').css('display','none');$j('#login-box').css('display','block');TINY.box.size($j('#login-box').width(),$j('#login-box').height());return false">&larr;Back</a>
			</div>
			<?php 
		}
		?>
	</div>
<?php 	
}
?>