function emailsignup()
{
    if($j('#email_address').val() == ''){
        alert("Please enter your email address");
        return;
    }
    var url = '/ajax/ajax.mailchimp.php';
	var industry = $j('#institution_type_signup').val();
    var data = {'email_address': $j('#email_address').val(), 'institution_type': industry, 'source': 'footer', 'promo_code' : 'false', 'action': 'subscribe'};

    var divname='#email_signup_result';

    $j.post(url,data,
         function(response)
        {
           if(response.result == 'failure'){
            $j(divname).html('There was a problem signing you up to our list');
           }
           else{
            $j(divname).html('You were successfully signed up to our list.');
            $j('.form-holder').hide();
           }
        } );
}

function emailsignup2(){}

function setListOrGridView(list_style)
{
	var url = '/ajax/ajax.set_list_or_grid_view.php?list_style='+list_style;
	$j.ajax(url, {
		type: "GET",
		success: function(returnObject) {
			// DO NOTHING
		},
		error: function(returnObject) {
			 alert('Sorry--could not switch view.');
		}
	});
	
}
function updateResultsNew( filter_name, filter_value, filter_name_2, filter_value_2)
{
    $j('#refine').append("<input type='hidden' name='"+filter_name+"' value='"+filter_value+"'>");
    if(typeof filter_name_2 != 'undefined' && typeof filter_value_2 != 'undefined')
    {
    	$j('#refine').append("<input type='hidden' name='"+filter_name_2+"' value='"+filter_value_2+"'>");
    }

    $j('#refine').action = $j('#refine').serialize();
    $j('#refine').submit();
}
function undoFilterSelection(filter_name)
{
    $j("#"+filter_name).val("");
    if (filter_name == "min_price") $j("#max_price").val("");
    $j('#refine').action = $j('#refine').serialize();
    $j('#refine').submit();
}

function cat_add_one_to_cart(product_id, sku, price, img_path, remaining, page_type, name) {
    removeCartPopup();
    $j('.error_msg').remove();

    var quantity = 0;
    var remaining = parseInt(remaining);
    var quantity_case = 0;
    var case_amt = 0;
    if ( $j("input[name='quantity[" + product_id + "]']").length > 0 ) {
        quantity = $j("input[name='quantity[" + product_id + "]']").val();
    }

    quantity = parseInt( quantity );

    var intRegex = /^\d+$/;
    if(!intRegex.test(quantity) || quantity < 1) {
    	$j("input[name='quantity[" + product_id + "]']").addClass('error_field').parent().append('<div class="error_msg">Please enter a quantity.</div>');
        return false;
    }

    /*if(quantity > remaining && remaining > 0){
    	$j("input[name='quantity[" + product_id + "]']").addClass('error_field').parent().append('<div class="error_msg">Currently you may only order ' + remaining + ' of this product.</div>');
        return false;
    }*/

    if ($j('#product_' + product_id + ' select.opts').length > 0) {
        if ($j('#product_' + product_id + ' select.opts')[0].selectedIndex == 0) {
        	var txt = new String($j('#product_' + product_id + ' select.opts')[0][0].text);
    	    var pos = txt.indexOf("Size")

    		if (pos >=0) alert ("Please select a size.")
    	    else alert("Please select a color.");


            $j('#product_' + product_id + ' select.opts').focus();
            return false;
        }
    }

    var option = $j("#option_" + product_id).val();

    if( $j('#product_' + product_id ).length && option == "" ) {
    	 var type = $j('#option_' + product_id).data('option-type').toLowerCase();
    	 alert('Please select a '+type+'.');
    	 return false;
    }
    var cartStats = {};
    var itemInfo = {product_id: product_id,
        quantity:quantity,
        price: price,
        name: name,
        sku: sku,
        img_path: img_path,
        option: option,
        selected_optional_options:'',
        customizations: '',
        presets: '',
        accessories:'',
        page_type: page_type};
    storeCartItem(itemInfo, cartStats);

    return false;
}

function updateCartCount(count){
	$j("#cart_count_header_mobile").html(count);
	if(count == 0) $j("#cart_count_header_mobile").addClass('empty');
		else{
			$j("#cart_count_header_mobile").removeClass('empty');
			if(count > 99) $j("#cart_count_header_mobile").addClass('three_digits');
			else $j("#cart_count_header_mobile").removeClass('three_digits');
		} 
	var items = (count ==1)?'':'s';
	$j("#cart_count_header").html("("+count+" item"+items+")");
}
function removeCartPopup() {
    if ($j("#cart_popup").length > 0) {
        $j("#cart_popup").remove();
    }
}
function showCartPopup(name, sku, quantity, img_path, cartStats, prod_id, option_id, selected_options) {

    updateCartCount(cartStats.item_count);
       
    $j(".cart_subtotal").each(function(){$j(this).html((cartStats.subtotal.toFixed(2)).toLocaleString());});

	    var cartContents = getCartHtml(name, sku, quantity, img_path, cartStats, prod_id, option_id, selected_options);
		
	    var close_button_clicked = true;

	    TINY.box.show({html:cartContents,
			boxid:'cart_popup_new',
			fixed:true,
			openjs:function(){ $j("#cart_popup_checkout").click(function() {
			        window.location.href = "/checkout.php";
			    });
			    $j("#cart_popup_view_cart").click(function() {
			    	sendToDataLayer('Cart Popup', 'Proceed to Checkout');
			        window.location.href = "/cart.php";
			    });
			    $j("#cart_popup_continue_shopping").click(function() {
			    	sendToDataLayer('Cart Popup', 'Continue Shopping');
			    	close_button_clicked = false;
			    	$j('#cart_popup_new .tclose').click();
			    });
				$j('#cart_popup_new .tclose').click(function() {
					if (close_button_clicked === true) sendToDataLayer('Cart Popup', 'Close X');
				});
			}
});
}
function getCartHtml(name, sku, quantity, img_path, cartStats, product_id, option_id,selected_options)
{
	var subTotal = new Number(cartStats.subtotal);
	subTotal = subTotal.toFixed(2).toLocaleString();
	var cartContents = "<div class='floating_cart_green_bar'>Item added to cart!</div>";
    var receivedHtml =$j(cartStats.ret_str);
    var desc_id ='#description_' + product_id + '_' + option_id;
    if(typeof(selected_options) != 'undefined' && selected_options != ','){
        desc_id  += selected_options.replace(/,/g,'_').replace(/_$/,'');
    }
	var option_description = receivedHtml.find(desc_id).html();
    //calculate sku from the cart html, the given sku may be wrong
    if(option_description){
         sku =receivedHtml.find(desc_id).parent().find('span[data-sku]').data('sku');
    }




	if (typeof cartStats.errors != "undefined"){
		if (typeof cartStats.errors.type != "undefined"){ cartContents += "<div class='error_msg'>"+cartStats.errors.msg+"</div>"; }
	}

		cartContents += "<div class='lside'><img class='floating_cart_prod_img' src='"+img_path+"'></div>";
	    cartContents += "<div class='rside'><span class='floating_cart_prod_name'>"+name+"</span>";
        if (option_description != null){
            cartContents += "<span class='floating_cart_prod_sku'>" + option_description + "</span>";
        }
	    cartContents += "<span class='floating_cart_prod_sku'>Item #: "+sku+"</span>";
	    cartContents += "<span class='floating_cart_prod_qty'>Quantity: "+quantity+"</span>";
	    cartContents += "<span class='floating_cart_prod_price'>Price each: "+cartStats.cart_price+"</span></div>";
	    cartContents += "<div class='floating_cart_summary'>Your cart now has " +cartStats.item_count + " item";
	    if (cartStats.item_count > 1) cartContents += "s";
	    cartContents += ".  <span class='floating_cart_summary_bold'>Total: $"+subTotal+"</span></div>";

	return cartContents+
			'<div class="btn-holder">'+
			'<button class="btn view_cart" id="cart_popup_view_cart">Proceed to Checkout</button>'+
			'<div class="continue-shop" id="cart_popup_continue_shopping">Continue Shopping</div>'+
			//	'<button class="btn checkout" id="cart_popup_checkout">Checkout</button>'+
			'</div>';

	/*return '<div class="shoping-cart">'+'<span id="cart_popup_x" class="cart_popup_right"></span>'+
	'<div class="shoping-cart-holder">'+
		'<ul class="shoping-cart-list">' +
		cartStats.ret_str +
		'</ul>'+
		'<div class="basket-holder">'+
			'<div class="btn-holder">'+
				'<button class="btn" id="cart_popup_continue_shopping">Continue Shopping</button>'+
				'<button class="btn checkout" id="cart_popup_checkout">Checkout</button>'+
			'</div>'+
			'<div class="total-price">'+
				'<span>Sub Total : '+ subTotal + '</span>'+
			'</div>'+
		'</div>'+
	'</div>'+
	'</div>';*/
}

function storeCartItem(itemInfo, cartStats) {
    var succeeded = false;

    itemInfo.option = (typeof itemInfo.option === undefined) ? "": itemInfo.option;
    itemInfo.selected_optional_options = (typeof itemInfo.selected_optional_options === undefined) ? "": itemInfo.selected_optional_options;
    itemInfo.presets = (typeof itemInfo.presets === undefined) ? "" : itemInfo.presets;
    itemInfo.option_quantities = (typeof itemInfo.option_quantites == undefined) ? "" : itemInfo.option_quantities;
    $j.ajax("/ajax/ajax.add_one_to_cart.php", {
        data: itemInfo,
        dataType: "json",
        type: "POST",
        success: function(returnObject) {

            cartStats.item_count = returnObject.count;
            cartStats.subtotal = returnObject.subtotal;
            cartStats.ret_str = returnObject.ret_str;
            cartStats.cart_price = returnObject.cart_price;
  if (returnObject.errors.length > 0) {
				var errors = jQuery.parseJSON(returnObject.errors);
				if(errors.type && errors.type.length > 0){
            		cartStats.errors = errors;
            		if(errors.type == 'min_qty' || errors.type == 'max_qty'){
            			succeeded = true;
            			cartStats.qty = errors.qty;
            		}
            	}else{
            		alert(errors.msg);
            	}
            	//$j('#quantity').addClass('error_field').parent().append('<div class="error_msg">' +limit+ ' of this product.</div>');
            }
            else {
                succeeded = true;
            }

            if(succeeded){
              
			  var name = itemInfo.name;
             /*   var name = "";

                if ($j("h1.product-title")) {
                    name = $j("h1.product-title").text();
                }*/
                if(cartStats.qty != undefined && cartStats.qty != itemInfo.quantity){
                    itemInfo.quantity = cartStats.qty;
                }

                var the_label = 'From: '+itemInfo.page_type+', Product: ' +name.trim();
                window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'addToCart','gaEventLabel': the_label,'ecommerce': {'add': {'actionField': {'list': itemInfo.page_type}, 'products': [{'name': name,'id': itemInfo.product_id, 'price': itemInfo.price,'quantity':itemInfo.quantity,'variant': itemInfo.selected_optional_options}]}}});
				//fbq('track', 'AddToCart', {content_type: 'product',content_ids: itemInfo.product_id ,value:itemInfo.price,currency: 'USD'});
                showCartPopup(name, itemInfo.sku, itemInfo.quantity, itemInfo.img_path, cartStats, itemInfo.product_id, itemInfo.current_product_option_id, itemInfo.selected_optional_options);
            }
            
        },
       error:function() {alert("something went wrong");}
    });

    return succeeded;
}

//Temporary - old
function updateOptionValue( product_id, option_id ,option_price, color_id)
{
    $j('#option_' + product_id).val(option_id);
    $j('.' + product_id + '_options').removeClass('selected');
    $j('img.no_reg.selected').removeClass('selected');
    $j('#' + option_id).addClass( 'selected' );

    if($j('.img_zoom').length){
    	$j('#zoom-gallery a').removeClass('active');
    	var ez = $j('#img_zoom').data('elevateZoom');

	    if($j('#option_color_' + color_id).length){
	    	$j('#option_color_' + color_id).parent().addClass('active');
	    	ez.swaptheimage($j('#option_color_' + color_id).parent().data('image'), $j('#option_color_' + color_id).parent().data('zoom-image'));
	    }else{
	    	$j('.main_image').addClass('active');
	    	ez.swaptheimage($j('.main_image').data('image'), $j('.main_image').data('zoom-image'));
	    }
    }
    option_price = $j.trim(option_price);
    if(option_price.charAt(0) == '$') option_price = "Our Price: " + option_price;
    $j('#one_pc_price_' + product_id).html(option_price.replace("Sale", "Sale Price"));
}
//Temporary - old

function reorderValues(values){
    all_vals.sort(function(a, b){return a-b});
    return ',' + all_vals.join(',') + ','
}

function getOptionalSelectedValues(){
    var presets = {};
    var selected_option_str = '';
    $j('.cart-box [data-required="0"]').each(function(){
        if(!$j(this).closest('.listing_available_colors_v1.option-selector-holder').is(':visible')){
            return true;
        }
        var this_type =$j(this).prop('type');
        var option_value_id = 0;
        if(this_type == 'hidden'){
            option_value_id = $j(this).val();
            if(option_value_id == ''){
                return true;
            }
        }
        if((this_type == 'checkbox' || this_type == 'radio') && !$j(this).is(':checked')){
            return true;
        }
        if(this_type == 'file' && $j(this).val() == ''){
            return true;
        }
        if(!option_value_id){
            option_value_id = $j(this).attr('id').replace(/[^0-9]/g,'');
        }
        selected_option_str += option_value_id + ',';
        var this_preset = $j(this).data('preset_value_id');
        if(Number(this_preset)> 0){
            presets[option_value_id] = this_preset;
        }
    });
    return [selected_option_str, presets];
}

//build a list of user input for option customization.
//If this is for validation, show errors and return false
//if required user input is missing
//Otherwise return object with customization information.
function getCustomizations(showError){
    if(typeof(showError) == 'undefined'){
        showError = false;
    }
    var are_customizations = true;
    var customizations = {};
    $j('.cart-box [name^="option_customization"]').each(function(){
       if(!$j(this).closest('.listing_available_colors_v1').is(':visible')){
            return true;
        }
        is_required = $j(this).data('required');
        value = $j(this).val();
        if(showError && is_required && value == ''){
            are_customizations = false;
            error_label = $j(this).data('label');
            var error_m = $j('<div/>').addClass("error_msg").text('Please ' + error_label );
            $j(this).before(error_m).focus();

            var this_container = $j(this).closest('.listing_available_colors_v1');
            this_container.addClass('error_message_container');
            slideTo(this_container);
 //           error_message_div.html('Please ' + error_label).focus();
   //         slideTo(error_message_div);
            return false;
        }
        if(value != ''){
            customizations[$j(this).data('option-value-id')] =
                [ $j(this).data('customization-type'), value];
        }
    });
    if(showError && !are_customizations){
        return false;
    }
    return customizations;
}

function getSetOptionValues(showErrors, forDisplay){
    if(showErrors == undefined){
        showErrors = true;
	    }
    if(forDisplay == undefined){
        forDisplay = false;
    }
    vals = new Array();
    presets = {};
    var all_set = true;
    error_message_div.hide();
    $j('.cart-box [data-required="1"]').each(function(){
        $j(this).prev('.error_msg').remove();
        if(!$j(this).parent().is(':visible')){
            return true;
        }
        if($j(this).val() == '' && $j(this).data('required-if-visible') == undefined){
            all_set = false;
            return forDisplay;
        }
        vals.push($j(this).val());
        if($j(this).data('preset_value_id')){
            presets[$j(this).val()] = $j(this).data('preset_value_id');
        }
    });
   if(!all_set && !forDisplay){
       return '';
    }
   $j.each(hidden_groups, function(group_id, isHidden){
        $j('#option_group_button_' + group_id).removeClass('disabled');
   });

    vals.sort(function(a, b){return a-b});
    return [',' + vals.join(',') + ',', presets];
            }
function stringToObj(str){
    var this_obj = {};
    var str_arr = str.split('&')
    for(var i = 0; i < str_arr.length; i++){
        this_val = str_arr[i].split('=')
        this_obj[this_val[0]] = this_val[1];
    }
    return this_obj;
       }

function slideTo(obj){
    var scroll_top = ($j(window).height() + 50);
    if($j('.header-block').length > 0){
        scroll_top = $j('.header-block').height() + 20;
    }
    $j('html,body').animate({
        scrollTop: obj.offset().top  - scroll_top},
         1000 );
}

var current_base_price = 0;

function displayAdditionalPrice(){
    //only display additional price
    //if all required options are set
    //i.e. there is a price already
    //displayed
    values = getSetOptionValues(false, true);
console.log('the values:', values);
   $j('#additional_charges').html('');
   $j('#total_charges').html('');
    var add_price_string = '';
    var total_price = current_base_price;
    try{
        var required_options_selected = values[0].replace(/^,/,'').replace(/,$/,'').split(',');
    }
    catch(TypeError){
        var required_options_selected = [];
    }
    var selected_options_and_presets = getOptionalSelectedValues();
    var presets = selected_options_and_presets[1];
    var all_selected_options = required_options_selected;
    if(selected_options_and_presets[0] != ''){
        all_selected_options = all_selected_options.concat(selected_options_and_presets[0].split(','));
    }
      $j.each(all_selected_options, function(index, option_value_id){
        if(presets.hasOwnProperty(option_value_id)){
            info = preset_info_obj[presets[option_value_id]];
        }
        else{
            info = option_info_obj[option_value_id];
        }
        try {
            additional_price = parseFloat(info.optional_additional_price);
            if(additional_price > 0){
                total_price += additional_price;
                add_price_string += '<span class=\'additional_price\'> + $' + info.optional_additional_price + '</span>'
                + '<span>for ' + info.price_text  + '</span><br/>';
            }
        }
        catch(TypeError){
            //no optional additional price, skip
        }
    });
   $j('#additional_charges').html(add_price_string);
   if(add_price_string != '' && current_base_price > 0){
       $j('#total_charges').html('$' + total_price.toFixed(2) + ' Total');
   }
   else{
        $j('#total_charges').html('');
   }
}
function toggleMeAndSib(obj, sib_class, show){
    var sibling = obj.next(sib_class);
    if(show){
        obj.slideDown();
        sibling.slideDown();
    }
    else{
       obj.find('input,textarea,select').each(function(){
            if($j(this).attr('type') != 'radio' && !$j(this).data('default') ==1){
           if($j(this).prop('type') == 'text' || $j(this).prop('tagName').toLowerCase() == 'textarea'){
               $j(this).val('Enter Text Here');
           }
           else {
               $j(this).val('');
           }
                if($j(this).attr('type') == 'hidden'){
                    if( $j(this).attr('id') != undefined){
                    //if this is a hidden for a select, we need to change the select to empty too.
                   var option_id = $j(this).attr('id').replace(/[^0-9]/g, '');
                   if($j('#option_select_' + option_id +'.dd-container').length > 0){
                       var selected_label =  $j('#option_select_' + option_id + '.dd-container').find('a.dd-selected');
                    selected_label.html('<label class="dd-selected_text">Please select</label>');
            }
                }
          }  }
       });
       clearPreview();
       obj.slideUp();
       sibling.slideUp();
    }
}

function isOptionValAndVisible(available_option_id){
    var option = $j('#option_select_' + available_option_id);
    if(!option.closest('.listing_available_colors_v1.option-selector-holder').is(':visible')){
        return false;
    }
    var option_type = option.prop('type');
    if((option_type == 'checkbox' || option_type == 'radio')
            && !option.is(':checked')){
        return false;
    }
    if(option.val() == ''){
        return false;
    }
    return true;
}

function toggleDependentChildren(available_option_id, parent_show, isRadio){
    //hide/show all direct descendants
    var show;
    //if the parent is hidden, don't check just hide
    if(parent_show != undefined && !parent_show){
       show = false;
    }
    else{
       show = isOptionValAndVisible(available_option_id);
    }

    $j('[data-child-row="' + available_option_id + '"]').each(
        function(){
            toggleMeAndSib($j(this), '.clear_option_selects', show);
            //recursively hide all children
            $j(this).find('[id^="option_select_"]').each(function(){
                toggleDependentChildren($j(this).attr('id').replace(/option_select_/, ''), show);
            });
    });
    //radios do not trigger change on de-select, so if one
    //radio was selected, check the rest
    if(!isRadio && $j('#option_select_' + available_option_id).attr('type') == 'radio'){
        var radio_name = $j('#option_select_'+ available_option_id).attr('name');
        $j('[name="' + radio_name + '"]').each(function(){
            var this_id = $j(this).attr('id').replace(/option_select_/, '');
            if(this_id != available_option_id){
                toggleDependentChildren(this_id, show, true);
        }
        });
    }
    //after everything is toggled, make sure the file upload is toggled too
    window.setTimeout(function(){hideFileUploads();},500);
}

function toggleOptionGroup(button_obj){
    if(button_obj.hasClass('disabled')){
        return;
    }
    var div_to_toggle = button_obj.next('.price-info-holder .cart-box .option_group_container');
    if($j(div_to_toggle).is(':visible')){
        button_obj.text( button_obj.text().replace(/Hide/,'Add'));
    }
    else{
        button_obj.text(button_obj.text().replace(/Add/, 'Hide'));
    }
    div_to_toggle.slideToggle(function(){
        //when option are hidden, hide their prices,
        //and vice versa
        displayAdditionalPrice();
        hideFileUploads();
      });
}

function hideFileUploads(){
    var all_hidden = true;
    $j('#files .option_container').each(function(){
        this_option_id = $j(this).data('option-id');
        if($j('label[for="option_customization_' + this_option_id + '"]').is(':visible')){
            all_hidden = false;
            $j('#upload_container').show();
            $j(this).slideDown();
        }
        else{
            $j(this).slideUp();
        }
       });
    if(all_hidden){
        $j('#upload_container').hide();
    }
}

var is_option_product = false;

function updateOptionData(result){
    is_option_product = true;
     var vals = getSetOptionValues(false, true);
     if(vals != '' && result != undefined){
            $j('button.btn-add').off('click');

           $j.post('ajax/ajax.product_info.php',
        {product_id : result.product_id,
         action : 'get_option_price_and_id',
         option_values : vals[0],
         presets: JSON.stringify(vals[1])},
            function(data){
                if(typeof(data) != 'object'){
                    data = $j.parseJSON(data);
                }
                if(data == null){
               $j('button.btn-add').off('click').bind('click', add_this_product_to_cart);
                    return;
                }
                 if(data.price > 0){
                    current_product_option_id = parseInt(data.id);
                   var parts = data.price.split('.');
                    var price_html = parts[0] + '.<sup>' + parts[1] + '</sup>';
                    var price_display = '<span itemprop="price" class="calculated">$'+ price_html + '</span>';
                    $j('#the-price').removeAttr('itemprop').html(price_display);
                   if(data.price_per_piece > 0){
                        if($j('.one_piece_price').length > 0){
                            $j('.one_piece_price').html('$' + data.price_per_piece);
                        }
                    }

                current_base_price = parseFloat(data.price);
                displayAdditionalPrice();
                }
               $j('button.btn-add').off('click').bind('click', add_this_product_to_cart);
            });
       if(options_with_previews.hasOwnProperty(result.option_id)){
            showPreview();
       }
        displayAdditionalPrice();
     }
    else{
        displayAdditionalPrice();
      try{
 if(options_with_previews.hasOwnProperty(result.option_id)){
            showPreview();
       }
}
catch(err){
}
}
         }

function showPreview(){
    var message = '';
    var customizations = getCustomizations(false);
    $j.each(customizations, function(key, vals){
        if(vals[0] == 'text'){
            message += vals[1] + "\r\n";
        }
    });
    if(message == ''){
        return;
    }
    var colors_to_get = '';
    var vals_to_get = '';
    $j.each(options_with_previews, function(key, val){
        var obj = $j('#option_' + key);
        if(!obj.parent().is(':visible')){
            return true;
        }
        var is_preset = obj.data('has-preset');
        var selected_val;
        if(is_preset){
            selected_val = obj.data('preset_value_id');
            result = preset_info_obj[selected_val];
        }
        else{
            selected_val = obj.val();
            result = option_info_obj[selected_val];
        }
        if(result != undefined && result.color_id != 0){
            colors_to_get += result.color_id + ',';
        }
        else if(result != undefined){
            vals_to_get += result.value_id + ',';
        }
    });
    colors_to_get = colors_to_get.replace(/,$/, '');
    vals_to_get = vals_to_get.replace(/,$/, '');

    var preview_obj = $j('.option_preview .preview_display:visible');
    if(colors_to_get.length > 0){
        $j.each(colors_to_get.split(','), function(key, val){
            this_info = colors_to_values[val];
            if(this_info[0] == 'bgcolor'){
                preview_obj.css('color', this_info[1]);
//                preview_obj.css('background', '#EEE');
            }
            else{
                preview_obj.css('background', 'url(' + this_info[1] + ')');
            }
        });
    }
    if(vals_to_get.length > 0){
        $j.each(vals_to_get.split(','), function(key, val){
            preview_obj.css('font-family', fonts_to_values[val] );
        });
    }
    preview_obj.html(nl2br(message)).show();
}

function clearPreview(){
    $j('.option_preview .preview_display:visible').html('Preview Area'
        ).css('color', '#000').css('font-family', 'inherit');
}

function processOptionChange(option_value_id, is_preset){
    //when passed in an input id instead of actual
    //option value id, strip text
    error_message_div.hide();
    $j('.error_message, .error_msg').each(function(){$j(this).remove();});
     $j('.error_message_container').each(function(){$j(this).removeClass('error_message_container');});
   var this_id = option_value_id;
    if(typeof(option_value_id) == 'string'){
        this_id = Number(option_value_id.replace('option_select_', ''));
    }
   if(preset_info_obj.hasOwnProperty(this_id)){
        result = preset_info_obj[this_id];
    }
    else{
        result = option_info_obj[this_id];
    }
    updateOptionData(result);
    toggleDependentChildren(this_id);
}

function getOptionInfo(info_str){
    result = {};
    info_array = info_str.split("&");
    $j.each(info_array, function(index, value){
        var this_array = value.split('=');
        result[this_array[0]] = this_array[1];
    });
    return result;
}

function updateOptionValueDdslick( selected )
{
    var result = getOptionInfo(selected.value);
       if($j('.img_zoom').length){
            $j('#zoom-gallery a').removeClass('active');
            if($j('#option_color_' + result.color_id).length){
                $j('#option_color_' + result.color_id).parent().addClass('active');
                change_image($j('#option_color_' + result.color_id).parent());
	}else{
                $j('.main_image').addClass('active');
                change_image($j('.main_image'));
            }
       }
      $j('#option_' + result.option_id).val(result.option_value_id).parent().parent().find('.error_field').removeClass('error_field');
        val_to_send = result.option_value_id;
        is_preset = false;
        if(result.preset_value_id != ''){
            $j('#option_' + result.option_id).data('preset_value_id', result.preset_value_id);
            val_to_send = result.preset_value_id;
            is_preset = true;
        }
        else{
            $j('#option_' + result.option_id).removeData('preset_value_id');
        }
    processOptionChange(val_to_send, is_preset);
}


function updateOptionValueDdslick_old( selected )
{
	var result = {};

	if(selected.value.indexOf("&") > -1){

	    var selected_info_array = selected.value.split("&");

	    for (var i = 0; i < selected_info_array.length; i++){
	        result[selected_info_array[i].substring(0, selected_info_array[i].indexOf('='))] = selected_info_array[i].substring(selected_info_array[i].indexOf('=')+1);
	    }

	    if($j('.img_zoom').length){
	    	$j('#zoom-gallery a').removeClass('active');

		    if($j('#option_color_' + result.color_id).length){
		    	$j('#option_color_' + result.color_id).parent().addClass('active');
		    	change_image($j('#option_color_' + result.color_id).parent());
		    }else{
		    	$j('.main_image').addClass('active');
		    	change_image($j('.main_image'));
		    }
	    }
	    option_price = $j.trim(result.option_price);

//	    if(option_price.charAt(0) == '$') result.option_price = result.option_price;
//	    $j('#one_pc_price_' + result.product_id).html(result.option_price.replace("Sale", ""));
	    result.option_price = result.option_price.substr(result.option_price.indexOf('$')) ;
	    $j('#one_pc_price_' + result.product_id +' #the-price').html(result.option_price);
	    if(result.option_case_price){
	    	$j('#one_pc_case_price_' + result.product_id).html(result.option_case_price);
	    }

	    $j('#option_' + result.product_id).val(result.option_id);

	}else{
		$j('#option_' + selected.value).val('');
	}
}
function updateOptionValueDropdown(product_id, product_price)
{
    var selected_option_text = $j('#option_' + product_id + ' option:selected').text();
    var selected_option_class = $j('#option_' + product_id + ' option:selected').attr('class');
    var selected_option_text_array = selected_option_text.split("-");
    var option_price = $j.trim(selected_option_text_array[selected_option_text_array.length - 1]);

    if(option_price.charAt(0) == '$') option_price = "Our Price: " + option_price;
    if (selected_option_class != '') option_price = selected_option_class + option_price;
    //alert('#one_pc_price_' + product_id);
    if ($j('#option_' + product_id).attr("selectedIndex") != 0) $j('#one_pc_price_' + product_id).html(option_price.replace("Sale", "Sale Price"));
    else $j('#one_pc_price_' + product_id).text("Our Price: $ " + product_price);
}
function compare_products(cat_id)
{
    var compare_list    = document.getElementsByName('compare[]');
    var to_compare      = [];
    var params          = "?cat_id=" + cat_id;
    var x = 0;

    for (var i = 0; i < compare_list.length; i++)
    {
        if (compare_list[i].checked)
        {
            to_compare[i] = compare_list[i].value;
            x++;
            params = params + "&compare[]=" + compare_list[i].value;
        }
    }

    if( x < 2 )
    {
        alert('Please choose at least 2 products to compare.');
        return false;
    }

    window.location = '/compare.php' + params;
}
function remove_from_compare(product_id)
{
	var window_location = window.location.toString();
	var new_location = window_location.replace("&compare[]="+product_id, "");
	var new_location = window_location.replace("&compare%5b%5d="+product_id, "");

	window.location = new_location;
}
/*
*****************************************************************
** cart.php functions
*****************************************************************
*/

function valid_cart_quantities(frm)
{
    var regex = /cart\[\d+\]\[qty\]/i;

    /* want to make sure we have a valid number in the input fields */
    for(i = 0; i < frm.elements.length; i++)
    {
        elem = frm.elements[i];

        if (elem && elem.name != 'undefined' && elem.name != null && elem.name.search(regex) != -1)
        {
            if (elem.value < 0)
            {
                return false;
            }
        }
    }

    return true;
}

function valid_zip_code(zip_code)
{
  
    var zcode_re = /^\d{5}([\-\s]?\d{4})?$/;

    if (!zcode_re.test(zip_code.trim()))
    {
        return false;
    }

    return true;
}

function set_zip(){
    $j.get('ajax/ajax.cart.php',
        {'action': 'get_zip'},
        function(data){
            data = JSON.parse(data);
            zip = data.zip;
            $j('input[name="zip_code"]').val(zip);
        }
    );
}
function secure_checkout_submit()
{

	var frm         = $j('#cart_form');
	var zip_code    = $j('#zip_code').val();
	var logged_in    = $j('#logged_in').val();
	var liftgate     = $j('#liftgate_amt');

	/* make sure we have valid quantities in the cart */
	// valid_cart_quantities
	/*if (!valid_cart_quantities(frm))
	{
	    alert("Quantity must be 0 or greater!");
	    return false;
	}*/

	/* now make sure we have a valid zip code */
	/*if (!valid_zip_code(zip_code))
	{
		alert("Please enter a valid zip code.");
		Effect.Pulsate('zip_code',{pulses:3,duration:1});
	    return false;
	}*/

	/*if (liftgate){
		if(!$(liftgate_rdbtn_Y).checked && !$(liftgate_rdbtn_N).checked){
			alert("Please select whether a liftgate should be provided for delivery.");
			return false;
		}
	}*/
	$j('#cart_action').val('secure_checkout');
	
	frm.submit();

    return false;
}

function checkout_paypal_submit(type)
{
	$j('.error_msg').remove();
	$j('#checkout_paypal_button').attr('disabled', true);

	if(type == 'cart'){
		var frm         = document.getElementById('cart_form');
		var zip_code    = document.getElementById('zip_code').value;

		/* make sure we have valid quantities in the cart */
		if (!valid_cart_quantities(frm))
		{
		    alert("Quantity must be 0 or greater!");
		    return false;
		}
	}

		//set up paypal call
	$j.ajax("/ajax/ajax.checkout_functions.php", {
		data: {
			action  	: 'setup_paypal_express_checkout',
			type		: type,
            shipping_email : $j('#shipping_email').val()
		},
		dataType: "json",
		type: "POST",
		beforeSend : function(){
			if(type == 'cart'){
				$j('.checkout_paypal_button').append('<div class=loader-small></div>');
				//$j('.loader-small').css("height", "24px");
				//$j('.loader-small').css("width","100%");
				//$j('.loader-small').css("float","right");
				//$j('.loader-small').css("background-position","50% 0px");

			}else{
				//$j('#account-details').append('<div class="step-holder-loader"></div>');
				$j('body').addClass('loading');
			}
        },
		success: function(returnObject) {

			if(returnObject.errors){
				$j('.loader-small').remove();
				$j('.step-holder-loader').remove();

				$j('#paypalErrors').html(returnObject.errors).removeClass('hidden');
				$j('#confirm_order_errors').html(returnObject.errors).removeClass('hidden');
				$j('#checkout_paypal_button').attr('disabled', false);
			} else {

			  if (type == 'checkout'){
				  //window.dataLayer = window.dataLayer || [];
				  //dataLayer.push({'event': 'checkout','ecommerce': {'checkout': {'actionField': {'step': 3}}}});
				}
				window.location = returnObject.redirect;
			}
		},
		error: function() {
			$j('.loader-small').remove();
			$j('#paypalErrors').html('something went wrong').removeClass('hidden');
			$j('#confirm_order_errors').html('something went wrong').removeClass('hidden');
		}
	});

    return false;
}
// this functions no longer used?
function update_zip()
{
    var frm         = $('cart_form');
    var zip_code    = $('zip_code').value;

    if (!valid_zip_code(zip_code))
    {
        alert("Please enter a valid zip code.");
        return false;
    }

    /* set the action */
    frm.action.value = "show_radios";

    /* submit the form */
    frm.submit();

    return true;
}
function update_shipmethod()
{
	var frm         = $('cart_form');
	var ship_method = '';
	/* set the action */
    var regex = /ship_method/i;

    /* want to make sure we have a valid number in the input fields */
    for(i = 0; i < frm.elements.length; i++)
    {
        elem = frm.elements[i];

        if (elem.name.search(regex) != -1)
        {
        	if (elem.checked) ship_method = elem.value;
        }
    }

    frm.action.value = "calc_zip";

    /* submit the form */
    frm.submit();

    return true;
}

function update_cart(cart_line_id)
{
	var action,item,cart;
	if(cart_line_id > 0){
		var data = {
				action	: 'remove_from_cart',
				item	: cart_line_id
		};
	}else{
		var data = {
				action	: 'update_cart',
				cart	: $j('.col-3 > input').serialize()
		};
	}


	$j('.error_field').removeClass('error_field');
	$j('.error_msg').remove();

    /* make sure we have valid quantities in the cart */
    $j('.col-3 > input').filter(function() {
        return this.value < 0 ;
    }).addClass('error_field').parent().prepend('<div class="error_msg">Quantity must be 0 or greater</div>');

	//show liftgate options only for checkout
	data.show_liftgate = 0;
	if($j('.checkout').length>0){
		data.show_liftgate = 1;
	}

    $j.ajax("../ajax/ajax.cart.php", {
		data: data,
		dataType: "json",
		type: "POST",
		beforeSend: function(){
            // this is where we append a loading image
			$j('.main-section').append('<div class="step-holder-loader"></div>');
          },
		success: function(returnObject) {
            $j('.cart-items-section').load(document.URL +  ' ul.shoping-list');
			var cart = $j('.shoping-list').parent();
			var result = eval(returnObject);

			if(result.item_count >0){
				
				if($j('.checkout').length>0){
					$j('#sidecart').stickyScroll({ container: '#checkout-container' });
				}

				if(result.error){
					cart.prepend("<div class='error_msg'>"+result.error+"</div>");
				}
				if (result.cost_info.shipping_amt > 0)
				   $j('.shipping_amt span.total-value').html('$' + result.cost_info.shipping_amt);
				$j('span.total-value.final.grand_total').html('$' + result.cost_info.grand_total);
				$j('div.total-row.sub_total span.total-value').html('$' + result.cost_info.sub_total);
				$j('#sales_tax').html('$' + result.cost_info.sales_tax);
				$j('.coupon_gift span.total-value').html("-$" + result.cost_info.coupon_gift);
				if(result.shipping_options){
					$j('#shipping_cost, #shipping-method-options').html(result.shipping_options);
					var costTable= result.shipping_options;
					var costTable= result.shipping_options;
					var message=$j( "#discount_message" ).text();
			        $j( ".discount_message" ).remove();
					var thispage=window.location.pathname;

		            if (costTable.indexOf("discount_message") >= 0){
			    	    
						if (thispage.indexOf('cart') >= 0){
			                $j("<div class='discount_message'>"+message+"</div>").prependTo("#shipping_discount_message");
			          }
			    	
					  if (thispage.indexOf('checkout') >= 0){
			            $j("<div class='discount_message'>"+message+"</div>").prependTo("#shipping-method-options");
			    	  }
			        }
				}
				$j('.essensaMessageBox').html(result.essensaMessage);

			}else{
                $j.get('ajax/ajax.checkout_functions.php',
                    {'action': 'continue_shopping'},
                    function(data){
                        data = JSON.parse(data);
                        link = data.link;
                        $j('.main-section').html("<div class='no_items'>THE SHOPPING CART IS CURRENTLY EMPTY <a class='button-brown' href='"+link+"'>Continue Shopping</a> </div> ");
                    }
                );

			}
			//update the bar with cart count and total
			updateCartCount(result.item_count);			
		    $j("#cart_subtotal_header").each(function(){$j(this).html((result.subtotal.toFixed(2)).toLocaleString());});
			$j('.step-holder-loader').remove();

		},
		error: function(xhr, status, thrownError) {
			//console.log(xhr.statusText);
			//console.log(xhr.responseText);
			//console.log(xhr.status);
			//console.log(thrownError);
			$j('.step-holder-loader').remove();
			$j('.cart-cost-summary').parent().prepend("<div class='error_msg'>something went wrong</div>");
		}
	});

    return false;
}

function init_sync_cart(){
	$j('.shoping-list input').on('blur',function(e){
		var field = $j(this).attr('name');
		if($j(this).parents('#sidecart').length){
			$j('.products-list.mobile .products-table [name="'+ field+'"]').val($j(this).val());
		}else{
			$j('#sidecart [name="'+ field+'"]' ).val($j(this).val());
		}
	});
	return true;
}

function checkCouponCode(){
	var frm = $('cart_form');
	var val = $("id_coupon_code").value;

	if(val == ''){
		alert('Invalid coupon code.');
		return false;
	}

	$("cart_action").value = 'apply_coupon_code';

	return true;
}
function applyRewards(type, variation)
{
	$j('.error_msg').remove();
	var frm = $('cart_form');
	var max_rewards = parseFloat($('max_rewards').value);

	var rewards_using = parseFloat($('rewards_using').value);

	var sub_total = parseFloat(jQuery('.sub_total').html().replace(/[$,]/g, ''));
	var cur_total = parseFloat(jQuery('.grand_total').html().replace(/[$,]/g, ''));
	var discount = parseFloat(jQuery('.coupon_gift').html());
    if(discount){discount.replace(/[$,]/g, '');}
	var rewards_applied = ($('rewards_applied'))?parseFloat($('rewards_applied').value):0;

	var grand_total = (cur_total + rewards_applied) * 100 ;

	if(type != "undo" && rewards_using % 1 !== 0){
		var error_msg = 'Please enter whole number';
		$j('#rewards-message').prepend("<div class='error_msg'>"+error_msg+"</div>");
		$('rewards_using').value = '';
		return false;
	}

	if(type == "undo"){
		rewards_using = 0;
	}else{
		if(rewards_using > max_rewards){

			var error_msg = "You do not have " + rewards_using + " points to apply. Your available point balance is "
			+ max_rewards + " points.";
			$j('#rewards-message').prepend("<div class='error_msg'>"+error_msg+"</div>");

			return false;
		}

		if(rewards_using > ((sub_total - discount) *100)){

			var error_msg = "You can not apply more points than the (subtotal - discounts).";
			$j('#rewards-message').prepend("<div class='error_msg'>"+error_msg+"</div>");

		    return false;
		}
	}


    $j.ajax("../ajax/ajax.cart.php", {
		data: {
			action  	: 'apply_rewards',
			rewards_using : rewards_using,
			variation: variation
		},
		dataType: "json",
		type: "POST",
		beforeSend: function(){
            // this is where we append a loading image
			$j('#rewards-message').prepend('<div class="loader"></div>');
          },
		success: function(returnObject) {

			$j('.loader').remove();
			$('rewards_using').value = '';

			var result = eval(returnObject);

			if(result.error){
				$j('#rewards-message').prepend("<div class='error_msg'>"+result.error+"</div>");
			}else{

				$j('#rewards-message').html(result.rewards_message);
				if(result.cost_info){
					//update costs
					for(var index in result.cost_info) {
						if(index == 'coupon_gift' || index == 'rewards_redeemed'){
							$j('.' + index).html('- $' + result.cost_info[index]);
						}
						else{
							$j('.' + index).html('$' + result.cost_info[index]);
						}
					}
					$j('#shipping_amt').html(result.cost_info.shipping_amt);
					$j('.cart-cost-summary').html(result.cost_summary);
					$j('#total_order_m').html(result.cost_info.grand_total);
					if ($j(window).width() < 768)
				        $j('#sub_total').html(result.subtotal.toFixed(2));
				}
			}

		},
		error: function(returnObject) {
			console.log(returnObject);
			$j('#rewards-message').html('something went wrong');
		}
	});

	//$("cart_action").value = 'apply_rewards';
	//frm.submit();

	return true;
}

function clear_rewards_field(fld, e)
{
    var max_rewards = $("max_rewards").value;
    /* put the text back if we lose focus and the field value is blank */
    if (e.type == 'blur' && (fld.value == null || fld.value == ''))
    {
        fld.value = 'Enter Points';
    }
    /* on focus we want to clear out the field */
    else if (e.type == 'focus' && (fld.value == max_rewards || fld.value == 'Enter Points'))
    {
        fld.value = "";
    }

    return true;
}

function calculate_shipping(zip_code, action, product_id)
{
	$j('.error_msg').remove();
	
    if (valid_zip_code(zip_code))
    {
		
    	var shipping_location = $j('input[name=shipping_location]:checked');
    	if(shipping_location != undefined){
    		shipping_location = shipping_location.val();
    	}else{
    		shipping_location = '';
    	}
       
    	var quantity = $j('#quantity').val();
    	if(product_id){
    		$j('#quantity').removeClass('error_field');
    		var intRegex = /^\d+$/;
            if(!intRegex.test(quantity) || quantity < 1) {
            	$j('#quantity').addClass('error_field').parent().append('<div class="error_msg">Please enter a quantity.</div>');
                return false;
            }
    	}

    	var method;
    	var button_type;
    	//radio buttons
    	if($j('input[name=shipping_method]:checked').length > 0){
    		method = $j('input[name=shipping_method]:checked').val();
    		button_type = 'radio';
    	}else if($j('.shipping-button').length > 0){
    	// buttons
    		method = $j('.shipping-button.active').data('shipping-method');
    		button_type = 'buttons';
    	}else{
			method = '' ;
		}

    	var as_radio_buttons;
		//for now use only radio buttons
		as_radio_buttons = 1;
		/*
    	if($j('#version-2').length>0){
    		as_radio_buttons = 0;
    	}else{
    		as_radio_buttons = 1;
    	}
		*/

		var show_liftgate = 0;
		if($j('.checkout').length>0){
			show_liftgate = 1;
		}
		var shipping_address = {address1:$j('#shipping_address').val(), address2:$j('#shipping_address2').val(), city:$j('#shipping_city').val(), state:$j('#shipping_state').val(), zip:zip_code};
		
	    $j.ajax("/ajax.calculate_shipping.php", {
			data: {
				action  	: action,
				zip 		: zip_code,
				product_id 	: product_id,
				qty			: quantity,
				method		: method,
				shipping_location : shipping_location,
				as_radio_buttons : as_radio_buttons,
				show_liftgate : show_liftgate,
                shipping_address :shipping_address
			},
			dataType: "json",
			type: "POST",
			beforeSend: function(){
	            // this is where we append a loading image
				$j( ".discount_message" ).remove();
				if (action !='checkout')
				    $j('#shipping_cost').append('<div class="step-holder-loader"></div>');
				
	          },
			success: function(returnObject) {
				
			console.log(returnObject);
				var result = eval(returnObject);
				var costTable= result.shipping_options;
				$j('#shipping_cost').html(result.shipping_options);
		        $j( ".discount_message" ).remove();
		        var thispage=window.location.pathname;

		        if (costTable.indexOf("discount_message") >= 0){
			    	if (thispage.indexOf('cart') >= 0){
			            $j("<div class='discount_message'>"+message+"</div>").prependTo("#shipping_discount_message");
			        }
			    	if (thispage.indexOf('checkout') >= 0){
			            $j("<div class='discount_message'>"+message+"</div>").prependTo("#shipping-method-options");
			    	}
			    }
                var tax_amount=Number(result.cost_info.sales_tax.replace(/\,/g,''));
				//if(action == 'cart'){ 
						if(tax_amount > 0){
							$j('div.total-row.sales_tax').removeClass('hidden');
							$j('#sales_tax').html('$' + result.cost_info.sales_tax);
						}else{
								$j('div.total-row.sales_tax').addClass('hidden');
						}
					$j('div.total-row.sub_total span.total-value').html('$' + result.cost_info.sub_total);
					if (result.cost_info.liftgate_amt > 0)
					$j('div.cart-cost-summary span.liftgate_amt').html('$' + result.cost_info.liftgate_amt);
					$j('#liftgate_amt.total-value').html('$' + result.cost_info.liftgate_amt);
					
					$j('.shipping_amt span.total-value').html('$' + result.cost_info.shipping_amt);
					
					if (result.cost_info.shipping_amt > 0)
					    $j('.total-value.shipping_amt').html('$' + result.cost_info.shipping_amt);
					
					$j('span.total-value.final.grand_total').html('$' + result.cost_info.grand_total);

			    //}
				

				if($j('#version-2').length>0){
					if($j('#shipping-method-options .error_msg').length>0){
						$j('#shipping_zip').parent().find('.label_msg').addClass('hidden');
						$j('#shipping_zip').addClass('checkout_error_field');
						sendToDataLayer('Checkout Error', 'Shipping Method', $j('#shipping-method-options .error_msg').text());
					}
				}
				 $j('.step-holder-loader').remove();

			},
			error: function(returnObject) {
				//console.log(returnObject);
				$j('#shipping_cost').html('something went wrong');
			}
		});
	  return true;
    }
    else
    {
    	if(zip_code !== ''){
    		$j('#zip_code').parent().append("<div class='error_msg'>Zip code entered, "+zip_code+", is not a valid US zipcode.</div>");
    	}else{
    		$j('#zip_code').parent().append("<div class='error_msg'>No zip code entered.</div>");
    	}
        return false;
    }
}
function coupon_code_action(action,coupon_code,variation){

	$j('.error_msg').remove();
    if (coupon_code)
    {

	    $j.ajax("../ajax/ajax.cart.php", {
			data: {
				action  	: action,
				coupon_code : coupon_code,
				variation : variation
			},
			dataType: "json",
			type: "POST",
			beforeSend: function(){
	            // this is where we append a loading image
				$j('#coupon-list').prepend('<div class="loader"></div>');
				$j('.error_msg').remove();
	          },
			success: function(returnObject) {

				$j('.loader').remove();
				$j('id_coupon_code').value = '';

				var result = eval(returnObject);

				if(result.error){
					$j('#coupon-list').parent().prepend("<div class='error_msg'>"+result.error+"</div>");
				}else{
					$j('#coupon-list').html(result.coupon_list);

					if(result.shipping_options){
						$j('#shipping_cost, #shipping-method-options').html(result.shipping_options);
					}

					//update costs
					for(var index in result.cost_info) {
						if(index == 'coupon_gift' || index == 'rewards_redeemed'){
							$j('.' + index).html('- $' + result.cost_info[index]);
						}
						else{
							$j('.' + index).html('$' + result.cost_info[index]);
						}
					}
					$j('#shipping_amt').html(result.cost_info.shipping_amt);
					$j('#sales_tax').html(result.cost_info.shipping_amt)
					$j('.totals-section').html(result.cost_summary);
				    $j('#total_order_m').html(result.cost_info.grand_total);
					if ($j(window).width() < 768)
					 $j('#sub_total').html(result.subtotal.toFixed(2));
					$j('.essensaMessageBox').html(result.cost_info.essensaMessage);
				}

			},
			error: function(returnObject) {
				//console.log(returnObject);
				$j('#coupon-list').html('something went wrong');
			}
		});

    }
    else
    {
    	$j('#id_coupon_code').parent().append("<div class='error_msg'>No code entered.</div>");
        return false;
    }


    return true;
}


/*
*****************************************************************
** Cart Step 3 - calculate shipping
*****************************************************************
*/
function calcShippingStep3(oSelect, shipping_discount, tax_rate){

	var method;
	//radio buttons
	if($j('input[name=shipping_method]:checked').length > 0){
		method = $j('input[name=shipping_method]:checked').val();
		//$j('#shipping-method-options #' + oSelect.id +', .shipping_cost #' + oSelect.id).prop("checked", true);
	}else{
	// buttons
		method = $j(oSelect).data('shipping-method');
		$j('.shipping-button').removeClass('active');
		$j(oSelect).addClass('active');
	}
	//var cur_total = jQuery('#cur_total').html();

	// give shipping discount only for ground shipping
	var method = $j('input[name=shipping_method]:checked').val();
	if(method !== 'GND'){
		shipping_discount = 0;
	}

	var sub_total = jQuery('.sub_total span.total-value').html();
	var coupon_gift = jQuery('.coupon_gift span.total-value').html();
	var rewards_redeemed = jQuery('.rewards_redeemed').html();
	var min_charge = jQuery('.min_charge').html();
	var sales_tax = jQuery('#sales_tax').html();
	var liftgate_amt = jQuery('#liftgate_amt').html();
	if (sub_total == null) sub_total = "0.00";
	if (coupon_gift == null) coupon_gift = "0.00";
	if (rewards_redeemed == null) rewards_redeemed = "0.00";
	if (min_charge == null) min_charge = "0.00";
	if (sales_tax == null) sales_tax = "0.00";
	if (liftgate_amt == null || liftgate_amt=="NaN") liftgate_amt = "0.00";
	sales_tax = parseFloat(sales_tax.replace(/[$,]/g, ''));
	sub_total = parseFloat(sub_total.replace(/[$,]/g, ''));
	rewards_redeemed = parseFloat(rewards_redeemed.replace(/[$,-]/g, ''));
	coupon_gift = coupon_gift.replace(/[$,]/g, '');
	coupon_gift = coupon_gift.replace(' ', '');
	coupon_gift = parseFloat(coupon_gift.replace('-', ''));
	min_charge = parseFloat(min_charge.replace(/[$,]/g, ''));
	liftgate_amt = parseFloat(liftgate_amt.replace(/[$,]/g, ''));
    //alert (sub_total + " " + coupon_gift + " " + min_charge + " " + sales_tax);

	$j('.error_msg').remove();


	/* show the data */
	var standard_shipping = $j(oSelect).data('extra');
	var freight = $j('.freight_amt').html();
	if (standard_shipping == null) standard_shipping = "0.00";
	if (freight == null) freight = "0.00";
	standard_shipping = parseFloat(standard_shipping.toString().replace(/[$,]/g, ''));
	freight = parseFloat(freight.replace(/[$,]/g, ''));

	var new_shipping = standard_shipping + freight;

	new_shipping = new_shipping - shipping_discount;
	if (new_shipping == 0){
		new_shipping = "0.00";
	}else{
		new_shipping = new_shipping.toFixed(2);
	}

   
	$j('.shipping_amt .total-value').html('$'+new_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	$j('.standard_shipping_amt').html(standard_shipping.toFixed(2));

	new_shipping = parseFloat(new_shipping);
	if (tax_rate == "" || tax_rate == "NaN") tax_rate = 0;

	new_sales_tax = (sub_total - coupon_gift  + min_charge + new_shipping + liftgate_amt) * (tax_rate * .01);
	
	new_sales_tax = Math.round(new_sales_tax*100)/100;
	
	var new_total = sub_total - coupon_gift - rewards_redeemed + min_charge + new_sales_tax + new_shipping + liftgate_amt;
	
	
	new_total = Math.round(new_total*100)/100;
	new_total = new_total.toFixed(2);
	new_sales_tax = new_sales_tax.toFixed(2);
	$j('#sales_tax').html('$'+new_sales_tax.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	//$('cur_total').update(new_total);
	$j('span.total-value.final.grand_total').html('$'+new_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	$j('#total_order_m').html(new_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

	$j.post("/ajax.calculate_shipping.php",{ method: method });

//	$j.ajax("/ajax.calculate_shipping.php", {
//		data: {
//			action  	: 'cart3',
//			method 		: method
//		},
//		dataType: "json",
//		type: "POST",
//		beforeSend: function(){
//			// this is where we append a loading image
//			//$j('#shipping_cost').prepend('<div class="loader"></div>');
//		},
//		success: function(returnObject) {
//
//			$j('.loader').remove();
//			var result = eval(returnObject);
//
//			switch (result)
//			{
//				case "-1"   :response = $j('.shipping-costs-breakdown').prepend("<div class='error_msg'>missing required data.</div>");
//								break;
//
//				default     :response = '';
//								/* show the data */
//
//								break;
//			}
//
//		},
//		error: function(returnObject) {
//			console.log(returnObject);
//			$j('.shipping-costs-breakdown').prepend("<div class='error_msg'>something went wrong</div>");
//		}
//	});
}

function toggle_liftgate(radio_btn,tax_rate){
	var charge_liftgate = false;
	if(radio_btn.value == 'liftgate_Y') charge_liftgate = true;

	$j('.error_msg').remove();

	$j.ajax("/ajax.calculate_shipping.php", {
		data: {
			action  	: 'cart3liftgate',
			charge_liftgate : charge_liftgate
		},
		dataType: "json",
		type: "POST",
		beforeSend: function(){
			// this is where we append a loading image
			//$j('#shipping_cost').html('<div class="loader"></div>');
		  },
		success: function(returnObject) {

			var liftgate_amt = eval(returnObject);
			console.log(liftgate_amt);

			if(liftgate_amt > 0){
				$j('#liftgate_amt').html('$'+liftgate_amt).parent().removeClass('hidden');
			}else{
				$j('#liftgate_amt').html('$'+liftgate_amt).parent().addClass('hidden');
			}


			//var grand_total = parseFloat( $j('.grand_total').html().replace(/[$,]/g, ''));

			var sub_total = jQuery('div.total-row.sub_total span.total-value').html();
			var coupon_gift = jQuery('.coupon_gift').html();
			var rewards_redeemed = jQuery('.rewards_redeemed').html();
			var min_charge = jQuery('.min_charge').html();
			var sales_tax = jQuery('#sales_tax').html();
			var shipping_amt = jQuery('.shipping_amt .total-value').html();
			if (sub_total == null) sub_total = "0.00";
			if (coupon_gift == null) coupon_gift = "0.00";
			if (rewards_redeemed == null) rewards_redeemed = "0.00";
			if (min_charge == null) min_charge = "0.00";
			if (sales_tax == null) sales_tax = "0.00";
			if (shipping_amt == null) shipping_amt = "0.00";
			sales_tax = parseFloat(sales_tax.replace(/[$,]/g, ''));
			sub_total = parseFloat(sub_total.replace(/[$,]/g, ''));
			rewards_redeemed = parseFloat(rewards_redeemed.replace(/[$,-]/g, ''));
			coupon_gift = coupon_gift.replace(/[$,]/g, '');
			coupon_gift = coupon_gift.replace(' ', '');
			coupon_gift = parseFloat(coupon_gift.replace('-', ''));
			min_charge = parseFloat(min_charge.replace(/[$,]/g, ''));
			shipping_amt = parseFloat(shipping_amt.replace(/[$,]/g, ''));

        //alert (sub_total + " " + coupon_gift + " " + min_charge + " " + sales_tax);

			new_sales_tax = (sub_total - coupon_gift - rewards_redeemed + min_charge + shipping_amt + liftgate_amt) * (tax_rate * .01);
			new_sales_tax = Math.round(new_sales_tax*100)/100;
			var new_total = sub_total - coupon_gift - rewards_redeemed + min_charge + new_sales_tax + shipping_amt + liftgate_amt;
			new_total = Math.round(new_total*100)/100;
			new_total = new_total.toFixed(2);
			new_sales_tax = new_sales_tax.toFixed(2);
			$j('#sales_tax').html('$'+new_sales_tax);
			$j('.grand_total').html('$'+new_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

		},
		error: function(returnObject) {
			//console.log(returnObject);
			$j('#shipping_cost').html('something went wrong');
		}
	});

}
/*
*****************************************************************
** checkout.php functions
*****************************************************************
*/

function toggle_accordion(stepToOpen){

	$j( document ).ready(function() {

		var step_order = {
				//'sign-in':1,
				'shipping-details':2,
				'shipping-method':3,
				'account-details':4,
				'confirm-order':5
				};

		if($j('.step-holder').not('.completed-step').is(':visible')) {
			$j('.step-holder').not('.completed-step').hide();
	    }

		//remove class completed-checkout-step from any steps that are after the current step
		//and close the coresponding summary box
		$j('.completed-checkout-step').each(function( index ) {

			currentStep = ($( this ).id).split('-step');
			currentStep = currentStep[0];

			if( step_order[currentStep] > (step_order[stepToOpen])){
				$j( this ).removeClass('completed-checkout-step');
				$j('#' + currentStep + '-completed').hide();
			}
		});

		if($j('#' + stepToOpen + '-step')){
			$j('#' + stepToOpen).show();
			$(stepToOpen + '-step').scrollIntoView();
			//$j('html,body').animate({scrollTop: $j('#' + stepToOpen + '-step').offset().top +500}, 2000);

			jQuery('.account-row').sameHeight({
				elements: '.label-box',
				flexible: true
			});
		}

	});
	return false;
}

function edit_checkout_step(step){
	if($j('#version-2').length>0){
		$j('html,body').animate({scrollTop: $j('#' + step + '-step').offset().top}, 2000);
	}else{
		//$j('#' + step + '-step').removeClass('completed-checkout-step');
		//$j('#' + step + '-completed').hide();
		//toggle_accordion(step);
		$j('html,body').animate({scrollTop: $j('#' + step + '-step').offset().top +500}, 2000);
	}

	return false;
}

// sign-in functions

function checkout_step1_continue(type){
	$j( document ).ready(function() {
		$j('#sign-in-step').addClass('completed-checkout-step');
		if(type){
			$j('#sign-in-completed-' + type).show();
		}else{
			$j('#sign-in-completed').show();
		}

		toggle_accordion('shipping-details');
	});
	return false;
}

function password_recovery(){

	$j.ajax("/popuplogin.php", {
		data: {
			action  : 'recover_password',
			email 	: $j('#recovery_email').val()
		},
		dataType: "json",
		type: "POST",
		success: function(returnObject) {
			var result = eval(returnObject);
			if(result.error){
				$j('#message').html(result.error).addClass('errorBox');
			}else if(result.message && !result.success){
				$j('#message').html(result.message);
			}else{
				$j('.login_box').html(result.message + "<button class='button-brown' onclick='TINY.box.fill(\"popuplogin.php?type=login\",1,0,1);return false;'>Sign In</button>");
			}
		},
		error: function (xhr, status, thrownError) {
			//console.log(xhr.statusText);
			//console.log(xhr.responseText);
			//console.log(xhr.status);
			//console.log(thrownError);
        	alert('Something went wrong.');
		}
	});

	return false;
}

// account-details & shipping-details functions

function copy_billing_info_to_shipping()
{
    var check_box_value     = null;

    /* make sure we are dealing with a valid element */
    if ($j('#shipping-same-as-billing').length >0)
    {
        check_box_value = $j('#shipping-same-as-billing').is(':checked');
    }

    /* if the checkbox was checked then we want to copy over all the info */
    if (check_box_value == true)
    {
    	$j('#billing_fname').val($j('#shipping_fname').val());
    	$j('#billing_lname').val($j('#shipping_lname').val());
    	$j('#billing_company').val($j('#shipping_company').val());
    	$j('#billing_address').val($j('#shipping_address').val());
    	$j('#billing_address2').val($j('#shipping_address2').val());
    	$j('#billing_city').val($j('#shipping_city').val());
    	$j('#billing_state').val($j('#shipping_state').val());
    	$j('#billing_zip').val($j('#shipping_zip').val());
    	$j('#billing_home_phone').val($j('#shipping_home_phone').val());
    }
    else
    {
    	$j('#billing_fname').val("");
    	$j('#billing_lname').val("");
    	$j('#billing_company').val("");
    	$j('#billing_address').val("");
    	$j('#billing_address2').val("");
    	$j('#billing_city').val("");
    	$j('#billing_state').val("");
    	$j('#billing_zip').val("");
    	$j('#billing_home_phone').val("");
    }

    return true;
}
function change_billing_address_info(){
	if($j('#shipping-same-as-billing').checked){
		$j('.billing-address-form').addClass('hidden');
		copy_shipping_to_billing();
		$j('.billing-address-info').removeClass('hidden');
	}else{
		$j('.billing-address-info').addClass('hidden');
		$j('#billing_fname, #billing_address, #billing_address2, #billing_city, #billing_state, #billing_zip, #billing_phone').val('');
		$j('.billing-address-info .billing-line').html('');
		$j('.billing-address-form').removeClass('hidden');
	}
}
function validateShippingBilling(type){
	var succeeded = false;
	var billing_only = false;
	var shipping_only = false;

	var stop_for_no_match_billing = '';
	var stop_for_no_match_shipping = '';
	var location_type = "";

	if($("stop_for_no_match_billing")){
		stop_for_no_match_billing = $("stop_for_no_match_billing").value
	}
	if($("stop_for_no_match_shipping")){
		stop_for_no_match_shipping = $("stop_for_no_match_shipping").value
	}

	if($("shipping-same-as-billing").checked){
		copy_billing_info_to_shipping();
	}

	if(type ==='billing' && !$("shipping-same-as-billing").checked){
		billing_only = true;
	}

	if($('shipping_locationR').checked){
		location_type = "residential";
	} else if($("shipping_locationC").checked) {
		location_type = "commercial";
	}
	else{
		location_type = "residential";
	}

	var data = {
				action       		: 'shipping_billing',
				type				: type,
				shipping_only		: shipping_only,
				billing_only 		: billing_only,
				billing_fname		: $("billing_fname").value,
				billing_lname    	: $("billing_lname").value,
				billing_company     : $("billing_company").value,
				billing_address     : $("billing_address").value,
				billing_address2    : $("billing_address2").value,
				billing_city        : $("billing_city").value,
				billing_state       : $("billing_state").value,
				billing_zip         : $("billing_zip").value,
				billing_home_phone  : $("billing_home_phone").value,
				shipping_email      : $("shipping_email").value,
				email_offer			: $("email_offer").value,

				same_as_billing     : ($("shipping-same-as-billing").checked ? "Y" : "N"),

				shipping_fname		: $("shipping_fname").value,
				shipping_lname    	: $("shipping_lname").value,
				shipping_company    : $("shipping_company").value,
				shipping_address    : $("shipping_address").value,
				shipping_address2   : $("shipping_address2").value,
				shipping_city       : $("shipping_city").value,
				shipping_state      : $("shipping_state").value,
				shipping_zip        : $("shipping_zip").value,
				shipping_home_phone : $("shipping_home_phone").value,
				shipping_location	: location_type,

				stop_for_no_match_billing  : stop_for_no_match_billing,
				stop_for_no_match_shipping : stop_for_no_match_shipping
		        };

	$j.ajax("/ajax/ajax.checkout_functions.php", {
        data: data,
        dataType: "json",
        type: "POST",
        beforeSend:function(){
            // this is where we append a loading image

        	if(type == 'billing'){
        		$j('#account-details').append('<div class="step-holder-loader"></div>');
        	}else{
        		$j('#shipping-details').append('<div class="step-holder-loader"></div>');
        	}
          },
        success: function(returnObject) {

        	$j('.step-holder-loader').remove();

        	var result = eval(returnObject);
			//console.log(result);

			//restore default
			$j('#' + type + '_errors').hide();
			$j('.checkout_error_field').removeClass('checkout_error_field');
			$j('.error_msg').remove();

			if(result.errors){

				for(var i=0, len=result.errors.length; i<len; i++){
					var id = result.errors[i].field;
					
					$j($(id)).addClass('checkout_error_field').parent().append(result.errors[i].msg);
				}
				//$j('html,body').animate({scrollTop: $j('.error_msg').parent().offset().top - 50}, 0 );
				
				jQuery('.account-row').sameHeight({
					elements: '.label-box',
					flexible: true
				});

			}

			if(result.result == 'passed_validation'){
				succeeded = true;

				if(type == 'shipping'){
					$j('#shipping-details-step').addClass('completed-checkout-step');
					$j('#shipping-details-completed div').html(result.shipping_addr);
					window.dataLayer = window.dataLayer || [];
					dataLayer.push({'event': 'checkout','ecommerce': {'checkout': {'actionField': {'step': 1}}}});
				}

				if(type == 'billing'){
					$j('.billing-holder').hide();

					toggle_accordion('billing-method');
					$j('#account-details-completed div').html(result.billing_addr);
					$j('#account-details-completed').show();
					$j('#account-details-step').addClass('completed-checkout-step');
//					show_shipping_methods();
				}else{
					toggle_accordion('shipping-method');
					$j('#shipping-details-completed').show();
					show_shipping_methods();
				}
			}
        },
        error: function (xhr, status, thrownError) {
//			console.log(xhr.statusText);
//			console.log(xhr.responseText);
//			console.log(xhr.status);
//			console.log(thrownError);
        	$j('.step-holder-loader').remove();
        	alert('Something went wrong.');
		}
    });

	$('stop_for_no_match_' + type).value = 'yes';

    return succeeded;
}

function valid_shipping(type){

	$('stop_for_no_match_' + type).value = 'no';

	$j('#' +type+'_city').removeClass('checkout_error_field').parent().find('.error_msg').remove();
	$j('#' +type+'_state').removeClass('checkout_error_field').parent().find('.error_msg').remove();
	$j('#' +type+'_zip').removeClass('checkout_error_field').parent().find('.error_msg').remove();

	//validateShippingBilling(type);

	return false;
}

function change_city_state_to_match_zip (address_type, suggested_city, suggested_state)
{
	var city_var, state_var;

	if (address_type == "shipping")
	{
		var city_var = $j('#shipping_form').find('#shipping_city')[0];
		var state_var = $j('#shipping_form').find('#shipping_state')[0];
		var zip_var = $j('#shipping_form').find('#shipping_zip')[0];
	}
	else if (address_type == "billing")
	{
		var city_var = $j('#billing_form').find('#billing_city')[0];
		var state_var = $j('#billing_form').find('#billing_state')[0];
		var zip_var = $j('#billing_form').find('#billing_zip')[0];
	}

	if(suggested_city){
		city_var.value = suggested_city;
		$j(city_var).removeClass('invalid').parent().find('.error_msg').remove();
	}
	if(suggested_state){
		state_var.value = suggested_state;
		$j(state_var).removeClass('invalid').parent().find('.error_msg').remove();
	}
	$j(zip_var).removeClass('invalid').parent().find('.error_msg').remove();

	return false;
}

function show_shipping_methods(){

	$j.ajax("/ajax/ajax.checkout_functions.php", {
		data: {
			action  : 'shipping_method_options',
			shipping_zip : $j('#shipping_zip').val()
		},
		dataType: "json",
		type: "POST",
		beforeSend:function(){
            // this is where we append a loading image
        		$j('#shipping-method').append('<div class="step-holder-loader"></div>');
          },
        success: function(returnObject) {
        	//console.log(returnObject);
        	$j('.step-holder-loader').remove();
        	if(returnObject.errors){
        		alert('errors');
        	}else{
        		$j('#shipping-method-options').html(returnObject);
    			$j('.shipping-holder').show().removeClass('hidden');
        	}
		}
	});
}

// shipping-method functions

function save_shipping_methods(){

	$j('.checkout_error_field').removeClass('checkout_error_field');
	$j('.error_msg').remove();

	if($j('[name = liftgate]').length >0 && $j('[name = liftgate]:checked').length == 0){
		$j('[name = liftgate]').parent().addClass('checkout_error_field');
		$j('.liftgate-holder').append('<div class="error_msg">Please select whether a liftgate should be provided for delivery.</div>');
		$j('html,body').animate({scrollTop: $j('.liftgate-holder').offset().top - 50}, 0 );

		return false;
	}
	var charge_liftgate;
	if($j('[name = liftgate]:checked').val() == 'liftgate_Y') {
		charge_liftgate = true;
	}else{
		charge_liftgate = false;
	}
	$j.ajax("/ajax/ajax.checkout_functions.php", {
		data: {
			action  			: 'save_shipping_methods',
			special_comments	: $j('#special_comments').val(),
			shipping_method		: $j('[name = shipping_method]:checked').val(),
			charge_liftgate		: charge_liftgate
		},
		dataType: "json",
		type: "POST",
		beforeSend:function(){
            // this is where we append a loading image
			$j('#shipping-method').append('<div class="step-holder-loader"></div>');
		},
		success: function(returnObject) {
			result = eval(returnObject);
			$j('.step-holder-loader').remove();

			$j('#shipping-method-step').addClass('completed-checkout-step');
			$j('#shipping-method-completed div').html(result.shipping_method_str);
			$j('.cart-cost-summary').html(result.cart_cost_summary);
			$j('.total-cost.mobile').html(result.cart_cost_summary);


			toggle_accordion('account-details');
			$j('#shipping-method-completed').show();
			window.dataLayer = window.dataLayer || [];
			dataLayer.push({'event': 'checkout', 'ecommerce': {'checkout': {'actionField': {'step': 2}}}});
		},

		error: function() {
			$j('.step-holder-loader').remove();
			alert('something went wrong');
		}
	});
}

// payment-method functions
function validate_cc_number(e)
{
    var keynum = (e.which) ? e.which : e.keyCode;

    // only allow numbers and backspaces and arrow keys
    // and tab key
    if (keynum == 9 || keynum == 8 || (keynum == 37 || keynum == 39) || (keynum <= 57 && keynum >= 48))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function hide_cc_number(input)
{
	var x_ed_out = '';
	var cc = input.value;
	if(cc){
	    //preg_replace('/(?:[\d]+)([\d]{4})$/', str_repeat("XXXX-", 3) . '${1}', format_cc_number());
	    var match = cc.replace(/[^\d]+/g, '').match(/(?:[\d]+)([\d]{4})$/);
	    if(match && cc.length >0){
			$j('#cc_number').data('the-number',match[0]);
	    	var x_ed_out = new Array( match[0].length - 3 ).join( 'X' ) + '-' + match[1];
	    	$j(input).value = x_ed_out;

			$j('#cc_number').removeClass('checkout_error_field').parent().find('.inline_error_msg').addClass('hidden');
			$j('#cc_number').parent().find('.label_msg').removeClass('hidden');

	    }else{
			$j('#cc_number').parent().find('.label_msg').addClass('hidden');
			$j('#cc_number').addClass('checkout_error_field').parent().find('.inline_error_msg').removeClass('hidden');
		}


	    //console.log(x_ed_out);
	    //console.log($j('#cc_number').data('the-number'));

	}
	return false;
}

function validateAccountAndPayment(){

	if($j('.payment-type:checked').val() == 'paypal'){
		checkout_paypal_submit('checkout');
		return false;
	}

	var account_step_password;
	if($j('#account_step_password')){
		account_step_password = $j('#account_step_password').val();
		account_step_password_conf = $j('#account_step_password_conf').val();
	}

	var data = {
			action  			: 'validate_account_and_payment',

			type				: 'billing',
			billing_fname		: $("billing_fname").value,
			billing_lname    	: $("billing_lname").value,
			billing_company     : $("billing_company").value,
			billing_address     : $("billing_address").value,
			billing_address2    : $("billing_address2").value,
			billing_city        : $("billing_city").value,
			billing_state       : $("billing_state").value,
			billing_zip         : $("billing_zip").value,
			billing_home_phone  : $("billing_home_phone").value,
			stop_for_no_match_billing  : $("stop_for_no_match_billing").value,

			cc_name				: $('cc_name').value,
			cc_number			: $j('#cc_number').data('the-number'),
			cc_expiration_month	: $('cc_expiration_month').value,
			cc_expiration_year	: $('cc_expiration_year').value,
			cc_ccv				: $('cc_ccv').value
		}

	$j.ajax("/ajax/ajax.checkout_functions.php", {
		data: data,
		dataType: "json",
		type: "POST",
		beforeSend:function(){
            // this is where we append a loading image
			$j('#account-details').append('<div class="step-holder-loader"></div>');
		},
		success: function(returnObject) {
				$j('.step-holder-loader').remove();

//				console.log(returnObject);
				var result = eval(returnObject);


				//restore default
				$j('.checkout_error_field').removeClass('checkout_error_field');
				$j('.error_msg').remove();

				//clear cc and ccv fields
				$('cc_ccv').value = '';
				//$('cc_number').value = '';

				if(result.errors){

					for(var i=0, len=result.errors.length; i<len; i++){
						var id = result.errors[i].field;
						$j($(id)).addClass('checkout_error_field').parent().append(result.errors[i].msg);
					}
				//	$j('html,body').animate({scrollTop: $j('.error_msg').parent().offset().top - 50}, 0 );

				}else if(result.result == 'passed_validation'){
					succeeded = true;
					window.dataLayer = window.dataLayer || [];
					dataLayer.push({'event': 'checkout','ecommerce': {'checkout': {'actionField': {'step': 3}}}});

						$j('.billing-holder').hide();

						$j('#account-details-completed-address').html(result.billing_addr);
						$j('#account-details-completed-payment').html(result.summary);
						$j('#account-details-completed').show();
						$j('#account-details-step').addClass('completed-checkout-step');

						//clear errors on confirm step
						$j('#confirm_order_errors').html('').addClass('hidden');
						//display signin section if they aren't logged in
						$j('.sign-up-holder').html(result.sign_in);

						toggle_accordion('confirm-order');
						$j('html,body').animate({scrollTop: $j('#place_order_button').offset().top - $j(window).height() + 50}, 0 );
						//show_confirm_order();

				}
			},
		error: function() {
			$j('.step-holder-loader').remove();
			alert('something went wrong');
		}
	});

	$("stop_for_no_match_billing").value = 'yes';

	return false;
}

function show_confirm_order(){

	//restore default
	$j('#confirm_order_errors').addClass('hidden');
	$j('.products-table').html('');
	$j('.sign-up-holder').html('');
	$j('.total-cost').html('');
	$j('#confirm-order-step-bottom').hide();

	$j.ajax("/ajax/ajax.checkout_functions.php", {
		data: {
			action  : 'create_confirm_order'
		},
		dataType: "json",
		type: "POST",
		beforeSend:function(){
            // this is where we append a loading image
			$j('#confirm-order').append('<div class="step-holder-loader"></div>');
		},
		success: function(returnObject) {

//			console.log(returnObject);

			$j('.products-table').html(returnObject.cart_items);
			$j('.total-cost').html(returnObject.cost_table);

			$j('.step-holder-loader').remove();
			$j('#confirm-order-step-bottom').show();

//			toggle_accordion('confirm-order');
		},
		error: function (xhr, status, thrownError) {
//			console.log(xhr.statusText);
//			console.log(xhr.responseText);
//			console.log(xhr.status);
//			console.log(thrownError);
			$j('.step-holder-loader').remove();
			alert('something went wrong');
		}
	});
}

function place_order_submit(){

	var paypal_checkout = 'N';
	var paypal_token = '';
	var payer_id ='';

	var account_step_password ='';
	var account_step_password_conf ='';
	if($j('#account_step_password')){
		account_step_password = $j('#account_step_password').val();
		account_step_password_conf = $j('#account_step_password_conf').val();
	}

	if($j(".paypal-express-checkout").length > 0){
		paypal_checkout = 'Y';
		paypal_token = $j("#paypal_token").val();
		payer_id = $j("#payer_id").val();
	}

    $j.ajax("/ajax/ajax.checkout_functions.php", {
    	data: {
			action  		: 'place_order',
			paypal_checkout : paypal_checkout,
			special_comments: $j("#special_comments").val(),
			token 			: paypal_token,
			PayerID			: payer_id,

			cc_number		: $j('#cc_number').data('the-number'),

			email_offer		: $("email_offer").value,
			shipping_email	: $j("#shipping_email").val(),

			account_step_password : account_step_password,
			account_step_password_conf : account_step_password_conf
		},
		dataType: "json",
		type: "POST",
		beforeSend:function(){
            // this is where we append a loading image
        		$j('#confirm-order').append('<div class="step-holder-loader"></div>');
          },
          success: function(returnObject) {

          	//restore default
  			$j('.checkout_error_field').removeClass('checkout_error_field');
  			$j('.error_msg').remove();

  			//console.log(returnObject);
  			var result = eval(returnObject);

  			if(result.errors.length > 0 || result.top_errors.length > 0 ){
  				$j('.step-holder-loader').remove();

  				if(result.top_errors.length>0){
  					$j('#confirm_order_errors').html(result.top_errors.join('<br>'));
  					$j('#confirm_order_errors').removeClass('hidden');
  				}

  				for(var i=0, len=result.errors.length; i<len; i++){
					var id = result.errors[i].field;
					if(id == 'account_step_password'){
						$j($(id)).addClass('checkout_error_field');
						$j($('account_step_password_conf')).parent().append(result.errors[i].msg);
					}else{
						$j($(id)).addClass('checkout_error_field').parent().append(result.errors[i].msg);
					}
				}

  				if(result.sign_in_checkout){
  					$j('.sign-up-holder').html('<div class="successful-sign-in">You successfully signed in!</div>');
  					$j('.user-nav').html(result.sign_in_checkout.regular);
  					$j('.btn-user').parent().replaceWith(result.sign_in_checkout.mobile);
  				}

  				$j('html,body').animate({scrollTop: $j('#confirm_order_errors ,.error_msg').offset().top - 50}, 0 );

  			}

  			if(result.redirect){
  				window.dataLayer = window.dataLayer || [];
  		        dataLayer.push({'event': 'checkout','ecommerce': {'checkout': {'actionField': {'step': 4}}}});
  				window.location = result.redirect;
  			}
  		},
		error: function (xhr, status, thrownError) {
//			console.log(xhr.statusText);
//			//console.log(xhr.responseText);
//			console.log(xhr.status);
//			console.log(thrownError);
			$j.ajax("/ajax/ajax.checkout_functions.php", {
				data: {
					action : 'debug_duplicate_orders',
					xhr : JSON.stringify(xhr),
					status : JSON.stringify(status),
					thrownError : JSON.stringify(thrownError)
				},
				dataType: "json",
				type: "POST",
		        success: function(returnObject) {
		        	alert('something went wrong');
					location.reload();

					$j('.step-holder-loader').remove();
		        },
				error: function() {
		        	alert('something went wrong');
					location.reload();

					$j('.step-holder-loader').remove();
		        }
			});
		}
	});
}
function place_order_submit_open_steps(){

	var info = $j('#checkout_form').serialize();
	var paypal_checkout = 'N';
	var paypal_token = '';
	var payer_id ='';

	//restore default
	$j('.checkout_error').removeClass('checkout_error');
	$j('.error_msg').remove();
	$j('.inline_error_msg').addClass('hidden');
	$j('.step-holder-loader').remove();
	$j('#confirm_order_errors').addClass('hidden');
	$j('#confirm-order-step-bottom').removeClass('error');
    // prevent checkout if shipping wasn't calculated yet
	
    if( !$j.isNumeric($j('.shipping_amt .total-value').html().replace(/[^0-9]/g,'')) && valid_zip_code($j('#shipping_zip').val())){

        calculate_shipping($j('#shipping_zip').val(),'cart','');
        var msg = "PLEASE NOTE: Shipping charges were just updated above.  Please <a href='#shipping-holder'>review them</a>, then press Confirm Order again";
        $j('#confirm_order_errors').html(msg);
        $j('#confirm_order_errors').removeClass('hidden');

       /* $j.ajax("../error_handling.php", {
            data: {
                action 	: 'StickyErrors',
                top_error_text   : JSON.stringify(msg)
            },
            dataType: "json",
        });*/
        return false;
    }

	if($j('input[name=shipping_method]:checked').length > 0){
		info += '&shipping_method=' + $j('input[name=shipping_method]:checked').val();
	}else if($j('.shipping-button.active').length > 0){
		// buttons
		info += '&shipping_method=' + $j('.shipping-button.active').data('shipping-method');
	}else if($j('#shipping_method').length > 0){
		// used when expedited is disabled or for freight
		info += '&shipping_method=' + $j('#shipping_method').data('shipping-method');
	}else{
		info += '&shipping_method=';
	}

	if($j('#shipping_method_2').length > 0){
		// used when there are two shipping methods
		info += '&shipping_method_2=' + $j('#shipping_method_2').data('shipping-method');
	}

	if($j("input#paypal-payment").is(':checked') && $j("input#paypal-payment").is(':visible')){
			checkout_paypal_submit('checkout');
			return;
	}

	if($j(".paypal-express-checkout").length == 0 && $j('[name = liftgate]').length >0 && $j('[name = liftgate]:checked').length == 0){
		$j('[name = liftgate]').parent().addClass('checkout_error_field');
		var the_err_msg = "Please select whether a liftgate should be provided for delivery.";
		$j('.liftgate-holder').append('<div class="error_msg">'+the_err_msg+'</div>');
		$j('html,body').animate({scrollTop: $j('#confirm_order_errors ,.checkout_error_field').not('.hidden').offset().top - 50}, 0 );
		sendToDataLayer('Checkout Error', 'liftgate', the_err_msg);
		return false;
	}

	if($j('[name = liftgate]:checked').val() == 'liftgate_Y') {
		info += '&charge_liftgate=' + 'true';
	}else{
		info += '&charge_liftgate=' + 'false';
	}
	if($j(".paypal-express-checkout").length > 0){
		paypal_checkout = 'Y';
		paypal_token = $j("#paypal_token").val();
		payer_id = $j("#payer_id").val();
	}

    $j.ajax("/ajax/ajax.checkout_functions.php",{
		data: {
			action  		: 'place_order_open',
			paypal_checkout : paypal_checkout,
			token 			: paypal_token,
			PayerID			: payer_id,
			form_info		: info,
			cc_num			: $j('#cc_number').data('the-number')
		},
		dataType: "json",
		type: "POST",
		timeout: 120000,
		beforeSend:function(){
            // this is where we append a loading image
			$j('.btn-holder').append('<div class="step-holder-loader"></div>');
        	$j('#place_order_button').attr('disabled', true);
          },
        success: function(returnObject) {
			
			var result = eval(returnObject);
			console.log(result.errors);
			if(result.errors.length > 0 || result.top_errors.length > 0 ){

				$j('#place_order_button').attr('disabled', false);
				$j('.step-holder-loader').remove();

				//$j('#confirm-order-step-bottom').addClass('error');
				//$j('.hidden-info-holder.shipping').removeClass('hidden');
				//if(!$j('shipping-same-as-billing').checked){$j('.hidden-info-holder.billing').removeClass('hidden');}

				if(result.top_errors.length>0){
					$j('#confirm_order_errors').html(result.top_errors.join('<br>'));
					$j('#confirm_order_errors').removeClass('hidden');
					$j('html,body').animate({scrollTop: $j('#confirm_order_errors').offset().top - $j(window).height() + 50}, 0 );
               
				}

				for(var i=0, len=result.errors.length; i<len; i++){
					var id = result.errors[i].field;
					if(id == 'account_step_password'){
						$j('#'+id).addClass('checkout_error_field');
						$j($j('account_step_password_conf')).parent().append(result.errors[i].msg);
						sendToDataLayer('Checkout Error', id, result.errors[i].msg);
						$j('html,body').animate({scrollTop: $j('.error_msg').parent().offset().top+50}, 2000);


					}else{
						//$j($j(id)).parent().find('.label_msg').addClass('hidden');
						var error_text = $j('#'+id).addClass('checkout_error_field').parent().find('.inline_error_msg').removeClass('hidden').text();
						$j('#'+id).addClass('checkout_error').parents('.field-holder').append("<span class='error_msg'>"+result.errors[i].msg+"</span>");
						sendToDataLayer('Checkout Error', id, error_text);
						//$j('html,body').animate({scrollTop: $j('.error_msg').parent().offset().top}, 2000);

					}
				}

				if(result.sign_in_checkout){
					$j('.sign-up-holder').html('<div class="successful-sign-in">You successfully signed in!</div>');
					$j('.user-nav').html(result.sign_in_checkout.regular);
					$j('.btn-user').parent().replaceWith(result.sign_in_checkout.mobile);
				}

				$j('html,body').animate({scrollTop: $j('#confirm_order_errors ,.checkout_error_field').not('.hidden').offset().top - 200}, 0 );

				return false;
			}

			if(result.redirect){
				//window.dataLayer = window.dataLayer || [];
				//dataLayer.push({'event': 'checkout', 'ecommerce': {'checkout': {'actionField': {'step': 4}}}});
				window.location = result.redirect;
			}
		},
		error: function (xhr, status, thrownError) {
			//console.log(xhr.statusText);
			//console.log(xhr.responseText);
			//console.log(xhr.status);
			//console.log(thrownError);
			$j('body').removeClass('loading');
			$j('#place_order_button').attr('disabled', false);
			$j('.step-holder-loader').remove();
			$j.ajax("../error_handling.php", {
				data: {
					action 	: 'checkout_error',
					statusText 	: JSON.stringify(xhr.statusText),
					xhrResponsText   : JSON.stringify(xhr.responseText),
					status 	: 'status: ' + xhr.status,
					thrownError : JSON.stringify(thrownError)
				},
				dataType: "json",
				type: "POST",
				success: function(returnObject) {
					console.log(returnObject);
					if(returnObject.order_id > 0){
						//redirect to invoice
						window.location = '../invoice.php';
					}else{
						var html = "<div id='msg-part'>Something weird just happened on our end, sorry about that. Please refresh your screen and try that again.</div>";
						html += "<div><button class='button_holder' onClick='TINY.box.hide();'>OK</button></div>";

						TINY.box.show({html:html,animate:false,close:false,boxid:'overlay-error',top:3})
					}
				},
				error: function(event, jqXHR, request, settings) {
					var html = "<div id='msg-part'>Something weird just happened on our end, sorry about that. Please refresh your screen and try that again.</div>";
					html += "<div><button class='button_holder' onClick='TINY.box.hide();'>OK</button></div>";

					TINY.box.show({html:html,animate:false,close:false,boxid:'overlay-error',top:3})
				}
			});
		}
	});


}

function fillInAddress(type) {
  // Get the place details from the autocomplete object.

	if(type=='shipping'){
		var place = autocomplete.shipping.getPlace();
	}else{
		var place = autocomplete.billing.getPlace();
	}

  var place_info = {};

  $j('#'+type+'-address-info :input').each(function(index){
	  $j( this ).val('').removeAttr('disabled');
	  });

  /*
  for (var component in form) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }
  */

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    place_info[addressType] = place.address_components[i].short_name;
  }
  //console.log(place_info);

  //populate the form fields
  var address1 = '';
  if(place_info['street_number']){ address1 += place_info['street_number'] + ' ';}
  if(place_info['route']){ address1 += place_info['route'];}

  $j('#'+type+'_address').val(address1).blur();
  $j('#'+type+'_city').val(place_info['locality']).blur();
  $j('#'+type+'_state').val(place_info['administrative_area_level_1']).blur();
  $j('#'+type+'_zip').val(place_info['postal_code']).blur(); //trigger the change event so that it recalculates shipping

  $j('#shipping-address-info').find('.hidden-info-holder').removeClass('hidden');

  if($('shipping-same-as-billing').checked){
	  copy_shipping_to_billing();
  }

}

function copy_shipping_to_billing(){

	var shipping_fname = $j('#shipping_fname').val();
	var shipping_address = $j('#shipping_address').val();
	var shipping_address2 = $j('#shipping_address2').val();
	var shipping_city = $j('#shipping_city').val();
	var shipping_state = $j('#shipping_state').val();
	var shipping_state_name = $j('#shipping_state option:selected').html();
	var shipping_zip = $j('#shipping_zip').val();
	var shipping_phone = $j('#shipping_phone').val();
	var city_state_zip = '' ;

	$j('#billing_fname').val(shipping_fname);
	$j('#billing_address').val(shipping_address);
	$j('#billing_address2').val(shipping_address2);
	$j('#billing_city').val(shipping_city);
	$j('#billing_state option').prop("selected", false);
	$j('#billing_state option[value='+shipping_state+']').prop("selected", 'selected');
	$j('#billing_zip').val(shipping_zip);
	$j('#billing_phone').val(shipping_phone);


	if(shipping_city){ city_state_zip += shipping_city + ', ';}
	if(shipping_state){ city_state_zip += shipping_state_name + ', ';}
	if(shipping_zip){ city_state_zip += shipping_zip ;}

	$j('#same-billing-name').html(shipping_fname);
	$j('#same-billing-address').html(shipping_address);
	$j('#same-billing-address2').html(shipping_address2);
	$j('#same-billing-city-state-zip').html(city_state_zip);
	$j('#same-billing-phone').html(shipping_phone);

	if(shipping_address || shipping_address2 || city_state_zip || shipping_phone){
		$j('.billing-address-info').removeClass('hidden');
	}else{
		$j('.billing-address-info').addClass('hidden');
	}

}

function checkout_email_action(email_field){
	var email = email_field.val();
	var parent = email_field.parent();

	if(validateEmail(email)){
		email_field.removeClass('checkout_error_field').addClass('checkout_valid_field');
		$j.ajax("ajax/ajax.checkout_functions.php", {
	        data: {
	        	action: 'email_action',
	        	shipping_email: email
	        },
	        dataType: "json",
	        type: "POST",
	        success: function(returnObject) {
	        	switch (returnObject.email_status)
	            {
	                case 'customer':
	                	$j('#signin_box').removeClass('hidden');
	                	$j('.have-account-text').removeClass('hidden');
						parent.find('.label_msg').removeClass('hidden');
						parent.find('.inline_error_msg').addClass('hidden');
						$j('.subscribe').addClass('hidden');
						$j('#email_offer').prop('checked', false);
						//$j('#email_offer').attr('checked', false);
	                	break;
					case 'logged_in':
						$j('#signin_box').addClass('hidden');
	                	$j('.have-account-text').addClass('hidden');
	                	$j('#account_step_password').val('');
						parent.find('.label_msg').removeClass('hidden');
						parent.find('.inline_error_msg').addClass('hidden');
						$j('.subscribe').addClass('hidden');
						$j('#email_offer').prop('checked', false);
						//$j('#email_offer').attr('checked', false);
	                	break;
	                case 'not_customer':
	                	$j('#signin_box').addClass('hidden');
	                	$j('.have-account-text').addClass('hidden');
	                	$j('#account_step_password').val('');
						parent.find('.label_msg').removeClass('hidden');
						parent.find('.inline_error_msg').addClass('hidden');
						$j('.subscribe').removeClass('hidden');
						//$j('#email_offer').attr('checked', false);
						if($j('#email_offer').data('customer-checked')){
							$j('#email_offer').prop('checked', true);
						}
	                	break;
	                case 'not_valid':
	                	email_field.removeClass('checkout_error_field').addClass('checkout_error_field');
	                	var error_text = parent.find('.inline_error_msg').removeClass('hidden').text();
						parent.find('.label_msg').addClass('hidden');
						$j('.subscribe').removeClass('hidden');
						if($j('#email_offer').data('customer-checked')){
							$j('#email_offer').prop('checked', true);
						}
						sendToDataLayer('Checkout Error', 'login', error_text);
						//$j('#email_offer').attr('checked', true);
						break;
	            }
	        },
	        error: function(){}
	    });
	}else{
		$j('#signin_box').addClass('hidden');
    	$j('#account_step_password').val('');
		add_inline_text(email_field,parent,false,false);
	}
}


/*
*****************************************************************
** product_info.php functions
*****************************************************************
*/

function addErrorMessageAndFocus(obj, extra_text){
  if(extra_text == undefined){
        extra_text = '';
  }
 // var label = obj.data('label');
  var option_id = obj.attr('id').replace(/[^\d]/g,'');
  if(error_labels.hasOwnProperty(option_id)){
    label = error_labels[option_id];
  }
  else{
      label = obj.data('label');
      var input_label = 'select a ';
      var input_type = obj.data('input-type');
      if(input_type != undefined && input_type == 'text'){
            input_label = 'enter text for ';
      }
      label = 'Please ' + input_label + label + extra_text;
  }
  if(input_type != undefined && input_type == 'text'){
        obj.siblings('[id^="option_customization"]').addClass('error_field');
  }
  else if(input_type == 'select'){
    obj.siblings('[id^="option_select"]').first().find('.dd-select').addClass('error_field');
  }
  var message_div = $j('<div/>').addClass("error_msg").text( label);
  obj.addClass('error_field').parent().prepend(message_div);

  obj.closest('.listing_available_colors_v1').addClass('error_message_container');
  slideTo(obj.closest('.listing_available_colors_v1'));
//  error_message_div.text( label).show();
//  obj.addClass('error_field');

  //  slideTo(error_message_div);
}

function isRequiredFilled(obj){
        var this_val = obj.val();
        var this_type = obj.attr('type');
        if(this_val == ''|| this_val == 'unselected' ||
            ((this_type == 'checkbox' || this_type == 'radio' )
                && !obj.is(':checked'))
          ){
        return false;
        }
     return true;
}
function add_to_cart(id, option, limit, sku, price, img_path,page_type, name)
{
    removeCartPopup();
	if($j('.spotlightActive').length) $j('.spotlightActive').fadeTo( 10 , 0, function(){$j(this).removeClass('spotlightActive');});		
    $j('.error_field').removeClass('error_field');
    $j('.error_msg').remove();

    var quantity = $j('#quantity').val();
    var intRegex = /^\d+$/;
    if(!intRegex.test(quantity) || quantity < 1) {
    	$j('#quantity').addClass('error_field').parent().append('<div class="error_msg">Please enter a quantity.</div>');
        return false;
    }
    var case_amt = 0;
    var quantity_case = 0;
    if ( $j("input[name='quantity_case']").length > 0 ) {
	var quantity_case = $j("input[name='quantity_case']").val();

	var case_amt = $j('#' + id + '_case_amount').val();
    }

    quantity = parseInt( quantity );
    quantity_case = parseInt( quantity_case );
    case_amt = parseInt( case_amt );
    quantity = quantity + (quantity_case * case_amt);

    var post_option = "";

    /*if( quantity > limit && limit > 0) {

    	$j('#quantity').addClass('error_field').parent().append('<div class="error_msg">Currently you may only order ' +limit+ ' of this product.</div>');

        //alert('Currently you may only order ' + limit + ' of this product.');
        return false;
    }*/

    if (option) {
        post_option = option;
    }

    var presets = {};
    var customizations = {};
	var selected_optional_options='';
    // make sure all required options are filled
    var are_options_selected = true;
    $j('.option_holder .option_input').each(function(){
		var $option_holder = $j(this).closest('.option_holder');
		var info = $j(this).data('info');
		if(!$j(this).val() ){
			if($option_holder.data('optional')=="0" || ($option_holder.data('required_if_visible')=="1" && !$option_holder.hasClass('not_visible')))
			{
			  are_options_selected = false;
              addErrorMessageAndFocus(info.option_id);
			  return false;
			}
			else return true;
		}
		else{
			if($option_holder.data('optional')=="0" || !$option_holder.hasClass('not_visible')){
				if(info.preset_value_id && info.preset_value_id !=''){
				 //collect preset data if there is
				presets[$j(this).val()] = info.preset_value_id;
				}
				if($option_holder.data('optional')=="1") //get optional values.
					selected_optional_options +=  $j(this).val()+',';
				if(info.customization=="Y"){ //get customizations
					if($option_holder.data('input_type')=='file') customizations[$j(this).val()]=[$option_holder.data('input_type'),$j('#customization_'+$j(this).val()).data('val')];
					else customizations[$j(this).val()]=[$option_holder.data('input_type'),$j('#customization_'+$j(this).val()).val()];
				}
			}
		  return are_options_selected;
		}
	});

    if(!are_options_selected){
		return false;
    }
    
	
   
    var accessories = jQuery.map( jQuery('.accessories input[type=checkbox]:checked'), function(e,i){
    	return e.value;
    });

   
    var cartStats = {};

    if(is_option_product && !current_product_option_id){
		//alert("ID :"+id+" option: "+current_product_option_id);
        var message = 'Product: ' + id + "\r\nSelected Optional Options: "  + selected_optional_options + "\r\nCustomizations: " + customizations + "\r\nPresets:" + presets;
        $j.post('ajax/ajax.product_info.php',{'message' : message, 'subject':'Add option product to cart w/o option id'});
    }
    var itemInfo = {product_id: id,
                    quantity: quantity,
                    price: price,
                    name: name,
                    sku: sku,
                    img_path: img_path,
                    option: current_product_option_id,
                    selected_optional_options:selected_optional_options,
                    customizations: customizations,
                    presets: presets,
                    page_type: 'product'};
    storeCartItem(itemInfo, cartStats);

    return false;
}

function toggleFileInput(option_id, disable){
    var file_input = $j('#file_upload_option_' + option_id);
    var button = file_input.closest('.fileinput-button');
    if(disable){
        file_input.prop('disabled');
        button.addClass('disabled');
    }
    else{
       file_input.removeProp('disabled');
       button.removeClass('disabled');
    }
}

function resetProgressBar(hide){
    $j('#progress .progress-bar').css('width', '0px');
    if(hide){
        setTimeout(function(){$j('#progress').hide()},500);
    }
    else{
        $j('#progress').show();
    }
}

var file_init = false;
function initFileUploads(){
    if(file_init){
        return;
    }
    file_init = true;
    'use strict';
    var url = 'ajax/ajax.product_info.php',
        uploadButton = $j('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $j(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
     var deleteButton = /*$j('<button/>')
        .addClass('btn btn-danger delete')
        .html('<i class="glyphicon glyphicon-trash"></i><span>Delete</span>')*/
 $j('<span/>').text('Delete').addClass('delete_link')
        .on('click', function(){
                var $this = $j(this);
             $j.ajax({
                    url: url,
                    type: 'POST',
                    data: {action:'delete_upload',
                    image:$this.data('url')},
                    success:function(data){
                        if(data.remove){
                            //remove data
                            var option_id =$this.data('option_id');
                            $j('#option_customization_' + option_id).val('');
                            $j('#option_select_' + option_id).val('');
                            displayAdditionalPrice();
                            toggleFileInput(option_id, false);
                            $this.parent().hide('slow', function(){$this.remove(); });
                        }
                        else{
                            $this.after($j('<span class="text-danger block"/>').text('Error Deleting File'));

                        }
                    }
              });
        });

    $j('.file_upload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxHeight: 100,
        previewCrop: false
    }).on('fileuploadadd', function (e, data) {
        //disable upload button once a file is added
        upload_button_id = $j(e.target).attr('id').replace(/file_upload_option_/, 'file_input_span_');
        var upload_button = $j('#' + upload_button_id);
        upload_button.addClass('disabled');
        //if the prograss bar hasn't been reset, do that now
        resetProgressBar(false);
        //if there was an error label, remove it
        $j(e.target).closest('.listing_available_colors_v1').removeClass('error_message_container')
        data.context = $j('<div/>').addClass('option_container').appendTo('#files');
        $j('#upload_container').show();
        $j.each(data.files, function (index, file) {
            var node = $j('<p/>');//.append($j('<span/>').text(file.name))
            if (!index) {
                node
                    .append('<br>')
 //                   not appending upload button because auto uploading files, but if we change that, add the button
 //                   .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
        $j('#progress .progress-bar').css('width','0px');
        $j('#upload_container').focus();
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $j(data.context.children()[index]);
            var label = $j('<span class="title"/>')
                    .text($j(e.target).data('label')
                    .replace(/Upload/,''));
        if (file.preview) {
            node
                .prepend(file.preview)
                .prepend(label);
        }
        if (file.error) {
            node.parent().remove();
            displayFileError($j(e.target), file.error);
        }
        if (index + 1 === data.files.length) {
            var upload_button = data.context.find('button');
            if(file.error){
                upload_button.remove();
                setTimeout(function(){data.context.hide('slow', function(){$j(this).remove()});}, 3000);
            }
            else{
                upload_button.text('Upload').prop('disabled', false).removeClass('disabled');
            }
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $j('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
console.log('the data' , data);
        $j.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $j('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $j(data.context.children()[index])
                    .wrap(link);
                $j(deleteButton).clone(true)
                    .attr('data-url', file.url)
                    .attr('data-option_id', file.option_id)
                    .appendTo(data.context);

                data.context.data('option-id', file.option_id);
                data.context.attr('data-option-id', file.option_id);
                $j('#option_customization_' + file.option_id).val(file.url.split('/').pop());
                $j('#option_select_' + file.option_id).val(file.option_id);
                //only allow one upload per input
                 displayAdditionalPrice();
                 toggleFileInput(file.option_id, true);
                setTimeout(function(){resetProgressBar(true);}, 3000);
            } else if (file.error) {
                $j(data.context.children()[index]).parent().remove();
                displayFileError($j(e.target), 'Error uploading file.');
            }
        });

    }).on('fileuploadfail', function (e, data) {
        $j.each(data.files, function (index) {
            $j(data.context.children()[index]).parent().remove();
            displayFileError($j(e.target), 'Error uploading file.');
        });
    }).prop('disabled', !$j.support.fileInput)
        .parent().addClass($j.support.fileInput ? undefined : 'disabled');

}

function displayFileError(e_target, errorMessage){
    var option_id = e_target.attr('id').replace(/file_upload_option_/, '');
    var error_span = $j('<span/>').attr('id','file_failure_' + option_id).text('Error uploading file.').addClass('text-danger');
    var upload_button = $j('#file_input_span_' + option_id);
    upload_button.after(error_span).prop('disabled', false);
    toggleFileInput(option_id, false);
    resetProgressBar(true);
        setTimeout(function(){$j('#file_failure_' + option_id).remove();},5000);
}
function open_review_tab(){

	if($j('#review-tab-slide').hasClass('js-slide-hidden') || !$j('#review-tab-slide').parent().is(':visible')){
		$j('.review-tab-link').click();
	}

	Effect.ScrollTo('review-tab-slide');

	return false;
}

function open_question_tab(){

	if($j('#question-tab-slide').hasClass('js-slide-hidden') || !$j('#question-tab-slide').parent().is(':visible')){
		$j('.question-tab-link').click();
	}

	Effect.ScrollTo('question-tab-slide');

	return false;
}

function change_image(thumb_image){

	//If a border is showing, remove it
	$j('.img_zoom').parent().removeClass("active");

	//add border to current image
	$j(thumb_image).addClass("active");

	//change images
	try{
        //change images
		if($j(thumb_image).hasClass('video')){
			$j('#zoom-div img').addClass("hidden");
			if($j('#zoom-div iframe').attr("src") != $j(thumb_image).data('zoom-image')){
				$j('#zoom-div iframe').attr("src",$j(thumb_image).data('zoom-image'));
			}
			$j('#zoom-icon').addClass('hidden');
			$j('#zoom-div iframe').removeClass("hidden");
		}else{
			$j('#zoom-div iframe').attr("src","");
			$j('#zoom-div iframe').addClass("hidden");
			$j('#zoom-div img').removeClass("hidden");

	        //main image
	        $j('#img_zoom').attr("src",$j(thumb_image).data('zoom-image')).data("zoom-image",$j(thumb_image).data('zoom-image'));
	        //mobile image
	        $j('#mobile-product-image').find('img').attr("src",$j(thumb_image).data('zoom-image'));

	        //reset the zoom
	        $j("#img_zoom").one("load", function() {
				addZoom($j(this));
			}).each(function() {
			  if(this.complete) $j(this).load();
			});
		}

        if($j('#spotlight-wrapper').hasClass('spotlightActive')){
        	//close the zoom
        	$j('#spotlight-wrapper').click();
        }
	}
    catch(err){
        //don't do anything on error
    }

	return false;
}


function stringToObj(str){
    var this_obj = {};
    var str_arr = str.split('&')
    for(var i = 0; i < str_arr.length; i++){
        this_val = str_arr[i].split('=')
        this_obj[this_val[0]] = this_val[1];
    }
    return this_obj;
}



/*
*****************************************************************
functionality for the email portion on the product info page
*****************************************************************
*/

function email_form_send_mail()
{
    var url             = '/ajax/ajax.email.php';

    $j('.email_error_field').removeClass('error_field');
    $j('.error_msg').remove();
	$j('.inline_error_msg').addClass('hidden');

    $j.ajax(url, {
		data: {action:'send',
				to_email: $('to_email').value,
				from_email: $('from_email').value,
				email_body: $('email_body').value,
				product_id: $('email_product_id').value,
				hours_of_operation: $('hours_of_operation').value
				},
		dataType: "json",
		type: "POST",
		beforeSend : function(){
			$j('.email_form').append('<div class="step-holder-loader"></div>');
        },
		success: function(returnObject) {

			var result = eval(returnObject);

			$j('.step-holder-loader').remove();

			//restore default
			$j('.email_error_field').removeClass('email_error_field');
			$j('.error_msg').remove();
			$j('.inline_error_msg').addClass('hidden');
			$j('.the_form .label_msg').removeClass('hidden');

			if(result.errors){
				var error_fields = new Array();


				for(var i=0, len=result.errors.length; i<len; i++){
					var id = result.errors[i].field;
					$j($(id)).addClass('email_error_field').parent().find('.label_msg').addClass('hidden');
					$j($(id)).addClass('email_error_field').parent().find('.inline_error_msg').removeClass('hidden');
					error_fields.push($(id));
				}

				TINY.box.size($j('.tcontent').width(),$j('.tcontent').height());

			}else{
				TINY.box.fill('<div class="email_form" id="email_form"><div class="title">Email</div><div class="message">Email Sent Successfully!</div>');
				TINY.box.size($j('.email_form .title').outerWidth(),150);
			}
		},
		error: function() {
			$j('.step-holder-loader').remove();
			TINY.box.fill('<div class="email_form" id="email_form"><div class="title">Email</div><div class="message">Something went wrong.</div>');
			TINY.box.size($j('.email_form .title').outerWidth(),150);
		}
	});

    return false;
}

function validateEmail(str){
	return str.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);
}

/*
*****************************************************************
Q&A
*****************************************************************
*/
function processQuestion() {

	var url = '/ajax/ajax.product_questions.php?'+$('product_question_frm').serialize();

	new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(response)
        {
        	var result = JSON.parse(response.responseText);

        	if(result.error){
        		$('question-errors').innerHTML = result.error;
        		//TINY.box.fill($('product_question_frm'));
        	} else {
  	        	TINY.box.fill(result.success);
  	        	$j('.question_overlay_link').text('Edit your question');

				e=document.getElementById('product_question_frm');
				if (e){
					e.value = '';
				}
        	}
         },

        onFailure: function()
        {
        	TINY.box.fill('something went wrong ...');
        }
    });
}

function processAnswer() {

	var url = '/ajax/ajax.product_questions.php?action=process_answer&'+$('answer-question-form').serialize();

	new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(response)
        {
        	var result = JSON.parse(response.responseText);

        	if(result.error){
        		$('answer_errors').innerHTML = result.error;
        	} else {
        		TINY.box.fill(result.success);
				e=document.getElementById('answer-question-form');
				if (e){
					e.value = '';
				}
        	}
         },

        onFailure: function()
        {
        	TINY.box.fill('something went wrong ...');
        }
    });
}

function deleteQuestion(question_id) {

	if (confirm('Are you sure you want to delete this question?')){

		var url = '/ajax/ajax.product_questions.php?action=delete&question_id='+question_id;
		new Ajax.Request(url,
	    {
	        method:'get',
	        onSuccess: function(response)
	        {
	        	var result = JSON.parse(response.responseText);
	        	if(result.success == 'success'){
	        		$j('.question_overlay_link').html('Ask a question');
	        	}
	        	TINY.box.fill(result.message);
	         },

	        onFailure: function()
	        {
	        	TINY.box.fill('something went wrong ...');
	        }
	    });
	}
}

/*
*****************************************************************
product_review.php - show reviews
*****************************************************************
*/

function showreviews(product_id,procon,page)
{

	var url = '/ajax/ajax.procon_review.php?review_type=' +procon + "&product=" + product_id+"&page=" + page;
	var divname='review-section-wrap';

	 new Ajax.Request(url,
    {
        method:'get',

        onSuccess: function(response)
        {
//        	console.log(response);
           $(divname).innerHTML = response.responseText;
//           console.log($(divname));

        },

        onFailure: function()
        {
//        	console.log('response');
            $(divname).innerHTML = 'something went wrong ...';
        }
  });

}

function nextreview(num,total,reviews,pid,type)
{

 //view 5 next reviews
   var array=reviews.split(",");
   var divname='review-section-wrap';

   for (i=0;i<total;i++)
   {
		   if (x=$j('#page'+i))
		 	 x.removeClass('active');
		   if (x=$j('#bpage'+i))
		 	 x.removeClass('active');
   }
	 $j('#bpage_next').removeClass('disabled');
	 $j('#bpage_back').removeClass('disabled');

   if (num < 5)       $j('#bpage_back').addClass('disabled');
   if (total-num < 5) $j('#bpage_next').addClass('disabled');

   $j('#page'+num).addClass('active');
   $j('#bpage'+num).addClass('active');

   var url = '/ajax/ajax.show_review_page.php?num='+num+'&reviews='+array+'&total='+total+'&product_id='+pid+'&sort='+type;

	 new Ajax.Request(url,
    {
        method: 'get',

        onSuccess: function(response)
        {
           $(divname).innerHTML = response.responseText;
        },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
       }
  });

}

/*
*****************************************************************
product_review.php - helpful review
*****************************************************************
*/
function helpful(id,bool)
{
	var url = '/ajax/ajax.helpful_review.php?review_id=' +id + "&helpful=" + bool;
	var divname='#helpfulthanks'+id;
  $j('#refresh_helpful'+id).html('<div class="label-helpful">Updating...</div>');
	 new Ajax.Request(url,
    {
        method:'get',

        onSuccess: function(response)
        {
					$j(divname).load(document.URL +  ' #refresh_helpful'+id);
        },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });
}
/*
*****************************************************************
product_review.php - add terms by user
*****************************************************************
*/


function useraddterm(product_id,term,procon,customerid)
{

var divname='newterm';
var newterm = term.value;
var url = '/ajax/ajax.new_term.php?review_type=' +procon + "&product=" + product_id + "&term=" + newterm+ "&customerid=" + customerid;
    switch (procon)
    {
        case 1     :var x=document.getElementById("probox");
				            var y=document.getElementById("proterm");
                                    break;

        case 2     :var x=document.getElementById("conbox");
				            var y=document.getElementById("con");
                                    break;

        case 3     :var x=document.getElementById("bestbox");
				            var y=document.getElementById("best");
                                    break;
    }

 if ((newterm!=null)&&(newterm!='')&&(newterm!=' '))
 {
   var option=document.createElement("option");
   option.text=newterm;
   option.selected=true;

	 new Ajax.Request(url,
    {
        method:'get',

        onSuccess: function(response)
        {
				 var content=response.responseText;
				 var mylen=x.length;
				 content=parseInt(content);
				 var flag=0;
				 for (var i = 0; i < mylen; i++) {
                  if (x.options[i].value == content) {
                      x.options[i].selected = true;
											flag=1;
									}
							  }
                  if (flag==0){
 										   option.value=response.responseText;
                      //	x[0].selected=false;
                       try
                       {
                              // for IE earlier than version 8
                       x.add(option,x.options[null]);
                       }
                      catch (e)
                      {
                         x.add(option,null);
                      }
									}
                y.value='';
        },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });
 }
}
/*
*****************************************************************
product_review.php
remove image from review
*****************************************************************
*/

function removeReviewImage(imgUrl,id) {

          var url = '/ajax/ajax.removeReviewimage.php?image='+imgUrl+'&id='+id;
          var divname='imageremoved';
					new Ajax.Request(url,
          {
             method:'get',
             onSuccess: function(response)
        {
					  $(divname).innerHTML = response.responseText;
						document.getElementById('reviewimag').style.display='none';
						jQuery('.row').sameHeight({elements: '.row-name',flexible: true});
         },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });
}

/*
*****************************************************************
ajax/ajax.procon_review.php
Add a comment to a review
*****************************************************************
*/

function processComment(i) {

	var url = '/ajax/ajax.sharecomment.php?'+$('sharecomment'+i).serialize();

	new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(response)
        {
        	var result = JSON.parse(response.responseText);

        	if(result.error){
        		$('comment_errors').innerHTML = result.error;
        	} else {
	        	$('sharecomment'+i).innerHTML = result.success;
				e=document.getElementById('comment'+i);
				if (e){
					e.value = '';
				}
        	}
         },

        onFailure: function()
        {
        	$('sharecomment'+i).innerHTML = 'something went wrong ...';
        }
    });
}

/*
*****************************************************************
** account.php functions
*****************************************************************
*/

function email_pending_rewards_chioce(){

    var checked;
    if($('notify_rewards').checked){
        checked = 'Y';
    }else{
        checked = 'N';
    }

    $j.ajax("ajax/ajax.email_pending_rewards_chioce.php", {
        async: false,
        data: {
        isChecked: checked
        },
        dataType: "json",
        type: "POST",
        success: function(returnObject) {  }
    });
}

function toggleShippingAddrDiv(is_it_checked)
{
    if (is_it_checked == true)
    {
        $j('.shipping_addr_section').css({display: "none"});
    }
    else
    {
        $j('.shipping_addr_section').css({display: "block"});
    }

    return true;
}

/* Mailchimp email signup form javascript */
function validateMailchimpForm()
{
	var have_errors = false;
    $j(".req").each(function(){
    	if ($j(this).children('input').val() == '')
        {
			have_errors = true;
    		$j(this).children('.feedback.error').show();
        }
    	else $j(this).children('.feedback.error').hide();
    });
    if(!$j("#LIST_COMMERCIAL").attr("checked") &&
    		!$j("#LIST_PROMOTIONS").attr("checked") &&
    		!$j("#LIST_RECIPES").attr("checked"))
    {
    	have_errors = true;
    	$j('#list_error').show();
    }
    if(!$j("#EMAILTYPE_HTML").attr("checked") &&
    		!$j("#EMAILTYPE_TEXT").attr("checked") &&
    		!$j("#EMAILTYPE_MOBILE").attr("checked"))
    {
    	have_errors = true;
    	$j('#format_error').show();
    }
    if (have_errors)
    {
    	$j('.formstatus').show();
        return false;
    }
    else return true;

}
function accountLogout()
{
	var url = '/ajax/ajax.logout.php';

	 new Ajax.Request(url,
	 {
        method:'get',

        onSuccess: function(response)
        {
        	alert('You have successfully logged out.');
        	window.location.reload(true);
        },

        onFailure: function()
        {
            alert('Sorry--logging out failed.');
        }
	 });
}
/*
*****************************************************************
login functionality
*****************************************************************
*/
function account_login(){

	$j('#password_errors').html('');
	$j('#checkout_password_errors').not('.hidden').addClass('hidden');
	$j('.checkout_error_field').removeClass('checkout_error_field');

	var type = "";
	var product_id ="";
	var log = "" ;
	var pwd;

	if($j('#checkout_page_login') && $j('#checkout_page_login').val() == 'Y'){
		type = 'checkout';
	}else if($j('wishlist_login') && $j('wishlist_login').value == 'Y'){
		type = 'wishlist';
		product_id = $j('popup_product_id').value;
	} else if ($j('answer_question_login') && $j('answer_question_login').value == 'Y') {
		type = 'answer_question';
	}

	if($j('#log')){ log = $j('#log').val(); }else{ log = $j('#shipping_email').val(); }
	if($j('#pwd')){ pwd = $j('#pwd').val(); }else{ pwd = $j('#account_step_password').val(); }

	var succeeded = false;
    	$j.ajax("/ajax/ajax.validate_account_frm.php", {
		data: {
			action  	: 'validate_password',
			log     	: log,
			pwd     	: pwd,
			type    	: type,
			product_id	: product_id
		},
		dataType: "json",
		type: "POST",
		success: function(returnObject) {

			var result = eval(returnObject);
			console.log(returnObject);
			if(result.errors){
				$j('#password_errors').html(result.errors);
				$j('#password_errors').show();
				var error_text = $j('#checkout_password_errors').removeClass('hidden').text();
				$j('#account_step_password').addClass('checkout_error_field');

				if(type == 'checkout'){
					jQuery('.step-holder').sameHeight({
						elements: '.register-box',
						flexible: true
					});
					sendToDataLayer('Checkout Error', 'checkout_password_errors', error_text);
				}else{
					TINY.box.size($j('.tcontent').width(),$j('.tcontent').height());
				}

			} else {
				succeeded = true;

				if (type == 'answer_question') {
					TINY.box.show({url:'ajax/ajax.product_questions.php',post:'action=show_answer_form&question_id=' + $j('#answer_question_id').val(),fixed:false,closejs:function(){window.location.reload(true);}});
					return false;
				}

				if($j('#href').val()){
//					window.location.href = $('href').value;
					window.location.replace($j('#href').val());
//					window.location.reload(true);
				} else {
					window.location.reload(true);
				}

				//$('account_login_form').submit();
			}
		},
		error: function (xhr, status, thrownError) {
			//console.log(xhr.statusText);
			//console.log(xhr.responseText);
			//console.log(xhr.status);
			//console.log(thrownError);
        	alert('Something went wrong.');
		}
	});
	return false;
}

function validate_account_frm(){

	var succeeded = false;

	$j.ajax("/ajax/ajax.validate_account_frm.php", {
        data: {	action       : 'new_account',
        		first_name   : $("info[first_name]").value,
				email        : $("info[email]").value,
				password     : $("info[password]").value,
				password_conf: $("info[password_conf]").value
        },
        dataType: "json",
        type: "POST",
        success: function(returnObject) {

		//var errors = JSON.stringify(returnObject)
		var result = eval(returnObject);
//		console.log(result);
		$('signup_errors').hide();

		if(result.errors){
			( function($) {
				$('#signup_errors').html(result.errors);
			} ) ( jQuery );

			$('signup_errors').show();
			TINY.box.size($j('.tcontent').width(),$j('.tcontent').height());

		}
		if(result.error_fields.length){

			//restore default
			var all_ids =['info[first_name]',
			              'info[email]',
					      'info[password]',
					      'info[password_conf]'
					      ];
			for(var i=0, len=all_ids.length; i<len; i++){
				$j($(all_ids[i])).removeClass('checkout_error_field');
			}

			//change background for fields with errors
			for(var i=0, len=result.error_fields.length; i<len; i++){

				var id = result.error_fields[i];
				$j($(id)).addClass('checkout_error_field');

			}
			TINY.box.size($j('.tcontent').width(),$j('.tcontent').height());
		}
		else {
			succeeded = true;
			$('frm_new_account').submit();
		}
        }
    });

    return succeeded;
}

function validate_account_frm_old(){

	var succeeded = false;

	var location_type = "";
	if($("info[location_type_residential]").checked){
		location_type = "residential";
	} else if($("info[location_type_commercial]").checked) {
		location_type = "commercial";
	}

	var location_type2 = "";
	if($("info2[location_type_residential]").checked){
		location_type2 = "residential";
	} else if($("info2[location_type_commercial]").checked) {
		location_type2 = "commercial";
	}

	$j.ajax("/ajax/ajax.validate_account_frm.php", {
        data: {
		action       : 'new_account',
		first_name   : $("info[first_name]").value,
		last_name    : $("info[last_name]").value,
		address1     : $("info[address1]").value,
		city         : $("info[city]").value,
		state        : $("info[state]").value,
		zip          : $("info[zip]").value,
		phone        : $("info[phone]").value,
		location_type: location_type,
		email        : $("info[email]").value,
		password     : $("info[password]").value,
		password_conf: $("info[password_conf]").value,
		role_id      : $("info[role_id]").value,
		institution_type_id: $("info[institution_type_id]").value,

		same_as_billing    : ($("same_as_billing").checked ? "Y" : "N"),

		info2_address1     : $("info2[address1]").value,
		info2_city         : $("info2[city]").value,
		info2_state        : $("info2[state]").value,
		info2_zip          : $("info2[zip]").value,
		info2_phone        : $("info2[phone]").value,
		info2_location_type: location_type2
        },
        dataType: "json",
        type: "POST",
        success: function(returnObject) {

		//var errors = JSON.stringify(returnObject)
		var result = eval(returnObject);
//		console.log(result);
		$('signup_errors').hide();

		if(result.errors){
			( function($) {
				$('#signup_errors').html(result.errors);
			} ) ( jQuery );

			$('signup_errors').show();
			TINY.box.size($j('.tcontent').width(),$j('.tcontent').height());

		}
		if(result.error_fields.length){

			//restore default
			var all_ids =['info[first_name]',
				      'info[last_name]',
				      'info[address1]',
				      'info[city]',
				      'info[state]',
				      'info[zip]',
				      'info[phone]',
				      'info[location_type]',
				      'info2[address1]',
				      'info2[city]',
				      'info2[state]',
				      'info2[zip]',
				      'info2[phone]',
				      'info2[location_type]',
				      'info[email]',
				      'info[password]',
				      'info[password_conf]',
				      'info[role_id]',
				      'info[institution_type_id]',
				      'location_residential',
				      'location_commercial',
				      'location_residential2',
				      'location_commercial2',
				      ];
			for(var i=0, len=all_ids.length; i<len; i++){
				$j($(all_ids[i])).removeClass('checkout_error_field');
			}

			//change background for fields with errors
			for(var i=0, len=result.error_fields.length; i<len; i++){

				var id = result.error_fields[i];
				$j($(id)).addClass('checkout_error_field');

			}
		}
		else {
			succeeded = true;
			$('frm_new_account').submit();
		}
        }
    });

    return succeeded;
}

/*
*****************************************************************
wishlist functionality
*****************************************************************
*/
function wishlist(product_id)
{
    var customer_id       = parseInt($j('#wishlist_customer_id').val());
    var wishlist_id       = parseInt($j('#wishlist_link_'+product_id).data('wishlist-id'));
    var wishlist_action   = $j('#wishlist_link_'+product_id).data('wishlist-action');
    var ret               = false;
    switch (wishlist_action)
    {
        case 'wishlist_add'     :return add_to_wishlist(product_id, customer_id);
                                    break;

        case 'wishlist_delete'  :return remove_from_wishlist(wishlist_id, product_id, customer_id);
                                    break;

        default                 :return false;
                                    break;
    }


    return false;
}


function add_to_wishlist(product_id, customer_id)
{
    var url = '/ajax.wishlist.php?action=add&product_id=' + product_id + '&customer_id=' + customer_id;

    /* do a little validation */
    if (!customer_id)
    {
        alert("You must be logged in to do this.");
        return false;
    }

    if (!product_id)
    {
        alert("Invalid product id!");
        return false;
    }
    /* we have the data we need now, so insert it. */
	
    $j.ajax(url,
    {
        type:'GET',
        success: function(transport)
        {
            var response = '';

            switch (transport.responseText)
            {
                case "-1"   :alert("Failed To Insert Item.");
                                break;

                case "-2"   :alert("Already In Your WishList");
                                break;

                default     :$j('#wishlist_link_' + product_id).data('wishlist-id',transport.responseText);
                			 $j('#wishlist_link_' + product_id).data('wishlist-action','wishlist_delete');
                             $j('#wishlist_link_' + product_id).html('Remove From My List');

                            alert("Item Added Successfully");

                                break;
            }
        },

        onFailure: function()
        {
            alert('Something went wrong.');
        }
    });


    return false;
}

function remove_from_wishlist(wishlist_id, product_id, customer_id, coming_from_my_account)
{
    var url = '/ajax.wishlist.php?action=delete&wishlist_id=' + wishlist_id;

    /* do a little validation */
    if (!wishlist_id)
    {
        alert("Invalid WishList Item.");
        return false;
    }

    /* we have the data we need now, so insert it. */
    $j.ajax(url,
    {
        type:'get',

        success: function(transport)
        {
            var response = '';

            switch (transport.responseText)
            {
                case "-1"   :alert("Failed To Delete Item From WishList.");
                                break;

                default     :
                                if (coming_from_my_account == 'undefined' || coming_from_my_account == null)
                                {
                                	$j('#wishlist_link_' + product_id).data('wishlist-id','0');
                       			 	$j('#wishlist_link_' + product_id).data('wishlist-action','wishlist_add');
                                    $j('#wishlist_link_' + product_id).html('+ Add to Wishlist')

                                    alert("Item Removed From WishList.");
                                }
                                else
                                {
                                    location.href = "/account.php";
                                    exit(0);
                                }

                                break;
            }
        },

        onFailure: function()
        {
            alert('Something went wrong.');
        }
    });


    return false;
}

/*
*****************************************************************
gift_certificate.php functionality
*****************************************************************
*/
function add_gift_card_to_cart(product_id,sku,img_path){
	var errors = false;

    removeCartPopup();
    $j('.gift_card_message_alert').text(''); //clear errors

    var price = "0.00" ;
    var card_delivery_date = $('text_id_card_delivery_date').value;
    var option = $j('#amount_selector option:selected').attr('id');

    if( $('amount_selector').value == 'custom') {
    	price = parseFloat($('custom_amount').value).toFixed(2);
	}else{
		price = parseFloat($('amount_selector').value).toFixed(2);
	}

    if($('recipient_email').value == ''){
    	$j('#recipient_email_error').text('Recipient Email is Required');
    	errors = true;
    } else if( !validateEmail( $('recipient_email').value ) ){
    	$j('#recipient_email_error').text("The e-mail address entered is not valid.");
    	errors = true;
	}
    if(price > 500){
		$j('#custom_amount_error').text('Gift card amount must be $500.00 or less');
		errors = true;
	}
    if(price < 10){
		$j('#custom_amount_error').text('Gift card amount must be $10.00 or more');
		errors = true;
	}
    //make sure the card is between today and the next 6 month. (format yyyy-MM-dd)
    if(card_delivery_date){
    	var user_date = new Date(card_delivery_date);
    	var today = new Date();
        today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        var endDate = new Date(today);
        endDate.setMonth(6);
    	if(user_date < today || user_date > endDate) {
    		$j('#delivery_date_error').text('Delivery date must be within the next six month');
    		errors = true;
    	}
    }

    var info = new Object();
    info['message'] = $('gift_card_message').value;
    info['recipient_email'] = $('recipient_email').value;
    info['sender_name'] = $('sender_name').value;
    info['card_delivery_date'] = card_delivery_date;
    info['price'] = price;

    if(!errors){
	    var cartStats = {};
	    if (storeGiftCardItem(product_id, option, cartStats, info)) {
	    	//alert(cartStats.return_string);
	        var name = "";
	        if ($j("div.maincontent_title > h1")) {
	            name = $j("div.maincontent_title > h1").text();
	        }

	        //window.scrollTo(0, 0);
	        showCartPopup(name, sku, price, quantity, img_path, cartStats);
	    }
    }

    return false;
}

function updateGiftCardAmtDropdown(product_id){

	var price = "0.00" ;
	$j('#custom_amount_error').text('');

	if($('amount_selector').value == 'custom'){
		$('custom_amount_div').show();
		price = parseFloat ( $('custom_amount').value );
		if(!price){ price = 0; }
		price = price.toFixed(2);
		$('custom_amount').value = price;
		if(price > 500){
			$j('#custom_amount_error').text('Gift card amount must be $500.00 or less');
			return false;
		}
		if(price < 10){
			$j('#custom_amount_error').text('Gift card amount must be $10.00 or more');
			return false;
		}
	}else{
		$('custom_amount_div').hide();
		price = parseFloat ( $('amount_selector').value );
		price = price.toFixed(2);
	}

	$j('#one_pc_price_' + product_id).text("Our Price: $ " + price);

	return false;
}

function storeGiftCardItem(product_id, option, cartStats, info) {

    var succeeded = false;
    $j.ajax("/ajax/ajax.add_one_to_cart.php", {
        async: false,
        data: {
            product_id: product_id,
            quantity: '1',
            option: (typeof option == undefined) ? "": option,
            action: 'store_gift_card',
            info: info
        },
        dataType: "json",
        type: "POST",
        success: function(returnObject) {

            cartStats.item_count = returnObject.count;
            cartStats.subtotal = returnObject.subtotal;
            cartStats.ret_str = returnObject.ret_str;

            if (returnObject.errors.length > 0) {
                alert(returnObject.errors);
            }
            else {
                succeeded = true;
            }
        }
    });

    return succeeded;
}
function video_box(box,embed_url){

	( function($) {

		//If a Popup is showing, remove it
		if($('#mask').css("display") == 'block'){
			$('.image_popup').fadeOut(300);
		}

		var embed_code = '<iframe id="popup_iframe" width="560" height="315" src="' + embed_url +'&autoplay=1" frameborder="0"></iframe>';

		$(big_video_popup_img).html(embed_code);

			//Fade in the Popup
			$(box).fadeIn(300);

			//Set the center alignment padding + border see css style
			var popMargTop = ($(box).height() + 24) / 2;
			var popMargLeft = ($(box).width() + 24) / 2;

			$(box).css({
				'margin-top' : -popMargTop,
				'margin-left' : -popMargLeft
			});

			// Add the mask to body
			$('body').append('<div id="mask"></div>');
			$('#mask').fadeIn(300);

			// When clicking on the button close the popup closed and stop the video
			$('a.close').live('click', function() {

				// assign the src to null, this then stops the video been playing
				$('#popup_iframe').attr('src', '');

				// Finally you reasign the URL back to your iframe, so when you hide and load it again you still have the link
				$('#popup_iframe').attr('src', embed_url);

				$('#mask , .video_popup').fadeOut(300 , function() {
					$('#mask').remove();
				});
				return false;
			});

	} ) ( jQuery );

}
function change_video(video,embed_url){

	//If a border is showing, remove it
	( function($) {
		$('.additional_videos_li').removeClass("selected");
	} ) ( jQuery );

	//add border to current video
	$j(video).parent().parent().addClass("selected");

	( function($) {

		// assign the src to null to stop the previous video playing
		$('#popup_iframe').attr('src', '');

		//change popup video
		var embed_code = '<iframe id="popup_iframe" width="560" height="315" src="' + embed_url +'" frameborder="0"></iframe>';

		$(big_video_popup_img).html(embed_code);

	} ) ( jQuery );
}
/*
*****************************************************************
my_account.php
unsubscirbe to a product
*****************************************************************
*/


function productUnsubscirbe(pid) {

var url = '/ajax/ajax.productunsubscribe.php?product_id='+pid;
var divname='unsubscribe'+pid;

 new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(response)
        {
					  $(divname).innerHTML = response.responseText;
         },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });


}

/*
*****************************************************************
my_account.php
remove image from profile
*****************************************************************
*/

function removeimage(imgUrl) {

          var url = '/ajax/ajax.removeimage.php?image='+imgUrl;
          var divname='imageremoved';
					new Ajax.Request(url,
          {
             method:'get',
             onSuccess: function(response)
        {
					  $(divname).innerHTML = response.responseText;
						document.getElementById('accountimag').style.display='none';
 					  setTimeout(function(){$(divname).innerHTML =''},3000);

         },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });
}
function add_review(pid){

		 var info=$j("#postreview"+pid).serialize();
         var url = '/ajax/ajax.post_review.php?action=submitted&'+info;

         var divname = 'rev_response'+pid;
					new Ajax.Request(url,
          {
             method:'get',
						 data:info,
             onSuccess: function(response)
        {
			if ( $j("#rev").length) {
	                 $j('html,body').animate({ scrollTop: $j('#rev').offset().top}, 1000);
			}
              $j("#postreview"+pid).closest("#write_review"+pid).addClass("hidden");
			  var elem = $j('#'+divname);
              if(elem.hasClass("hidden")) elem.removeClass('hidden');
			  else elem.addClass('hidden');
						   elem.html(response.responseText);
               $j("#write_review"+pid).addClass("hidden");
							 $j("#star_rating").val("");
							 $j("#rating_box>ul>li.setted").removeClass("setted");
							 $j('#postreview'+pid).trigger("reset");
		    },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });
}

function sendToDataLayer(eventCategory, eventLabel, eventAction)
{
	if (eventAction === undefined) { eventAction = "Click"; }
	window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': eventCategory,'gaEventAction': eventAction,'gaEventLabel': eventLabel});
}
function sendToDataLayer_key(eventCategory, eventLabel,e,eventAction)
{
	if (eventAction === undefined) { eventAction = "Click"; }
	var code = (e.keyCode ? e.keyCode : e.which);
 if(code == 13){
		window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': eventCategory,'gaEventAction': eventAction,'gaEventLabel': eventLabel});
		}
}

function refresh_review (){

				 var info=$j("#postreview").serialize();
         var url = '/ajax/ajax.post_review.php?action=connected&'+info;
         var divname='rev_response';
	 			 $j('#reviewer_info_refreshed').html('<div class=loader></div>');
					new Ajax.Request(url,
          {
             method:'get',
						 data:info,
             onSuccess  : function(response)
        {
						var elem = document.getElementById(divname);
              if(response.responseText.length > 5){
			            $j("#write_review").addClass("hidden");
                  elem.className = elem.className == 'hidden' ? '' : 'hidden';
									$(divname).innerHTML = response.responseText;
									$j("#star_rating").val("");
							    $j("#rating_box>ul>li.setted").removeClass("setted");
									$j('#postreview').trigger("reset");
							 }
						$j('#reviewer_info').load(document.URL +  ' #reviewer_info_refreshed');
         },

        onFailure: function()
        {
            $(divname).innerHTML = 'something went wrong ...';
        }
  });

}

function social_open (options){
			options.windowName = options.windowName || 'ConnectWithOAuth';
			options.windowOptions = options.windowOptions || 'location=0,status=0,width='+options.width+',height='+options.height+',scrollbars=1';
			options.callback = options.callback || function () {
						//refresh_review();
			};
			var that = this;
			that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
			that._oauthInterval = window.setInterval(function () {
				if (that._oauthWindow.closed) {
					window.clearInterval(that._oauthInterval);
		    	options.callback();
				}
			},10);
    }
function add_free_item()
{
    var cartStats = {};
    var itemInfo = {
    	product_id:'276454',
        quantity:'1',
        price: '0.00',
        name: '$5 Starbucks Gift Card',
        sku: '0026-SB2385',
        img_path: 'https://itempics-tigerchef.netdna-ssl.com/Starbucks-Gift-Card-276454_thumb.jpg',
        option: '',
        selected_optional_options:'',
        customizations: '',
        presets: '',
        accessories:'',
        option_quantities:'',
        page_type: ''};

    $j.ajax("/ajax/ajax.add_one_to_cart.php", {
        data: itemInfo,
        dataType: "json",
        type: "POST",
        success: function(returnObject) {
            cartStats.item_count = returnObject.count;
            cartStats.subtotal = returnObject.subtotal;
            cartStats.ret_str = returnObject.ret_str;
            cartStats.cart_price = returnObject.cart_price;
            if (returnObject.errors.length > 0) {
                var errors = jQuery.parseJSON(returnObject.errors);
                if(errors.type && errors.type.length > 0){
                    cartStats.errors = errors;
                    if(errors.type == 'min_qty' || errors.type == 'max_qty'){
                        succeeded = true;
                        cartStats.qty = errors.qty;
                    }
                }else{
                    alert(errors.msg);
                }
            }
            else {
                succeeded = true;
            }

    updateCartCount(cartStats.item_count);
    $j(".cart_subtotal").each(function(){$j(this).html((cartStats.subtotal.toFixed(2)).toLocaleString());});
}
    });
}
   function delete_free_item()
   {
	   if($j('#free_item_id'))
	   {
	       update_cart($j('#free_item_id').html());
	       updateCartCount(result.item_count);
	       $j("#cart_subtotal_header").each(function(){$j(this).html((result.subtotal.toFixed(2)).toLocaleString());});
	   }
   }
function add_to_cart_track(id,name,brand,price){
	var qty_box = document.getElementById("quantity"); 
	if (qty_box) var qty = qty_box.value;
	else var qty = 1;
	gtag('event', 'add_to_cart', {
			  "items": [
				{
				  "id": id,
				  "name": name,
				  "brand": brand,
				  "quantity":qty,
				  "price": price
				}
			  ]
			});
return;
}
function select_content_track(p,list){
	gtag('event', 'select_content', {
		'content_type': 'product', 'items': [
		{
			'id': p.id,
			'name': p.name,
			'list': list,
			'brand': p.brand_name,
			'price': p.price}
		]});
		return;
}
function remove_from_cart_track(p, qty){
	gtag('event', 'remove_from_cart', {
		'items': [
		{
			'id': p.id,
			'name': p.name,
			'brand': p.brand_name,
			'quantity': qty,
			'price': p.price}
		]});
}

