var legend = {1:'POOR', 2:'FAIR', 3:'AVERAGE', 4:'GOOD', 5:'EXCELLENT'};


	function toggle_review(someId) {
    var someElem = document.getElementById(someId);
    someElem.className = someElem.className == 'hidden' ? '' : 'hidden';
    if (someId=="write_review")	
	$j('html,body').animate({ scrollTop: $j('#write_review').offset().top}, 1000);
		return false;
}

function initPage()
{

	var rates = document.getElementsByTagName("ul");
	for (i = 0; i < rates.length; i ++)
	{
		if (rates[i].className.indexOf("rating") != -1)
		{
			rates[i]._lis = rates[i].getElementsByTagName("li");
			
			rates[i].onmouseover = function() {
				for (k = 0; k < this._lis.length; k++)
				{
					if (this._lis[k].className.indexOf("active") != -1)
					{
						this._active = this._lis[k];
						this._lis[k].className = this._lis[k].className.replace("active", "");
					}
					
					if (this._lis[k].className.indexOf("setted") != -1)
					  if(this.parentNode.className.indexOf("edit_rate")===0){
						this._setted = this._lis[k];
						this._lis[k].className = this._lis[k].className.replace("setted", "");
					}
				}
			}
			rates[i].onmouseout = function() {
				if (this._active && this._active.className.indexOf("active") == -1 && !this._setted)
				{
					this._active.className += " active";
				}
				
				if (this._setted && this._setted.className.indexOf("setted") == -1) 
				{
					this._setted.className += " setted";
				}
			return false;	
			}
			
			var links = rates[i].getElementsByTagName("a");
			for (k = 0; k < links.length; k ++)
			{
				links[k].onclick = function() {
					
					this.parentNode.parentNode._in_hover = true;
					this.parentNode.parentNode._setted = this.parentNode;
					$j(this.parentNode.parentNode).data('setted_index',$j(this).html());
					
					if (this.parentNode.className.indexOf("setted") == -1)	
					{
						this.parentNode.className += " setted";
					}
					$j(this).closest( "form" ).find('input#star_rating').val(this.href.split('#')[1]);
					
					$j(this).parents('#rating_box').find('.rating_legend').html(legend[$j(this).html()]);
					this.blur();
					return false;
				}
			}
		}
	}
} 

$j('.rating a').hover(
		function() {
			$j(this).parents('#rating_box').find('.rating_legend').html(legend[$j(this).html()]);
		}, function() {
			if($j('.rating').data('setted_index')){
				$j(this).parents('#rating_box').find('.rating_legend').html(legend[$j(this).parents('.rating').data('setted_index')]);
			}else{
				$j(this).parents('#rating_box').find('.rating_legend').html('');
			}
		}
);
$j('.rating a').click(function() {
	var elm = $j(this).parents('#write_review').find('#review_inputs');
	elm.removeClass("hidden");
		});

if (window.addEventListener)
	window.addEventListener("load", initPage, false);
else if (window.attachEvent)
	window.attachEvent("onload", initPage);
	

function validateForms(form_id)
{
  var blnvalidate = true;
	var valid_radio = false;
	var error_text="";
  var elementsInputs;
	var currentForm = document.getElementById(form_id);
  elementsInputs = currentForm.elements;
  if  (!($j('#'+form_id+' input#star_rating').val() >0)){
           blnvalidate = false;
					 $j('#'+form_id+' #rating_box').addClass( "invalid" );
	}		 		
	else {   $j('#'+form_id+' #rating_box').removeClass( "invalid" );} 
  
    for (var intCounter = 0; intCounter < elementsInputs.length; intCounter++)
    {
		if (elementsInputs[intCounter].classList.contains('req_text'))
        { 
	            if (validateText(elementsInputs, intCounter))
            {
                blnvalidate = false;
							  elementsInputs[intCounter].classList.remove('invalid');
                elementsInputs[intCounter].className +=' invalid';
            }
			else elementsInputs[intCounter].className=elementsInputs[intCounter].className.replace('invalid','');
    }
     if (elementsInputs[intCounter].classList.contains("req_email"))
        { 
				if (validateEmail(elementsInputs, intCounter))
            {
                blnvalidate = false;
						    if(!elementsInputs[intCounter].classList.contains('invalid'))
                elementsInputs[intCounter].className +=' invalid';
            }
			else elementsInputs[intCounter].className=elementsInputs[intCounter].className.replace('invalid','');
        }

		if (elementsInputs[intCounter].classList.contains("req_url"))
        {
            if (validateEmail(elementsInputs, intCounter))
            {
                blnvalidate = false;
                elementsInputs[intCounter].className +=' invalid';
            }
			else elementsInputs[intCounter].className=elementsInputs[intCounter].className.replace('invalid','');
		}	
    
		valid_select=false;

		if (elementsInputs[intCounter].classList.contains("req_select"))
        {
				    if (elementsInputs[intCounter].value != '')
            {
                valid_select = true;
            }
				
	    if (valid_select == false){
	       blnvalidate = false;
				 if(!elementsInputs[intCounter].classList.contains('invalid'))
                  elementsInputs[intCounter].className +=' invalid';
	    }					
			else elementsInputs[intCounter].className=elementsInputs[intCounter].className.replace(' invalid','');
		}	
				
	}
	if(!blnvalidate) $j('#'+form_id+' #errorBox').show();
	else $j('#'+form_id+' #errorBox').hide();
	$j('html,body').animate({ scrollTop: $j('#write_review').offset().top}, 1000);

 return blnvalidate;
}

function validateEmail(elementsInputs, intCounter)
{
    var emailFilter=/^.+@.+\..{2,3}$/;
    if (!emailFilter.test(elementsInputs[intCounter].value))
    {
        return true;
    }
}

function validateText(elementsInputs, intCounter)
{
	 if (elementsInputs[intCounter].value == "")
    {
        return true;
    }
}
