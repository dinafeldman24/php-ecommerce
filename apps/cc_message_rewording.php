<?
class CCMessageRewording {
    static function insert($info) {
        return db_insert ( 'credit_card_message_rewording', $info);
    }
    static function update($id, $info) {
        return db_update ( 'credit_card_message_rewording', $id, $info, 'id');
    }
    static function delete($id) {
        return db_delete ( 'credit_card_message_rewording', $id );
    }
    static function get($id = 0, $original_message = '', $order = '', $order_asc = '', $limit = 0, $start = 0, $get_total = false) {
        $sql = "SELECT ";
        if ($get_total) {
            $sql .= " COUNT(DISTINCT(credit_card_message_rewording.id)) AS total ";
        } else {
            $sql .= " credit_card_message_rewording.* ";
        }

        $sql .= " FROM credit_card_message_rewording ";

        $sql .= " WHERE 1 ";

        if ($id > 0) {
            $sql .= " AND credit_card_message_rewording.id = $id ";
        }

        if ($original_message) {
            $sql .= " AND `credit_card_message_rewording`.original_message = $original_message";
        }

        if (! $get_total) {
            $sql .= ' GROUP BY credit_card_message_rewording.id ';

            if ($order) {
                $sql .= " ORDER BY ";

                $sql .= addslashes ( $order );

                if ($order_asc !== '' && ! $order_asc) {
                    $sql .= ' DESC ';
                }
            } else {
                $sql .= " ORDER BY credit_card_message_rewording.id DESC";
            }
        }

        if ($limit > 0) {
            $sql .= db_limit ( $limit, $start );
        }

        if (! $get_total) {
            $ret = db_query_array ( $sql );
        } else {
            $ret = db_query_array ( $sql );
            return $ret [0] ['total'];
        }
        return $ret;
    }
    static function get1($id) {
        $id = ( int ) $id;

        if (! $id)
            return false;
        $result = self::get ( $id );

        return is_array ( $result ) ? array_shift ( $result ) : false;
    }

    /**
     * @param $original_msg
     * @param bool $match_exact : true - will reword only for exact match, false - will reword for substring match
     * @return mixed
     */
    static function reword($original_msg, $match_exact=true){
        $messages = CCMessageRewording::get();
        foreach ($messages as $message) {
            if($match_exact){
            	if(strtolower($original_msg)===strtolower(trim($message['original_message']))){return $message['replacement_message'];}
            }else{
                if(strpos(strtolower($original_msg),strtolower($message['original_message']))!==false){return $message['replacement_message'];}
            }
        }
        return $original_msg;
    }
}

