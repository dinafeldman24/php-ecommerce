<?php

/*****************
*   Class for interfacing
*   with RMA items
*   Hadassa Golovenshitz
*   March 2 2015
*
*****************/

class RMAItem{

    var $id;
    var $order_rma_id;
    var $order_item_id;
    var $restocking_fee;
    var $restocking_fee_type;
    var $quantity;
    var $action;
    static $restocking_fee_types = array('%', '$');
    static $actions = array('Restock', 'Dispose', 'Return to Vendor');


    function RMAItem($id = null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    /*******************
    *
    * DB Functions 
    *
    ******************/

    function populate($id){
        $info = db_get1('SELECT * FROM rma_items WHERE id = ' . $id);
        if(is_array($info)){
            $this->setAll($info, $id);
        }
    }

    function insert($info){
        $id = db_insert('rma_items', $info);
        if(is_numeric($id)){
            $this->setAll($info, $id);
            return true;
        }
        return false;
    }

    /********************
    *
    * Getter and Setters
    *
    ********************/

    function setAll($info, $id = null){
        if(!is_null($id)){
            $this->id = $id;
        }
        elseif(isset($info['id'])){
            $this->id = $info['id'];
        }
        $this->order_rma_id = $info['order_rma_id'];
        $this->order_item_id = $info['order_item_id'];
        $this->restocking_fee = $info['restocking_fee'];
        $this->restocking_fee_type = $info['restocking_fee_type'];
        $this->action = $info['action'];
        $this->action = $info['quantity'];
    }

    /*******************
    *
    * Display Functions
    *
    *******************/

    static function buildDropdown($values){
        $str = '';
        foreach($values as $curr){
            $str .= "<option value='$curr'>$curr</option>";
        }
        return $str;
        
    }
    static function getRFTypesDropdown(){
        return self::buildDropdown(self::$restocking_fee_types);
    }

    static function getActionDropdown(){
        return self::buildDropdown(self::$actions);
    }

}
