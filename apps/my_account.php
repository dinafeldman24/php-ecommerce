<?
class MyAccount
{
    static function show_title_bar($title = "My Account")
    {
    	global $CFG;
    	if (MyAccount::logged_in()) echo "<div class='login-wrapper'>";
    	?>  
			<div class="content-wrap account-content">
			<a name="top"></a>
			<div class="content-title">
			<? if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $title == "My Account") $title = "My Essensa / Innovatix Member Account";?>
				<h1><?= $title ?></h1>
				<?
			  	
                if ($_SESSION['cust_acc_id'])
                {?>
				<nav class="account-nav">				
                	<? if (Customers::seeIfEssensaMember() && $title == "My Account") $title = "My Essensa / Innovatix Member Account";
                ?>
					<span class="text-welcome"><a href="<?=$CFG->baseurl?>account.php">Welcome <?=$_SESSION['cust_name']?></a></span>				
					<ul>
						<li><a href="<?=$_SERVER['PHP_SELF']."?action=old_orders"?>">View all Order History</a></li>
						<li><a href="<?=$CFG->baseurl?>returns.php">Return Items or Gifts</a></li>
						<li><a href="<?=$_SERVER['PHP_SELF']."?action=logout"?>">Sign Out</a></li>
					</ul>
				</nav>
				<? } ?>
			</div>  
		</div>
  	<? if (MyAccount::logged_in()) echo "</div>";   
   	   return true;
    }

    static function show_my_account_landing($msg='')
    {    
    	global $CFG;
    	
                
            if ($_GET['track_new_user']=='y'){?>
              <script>
                window.dataLayer = window.dataLayer || [];    dataLayer.push
                                ({'event': 'gaTriggerEvent','gaEventCategory': 'Account',
                                  'gaEventAction': 'Click',
                                    'gaEventLabel': 'Registration'});  
             </script>
            <?}    ?>
    	

		<div class="account-box main-section">						

            	
            	<div class="account-block">
						<div class="col">
						<?echo MyAccount::showOrders('recent');?>
						<a name="account_settings"></a>
				        <?if ($msg)
	    	             {?>
	    	      			      <span class="account-error"><?=$msg;?></span>
                         <?}?>
							<div class="title-box">
								<h2>Account <span>settings</span></h2>
							</div>
							<div class="content-box">
								<div class="holder">
									<dl>
									<? $info = Customers::get1($_SESSION['cust_acc_id']);
									if ($info) 
									{
									?>
										<dt>First Name:</dt>
										<dd><?=$info['first_name']?></dd>
										<dt>Last Name:</dt>
										<dd><?=$info['last_name']?></dd>
										<dt>Email:</dt>
										<dd><?=$info['email']?></dd>
										<dt>Password:</dt>
										<dd>******</dd>
										<? if ($info['institution_type_id']){?>
										<dt>Type of Institution or Company:</dt>
										<dd><?=$info['institution_type_caption']?></dd>
										<? }?>
										<? if ($info['institution_name']){?>
										<dt>Name of Institution or Company:</dt>
										<dd><?=$info['institution_name']?></dd><? }?>
										<? if ($info['role_id']){?><dt>Role:</dt>
										<dd><?=$info['role_caption']?></dd><? }?>										
										<? if ($info['ein_number']){?><dt>EIN Number:</dt>
										<dd><?=$info['ein_number']?></dd><? }?>							
										<? if ($info['img_url']){?>
										<dt>My Picture:</dt>
										<dd>
										<a href="<?=$_SERVER['PHP_SELF']. "?action=change_password"?>"><img class="my-picture" src="upload/<?=$info['img_url']?>"  height="40" border="0"></a> 
										</dd><? }?>
									</dl>
									<a href="<?=$_SERVER['PHP_SELF']. "?action=change_password"?>" class="btn-edit">Edit</a>
								</div>
							</div>
						</div>
						<?
						}
						?>    
  					<div class="col">
							<div class="title-box">
								<h2>Billing <span>address</span></h2>
							</div>
							<div class="content-box">
								<div class="holder">
								<? $billing_addresses = Customers::getPrimaryBillingAddress($_SESSION['cust_acc_id']);

	        			if (is_array($billing_addresses))
	        			{
	           				$billing        = $billing_addresses[0];
								?>
									<span class="name"><?=$billing['first_name']." ".$billing['last_name']?></span>
									<address>
									<? if ($billing['company']) {?><?=$billing['company']?><br /><? } ?>
									<?=$billing['address1']?><br />
									<? if ($billing['address2']) {?><?=$billing['address2']?><br /><? } ?>
									<?= $billing['city'] . ", " . $billing['state'] . " " . $billing['zip'] ?>
									<? if ($billing['location_type']) {?><br /><?=$billing['location_type']?><? } ?>							
									<? if ($billing['phone']) {?><br /><?=FixPhone::displayPhone($billing['phone'])?><? } ?>
									</address>														
							<?
						}
						?>
								</div>
								<a href="<?=$_SERVER['PHP_SELF']. "?action=change_billing"?>" class="btn-edit">Edit</a>
							</div>
						</div>    																												
						<div class="col last">
							<div class="title-box">
								<h2>Shipping <span>address</span></h2>
							</div>
							<div class="content-box">
								<div class="holder">								
                        	<? 
                        		$shipping_addresses = Customers::getPrimaryShippingAddress($_SESSION['cust_acc_id']);
			        			if (is_array($shipping_addresses))
	    		    			{
	           						$shipping   = $shipping_addresses[0];
                        		?>
									<span class="name"><?=$shipping['first_name']." ".$shipping['last_name']?></span>
									<address>
									<? if ($shipping['company']) {?><?=$shipping['company']?><br /><? } ?>
									<?=$shipping['address1']?><br />
									<? if ($shipping['address2']) {?><?=$shipping['address2']?><br /><? } ?>
									<?= $shipping['city'] . ", " . $shipping['state'] . " " . $shipping['zip'] ?><br /> 
									<? if ($shipping['location_type']) {?><?=$shipping['location_type']?><br /><? } ?>							
									<? if ($shipping['phone']) {?><?=FixPhone::displayPhone($shipping['phone'])?><? } ?>
									</address>
								<?}	?>
								</div>
								<a href="<?=$_SERVER['PHP_SELF']. "?action=change_shipping"?>" class="btn-edit">Edit</a>
							</div>							
						</div>
					</div>					
		    
		    <? 	//MyAccount::show_wishlist_items();
        		MyAccount::show_product_reviews();
				//MyAccount::show_user_subscription();
				?></div> </div><? 
				return true;
    }

    static function show_wishlist_items()
    {
        global $CFG;
?>
	<a name="wishlist"></a>
		<div class="title-box">
			<h2>Items on your wishlist</h2>
		</div>
		<div class="content-box">
<? 
        $wishlist_items = WishList::get_items_for_customer($_SESSION['cust_acc_id']);
        if (is_array($wishlist_items))
        {
?>					
			<ul class="wishlist">
			<? foreach ($wishlist_items as $wishlist_item)
               {
                	$product = Products::get1($wishlist_item['product_id']);

                	$name = $product['name'];
					$name = preg_replace('/[^A-z0-9]/s','-',$name);
					//$path = '//itempics.tigerchef.com/' . $product['img_thumb_url'];
					$path = $CFG->itempics_server . '/' . $product['img_thumb_url'];
					$path_on_server = $CFG->dirroot . '/itempics/' . $product['img_thumb_url'];
           			if (!file_exists($path_on_server) || !$product['img_thumb_url']) $path =$CFG->sslurl . 'images/'.$CFG->no_image_thumbnail_name;
           					
                ?>
							<li>
								<div class="product-info">
								<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?><a href="<?=Catalog::makeProductLink_($product)?>"> <? } ?>
									<img class="alignleft" src="<?=$path?>" width="74" height="74" alt="<?=$product['name']?>" />
									<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a><? } ?>
									<div class="info-holder">
									<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?><a href="<?=Catalog::makeProductLink_($product)?>"> <? } ?>									
										<h3><?=$product['name']?></h3>
										<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a><? } ?>
										<span>Item #(<?=$product['mfr_part_num']?>)</span>
									</div>
								</div>
								<strong class="price">$<?=number_format($product['price'], 2)?></strong>
								<div class="btn-box">
									<a href="javascript:void(0);"
										onclick="if (confirm('Are you sure you want to remove this item from your wishlist?')) { remove_from_wishlist(<?=$wishlist_item['id']?>, 0, 0, 1);}" class="btn-remove">Remove</a>
								</div>
							</li>
			<? } ?>
			</ul>
		<? } 
		else 
		{ ?>
			Your wishlist is empty. You may add any product	to your wishlist by clicking the 
			'Save to My List' link on a product	page.
		<? }
        ?>  
		</div>
<?         return true;
    }

    static function show_product_reviews()
    {
    	global $CFG;
    	
            /* determine which column we are sorting on */
            $column     = 'product_reviews.ts';
            $order_by   = 'DESC';

            switch ($_REQUEST['review_selection'])
            {
                case "1"    :   $column = 'product_reviews.ts';
                                break;

                case "2"    :   $column = 'product_reviews.title';
                                $order_by = 'ASC';
                                break;

                case "3"    :   $column = 'product_reviews.title';
                                $order_by = 'DESC';
                                break;

                default     :   break;
            }

            $reviews = Product_Reviews::get(0,0, $_SESSION['cust_acc_id'], '', $column, $order_by);
            $product = null;
            ?>
<form id="reviews_selection_frm" action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="reviews-form">
							<fieldset>
							<a name="product_reviews"></a>
							<div class="title-box" id="review-section">
								<h2>Your product reviews</h2>
	<!-- 	 <select name="review_selection" onchange="$('reviews_selection_frm').submit();">
				<option value="1"
					<?=($_REQUEST['review_selection'] == "1") ? 'selected' : ''?>>Most
					recent reviews</option>
				<option value="2"
					<?=($_REQUEST['review_selection'] == "2") ? 'selected' : ''?>>Title
					(asc)</option>
				<option value="3"
					<?=($_REQUEST['review_selection'] == "3") ? 'selected' : ''?>>Title
					(desc)</option>
			</select>-->
                </div>
</form>
<?
            if ($reviews)
            {
            ?>
<form id="review_edit_frm" action="/product_review.php" method="get">
	<input type="hidden" id="action" name="action" value="edit"> 
	<input type="hidden" id="product_id" name="product_id" value="" /> 
	<input type="hidden" id="review_id" name="review_id" value="" />
		<div class="content-box">
		<ul class="reviews-list">         
         <?
         	foreach ($reviews as $review)
           	{
           	    $product = Products::get1($review['product_id']);
           	    $path = $CFG->itempics_server . '/' . $product['img_thumb_url'];
				$path_on_server = $CFG->dirroot . '/itempics/' . $product['img_thumb_url'];
       			if (!file_exists($path_on_server) || !$product['img_thumb_url']) $path =$CFG->sslurl . 'images/'. $CFG->no_image_thumbnail_name;
           	?>
    <li>
			<div class="review-holder">
			<time datetime="<?=strftime("%m/%d/%Y", strtotime($review['ts']))?>"><?=strftime("%m/%d/%Y", strtotime($review['ts']))?></time>
			<h3>
			<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?>
		<a href="<?=Catalog::makeProductLink_($product)?>"><? } ?>
	<!-- 	<img src="<?=$path?>" alt="<?=$product['name'] ?>" border="0">-->
		<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a><? } ?>													
		<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?><a	href="<?=Catalog::makeProductLink_($product)?>"><? } ?>
		<?=$product['name']?>
		<? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a> - <? } ?>											
											
<span class="approval"><?php if($review['approved']=='Y') echo 'Approved';
 else echo ($review['approved']=='N')? 'Not approved' : 'Pending approval'; ?>	</span>										
							</div>
							<div class="buttons">
							<a onclick="$('product_id').value=<?=$review['product_id']?>;$('review_id').value=<?=$review['id']?>; $('review_edit_frm').submit(); return false;" href="javascript:void(0);">
							<button class="button-brown">Edit</button>
							</a>
							<a onclick="if (confirm('Are you sure you want to delete this review?')){ $('action').value='fastdelete';$('product_id').value=<?=$review['product_id']?>;$('review_id').value=<?=$review['id']?>; $('review_edit_frm').submit(); return false;}" href="javascript:void(0);">
							<button class="button-brown">Delete</button>
							</a>
							</div>
					</li>		               	
                <?
            	}
            	?>
				</ul>
			</div>
</form>
<?
            } else { ?>
            <div class="content-box">
            <div class="holder"><p>
You have not written any reviews. You may submit a
	review or rating on any product page.
			</p></div>
	</div>
<? } ?>
</fieldset>
       <?  return true;
    }

 static function show_user_subscription()
    {
		?>
		 <a name="subscribe"></a>
		 <div class="title-box"><h2>You subscribed to the following products</h2>
	   
     </div>
    <? $subs = Product_Subscribers::get('',$_SESSION['cust_acc_id']);
		   if ($subs){?>
			 <div class="acourt_text_box">
	     <table><tr><th colspan="2">Product </th><th> Action</th></tr>
	     <?foreach ($subs as $sub){?>
			 <?$product = Products::get1($sub['product_id'])?>
			 <td><span class="acourt_name">
			 <? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?><a href="<?=Catalog::makeProductLink_($product)?>"><? } ?>
			 <img src="<?=$CFG->itempics_server . '/' . $product['img_thumb_url']?>" alt="" border="0">
			 <? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a><? } ?>
			 </td>
			 <td>
			 <? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?><a	href="<?=Catalog::makeProductLink_($product)?>"><? } ?>
			 <?=$product['name']?>
			 <? if ($product['is_active'] == 'Y' && $product['is_deleted'] == 'N') {?></a><? } ?>
			 </span></td>
			 <td><input id="unsubscribe" type="button" value="Unsubscribe" onclick="productUnsubscirbe(<?=$product['id']?>);return false;">
			 <div id="unsubscribe<?=$sub['product_id']?>"></div></td></tr> 
 
				<? }?>
			 </table></div>	 
       <? }
		  else {?>
		 <div class="content-box">
		 	<div class="holder"><p>You have no active subscriptions.</p></div>
		 </div>
		 <? }?>
     
	  <?  return true;
    }

    static function editInfo($info = '', $msg = '', $upgrade_msg = false)
    {?>
    	<div class="account-box main-section">						
      
      <?php   /* load the customer data from db if info has nothing in it */
        if (!$info)
        {
            $info = Customers::get1($_SESSION['cust_acc_id']);
        }

        if ($info['account_system'] == "old")
        {
        	echo "<div class='upgrade_account'><h2>Become a TigerChef!</h2>Check out the benefits <a href='become_a_tigerchef.php' target=_blank>here</a>.<br/>Please update your account before proceeding!</div>";	
        }	
				
        if($info['require_password_reset'] == 'Y'){
	    	echo '<div class="niceMessage">Your password was reset to a temporary password during the password recovery process. Please choose a new password.</div>';
		}
	?>
   <form method="post" id="frm_update_account"
	action="<?=$_SERVER['PHP_SELF']?>#account_settings" enctype="multipart/form-data">
	<input type="hidden" name="action" value="update_account">
	<?  if ($msg)
	   	{
	   	?>
		<span class="account-error"><?=$msg;?></span>
		<?
	    }
	    ?>
	
	    <div class="cart_box">
		<h3>
			 required fields<span class="error-img">*</span> 
		</h3>
		<div class="form_box">
			<ul class="side-a">
				<li><label>First Name:</label><input class="input_box"
					name="info[first_name]" value="<?=$info['first_name']?>"
					type="text" /> <img src="images/error_img.png" alt="" /></li>
				<li><label>Last Name:</label><input class="input_box"
					name="info[last_name]" value="<?=$info['last_name']?>" type="text" />
					<img src="images/error_img.png" alt="" /></li>
				<li><label>Email:</label><input class="input_box" name="info[email]"
					value="<?=$info['email']?>" type="text" /> <img
					src="images/error_img.png" alt="" /></li>
	
				<li><label>Type of Company or Institution:</label> <select class="input_box"
					name="info[institution_type_id]">
						<option value=''></option>
                    <? 
            $type_of_company_array = MyAccount::getAllCustomerInstitutionTypes(true);
            
            foreach ($type_of_company_array as $the_type)
            {
            	echo "<option value='" . $the_type['id'] . "'";
            	if ($info['institution_type_id'] == $the_type['id']) echo " selected";
            	echo ">" . $the_type['caption'] . "</option>";
            }
                    ?>
                    </select> <img src="images/error_img.png" alt="" /></li>
				<li><label>Name of Company or Institution:</label><input
					class="input_box" name="info[institution_name]" type="text"
					value="<?=$info['institution_name']?>" /></li>
			<? $role_array = MyAccount::getAllCustomerRoles(true);
				if ($role_array) {?>
				<li><label>Role:</label> <select class="input_box" name="info[role_id]">
						<option value=''></option>
                    <? 
            
            foreach ($role_array as $the_role)
            {
            	echo "<option value='" . $the_role['id'] . "'";
            	if ($info['role_id'] == $the_role['id']) echo " selected";
            	echo ">" . $the_role['caption'] . "</option>";
            }
                    ?>                                                               
                    </select> <img src="images/error_img.png" alt="" />		
			</li>
			<?}?>
			</ul>
		</div>
		<div class="form_box">
			<ul class="side-b">			
	      <li><label>Add/Edit Photo:<Br/></label>				
        <input type="file" value="<?=$info['img_url']?>" name="img_url" id="file"></li>				
	
				<li><label>Password:</label><input class="input_box"
					name="info[password]" type="password" /> <img
					src="images/error_img.png" alt="" /></li>
				<li><label>Confirm Password:</label><input class="input_box"
					name="info[password_conf]" type="password" /> <img
					src="images/error_img.png" alt="" /></li>
					<li><label>EIN Number:</label><input class="input_box"
					name="info[ein_number]" id="info[ein_number]" type="text" value="<?=$info['ein_number']?>"/>
					<br/><p class='ein_explanation'>If you are a member of a purchasing group, please enter your EIN number.</p>
					</li>
					<li>
					<div id="accountimag">		
				  <?php if ($info['img_url']){?>
					<label>Your current photo:</label>
					<br/>Click <a href="#" onclick="removeimage('<?=$info['img_url']?>');return false;" id="removeimage">here</a> to remove your image
					
					<img src="upload/<?=$info['img_url']?>" height="150">
					<?php }?>
					</div>
					<div id="imageremoved"></div>
					</li>
			</ul>
		</div>
	</div>
	<div class="cart_box">
		<div class="box">
			<input class="sign_in button-brown" value="Update Account"
				name="update_account_button" type="button"
				onclick="$('frm_update_account').submit();" /> 
		</div>
	</div>
 </form>
</div>
<?
        return true;
    }

    static function newAccount($info = '', $msg = '', $info2 = '', $overlay = false,$link)
    {
    	global $CFG;
    	?>
    	    <form method="post" id="frm_new_account" action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
    		<input type="hidden" name="action" value="create_account"> 
    		<input type="hidden" name="hours_of_operation" value="">
    		<input type="hidden" name="link" value="<?=$link?>">
    	   <?
    			if ($msg)
    	    	{
    	    	?>
    			<span class="account-error"><?=$msg;?></span>
    			<?
    	    	}
    	    	?>
        <div class="form-box fieldset">
            <input name="success_url" value="" type="hidden">
            <input name="error_url" value="" type="hidden">
            <h2 class="legend">Personal Information</h2>
            <ul class="form-list">
                <li class="fields">
                    
                    <label>Name:<em>*</em></label> 
					       <input class="input_box" name="info[first_name]" id="info[first_name]" type="text" value="<?=$info['first_name']?>"/>
                </li>
                <li>
                    <label>Email Address:<em>*</em></label> 
					<input class="input_box" name="info[email]" id="info[email]" type="email" value="<?=$info['email']?>"/>
				<!-- <li><label>&nbsp;</label><input type='checkbox'
					name="info[receive_email_specials]" value='Y'
					<?=(!$info || $info['receive_email_specials'] == 'Y')? "checked" : ""?>><span
					>&nbsp;Receive updates via email</span></li> -->
                </li>
                                                                                                    </ul>
        </div>
        <div class="form-box fieldset">
            <h2 class="legend">Login Information</h2>
            <ul class="form-list">
                <li class="fields">
                    <label>Password:<em>*</em></label>
					<input class="input_box" name="info[password]" id="info[password]" type="password" />
				    
                    <label>Confirm Password:<em>*</em></label>
					<input class="input_box" name="info[password_conf]" id="info[password_conf]" type="password"  onkeypress="if (event.keyCode == 13) {document.getElementById('popup_sign_in_btn').click();return false;} else return true;" />
                </li>
              </ul>
      
        </div>
        <div class="buttons-set">
            <div class="cart_box" >
		    		<div class="box" >
		    		 <a href="/account.php" class="back-link"><< Back</a>
		    		    <? if($overlay){ ?>
		    			<input class="sign_in button-brown" 
		    				value="Submit" name="create_account_button" type="button"
		    				onclick="validate_account_frm()" />
		    			<? } else { ?>
		    			<input class="sign_in button-brown" 
		    				value="Submit" name="create_account_button" type="button" 
		    				onclick="$j('#frm_new_account').submit();" />
		    			<? } ?>
		    		</div>
		    <p class="required">* Required Fields</p>	
		    </div>	
        </div>
       </form>
    
    <?	 return true;
    }
    
    static function newAccountOld($info = '', $msg = '', $info2 = '', $overlay = false,$link)  
    {
    	global $CFG;
	?>
	    <form method="post" id="frm_new_account" action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
		<input type="hidden" name="action" value="create_account"> 
		<input type="hidden" name="hours_of_operation" value="">
		<input type="hidden" name="link" value="<?=$link?>">
	   <?
			if ($msg)
	    	{
	    	?>
			<span class="account-error"><?=$msg;?></span>
			<?
	    	}
	    	?>
	   

	    <div class="cart_box">
	    <h1>Personal Information</h1>
		<h3>
		 required fields<span class="error-img">*</span>
		</h3>
		<div class="top"></div>
		<div class="form_box">
			<ul class="side-a">
				<li><label>First Name: <span class="error-img">*</span></label><input class="input_box"
					name="info[first_name]" id="info[first_name]" value="<?=$info['first_name']?>"
					type="text" /></li>
				<li><label>Last Name:<span class="error-img">*</span></label><input class="input_box"
					name="info[last_name]" id="info[last_name]" value="<?=$info['last_name']?>" type="text" />
					</li>
			</ul>
			<ul class="side-b">
				<li><label>Type of Company or Institution:<span class="error-img">*</span></label> <select class="input_box"
					name="info[institution_type_id]" id="info[institution_type_id]">
						<option value=''>--Choose Company Type--</option>
                    <? 
            $type_of_company_array = MyAccount::getAllCustomerInstitutionTypes(true);
            
            foreach ($type_of_company_array as $the_type)
            {
            	echo "<option value='" . $the_type['id'] . "'";
            	if ($info['institution_type_id'] == $the_type['id']) echo " selected";
            	echo ">" . $the_type['caption'] . "</option>";
            }
                    ?>
                    </select> </li>
				<li><label>Name of Company or Institution:</label><input
					class="input_box" name="info[institution_name]" type="text"
					value="<?=$info['institution_name']?>" /></li>
				<li><label>Role:<span class="error-img">*</span></label> <select class="input_box" name="info[role_id]" id="info[role_id]">
						<option value=''>--Choose Role--</option>
                    <? 
            $role_array = MyAccount::getAllCustomerRoles(true);
            
            foreach ($role_array as $the_role)
            {
            	echo "<option value='" . $the_role['id'] . "'";
            	if ($info['role_id'] == $the_role['id']) echo " selected";
            	echo ">" . $the_role['caption'] . "</option>";
            }
                    ?>                                                               
                </select> 
					
			</li>				
	<? if(!$overlay){ ?>				    
	      <li><label>Add Your Photo:</label>
        <input type="file" value="<?=$info['img_url']?>" name="img_url" id="file"></li>
			<? } ?>
			</ul>	
			<div style="clear:both;"></div>			
				<h1>Billing Address:</h1>
				<div class="top"></div>

			<ul class="side-a">
				<li><label>Company:</label><input class="input_box"
					name="info[company]" value="<?=$info['company']?>" type="text" /></li>
				<li><label>Address:<span class="error-img">*</span></label><input class="input_box"
					name="info[address1]" id="info[address1]" value="<?=$info['address1']?>" type="text" />
					</li>
				<li><label>&nbsp;</label><input class="input_box"
					name="info[address2]" value="<?=$info['address2']?>" type="text" /></li>
				<li><label>City:<span class="error-img">*</span></label><input class="input_box" name="info[city]" id="info[city]"
					value="<?=$info['city']?>" type="text" />
					</li>
				<li><label>State:<span class="error-img">*</span></label><?=stateSelect ("info[state]", $info['state'],'',false,'Please Select:','','','','','','input_box','','','',"info[state]",'')?>
			</li>
				<li><label>Zip:<span class="error-img">*</span></label><input class="input_box" name="info[zip]" id="info[zip]"
					value="<?=$info['zip']?>" type="text" />
					</li>
				<li><label>Location:<span class="error-img">*</span></label><span class="location" >
						<input class="input_button"
						name="info[location_type]"  id="info[location_type_commercial]" type="radio" value="Commercial"
						<?=(($info['location_type'] == 'Commercial') ? "checked" : "")?> /><span id='location_commercial'
						>Business</span>
						<input class="input_button"
						name="info[location_type]"  id="info[location_type_residential]" type="radio" value="Residential"
						<?=(($info['location_type'] == 'Residential') ? "checked" : "")?> />
						<span  id='location_residential'>Residential</span>
						</span>
						</li>
						</ul>
						<ul class="side-b">
				<li><label>Phone:<span class="error-img">*</span></label><input class="input_box" name="info[phone]" id="info[phone]"
					value="<?=$info['phone']?>" type="text" />
</li>
				<li><label>Email:<span class="error-img">*</span></label><input class="input_box" name="info[email]" id="info[email]"
					value="<?=$info['email']?>" type="text" />
					</li>
				<!-- <li><label>&nbsp;</label><input type='checkbox'
					name="info[receive_email_specials]" value='Y'
					<?=(!$info || $info['receive_email_specials'] == 'Y')? "checked" : ""?>><span
					>&nbsp;Receive updates via email</span></li>-->
		
	
				<li><label>Password:<span class="error-img">*</span></label><input class="input_box"
					name="info[password]" id="info[password]" type="password" /> 
					</li>
				<li><label>Confirm Password:<span class="error-img">*</span></label><input class="input_box"
					name="info[password_conf]" id="info[password_conf]" type="password" /> 
</li>
				<li><label>EIN Number:</label><input class="input_box"
					name="info[ein_number]" id="info[ein_number]" type="text" value="<?=$info['ein_number']?>"/>
					<p class='ein_explanation'>If you are a member of a purchasing group, please enter your EIN number.</p>
					</li>			
			</ul>
			<div style="clear:both">

				<h1>Shipping Address:</h1>
				<div class="same-as-billing"><input type='checkbox'
					name='same_as_billing' id='same_as_billing' value='Y'
					onClick='toggleShippingAddrDiv(this.checked);'
					<?=(!$_POST || $_POST['same_as_billing'] == 'Y')? "checked" : ""?>>&nbsp;Same
				as Billing</div>
			<span id="shipping_addr_section" class="shipping_addr_section"
				<?=(!$_POST || $_POST['same_as_billing'] == 'Y')? ' style="display:none"' : ""?>>
				<div class="top"></div>
				<ul class="side-a">
				
					<li><label>Company:</label><input class="input_box"
						name="info2[company]" value="<?=$info2['company']?>" type="text" /></li>
					<li><label>Address:<span class="error-img">*</span></label><input class="input_box"
						name="info2[address1]" id="info2[address1]" value="<?=$info2['address1']?>" type="text" />
</li>
					<li><label>&nbsp;</label><input class="input_box"
						name="info2[address2]" value="<?=$info2['address2']?>" type="text" /></li>
					<li><label>City:<span class="error-img">*</span></label><input class="input_box" name="info2[city]" id="info2[city]"
						value="<?=$info2['city']?>" type="text" />
</li></ul>
<ul class="side-b">
					<li><label>State:<span class="error-img">*</span></label><?=stateSelect("info2[state]", $info2['state'], '',false,'Please Select:','','','','','','input_box','','','',"info2[state]",'')?>
</li>
					<li><label>Zip:<span class="error-img">*</span></label><input class="input_box" name="info2[zip]" id="info2[zip]"
						value="<?=$info2['zip']?>" type="text" />
</li>
					<li><label>Location:<span class="error-img">*</span></label><span class="location">
							<input
							name="info2[location_type]" id="info2[location_type_commercial]" type="radio" value="Commercial"
							<?=(($info2['location_type'] == 'Commercial') ? "checked" : "")?> /><span id='location_commercial2'
							>Business</span></span>
							<input
							name="info2[location_type]" id="info2[location_type_residential]" type="radio" value="Residential"
							<?=(($info2['location_type'] == 'Residential') ? "checked" : "")?> /><span id='location_residential2'
							>Residential</span>
</li>


					<li><label>Phone:<span class="error-img">*</span></label><input class="input_box"
						name="info2[phone]" id="info2[phone]" value="<?=$info2['phone']?>" type="text" /></li>
				</ul>
				<div style="clear:both;"></div>
			</span>
		</div>
	</div>
	<div class="cart_box" >
		<div class="box" >
		    <? if($overlay){ ?>
			<input class="sign_in" 
				value="Create Account" name="create_account_button" type="button"
				onclick="validate_account_frm()" />
			<? } else { ?>
			<input class="sign_in" 
				value="Create Account" name="create_account_button" type="button"
				onclick="$('frm_new_account').submit();" />
			<? } ?>
		</div>
	</div>	
</form>
</div>
<?	 return true;
	}

	static function showLoginForm($msg = '', $email = '', $create_button = false)
	{
	    global $CFG;    
	    ?>
		
		<div class="col2-set account-login">
	       <div class="col-1">
		     <div class="content">
                    <h2>New Customers</h2>
                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
             </div>
                <div class="buttons-set">
                    <a id="new-account" href="<?=$_SERVER['PHP_SELF']. "?action=new_user"?>"><button type="button" title="Create an Account" class="button-brown">Create an Account</button></a>
                </div>
		  </div>	 
		
		<div class="col-2">
           <div class="content">
           <h2>Registered Customers</h2>
            <p>If you have an account with us, please log in.</p>
            <form name="loginform" id="frm_login" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	        <input type="hidden" name="action" value="login" /> <input
		     type="hidden" name="redirect" value="<?=$_REQUEST['redirect']?>" />
	        <?
			if ($msg)
	    	{
	    	?>
			<span class="account-error"><?=$msg;?></span>
			<?
	    	}
	    	?>
	    
	    
		<div class="form-list">
		    <ul>
			<li><label class="required">Email Address<em>*</em></label><input class="input_box" name="log" type="email" /></li>
			<li><label class="required">Password<em>*</em></label><input class="input_box" name="pwd"
				type="password" /> 

			</li>
            </ul>			
			<p class="required">* Required Fields</p>
		</div>
		</div>
                <div class="buttons-set">
                    <a id="forgot-password" href="<?=$_SERVER['PHP_SELF']."?action=forgot_password"?>">Forgot Your Password?</a>
                    <button type="submit" class="button-brown" title="Login" name="sign_in" onclick="$('frm_login').submit();">Login</button>
               </div>
		</div>


<?
        return true;
	}
	
	static function recoverPassword($email){

		$error = '';
		global $CFG;

		if($email){
			if(!validate_email($email)){
				$error .= "Invalid e-mail address format. <br />";
			}
		
			$cust = db_get1( Customers::get('','','','','','',$email) );
		
			if(!$error){
				if($cust){
					//reset password randomly
					$chars = "23456789ABCDEFGHJKLMNPQRSTUVWXTZabcdefghkmnpqrstuvwxyz";
					$size = strlen( $chars );
					$temp_password = "";
					$length = 7;
					for( $i = 0; $i < $length; $i++ ) {
						$temp_password .= $chars[ rand( 0, $size - 1 ) ];
					}
					//update customer table
					Customers::update($cust['id'],array('password' => $temp_password, 'require_password_reset' => 'Y'));
		
					$datum['body'] = "Dear Customer, <br />	<br />
					Your temporary password is $temp_password<br /><br />
		
					Please log in at ".$CFG->baseurl."account.php?action=login_screen to change the temporary password.
					<br/><br/>If you did not invoke password recovery, please call us at ".$CFG->company_phone .  " or visit ".$CFG->baseurl."contact_us.php
					<br/><br/>Thank you,<br/><br/>
					".$CFG->company_name." Customer Service";
					TigerEmail::sendOne("$cust[first_name] $cust[last_name]",$cust['email'],'blank',$datum,"The {$CFG->company_name} information you requested.");
							$success = true;
							$message = "We have just sent an e-mail with the remaining recovery instructions to ".$email.". Thank you.";
				} else {
							$message .= "We do not have an account on file for " . $email . "<br />";
									$success = false;
							}
			}
		}
		return array('email'=>$email,'success'=>$success,'message'=>$message,'error'=>$error);
	}

	static function forgotPassword($email = '',$popup=false)
	{
		global $CFG;		
		$info = self::recoverPassword($email);
        $error = $info['error'];
		$message = $info['message'];
		$success = $info['success'];	                                    
        ?>
        <form name="forgotform" id="frm_login" action="<?=$_SERVER['PHP_SELF']?>" method="post" class="login_box">
	    <input type="hidden" name="action" value="recover_password" /> 
	    <input type="hidden" name="redirect" value="<?=$_REQUEST['redirect']?>" />
        <?
	    	if ($error) 
	    	{
	    	?>
	    		<div class="errorBox"><?=$error?></div>
	    	<?
	    	} else if($message){
	    	?>
	    		<div class="errorBox"><?=$message?></div>
	    	
	    	<?
	    	}
	    if($popup){
	    	$onclick = 'password_recovery();return false;';
	    }
	    ?>
        
        <div class="fieldset">
        <h2 class="legend">Retrieve your password here</h2>
        <p id="message">Please enter your email address below. You will receive a link to reset your password.</p>
        <ul class="form-list ">
            <li>
                <label for="email_address" class="required"><em>*</em>Email Address</label>
                <div class="input-box">
                    <input name="email" alt="email" id="recovery_email" class="input-box required-entry validate-email" value="" type="text">
                </div>
            </li>
        </ul>
    </div>
    <div class="buttons-set">
        <p class="required">* Required Fields</p>
                 <button class="button-brown" form="frm_login" value="Submit" type="submit" tabindex="1" onclick="<?=$onclick?>" />Submit</button> 
				<?php 
				if(!$popup){
				?>
					<a href="#" onclick="window.location.href='<?=$_SERVER['PHP_SELF']?>'">« Back to Login</a>
				<?php 
				}
				?>

    </div>
</form>
        
<? 		
        return true;
	}

    static function showBillingAddressForm($info = '', $msg = '')
	{
	    $billing           = null;
	    $billing_addresses = array();
	    $address_id        = '';
			
			/* fetch the primary billing address */
		if (! $info) {
			$billing_addresses = Customers::getPrimaryBillingAddress($_SESSION['cust_acc_id']);

	        if (is_array($billing_addresses))
	        {
	           $billing        = $billing_addresses[0];
	           $address_id     = $billing['id'];

	        }
	    }
	    else
	    {
	        $address_id = $_REQUEST['address_id'];
	        $billing    = $info;
	    }

        ?>
		<div class="account-box main-section">
        <form method="post" id="frm_billing_address"
	action="<?=$_SERVER['PHP_SELF']?>#account_settings">
	<input type="hidden" name="action" value="update_billing"> <input
		type="hidden" name="info[billing]" value="Y"> <input type="hidden"
		name="address_id" value="<?=$address_id?>">
	    <?
	    if ($msg)
	    {
	    	?>
			<span class="account-error"><?=$msg;?></span>
		<?
	    }
	    ?>
	    

	    <div class="cart_box">
		<div class="form_box">
			<h3>Required Fields
				<img src="images/error_img.png" alt="" />
			</h3>
			<ul>
				<li><label>First Name:</label><input class="input_box"
					name="info[first_name]" value="<?=$billing['first_name']?>"
					type="text" /><img src="images/error_img.png" alt="" /></li>
				<li><label>Last Name:</label><input class="input_box"
					name="info[last_name]" value="<?=$billing['last_name']?>"
					type="text" /><img src="images/error_img.png" alt="" /></li>
				<li><label>Company:</label><input class="input_box"
					name="info[company]" value="<?=$billing['company']?>" type="text" /></li>
				<li><label>Address:</label><input class="input_box"
					name="info[address1]" value="<?=$billing['address1']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
				<li><label>&nbsp;</label><input class="input_box"
					name="info[address2]" value="<?=$billing['address2']?>" type="text" /></li>
				<li><label>City:</label><input class="input_box" name="info[city]"
					value="<?=$billing['city']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
				<li><label>State:</label><?=stateSelect("info[state]", $billing['state'], '',false,'Please Select:','','','','','','input_box','','','','','')?> <img
					src="images/error_img.png" alt="" /></li>
				<li><label>Zip:</label><input class="input_box" name="info[zip]"
					value="<?=$billing['zip']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
				<li><label>Location:</label><input name="info[location_type]"
					type="radio" value="Residential"
					<?=(($billing['location_type'] == 'Residential') ? "checked" : "")?> /><span
					>Residential</span><input
					name="info[location_type]" type="radio" value="Commercial"
					<?=(($billing['location_type'] == 'Commercial') ? "checked" : "")?> /><span
					>Commercial</span><img
					src="images/error_img.png" alt="" /></li>
				<li><label>Home Phone:</label><input class="input_box"
					name="info[phone]" value="<?=$billing['phone']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
			</ul>
		</div>
	</div>

	<div class="cart_box">
		<div class="box">
			<input class="sign_in button-brown" value="Update Address"
				name="update_billing_button" type="button"
				onclick="$('frm_billing_address').submit();" />
				<input
				class="sign_in button-brown" style="margin-right:8px;" value="Cancel"
				name="cancel_button" type="button"
				onclick="$('frm_billing_address').action.value='cancel'; $('frm_billing_address').submit();" />
		</div>
	</div>
</form>
</div>
<?

        return true;
	}

	static function showTrackPackages(){
		global $CFG, $msg;

		$orders = Orders::getTracking('','',$_SESSION['cust_acc_id']);

		if ($msg)
	    {
	    	?>
			<span class="account-error"><?=$msg;?></span>
		<?
	    }
	    ?>
		
<p>Click on the tracking number to view tracking information on the
	shipper's website.</p>
<table id="hor-minimalist-a" class="tracking">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Order Number</th>
			<th scope="col">No. Items</th>
			<th scope="col">Placed On</th>
			<th scope="col">Shipper</th>
			<th scope="col">Tracking No.</th>
		</tr>
	</thead>

	<tbody>

		    <?
		    if (is_array($orders))
		    {
		        foreach ($orders as $idx => $order)
		        {
		        ?>
		            <tr>
			<td><?=$idx + 1 ?></td>
			<td><a href="<?= $_SERVER['PHP_SELF']?>."
				?action=print_invoice&order_id=<?=$order['order_id']?>"><?= $CFG->order_prefix.$order['order_id'] ?></a></td>
			<td><?= $order['num_units'] ?></td>
			<td><?= date('m-d-Y h:ia',strtotime($order['date'])) ?></td>
			<td><?=$order['shipper']?></td>
			<td><a
				href="<?=str_replace("[tracking_number]",$order['tracking_number'],$order['tracking_url'])?>"
				target="_blank" class="tracking_link"><?=$order['tracking_number']?></a></td>
		</tr>
		        <?
		        }
		    }
		    else
		    {
		    ?>
		    <tr>
			<td colspan="3">You have no recent orders. Click View All Order
				History to view all orders.</td>
			</tr>
		    <?
		    }
		    ?>

		    </tbody>
</table>
<?php
	}

	static function showShippingAddressForm($info = '', $msg = '')
	{
	    $shipping              = null;
	    $shipping_addresses    = array();
	    $address_id            = '';

	    /* fetch the primary shipping address */
	    if (!$info)
	    {
	        $shipping_addresses    = Customers::getPrimaryShippingAddress($_SESSION['cust_acc_id']);

	        if (is_array($shipping_addresses))
	        {
	           $shipping   = $shipping_addresses[0];
	           $address_id = $shipping['id'];
	        }
	    }
	    else
	    {
	        $address_id = $_REQUEST['address_id'];
	        $shipping    = $info;
	    }
        
        ?>
<div class="account-box main-section">
<form method="post" id="frm_shipping_address"
	action="<?=$_SERVER['PHP_SELF']?>#account_settings">
	<input type="hidden" name="action" value="update_shipping"> <input
		type="hidden" name="info[billing]" value="N"> <input type="hidden"
		name="address_id" value="<?=$address_id?>">
	    <?
			if ($msg)
	    	{
	    	?>
			<span class="account-error"><?=$msg;?></span>
			<?
	    	}
	    	?>
	    

        <div class="cart_box">
		<div class="form_box">
			<h3>Required Fields
				<img src="images/error_img.png" alt="" />
			</h3>
			<ul>
				<li><label>First Name:</label><input class="input_box"
					name="info[first_name]" value="<?=$shipping['first_name']?>"
					type="text" /><img src="images/error_img.png" alt="" /></li>
				<li><label>Last Name:</label><input class="input_box"
					name="info[last_name]" value="<?=$shipping['last_name']?>"
					type="text" /><img src="images/error_img.png" alt="" /></li>
				<li><label>Company:</label><input class="input_box"
					name="info[company]" value="<?=$shipping['company']?>" type="text" /></li>
				<li><label>Address:</label><input class="input_box"
					name="info[address1]" value="<?=$shipping['address1']?>"
					type="text" /><img src="images/error_img.png" alt="" /></li>
				<li><label>&nbsp;</label><input class="input_box"
					name="info[address2]" value="<?=$shipping['address2']?>"
					type="text" /></li>
				<li><label>City:</label><input class="input_box" name="info[city]"
					value="<?=$shipping['city']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
				<li><label>State:</label><?=stateSelect("info[state]", $shipping['state'], '',false,'Please Select:','','','','','','input_box','','','','','')?> <img
					src="images/error_img.png" alt="" /></li>
				<li><label>Zip:</label><input class="input_box" name="info[zip]"
					value="<?=$shipping['zip']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
				<li><label>Location:</label><input name="info[location_type]"
					type="radio" value="Residential"
					<?=(($shipping['location_type'] == 'Residential') ? "checked" : "")?> /><span
					>Residential</span><input
					name="info[location_type]" type="radio" value="commercial"
					<?=(($shipping['location_type'] == 'Commercial') ? "checked" : "")?> /><span
					>Commercial</span><img
					src="images/error_img.png" alt="" /></li>
				<li><label>Home Phone:</label><input class="input_box"
					name="info[phone]" value="<?=$shipping['phone']?>" type="text" /><img
					src="images/error_img.png" alt="" /></li>
			</ul>
		</div>
	</div>
	<div class="cart_box">
		<div class="box">
			<input class="sign_in button-brown" value="Update Address"
				name="update_shipping_button" type="button"
				onclick="$('frm_shipping_address').submit();" />
				<input
				class="sign_in button-brown" value="Cancel" style="margin-right:8px;"
				name="cancel_button" type="button"
				onclick="$('frm_shipping_address').action.value='cancel'; $('frm_shipping_address').submit();" />
		</div>
	</div>
</form>
</div>
<?
        return true;
	}


	static function update_address($info, &$errors = '', $type = '')
    {
        /* validate information */
        $validation_fields  = array
        (
            'first_name'    => 'First Name',
            'last_name'     => 'Last Name',
            'address1'      => 'Address',
            'city'          => 'City',
            'state'         => 'State',
            'zip'           => 'Zip Code',
            'phone'         => 'Phone Number',
            'location_type' => 'Location',
	        );

        $num_errors         = 0;

        foreach ($validation_fields as $field => $field_name)
        {
            if ($info[$field] == '')
            {
                $errors .= sprintf("%s is missing.<br/>", $field_name);
                ++$num_errors;
            }
        }
								
        /* now determine if there were errors */
        if ($num_errors > 0)
        {
            return false;
        }
        else
        {
            /* update - this value is coming from the shipping and billing address form */
            if ($_REQUEST['address_id'])
            {
                Customers::updateAddress($_REQUEST['address_id'], $info);
            }

            /* insert */
            else
            {
                $info['primary_address'] = 'Y';
                Customers::addAddress($_SESSION['cust_acc_id'], $info);
            }

            /* just update the customer address fields in the customer table if we are updating the billing */
            if ($type == 'billing')
            {
                unset($info['billing']);
                unset($info['first_name']);
                unset($info['last_name']);
                unset($info['primary_address']);

                Customers::update($_SESSION['cust_acc_id'], $info);
            }
            return true;
        }
        return true;
    }

	static function updateInfo($info, &$errors = '')
	{
		global $CFG;
	    $customer      = null;
	    $existing      = null;
	    $num_errors    = 0;

	    /* fetch the customer */
		$customer = Customers::get1( $_SESSION['cust_acc_id'] );

		/* make sure that they are not trying to use an email already in use */
		if ($customer['email'] != $info['email'])
		{
            $existing = Customers::get(0,'','','','','',$info['email']);

		    if (is_array($existing) && count($existing) > 0)
		    {
			    $errors .= 'That email address is already in use.<br/>';
                ++$num_errors;
		    }
		}

if ($_FILES["img_url"]["name"]){
$allowedExts = array("jpg", "jpeg", "gif", "png");
$extension = end(explode(".", $_FILES["img_url"]["name"]));
if ((($_FILES["img_url"]["type"] == "image/gif")
|| ($_FILES["img_url"]["type"] == "image/jpeg")
|| ($_FILES["img_url"]["type"] == "image/png")
|| ($_FILES["img_url"]["type"] == "image/pjpeg"))
&& ($_FILES["img_url"]["size"]/1024 < 2000)
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["img_url"]["error"] > 0)
    {
    $errors .= "There is a problem with your uploaded file: " . $_FILES["img_url"]["error"] . "<br>";
		++$num_errors;
    }
  else
    {
    if (file_exists("upload/" . $_FILES["img_url"]["name"]))
      {
      $errors .= sprintf($_FILES["img_url"]["name"] . " already exists. Choose a file with a different name.<br/>");
			++$num_errors;
      }
     }
  }
else
  {
      $errors .= "Invalid file <br>";
      ++$num_errors; 
  }
}	
		/* Validate Passwords */
		if ($info['password'])
		{
    		if ($info['password'] != $info['password_conf'])
    		{
    			$errors .= 'Your passwords do not match.<br/>';
                ++$num_errors;
    		}

    		if (strlen($info['password']) < 6)
    		{
    			$errors .= 'Your password must be at least 6 characters long.<br/>';
                ++$num_errors;
    		}
		}
		else
		{
	        $errors .= 'Password is missing.<br/>';
		    ++$num_errors;
		}

		/* Validate first name and last name */
		if (!$info['first_name'])
		{
		    $errors .= "First name is missing.<br/>";
			++$num_errors;		    
		}

		if (!$info['last_name'])
		{
		    $errors .= "Last name is missing.<br/>";
			++$num_errors;
		}
		if (!$info['role_id'])
        {
            $errors .= 'Your Role is missing.<br/>';
            ++$num_errors;
        }
		if (!$info['institution_type_id'])
        {
            $errors .= 'Your Company or Institution Type is missing.<br/>';
            ++$num_errors;
        }
		// enforce that if choose student, must choose culinary school
		if ($info['role_id'] == $CFG->student_role_id && $info['institution_type_id']!= $CFG->culinary_school_id)		
		{
			$errors .= "You must choose \"Culinary School\" as your Insitution Type if you have chosen \"Student\" as your role";
			++$num_errors;			
		}
		
		if ($customer['dedicated_rep'] == "0")
		{
			if (time() % 3 == 0) $info['dedicated_rep'] = $CFG->dedicated_rep_1;
			else if (time() % 3 == 1) $info['dedicated_rep'] = $CFG->dedicated_rep_2;
			else $info['dedicated_rep'] = $CFG->dedicated_rep_3;
		}
		 			
		
		/* If there are errors, well, get out */
		if ($num_errors > 0)
		{
			return false;
		}
		else
		{
		  
			if ($_FILES["img_url"]["name"]){   /*give the img file a new name to avoid duplicate filenames*/
          $newname=time();              
	        $newname.=".".$extension;
	        $info['img_url']=$newname;		
          if ($_FILES["img_url"]["name"]){move_uploaded_file($_FILES["img_url"]["tmp_name"],"upload/".$newname);}
			}
			unset($info['password_conf']);
			$info['account_system'] = "new";
			$info['account_activated'] = "Y";
	
	if($customer['require_password_reset'] == 'Y'){
		$info['require_password_reset'] = 'N';
	    }
	
			/* update the customer db */
            Customers::update($_SESSION['cust_acc_id'], $info);

            /* reset the persons name in case it was changed */
            $_SESSION['cust_name'] = $info['first_name'] . ' ' . $info['last_name'];

			return true;
		}

      return true;
	}

	static function createAccount(&$info, &$errors = '', $info2=array(),&$err_msgs='',$validate_all=true)
	{
		global $CFG;
	
		$errors     = '';
		$num_errors = 0;
	
		/* validate information */
		$validation_fields  = array
		(
				'first_name'    => 'First Name',
		);
		
		$num_errors         = 0;
		
		foreach ($validation_fields as $field => $field_name)
		{
			if ($info[$field] == '')
			{
				$msg = sprintf("%s is missing.", $field_name);
				$errors .= $msg . '</br>';
				$err_msgs[] =  array('field'=>$field,'msg'=>$msg);
				++$num_errors;
			}
		}
		
		/* Validate Email */
		if ($info['email'])
		{
			$existing = Customers::get(0,'','','','','',$info['email']);
	
			if(is_array($existing) && count($existing) > 0)
			{
				$msg = 'That email address is already in use.';
				$errors .= $msg . "<br/>";
				$err_msgs[] =  array('field'=>'email','msg'=>$msg);
				++$num_errors;
			}
			if (validate_email($info['email'],true)=== false || strstr($info['email'], "adulttoys") !== false || strstr($info['email'], "feeladult.com") !== false ||
			strstr($info['email'], "sex.com") !== false)
			{
				$msg = 'Your email is invalid.';
				$errors .= $msg . "<br/>";
				$err_msgs[] =  array('field'=>'email','msg'=>$msg);
				++$num_errors;
			}
		}
		else
		{
			$msg = 'Your email is missing.';
			$errors .= $msg . "<br/>";
			$err_msgs[] =  array('field'=>'email','msg'=>$msg);
			++$num_errors;
		}
	
		/* Validate Passwords */
		if ($info['password'])
		{
			if ($info['password'] != $info['password_conf'])
			{
				$msg = 'Your passwords do not match.';
				$errors .= $msg . "<br/>";
				$err_msgs[] =  array('field'=>'password_conf','msg'=>$msg);
				++$num_errors;
			}
	
			if (strlen($info['password']) < 6)
			{
				$msg = 'Your password must be at least 6 characters long.';
				$errors .= $msg . "<br/>";
				$err_msgs[] =  array('field'=>'password','msg'=>$msg);
				++$num_errors;
			}
		}
		else
		{
			$msg = 'Password is missing.';
			$errors .= $msg . "<br/>";
			$err_msgs[] =  array('field'=>'password','msg'=>$msg);
			++$num_errors;
		}
	
		/* If there are errors, well, get out */
		if ($num_errors > 0)
		{
			return false;
		}
		else
		{

			unset($info['password_conf']);
			unset($info['hours_of_operation']);
				
			$info['sign_up_date'] = date('Y-m-d H:i:s');
				
			// TE - changed to not require email activation
			$info['account_activated'] = 'Y';
				
			$info['account_system'] = 'new';
			if (time() % 3 == 0) $info['dedicated_rep'] = $CFG->dedicated_rep_1;
			else if (time() % 3 == 1) $info['dedicated_rep'] = $CFG->dedicated_rep_2;
			else $info['dedicated_rep'] = $CFG->dedicated_rep_3;
	
			$cust_id     = Customers::insert($info);

			$_SESSION['logged_in']       = false;
				
			$activation_link_hash = md5($info['first_name'] . ' ' . $info['last_name'] .' ' . $info['email'] . " ".$cust_id);
						
			// TE - changed to not require email activation - disabled the email, and logged them in
			// 			/* send notification */
			// 			$vars = array();
			// 			$vars['href'] = $CFG->sslurl . "activate_account.php?activation_code=".$activation_link_hash;
			// 			$vars['email_address'] = $info['email'];
			// 			$vars['store_id'] = "1";
			// 			$E = TigerEmail::sendOne($info['first_name'] . ' ' . $info['last_name'],
			//                                     $info['email'], 'new_account', $vars, '');
			MyAccount::login($info['email'],$info['password']);
	
			return true;
		}
	}
	
	static function createAccountOld(&$info, &$errors = '', $info2=array(),&$err_msgs='',$validate_all=true)
	{
        global $CFG;

        $errors     = '';
        $num_errors = 0;
        
        if($validate_all){
	        /* validate information */
	        $validation_fields  = array
	        (
	            'first_name'    => 'First Name',
	            'last_name'     => 'Last Name',
	            'address1'      => 'Billing Address',
	            'city'          => 'Billing City',
	            'state'         => 'Billing State',
	            'zip'           => 'Billing Zip Code',
	            'phone'         => 'Billing Phone Number',
	            'location_type' => 'Billing Location'
	        );
	
	        $num_errors         = 0;
	
	        foreach ($validation_fields as $field => $field_name)
	        {
	            if ($info[$field] == '')
	            {
	                $msg = sprintf("%s is missing.", $field_name);
	                $errors .= $msg . '</br>';
	                $err_msgs[] =  array('field'=>$field,'msg'=>$msg);
	                ++$num_errors;
	            }
	        }
			if ($_FILES["img_url"]["name"]){
			$allowedExts = array("jpg", "jpeg", "gif", "png");
			$extension = end(explode(".", $_FILES["img_url"]["name"]));
			if ((($_FILES["img_url"]["type"] == "image/gif")
			|| ($_FILES["img_url"]["type"] == "image/jpeg")
			|| ($_FILES["img_url"]["type"] == "image/png")
			|| ($_FILES["img_url"]["type"] == "image/pjpeg"))
			&& ($_FILES["img_url"]["size"]/1024 < 2000)
			&& in_array($extension, $allowedExts))
			  {
			  if ($_FILES["img_url"]["error"] > 0)
			    {
			    $msg = sprintf( "There is a problem with your uploaded file: " . $_FILES["img_url"]["error"]);
			    $errors .= $msg . '</br>';
			    $err_msgs[] =  array('field'=>$field,'msg'=>$msg);
					++$num_errors;
			    }
			  else
			    {
			    if (file_exists("upload/" . $_FILES["img_url"]["name"]))
			      {
			      $msg = sprintf($_FILES["img_url"]["name"] . " already exists. Choose a file with a different name.");
			      $errors .= $msg . '</br>';
			      $err_msgs[] =  array('field'=>$field,'msg'=>$msg);
						++$num_errors;
			      }
			     }
			  }
			else
			  {
			      $msg = sprintf("Invalid file ");
			      $errors .= $msg . '</br>';
			      $err_msgs[] =  array('field'=>$field,'msg'=>$msg);
			      ++$num_errors; 
			  }
			}
	
	        if ($_POST['same_as_billing'] == "Y") // if shipping the same as billing
	        {
	        	$info2 = $info;
	        	unset($info2['password']);
	        	unset($info2['password_conf']);
				unset($info2['institution_type_id']);
				unset($info2['institution_name']);
				unset($info2['role_id']);						        	
				unset($info2['email']);
				unset($info2['ein_number']);
				//unset($info2['receive_email_specials']);
				unset($info2['hours_of_operation']);
				unset($info2['img_url']);
	        }
	        else 
	        {
	            foreach ($validation_fields as $field => $field_name)
	        	{
	        		if ($field == "first_name" || $field == "last_name") continue;
	            	if ($info2[$field] == '')
	            	{
	            		$field_name = str_replace("Billing", "Shipping", $field_name); 
	                	$msg = sprintf("%s is missing.", $field_name);
	                	$errors .= $msg . "<br/>";
	                	$err_msgs[] =  array('field'=>$field,'msg'=>$msg);
	                	++$num_errors;
	            	}
	        	}        	
	        }
	        
	        if (!$info['role_id'])
	        {
	        	$msg = 'Your Role is missing.';
	        	$errors .= $msg . "<br/>";
	        	$err_msgs[] =  array('field'=>'role_id','msg'=>$msg);
	        	++$num_errors;
	        }
	        if (!$info['institution_type_id'])
	        {
	        	$msg = 'Your Company or Institution Type is missing.';
	        	$errors .= $msg . "<br/>";
	        	$err_msgs[] =  array('field'=>'institution_type_id','msg'=>$msg);
	        	++$num_errors;
	        }
	        // enforce that if choose student, must choose culinary school
	        if ($info['role_id'] == $CFG->student_role_id && $info['institution_type_id']!= $CFG->culinary_school_id)
	        {
	        	$msg = "You must choose \"Culinary School\" as your Insitution Type if you have chosen \"Student\" as your role";
	        	$errors .= $msg . "<br/>";
	        	$err_msgs[] =  array('field'=>'institution_type_id','msg'=>$msg);
	        	++$num_errors;
	        }
	        
	        if ($info['hours_of_operation'] != "")
	        {
	        	$msg = 'You are not allowed to enter create an account.';
	        	$errors .= $msg . "<br/>";
	        	$err_msgs[] =  array('field'=>'hours_of_operation','msg'=>$msg);
	        	++$num_errors;
	        }
        }
        /* Validate Email */
        if ($info['email'])
        {
		    $existing = Customers::get(0,'','','','','',$info['email']);

		    if(is_array($existing) && count($existing) > 0)
		    {
			    $msg = 'That email address is already in use.';
			    $errors .= $msg . "<br/>";
			    $err_msgs[] =  array('field'=>'email','msg'=>$msg);
                ++$num_errors;
		    }
		    if (strstr($info['email'], "adulttoys") !== false || strstr($info['email'], "feeladult.com") !== false ||
		    	strstr($info['email'], "sex.com") !== false)
		    {
		    	$msg = 'Your email is invalid.';
		    	$errors .= $msg . "<br/>";
		    	$err_msgs[] =  array('field'=>'email','msg'=>$msg);
            	++$num_errors;
		    }
        }
        else
        {
            $msg = 'Your email is missing.';
            $errors .= $msg . "<br/>";
            $err_msgs[] =  array('field'=>'email','msg'=>$msg);
            ++$num_errors;
        }

  		/* Validate Passwords */
		if ($info['password'])
		{
    		if ($info['password'] != $info['password_conf'])
    		{
    			$msg = 'Your passwords do not match.';
    			$errors .= $msg . "<br/>";
    			$err_msgs[] =  array('field'=>'password_conf','msg'=>$msg);
                ++$num_errors;
    		}

    		if (strlen($info['password']) < 6)
    		{
    			$msg = 'Your password must be at least 6 characters long.';
    			$errors .= $msg . "<br/>";
    			$err_msgs[] =  array('field'=>'password','msg'=>$msg);
                ++$num_errors;
    		}
		}
		else
		{
	        $msg = 'Password is missing.';
	        $errors .= $msg . "<br/>";
	        $err_msgs[] =  array('field'=>'password','msg'=>$msg);
		    ++$num_errors;
		}
		
		/* If there are errors, well, get out */
		if ($num_errors > 0)
		{
			return false;
		}
		else
		{
		 /*give the img file a new name to avoid duplicate filenames*/
		 if ($_FILES["img_url"]["name"]){
        $newname=time();              
	      $newname.=".".$extension;
	      $info['img_url']=$newname;		
        if ($_FILES["img_url"]["name"]){move_uploaded_file($_FILES["img_url"]["tmp_name"],"upload/".$newname);}
			}	
		 
			unset($info['password_conf']);
			unset($info['hours_of_operation']);
			
			$info['sign_up_date'] = date('Y-m-d H:i:s');
			
			/*if ($info['receive_email_specials'] == 'Y') 
			{
				$nlo = new object();
		    	$nlo->email = $info['email'];
		    	$rec = NewsLetterList::get1ByO( $nlo );
		    
		    	if( !$rec ) {
					NewsletterList::insert( array('email' => $info['email'], 'is_active' => 'Y' ), "date_added" );
		    	} else {
					//Update the current record to Y
					NewsLetterList::update( $rec['id'], array( 'is_active' => 'Y' ) );
		    	}
			}
			unset($info['receive_email_specials']);*/
			
			// TE - changed to not require email activation
			$info['account_activated'] = 'Y';
			
			$info['account_system'] = 'new';
			if (time() % 3 == 0) $info['dedicated_rep'] = $CFG->dedicated_rep_1;
			else if (time() % 3 == 1) $info['dedicated_rep'] = $CFG->dedicated_rep_2;
			else $info['dedicated_rep'] = $CFG->dedicated_rep_3; 		
						
			$cust_id     = Customers::insert($info);

			$_SESSION['logged_in']       = false;
					
			$activation_link_hash = md5($info['first_name'] . ' ' . $info['last_name'] .' ' . $info['email'] . " ".$cust_id);
			
			// also add shipping address
			$info2['primary_address'] = 'Y';
			$info2['billing'] = 'N';			
			Customers::addAddress($cust_id, $info2);
        	
			// make $info3 hold billing information, too, so can be inserted into customer_shipping_addresses table
            $info3 = $info;
        	unset($info3['password']);
        	unset($info3['password_conf']);
			unset($info3['institution_type_id']);
			unset($info3['institution_name']);
			unset($info3['role_id']);		
			unset($info3['email']);
			//unset($info3['receive_email_specials']);
			unset($info3['account_system']);
			unset($info3['account_activated']);
			unset($info3['dedicated_rep']);
			unset($info3['sign_up_date']);
			unset($info3['ein_number']);
			unset($info3['img_url']);

			$info3['primary_address'] = 'Y';
			$info3['billing'] = 'Y';	
			Customers::addAddress($cust_id, $info3);	
			
			// TE - changed to not require email activation - disabled the email, and logged them in
// 			/* send notification */
// 			$vars = array();
// 			$vars['href'] = $CFG->sslurl . "activate_account.php?activation_code=".$activation_link_hash;
// 			$vars['email_address'] = $info['email'];
// 			$vars['store_id'] = "1";
// 			$E = TigerEmail::sendOne($info['first_name'] . ' ' . $info['last_name'],
//                                     $info['email'], 'new_account', $vars, '');
			MyAccount::login($info['email'],$info['password']);
                    
			return true;
		}
	}

	static function logged_in()
	{
	    if(!$_SESSION['cust_acc_id'] && $_COOKIE['justchargerRemember'] && !$_REQUEST['action']=='logout'){
		list($username,$md5) = explode('_',$_COOKIE['justchargerRemember']);
		MyAccount::login_cookie($username,$md5);
	    }
	    return ($_SESSION['cust_acc_id']) ? true : false;
	}

	static function logout()
	{
	    global $CFG;
	    
	    $_SESSION = array(); 
	    session_unset();
	    session_destroy();
	    
	    if ($CFG->in_testing)
	    {
    	    setcookie("justchargerRemember", "", time()-3600, '/', "dev1-justchargerplates.tigerchef.com" );
	    }
	    else
	    {
		    setcookie("justchargerRemember", "", time()-3600, '/', ".justchargerplates.com" );
	    }
		return true;
	}

	static function login($username, $password, $active_only=false)
	{
	    global $cart;
	    
	    if ($username && $password)	    
	    {
		global $CFG;
		
    		$customer = Customers::getByLogin($username, $password, $active_only);
    		
    		if (is_array($customer) && count($customer) > 0)
    		{
    			$_SESSION['cust_acc_id']     = $customer['id'];
    			$_SESSION['cust_name']       = $customer['first_name'] . ' ' . $customer['last_name'];
    			$_SESSION['cust_username']   = $customer['email'];
			//$_SESSION['cust_username']   = $_POST['log'];
    			$_SESSION['logged_in']       = true;
			
			//make sure any items put in the cart before they logged in have a customer id
			        $previous_items = $cart->getCustomerItems();
			        $items_added = $cart->AddCustomerId();
			
			if($items_added && count($previous_items)>0){
			    $_SESSION['show_cart_message'] = true;
			}
			//remove any irrelevant promo codes from previous sessions
			$cart->removeExpiredPromoCodes();

    			//if ($_POST['remember_me'])
		        {
			    
			        if( $CFG->in_testing )
			        {
			        		setcookie("justchargerRemember", $customer['email'] . '_' . md5( $customer['email'] . md5(md5($customer['id']) . $_POST['pwd'] . $customer['id']) ), time()+60*60*24*30, '/', "dev1-justchargerplates.tigerchef.com" );
			        }
				    else
				    {
				        setcookie("justchargerRemember", $customer['email'] . '_' . md5( $customer['email'] . md5(md5($customer['id']) . $_POST['pwd'] . $customer['id']) ), time()+60*60*24*30, '/', ".justchargerplates.com" );
		            }
		        }

				if ($customer['account_system'] == "old") 
				{					
					$_REQUEST['redirect'] = "account.php?action=upgrade";
				}
			
			if($customer['require_password_reset'] == 'Y'){
			    $_REQUEST['redirect'] = "account.php?action=change_password";
			}
			
      			return true;
    		}
	    }

		return false;
	}

	static function login_cookie($username, $md5)
	{
		global $cart;
		
		$customer = Customers::get(0, '', '', '', '', '', $username);
		if (md5( $customer[0]['email'] . $customer[0]['password']) == $md5)
		{
			if(is_array($customer) && count($customer) == 1)
			{
				$_SESSION['cust_acc_id']    = $customer[0]['id'];
				$_SESSION['cust_username']  = $customer[0]['email'];
				$_SESSION['cust_name']      = $customer[0]['first_name'] . ' ' . $customer[0]['last_name'];
				$_SESSION['logged_in']      = true;
				
				//$_SESSION['cart_data']['cust_acc_id'] = $customer[0]['id'];
				//remove any irrelevant promo codes from previous sessions
				$cart->removeExpiredPromoCodes();
				$cart->updateCartReference();
		
				return true;
			}
		}

		return false;
	}
	static function showItems($items){
		foreach($items as $item){
		$item_img=Catalog::makeProductImageLink($item['product_id']);
		?>
		<div class="obcontent">
		            <span class="image"><img src="<?=$item_img?>"></span>
		            <span class="name"><?=$item['product_name']?></span>		                      
		</div>
		<?}
	}
	static function showOrders($criteria = '')
	{
        ?>
<script>
        function toggleItems()
        {
            if( $('morelines').style.display == 'none' ) {
                $('morelines').style.display = 'inline';
                $('showmore').style.display = 'none';
                $('hidemore').style.display = '';
            }
            else {
                $('morelines').style.display = 'none';
                $('showmore').style.display = '';
                $('hidemore').style.display = 'none';
            }
        }
        </script>
<? global $CFG;
        $order_status_id        = array();
        $orders                 = array();
		$start_date = $end_date = "";
		
        /* determine which orders we should be showing the status id's correspond to those in the order_statuses table */
        switch ($criteria)
        {
            case    'open'      :   $order_status_id = array(1,3,5);
            						$box_title_text = "My Open Orders";
                                    break;

            case    'invoice'   :
            case    'closed'    :   $order_status_id = array(2,4);
            						$box_title_text = "My Orders";
                                    break;
            case 	'recent'	:	$order_status_id = 0;
            						$start_date = date('Y-m-d', strtotime("-3 month"));
            						$end_date = date('Y-m-d', strtotime("now"));
            						$box_title_text = "My Recent Orders";
            						break;
            default             :   $order_status_id = 0;
            						$box_title_text = "My Orders";
                                    break;
        }

        /* fetch all the orders */
		$orders = Orders::get(0,$_SESSION['cust_acc_id'], '', '', $order_status_id, $start_date, $end_date);
		?>
		<a name='orders'></a>
		<div class="title-box">
			<h2><?= $box_title_text ?></h2>
		</div>
	    <?
		    if (is_array($orders))
		    {
		    	?>
		    	<div class="recent-orders">
						<div class="orders-holder">
		    	<? 
		        foreach ($orders as $order)
		        {				
		        ?>
		            <div class="order-box">
		            <div class="obheader">
		            <div class="sec">
		            <span class="title">Order placed on:</span>
		            <span class="content"><?= date('m-d-Y',strtotime($order['date'])) ?></span></div>
                    <div class="sec">
		            <span class="title">Order total:</span>
		            <span class="content">$<?=$order['order_total']?></span>
		            </div>
		            <div class="section">
		            <div class="sec">
		            <span class="title">Order #:</span>
		            <span class="content"><?= $order['id'] ?></span></div>
		            <div class="sec last">
		            <span class="title">Order Status:</span>
		            <span class="content"><?=$order['order_status_name']?></span></div>
		            </div>
		             </div>
					  <div class="oblinks">
		            <span class="obreview"><a href='<?= $_SERVER['PHP_SELF']?>?action=write_review'>Write Review</a></span>
		            <span class="obprint"><a href='<?= $_SERVER['PHP_SELF']?>?action=print_invoice&order_id=<?= $order['id'] ?>'>Print Invoice</a></span>
					<?$trackingnums 	= Orders::getTracking(0, $order['id']);
					
					if (count($trackingnums))
						if (count($trackingnums)>=1)
							$link= 'href="'.$_SERVER['PHP_SELF'].'?action=order_details&order_id='.$order['id'].'#tracking"';
						else $link='href="'.str_replace('[tracking_number]',$trackingnums[0]['tracking_number'],$trackingnums[0]['tracking_url']).'" target="_blank"';
					else $link='class="disabled"';
			
						?>
		            <span class="obtrack"><a <?=$link?>>Track Package</a></span>
		            <span class="obreturns"><a href="/returns.php">Return item</a></span>
		            <span class="view"><a href='<?= $_SERVER['PHP_SELF']?>?action=order_details&order_id=<?= $order['id'] ?>'>View Order Details</a></span>
		            
		            </div>
					 <?$the_items = Orders::getItems(0, $order['id']);
					 MyAccount::showItems($the_items);?>
		              
		            
		            
		            
		    <!--         
		            
								<dl>
								<dt>Order No.</dt>
									<dd><a href='<?=$_SERVER['PHP_SELF']?>?action=order_details&order_id=<?= $order['id'] ?>'><?= $CFG->order_prefix.$order['id'] ?></a></dd>
									<dt>Order Date</dt>
									<dd><?= date('m-d-Y',strtotime($order['date'])) ?></dd>
									<dt>Total</dt>
									<dd>$<?=$order['order_total']?></dd>
									<dt>Status</dt>
									<dd><?=$order['order_status_name']?></dd>
									<dt>Order Details</dt>
									<dd><a href='<?= $_SERVER['PHP_SELF']?>?action=order_details&order_id=<?= $order['id'] ?>'>View</a></dd>
									<dt>Review Item</dt>
									 <dd><a href='<?= $_SERVER['PHP_SELF']?>?action=print_invoice&order_id=<?= $order['id'] ?>'>Print Invoice</a></dd>
									<dd><a href='<?=Catalog::makeProductLink_($one_item);?>#review-section'>Write Review</a></dd>
								</dl>
									-->

		             <? 
		                $counter = 0;
		                $the_items_new = array();
		                // re-organize so that when there are multiple lines for the same item (because of case pricing), 
		                // it displays the sum of their quantities.
		                foreach ($the_items as $one_item)
		                {
		                	$this_prod_id = $one_item['product_id'] . "-" . $one_item['product_options_id'];
		                	$the_items_new[$this_prod_id]['qty'] += $one_item['qty'];
		                	$the_items_new[$this_prod_id]['product_name'] = $one_item['product_name'];
											$the_items_new[$this_prod_id]['product_id']=$one_item['product_id'];
											$the_items_new[$this_prod_id]['date']=$one_item['date'];
											$the_items_new[$this_prod_id]['url_name']=$one_item['url_name'];
			                }
		                foreach ($the_items_new as $one_item)
		                {
		                	$counter++;
											$now = time(); 
                      $order_date = strtotime($one_item['date']);
                      $datediff = $now - $order_date;
                      $datediff=floor($datediff/(60*60*24));
					if ($counter == 16) echo "<div id='showmore' style='display:block'><a href='#".$order['id']."' onClick=\"toggleItems();\">View More Items ></a></div><div id='morelines' style='display:none'>";
				echo "<table><tr><td >";
						if ($datediff > 7){
							   ?>
								   
						  <?}
							echo "</td><td>";
		                	echo "&nbsp;&nbsp;&nbsp;";		                	 
		                //	echo $one_item['qty']. "   " . $one_item['product_name'];
											echo "</td></tr></table>";
											
		                }
		                if ($counter >= 16) 
		                {
		                	echo "</div>";
		                	echo "<div id='hidemore' style='display:none'><a href='#".$order['id']."' onClick=\"toggleItems();\">< Hide More Items</a></div>";
		                }
		                ?>
		                </div>	
		        <?
		        }?>
		        </div>
		        <div class="btn-box">
					<? if ($criteria == "") // if was showing old orders
					{?>
					<a href="<?=$_SERVER['PHP_SELF']?>" class="btn">Return to My Account</a>
					<?}
					else 
					{?> 		        
					<a href="<?=$_SERVER['PHP_SELF']."?action=old_orders"?>" class="btn">View All Orders</a>
					<? } ?>
				</div>
				</div>
		    <? }
		    else
		    {
		    ?>
					<div class="content-box no-order">
						<div class="holder">
							<p>You have no <?=$criteria?> orders. Click <a href="<?=$_SERVER['PHP_SELF']."?action=old_orders"?>">View all Order History</a> to view all orders.</p>
						</div>
					</div>
		    <?
		    }
	}
	static function showReviewSection(){
		$orders = Orders::get(0,$_SESSION['cust_acc_id'], '', '', 0);?>
		<div class="order-box">
		<?$bg_color='colored';
		foreach($orders as $order){
		$items = Orders::getItems(0, $order['id']);		
		foreach($items as $item){
		$item_img=Catalog::makeProductImageLink($item['product_id']);
		$bg_color=($bg_color=='')?'colored':'';
		?>
		<div class="obcontent <?=$bg_color?>">
		
		            <span class="image"><img src="<?=$item_img?>"></span>
		            <div class="write_review"><span class="name"><?=$item['product_name']?></span>		                      
		            <span><div id="rev_response<?=$item['product_id']?>" class="rev_response hidden"></div>
					<?=write_review($item, false, $item['product_id'])?></span></div>		                      
		
		</div>
			<?}
		}?>
		</div>
		<?
	}
	static function showInvoice($id, $customer_id = 0)
	{
		if($customer_id != 0)
		{
			$orders = Orders::get($id,$customer_id);

			if(!is_array($orders) || count($orders) == 0)
			{
			    return;
			}
		}
		$invoice = Orders::makeInvoiceForDisplayRedesign($id, false, true);
		?>
<div class="wrapper white invoice">
	<div id="content">
				<img class="invoice-img" src="/images/order-confirmation-icon.png" alt="Order Confirmation">
	
		<div class="invoice-top"></div>
		<h1 class="invoice">Invoice for order <?= $id?></h1>
		<?
		echo $invoice;
		?>				
	</div>
</div>
		<? 

		echo'<br>';
	}


	static function checkRequiredFields($info, $reqs, $check_email = true)
	{
		$missing = array();

		if (is_array($reqs))
		{
			foreach($reqs as $key => $req)
			{
				if (is_array($req))
				{
					foreach($req as $sub_req)
					{
						if (empty($info[$key][$sub_req]) || $info[$key][$sub_req] = '')
						{
						    $missing[$key][] = $sub_req;
						}
					}
				}
				else
				{
					if (empty($info[$req]) || $info[$req] = '')
					{
					    $missing[] = $req;
					}
				}
			}

			if ($info['password'] != '' && $info['password'] != $info['password_conf'])
			{
				$missing['password'] = 'The passwords you entered did not match';
			}

			if ($check_email && $info['email'] )
			{
				$existing = Customers::get(0,'','','','','',$info['email']);

				if (is_array($existing) && count($existing) > 0)
				{
					$missing['email'] = 'That email address is already in use.<br>';
				}
			}
		}

		return $missing;
	}
	static function showOrderDetails($id)
	{
		echo '<div class="account-box main-section">';
		Orders::makeMyAccountOrderDetailsDisplay($id);

		echo'<br></div>';
	}
	static function getAllCustomerInstitutionTypes($active_only=false)
	{
		$sql = "SELECT * FROM customer_institution_types WHERE 1";
		
		if($active_only) $sql .= " AND is_active = 'Y'";
		
		$sql .= " ORDER BY caption asc";
		//echo $sql;
		return db_query_array($sql);
	}
	
	static function getInstitutionTypeCaptions($ids)
	{
		$id_array = explode("|",$ids);
		$counter = 0;
		$caption = "";
		if ($id_array)
		{
			foreach($id_array as $id)
			{
				if ($id == "") continue;
			
				$sql = "SELECT * FROM customer_institution_types WHERE 1";		
				$sql .= " AND id = '$id'";
				
				$result = db_query_array($sql);
				if ($counter > 0) $caption .= " or ";
				$caption .= $result[0]['caption'];
				$counter++;
			}
		}
		return $caption;
	}
	
	static function getInstitutionTypeInfo($id=0,$caption='')
	{
		$id = (int)$id;
			
		$sql = "SELECT * FROM customer_institution_types WHERE is_active = 'Y' ";

		if($id>0){
			$sql .= " AND id = '$id' ";
		}

		if($caption){
			$sql .= " AND caption = '".mysql_real_escape_string($caption)."' ";
		}

		$result = db_get1(db_query_array($sql));
		return $result;
	}
	
	static function getAllCustomerRoles($active_only=false)
	{
		$sql = "SELECT * FROM customer_roles WHERE 1";
		
		if($active_only) $sql .= " AND is_active = 'Y'";
		
		$sql .= " ORDER BY caption asc";
	//	echo $sql;
		return db_query_array($sql);
	}
	static function getRoleCaptions($ids, $plural = false)
	{
		$id_array = explode("|",$ids);
		$counter = 0;
		$caption = "";
		if ($id_array)
		{
			foreach($id_array as $id)
			{
				if ($id == "") continue;
			
				$sql = "SELECT * FROM customer_roles WHERE 1";		
				$sql .= " AND id = '$id'";
				
				$result = db_query_array($sql);
				if ($counter > 0) $caption .= " or ";
				$caption .= $result[0]['caption'];
				if ($plural) $caption .= "s";
				$counter++;
			}
		}
		return $caption;
	}
	static function seeIfEssensaMember()
	{
		$see_if_active_essensa_member_sql = "SELECT * FROM essensa_members, customers WHERE
			customers.id = " . $_SESSION['cust_acc_id'] ." AND 
			customers.ein_number = essensa_members.gpo_id AND essensa_members.member_status = 'Active'"; 
		$see_if_active_essensa_member = db_query_array($see_if_active_essensa_member_sql);
					
		if ($see_if_active_essensa_member) return true;
		else return false;
	}
	
    static function show_my_account_sidebar()
    {
    	?>
						<ul>
    						
						<li><a href="<?= $_SERVER['PHP_SELF']?>#orders" <? if (!$_SESSION['cust_acc_id']) echo "class='disabled-acct-sidebar' onClick='return false;'"; ?>>Order History</a></li>
						<li><a href="<?= $_SERVER['PHP_SELF']?>#account_settings" <? if (!$_SESSION['cust_acc_id']) echo "class='disabled-acct-sidebar' onClick='return false;'"; ?>>Account Settings</a></li>
						<li><a href="<?= $_SERVER['PHP_SELF']?>#product_reviews" <? if (!$_SESSION['cust_acc_id']) echo "class='disabled-acct-sidebar' onClick='return false;'"; ?>>Product Reviews</a></li>
					<!-- 	<li><a href="<?= $_SERVER['PHP_SELF']?>#subscribe" <? if (!$_SESSION['cust_acc_id']) echo "class='disabled-acct-sidebar' onClick='return false;'"; ?>>Product Subscriptions</a></li>-->
						</ul>
    <? 
    }
    
    static function get_header_account_text(){
    	global $CFG;
    	$return = array();
    	$url = isset( $_SERVER['HTTPS'] )?$CFG->sslurl:$CFG->baseurl;
    	if (!$url) $url = "/";
    	
    	if (!MyAccount::logged_in())
    	{				    		
    		$track_sign_in =  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'Sign in link'});"; 
    		$return['regular'] = '<li><a class="login-btn" href="#" onclick="'.$track_sign_in.'TINY.box.show({url:\''.$url.'popuplogin.php\',post:\'type=login&link='.$_SERVER['PHP_SELF'].'\',fixed:false});return false;">Sign In</a></li>';
    		$track_register =  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'Register link'});";
    		$return['regular'] .= '<li><a href="'.$url.'account.php?action=new_user" onclick = "'.$track_register.'">Register</a></li>';
    		$track_mobile_log_in=  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'Mobile sign in link'});";	
    		$return['mobile'] = '<li><a class="btn-user" href="#" onclick="'.$track_mobile_log_in.'TINY.box.show({url:\''.$url.'popuplogin.php\',post:\'type=login&link='.$_SERVER['PHP_SELF'].'\',fixed:false});return false;">User</a></li>';
    	} else {
    		$track_view_account=  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'View account link'});";
    		$return['regular'] = '<li class="account-login">Hi, <a href="'.$url.'account.php" onclick="'.$track_view_account.'">'.$_SESSION['cust_name'].'</a></li>';
    		$track_log_out=  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'Log out link'});";
    		$return['regular'] .= '<li><a href="'.$url.'account.php?action=logout" onClick="'.$track_log_out.'accountLogout(); return false;">Log Out</a></li>';
   		    $track_mobile_view_account=  "window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'gaTriggerEvent','gaEventCategory': 'Header','gaEventAction': 'Click','gaEventLabel': 'Mobile view account link'});";
    		$return['mobile'] = '<li><a class="btn-user" href="'.$url.'account.php" onclick="'.$track_mobile_view_account.'">User</a></li>';
    	}
    	return $return;
    }
}
?>
