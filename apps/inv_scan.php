<? 
class InvScan
{
	static function insert($info)
	{	
		return db_insert('inv_scan',$info);
	}
	
	static function update($id,$info)
	{
		return db_update('inv_scan',$id,$info);
	}

	static function delete($id)
	{
		return db_delete('inv_scan',$id);
	}
	
	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = InvScan::get($id);
		return $result[0];
	}
	static function areRecords()
	{
		$sql = "SELECT count(*) AS num from inv_scan";
		$result = db_query_array($sql);
		$result_row = $result[0];
		if ($result_row['num'] > 0) return true;
		else return false;
	}
	static function get($id=0, $orderby='', $groupby='')
	{
		$sql = "SELECT inv_scan.*, products.name, products.vendor_sku, 
				product_options.vendor_sku AS option_vendor_sku, 
				product_options.value FROM inv_scan 
				LEFT JOIN products on inv_scan.product_id = products.id
				LEFT JOIN product_options on inv_scan.product_option_id = product_options.id
				WHERE 1";
		if ($id > 0) 
		{
			$id = (int) $id;
			$sql .= " AND inv_scan.id = $id";
		}
		if ($groupby != '') $sql .= " GROUP BY " . $groupby;
		
		if ($orderby != '') $sql .= " ORDER BY " . $orderby;
		
		return db_query_array($sql);
	}
	
	static function empty_table()
	{
		$sql = "TRUNCATE TABLE inv_scan";
		return db_query($sql);
	}
	
	static function insert_into_history_table ($id)
	{
		$info = InvScan::get1($id);
		return db_insert('inv_scan_history',$info);
	}
	static function insert_all_into_history_table()
	{
		return db_query("REPLACE INTO inv_scan_history (id, product_id, product_option_id, qty, scan_date) SELECT * from inv_scan");
	}	
	static function getScanSums()
	{
		$sql = "SELECT product_id, product_option_id, SUM(qty) as sum_qty FROM inv_scan
				GROUP BY product_id, product_option_id";
		
		return db_query_array($sql);		
	}
	static function deleteInvRecords($product_id, $product_option_id, $inventory_location)
	{
		$sql = "DELETE FROM inventory WHERE product_id = $product_id AND product_option_id = $product_option_id AND
				store_id = $inventory_location";
		db_query($sql);
	}

	static function insertInvRecord($product_id, $product_option_id, $inventory_location, $qty)
	{
		$info = array();
		$info['product_id'] = $product_id;
		$info['product_option_id'] = $product_option_id;
		$info['store_id'] = $inventory_location;
		$info['qty'] = $qty;
		Inventory::insert($info);
	}

	static function deleteByIds($product_id, $product_option_id)
	{
		$sql = "DELETE FROM inv_scan WHERE product_id = $product_id AND product_option_id = $product_option_id";
		db_query($sql);
	}
	
}
?>