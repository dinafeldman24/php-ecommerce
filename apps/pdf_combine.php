<?php

include_once(dirname(__FILE__).'/pdf.php');

class PDFCombine extends PDF {
	
	private $pdf_files;
	private $bookmarks;
	private $pdf_ranges;
	
	private $pdf_buffer;
	private $banner;
	private $banner2;
	private $num_pages = 1;
	
	public function __construct()
	{
		$this->pdf_files = array();
		$this->bookmarks = array();
	}
	
	public function __destruct()
	{
		if(is_array($this->pdf_files)){
			foreach($this->pdf_files as $file){
				if(substr($file,0,4) == '/tmp') @unlink($file);
			}
			$this->pdf_files = array();
		}	
	}
	
	public function add($pdf_file,$bookmark='',$placement=1,$page_start=0,$page_end=0)
	{
		$pdflib = new PDFLib();
		if (!$this->getPassword())
			$this->setPassword('bizlic');
		
		$pdf = $pdflib->open_pdi($pdf_file, "password ".$this->getPassword(), 0);

		if (!$pdf) {
			die("Errodr: " . $pdf_file. $pdflib->get_errmsg());
	    }

		$this->num_pages += $pdflib->get_pdi_value("/Root/Pages/Count", $pdf, 0, 0);
		
		if ($placement != -1 || !is_array($this->pdf_files) || !is_array($this->bookmarks) || !is_array($this->pdf_ranges)) {
			$this->pdf_files[] = $pdf_file;
			$this->bookmarks[] = $bookmark;
			$this->pdf_ranges[] = array($page_start,$page_end);
		}
		else {
			array_unshift($this->pdf_files,$pdf_file);
			array_unshift($this->bookmarks,$bookmark);
			array_unshift($this->pdf_ranges,array($page_start,$page_end));		
		}
	}	
	
	public function addBefore($pdf_file,$bookmark='')
	{
		$this->add($pdf_file,$bookmark,-1);
	}
	
	public function addAfter($pdf_file,$bookmark='')
	{
		$this->add($pdf_file,$bookmark,1);
	}
	
	public function addBanner($text,$additional_text='')
	{
		$this->banner = $text;
		$this->banner2 = $additional_text;
	}
	
	protected function compile(&$pdflib)
	{
		global $CFG;
				
		if ($this->banner) {
			$ct = 1;
			
			//$pdflib->begin_page_ext(612,792, "");
			$pdflib->begin_page_ext(595,842, "");
			
			$font = $pdflib->load_font("Helvetica-Bold", "winansi", "");
		    $pdflib->setfont($font, 24.0);
		    $pdflib->set_text_pos(50, 700);
		    $pdflib->show($this->banner);
		    $pdflib->setfont($font, 12.0);
		    $pdflib->set_text_pos(50, 650);
		    $banner2_array = explode("\r\n",$this->banner2);
		    
		    foreach ($banner2_array as $b2){
		    	if ($b2 != ''){
		    		$pdflib->show($b2);
		    		$pdflib->continue_text("");
		    	}
		    }
		    
		    
		    
		    $pdflib->continue_text("Contents:");
		    
		    $font = $pdflib->load_font("Helvetica", "winansi", "");
		    $pdflib->setfont($font, 12.0);
		    
			foreach ($this->pdf_files as $key => $pdf_file) {
		    	$pdf = $pdflib->open_pdi($pdf_file, "password ".$this->getPassword(), 0);
				$pdflib->continue_text($this->bookmarks[$key]);
				
				if (!$pdf) {
					die("Error: " . $pdf_file. $pdflib->get_errmsg());
			    }
	
				$ct += $pdflib->get_pdi_value("/Root/Pages/Count", $pdf, 0, 0);
			}
			
			$pdflib->setfont($font, 24.0);
			$pdflib->continue_text("");
		    $pdflib->continue_text("Total $ct Pages");
		    $pdflib->end_page_ext("");
		}
					    
	    foreach ($this->pdf_files as $key => $pdf_file) {
	    	$pdf = $pdflib->open_pdi($pdf_file, "password bizlic", 0);
	    	
	    	if (!$pdf) {
				die("Error: " . $pdflib->get_errmsg());
		    }
		    
			$ct = $pdflib->get_pdi_value("/Root/Pages/Count", $pdf, 0, 0);

			$start_page = ($this->pdf_ranges[$key][0] > 0) ? $this->pdf_ranges[$key][0] : 1;
			$end_page = ($this->pdf_ranges[$key][1] > 0) ? $this->pdf_ranges[$key][1] : $ct;
					
			for ($pageno = $start_page; $pageno <= $end_page; $pageno++) {
				$page = $pdflib->open_pdi_page($pdf, $pageno, "");
				//$pdflib->begin_page_ext(20, 20, "");		
				$pdflib->begin_page_ext(612,792, "");
				if ($this->bookmarks[$key] && !$bookmarked[$key]) {
					$bookmarked[$key] = true;
	    			$pdflib->create_bookmark($this->bookmarks[$key],'');
				}
	    			
				$pdflib->fit_pdi_page($page, 0, 0, "adjustpage");//
				$pdflib->end_page_ext("");		
				$pdflib->close_pdi_page($page);
				
		    }		    
	    	
	    	$pdflib->close_pdi($pdf);
	    }
		$this->num_pages = $ct;	    
	}
	
	
	public function getNumPages()
	{
		return $this->num_pages;
	}
	
}

?>