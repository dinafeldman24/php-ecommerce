<?
class AmazonInventoryFeed
{ 
	static function insert($info)
	{
		return db_insert('amazon_inventory_feed',$info);
	}
	
	static function update($id,$info)
	{
		return db_update('amazon_inventory_feed',$id,$info,'id');
	}
	
	static function empty_table()
	{
		mysql_query("TRUNCATE TABLE amazon_inventory_feed") or die(mysql_error());
	}
	
	static function delete($id)
	{
		return db_delete('amazon_inventory_feed',$id);
	}
	
	static function get($sku="",$order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(amazon_inventory_feed.id)) AS total ";
		} else {
			$sql .= " amazon_inventory_feed.* ";
		}
	
		$sql .= " FROM amazon_inventory_feed ";
	
		$sql .= " WHERE 1 ";
	
		if ($sku > 0) {
			$sql .= " AND amazon_inventory_feed.sku = '$sku' ";
		}
	
		if (!$get_total){
			//$sql .= ' GROUP BY amazon_inventory_feed.id ';
	
			if($order){
				$sql .= " ORDER BY ";
	
				$sql .= addslashes($order);
	
				if ($order_asc !== '' && !$order_asc) {
					$sql .= ' DESC ';
				}
			}
		}
	
		if ($limit > 0) {
			$sql .= db_limit($limit,$start);
		}
	
		if (!$get_total) {
			$ret = db_query_array($sql);
		} else {
			$ret = db_query_array($sql,'',true);
		}
	
		return $ret;
	}
	
	static function get1($sku)
	{
		if (!$sku) return false;
		$result = self::get($sku);
	
		return $result[0];
	}
	
	static function update_prods_with_asins()
	{ 
		$sql = "UPDATE `amazon_inventory_feed`, `products` set products.asin = amazon_inventory_feed.asin WHERE amazon_inventory_feed.sku = products.vendor_sku";
		mysql_query($sql) or die(mysql_error());
		$sql2 = "UPDATE `amazon_inventory_feed`, `product_options` set product_options.asin = amazon_inventory_feed.asin WHERE amazon_inventory_feed.sku = product_options.vendor_sku";
		mysql_query($sql2) or die(mysql_error());
		$sql3 = "UPDATE `amazon_inventory_feed`, `amazon_warehouse_skus` set amazon_warehouse_skus.asin = amazon_inventory_feed.asin WHERE amazon_inventory_feed.sku = amazon_warehouse_skus.warehouse_sku";
		mysql_query($sql3) or die(mysql_error());							
	}
	
	static function get_prods_for_channelmax()
	{
		
		$floor_over_30_multiplier = "1.43"; // new way, net profit margin
		 
		$floor_under_30_multiplier = "1.43"; // new way, net profit margin
		
		$floor_multiplier_fba = "1.43"; // new way, net profit margin
		
		$floor_multiplier_warehouse = "1.18"; // new way, net profit margin
				
		$floor_multiplier_flash_furniture = "1.82"; // new way, net profit margin
				
		$floor_multiplier_tiger_chef = "1.5"; // new way, net profit margin 		
		
		$floor_under_30_multiplier_johnson_rose = "1.43";		
		$floor_over_30_multiplier_johnson_rose = "1.43";
		
		$floor_over_30_multiplier_town = "1.43";
		 
		$ceiling_over_30_multiplier = "1.85"; // old way, markup--keeping this way per Mr. O		
		$ceiling_under_30_multiplier = "3.00"; // old way, markup--keeping this way per Mr. O
				
		
		$ceiling_multiplier_warehouse = "1.25"; // new way, net profit margin
		$cdn_extra_fee = 4;
		$bunn_extra_fee = 8;
		$calmil_extra_fee = 10;
		$crestware_extra_fee = 5;
		
		$fba_cost_string = "(if((product_options.vendor_price IS NOT NULL AND product_options.vendor_price <> '0.00'), product_options.vendor_price,products.vendor_price) + products.fba_order_handling_fee + products.fba_pick_and_pack_fee +	products.fba_weight_fee + products.fba_storage_fee + products.fba_inbound_shipping_cost)";
		// non fba UNION fba UNION warehouse skus
		$sql = "(SELECT '0' as is_fba, 
		if(product_options.vendor_sku IS NOT NULL, product_options.vendor_sku, products.vendor_sku) AS the_vendor_sku, 
		if(product_options.vendor_sku IS NOT NULL, product_options.asin, products.asin) AS the_asin,
		CASE WHEN (products.map_price > 0 AND products.vendor_price < products.map_price)
			 THEN products.map_price
			 ELSE
				CASE WHEN ($fba_cost_string > 0)
				THEN 
					CASE WHEN (products.brand_id = 1093) THEN $floor_multiplier_flash_furniture * $fba_cost_string
					     WHEN (products.brand_id = 26) THEN $floor_multiplier_tiger_chef * $fba_cost_string
						 WHEN (products.brand_id = 676)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN $floor_over_30_multiplier_johnson_rose * $fba_cost_string
								ELSE $floor_under_30_multiplier_johnson_rose * $fba_cost_string 
								END
						 WHEN (products.brand_id = 104)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN $floor_over_30_multiplier_town * $fba_cost_string
								ELSE $floor_under_30_multiplier * $fba_cost_string 
								END								
						WHEN (products.brand_id = 200)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN ($floor_over_30_multiplier * $fba_cost_string) + $cdn_extra_fee
								ELSE ($floor_under_30_multiplier * $fba_cost_string) + $cdn_extra_fee 
								END		
						WHEN (products.brand_id = 673)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN ($floor_over_30_multiplier * $fba_cost_string) + $bunn_extra_fee
								ELSE ($floor_under_30_multiplier * $fba_cost_string) + $bunn_extra_fee 
								END	
						WHEN (products.brand_id = 1135)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN 
									CASE WHEN ($fba_cost_string < 50)
									THEN ($floor_over_30_multiplier * $fba_cost_string) + $calmil_extra_fee
									ELSE ($floor_over_30_multiplier * $fba_cost_string)
									END
								ELSE ($floor_under_30_multiplier * $fba_cost_string) + $calmil_extra_fee 
								END									
						WHEN (products.brand_id = 91)
							 THEN 
								CASE WHEN ($fba_cost_string > 30)
								THEN ($floor_over_30_multiplier * $fba_cost_string)									
								ELSE
									CASE WHEN ($fba_cost_string <= 25)
									THEN  $floor_under_30_multiplier * $fba_cost_string + $crestware_extra_fee
									ELSE $floor_under_30_multiplier * $fba_cost_string
									END 
								END								
						 ELSE
							CASE WHEN ($fba_cost_string > 30)
							THEN $floor_over_30_multiplier * $fba_cost_string
							ELSE $floor_under_30_multiplier * $fba_cost_string
							END
					END										
				WHEN (product_options.vendor_sku IS NOT NULL)
				THEN
					CASE WHEN (products.brand_id = 1093) THEN $floor_multiplier_flash_furniture * product_options.additional_price
						 WHEN (products.brand_id = 26) THEN $floor_multiplier_tiger_chef * product_options.additional_price
						 WHEN (products.brand_id = 676)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN $floor_over_30_multiplier_johnson_rose * product_options.additional_price
								ELSE $floor_under_30_multiplier_johnson_rose * product_options.additional_price 
								END
						WHEN (products.brand_id = 104)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN $floor_over_30_multiplier_town * product_options.additional_price
								ELSE $floor_under_30_multiplier * product_options.additional_price 
								END								
						 WHEN (products.brand_id = 200)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN ($floor_over_30_multiplier * product_options.additional_price) + $cdn_extra_fee
								ELSE ($floor_under_30_multiplier * product_options.additional_price) + $cdn_extra_fee 
								END
						WHEN (products.brand_id = 673)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN ($floor_over_30_multiplier * product_options.additional_price) + $bunn_extra_fee
								ELSE ($floor_under_30_multiplier * product_options.additional_price) + $bunn_extra_fee 
								END	
						WHEN (products.brand_id = 1135)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN 
									CASE WHEN (product_options.additional_price < 50)
									THEN ($floor_over_30_multiplier * product_options.additional_price) + $calmil_extra_fee
									ELSE ($floor_over_30_multiplier * product_options.additional_price)
									END
								ELSE ($floor_under_30_multiplier * product_options.additional_price) + $calmil_extra_fee 
								END									
						WHEN (products.brand_id = 91)
							 THEN 
								CASE WHEN (product_options.additional_price > 30)
								THEN ($floor_over_30_multiplier * product_options.additional_price)									
								ELSE
									CASE WHEN (product_options.additional_price <= 25)
									THEN  $floor_under_30_multiplier * product_options.additional_price + $crestware_extra_fee
									ELSE $floor_under_30_multiplier * product_options.additional_price
									END 
								END														
						ELSE 
							CASE WHEN (product_options.additional_price > 30)
							THEN $floor_over_30_multiplier * product_options.additional_price
							ELSE $floor_under_30_multiplier * product_options.additional_price
							END
					END			
				ELSE
					CASE WHEN (products.brand_id = 1093) THEN $floor_multiplier_flash_furniture * products.price
						 WHEN (products.brand_id = 26) THEN $floor_multiplier_tiger_chef * products.price
						 WHEN (products.brand_id = 676)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN $floor_over_30_multiplier_johnson_rose * products.price
								ELSE $floor_under_30_multiplier_johnson_rose * products.price 
								END
						WHEN (products.brand_id = 104)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN $floor_over_30_multiplier_town * products.price
								ELSE $floor_under_30_multiplier * products.price 
								END								
						WHEN (products.brand_id = 200)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN ($floor_over_30_multiplier * products.price) + $cdn_extra_fee
								ELSE ($floor_under_30_multiplier * products.price) + $cdn_extra_fee 
								END								
						WHEN (products.brand_id = 673)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN ($floor_over_30_multiplier * products.price) + $bunn_extra_fee
								ELSE ($floor_under_30_multiplier * products.price) + $bunn_extra_fee 
								END	
						WHEN (products.brand_id = 1135)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN 
									CASE WHEN (products.price < 50)
									THEN ($floor_over_30_multiplier * products.price) + $calmil_extra_fee
									ELSE ($floor_over_30_multiplier * products.price)
									END
								ELSE ($floor_under_30_multiplier * products.price) + $calmil_extra_fee 
								END									
						WHEN (products.brand_id = 91)
							 THEN 
								CASE WHEN (products.price > 30)
								THEN ($floor_over_30_multiplier * products.price)									
								ELSE
									CASE WHEN (products.price <= 25)
									THEN  $floor_under_30_multiplier * products.price + $crestware_extra_fee
									ELSE $floor_under_30_multiplier * products.price
									END 
								END																
						ELSE 
							CASE WHEN (products.price > 30)
							THEN $floor_over_30_multiplier * products.price
							ELSE $floor_under_30_multiplier * products.price
							END
					END
				END
			END AS floorPrice,
			
			CASE WHEN (products.map_price > 0 AND products.vendor_price < products.map_price AND products.brand_id = 26)
				THEN products.map_price
				ELSE
					CASE WHEN ($fba_cost_string > 0)
						THEN 
						CASE WHEN ($fba_cost_string > 30) THEN $ceiling_over_30_multiplier * $fba_cost_string
						ELSE $ceiling_under_30_multiplier * $fba_cost_string
						END				
					WHEN (product_options.vendor_sku IS NOT NULL)
					THEN
					CASE WHEN (product_options.additional_price > 30)
						THEN $ceiling_over_30_multiplier * product_options.additional_price
						ELSE $ceiling_under_30_multiplier * product_options.additional_price
						END
					ELSE
						CASE WHEN (products.price > 30)
						THEN $ceiling_over_30_multiplier * products.price
						ELSE $ceiling_under_30_multiplier * products.price
						END
					END 
				END AS ceilPrice		   
		
		FROM (`products` , amazon_inventory_feed)
		LEFT JOIN product_options ON product_options.product_id = products.id
		WHERE ((product_options.vendor_sku IS NOT NULL AND product_options.vendor_sku = sku AND product_options.is_active = 'Y' AND product_options.is_deleted = 'N')
		OR (product_options.vendor_sku IS NULL AND products.vendor_sku = sku))		
		AND amazon_inventory_feed.quantity >0 AND products.is_deleted = 'N' AND products.brand_id<> 26
		GROUP BY the_vendor_sku 
		ORDER BY products.is_deleted asc, products.is_active desc)
		UNION
		
		(SELECT '1' as is_fba, if(product_options.vendor_sku IS NOT NULL, product_options.vendor_sku,
		products.vendor_sku) AS the_vendor_sku, if(product_options.vendor_sku IS NOT NULL, product_options.asin, products.asin) AS the_asin, 
		if (products.map_price > 0, products.map_price, if ($fba_cost_string > 0, $floor_multiplier_fba * $fba_cost_string,$floor_multiplier_fba*if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price))) AS floorPrice,
		CASE WHEN (products.map_price > 0 AND products.vendor_price < products.map_price AND products.brand_id = 26)
				THEN products.map_price
				ELSE 
		(if($fba_cost_string > 0,
			if ($fba_cost_string > 30, $ceiling_over_30_multiplier * $fba_cost_string,
			$ceiling_under_30_multiplier * $fba_cost_string), 
		if(if(product_options.vendor_sku IS NOT NULL, 
		product_options.additional_price,products.price) > 30, 
			$ceiling_over_30_multiplier * if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price), 
			$ceiling_under_30_multiplier * if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price))))
			END
			 AS ceilPrice
		FROM (`products` , amazon_fba_inventory)
		LEFT JOIN product_options ON product_options.product_id = products.id
		WHERE ((product_options.vendor_sku IS NOT NULL AND product_options.vendor_sku = sku AND product_options.is_active = 'Y' AND product_options.is_deleted = 'N')
		OR (product_options.vendor_sku IS NULL AND products.vendor_sku = sku))		
		AND amazon_fba_inventory.afn_fulfillable_quantity >0
		AND products.is_deleted =  'N' AND products.brand_id<> 26	
		GROUP BY the_vendor_sku
		ORDER BY products.is_deleted asc, products.is_active desc) 		
		
		UNION
		
		(SELECT '1' as is_fba, if(product_options.vendor_sku IS NOT NULL, product_options.mfr_part_num,
		products.mfr_part_num) AS the_vendor_sku, if(product_options.vendor_sku IS NOT NULL, product_options.asin, products.asin) AS the_asin, 
		if (products.map_price > 0, products.map_price, if ($fba_cost_string > 0, $floor_multiplier_fba * $fba_cost_string,$floor_multiplier_fba*if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price))) AS floorPrice, 
		CASE WHEN (products.map_price > 0 AND products.vendor_price < products.map_price AND products.brand_id = 26)
				THEN products.map_price
				ELSE 
		(if($fba_cost_string > 0,
			if ($fba_cost_string > 30, $ceiling_over_30_multiplier * $fba_cost_string,
			$ceiling_under_30_multiplier * $fba_cost_string), 
		if(if(product_options.vendor_sku IS NOT NULL, 
		product_options.additional_price,products.price) > 30, 
			$ceiling_over_30_multiplier * if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price), 
			$ceiling_under_30_multiplier * if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price))))
			END
			 AS ceilPrice
		FROM (`products` , amazon_fba_inventory)
		LEFT JOIN product_options ON product_options.product_id = products.id
		WHERE ((product_options.vendor_sku IS NOT NULL AND product_options.mfr_part_num = sku AND product_options.is_active = 'Y' AND product_options.is_deleted = 'N')
		OR (product_options.vendor_sku IS NULL AND products.mfr_part_num = sku))
		AND amazon_fba_inventory.afn_fulfillable_quantity >0
		AND products.is_deleted =  'N' AND products.brand_id<> 26
		GROUP BY the_vendor_sku
		ORDER BY products.is_deleted asc, products.is_active desc) 		
		
		UNION
		
		(SELECT '0' as is_fba,
        amazon_warehouse_skus.warehouse_sku AS the_vendor_sku,
        amazon_warehouse_skus.asin AS the_asin,
        if (products.map_price > 0, products.map_price, if ($fba_cost_string > 0,
        if($fba_cost_string > 30, $floor_multiplier_warehouse * $fba_cost_string,$floor_multiplier_warehouse * $fba_cost_string),
         if(product_options.vendor_sku IS NOT NULL,
          if(product_options.additional_price > 30,$floor_multiplier_warehouse * product_options.additional_price,$floor_multiplier_warehouse * product_options.additional_price) ,
          if(products.price > 30, $floor_multiplier_warehouse * products.price, $floor_multiplier_warehouse * products.price)))) AS floorPrice,
        if($fba_cost_string > 0 ,if ($fba_cost_string > 30, $ceiling_multiplier_warehouse * $fba_cost_string,
            $ceiling_multiplier_warehouse * $fba_cost_string), if(if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price) > 30, $ceiling_multiplier_warehouse * products.price,
            $ceiling_multiplier_warehouse * if(product_options.vendor_sku IS NOT NULL, product_options.additional_price,products.price))) AS ceilPrice
        FROM (`products` , amazon_inventory_feed, amazon_warehouse_skus)
        LEFT JOIN product_options ON (product_options.id = amazon_warehouse_skus.product_option_id)
        WHERE ((product_options.vendor_sku IS NOT NULL AND product_options.is_active = 'Y' AND product_options.is_deleted = 'N')
        OR (product_options.vendor_sku IS NULL))
        AND amazon_inventory_feed.quantity >0 AND products.is_deleted = 'N' AND products.brand_id<> 26
		AND amazon_warehouse_skus.product_id = products.id AND amazon_warehouse_skus.warehouse_sku = amazon_inventory_feed.sku
        GROUP BY the_vendor_sku
        ORDER BY products.is_deleted asc, products.is_active desc)
        
		ORDER BY is_fba desc, the_vendor_sku asc
		";
		//mail("rachel@tigerchef.com", "channelmax sql", "$sql");
		$ret = db_query_array($sql);
		return $ret;
	}
	
	static function put_feed_via_ftp($ftp_user_name, $ftp_user_pass, $file)
	{
		$conn_id = ftp_connect($ftp_server);
		
		// login with username and password
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
		
		// upload a file
		if (ftp_put($conn_id, $remote_file, $file, FTP_ASCII)) {
			echo "Successfully uploaded $file\n";
		} else {
			echo "There was a problem while uploading $file\n";
		}
		
		// close the connection
		ftp_close($conn_id);
	}
}
?>