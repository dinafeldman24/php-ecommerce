<?php

class Cart {

	// Anything stored in this data array will be stored automatically in the session.
	// i.e. $cart->data['billing_zip'] = 12345;
	// echo $cart->data['billing_zip'];
	public $data; // misc session data

	public function __construct(){

		if($_SESSION['cart_data']){
			$this->data =& $_SESSION['cart_data'];
		} else {
			$_SESSION['cart_data'] = array();
			$this->data =& $_SESSION['cart_data'];

			$this->init();
		}



	}

	public function init(){
		$this->data['shipping_method'] = "GND"; // UPS Ground
		self::updateFreight();
		//self::generateCartReference();
	}

      function cmp($a, $b)
      {
	if ($a['ship_date']->unixTime == $b['ship_date']->unixTime)
	  return 0;

	return ($a['ship_date']->unixTime < $b['ship_date']->unixTime) ? -1 : 1;
      }


	// if $default_date is set, dont check off the cheapest shipping option.
	function presentNewRow($product_id, $default_name = '', $default_qty = 1, $default_date = '', $date_error_msg = '', $is_perishable = false, $product='')
	{

	global $use_interface;

	?>
	<tr>
		<td colspan="2">
		  <a name='_down'></a>
			<table border=0 cellspacing=0 cellpadding=0 width="100%">
			<tr valign=top>

				<!-- shipping options -->

				<td width="40%" style='padding-right:10px; border-right:0px solid #F08109'>
					<br>
					<table border=0 width="100%">
					<tr>
						<td bgcolor="#FBDFC1" align="center" class="boldGrey" colspan=4>Shipping:</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<!-- <td><b style="color: #333333;">Method:</b></td> -->
						<td><b style="color: #333333;"><nobr>Delivered By:&nbsp;&nbsp;&nbsp;</nobr></b></td>
						<td><b style="color: #333333;">Price:</b></td>
					</tr>


					<?

					$list = Products::getShippingList($product_id);

					//print_Ar($list);

					$cnt = 1;
					if ($list) foreach ($list as $l) {

						//if ($cnt == 1 && !$default_date)
						//	$checked = 'checked';
						//else
							$checked = "";

						echo "<tr>
								<td colspan=4 height=1 bgcolor=#FBEBD6></td>
							  </tr>";

						echo "<tr>
								<td><input type='radio' name='_new_shipping' value='$l[id]' id='_shipping_new_$l[id]' $checked onClick='clear_text(\"_new_future_date\"); '></td>
								<!-- <td class=small><label for='_shipping_new_$l[id]'>$l[name]</label></td> -->
								<td class=small>".(($l['days'] != -1) ? date('M. d', $l[deliver_date]->unixTime) : $l[name])."</td>
								<td class=small>\$$l[price]</td>
							  </tr>";


						$cnt++;
					}

					if ($is_perishable) {
						?>
						<!--
						<tr>
							<td colspan=4 style='padding-top:10px'>
							  <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#CCCCCC">
							  <tr>
								<td class="small"><div align="center">Please
									note that this is
									a perishable item
									and requires expedited
									shipping. </div></td>
							  </tr>
							</table>
							</td>
						</tr>-->

					<? } ?>

					<tr><td colspan=3>
					<div style="background-color: #FFFFFF; width:100%; padding-top:10px;" ><div align="center" class="boldGrey" style='background-color:#FBDFC1'>Important Notes:</div>
					<ul>
							<li>
								Shipping charges are calculated per item.
							</li>
							<li>
								The �Delivered By� date is for this item only and changes daily after

								<?
								 $time = new TimeWrapper($product['shipping_cutoff_time']);
								 echo date('g:iA', ($time->getUnix()));

								?>
								EST.
							</li>
							<li>
								To ensure the quality of our perishible items, many of our products cannot be in transit over weekends and holidays. Those orders placed from Wednesday�s cut-off through Friday will only offer expedited shipping options.
							</li>
							<li>
								Shipping to Hawaii & Alaska is not available.
							</li>
							<!--
								<li>
								If using the �Future Arrival Date� option, you will be charged the least expensive shipping method available for that date. Your purchase will arrive on or before the selected date.
							</li>
							-->
						</ul>
					</div>
					</td></tr>



					<!--
					<tr>
					<td colspan='2' style='padding-left:5px;'>
					<span class="boldOrangeProduct"><label for='_new_future_radio'>Future date:</label></span>
					</td>
					</tr>
					<tr>
						<td><input type='radio' name='_new_shipping' value='future'  id='_new_future_radio'></td>
						<td colspan=3>

								<?= Calendar800::calendarSelect('_new_future_date',$default_date, true, 'm/d/Y','MM/dd/yyyy', 'document.the_form._new_shipping[document.the_form._new_shipping.length-1].checked=true;') ?>
						</td>
					</tr>
					<tr>
						<td colspan=4 class="productSmall">Click on Calendar for options</td>
					</tr>
					-->
					<?

					if ($date_error_msg) {
						?>
						<tr>
							<td colspan='4' class='error_msg' style='border:1px solid #F08109; padding:5px' bgcolor='#FAE8DD'>
							     <a name='to_error'></a><b><?= $date_error_msg ?></b>
							  </td>
						</tr>
						<?
					}

					?>
					</table>


				</td>



				<!-- recipient name/qty -->

				<td width="50%" style='padding-left:10px'><br>


					<? if ($use_interface == 'simple') { ?>

					<table border=0 cellpadding=5 cellspacing=0>
					<tr>
						<td class="boldOrangeProduct">recipient name:</td>
						<td><input type="text" name="_new_name" value="<?=$default_name?>" size=10 maxlength=50></td>
					</tr>
					<tr>
						<td class="boldOrangeProduct">quantity:</td>
						<td><input type="text" name="_new_qty" value="<?=$default_qty?>" size=3 maxlength=2></td>
					</tr>
					<tr>
					<td></td>
					<td>


					<?

					  if ($_SESSION['callcenter'])
					    $js_param = 'true';
					  else
					    $js_param = 'false';


					?>

					<input type="image" src="/images/buy.gif" width="100" height="20" alt="Add This Item to Your Cart" border=0 onClick='return validateProductForm(<?=$js_param?>);'>




					</td>
					</tr>
					</table>




					<br>

					<? include('guarantee_suggest.inc.php') ?>

					<br>


					<? } else { ?>

					<script type="text/javascript" language="javascript">

					function checkForm(max_checkbox_index)
					{

						//alert(1);

						var pass = false;

						if (document.the_form._new_name.value && document.the_form._new_qty.value) {

							//alert(2);
							pass = true;

						} else {

							//alert('loop');

						  for (var i = 0; i < max_checkbox_index; i++) {

						  	if (document.getElementById('lm_list_' + i).checked) {
						  		pass = true;
						  		break;
						  	} else {
						  		// nothing
						  	}

						  }

						  if (!pass) {
						  	alert("Please select a recipient from EZShop, or select a name for an individual recipient.");
						  }

						  return pass;

						}
					}

					</script>

					<table border=0 width=90% cellspacing="0" align=center style='border:1px solid #F08109;' cellpadding=4>
					<tr><td colspan=5 bgcolor="#FBDFC1"><span class="boldOrangeProduct"><u>Add from your EZShop Address Book</u><sup>TM</sup>:</span></td></tr>
					<tr>
					<td><table width=100% border=0 cellspacing=0 cellpadding=4>


					<tr>
					<td width=50><b>Jump to:</b></td>
					<td align='center'><a href='#__A' class='underline'>A-G</a></td>
					<td align='center'><a href='#__H' class='underline'>H-M</a></td>
					<td align='center'><a href='#__N' class='underline'>N-S</a></td>
					<td align='center'><a href='#__T' class='underline'>T-Z</a></td>
					</tr>

					<tr><td colspan=5><a href='#' class='underline' onClick='setBigDivVisibility("visible"); setBigDivLocation("/listmanager.php?action=add_entry_popup&noheader=1&nofooter=1&nosidebar=1"); ;return false;'><b>Add to your address book</b></a></td></tr>

					</table>


					<div style='overflow:auto; height:160px; border:1px solid orange; padding:7px' >

					<?

						$people = ListManager::getEntries(0, $_SESSION['list_mgr']['id'], 'entry_name');

						?> <table width=100% border=0> <?

						$current_letter = "";

						$cnt = 0;

						if ($people) foreach ($people as $p) {

							$l = strtoupper(substr($p[entry_name],0,1));

							if ($l != $current_letter) {

								$new_letter = true;

								$current_letter = $l;

							} else {

								$new_letter = false;
							}

							// for some reason i tried moving all the styles out to classes, and IE butchers it.  Not sure why.
							if ($new_letter) {
								echo "<tr><td height=5></td></tr>";
								echo "<tr><td colspan='3'><span style='background-color:#FBDFC1; border: 1px solid #F08109;'>&nbsp;<a name='__$current_letter' style='color:#F08109;'><b>$current_letter</b>&nbsp;</span><hr class='listmanager'></td></tr>";
							}

							if (@array_key_exists($p['id'], $_REQUEST['lm'])) $checked='checked'; else $checked = "";

							echo "<tr><td width=20><input type='checkbox' id='lm_list_$cnt' $checked name='lm[$p[id]]'></td><td><label for='$p[id]'>$p[entry_name]</td><td align=center width=75><a href='#' onClick=\"setDivLocation('/listmanager.php?action=lm_popup&id=$p[id]&noheader=1&nofooter=1&nosidebar=1'); setDivVisibility('visible'); return false;\"><small>[view]</small></a><br><a href='#' onClick=\"setDivLocation('/listmanager.php?action=lm_popup_history&id=$p[id]&noheader=1&nofooter=1&nosidebar=1'); setDivVisibility('visible'); return false;\"><small>[history]</small></a></td></tr>";

							//print_ar($p);

							$cnt++;

						}

						echo $cnt;

						?> </table> <?

					?>



					</div>

					</td>
					</tr>
					<tr>

					<td>
					&nbsp;<input type="image" src="/images/buy.gif" width="100" height="20" alt="Add This Item to Your Cart" border=0 onClick='return checkForm(<?=$cnt?>);'>
					</td>

					</tr>
					</table>

					<br>

					<center>
					<img src='/pics/or.gif'>
					</center>

					<br>

					<table border=0 cellpadding=4 cellspacing=0 width=90% align=center style='border:1px solid #F08109;'>
					<tr><td colspan=5 bgcolor="#FBDFC1"><span class="boldOrangeProduct"><u>Add an individual recipient</u>:</span></td></tr>
					<tr>
						<td class="boldOrangeProduct">recipient name:</td>
						<td><input type="text" name="_new_name" value="<?=$default_name?>" size=10 maxlength=50></td>
						<td class="boldOrangeProduct">quantity:</td>
						<td><input type="text" name="_new_qty" value="<?=$default_qty?>" size=3 maxlength=2></td>
					</tr>

					<tr>
					  <td align=left>&nbsp;<input type="image" src="/images/buy.gif" width="100" height="20" alt="Add This Item to Your Cart" border=0 onClick='return checkForm(<?=$cnt?>);'></td>
					</tr>

					</table>

					<br>

					<? } ?>

				</td>




			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<?

	}

	function AddCustomerId(){
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$query = " UPDATE cart SET  customer_id =  '$cust_acc_id' 
				WHERE  customer_id = 0 
				AND  session_id =  '".session_id()."'";
		db_query_array($query);
		$rows = mysql_affected_rows();
		
		//check that not entering duplicate promo code
		$sql ="SELECT promo_code_id FROM  cart_promo_codes WHERE customer_id = '$cust_acc_id'";
		$previous_promos = db_query_array($sql);
		foreach ($previous_promos as $promo){
			$promos .= $promo['promo_code_id'];
		}
		
		$query = " UPDATE cart_promo_codes SET customer_id =  '$cust_acc_id' 
				WHERE  customer_id = 0 
				AND  session_id =  '".session_id()."'
				AND promo_code_id NOT IN ('".rtrim($promos,',')."')";
		db_query_array($query);
		
		self::updateCartReference();

		//returns true if customer id was added to items in the cart from current session
		return ($rows > 0 ? true : false) ;
	}
		
	function getCustomerItems(){
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		$query = " SELECT * FROM cart
				WHERE cart.customer_id = '" . $cust_acc_id . "' ";
		return db_query_array($query);
	}

function getExistingCartID($product_id, $package_id = 0, $option_id = 0, $is_gift = '', $selected_options = '', $presets = '')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($option_id != 0 || !empty($selected_options) || !empty($presets))
		{
            $available_options_clause = (empty($selected_options))? ' isnull(available_options_id) ' : ' available_options_id = \'' . $selected_options . '\'';
            $preset_clause = empty($presets) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . json_encode($presets) . "'";
			$query = "SELECT cart.id FROM cart,cart_options WHERE
						cart_id = cart.id AND  
					product_id = '$product_id' AND
					package_id = '$package_id' 
					AND product_option_id = '$option_id'
                    AND $available_options_clause
                    AND $preset_clause ";
			if ($is_gift == 'gift') $query .= " AND cart.is_gift = 'Y'";
			else $query .= " AND cart.is_gift = 'N'";
			$query .= " AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";		
					
		}
		else 
		{
			$query = "SELECT id FROM cart WHERE
					product_id = '$product_id' AND
					package_id = '$package_id'";
			if ($is_gift == 'gift') $query .= " AND cart.is_gift = 'Y'";
			else $query .= " AND cart.is_gift = 'N'";
			$query .="
					AND (session_id = '".session_id()."'";
			if($cust_acc_id  && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";	
		}
		$result = db_query_array($query);

		if(is_array($result) && count($result)) return $result[0]['id'];
		
		return 0;
	}
		
	function getTotalCartQtyForProduct ($product_id, $package_id = 0, $option_id = 0, $selected_options='', $presets = '')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($option_id != 0 || !empty($selected_options) || !empty($presets))
		{
            $available_options_clause = empty($selected_options) ? 'isnull(available_options_id)' : "available_options_id = '" . $selected_options . "'"; 
            $preset_clause = empty($presets) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . json_encode($presets) . "'";
			$query = "SELECT sum(qty) as sum_qty FROM cart, cart_options WHERE
					cart_id = cart.id AND
					product_id = '$product_id' AND
					package_id = '$package_id' AND
					product_option_id = '$option_id'
                    AND $available_options_clause
                    AND $preset_clause 
	                   AND (session_id = '".session_id()."'";
           if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";	
		}
		else 
		{
			$query = "SELECT sum(qty) as sum_qty FROM cart WHERE
					product_id = '$product_id' AND
					package_id = '$package_id'
					AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";	
		}
		$result = db_query_array($query);


		if(is_array($result) && count($result)) return $result[0]['sum_qty'];

		return 0;
	}
	
	function getNumLinesForProduct ($product_id, $package_id = 0, $option_id = 0, $selected_options = '', $presets= '')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($option_id != 0 || !empty($selected_options) || !empty($presets))
		{
            $available_options_clause = empty($selected_options)? 'isnull(available_options_id)' : "available_options_id='" . $selected_options . "'";
            $preset_clause = empty($presets) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . json_encode($presets) . "'";
			$query = "SELECT count(*) as num_lines FROM cart, cart_options WHERE
					cart_id = cart.id AND
					product_id = '$product_id' AND
					package_id = '$package_id' AND
					product_option_id = '$option_id'
                    AND $available_options_clause
                    AND $preset_clause ";
  			$query .= " AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";	
		}
		else 
		{
			$query = "SELECT count(*) as num_lines FROM cart WHERE
					product_id = '$product_id' AND
					package_id = '$package_id' 
					AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
				$query .= " ) LIMIT 1";	
		}
		$result = db_query_array($query);


		if(is_array($result) && count($result)) return $result[0]['num_lines'];

		return 0;
	}
	
	function getExistingCartProduct($product_id, $package_id = 0, $option_id = 0, $is_gift = '', $selected_options='', $presets = '')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($option_id != 0 || !empty($selected_options) || !empty($presets))
		{
            $available_options_clause = empty($selected_options)? 'isnull(available_options_id)' : "available_options_id='" . $selected_options . "'";
            $preset_clause = empty($presets) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . json_encode($presets) . "'";
			$query = "SELECT cart.qty, cart_price FROM cart,cart_options WHERE
			cart_id = cart.id AND
			product_id = '$product_id' AND
			package_id = '$package_id'
			AND product_option_id = '$option_id'
             AND  $available_options_clause
             AND $preset_clause ";
			if ($is_gift == 'gift') $query .= " AND cart.is_gift = 'Y'";
			else $query .= " AND cart.is_gift = 'N'";
			$query .= " AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$query .= " )
						ORDER BY cart_price ASC  ";
	
		}
		else
		{
			$query = "SELECT cart.qty, cart_price FROM cart WHERE
			product_id = '$product_id' AND
			package_id = '$package_id'";
			if ($is_gift == 'gift') $query .= " AND cart.is_gift = 'Y'";
			else $query .= " AND cart.is_gift = 'N'";
			$query .="
					AND (session_id = '".session_id()."'";
			if($cust_acc_id  && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$query .= " )
						ORDER BY cart_price ASC  ";
		}
		$result = db_query_array($query);
		return $result;
	}

	function add($product_id,$qty,$product_options_id='',$package_id=0, $package_uid=0, $recipient_name = '', $shipping_id = 0, $ship_date = '', $deliver_date = '', $add_type = '', $list_mgr_entry_id = '', $cat_id = 0, $occasion_id = 0, $accessories='', $selected_options='', $customizations = 0, $presets = '')
	{
	    global $CFG;
	    global $cart;
	    
	   	if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
	    if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
	    
	    $product = Products::get1( $product_id );
        $is_essensa = ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember());
	     
		$can_buy_info = Products::getCanBuyInfo($product, $CFG);
    	$can_buy = $can_buy_info['can_buy'];
    	$can_buy_limit = $can_buy_info['can_buy_limit'];
    	
		$qty = (int) $qty;
		$error = false;
		//echo $can_buy_limit;
		if( $can_buy_limit  || !$can_buy )
		{
			$prev_qty = Cart::getTotalCartQtyForProduct($product_id,$package_id,$product_options_id, $selected_options, $presets);
			$qty = $prev_qty + $qty ;
		    if( $qty > $can_buy_limit && $can_buy_limit)
		    {	
				$qty = $can_buy_limit - $prev_qty;
				if($can_buy_limit==1){
					$is_are = "is";
					$have_has = " has";
				}
				else{
					$is_are = "are";
					$have_has = " have";
				}
				if($qty<1){
					$qty = 0;
					$error = "There ".$is_are." currently only " . $can_buy_limit . " " . $product['name'] . " available. You already have " . $can_buy_limit . " in your cart.";
				}
				else{
				$error = array(	'type' 	=> 'max_qty',
								'msg'	=> "There ".$is_are." currently only " . $can_buy_limit . " " . $product['name'] . " available. " . $can_buy_limit . $have_has ." been added to your cart.",
								'qty'	=> $qty);
				}
		    }
		    else if( !$can_buy ) {
			$qty = 0;
			$error = "There are currently no more " . $product['name'] . " available.";
		    }
		}
		
		if($can_buy_limit){ $product['min_qty'] = 0; }
		if($product['min_qty']>$qty){
			$prev_qty = Cart::getTotalCartQtyForProduct($product_id,$package_id,$product_options_id, $selected_options, $presets);
			
			if($product['min_qty'] > $prev_qty+$qty){
				$qty = $product['min_qty'];
				$error = array(	'type' 	=> 'min_qty',
								'msg'	=> "This item has a minimum order qty of " . $product['min_qty'] . ". Your cart has been adjusted",
								'qty'	=> $qty);
			}
		}

		if (!$qty || ($qty > $can_buy_limit && $can_buy_limit))
		    return $error;

	/*	if( !is_array( $product_options ) && $product_options )
			$product_options = array( $product_options );
*/            
		// check for item in cart
		if($package_id == 0 ){
			//This needs to take the option id into account, if the options dont match, its a different product.
			//if( is_numeric( $product_options_id ) && $product_options_id > 0)
			if( (is_numeric( $product_options_id ) &&( $product_options_id > 0) || !empty($selected_options)) || !empty($presets))
            {
                $message .= ' in if is_numeric or not empaty ' . PHP_EOL;
				$prev_id = Cart::getExistingCartID($product_id, $package_id, $product_options_id, $add_type, $selected_options, $presets);
                $message .= 'there is a prev id';
                if($product_options_id){
                if ($is_essensa){
                    $price_per_pc = ProductOptionCache::getProductOptions($product_options_id, $product_id, 'total_essensa_price');
                }
                else{
                         $price_per_pc = ProductOptionCache::getProductOptions($product_options_id, $product_id, 'total_price');
                    } 
                }
                else{
                    $prices_to_print_array = Products::getPriceToDisplayForProduct($product['id']);
                    if($prices_to_print_array['sale_price_to_print']){
                        $price_per_pc = $sale_price_to_print = $prices_to_print_array['sale_price_to_print'];
                }
                    else $price_per_pc = $prices_to_print_array['reg_price_to_print'];
                }
            }

			else 
			{
				$prev_id = Cart::getExistingCartID($product_id, 0, 0,$add_type);
				$product_data = Products::get1($product_id);
		 		//if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $product_data['essensa_price'] != '0.00' && $product_data['essensa_price'] < $product_data['price'])
				//{
					//$price_per_pc = $product_data['essensa_price'];
				//}				
				//else $price_per_pc = $product_data['price'];
				$prices_to_print_array = Products::getPriceToDisplayForProduct($product['id']);
				if ($prices_to_print_array['sale_price_to_print'])
				{
					$price_per_pc = $sale_price_to_print = $prices_to_print_array['sale_price_to_print'];
				}
				else $price_per_pc = $prices_to_print_array['reg_price_to_print'];				
			}
            if(!empty($selected_options) && $selected_options != ','){
                //going to add addtional price calculations here
                $price_per_pc += Products::getPriceForOptionalOptions($selected_options, $is_essensa, $product['id'], $presets);
            }
			if ($prev_id && $add_type != 'gift')
			{
              $num_lines = Cart::getNumLinesForProduct ($product_id, $package_id,$product_options_id, $selected_options, $presets);

				if( $can_buy_limit || !$can_buy ) {
					$query = "UPDATE cart SET qty = $qty
						WHERE id = '$prev_id'
						AND (session_id = '".session_id()."'";
					if($cust_acc_id && $logged_in_time!='checkout'){
						$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
					}
					$query .= " ) ";
					
					db_query($query);
				}
				else if ($num_lines > 1)
				{
					$sum_qty = Cart::getTotalCartQtyForProduct ($product_id, $package_id, $product_options_id,$selected_options, $presets);
					
					// delete all other lines
					if ((is_numeric( $product_options_id ) && $product_options_id) || !empty($selected_options) || !empty($presets))
					{
                        $available_options_clause = (empty($selected_options))? ' isnull(available_options_id) ' : ' available_options_id = \'' . $selected_options . '\'';
                        $preset_clause = empty($presets) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . json_encode($presets) . "'";

						$del_query_2 = "DELETE FROM cart USING cart, cart_options 
                        WHERE cart.id = cart_options.cart_id AND cart.id <> '$prev_id' 
                        AND product_id = '$product_id' 
                        AND cart_options.product_option_id = '$product_options_id' 
                        AND $available_options_clause
                        AND $preset_clause
                        AND session_id = '".session_id()."'";
						db_query($del_query_2);					
						$del_query_1 = "DELETE FROM cart_options WHERE cart_id <> '".$prev_id."' AND cart_options.product_option_id = '".$product_options_id."'";
						db_query($del_query_1);
					}
					else 
					{
						$del_query_2 = "DELETE FROM cart
								WHERE cart.id <> '$prev_id'
								AND product_id = '$product_id'
								AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$del_query_2 .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$del_query_2 .= " ) ";
						db_query($del_query_2);					
					}
					$update_query = "UPDATE cart SET qty = $qty + $sum_qty, cart_price = '$price_per_pc'
							WHERE id = '$prev_id'
							AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$update_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$update_query .= " ) ";			 					
					db_query($update_query) or die(mysql_error());
				}
				else {
					$update_query = "UPDATE cart SET qty = qty + $qty
							WHERE id = '$prev_id'
							AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$update_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$update_query .= " ) ";	
					db_query($update_query);
				}
				$updated = true;
			}
			if($prev_id && $add_type == 'gift')	{
				$updated = true;
			}		
		}
		$product = Products::get1( $product_id );

		//$cart_price = Products::getProductMarkup($product);
		//if($cart_price === false){
		//	return false;
		//}

		if($add_type =='gift'){
			$price_per_pc = 0.00;	
		}
		
		if(!$updated){
			//$cart_price = $product['price'];
			$info = Array(
					'product_id' => $product_id,
					'package_id' => $package_id,
					'package_uid' => $package_uid,
					'qty' => $qty,
					'session_id' => session_id(),
					'cat_id' => $occasion_id ? 0 : $cat_id,
					'occasion_id' => $occasion_id,
					'cart_price'  => $price_per_pc
			);
			if($cust_acc_id && $logged_in_time!='checkout'){
				$info['customer_id'] = $cust_acc_id ;
			}
			if($add_type =='gift'){
				$info['is_gift'] = 'Y';
			}

			$cart_id = db_insert('cart',$info,'date_added');
			
// 			if($product['shipping_type'] == 'Freight'){
// 				$cart->data['freight_data']['freight'] ++;
// 			} else {
// 				$cart->data['freight_data']['standard'] ++;
// 			}
			self::updateFreight();
			
		}


		// Insert Accessories If There
		if($_REQUEST['accessories']){
			$accs = explode(',',$_REQUEST['accessories']);
			if($accs)foreach($accs as $acc_id){

				$prod_acc = Products::get1($acc_id);

				// Accessory exists in Cart? Update It
				if($prev_id = Cart::getExistingCartID($acc_id)){
					db_query("UPDATE cart SET qty = qty + $qty WHERE id = '$prev_id' ");
				} else {
					$info = Array(
						'product_id' => $acc_id,
						'package_id' => $package_id,
						'package_uid' => $package_uid,
						'qty' => $qty,
						'session_id' => session_id(),
						'cat_id' => $occasion_id ? 0 : $cat_id,
						'occasion_id' => $occasion_id,
						'cart_price'  => $prod_acc['price']
					);
					if($cust_acc_id && $logged_in_time!='checkout'){
						$info['customer_id'] = $cust_acc_id ;
					}
					
					$cart_idx = db_insert('cart',$info,'date_added');
				}
			}
		}
		if (!$updated && ((is_numeric($product_options_id) && $product_options_id > 0)|| !empty($selected_options) || !empty($presets))) {

				$info = Array(
					'cart_id' => $cart_id,
					'product_option_id' => $product_options_id
				);
                if(!empty($selected_options)){
                    $info['available_options_id'] = $selected_options;
                }

                if($customizations){
                    $info['user_input'] = json_encode($customizations);
                }
                if(!empty($presets)){
                    $info['preset_value_ids'] = json_encode($presets);
                }
				$cart_option_id = db_insert('cart_options',$info);
                //if any of the options have related products, add them to cart
                //this is for items that should be one per cart line, not multiplied 
                //by quantity of items
                $related_products = ProductOptions::getRelatedProductForAvailableOptions($product_options_id, $selected_options, $product_id);
                foreach($related_products as $curr){
                    $num_in_cart =Cart::getTotalCartQtyForProduct ($curr['product_id'], $package_id, $curr['option_id']); 
                    if($num_in_cart < 1){
                        self::add($curr['product_id'], 1, $curr['option_id']);
                    }
                }
		}

		if( $error ){
			if(is_array($error)){
				$error['cart_id'] = ($cart_id) ? $cart_id : $prev_id ;
				return json_encode($error);
			}else{
				return $error;
			}
		}
		
		$this->cart_split_lines();
		    
		return ($cart_id) ? $cart_id : $prev_id ;
	}
	
	function cart_split_lines()
	{
		 if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$cart_lines = $this->get();
		$message = print_r($cart_lines, true);
		//mail("rachel@tigerchef.com", "the print r", $message);
		foreach ($cart_lines as $the_line)	// each sku is already only 1 line	
		{			
            if(is_array($the_line['options'])){
                $product_option_id = $the_line['options'][0]['product_option_id'];
            }
			// see if the sku has case_discount = 'Y'
			if ($the_line['case_discount'] == 'Y')
			 {
			 	$num_full_cases = floor($the_line['qty'] / $the_line['num_pcs_per_case']);
			 	$num_individual_pcs = $the_line['qty'] % $the_line['num_pcs_per_case'];
			 	$product_data = Products::get1($the_line['product_id']);
			
			 	if ($num_full_cases > 0 || $num_individual_pcs > 0)
			 	{ 
			 		if (is_array($the_line['options']))
					{
                        $the_option = $the_line['options'][0];
                        $available_options_clause = (empty($the_option['available_options_id']))? ' isnull(available_options_id) ' : ' available_options_id = \'' . $the_option['available_options_id'] . '\'';
                        $preset_clause = empty($the_option['preset_value_ids']) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . $the_option['preset_value_ids'] . "'";
						$del_query_1 = "DELETE from cart_options WHERE cart_id = '".$the_line['id']."' 
                        AND cart_options.id = '".$the_line['options'][0]['id']."'
                        AND $available_options_clause
                        AND $preset_clause";
						db_query($del_query_1);
					}
					$del_query_2 = "DELETE from cart
							where cart.id = '".$the_line['id']."'
							AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$del_query_2 .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$del_query_2 .= " ) ";	
					db_query($del_query_2);					
			 	}
			 	if (is_array($the_line['options']))
			 	{
                    $is_essensa = ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember());
                $sale = Sales::get(0, true, $the_line['product_id'], '', $product_option_id);
                if(!is_null($sale)){
                    $price_per_pc = $sale[0]['sale_price'];
                }
                else{
                    if ($is_essensa){
                        $price_per_pc = ProductOptionCache::getProductOptions($product_option_id, $the_line['product_id'], 'essensa_price');
                    }
                    else{
                        $price_per_pc = ProductOptionCache::getProductOptions($product_option_id, $the_line['product_id'], 'price');
                    }
                }
			 	}
			 	else 
			 	{		
			 		if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $product_data['essensa_price'] != '0.00' && $product_data['essensa_price'] < $product_data['price'])
					{
						$price_per_pc = $product_data['essensa_price'];
					}				
					else $price_per_pc = $product_data['price'];	 							
			 	}
			 	if ($num_full_cases > 0)
			 	{
			 		$info = Array(
						'product_id' => $the_line['product_id'],
						'package_id' => $the_line['package_id'],
						'package_uid' => $the_line['package_uid'],
						'qty' => $num_full_cases * $the_line['num_pcs_per_case'],
						'session_id' => session_id(),
						'cat_id' => $the_line['cat_id'],
						'occasion_id' => $the_line['occasion_id'],
						'cart_price'  => $price_per_pc * (1- $product_data['case_price_percent'])
					);
					if($cust_acc_id){
						$info['customer_id'] = $cust_acc_id ;
					}

					$cart_id = db_insert('cart',$info,'date_added');
					if (is_array($the_line['options']))
					{
                        $the_option = $the_line['options'][0];
						$info2 = Array(
							'cart_id' => $cart_id,
							'product_option_id' => $product_option_id
						);
                        if(!empty($the_option['available_options_id'])){
                            $info2['available_options_id'] = $the_option['available_options_id'];
                        }
                        if(!empty($the_option['user_input'])){
                            $info2['user_input'] = $the_option['user_input'];
                        }
                        if(!empty($the_option['preset_value_ids'])){
                            $info2['preset_value_ids'] = $the_option['preset_value_ids'];
                        }
						db_insert('cart_options',$info2);
					}
			 	}
			 	if ($num_individual_pcs > 0)
			 	{
			 		$info = Array(
						'product_id' => $the_line['product_id'],
						'package_id' => $the_line['package_id'],
						'package_uid' => $the_line['package_uid'],
						'qty' => $num_individual_pcs,
						'session_id' => session_id(),
						'cat_id' => $the_line['cat_id'],
						'occasion_id' => $the_line['occasion_id'],
						'cart_price'  => $price_per_pc
					);
					if($cust_acc_id){
						$info['customer_id'] = $cust_acc_id ;
					}
					$cart_id = db_insert('cart',$info,'date_added');
					if (is_array($the_line['options']))
					{
                        $the_option = $the_line['options'][0];
						$info2 = Array(
							'cart_id' => $cart_id,
							'product_option_id' => $the_line['product_option_id']
						);
                        if(!empty($the_option['available_options_id'])){
                            $info2['available_options_id'] = $the_option['available_options_id'];
                        }
                        if(!empty($the_option['user_input'])){
                            $info2['user_input'] = $the_option['user_input'];
                        }
                        if(!empty($the_option['preset_value_ids'])){
                            $info2['preset_value_ids'] = $the_option['preset_value_ids'];
                        }
						db_insert('cart_options',$info2);
					}
					
			 	}
			 }
			 else 
			 {
				// do nothing!
				//mail("rachel@tigerchef.com", "doing nothing", $the_line['product_id']);
			 }
		}
	}


	function addPackage($package_id,$products,$qty,$options,$stock_level='')
	{ 
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];

		$qty = (int) $qty;
		if (!$qty) return false;

		$info = Array(
			'qty' => $qty,
			'package_id' => $package_id,
			'session_id' => session_id(),
			'stock_level' => $stock_level
		);
		if($cust_acc_id){
				$info['customer_id'] = $cust_acc_id ;
			}

		$pkg_uid = db_insert('cart_packages',$info);

		foreach ($products as $product_id) {
			$this->add($product_id,1,$options[$product_id],$package_id,$pkg_uid);
		}

		return true;
	}

	function update($qtys)
	{
	    global $CFG;
	    
	    if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
	    if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];	
			
		if (!is_array($qtys)) return false;


		$cart_info = Cart::get('','','id');
		//mail('tova@tigerchef.com', 'update query2', var_export($qtys,true) . '\n' . var_export($cart_info,true));
		
		$error = "";
		$combined_lines = array();
		foreach ($qtys as $cart_id => $info) {
			
			if ($info['qty'] == 0) {
				$this->delete($cart_id);
				continue;
			}
			$qty = (int) $info['qty'];
			
            $option = $cart_info[$cart_id]['options'][0];
			// if we already combined this sku's qtys
			if (is_array( $cart_info[$cart_id]['options'] ) && in_array($cart_info[$cart_id]['product_id'] . " - " . $option['id'] . " - " . $option['available_options_id'] . ' - ' . $option['preset_value_ids'],$combined_lines))
			{
                        $available_options_clause = empty($option['available_options_id'] )? 'isnull(available_options_id)' : "available_options_id='" . $option['available_options_id']. "'";
                        $preset_clause = empty($option['preset_value_ids']) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . $option['preset_value_ids'];
				$update_query = "UPDATE cart, cart_options SET qty = qty + $qty
						WHERE cart.id = cart_options.cart_id
						AND product_id = '".$cart_info[$cart_id]['product_id']."'
						AND cart_options.product_option_id = '".$option['option_id']."' 
                        AND $available_options_clause 
                        AND $preset_clause
                        AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$update_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$update_query .= " ) ";
				db_query($update_query) or die(mysql_error());				
				continue;
			}
			else if (in_array( $cart_info[$cart_id]['product_id'] ,$combined_lines))
			{				
				$update_query = "UPDATE cart SET qty = qty + $qty
						WHERE product_id = '".$cart_info[$cart_id]['product_id']."'
						AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$update_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$update_query .= " ) ";
				db_query($update_query) or die(mysql_error());				
				continue;
			}
			
			
			$product = Products::get1( $cart_info[$cart_id]['product_id'] );
//			print_ar( $product );

			$can_buy_info = Products::getCanBuyInfo($product, $CFG);
    		$can_buy = $can_buy_info['can_buy'];
    		$can_buy_limit = $can_buy_info['can_buy_limit'];
    		
    		$qty = (int) $qty;
			
			if( $can_buy_limit || !$can_buy )
			{
			    if( $qty > $can_buy_limit && $can_buy_limit)
			    {	$qty = $can_buy_limit;

				$error .= "<div>There are currently only " . $can_buy_limit . " " . $product['name'] . " available. " . $can_buy_limit . " have been added to your cart.</div>";
			    }
			    else if( !$can_buy ) {
				$qty = 0;
				$error .= "<div>There are currently no more " . $product['name'] . " available.</div>";
			    }
			}
			
			if($can_buy_limit){ $product['min_qty'] = 0; }
			if($product['min_qty']>$qty){
			
				$qty = $product['min_qty'];
				$error .= "<div>The minimum order quantity for " . $product['name'] . " is " . $product['min_qty'] . ". ". $product['min_qty'] . " have been added to your cart.</div>";
			}

			if (!$qty) continue;
			
			$num_lines = Cart::getNumLinesForProduct ( $cart_info[$cart_id]['product_id'] ,  $cart_info[$cart_id]['package_id'] ,$option['product_option_id'], $option['available_options_id'], $option['preset_value_ids']);
		
			if ($num_lines > 1)
				{
					$sum_qty = Cart::getTotalCartQtyForProduct ($cart_info[$cart_id]['product_id'], $cart_info[$cart_id]['package_id'], $option['product_option_id'], $option['available_options_id'], $option['preset_value_ids']);
					// delete all other lines
					
					if (is_array( $cart_info[$cart_id]['options'] ))
					{
                        $available_options_clause = empty($option['available_options_id'] )? 'isnull(available_options_id)' : "available_options_id='" . $option['available_options_id']. "'";
                        $preset_clause = empty($option['preset_value_ids']) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . $option['preset_value_ids'];
						$del_query_2 = "DELETE FROM cart USING cart, cart_options
								WHERE cart.id = cart_options.cart_id
								AND cart.id <> '$cart_id' AND product_id = '".$cart_info[$cart_id]['product_id']."'
								AND cart_options.product_option_id = '".$option['product_option_id']."' 
                                AND  $available_options_clause
                                AND $preset_clause
								AND (session_id = '".session_id()."'";
								if($cust_acc_id && $logged_in_time!='checkout'){
									$del_query_2 .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
								}
								$del_query_2 .= " ) ";
						db_query($del_query_2);					
						$del_query_1 = "DELETE FROM cart_options WHERE cart_id <> '".$cart_id."' AND cart_options.id = '".$option['id']."'";
						db_query($del_query_1);
						$combined_lines[] = $cart_info[$cart_id]['product_id'] . " - " . $option['id'] . " - " . $option['available_options'] . ' - ' . $option['preset_value_ids'];
					}
					else 
					{
						$del_query_2 = "DELETE FROM cart
								WHERE cart.id <> '$cart_id'
								AND product_id = '".$cart_info[$cart_id]['product_id']."'
								AND (session_id = '".session_id()."'";
								if($cust_acc_id && $logged_in_time!='checkout'){
									$del_query_2 .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
								}
								$del_query_2 .= " ) ";
						db_query($del_query_2);
						$combined_lines[] = $cart_info[$cart_id]['product_id'];					
					}
					
					
					$update_query = "UPDATE cart SET qty = $qty
							WHERE id = '$cart_id'
							AND (session_id = '".session_id()."'";
							if($cust_acc_id && $logged_in_time!='checkout'){
								$update_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
							}
							$update_query .= " ) ";			 					
					db_query($update_query) or die(mysql_error());
				}
				else {
					$query = "UPDATE cart SET qty = '$qty'
						WHERE id = '$cart_id'
						AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$query .= " ) ";	
					db_query($query);
				}						
		}
		
		//mail('tova@tigerchef.com', 'update query', $update_query . $qty . $error);
		
		$this->cart_split_lines();
		return $error;
	}

	function updatepackage($qtys)
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if (!is_array($qtys)) return false;

		foreach ($qtys as $cart_id => $qty) {

			if (!$cart_id) continue;

			if (!$qty) {
				$this->delete($cart_id);
				continue;
			}

			$qty = (int) $qty;

			if (!$qty) continue;
			
			$query = "UPDATE cart_packages SET qty = $qty
				WHERE id = $cart_id
				AND (session_id = '".session_id()."'";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$query .= " OR cart_packages.customer_id = '" . $cust_acc_id . "' ";
				}
				$query .= " ) ";	
			db_query($query);
		}

		return true;
	}

	function delete($id)
	{
		global $CFG;
		
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
		
		$id = (int) $id;
		if (!$id) return false;
		
		// with new case pricing, first see all rows with that sku and option
		// and delete them all!
		$query = "SELECT * FROM cart LEFT JOIN cart_options on cart.id = cart_options.cart_id
			WHERE cart.id = $id 
			AND (session_id = '".session_id()."'";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$query .= " ) ";	
		
		$this_cart_item_array = db_query_array ($query);
		$this_cart_item = $this_cart_item_array[0];
		
		if ($this_cart_item['product_option_id'] || !empty($this_cart_item['available_options_id']) || !empty($this_cart_item['preset_value_ids']))
		{
			// do a delete from options AND cart if there's an option id
            $available_options_clause = empty($this_cart_item['available_options_id'] )? 'isnull(available_options_id)' : "available_options_id='" . $this_cart_item['available_options_id'] . "'";
            $preset_clause = empty($this_cart_item['preset_value_ids']) ? 'isnull(preset_value_ids)' : "preset_value_ids='" . $this_cart_item['preset_value_ids'] . "'";
			$option_del_query = "DELETE cart.*, cart_options.* FROM cart, cart_options
						WHERE cart.id = cart_options.cart_id
						AND product_id = '".$this_cart_item['product_id'] ."'
						AND product_option_id = '".$this_cart_item['product_option_id']."' 
                         AND $available_options_clause
                         AND $preset_clause
                        AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$option_del_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$option_del_query .= " ) ";	

			$ret = db_query($option_del_query) or die(mysql_error());
            self::removeOnePerCartProduct($this_cart_item['available_options_id'], session_id(), $this_cart_item['product_id']);
		}
		// otherwise, delete from cart where that product_id is there
		else
		{
			$cart_del_query = "DELETE FROM cart
						WHERE product_id = '".$this_cart_item['product_id'] ."'
						AND (session_id = '".session_id()."'";
						if($cust_acc_id && $logged_in_time!='checkout'){
							$cart_del_query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
						}
						$cart_del_query .= " ) ";	
			db_query($cart_del_query) or die(mysql_error());
		}				
		
		/*	
		db_query("DELETE FROM cart
				  WHERE id = $id AND session_id = '".session_id()."'");

		if (db_affected_rows() > 0) {
			db_query("DELETE FROM cart_options WHERE cart_id = $id");
		}*/

		return true;
	}
	
	function deleteGifts(){
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		
		//option query
		$query = "DELETE cart.*, cart_options.* FROM cart, cart_options
					WHERE cart.id = cart_options.cart_id
					AND (session_id = '".session_id()."'";
		if($cust_acc_id){
			$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
		}
		$query .= " ) AND cart.is_gift = 'Y' ";
			
		//echo $query;
		
		db_query($query) or die(mysql_error());
		
		//no option query
		$query = "DELETE cart.* FROM cart
					WHERE (session_id = '".session_id()."'";
		if($cust_acc_id){
			$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
		}
		$query .= " ) AND cart.is_gift = 'Y' ";
			
		//echo $query;
		
		db_query($query) or die(mysql_error());
	}

	function emptyCart($cart_reference='')
	{
	    global $cart;
		
	    if($cart_reference){
			$refernce_info = self::get1CartReference($cart_reference); 
			$session_id = $refernce_info['session_id'];
			$cust_acc_id = $refernce_info['customer_id'];
		} else
		{
			$session_id = session_id() ;
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
		
		//echo "CART EMPTIED";
		//exit();
		$query = "SELECT id FROM cart
				WHERE (session_id = '".$session_id."'";
				if($cust_acc_id){
					$query .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$query .= " ) ";	
		$result = db_query_array($query);

		if (is_array($result)) {
			$keys = array();
			foreach ($result as $row) {
				$keys[] = $row['id'];
			}

			$str_keys = implode(',',$keys);

			db_query("DELETE FROM cart
				  	  WHERE id IN ($str_keys)");

			db_query("DELETE FROM cart_options
				  	  WHERE cart_id IN ($str_keys)");
		}
		
		$query = "DELETE FROM cart_promo_codes
			WHERE (session_id = '".$session_id."'";
			if($cust_acc_id){
				$query .= " OR cart_promo_codes.customer_id = '" . $cust_acc_id . "' ";
			}
		$query .= " ) ";	
		$result = db_query_array($query);
		
		$query = "DELETE FROM cart_reference
					WHERE cart_reference = '".$cart_reference."'";
		$result = db_query_array($query);

		//unset( $cart );
		unset( $_SESSION['cart_data'] );
		self::init();
	}

	function deletePackage($id)
	{

		$id = (int) $id;

    	$recs = db_query_array("SELECT * FROM cart
				  WHERE package_uid = $id");

    	// yes, I know this is lousy code... mmkay thanks.

    	if (is_array($recs))
    	  foreach ($recs as $rec)
    	    Cart::delete($rec['id']);

		return true;
	}



	function getPackageLine($id)
	{

		$sql = "SELECT cart_packages.*,
					packages.name AS package_name,
					packages.price,
					packages.main_product_id,
		            cart_packages.id AS package_uid
				FROM cart_packages
				LEFT JOIN packages ON packages.id = cart_packages.package_id
				WHERE cart_packages.id = $id";


		$tmp = db_query_array($sql,'',true);



		// added by jaimie.

		// SUM(additional_price)
		// , product_options WHERE product_options.product_id = cart.product_id AND
		$q = "SELECT SUM(additional_price) AS addit from cart, cart_options, product_options WHERE cart.package_uid = $id AND cart_options.cart_id = cart.id AND product_options.id = cart_options.product_option_id";
		//echo $q;
		$res = db_query_array($q);
		//echo "<pre>";
		//print_r($res);
		//echo "</pre>";
		$tmp['price'] += $res[0]['addit'];
		return $tmp;

	}

	function count($cart_info)
	{
		$count = 0;

		if ($cart_info) foreach ($cart_info as $row) {
			if ($row['package_uid'] == 0) {
				$count++;
			}
			else if (!$packages[$package_uid]) {
				$count++;
				$packages[$package_uid] = 1;
			}
		}

		return $count;
	}

	function get($order = '', $order_asc = '', $key='',$whichProds = "all",$whichDiscount = "",$coupon_code = "",$cart_reference='',$paypal_token='')
	{		
		$session_id = session_id();
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];	
		
		if($paypal_token){
			$paypal_token_info = PaypalCc::getSessionInfo($paypal_token);					
			$session_id = $paypal_token_info['session_id'];
			if(!$cust_acc_id && $paypal_token_info['customer_id']){ $cust_acc_id = $paypal_token_info['customer_id']; }
		}
		
		//LEFT JOIN brands ON brands.id = products.brand_id
		// SELECT occasions.id as occasion_id, occasions.name as occasion_name
		// LEFT JOIN occasions ON (occasions.id = cart.occasion_id)
		$sql = "SELECT cart.*,
					products.name AS product_name,
					products.price,
					products.weight,
					products.mfr_part_num,
					products.shipping_type,
					products.is_promo_allowed,
					products.free_shipping,
					products.case_discount,
					(products.price * (1 - products.case_price_percent)) AS case_price_per_piece,
					products.num_pcs_per_case,
					products.allow_po_box,
					cats.id AS cat_id,cats.name AS cat_name,
					cart_options.product_option_id, products.brand_id,					
                    cart_options.is_optional, cart_options.available_options_id
				FROM cart
				LEFT JOIN products ON products.id = cart.product_id
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				LEFT JOIN cats ON cats.id = cart.cat_id
				LEFT JOIN cart_options ON cart.id = cart_options.cart_id ";
		if($cart_reference){
			$sql .= "LEFT JOIN cart_reference ON ((cart.session_id = cart_reference.session_id ) OR (cart.customer_id >0 AND cart_reference.checkout_login = 'N' AND cart.customer_id = cart_reference.customer_id)) ";
		}							
		
		$sql .= "WHERE 1 ";

		if($cart_reference){
			$sql .= " AND cart_reference.cart_reference = '$cart_reference' ";
		} else {
			$sql .= " AND ( cart.session_id = '".$session_id."' ";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$sql .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$sql .= " ) ";
		}
		
		// if it's "all", which is the default, don't add any clause
		if ($whichProds != "all")
		{
			if (!$whichDiscount) $whichDiscount = "total";
			if ($whichDiscount == "total") $discountField = "qualifiesForDiscount";
			else if ($whichDiscount == "shipping") $discountField = "qualifiesForShippingDiscount";
		
			if ($whichProds == "qualifying") 
			{
				if ($coupon_code != "") $sql .= " AND $discountField like '%$coupon_code%'";
				else $sql .= " AND $discountField != ''";
			}
			else if ($whichProds == "nonQualifying") 
			{
				if ($coupon_code != "") $sql .= " AND $discountField not like '%$coupon_code%'";
				else $sql .= " AND $discountField = ''";
			}
			else if($whichProds == "not_po_box_allowed"){
				$sql .= " AND (products.allow_po_box = 'N' || products.shipping_type = 'Freight' || products.weight > 100) ";
			}
		}
		$sql .= "GROUP BY cart.id ";

		if (!$order)
		{
		  //$order = "cart.date_added";
		  $order = "cart.product_id, product_option_id";		  
		  //$order = "cart.product_id"; 
		}
		//$sql .= db_orderby($order, $order_asc);
		
		$sql .= " ORDER BY $order $order_asc";
// 		mail("tova@tigerchef.com","cart4", var_export($sql,true));
//echo $sql; 
		$cart = db_query_array($sql, $key);

		if (!is_array($cart)) return false;


		foreach ($cart as $key => $line) {

//echo PHP_EOL . 'the current product: ' . $line['product_name'];
/*			$sql = "SELECT product_options.id,vendor_sku,value,additional_price,options.name AS option_name, product_options.mfr_part_num
					FROM cart_options
						LEFT JOIN product_options ON product_options.id = cart_options.product_option_id
						LEFT JOIN options ON options.id = product_options.option_id
					WHERE cart_options.cart_id = $line[id]
					ORDER BY product_options.order_fld";*/
            $sql = "SELECT id, product_option_id, is_optional,available_options_id, user_input, preset_value_ids FROM cart_options  WHERE cart_id = $line[id]";
			$options = db_query_array($sql);

			$prod = Products::get1( $cart[$key]['product_id'] );

			if (is_array($options) && count($options))
			{
			    $cart[$key]['options'] = $options;
				$cart[$key]['orig_price'] = $cart[$key]['price'];

				foreach ($options as $option)
				{
                    $prod_option_id = 0;
                    if($option['product_options_id']){
                        $prod_option_id = $option['product_options_id'];    }
                     elseif($option['product_option_id']){
                        $prod_option_id = $option['product_option_id'];
                     }
                     if($prod_option_id){
					$prod_option = Products::getProductOption( $prod_option_id );
					$cart[$key]['price'] = $prod_option['additional_price'];
					$prod['price'] = $prod_option['additional_price'];
					$prod['final_price'] = $prod_option['final_price'];

					$price = Products::getProductMarkup( $prod_option );
				}
			}
			}
			else
			{
				$price = Products::getProductMarkup( $prod );
               // echo 'there are no options' . PHP_EOL; 
			}

			//$cart[$key]['price'] = $price;
			$cart[$key]['line_total'] = $cart[$key]['cart_price'] * $line['qty']; //$cart[$key]['price'] * $line['qty'];

		}
		
		return $cart;
	}
	
	function get1CartItem($id){
		$sql = "SELECT * from cart
		WHERE id = $id ";
		return db_get1(db_query_array($sql));
	}
	

    function showProductLineA($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;
		global $cart;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
	
	if( $cart_line['options'] )
	{
	    //Only 1 option can be selected
	    $option = $cart_line['options'][0];
	    $vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
	}

        ?>

        <div class="cart_box">
	    <?
	    if ($show_title_box)
	    {
        ?>
        <div class="cart_title_box">
            <span class="columns_1">Item</span>
            <span class="columns_2">Details</span>
            <span class="columns_3">Quantity</span>
            <span class="columns_4">Price Each</span>
            <span class="columns_5">Total</span>
        </div>
        <?
	    }

	    ?>

            <div class="cart_box_inner">
                <div class="img_box">
                <? if ($product_info['is_active'] == 'Y' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>"><? } ?><img src="<?=$product_image_link?>" alt="<?=$product_info['mfr_part_num']?>" /><?
                 if ($product_info['is_active'] == 'Y' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?></a><? } ?></div>
                    <div class="details_box">
                        <h4>
                        <? if ($product_info['is_active'] == 'Y' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>"><? } ?>ITEM # (<?=$vsku ? $vsku : $product_info['mfr_part_num']?>)<? if ($product_info['is_active'] == 'Y' && !Products::isOnePerCartProduct($cart_line['producut_id'])) {?></a><? } ?></h4>
                        <p><span><?=str_replace('�','&reg;',$product_info['name'])?></span> <br />by <?=$brand['name']?><br />
			<?
	        if (strpos($product_info['promo_msg'], "do not qualify for free shipping") !== false) echo "<span class='no_free_ship'>This item does not qualify for free shipping.</span>";
			if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
				echo "<div class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</div>";
			}
			$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
			if($ships_in){
				echo "<div class='ships_in'>";
				if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
					echo "Once in stock this item ships in ";
				}else{
					echo "Ships in ";
				}
				echo $ships_in . "</div>";
			}
			if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
				echo "<div class='freight'>";
				if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
					echo "Ships freight";
				}else{
					echo "Ships via standard carrier";	
				}
				echo "</div>";
			}

			?>

        <?
    }
    function showProductLineBReg($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
        
		if( $cart_line['options'] )
		{
	    	//Only 1 option can be selected
	    	$option = $cart_line['options'][0];
	    	$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
		}

/*    	if($brand['min_amount'] > 0 && $brand['min_amount'] > $cart_line['line_total'] && $product_info['exclude_min'] == 'N'&& !Deals::seeIfCurrentNotSoldOutDealForProd ($cart_line['product_id']))
        {
            echo '<span style="color:red;font-weight:bold;">* This product is subject to a  ' . $brand['name'] .  ' minimum charge. ($' . $brand['min_amount'] . ' minimum) <br/></span>';
        }*/
        if(is_array($brand_discount))
        {
            echo '*<span style="color:#000000">Get ' . number_format($brand_discount[0]['discount'], 0) . '% off when purchasing $' . number_format($brand_discount[0]['amount'],0) . ' + of ' . $brand['name'] . ' products.<br /></span>';
        }
    	if ($product_info['case_discount'] == 'Y')
		{
			 	$full_case_only = ($cart_line['qty'] % $product_info['num_pcs_per_case'] == 0) ? true: false;
			 	if ($full_case_only)             echo '*<span style="color:#000000">Case discount applied.<br /></span>'; 
		}
         ?>
          </p>
          </div>
          <div class="quantity_text">
          	<?php if($cart_line['is_gift'] == 'N'){?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=$cart_line['qty']?>" class="input_box" type="text" <?=($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : ''?>/><b><a href="javascript:void(0);" onClick="remove_cart_line_item(<?=$cart_line['id']?>); return false;">Remove</a></b>
          	<?php }else{ ?>
          		<?=$cart_line['qty']?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=$cart_line['qty']?>" class="input_box" type="hidden" >
          	<?php }?>
          </div>
          <span class="columns_span">$<?=number_format($cart_line['cart_price'],2)?></span>
          <label class="columns_label">$<?=number_format($cart_line['line_total'],2)?></label>
          </div>
        </div>
        <? 
    }
    function showProductLineBCase($prev_cart_line, $cart_line)
    {
    	global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= ($cart_line['line_total'] + $prev_cart_line['line_total']) * ($brand_discount[0]['amount']*.01);
        
		if( $cart_line['options'] )
		{
	    	//Only 1 option can be selected
	    	$option = $cart_line['options'][0];
	    	$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
		}
/*    	if($brand['min_amount'] > 0 && $brand['min_amount'] > ($cart_line['line_total'] + $prev_cart_line['line_total']) && $product_info['exclude_min'] == 'N' && !Deals::seeIfCurrentNotSoldOutDealForProd ($cart_line['product_id']))
        {
            echo '<span style="color:red;font-weight:bold;">* This product is subject to a  ' . $brand['name'] .  ' minimum charge. ($' . $brand['min_amount'] . ' minimum) <br/></span>';
        }*/
        if(is_array($brand_discount))
        {
            echo '*<span style="color:#000000">Get ' . number_format($brand_discount[0]['discount'], 0) . '% off when purchasing $' . number_format($brand_discount[0]['amount'],0) . ' + of' . $brand['name'] . ' products.<br /></span>';
        }
         ?>
          </p>
          </div>
          <div class="quantity_text">
          	<?php if($cart_line['is_gift'] == 'N') {?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=($cart_line['qty'] + $prev_cart_line['qty'])?>" class="input_box" type="text" <?=( $cart_line['product_id'] == $CFG->gift_wrap_product_id)  ? 'disabled' : ''?>/><b><a href="javascript:void(0);" onClick="remove_cart_line_item(<?=$cart_line['id']?>);  return false;">Remove</a></b>
          	<?php } else { ?>
          		<?=($cart_line['qty'] + $prev_cart_line['qty'])?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=($cart_line['qty'] + $prev_cart_line['qty'])?>" class="input_box" type="hidden" >
          	<?php }?>
          </div>
          <span class="columns_span"><?=$cart_line['qty'] . " @ $" . number_format($cart_line['cart_price'], 2) ." ea. <br>" .  $prev_cart_line['qty'] . " @ $" . number_format($prev_cart_line['cart_price'],2) . " ea."?></span>
          <label class="columns_label">$<?=number_format(($cart_line['line_total'] + $prev_cart_line['line_total']),2)?></label>
          </div>
        </div>
        <?
    }
static function generateProductLineForFloatingCart($cart_line, $show_title_box = false, $type="")
    {
        global $CFG;
	global $cart;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
        
	if( $cart_line['options'] )
	{
	    //Only 1 option can be selected
	    $option = $cart_line['options'][0];
	    $vsku = $option['mfr_part_num'] . ' - ' . $option['value'];

	}

      $return_string = "<div class='cart_box_floating'>";
	    
	    if ($show_title_box)
	    {
        	$return_string .= '
        	<div class="cart_title_box_floating">
            <span class="columns_1_floating">Item # / </span>';
        	
       		$return_string .= '<span class="columns_2_floating">Details</span>
            	<span class="columns_3_floating">Quantity</span>
            	<span class="columns_4_floating">Price Each</span>
            	<span class="columns_5_floating">Total</span>
        		</div>';
	    }
        $return_string .= '
            <div class="cart_box_inner_floating">';
        	$return_string .= '        <div class="img_box_floating">
                <a href="'.$product_link.'">        
         		<img src="'.$product_image_link.'" alt="'.$product_info['mfr_part_num'].'" width="90" height="90"/>
                </a></div>';
			$details_class = "details_box_floating";
        $return_string .= '<div class="'.$details_class.'">
                        <h4>';
		$return_string .= '<a href="'.$product_link.'">';
        $return_string .= 'ITEM # (';
        $return_string .= $vsku ? $vsku : $product_info['mfr_part_num'];
        $return_string .= ')';
        $return_string .= '</a>';
        $return_string .= '</h4>
                        <p><span>'.
        str_replace('�','&reg;',$product_info['name']);
	
	if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
		$return_string .= "<div class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</div>";
	}
	$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
	if($ships_in){
		$return_string .= "<div class='ships_in'>";
		if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
			$return_string .= "Once in stock this item ships in ";
		}else{
			$return_string .= "Ships in ";
		}
		$return_string .= $ships_in . "</div>";
	}
	if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
		$return_string .= "<div class='freight'>";
		if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
			$return_string .= "Ships freight";
		}else{
			$return_string .= "Ships via standard carrier";	
		}
		$return_string .= "</div>";
	}
	
	
        //$return_string .='</span> <br />by '.$brand['name'].'<br />';
                        
                        /*if($brand[0]['min_amount'] > 0 && $brand[0]['min_amount'] > $cart_line['line_total'] && $product_info['exclude_min'] == 'N')
                        {
                            $return_string .= '<span style="color:red;font-weight:bold;">* This product is subject to a  ' . $brand[0]['name'] .  ' minimum charge. ($' . $brand[0]['min_amount'] . ' minimum) <br/></span>';
                        }
                        if(is_array($brand_discount))
                        {
                            $return_string .= '*<span style="color:#000000">Get ' . number_format($brand_discount[0]['discount'], 0) . '% off when purchasing $' . number_format($brand_discount[0]['amount'],0) . ' + of' . $brand['name'] . ' products.<br /></span>';
                        }*/
         $return_string .= '
                        </p>
                    </div>
                    
                <div class="quantity_text_floating">';
         //$return_string .= '<input name="cart['.$cart_line[id].'][qty]" value="'.
         //mail("rachel@tigerchef.com", "return string", $return_string);
     return $return_string;   
         
    }
    function showProductLineBRegFloating($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {    
        global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
        
		if( $cart_line['options'] )
		{
	    	//Only 1 option can be selected
	    	$option = $cart_line['options'][0];
	    	$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
		}
    

		     $return_string .= $cart_line['qty'];
         //$return_string .= '" class="input_box" type="text" ';
         //$return_string .= ($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : '';
         //$return_string .= '/><b></b>';
         $return_string .= '</div>
                <span class="columns_span_floating">$'.number_format($cart_line['cart_price'],2).'</span>
                <label class="columns_label_floating">$'.number_format($cart_line['line_total'],2).'</label>
            </div>
        </div>';
	
         //echo $return_string." is ret string";
         return $return_string;         
    }
    
    function showProductLineBCaseFloating($prev_cart_line, $cart_line)
    {
    	global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= ($cart_line['line_total'] + $prev_cart_line['line_total']) * ($brand_discount[0]['amount']*.01);
        
		if( $cart_line['options'] )
		{
	    	//Only 1 option can be selected
	    	$vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
		}
		$return_string .= ($cart_line['qty'] + $prev_cart_line['qty']);
         //$return_string .= '" class="input_box" type="text" ';
         //$return_string .= ($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : '';
         //$return_string .= '/><b></b>';
         $return_string .= '</div>
                <span class="columns_span_floating">'.$cart_line['qty'] . ' @ $' . number_format($cart_line['cart_price'], 2) .' ea. <br>' .  $prev_cart_line['qty'] . ' @ $' . number_format($prev_cart_line['cart_price'],2) . ' ea.</span>
                <label class="columns_label_floating">$'.number_format($cart_line['line_total'] + $prev_cart_line['line_total'],2).'</label>
            </div>
        </div>';
         //echo $return_string." is ret string";
         return $return_string;
    }
    static function generateProductLineForCheckoutCart($cart_line, $show_title_box = false, $type="")
    {
    	global $CFG;
	global $cart;
    
    	$product_info       = Products::get1($cart_line['product_id']);
    	$product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
    	$product_image_link = Catalog::makeProductImageLink($product_info['id']);
    	$vsku               = $product_info['mfr_part_num'];
    	$brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
    	$brand              = Brands::get1($product_info['brand_id']);
    	$line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
    
    	if( $cart_line['options'] )
    	{
    		//Only 1 option can be selected
    		$option = $cart_line['options'][0];
    		$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
    
    	}
    
    	$return_string = "<div class='cart_box'>";
    	 
    	if ($show_title_box)
    	{
    		$return_string .= '
    		<div class="cart_title_box">
    		<span class="columns_1">Item</span>';
    		 
    		$return_string .= '
    			<span class="columns_2">&nbsp;</span>
    			<span class="columns_3">Quantity</span>
    			<span class="columns_4">Price Each</span>
    			<span class="columns_5">Total</span>
    			</div>';
    		
    	}
    	$return_string .= '
    	<div class="cart_box_inner">';
		
    	$details_class = "details_box_wider";
    	$return_string .= '<div class="'.$details_class.'">
    	<h4>';
    	
    	$return_string .= 'ITEM # (';
    	$return_string .= $vsku ? $vsku : $product_info['mfr_part_num'];
    	$return_string .= ')';
    	
    	$return_string .= '</h4>
    	<p><span>'.
    	str_replace('�','&reg;',$product_info['name']);
	if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
		$return_string .= "<div class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</div>";
	}
	$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
	if($ships_in){
		$return_string .= "<div class='ships_in'>";
		if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
			$return_string .= "Once in stock this item ships in ";
		}else{
			$return_string .= "Ships in ";
		}
		$return_string .= $ships_in . "</div>";
	}
	if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
		$return_string .= "<div class='freight'>";
		if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
			$return_string .= "Ships freight";
		}else{
			$return_string .= "Ships via standard carrier";	
		}
		$return_string .= "</div>";
	}
    	//$return_string .='</span> <br />by '.$brand['name'].'<br />';
    
    	/*if($brand[0]['min_amount'] > 0 && $brand[0]['min_amount'] > $cart_line['line_total'] && $product_info['exclude_min'] == 'N')
    	 {
    	$return_string .= '<span style="color:red;font-weight:bold;">* This product is subject to a  ' . $brand[0]['name'] .  ' minimum charge. ($' . $brand[0]['min_amount'] . ' minimum) <br/></span>';
    	}
    	if(is_array($brand_discount))
    	{
    	$return_string .= '*<span style="color:#000000">Get ' . number_format($brand_discount[0]['discount'], 0) . '% off when purchasing $' . number_format($brand_discount[0]['amount'],0) . ' + of' . $brand['name'] . ' products.<br /></span>';
    	}*/
    	$return_string .= '
    	</p>
    	</div>
    
    	<div class="quantity_text">';
    	//$return_string .= '<input name="cart['.$cart_line[id].'][qty]" value="'.
    
    	return $return_string;
    	 
    }
    function showProductLineBRegCheckout($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
    	global $CFG;
    
    	$product_info       = Products::get1($cart_line['product_id']);
    	$product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
    	$product_image_link = Catalog::makeProductImageLink($product_info['id']);
    	$vsku               = $product_info['mfr_part_num'];
    	$brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
    	$brand              = Brands::get1($product_info['brand_id']);
    	$line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
    
    	if( $cart_line['options'] )
    	{
    		//Only 1 option can be selected
    		$option = $cart_line['options'][0];
    		$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
    	}
    
    
    	$return_string .= $cart_line['qty'];
    	//$return_string .= '" class="input_box" type="text" ';
    	//$return_string .= ($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : '';
    	//$return_string .= '/><b></b>';
    	$return_string .= '</div>
    	<span class="columns_span">$'.number_format($cart_line['cart_price'],2).'</span>
    	<label class="columns_label">$'.number_format($cart_line['line_total'],2).'</label>
    	</div>
    	</div>';
    	//echo $return_string." is ret string";
    	return $return_string;
    }
    
    function showProductLineBCaseCheckout($prev_cart_line, $cart_line)
    {
    	global $CFG;
    
    	$product_info       = Products::get1($cart_line['product_id']);
    	$product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
    	$product_image_link = Catalog::makeProductImageLink($product_info['id']);
    	$vsku               = $product_info['mfr_part_num'];
    	$brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
    	$brand              = Brands::get1($product_info['brand_id']);
    	$line_brand_discount= ($cart_line['line_total'] + $prev_cart_line['line_total']) * ($brand_discount[0]['amount']*.01);
    
    	if( $cart_line['options'] )
    	{
    		//Only 1 option can be selected
    		$option = $cart_line['options'][0];
    		$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
    	}
    	$return_string .= ($cart_line['qty'] + $prev_cart_line['qty']);
    	//$return_string .= '" class="input_box" type="text" ';
    	//$return_string .= ($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : '';
    	//$return_string .= '/><b></b>';
    	$return_string .= '</div>
    	<span class="columns_span">'.$cart_line['qty'] . ' @ $' . number_format($cart_line['cart_price'], 2) .' ea. <br>' .  $prev_cart_line['qty'] . ' @ $' . number_format($prev_cart_line['cart_price'],2) . ' ea.</span>
    	<label class="columns_label">$'.number_format($cart_line['line_total'] + $prev_cart_line['line_total'],2).'</label>
    	</div>
    	</div>';
    	//echo $return_string." is ret string";
    	return $return_string;
    }
    
	function showPackageMainLine($package_line,$color_bg = false)
	{

		$package_info = Array(
			'id' => $package_line['package_id'],
			'name' => $package_line['package_name']
		);

?>
	  <tr style="background-color: #f8f8f8;">
	  	<td class=cart_rowl style="background-color: #f8f8f8;">
	  	<?= '<a style="font-weight: bold;" href="'.Catalog::makePackageLink($package_info).'">"'.$package_line['package_name'].'" Package</a>' ?>
	  	</td>
		<td class=cart_row>
	  	<a href="javascript:deleteFromCart(<?=$package_line[id]?>, true)" class=cart_remove>Remove</a>
	  	</td>
	  	<td class=cart_row>$<?=number_format($package_line['price'],2)?></td>
	  	<td class=cart_rowr>
	  	$<?= number_format($package_line['line_total'], 2)?>
	  	</td>
	  </tr>
<?

	}

	function showPackageLine($cart_line,$color_bg = false)
	{
		global $CFG;

		$product_info = Products::get1($cart_line['product_id']);
		$url = Catalog::makeProductLink_($product_info, $product_info['cat_id'], $product_info['occasion_id']);

		$options = '';

		if (is_array($cart_line['options'])) {

			foreach ($cart_line['options'] as $option) {
				$options .= "$option[name]: $option[value]<br>";
			}
		}

?>
	  <tr>
	  	<td class=cart_rowl style="padding-left: 30px;">

	  		<a href='<?=$url?>'><img src='/itempics/<?=$cart_line['product_id']?><?=$CFG->home_suffix?>.jpg' border=0 align="left" style="margin: 0px 10px;"></a>
	  		<a href='<?=$url?>' style='font-weight: bold;'><?=$product_info['name']?></a><br>
	  		<?= $product_info['mfr_part_num'] ?><br>
	  		<?= $options ?>

	  	</td>

	  	<td class=cart_row>1</td>
	  	<td class=cart_row>--</td>
	  	<td class=cart_rowr>--</td>
	  </tr>
<?

	}
	
	function addPromoCode($promo_code_id,$cart_reference='')
	{
		if($cart_reference){
			$reference = self::get1CartReference($cart_reference);
			$session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
		$info = array('session_id' => $session_id, 'promo_code_id' => $promo_code_id, 'customer_id' => $cust_acc_id);
		
		return db_replace('cart_promo_codes', $info);
	}

	function deletePromoCode($id='' , $promo_code_id ='')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$query = "DELETE FROM cart_promo_codes
				WHERE 1 ";
		if($id){
			$query .= " AND id = '$id' ";
		}
		if($promo_code_id){
			$query .= " AND promo_code_id = '$promo_code_id' ";
		}
		
		$query .= " AND (session_id = '".session_id()."'";
		
		if($cust_acc_id  && $logged_in_time!='checkout'){
			$query .= " OR cart_promo_codes.customer_id = '" . $cust_acc_id . "' ";
		}
			$query .= " ) ";
		//echo $query;
		return db_query_array($query);		
	}
	
	function removeExpiredPromoCodes(){
		PromoCodes::removeAutoPromoCodes();
		$promos = $this->getPromoCodes('','','validate');
		foreach ($promos as $promo){
			$validity = PromoCodes::getValidity($promo);
			if($validity['result'] == false )
			{
				$this->deletePromoCode('',$promo['promo_code_id']);
			}
		}
	}
	
// 	function removeAutoPromoCodes(){
// 		$promos = $this->getPromoCodes('','','validate');
// 		foreach ($promos as $promo){
// 			if ($promo['auto'] == 'Y')			
// 			{
// 				$this->deletePromoCode('',$promo['promo_code_id']);
// 			}
// 		}
// 	}

	function getPromoCodes($id = 0, $promo_code_id = '', $type='', $cart_reference='')
	{
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$q = " SELECT cart_promo_codes.*,
			promo_codes.combo_type as promo_code_combo_type,
			promo_codes.code as promo_code_code,
			promo_codes.description as promo_code_description,
			promo_codes.essensa_only ";
		if($type == 'validate')	{
			$q = " SELECT promo_codes.* ";
		}

		$q .= "	FROM cart_promo_codes LEFT JOIN promo_codes ON (cart_promo_codes.promo_code_id = promo_codes.id) ";
		if($cart_reference){
			$q .= "LEFT JOIN cart_reference ON ((cart_promo_codes.session_id = cart_reference.session_id ) OR (cart_promo_codes.customer_id >0 AND cart_reference.checkout_login = 'N' AND cart_promo_codes.customer_id = cart_reference.customer_id)) ";
		}	
			
		$q .= " WHERE 1 ";

		if ($id) {
			$q .= db_restrict('id', $id);
		}

		if($cart_reference){
			$q .= " AND cart_reference.cart_reference = '$cart_reference' ";
		} else {
			$q .= " AND ( cart_promo_codes.session_id = '".session_id()."' ";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$q .= " OR cart_promo_codes.customer_id = '" . $cust_acc_id . "' ";
			}
			$q .= " ) ";
		}
		
		if ($promo_code_id) {
			$q .= db_restrict('promo_code_id', $promo_code_id);
		}

		// first apply 'dollars', then 'percentage'. This is relying on alpha sorting,
		// but for now this is OK. If this breaks later on, use a const table w/ ordering.
		// RSunness added "code"
		$q .= " ORDER BY global_discount_type, code ASC";
		
		//echo $q;
		
		return db_query_array($q, 'id');

	}
	
	function getAutoPromos(){
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$sql = "SELECT cart.product_id, 
				promo_code_id, 
				promo_codes.combo_type as promo_code_combo_type, 
				promo_codes.code as promo_code_code, 
				promo_codes.description as promo_code_description,
				promo_codes.auto as auto_applied
			FROM cart 
			LEFT JOIN promo_code_require_products ON cart.product_id = promo_code_require_products.product_id
			LEFT JOIN promo_codes ON promo_code_require_products.promo_code_id = promo_codes.id 
			#LEFT JOIN promo_code_require_products ON promo_code_require_products.promo_code_id = promo_codes.id 
			WHERE 1 
			AND promo_codes.auto = 'Y' 
			AND (expires = 'N' OR expire_date > NOW())";
		$sql .= " AND (session_id = '".session_id()."'";
		if($cust_acc_id && $logged_in_time!='checkout'){
				$sql .= " OR customer_id = '" . $cust_acc_id . "' ";
			}
		$sql .=	") #GROUP BY promo_code_id
			ORDER BY promo_code_require_products.qty DESC 
			";
		return db_query_array($sql);		
	}

	function checkCouponConflicts($promo_code_id,$cart_reference='')
	{
		$all_codes = $this->getPromoCodes(0,'','',$cart_reference);
		return PromoCodes::checkCouponConflicts($promo_code_id,$all_codes);
	}

function getTax( $tax_rate = 0.00 ,$cart_reference = '',$zip_code='',$shipping_cost='')
	{
	    global $CFG;
	    global $cart;
	    
	    if( $tax_rate <= 0.00 )
		return 0.00;
	    
	    $total = self::getCartTotal(true,true,"","", $cart_reference);
	    //echo $total;
	    
	    if(!$shipping_cost){
		    $regular_shipping_cost = Shipping::calcShipping(0,'cart','',$zip_code,0,false,'',false,$cart_reference);
		    $shipping_cost  = Shipping::calcShipping(0, 'cart', '', $zip_code, 0, false, "", true, $cart_reference);
		    $shipping_discount = $regular_shipping_cost - $shipping_cost;
	    }
	    
	    $coupon_gift_arr = self::discountCalc(0,'',false,$cart_reference);
		$coupon_gift_arr['aps']['shipping_discount'] = $shipping_discount;
		$coupon_gift_arr['shipping_discount'] = $shipping_discount;
	    //print_ar( $coupon_gift_arr);
	    $coupon_gift = $coupon_gift_arr['overall_discount'] - $coupon_gift_arr['shipping_discount'];

    	$min_charge = self::calcMinCharges($cart_reference);
    	$liftgate = Shipping::calcLiftgateFee('','cart');
    	$liftgate_charge = $cart->data['charge_liftgate_fee'] ? $liftgate : 0.0 ;

	    //print_ar( $tax_rate );
	   // echo $total . " " . $coupon_gift . " " . $shipping_cost . " " . $min_charge . " " . $liftgate_charge . " " . $cart->data['rewards_using'];
	    
    	$tax_amount = ($total - $coupon_gift + $shipping_cost + $min_charge + $liftgate_charge - $cart->data['rewards_using']) * ($tax_rate * .01);
	    
	    return $tax_amount;
	}
		
	function getCartTotal($include_products = true, $include_diamonds = true, $type="", $coupon_code = "", $cart_reference = '', $paypal_token='')
	{ 
//		$session_id = session_id();
//
//		$q = " SELECT SUM(cart.qty * products.price) as total
//				FROM cart
//					LEFT JOIN products ON (products.id = cart.product_id)
//					WHERE cart.session_id = '$session_id'";
//
//		$info = db_get1(db_query_array($q));

		
		if ($type == "") 
		{
			//var_dump( $type);
			$cart_info = self::get('','','','','','',$cart_reference,$paypal_token);
		}
		else 
		{
			//var_dump( $type);
			$cart_info = self::get('', '', '', "qualifying",$type,$coupon_code,$cart_reference,$paypal_token);
		}
		
		$total = 0;
		if($cart_info) foreach( $cart_info as $item )
		{
		    $total += $item['line_total'];
		}
		return $total;
/*
		$items = Cart::getCartGrouped();

		$info['total'] = 0;
		foreach( $items as $item )
		{
			$product = Products::get1( $item['product_id'] );
			$markup  = Products::getProductMarkup( $product );
			$item_total = $markup * $item['sum'];
			$info['total'] += $item_total;
		}
		//return $info;
		*/
	//	return $info['total'];
	}
		
	function updateFreight() {
		global $cart;
		global $CFG;
		
		//not using this method anymore - it is now set when shipping is calculated
		return;
		
		unset($cart->data['shipping_method_2']);

		$cart_info = self::get();

		$freight = 0;
		$standard = 0;
		foreach ($cart_info as $item) {
			if($item['shipping_type'] == 'Freight'){
				$freight++;
			} else {
				$standard++;
			}
		}
		
		if( $freight>0 && $standard>0){
			if($cart->data['shipping_method'] == $CFG->freight_shipping_code || !$cart->data['shipping_method']){
				$cart->data['shipping_method'] = 'GND';
			}
			$cart->data['shipping_method_2'] = $CFG->freight_shipping_code;
		} elseif ($freight>0){
			$cart->data['shipping_method'] = $CFG->freight_shipping_code;
		} elseif ($cart->data['shipping_method'] == $CFG->freight_shipping_code || !$cart->data['shipping_method']){
			$cart->data['shipping_method'] = 'GND';
		}
		
		if($freight == 0){
			unset($cart->data['charge_liftgate_fee']);
		}
	}

	function getCartStatsShort(&$item_count, &$subtotal) {
        $subtotal = 0;
        $item_count = 0;

        $items = self::get();
        
        if (is_array($items))
        {       
            foreach( $items as $cart_item )
            {
                $item_count += $cart_item["qty"];
                $subtotal += $cart_item["line_total"];
            }
        }
	}
     function getCartStats(&$item_count, &$subtotal, &$all_items_data="", $type="") {

		global $CFG;
		
		$subtotal = 0;
        $item_count = 0;

		$items = self::get();
		
        $this_option = $this_product = $prev_option = $prev_product = $prev_cart_item = $prev_b_type = $this_optional = $prev_optional = $this_preset = $prev_preset = "";
        
        if ($type == "checkout")
        {
        	$b_reg_function = "self::showProductLineBRegCheckout";
        	$b_case_function = "self::showProductLineBCaseCheckout";
        	$a_function = "self::generateProductLineForCheckoutCart";
        }
     	else if ($type == "cart_redesign")
        {
        	$b_reg_function = "self::showProductLineBRegFloatingRedesign";
 			$b_case_function = "self::showProductLineBCaseFloatingRedesign";
 			$a_function = "self::generateProductLineForFloatingCartRedesign";
        }
		else
		{
 			$b_reg_function = "self::showProductLineBRegFloating";
 			$b_case_function = "self::showProductLineBCaseFloating";
 			$a_function = "self::generateProductLineForFloatingCart";
		}
		
 		
 		include $CFG->dirroot ."/includes/case_cart_include.php";
            	
        //    $all_items_data  .= self::generateProductLineForFloatingCart($cart_item, $show_title);
            //print_r($item);
       //     $show_title = false;
       // 	}
        //}
	
	
    }
			
    function getCartGrouped($onlyEligibleForDiscount=false,$cart_reference='')
    {
    	
    	if($cart_reference){
    		$reference = Cart::get1CartReference($cart_reference);
    		$session_id = $reference['session_id'];
    		if($reference['customer_id'])
    			$cust_acc_id = $reference['customer_id'];
    	} else {
    		$session_id = session_id();
    		if($_SESSION['cust_acc_id'])
    			$cust_acc_id = $_SESSION['cust_acc_id'];
    	}
   
			if($_SESSION['logged_in_time'])
				$logged_in_time = $_SESSION['logged_in_time'];

			$q = " SELECT product_id, SUM(qty) as sum, cat_id, brand_id
				FROM cart LEFT JOIN products on products.id = cart.product_id
                WHERE product_id <> 0
                AND (session_id = '$session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			if ($onlyEligibleForDiscount) $q .= " AND qualifiesForDiscount = 'Y'";
			$q .= " GROUP BY product_id";

			return db_query_array($q, 'product_id');

	}

	function getCartGroupedByOptions($onlyEligibleForDiscount=false,$cart_reference='')
	{
			
		if($cart_reference){
			$reference = Cart::get1CartReference($cart_reference);
			$session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
	
			if($_SESSION['logged_in_time'])
				$logged_in_time = $_SESSION['logged_in_time'];

			$q = " SELECT cart.id as the_cart_id, product_id, product_option_id, SUM(qty) as sum, cat_id, brand_id
				FROM cart LEFT JOIN products on products.id = cart.product_id
				LEFT JOIN cart_options ON cart.id = cart_options.cart_id
				WHERE product_id <> 0
				AND (session_id = '$session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			if ($onlyEligibleForDiscount) $q .= " AND qualifiesForDiscount = 'Y'";
			$q .= " GROUP BY product_id, product_option_id";

			return db_query_array($q);

	}
	
	function getCartCatGrouped($onlyEligibleForDiscount=false)
	{
			$session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
			if($_SESSION['logged_in_time'])
				$logged_in_time = $_SESSION['logged_in_time'];
			
			$q = " SELECT cat_id, SUM(qty) as sum, SUM(qty * cart.cart_price) as sum_price
				FROM cart LEFT JOIN products ON (cart.product_id = products.id)
				WHERE product_id <> 0
				AND (session_id = '$session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";
			if ($onlyEligibleForDiscount) $q .= " AND qualifiesForDiscount = 'Y'";
			$q .= " GROUP BY cat_id";
			return db_query_array($q, 'cat_id');
	}

	function getCartBrandGrouped($onlyEligibleForDiscount=false)
	{
			$session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
			if($_SESSION['logged_in_time'])
				$logged_in_time = $_SESSION['logged_in_time'];

			$q = " SELECT brand_id, SUM(qty) as sum, SUM(qty * cart.cart_price) as sum_price
				FROM cart LEFT JOIN products ON (products.id = cart.product_id)
				WHERE product_id <> 0
				AND (session_id = '$session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";
			if ($onlyEligibleForDiscount) $q .= " AND qualifiesForDiscount = 'Y'";
			$q .= " GROUP BY brand_id" ;
			return db_query_array($q, 'brand_id');
	}

	function getCartOccGrouped($onlyEligibleForDiscount=false,$cart_reference='')
	{
		if($cart_reference){
			$reference = Cart::get1CartReference($cart_reference);
			$session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}	
			if($_SESSION['logged_in_time'])
				$logged_in_time = $_SESSION['logged_in_time'];

			$q = " SELECT occasion_id, SUM(qty) as sum, SUM(qty * cart.cart_price) as sum_price
				FROM cart LEFT JOIN products ON (cart.product_id = products.id)
				WHERE (session_id = '$session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
			$q .= " ) AND product_id <> 0 ";
			if ($onlyEligibleForDiscount) $q .= " AND qualifiesForDiscount = 'Y'";
			$q .= " GROUP BY occasion_id";
			return db_query_array($q, 'occasion_id');
	}

	function discountCalc($shipping_amt = 0.00, $max_discount = '',$for_checkout=false, $cart_reference='')
	{	
		return PromoCodes::discountCalc('', $shipping_amt, $max_discount, $for_checkout, $cart_reference);
	}

	function cantUseCoupon($cart_info){
		if(!is_array($cart_info)){
			return true; //returning true means bad data, return false means you can use coupon, return array returns products you can use.
		}

		$badProds = array();
		if(!empty($cart_info)){
			foreach($cart_info as $item){
				if($item['is_promo_allowed'] == "N"){
					$badProds[] = array("id"=>$item['id'],"product_name"=>$item['product_name']);
				}
			}
		}

		if(empty($badProds)){
			return false;
		}
		else{
			return $badProds;
		}


	}

	function calcMinCharges($cart_reference = '')
	{
	    global $CFG;

	    $cart_info = $this->get('','','','','','',$cart_reference);

	    $brand_totals = array();

	    if(is_array($cart_info))foreach( $cart_info as $item )
	    {
			$product = Products::get1( $item['product_id'] );
			$brand[$product['brand_id']] = Brands::get1( $product['brand_id'] );
	
			if( $product['exclude_min'] == 'Y' )
			    continue;
	
			//exclude items in stock
			$product_stock = db_get1(Inventory::get('','','','','',$item['product_id'],$item['product_option_id'],'',$CFG->default_inventory_location));
			$qty = (($item['qty'] - $product_stock['qty']) > 0) ? ($item['qty'] - $product_stock['qty']) : 0 ;
			
			$line_total = $qty * $item['cart_price'];
			
			//$line_total = self::getCartLinePrice( $item['product_id'] );
			
			$brand_totals[$product['brand_id']] += $line_total;
			
			// for Chef Revival and San Jamar, only have one fee, so add Chef Revival to San Jamar
			// and don't calculate Chef Revival
			if($product['brand_id'] == 278)
			{
				$brand_totals[88] += $line_total;
				$brand[88] = Brands::get1(88);			
		    }
	    }
	    
	    $min_charge = 0;
		unset($brand_totals[278]);

	    foreach( $brand_totals as $brand_id => $total )
	    {
			if( $brand[$brand_id]['min_amount'] > 0 )
			{
			    if($total>0 && $total < $brand[$brand_id]['min_amount'] )
			    {
					$min_charge += $brand[$brand_id]['min_charge'];
			    }
			}
	    }

	    return $min_charge;
	}
	function revertCartNoProdsQualify($cart_reference = '',$coupon_code = '')
	{
		if($cart_reference){
			$reference = self::get1CartReference($cart_reference);
			$the_session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$the_session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($coupon_code == '') $q = "UPDATE cart SET cart.qualifiesForDiscount = '', cart.qualifiesForShippingDiscount = ''";
		else 
		{
			$coupon_code = ",".$coupon_code;
			$q = "UPDATE cart SET cart.qualifiesForDiscount = REPLACE(cart.qualifiesForDiscount,'".$coupon_code."',''), 
					cart.qualifiesForShippingDiscount = REPLACE(cart.qualifiesForShippingDiscount,'".$coupon_code."','')";
		}
		$q .="	WHERE (session_id = '$the_session_id' ";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$q .= " ) ";
		db_query($q);
	}
	function updateCartProdsQualify($qualifying_prods, $discount_type, $coupon_code, $cart_reference='')
	{
		if($cart_reference){
			$reference = self::get1CartReference($cart_reference);
			$the_session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$the_session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		$prod_id_list = implode(",", $qualifying_prods);
		
		//add other cart lines for the same product id and option id (for case pricing)
		$sql = "SELECT cart.id AS the_cart_id
			FROM cart
			LEFT JOIN products ON products.id = cart.product_id
			LEFT JOIN cart_options ON cart.id = cart_options.cart_id
			LEFT JOIN (
				SELECT cart.id AS the_cart_id, product_id, product_option_id
				FROM cart
				LEFT JOIN products ON products.id = cart.product_id
				LEFT JOIN cart_options ON cart.id = cart_options.cart_id
				WHERE cart.id
				IN ( $prod_id_list )
				) AS qualifying
				ON ( qualifying.product_id = cart.product_id
						AND (cart_options.product_option_id IS NULL
								OR
							qualifying.product_option_id = cart_options.product_option_id )
					)
			WHERE  qualifying.the_cart_id
					AND (session_id = '$the_session_id' ";
			if($cust_acc_id && $logged_in_time!='checkout'){
				$sql .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
			}
			$sql .= " )";
		
		$all_qualifying_prods = db_query_array($sql);
		foreach ($all_qualifying_prods as $prod){
			$qp[] = $prod['the_cart_id'];
		}
		$prod_id_list = implode(",", $qp); 
		
		if ($discount_type == "total"){
			$q = "UPDATE cart SET qualifiesForDiscount = concat(qualifiesForDiscount, ',', '$coupon_code')
				WHERE cart.id in ($prod_id_list)
				AND (session_id = '$the_session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			db_query($q);
		} else if ($discount_type == "shipping") {
			$q = "UPDATE cart SET qualifiesForShippingDiscount = concat(qualifiesForShippingDiscount, ',', '$coupon_code')
				WHERE cart.id in ($prod_id_list)
				AND (session_id = '$the_session_id' ";
				if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			db_query($q);
		}
		//if (session_id() == "4bhi3l0aklloq3nsip6a4abft6")mail("rachel@tigerchef.com", "$coupon_code-$discount_type", $q);
	}
	function updateCartAllProdsQualify($discount_type, $coupon_code, $cart_reference='')
	{ 
		if($cart_reference){
			$reference = self::get1CartReference($cart_reference);
			$the_session_id = $reference['session_id'];
			if($reference['customer_id'])
				$cust_acc_id = $reference['customer_id'];
		} else {
			$the_session_id = session_id();
			if($_SESSION['cust_acc_id'])
				$cust_acc_id = $_SESSION['cust_acc_id'];
		}
		
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
			
		if ($discount_type == "total"){
			$q = "UPDATE cart SET qualifiesForDiscount = concat(qualifiesForDiscount, ',', '$coupon_code')
			WHERE ( session_id = '$the_session_id'";
			if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			db_query($q);
		} else if ($discount_type == "shipping") {
			$q = "UPDATE cart SET qualifiesForShippingDiscount = concat(qualifiesForShippingDiscount, ',', '$coupon_code')
			WHERE ( session_id = '$the_session_id'";
			if($cust_acc_id && $logged_in_time!='checkout'){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
				$q .= " ) ";	
			db_query($q);
		}
// 		echo $q;
	}
	function setUpProdWithCouponsInCart($coupon_info,$cart_reference='')
	{
		$qualify_for_discount_on_brands_and_cats = PromoCodes::getBrandandCatQualifyingProducts($coupon_info,'',$cart_reference);
		
		$qualify_for_discount_on_prods = PromoCodes::getItemQualifyingProducts($coupon_info);
		$return_info = PromoCodes::getTotalQualifyingProducts($coupon_info);
		$qualify_for_total_discounts = $return_info[0];
		$not_qualify_for_total_discounts = $return_info[1];
				
		// get union
		$all_qualifying_prods = array_unique(array_merge($qualify_for_discount_on_brands_and_cats,$qualify_for_discount_on_prods));
		$output = print_r($all_qualifying_prods, true);
		
		if ($all_qualifying_prods && count($all_qualifying_prods > 0)) 
		{
			if ($coupon_info['global_discount_amt'] && $coupon_info['global_discount_amt'] != "0.00") 
			{
				$this->updateCartProdsQualify($all_qualifying_prods, "total",$coupon_info['code'],$cart_reference);
			}
			if ($coupon_info['shipping_discount_amt'] && $coupon_info['shipping_discount_amt'] != "0.00") 
			{
				$this->updateCartProdsQualify($all_qualifying_prods, "shipping",$coupon_info['code'],$cart_reference);
			}
		}
		else if (count($not_qualify_for_total_discounts) > 0 && count($qualify_for_total_discounts) > 0)
		/*else if ($qualify_for_total_discounts && count($qualify_for_total_discounts) > 0)*/
		{
			if ($coupon_info['global_discount_amt'] && $coupon_info['global_discount_amt'] != "0.00") 
			{
				$this->updateCartProdsQualify($qualify_for_total_discounts, "total",$coupon_info['code'],$cart_reference);
			}
			if ($coupon_info['shipping_discount_amt'] && $coupon_info['shipping_discount_amt'] != "0.00") 
			{
				$this->updateCartProdsQualify($qualify_for_total_discounts, "shipping",$coupon_info['code'],$cart_reference);
			}
		}
		else if (count($not_qualify_for_total_discounts) == 0)  
		{
			if ($coupon_info['global_discount_amt'] && $coupon_info['global_discount_amt'] != "0.00") 
				$this->updateCartAllProdsQualify("total", $coupon_info['code'], $cart_reference);
			if ($coupon_info['shipping_discount_amt'] && $coupon_info['shipping_discount_amt'] != "0.00")	
				$this->updateCartAllProdsQualify("shipping", $coupon_info['code'], $cart_reference);
		}
	}
	
	static  function getCartLinePrice( )
	{
	    global $CFG, $cart;
	    
	    $cart_info = $cart->get();
	    
	    $discount = self::discountCalc( $cart->data['shipping_cost'] );
	    //print_ar( $discount );
	    //Total discount needs to be split on all products
	    //brand discounts split on products of that brand
	    //cat discounts split on products of that cat
	    
	    
	    if( $discount['breakdown']['item_discount'] )
		foreach( $discount['breakdown']['item_discount'] as $prod_id => $value )
		{

		}
	    
	    if(is_array($cart_info))foreach( $cart_info as $item )
	    {
		
	    }
	    
	}

	static function getCustomersForReminderEmail(){

		$cart_result = array();
		$sessions_result = array();

		$sql = " SELECT DISTINCT customer_id, session_id, first_name, last_name, CONCAT(first_name,' ',last_name) AS customer_name, email, dedicated_rep
				FROM cart
				LEFT JOIN customers ON cart.customer_id = customers.id
				WHERE customer_id > 0 AND cart.is_gift = 'N'
				AND (email_reminder_sent = 1 AND date_added < DATE_SUB(NOW(),INTERVAL 5 DAY )
							OR
				email_reminder_sent = 0 AND date_added < DATE_SUB(NOW(),INTERVAL 30 MINUTE ) )
				AND cart.customer_id NOT IN (SELECT customer_id
										FROM cart
										WHERE customer_id > 0
										AND  date_added > DATE_SUB(NOW(),INTERVAL 30 MINUTE))
				AND customers.last_reminder_email_sent < DATE_SUB(NOW(),INTERVAL 1 DAY)
				GROUP BY email
	
			" ;

		$cart_result = db_query_array($sql);

		$sql = " SELECT DISTINCT cart.session_id, email, customer_name
				FROM cart
				LEFT JOIN sessions ON cart.session_id = sessions.session_id
				WHERE sessions.email <> '' AND cart.is_gift = 'N'
				AND (email_reminder_sent = 1 AND date_added < DATE_SUB(NOW(),INTERVAL 5 DAY )
							OR
				email_reminder_sent = 0 AND date_added < DATE_SUB(NOW(),INTERVAL 30 MINUTE ) )
				AND cart.session_id NOT IN (SELECT session_id
										FROM cart
										WHERE customer_id > 0
										AND  date_added > DATE_SUB(NOW(),INTERVAL 30 MINUTE))
				AND sessions.last_reminder_email_sent < DATE_SUB(NOW(),INTERVAL 1 DAY)
				GROUP BY email
			" ;
		$session_result = array();
		$sessions_result = db_query_array($sql);

		if($sessions_result){
			foreach ($sessions_result as $item) {
				$session_list[$item['email']] = $item;
			}
			foreach($cart_result as $key=>$result){
				unset($session_list[$result['email']]);
			}
		}

		$all_customers = array_merge((array)$cart_result,(array)$session_list);
		return 	$all_customers;
	}

	static function getCustomerCartItems($customer_id,$session_id){

		$sql = " SELECT cart.*, products.name AS product_name, url_name, products.id AS product_id
				FROM cart
				LEFT JOIN products ON products.id = cart.product_id
				WHERE  cart.is_gift = 'N' AND products.is_active = 'Y' AND products.is_active = 'Y' " ;
		if($customer_id){
			$sql .= " AND (customer_id = {$customer_id} 
								OR 
								session_id IN (SELECT DISTINCT session_id FROM cart WHERE customer_id = {$customer_id})
								)";
		}else{
			$sql .= " AND (session_id = '{$session_id}' 
								OR
								customer_id > 0 AND (customer_id IN (SELECT DISTINCT customer_id FROM cart WHERE session_id = '{$session_id}' ))
								)";
		}
		return db_query_array($sql);

	}
	static function SetReminderEmailSent($emailed_list,$customer_id,$session_id,$email){

		//mark items that where sent
		if($emailed_list) {

			$sql = " UPDATE cart
							SET email_reminder_sent = IF(date_added > DATE_SUB(NOW(),INTERVAL 5 DAY ) , 1 , 2)
							WHERE id IN ($emailed_list) ";
			db_query_array($sql);


			$sql = " SELECT DISTINCT(cart.session_id)
								FROM 
									`cart` 
									left join customers on cart.customer_id = customers.id 
									left join sessions on sessions.session_id = cart.session_id 
								WHERE 
									(cart.id IN ($emailed_list) ";
			if($email) {
				$sql .= "
									 OR sessions.email = '{$email}' 
									 OR customers.email = '{$email}'
									";
			}
			$sql .= ")";

			$distinct_sessions = db_query_array($sql);
			foreach ($distinct_sessions as $distinct_session) {
				$session_list[] = "'{$distinct_session['session_id']}'";
			}
			if (!$customer_id) {
				$sql = " SELECT DISTINCT(cart.customer_id)
								FROM 
									`cart` 
									left join customers on cart.customer_id = customers.id 
									left join sessions on sessions.session_id = cart.session_id 
								WHERE 
									customer_id > 0 AND (cart.id IN ($emailed_list) " ;
				if($email) {
					$sql .= "
									 OR sessions.email = '{$email}' 
									 OR customers.email = '{$email}'
									";
				}
				$sql .= ")";
				$customer_id = db_query_array($sql);
				if(is_array($customer_id)){$customer_id = $customer_id['customer_id'];}
			}
		}

		if($customer_id){
			//update when the last reminder email was sent to the customer
			$sql = " UPDATE customers
								SET last_reminder_email_sent = NOW()
								WHERE id = {$customer_id} " ;
			db_query_array($sql);
		}
		if($session_id){
			//update when the last reminder email was sent by session
			$sql = " UPDATE sessions
								SET last_reminder_email_sent = NOW()
								WHERE 1 AND (";
			if($email){
				$sql .= "   sessions.email = '{$email}' OR ";
			}
			if($distinct_sessions){
				$sql .= "  sessions.session_id IN (".implode(',',$session_list).") )";
			}else{
				$sql .= "  sessions.session_id = '{$session_id}' )" ;
			}
			db_query_array($sql);
		}
	}
	
	static function deleteOldCartItems(){
		
		//delete from cart option table	
		$sql = " DELETE FROM cart_options
			WHERE cart_id IN (SELECT id FROM cart
						WHERE customer_id = 0
						AND date_added < DATE_SUB(NOW(),INTERVAL 45 DAY ))";	
		db_query_array($sql);
		
		//delete from cart promos table
		$sql = " DELETE FROM cart_promo_codes
			WHERE session_id IN (SELECT session_id FROM cart
						WHERE customer_id = 0
						AND date_added < DATE_SUB(NOW(),INTERVAL 45 DAY ))
			AND customer_id = 0";	
		db_query_array($sql);
		
		//delete from cart reference table
		$sql = " DELETE FROM cart_reference
				WHERE cart_reference.customer_id =0 
				AND date < DATE_SUB( NOW( ) , INTERVAL 45 DAY ) ";
		db_query_array($sql);
		
		//delete from cart table	
		$sql = " DELETE FROM cart
			WHERE customer_id = 0
			AND date_added < DATE_SUB(NOW(),INTERVAL 45 DAY )" ;	
		db_query_array($sql);	
		
	}
	
	static function generateCartReference($check_if_cart_reference_exists=true){

		$generate = true;
		
		//Make sure it's not a spider/bot
		$ua =  $_SERVER['HTTP_USER_AGENT'] ;
		if (strpos($ua, '+http') == true || strpos($ua, 'http:') == true ||
		strpos($ua, 'www.') == true || strpos($ua, '@') == true)
		{	// i'm spider
			return false;
		}
		
		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		
		if($check_if_cart_reference_exists){
			$generate = is_array(self::getCartReference()) === false ;
		}
		
		if($generate){

			$cart_reference = strtoupper(substr(md5(rand() + session_id()), 0, 8));			
			
			while(is_array(self::referenceExists($cart_reference)) === true){
				$cart_reference = strtoupper(substr(md5(rand() + session_id()), 0, 8));
			}
			
			$info = array('cart_reference'	=> $cart_reference,
							'session_id' 	=> session_id(),
							'customer_id' 	=> $cust_acc_id,);
			
			return db_insert('cart_reference',$info);
		}
	}
	
	static function referenceExists($cart_reference){
		$sql = "SELECT * from cart_reference WHERE cart_reference = '$cart_reference' ";
		return db_query_array($sql);
	}
	
	static function getCartReference($cart_reference='',$session_id='',$customer_id=''){
	
		if($_SESSION['cust_acc_id'] && !$customer_id)
			$customer_id = $_SESSION['cust_acc_id'];
		
		if(!$session_id){
			$session_id = session_id();
		}
	
		$sql = "SELECT cart_reference.* FROM cart_reference WHERE 1 ";
		
		if($cart_reference){
			$sql .= " AND cart_reference.cart_reference = '$cart_reference' ";
		} else {
			$sql .= " AND (cart_reference.session_id = '$session_id' ";
			if($customer_id){
				$sql .= " OR cart_reference.customer_id = '$customer_id' ";
			}
			$sql .= ") ";
		}

		//echo $sql;
		$return = db_query_array($sql);

		if(!$return){
			self::generateCartReference(false);
			$sql = "SELECT cart_reference.* FROM cart_reference WHERE 1 ";
			
			$sql .= " AND (cart_reference.session_id = '$session_id' ";
			if($customer_id){
				$sql .= " OR cart_reference.customer_id = '$customer_id' ";
			}
			$sql .= ") ";
				
			$return = db_query_array($sql);
		}
				
		return $return;
	}
	
	static function get1CartReference($cart_reference){
		if (!$cart_reference)
		{
			return false;
		}
		
		$result = self::getCartReference($cart_reference);
		return $result[0];
	}
	
	function updateCartReference($cart_reference=''){

		if($_SESSION['cust_acc_id'])
			$cust_acc_id = $_SESSION['cust_acc_id'];
		if($_SESSION['logged_in_time'])
			$logged_in_time = $_SESSION['logged_in_time'];
		
		if(!$cart_reference){	
			$cart_reference = self::getCartReference('','',$cust_acc_id);
		}
		if(is_array($cart_reference)){
			//delete old one
			db_query ("DELETE FROM cart_reference WHERE customer_id =  '$cust_acc_id' AND session_id <> '".session_id()."'");
		}
		
		$query = " UPDATE cart_reference SET customer_id =  '$cust_acc_id' ";
		if($logged_in_time == 'checkout'){
			$query .= ", checkout_login = 'Y' ";
		} else {
			$query .= ", checkout_login = 'N' ";
		}
		$query .= " WHERE session_id =  '".session_id()."'";
		
		db_query_array($query);
	}
		function show_cart_reference(){
		global $cart;
		?>
		<div class="cart_reference">
			<?php
			$cart_reference = db_get1($cart->getCartReference());
			?>
			<p>
			<strong>Cart ID: <?= $cart_reference['cart_reference']; ?></strong>
			</p>  
		</div>
		<?php
	}
	
	static function generateProductLineForFloatingCartRedesign($cart_line, $show_title_box = false, $type="")
    {
        global $CFG;
		global $cart;
        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand_discount     = BrandDiscount::get(0,'','',0,0,false,$product_info['brand_id']);
        $brand              = Brands::get1($product_info['brand_id']);
        $line_brand_discount= $cart_line['line_total'] * ($brand_discount[0]['amount']*.01);
        $option_description = '';
        $optionals          = '';

		if( is_array($cart_line['options'] ))
		{
	    	$vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
            $option_description = ProductOptionCache::getDescriptionForOptions($cart_line['options'], $cart_line['product_id']); 
            $optionals = str_replace(',','_',rtrim($cart_line['options'][0]['available_options_id'], ','));
		}

      	$return_string = "<li>";
	    	    
        $return_string .= '<div class="img-holder">';
        $return_string .= '<a href="'.$product_link.'" onclick="">';
        $return_string .= '<img src="'.$product_image_link.'" alt="'.$product_info['mfr_part_num'].'" width="61" height="61" />';
		$return_string .= '</a></div>';
		$return_string .= '<div class="description" rel="generateProductLineForFloatingCartRedesign">';
		 if(!!Products::isOnePerCartProduct($cart_line['product_id'])){
			$return_string .= '<a href="'.$product_link.'" onclick="">';
		}
		$return_string .= '<strong>'. str_replace('�','&reg;',$product_info['name']) . '</strong>';
        if(!empty($option_description)){
            $return_string .= '<span id="description_' . $cart_line['product_id'] . '_' .  $cart_line['product_option_id']  . $optionals;
            $return_string .= '">' . $option_description . '</span>';
        }
        if(!!Products::isOnePerCartProduct($cart_line['product_id'])){
 			$return_string .= '</a>';
		}	
        $final_sku =$vsku ? $vsku : $product_info['mfr_part_num']; 
        $return_string .= '<span data-sku="' . $final_sku . '">Item #: ' . $final_sku . '</span>';
        
        	if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
			$return_string .= "<span class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
		}
		$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
		if($ships_in){
			$return_string .= "<span class='ships_in'>";
			if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
				$return_string .= "Once in stock this item ships in ";
			}else{
				$return_string .= "Ships in ";
			}
			$return_string .= $ships_in . "</span>";
		}
		if($cart->data['shipping_method'] && $cart->data['shipping_method_2'] == $CFG->freight_shipping_code){
			$return_string .= "<span class='freight'>";
			if($product_info['shipping_type'] == 'Freight'){
				$return_string .= "Ships freight";
			}else{
				$return_string .= "Ships via standard carrier";	
			}
			$return_string .= "</span>";
		}		        
        $return_string .= '</div>';
		            
        return $return_string;   
         
    }
	
	function showProductLineBRegFloatingRedesign($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {    
        global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);        
        
		$return_string .= '<div class="price-holder">';
		$return_string .= '<div class="holder">';
        $return_string .= '<span class="qty">'.$cart_line['qty'].'</span>';
        $return_string .= '<span class="price">$'.number_format($cart_line['line_total'],2).'</span>';
        $return_string .= '</div>'; 
        $return_string .= '</div>';
        $return_string .= '</li>';
	
         //echo $return_string." is ret string";
         return $return_string;         
    }
    function showProductLineBCaseFloatingRedesign($prev_cart_line, $cart_line)
    {
    	global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);

        $return_string .= '<div class="price-holder">';
		$return_string .= '<div class="holder">';
        $return_string .= '<span class="qty">'.($cart_line['qty'] + $prev_cart_line['qty']).'</span>';
        $return_string .= '<span class="price">$'.number_format($cart_line['line_total'] + $prev_cart_line['line_total'],2).'</span>';
        $return_string .= '</div>'; 
        $return_string .= '</div>';
        $return_string .= '</li>';
        
         //echo $return_string." is ret string";
         return $return_string;
    }
	function showProductLineA_charger($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;
		global $cart;
        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand              = Brands::get1($product_info['brand_id']);
		$prod_for_tracking['id']= $product_info['id'];
		$prod_for_tracking['name']= preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($product_info['name'], ENT_QUOTES));
		$prod_for_tracking['brand_name']= $product_info['brand_name'];
		$prod_for_tracking['price']= number_format($cart_line['line_total'],2);
	if( $cart_line['options'] )
	{
	    //Only 1 option can be selected
	//    $option = $cart_line['options'][0];
	$vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
//    $vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
	}

	    if ($show_title_box)
	    {
        ?>
        <ul class="shoping-list">
			<li class="heading">
				<div class="col-1">Product</div>
				<div class="col-2">Unit Price</div>				
				<div class="col-3">Quantity</div>
				<div class="col-4">Price</div>
				<div class="col-5"></div>
			</li>
        <?
	    }

	    ?>			
			<li>
			<div class="col-1">
				<div class="holder">
				<div class="img-holder">				
                <? if ($product_info['is_active'] == 'Y' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>" onclick='select_content_track(<?=json_encode($prod_for_tracking)?>,"cart products")'><? } ?><img src="<?=$product_image_link?>" alt="<?=$product_info['mfr_part_num']?>"  /><? if ($product_info['is_active'] == 'Y'&& !Products::isOnePerCartProduct($cart_line['product_id'])) {?></a><? } ?>
                	</div>                                            
                        <div class="description" rel='show_product_line_a_redesign'>                     
                        <?php
                         if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>" onclick='select_content_track(<?=json_encode($prod_for_tracking)?>,"cart products")'><? } ?><?=str_replace('�','&reg;',$product_info['name'])?><? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?></span></a><? } ?>                                                
                      <? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>" onclick='select_content_track(<?=json_encode($prod_for_tracking)?>,"cart products")'><? } ?><span>ITEM # (<?=$vsku ? $vsku : $product_info['mfr_part_num']?>)<? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?></span></a><? } ?>						 
                        <?php if(is_array($cart_line['options'])){ ?>
                        <span class='option_description'>
                        <?php
                            echo ProductOptionCache::getDescriptionForOptions($cart_line['options'], $cart_line['product_id']);
                        ?>
                        </span><?php
                        }
	        if (strpos($product_info['promo_msg'], "do not qualify for free shipping") !== false) echo "<span class='no_free_ship'>This item does not qualify for free shipping.</span>";
			if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
				echo "<span class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
			}
			$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
			if($ships_in){
				echo "<span class='ships_in'>";
				if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
					echo "Once in stock this item ships in ";
				}else{
					echo "Ships in ";
				}
				echo $ships_in . "</span>";
			}
			if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
				echo "<span class='freight'>";
				if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
					echo "Ships freight";
				}else{
					echo "Ships via standard carrier";
				}
				echo "</span>";
			}

			?>
			</div>
			</div>
			</div>
        <?
    }

	function showProductLineB_charger($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $vsku               = $product_info['mfr_part_num'];
				
       if(is_array($cart_line['options'])){ 
			      $vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
       }
 
        
        $can_buy_array = Products::getCanBuyInfo($product_info, $CFG);
        $can_buy_limit = $can_buy_array['can_buy_limit'];
        
        if($can_buy_limit){ $product_info['min_qty'] = 0; }
        
		$product_info['min_qty'] = $product_info['min_qty'] ? $product_info['min_qty'] : '0';
		$prod_for_tracking['id']= $product_info['id'];
		$prod_for_tracking['name']= preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($product_info['name'], ENT_QUOTES));
		$prod_for_tracking['brand_name']= $product_info['brand_name'];
		$prod_for_tracking['price']= number_format($cart_line['line_total'],2);
         ?>
			<div class="col-2 price">$<?=number_format($cart_line['cart_price'],2)?></div>
			<div class="col-3">
          	<?php if($cart_line['is_gift'] == 'N' && !Products::isOnePerCartProduct($cart_line['product_id'])){?>
          		<input type="text" name="cart[<?=$cart_line[id]?>][qty]" value="<?=$cart_line['qty']?>" class="input_box cart_qty_input"  
          				data-min-qty="<?=$product_info['min_qty']?>"
          				onkeypress="if (event.keyCode == 13) {update_cart();return false;} else return true;"
         				<?=($cart_line['product_id'] == $CFG->gift_wrap_product_id) ? 'disabled' : ''?>/>
          		<br /><a class="update_cart_link" href="javascript:void(0);" onclick="gtag('event', 'cart update', {'event_category': 'cart', 'value': 'cart update'}); update_cart();">Update</a>
          	<?php }else{ ?>
          		<?=$cart_line['qty']?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=$cart_line['qty']?>" class="input_box" type="hidden" >
          	<?php }?>
				</div>
				<div class="col-4 price total">$<?=number_format($cart_line['line_total'],2)?></div>
				
      	<div class="col-5"><a href="javascript:void(0);" onClick='remove_from_cart_track(<?=json_encode($prod_for_tracking)?>,"<?=$cart_line['qty']?>"); update_cart(<?=$cart_line['id']?>); return false;' class="remove"<?php if(Products::isOnePerCartProduct($cart_line['product_id'])){ echo "style='display:none;'";}?>>X</a></div>
			</li>                           
        <? 
    }
	function showProductLineBCase_charger($prev_cart_line, $cart_line)
    {
    	global $CFG;
    
		    $product_info = Products::get1($cart_line['product_id']);
				$vsku = $product_info['mfr_part_num'];
				
       if(is_array($cart_line['options'])){ 
			      $vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
       }
        
        $can_buy_array = Products::getCanBuyInfo($product_info, $CFG);
        $can_buy_limit = $can_buy_array['can_buy_limit'];
        
        if($can_buy_limit){ $product_info['min_qty'] = 0; }
        $product_info['min_qty'] = $product_info['min_qty'] ? $product_info['min_qty'] : '0';

         ?>
			
			<div class="col-2 price"><div><?='<small>'.$cart_line['qty'] . " @</small> $" . number_format($cart_line['cart_price'], 2) ."</div><div><small>" .  $prev_cart_line['qty'] . " @</small> $" . number_format($prev_cart_line['cart_price'],2)?></div></div>
			<div class="col-3">
          	<?php 
            if($cart_line['is_gift'] == 'N' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?>
          		<input type="text" name="cart[<?=$cart_line[id]?>][qty]" value="<?=($cart_line['qty'] + $prev_cart_line['qty'])?>" class="input_box cart_qty_input"  
          		         data-min-qty="<?=$product_info['min_qty']?>"
          		         onkeypress="if (event.keyCode == 13) {update_cart();return false;} else return true;"
         				<?=( $cart_line['product_id'] == $CFG->gift_wrap_product_id)  ? 'disabled' : ''?>/>
          		<br /><a class="update_cart_link" href="javascript:void(0);" onclick="sendToDataLayer('Cart Update', '<?=strtr($product_info['name'], array('\\' => '\\\\', "'" => "\\'", '"' => '&quot;', "\r" => '\\r', "\n" => '\\n' )) ?>');update_cart();">Update</a>
          	<?php } else { ?>
          		<?=($cart_line['qty'] + $prev_cart_line['qty'])?>
          		<input name="cart[<?=$cart_line[id]?>][qty]" value="<?=($cart_line['qty'] + $prev_cart_line['qty'])?>" class="input_box" type="hidden" >
          	<?php }?>
			</div>					
          
          <div class="col-4 price total">$<?=number_format(($cart_line['line_total'] + $prev_cart_line['line_total']),2)?></div>
		  <?$temp_qty=$cart_line['qty'] + $prev_cart_line['qty'];
		    $track="window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'removeFromCart','gaEventLabel': '".strtr($product_info['name'], array('\\' => '\\\\', "'" => "\\'", '"' => '&quot;', "\r" => '\\r', "\n" => '\\n' ))."',ecommerce': {'remove': {'products': [{'name': '".htmlspecialchars(str_replace("'","",$product_info['name']), ENT_QUOTES)."','id': '".$product_info['id']."','variant': '".$vsku."','price': '".number_format(($cart_line['line_total'] + $prev_cart_line['line_total']),2)."','quantity':'".$temp_qty."' }] } }});";?>
		  <div class="col-5"><a href="javascript:void(0);" onClick="<?=$track?>;update_cart(<?=$cart_line['id']?>); return false;" class="remove">X</a></div>
		</li>
        <?
    }
	function showProductLineA_checkout_charger($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;
		global $cart;
        $product_info       = Products::get1($cart_line['product_id']);
        $product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
        $product_image_link = Catalog::makeProductImageLink($product_info['id']);
        $vsku               = $product_info['mfr_part_num'];
        $brand              = Brands::get1($product_info['brand_id']);
		//$track = getProdTracking($product_info,'Cart Product Link');
		$prod_for_tracking['id']= $product_info['id'];
		$prod_for_tracking['name']= preg_replace("/[^a-zA-Z0-9' ']+/", "", html_entity_decode($product_info['name'], ENT_QUOTES));
		$prod_for_tracking['brand_name']= $product_info['brand_name'];
		$prod_for_tracking['price']= number_format($cart_line['line_total'],2);
	if( $cart_line['options'] )
	{
	    //Only 1 option can be selected
	//    $option = $cart_line['options'][0];
	$vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
//    $vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
	}

	    if ($show_title_box)
	    {
        ?>
        <ul class="shoping-list checkout">
			<li class="heading">
				<div class="col-1">Product</div>
				<!--<div class="col-2">Unit Price</div>				-->
				<div class="col-3">QTY</div>
				<div class="col-4">Price</div>
				<!--<div class="col-5"></div>-->
			</li>
        <?
	    }

	    ?>			
			<li>
			<div class="col-1">
				<div class="holder">
				    <div class="description" rel='show_product_line_a_redesign'>                     
                        <?php
                         if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>" onclick='select_content_track(<?=json_encode($prod_for_tracking)?>,"checkout products")'><? } ?><?=str_replace('�','&reg;',$product_info['name'])?><? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?></span></a><? } ?>                                                
                      <? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?><a href="<?=$product_link?>" onclick='select_content_track(<?=json_encode($prod_for_tracking)?>,"checkout products")'><? } ?><span>ITEM # (<?=$vsku ? $vsku : $product_info['mfr_part_num']?>)<? if ($product_info['is_active'] == 'Y'  && !Products::isOnePerCartProduct($cart_line['product_id'])) {?></span></a><? } ?>						 
                        <?php if(is_array($cart_line['options'])){ ?>
                        <span class='option_description'>
                        <?php
                            echo ProductOptionCache::getDescriptionForOptions($cart_line['options'], $cart_line['product_id']);
                        ?>
                        </span><?php
                        }
	        if (strpos($product_info['promo_msg'], "do not qualify for free shipping") !== false) echo "<span class='no_free_ship'>This item does not qualify for free shipping.</span>";
			if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
				echo "<span class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
			}
			$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
			if($ships_in){
				echo "<span class='ships_in'>";
				if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
					echo "Once in stock this item ships in ";
				}else{
					echo "Ships in ";
				}
				echo $ships_in . "</span>";
			}
			if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
				echo "<span class='freight'>";
				if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
					echo "Ships freight";
				}else{
					echo "Ships via standard carrier";
				}
				echo "</span>";
			}

			?>
			</div>
			</div>
			</div>
        <?
    }

	function showProductLineB_checkout_charger($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

        $product_info       = Products::get1($cart_line['product_id']);
        $vsku               = $product_info['mfr_part_num'];
				
       if(is_array($cart_line['options'])){ 
			      $vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
       }
 
        
        $can_buy_array = Products::getCanBuyInfo($product_info, $CFG);
        $can_buy_limit = $can_buy_array['can_buy_limit'];
        
        if($can_buy_limit){ $product_info['min_qty'] = 0; }
        $product_info['min_qty'] = $product_info['min_qty'] ? $product_info['min_qty'] : '0';
         ?>
			<div class="col-3">
          	<?php if($cart_line['is_gift'] == 'N' && !Products::isOnePerCartProduct($cart_line['product_id'])){?>
          		<?=$cart_line['qty']?>
          	<?php }else{ ?>
          		<?=$cart_line['qty']?>
          	<?php }?>
				</div>
				<div class="col-4 price total">$<?=number_format($cart_line['line_total'],2)?></div>
				<?$track="window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'removeFromCart','gaEventLabel': '".strtr($product_info['name'], array('\\' => '\\\\', "'" => "\\'", '"' => '&quot;', "\r" => '\\r', "\n" => '\\n' ))."','ecommerce': {'remove': {'products': [{'name': '".htmlspecialchars(str_replace("'","",$product_info['name']), ENT_QUOTES)."','id': '".$product_info['id']."','variant': '".$vsku."','price': '".number_format($cart_line['cart_price'],2)."','quantity':'".$cart_line['qty']."' }] } }});";?>
      		</li>                           
        <? 
    }
	function showProductLineBCase_checkout_charger($prev_cart_line, $cart_line)
    {
    	global $CFG;
    
		    $product_info = Products::get1($cart_line['product_id']);
				$vsku = $product_info['mfr_part_num'];
				
       if(is_array($cart_line['options'])){ 
			      $vsku = ProductOptionCache::getSKUForOptions($cart_line['options'], $cart_line['product_id']);
       }
        
        $can_buy_array = Products::getCanBuyInfo($product_info, $CFG);
        $can_buy_limit = $can_buy_array['can_buy_limit'];
        
        if($can_buy_limit){ $product_info['min_qty'] = 0; }
        $product_info['min_qty'] = $product_info['min_qty'] ? $product_info['min_qty'] : '0';

         ?>
			<div class="col-3">
          	<?php 
            if($cart_line['is_gift'] == 'N' && !Products::isOnePerCartProduct($cart_line['product_id'])) {?>
          		<input type="text" name="cart[<?=$cart_line[id]?>][qty]" value="<?=($cart_line['qty'] + $prev_cart_line['qty'])?>" class="input_box cart_qty_input"  
          		         data-min-qty="<?=$product_info['min_qty']?>"
          		         onkeypress="if (event.keyCode == 13) {update_cart();return false;} else return true;"
         				<?=( $cart_line['product_id'] == $CFG->gift_wrap_product_id)  ? 'disabled' : ''?>/>
          		<br /><a class="update_cart_link" href="javascript:void(0);" onclick="sendToDataLayer('Cart Update', '<?=strtr($product_info['name'], array('\\' => '\\\\', "'" => "\\'", '"' => '&quot;', "\r" => '\\r', "\n" => '\\n' )) ?>');update_cart();">Update</a>
          	<?php } else { ?>
          		<?=($cart_line['qty'] + $prev_cart_line['qty'])?>
          	<?php }?>
			</div>					
          
          <div class="col-4 price total">$<?=number_format(($cart_line['line_total'] + $prev_cart_line['line_total']),2)?></div>
		  <?$temp_qty=$cart_line['qty'] + $prev_cart_line['qty'];
		    $track="window.dataLayer = window.dataLayer || [];dataLayer.push({'event': 'removeFromCart','gaEventLabel': '".strtr($product_info['name'], array('\\' => '\\\\', "'" => "\\'", '"' => '&quot;', "\r" => '\\r', "\n" => '\\n' ))."',ecommerce': {'remove': {'products': [{'name': '".htmlspecialchars(str_replace("'","",$product_info['name']), ENT_QUOTES)."','id': '".$product_info['id']."','variant': '".$vsku."','price': '".number_format(($cart_line['line_total'] + $prev_cart_line['line_total']),2)."','quantity':'".$temp_qty."' }] } }});";?>
		  
		</li>
        <?
    }
   
    function getCrossSellsForCart($num_to_get)
    {
    	global $cart;
    	$items = $cart->get();
    	$full_cross_sells_array = array();
    	
    	if ($items)
    	{
    		foreach ($items as $item)
    		{    			
    			$cats = Cats::getCatsForProduct($item['product_id']);
				$tree = Cats::getCatsTree( $cats[count($cats)-1]['id']);
				foreach ($tree as $cat_info)
				{
					$bottom_cat_id = $cat_info['id'];
    			}
    
    			$cross_sells = Products::get_cross_sells_for_product($item['product_id'], $bottom_cat_id);

    			foreach($cross_sells as $a_cross_sell)
    			{ 
    				$full_cross_sells_array[] = $a_cross_sell;
    			}
    			if (count($full_cross_sells_array) >= $num_to_get) break;
    		}	
    	}
    	return $full_cross_sells_array;
    }
    
    /**
     * @param object $vars (needs to be an object or a class - an array wont work)
     * @return string
     */
    function create_cost_breakdown_table($vars){
    
    	global $cart;
    	$zipcode = $vars->cart2['shipping_zip'] ? $vars->cart2['shipping_zip'] : $cart->data['zipcode'];

    	$cost_table  = '<div class="total-row sub_total">
						<span class="total-title">Subtotal:</span>
						<span class="total-value">$'.number_format($vars->sub_total, 2).'</span>
					</div>';
    	if ($vars->min_charge > 0.0)
    	{
    		$cost_table .= '<div class="total-row min_charge">
									<span class="total-title">Minimum Surcharge:</span>
									<span class="total-value">$'.number_format($vars->min_charge, 2).'</span>
								</div>';
    	}
    	$cost_table .= '<div class="total-row shipping_amt">
									<span class="total-title">Shipping Cost:</span>
									<span class="total-value">';
    	$cost_table .= ($zipcode) ? ('$'.number_format($vars->shipping_cost, 2)) : 'Waiting for address';
    	$cost_table .= '</span>
								</div>';
		$cost_table .= '<div class="total-row  liftgate_amt';
    	$cost_table .= ($vars->liftgate > 0.0)? '' : ' hidden' ;
    	$cost_table .= '">
									<span class="total-title">Liftgate Fee:</span>
									<span class="total-value" id="liftgate_amt">$'.number_format($vars->liftgate, 2).'</span>
							</div>';
    	if ($vars->coupon_gift > 0.0)
    	{
    		$cost_table .= '<div class="total-row coupon_gift">
									<span class="total-title">Coupon Code Discount:</span>
									<span class="total-value">- $'.number_format($vars->coupon_gift, 2).'</span>
								</div>';
    	}
    	if ($cart->data['rewards_using'] > 0.0)
    	{
    		$cost_table .= '<div class="total-row rewards_redeemed">
									<span class="total-title">Rewards Redeemed:</span>
									<span class="total-value" id="rewards_redeemed">- $'.number_format($cart->data['rewards_using'], 2).'</span>
								</div>';
    	}
    	$cost_table .= '<div class="total-row  sales_tax';
    	$cost_table .= ($vars->sales_tax > 0)? '' : ' hidden' ;
    	$cost_table .= '">
									<span class="total-title">Sales Tax:</span>
									<span class="total-value" id="sales_tax">$'.number_format($vars->sales_tax, 2).'</span>
<div style="margin-bottom:15px;">&nbsp;</div>'.
									//<span class="tax-exempt"><a href="/tax-exempt.html" onclick="window.open(this.href,\'\',\'resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=514,height=199,status\');return false;">Are you tax exempt?</a></span>
									'</div>';
    	/*
    	 $cost_table .= '<div class="total-row">
    	<span class="total-title">Sales Tax:</span>
    	<span class="total-value sales_tax" id="sales_tax">';
    	$cost_table .= ($zipcode) ? ('$'.number_format($vars->sales_tax, 2)) : ' --- ';
    	$cost_table .= '</span>
    	</div>';
    	*/
    
    	$cost_table .= '<div class="total-row">
								<span class="total-title final">Total</span>
								<span class="total-value final grand_total">$'.number_format($vars->grand_total, 2).'</span>
							</div>';
    	return $cost_table;
    }
    
function build_shipping_method_dropdown($shipping_discount=0,$tax_rate='',$zip_code='',$radio_buttons=true,$shipping_options=null,$show_liftgate_options=false){

		global $CFG;
		global $cart;
		global $checkout;
		$zip_code = $zip_code ? $zip_code : $cart->data['zipcode'];

    	$shipping_discount = round($shipping_discount, 2);
		if ($tax_rate == "") $sales_tax_rate_to_use = 0.00;
		else $sales_tax_rate_to_use = $tax_rate;

		//$shipping_costs = Shipping::getShippingOptions($zip_code,'cart','','','','','',$shipping_options); 

		$sel_method = $cart->data['shipping_method'];
		
		$return_str = Shipping::get_shipping_display($shipping_options,$sel_method,$shipping_discount,$sales_tax_rate_to_use,$radio_buttons,$show_liftgate_options);
        		
		return $return_str;  
    }
    
    function get_applied_coupons($original_applied_coupons,$shipping_discount){
    
    	if(is_array($original_applied_coupons)){
    		foreach($original_applied_coupons as $a){
    
    			if($a['invalid_msg']){
    				$promoCodeError .= " " . $a['invalid_msg'];
    			} else {
    				$the_discount_amount = ($a['discount_amount'] != '0.00' ? $a['discount_amount'] : $a['shipping_discount_amount']);
    				$the_discount_type = ($a['discount_amount'] != '0.00' ? $a['discount_type'] : $a['shipping_discount_type']);
    				$the_discount_type = ($a['send_gift'] != 'N' ? $a['send_gift'] : $the_discount_type);
    				$this_overall_discount = ($a['discount_amount'] != '0.00' ? $a['this_overall_discount'] : $shipping_discount);
    
    				switch($a['send_gift']){
    					case 'product':
    						$gift_id = $a['gift_product_id'];
    						break;
    					case 'card':
    						$gift_id = $a['gift_promo_id'];
    						break;
    					case 'N':
    						$gift_id = "";
    						break;
    				}
    
    				$applied_coupons[] = array(
    						'id'=>$a['id'],
    						'description'=>$a['promo_code_description'],
    						'code' => $a['promo_code_code'],
    						'auto_applied' => $a['auto_applied'],
    						'amount' => $the_discount_amount,
    						'type' => $the_discount_type,
    						'this_overall_discount' => $this_overall_discount,
    						'gift_id' => $gift_id);
    			}
    		}
    	}
    	return array('coupons' => $applied_coupons, 'error_msg' => $promoCodeError);
    }
    
    function get_display_coupon_list($applied_coupons, $variation = ''){
    
    	foreach($applied_coupons as $ac){
    		$str .= "<span>";
    			
    		if($ac['type']=='dollars')
    		{
    			$str .= '$' . number_format($ac['amount'],2);
    		}
    		elseif($ac['type']=='card' || $ac['type']=='product'){
    			$str .= "Promotion ";
    		}
    		else
    		{
    			$str .=  number_format($ac['amount']) . '% (' . number_format($ac['this_overall_discount'],2) . ')';
    		}
    		$str .= ' - ' . htmlspecialchars($ac['description']) ;
    			
    		if($ac['auto_applied'] == 'N')
    		{
    			$str .= ' <a href="#" onclick="gtag(\'event\', \'Remove Coupon Code\', {\'event_category\': \'Cart\', \'value\': \'Remove Coupon Code\'});coupon_code_action(\'remove_coupon\','.$ac['id'].',\''.$variation.'\');return false;">(Remove)</a>';
    		}
    		$str .= '</span><br/>';
    			
    	}
    
    	return $str;
    }
	
	function create_cost_breakdown_table_var1($vars){
	
		global $cart, $CFG;
	
		$is_valid = true;
		$invalidZipErr = "";
		if ($_REQUEST['zip_code'])
		{
			$is_valid = ZipCodesUSA::is_zip_code_valid($_REQUEST['zip_code']);
			if (!$is_valid)
			{
				$invalidZipErr = "<span class='redbold'>Zip code entered, " . $_REQUEST['zip_code'] . ", is not a valid US zipcode.</span>";
			}
			else if(ZipCodesUSA::is_zip_code_alaska($_REQUEST['zip_code']))
			{
				$invalidZipErr = "<span class='redbold'>We are sorry, but we cannot take orders shipping to Alaska or Hawaii on our website.</span>";
			}
			else if(ZipCodesUSA::is_zip_code_hawaii($_REQUEST['zip_code']))
			{
				$invalidZipErr = "<span class='redbold'>We are sorry, but we cannot take orders shipping to Alaska or Hawaii on our website.</span>";
			}
		}
	
		$zipcode = $vars->cart2['shipping_zip'] ? $vars->cart2['shipping_zip'] : $cart->data['zipcode'];
	
		$cost_table  = '<div class="total-row sub_total">
						<span class="total-title">Product Total:</span>
						<span class="total-value">$'.number_format($vars->sub_total, 2).'</span>
					</div>';

		$cost_table .= '<div class="total-row shipping_amt">
									<span class="total-title">Shipping:</span>
									<span class="total-value">';
		$cost_table .= ($zipcode) ? ('$'.number_format($vars->shipping_cost, 2)) : 'Waiting for zip code';
		$cost_table .= '</span>
								</div>';
											
		if ($vars->min_charge > 0.0)
		{
			$cost_table .= '<div class="total-row min_charge">
									<span class="total-title">Minimum Surcharge:</span>
									<span class="total-value">$'.number_format($vars->min_charge, 2).'</span>
								</div>';
		}
	
		if($cart->data['shipping_method'] == $CFG->freight_shipping_code || $cart->data['shipping_method_2'] == $CFG->freight_shipping_code)
		{
			//check if any items on the order require liftgate charges
			if($vars->liftgate>0){
				if ($vars->tax_rate == "") $sales_tax_rate_to_use = 0.00;
				else $sales_tax_rate_to_use = $vars->tax_rate;
	
				$liftgate_charge = $cart->data['charge_liftgate_fee'] ? $vars->liftgate : 0.0 ;
				 
			}
		}
	
		if ($vars->liftgate > 0.0)
		{
			$cost_table .= '<div class="total-row liftgate_amt">
									<span class="total-title ">Liftgate Fee:</span>
									<span class="total-value">$'.number_format($vars->liftgate, 2).'</span>
								</div>';
		}
		else $cost_table .= '<span class="liftgate_amt" style="display:none">$'.number_format($vars->liftgate, 2).'</span>';
		if ($vars->coupon_gift > 0.0)
		{
			$cost_table .= '<div class="total-row coupon_gift">
									<span class="total-title">Coupon Code Discount:</span>
									<span class="total-value">- $'.number_format($vars->coupon_gift, 2).'</span>
								</div>';
		}
		else $cost_table .= '<span class="coupon_gift" style="display:none">$'.number_format($vars->coupon_gift, 2).'</span>';
		if ($cart->data['rewards_using'] > 0.0)
		{
			$cost_table .= '<div class="total-row rewards_applied">
									<span class="total-title">Rewards Redeemed:</span>
									<span class="total-value rewards_redeemed" id="rewards_redeemed">- $'.number_format($cart->data['rewards_using'], 2).'</span>
								</div>';
		}
		else $cost_table .= '<span class="rewards_applied" style="display:none">$'.number_format($cart->data['rewards_using'], 2).'</span>';
		$cost_table .= '<div class="total-row sales_tax ';
		$cost_table .= ($vars->sales_tax > 0)? '' : 'hidden' ;
		$cost_table .= '">
									<span class="total-title">Sales Tax:</span>
									<span class="total-value" id="sales_tax">$'.number_format($vars->sales_tax, 2).'</span>'.
							//<br/><br/>		<span class="tax-exempt"><a href="/tax-exempt.html" onclick="window.open(this.href,\'\',\'resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=514,height=199,status\');return false;">Are you tax exempt?</a></span>
									
									'</div>';
	
		$cost_table .= '<div class="total-row final-row">
								<span class="total-title final">Order Total</span>
								<span class="total-value final grand_total">$'.number_format($vars->grand_total, 2).'</span>
							</div>';
		return $cost_table;
	}

    static function getDescriptionForSingleOption($option_id){
       
        return ProductOptionCache::getDescriptionForSingleOption($option_id);
        }
    static function getDescriptionForOptions($options, $product_id = ''){

        return ProductOptionCache::getDescriptionForOptions($options, $product_id);
        }
    static function getSKUForOptions($options, $pid){

        return ProductOptionCache::getSKUForOptions($options, $pid);
        }
    static function getCartLineIdForProduct($session_id, $product_id, $option_id = 0){
        $sql = "SELECT cart.id FROM cart LEFT JOIN cart_options ON cart.id = cart_options.cart_id 
            WHERE product_id = '$product_id' AND session_id = '$session_id' ";
        if($option_id > 0){
            $sql .= " AND product_option_id = '$option_id' ";
        }
        $ret = db_query_array($sql);
        if(sizeof($ret)){
            $line = $ret[0];
            return $line['id'];
        }
        return 0;
    }

    static function optionInCartLine($option, $cart_line_options){
        return strpos(',' . $cart_line_options . ',',','.$option.',') === true;
        }
    static function removeOnePerCartProduct($selected_options, $session_id, $product_id){
        if(empty($selected_options)){
            return;
            }
        $options = explode(',', $selected_options);
        $prods_to_remove = array();
        $all_one_cart_products = array();
        //go through optional options, see if any have a one time product
        $all_one_cart_products = ProductOptionCache::getRelatedProductsForOptions($options, $product_id);
        if(!sizeof($all_one_cart_products)){
            return;
            }
        //now select all cart lines with this option from cart options, if there are
        $sql = "SELECT available_options_id AS options FROM cart_options co JOIN cart c ON co.cart_id = c.id
        WHERE session_id = '$session_id' AND (";
        foreach(array_keys($all_one_cart_products) as $curr){
            $sql .= "LOCATE(',$curr,', CONCAT(',',available_options_id,',')) > 0 OR ";
                }
        $sql = substr($sql, 0, -4) . ')';
        $rows = db_query_array($sql);
        if(sizeof($rows) == 0){
            foreach($all_one_cart_products as $pa_id => $info){
                $prods_to_remove[] = $info;
            }
        }
        else{
            //foreach row, check each available option that has a one per cart product and see if it exists
            foreach($rows as $curr){
                foreach($options_found as $opt => $found){
                    if($found){
                        continue;
                    }
                    $options_found[$opt] = self::optionInCartLine($curr['options'], $opt);
                }
            }
            foreach($options_found as $opt => $is_found){
                if(!$is_found){
                    $prods_to_remove[] = $info;
                }
            }
    }

        //now foreach available option that has a one per cart product, but does not exist, delete the one per cart product cart line
        foreach($prods_to_remove as $info){
           $cart_line_id = self::getCartLineIdForProduct($session_id, $info['product_id'], $info['option_id']);
           if($cart_line_id){
             self::delete($cart_line_id);
            }
        }
     }

      function get_zip_from_ip(){
		$ipaddress = Util::get_client_ip();
		$url = "freegeoip.net/json/$ipaddress";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

		$response = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($response,true);

		if($response['zip_code'] && !$this->data['zipcode']){
			$this->data['zipcode'] = $response['zip_code'];
		}
		return $response['zip_code'];
	}
}
?>
