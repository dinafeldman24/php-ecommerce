<?php
$title = 'Product Export';
	set_time_limit( 0 );
	//This report will create the single feed output, also needs to be able to run automatically to produce feed file
	if( $_REQUEST['type'] )
		$action = $_REQUEST['type'];
	elseif( !isset( $action ) )
		$action = '';
	
	switch( $action )
	{
		case 'create_file':
			set_time_limit(0);
			include_once('application.php');
			generateFeed('file');
			break;
		case 'launch_generator':
			global $CFG;
			set_time_limit(0);
			include_once('application.php');
			include_once('header.php');
			?>
			<div style="font-size:12px;margin:0 auto;padding:5px;width:500px;height:85px;background-color:#FFF;border:2px solid #000">
				<div id="timeElapsed" style="width:500px;height:15px;background-color:#fff;border:1px solid #666;">
					<?$start_time = time()?>
					<iframe src="<?=$CFG->baseurl."edit/product_export.php?type=create_file&starttime=".$start_time?>" width="1px" height="1px"></iframe>
				</div>
				<script type="text/javascript">
					new PeriodicalExecuter(function(pe) {
					  	var url = "ajax.generateFeed.php";
						var div = "timeElapsed";
						var params = "rand=<?=$start_time?>";
						
						var ajax = new Ajax.Updater(
							div,
							url,
							{
								parameters: params,
								method: "post",
								evalScripts: true
							}
						);
					}, 3);
				</script>
				</div>
			</div>
			<?
			include_once('footer.php');
			break;	
		case 'export_feed':
			set_time_limit(0);
			include_once('application.php');
			generateFeed('download');
			break;
		case 'product_reviews':
			include_once('application.php');
			//include_once('header.php');
			generateProductReviewsFeed();
			include_once('header.php');
			showFilterBox('Feed Generated Successfully! : <a href="'.$CFG->baseurl.'feeds/productreviews.txt" target="_blank">'.$CFG->baseurl.'feeds/productreviews.txt</a>');
			include_once('footer.php');
			//include_once('footer.php');
			break;
		case 'create_feed':
			include_once('header.php');
			generateFeed();
			include_once('footer.php');
			break;
		case 'unavailable':
			include_once('application.php');
			unavailableProducts();
			break;
		default:
			include_once('header.php');
			//Should I have a filter box with options?
			//perhaps active/website options
			showFilterBox();
			include_once('footer.php');
	}
	

	
function showFilterBox($message='')
{
	$query = $_GET['query'];
    $letter = $_GET['letter'];  
    $cat_id = $_GET['cat_id'];
	$is_active = $_GET['is_active'];
	$is_available = $_GET['is_available'];
	$brand_id = $_GET['brand_id'];
    $letters = range('A', 'Z');
   
?>

<script type="text/javascript" src="js/cat_array.js"></script>
<script type="text/javascript">
	
	function updateSubs( cat, manualCat, selectedCat )
	{
		if( manualCat )
		{
			var new_cat = manualCat;
			//alert( selectedCat );
			var selected_cat = selectedCat;
		}
		else
		{
			var new_cat = cat.value;
			var selected_cat = 0;
		}
		var sub_length = $('sub_cat').length;
		for( var i = 0; i < sub_length; i++ )
		{
			$('sub_cat').options[i] = null;
		}
		if( new_cat > 0 )
		{
				var new_length = subs[new_cat].length;
			if( new_length > 0 )
				$('sub_cat').length = new_length;
			else
				$('sub_cat').length = 1;
		}
		else
			$('sub_cat').length = 1;
		
		$('sub_cat').options[0] = new Option('All', 0);
		if( new_cat > 0)
			for( var i = 0; i < new_length; i++ )
			{ 
				//alert( new_cat + ' ' + i );
				$('sub_cat').options[i+1] = new Option(subs[new_cat][i]['value'], subs[new_cat][i]['id']);
				if( subs[new_cat][i]['id'] == selected_cat )
				{
					//alert( selected_cat );
					$('sub_cat').options[i+1].selected = true;
				}
			}
	}
</script>
<?
    
    if ($_GET['action'] == 'link_select')
		$linkSelect = '&action=link_select&id='.$_GET['id'];
	else if ($_GET['action'] == 'package_select')
		$linkSelect = '&action=package_select&id='.$_GET['id'];
    ?>
    <form method="GET" name="exportFrm" action="<?= $_SERVER['PHP_SELF'] ?>" id="filter_box">
    <input type="hidden" name="item_numb" value="<?= $_REQUEST['item_numb'] ?>">
		<input type="hidden" name="action" value="create_feed">
   <?=($message) ? '<p style="font-weight:bold">'.$message.'</p>' : ''?></p>
	<fieldset>
		<legend>Filter Products</legend>
		<table border=0 width="99%">
		<tr valign=top>
			<td>
			
			<table border=0>

			<tr>
			<td>
				Show Featured:
			</td>
			<td>
				 			 <input id=isfeaturedAll type="radio" name="is_featured" value=""<?= (!$_REQUEST[is_featured]) ? ' checked' : '' ?>> <label for=isfeaturedAll>ALL</label>
							 <input id=isfeaturedY   type="radio" name="is_featured" value="Y"<?= ($_REQUEST[is_featured] == 'Y') ? ' checked' : '' ?>> <label for=isfeaturedY>Yes</label>
							 <input id=isfeaturedN   type="radio" name="is_featured" value="N"<?= ($_REQUEST[is_featured]== 'N') ? ' checked' : '' ?>> <label for=isfeaturedN>No</label>
			</td>
			</tr>
			<tr>
			<td>
				Show Active:
			</td>
			<td>
				 			 <input id=isActiveAll type="radio" name="is_active" value=""> <label for=isfeaturedAll>ALL</label>
							 <input id=isActiveY   type="radio" name="is_active" value="Y"<?= ($_REQUEST[is_active] == 'Y' || !$_REQUEST[is_active]) ? ' checked' : '' ?>> <label for=isfeaturedY>Yes</label>
							 <input id=isActiveN   type="radio" name="is_active" value="N"<?= ($_REQUEST[is_active]== 'N') ? ' checked' : '' ?>> <label for=isfeaturedN>No</label>
			</td>
			</tr>
			<tr>
			<td>
				Show Website:
			</td>
			<td>
				 			 <input id=isWebsiteAll type="radio" name="website" value=""<?= (!$_REQUEST[website]) ? ' checked' : '' ?>> <label for=isWebsiteAll>ALL</label>
							 <input id=isWebsiteY   type="radio" name="website" value="Y"<?= ($_REQUEST[website] == 'Y') ? ' checked' : '' ?>> <label for=isWebsiteY>Yes</label>
							 <input id=isWebsiteN   type="radio" name="website" value="N"<?= ($_REQUEST[website]== 'N') ? ' checked' : '' ?>> <label for=isWebsitedN>No</label>
			</td>
			</tr>
			<tr>
			<td>
				Show Available:
			</td>
			<td>
				 			 <input id=isAvailableAll type="radio" name="is_available" value=""<?= (!$_REQUEST[is_available]) ? ' checked' : '' ?>> <label for=isAvailableAll>ALL</label>
							 <input id=isAvailableY   type="radio" name="is_available" value="Y"<?= ($_REQUEST[is_available] == 'Y') ? ' checked' : '' ?>> <label for=isAvailableY>Yes</label>
							 <input id=isAvailableN   type="radio" name="is_available" value="N"<?= ($_REQUEST[is_available]== 'N') ? ' checked' : '' ?>> <label for=isAvailableN>No</label>
			</td>
			</tr>
			<tr>
			<td>
				Type:
			</td>
			<td>
				 			 <input id=displayScreen type="radio" name="type" value="create_feed"<?= ($_REQUEST[type] == 'create_feed' ) ? ' checked' : '' ?>> <label for=displayScreen>Display</label>
							 <input id=displayDownload   type="radio" name="type" value="export_feed"<?= ($_REQUEST[type] == 'export_feed' || !$_REQUEST[type]) ? ' checked' : '' ?>> <label for=displayDownload>Download</label><br/>
							 <input id=displayProductReviews   type="radio" name="type" value="product_reviews"<?= ($_REQUEST[type] == 'product_reviews') ? ' checked' : '' ?>> <label for=displayProductReviews>Product Reviews (Download)</label>
			</td>
			</tr>
			</table>
			
			</td>
			
			<td>
			
			<table border=0>
			<!--
			<tr>
				<td>Per Page:</td>
				<td><?// $selectBox ?></td>
			</tr>
			-->
			<tr>
				<td>Main Category:</td>
				<td><select name="cat_id" onchange="updateSubs(this);" >
					<option value=0> ALL
					<?
						$cats = Cats::get(0,'','','','',0);
						
						if(is_array($cats))
						foreach ($cats as $cat) {
							echo "<option value=$cat[id]".($cat[id] == $_REQUEST['cat_id'] ? ' selected' : '')."> $cat[name]";
						}
				
					?>
					</select></td>
			</tr>
			<tr>
				<td>Sub Category:</td>
				<td><select name="sub_cat" id="sub_cat" >
					<option value=0> ALL
					<?
					if( $_REQUEST['cat_id'] )
					{
						outputPHPSubArray( $_REQUEST['cat_id'] );
					}
					?>
					</select></td>
			</tr>
			<? /*
			<tr>
				<td>Theme:</td>
				<td><select name="occ_id" >
					<option value=0> ALL
					<?

						$occs = Occasions::get();
						
						if(is_array($occs))
						foreach ($occs as $occ) {
							echo "<option value=$occ[id]".($occ[id] == $_REQUEST['occ_id'] ? ' selected' : '')."> $occ[name]";
						}
				
					?>
					</select></td>
			</tr>	
			*/ ?>		

			<tr>
				<td>Brand:</td>
				<td><select name="brand_id" >
					<option value=0> ALL
					<?

						$brands = Brands::get();
						if( is_array( $brands ) )
						foreach ($brands as $brand) {
							echo "<option value=$brand[id]".($brand[id] == $_REQUEST['brand_id'] ? ' selected' : '')."> $brand[name]";
						}
				
					?>
					</select></td>
			</tr>
			<tr>
				<td colspan="2"><a href="product_export.php?type=launch_generator" onclick="return createFile()">Create New Singlefeed File</td>
				<script type="text/javascript">
					function createFile(){
						return true;
					}
				</script>
			</tr>
			
			<tr rowspan="3">
				<td colspan="2"><div id="cat_filter_box"></div></td>
			</tr>
			</table>
						
			</td>
			<td><input type="button" onclick="checkFrm()" value="filter" name="filter_button"></td>
		</tr>
		</table>
	</fieldset>

	</form>
	<script type="text/javascript">
		function checkFrm(){
			if($('displayProductReviews').checked){
				var answer = confirm("This export will overwrite the contents of productreviews.txt.\n\nAre you sure you want to continue?");
				if(answer){
					document.exportFrm.submit();
				}
			}
			else{
				document.exportFrm.submit();
			}
		}
	</script>
	<?
}


function outputSubArray()
{
	$main_cats = Cats::get(0,'','','','',0);
	$main_count = count( $main_cats );
	echo'var subs= [];';
	$count = 0;
	foreach( $main_cats as $cat )
	{
		echo'subs['.$cat['id'].'] = [];';
		$count = outputSubs( $cat['id'], $cat['id'], $count );
	}
}

function outputSubs( $cat_id, $main_cat, $count=0, $level = 1 )
{
	$subs = Cats::get(0,'','','','',$cat_id);

	if( is_array( $subs ) )
	{
		foreach( $subs as $sub )
		{
			$name = trim($sub['name']);
			for( $i=1; $i < $level; $i++ )
			{
				$name = "-" . $name;
			}
			echo'subs[' . $main_cat .']['. $count .'] = [];';
			echo'subs[' . $main_cat .']['. $count .']["id"] = '. $sub['id'] . ';';
			echo'subs[' . $main_cat .']['. $count .']["value"] = \''. ($name) .'\';';
			echo"\n";

			$count++;
			$next = Cats::get(0,'','','','',$sub['id']);
			
			if( is_array( $next ) )
			{
				$count = outputSubs( $sub['id'], $main_cat, $count, $level+1 );
			}
		}
	}
	
		return $count;
}

function outputPHPSubArray( $cat_id )
{
	$count = 0;

	$count = outputPHPSubs( $cat_id, $cat_id, $count );
}

function outputPHPSubs( $cat_id, $main_cat, $count=0, $level = 1 )
{
	$subs = Cats::get(0,'','','','',$cat_id);

	if( is_array( $subs ) )
	{
		foreach( $subs as $sub )
		{
			$name = $sub['name'];
			for( $i=1; $i < $level; $i++ )
			{
				$name = "-" . $name;
			}
			echo "<option value=$sub[id]".($sub[id] == $_REQUEST['sub_cat'] ? ' selected' : '')."> $name";

			$count++;
			$next = Cats::get(0,'','','','',$sub['id']);
			
			if( is_array( $next ) )
			{
				$count = outputPHPSubs( $sub['id'], $main_cat, $count, $level+1 );
			}
		}
	}
	
		return $count;
}

function generateFeed( $type = 'screen' )
{
	global $CFG;
	$CFG->dont_store_qs_queries = true;
	if( $_GET['sub_cat'] > 0 )
		$cat_id = $_GET['sub_cat'];
	else
		$cat_id = $_GET['cat_id'];
	$brand_id = $_GET['brand_id'];

	$is_active = $_GET['is_active'];
	if ($is_active == 'Y') $is_active = true;
	else if ($is_active == 'N') $is_active = false;
	else  $is_active = '';

	$is_available = $_GET['is_available'];
	/*if ($is_available == 'Y') $is_available = true;
	else if ($is_available == 'N') $is_available = false;
	else  $is_available = '';
	*/

	$website = $_GET['website'];
	if ($website == 'Y') $website = true;
	else if ($website == 'N') $website = false;
	else  $website = '';

	$is_featured = $_GET['is_website'];
	if ($is_featured == 'Y') $is_featured = true;
	else if ($is_featured == 'N') $is_featured = false;
	else  $is_featured = '';

	if( $type == 'file' )
	{
		$is_active = 'Y';
		$is_available = 'Y';
		$is_featured = '';
		$website = '';
	}

	switch( $type )
	{
		case 'file':
			//don't break;
			$i=0;
		case'download':
			global $CFG;
			$filename="GoogleBase" . date('Ymd');

			$limit = 500;
			$start = 0;

			if($type == "file"){
				//had issues with tmpfile
				$dirname = "feeds";
				$_REQUEST['starttime'] = $_REQUEST['starttime'] > 0 ? $_REQUEST['starttime'] : time();
				$temp_filename = $CFG->dirroot."/".$dirname."/google_temp_".$_REQUEST['starttime'].".txt";
				$data_file = fopen($temp_filename,"w");
				$temp_status = "$CFG->dirroot/".$dirname."/status_".$_REQUEST['starttime'].".txt";
				$status_file = fopen($temp_status,"w");
				$starttime = time();
				$data = "link\ttitle\tdescription\timage_link\tprice\tid\tcondition\tbrand\tpayment_notes\tproduct_type\tweight\tshipping\theight\twidth\tcapacity\r\n";
				fputs($data_file,$data);
			}else{
				header('Date: ' . date("D M j G:i:s T Y"));
				header('Last-Modified: ' . date("D M j G:i:s T Y"));
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				header("Content-Type: text/plain");
				header("Content-Disposition: attachment; filename=\"$filename.txt\"");
				//echo"SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tPROMOTIONAL MESSAGE\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
				$data = "link\ttitle\tdescription\timage_link\tprice\tid\tcondition\tbrand\tpayment_notes\tproduct_type\tweight\tshipping\theight\twidth\tcapacity\r\n";
				echo $data;
			}

			//echo"SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tMUZE ID\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tSHIPPING WEIGHT\tPROMOTIONAL MESSAGE\tFORMAT\tGENDER\tDEPARTMENT\tSIZE\tCOLOR\tAGE RANGE\tAGE GROUP\tAVAILABILITY\tSTOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)\tNRF SIZE\tNRF COLOR\tDISTRIBUTOR ID\tINGRAM PART #\tASIN\tZIP CODE\tOUTLET\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tPUBLISHER\tMOVIE DIRECTOR\tACTOR\tARTIST\tPICKUP\tPRICE TYPE\tTAX PERCENT\tTAX REGION\tMADE_IN\tMATERIAL\tSTYLE\tBINDING\tEDITION\tGENRE\tPAGES\tFILM TYPE\tFOCUS TYPE\tRESOLUTION\tZOOM\tMEGAPIXELS\tMEMORY\tPROCESSOR SPEED\tWIRELESS INTERFACE\tBATTERY LIFE\tCAPACITY\tOPERATING SYSTEM\tOPTICAL DRIVE\tRECOMMENDED USAGE\tSCREEN SIZE\tASPECT RATIO\tDISPLAY TYPE\tCOLOR OUTPUT\tMEMORY CARD SLOT\tLOAD TYPE\tFEATURE\tFUNCTIONS\tTECH SPECIFICATION LINK\tHEIGHT\tLENGTH\tWIDTH\tINSTALLATION\tOCCASION\tHEEL HEIGHT\tSHOE WIDTH\tGAME PLATFORM\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tPRONTO URL REPLACEMENT\tSHOPPING.COM URL REPLACEMENT\tSHOPZILLA URL REPLACEMENT\tSMARTER URL REPLACEMENT\tTHEFIND URL REPLACEMENT\tYAHOO SHOPPING URL REPLACEMENT\tGOOGLE BASE PRODUCT NAME REPLACEMENT\tGOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
						do {

			$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
			$count = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',true,'',$website, '', $limit, $start,'','',$is_available );
			$count = $count[0]['total'];
			
			if( is_array( $prods ) )
				foreach( $prods as $prod )
				{
					if($prod['include_google'] !== "N"){
						if($type == "file"){
							ob_start();
							tabSeperateGoogle( $prod, $is_available );
							$row = ob_get_contents();
							ob_end_clean();
							fputs($data_file,$row);
							$i++;
							if($i%17 == 0){
								file_put_contents($temp_status,$i.",".$count.",".$starttime.",".time());
							}
						}else{
							tabSeperateGoogle( $prod, $is_available );
						}
						$counter++;
					}
					unset($prod);
				}

				$start += $limit;
			} while ( $prods );

			if($type == "file"){
				$final_file = fopen("$CFG->dirroot/".$dirname."/google_base.txt","w");
				copy($temp_filename,"$CFG->dirroot/".$dirname."/google_base.txt");
				unlink($temp_filename);
				unlink($temp_status);
				fclose($data_file);
				echo 'Success! <a href="'.$CFG->baseurl.'/'.$dirname.'/google_base.txt">'.$CFG->baseurl.'/'.$dirname.'/google_base.txt</a>';
				file_put_contents("$CFG->dirroot/".$dirname."/status.txt","complete");
			}
			break;
		case 'screen':
			showFilterBox();

			//output data to screen
			$urlBegin = 'product_export.php?1';
			$hth = new HtmlTableHeading($urlBegin);

			$hth->addItem('','SF PRODUCT TYPE');
			$hth->addItem('','MPN (MANUFACTURER PART NUMBER)');
			$hth->addItem('','ISBN (IF BOOK)');
			$hth->addItem('','UPC (UNIVERSAL PRODUCT CODE)');
			$hth->addItem('','UNIQUE INTERNAL CODE');


			echo '<table cellspacing=1 cellpadding=5 class=listing >
			<tr class=listing_heading>';
			$hth->show();
			echo'</tr>';

			$start = 0;
			$limit = 1000;
			do {
				$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
				if($prods){
					foreach( $prods as $prod )
					{
						if($prod['include_google'] !== "N" ){
							$i = displayProductRowGoogle( $prod, $i, $is_available );
						}
					}
				}
				$start += $limit;
			} while( $prods );
			echo'</table>';
			break;
	}

}

function generateSingleFeed( $type='screen' )
{	
	global $CFG;
	$CFG->dont_store_qs_queries = true;
	if( $_GET['sub_cat'] > 0 )
		$cat_id = $_GET['sub_cat'];
	else 
		$cat_id = $_GET['cat_id'];
	$brand_id = $_GET['brand_id'];	
	
	$is_active = $_GET['is_active'];
	if ($is_active == 'Y') $is_active = true;
	else if ($is_active == 'N') $is_active = false;
	else  $is_active = '';

	$is_available = $_GET['is_available'];
	/*if ($is_available == 'Y') $is_available = true;
	else if ($is_available == 'N') $is_available = false;
	else  $is_available = '';
	*/
	
	$website = $_GET['website'];
	if ($website == 'Y') $website = true;
	else if ($website == 'N') $website = false;
	else  $website = '';	
	
	$is_featured = $_GET['is_website'];
	if ($is_featured == 'Y') $is_featured = true;
	else if ($is_featured == 'N') $is_featured = false;
	else  $is_featured = '';	
	
	if( $type == 'file' )
	{
		$is_active = 'Y';
		$is_available = 'Y';
		$is_featured = '';
		$website = '';
	}	
	
	switch( $type )
	{
		case 'file':
			//don't break;
			$i=0; 
		case'download':
			global $CFG;
			$filename="Singlefeed" . date('Ymd');
			
			$limit = 500;
			$start = 0;
			
			if($type == "file"){
				//had issues with tmpfile
				$dirname = "feeds";
				$_REQUEST['starttime'] = $_REQUEST['starttime'] > 0 ? $_REQUEST['starttime'] : time();
				$temp_filename = $CFG->dirroot."/".$dirname."/singlefeed_temp_".$_REQUEST['starttime'].".txt";
				$data_file = fopen($temp_filename,"w");
				$temp_status = "$CFG->dirroot/".$dirname."/status_".$_REQUEST['starttime'].".txt";
				$status_file = fopen($temp_status,"w");
				$starttime = time();
				$data = "SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tPROMOTIONAL MESSAGE\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
				fputs($data_file,$data);				
			}else{
				header('Date: ' . date("D M j G:i:s T Y"));
				header('Last-Modified: ' . date("D M j G:i:s T Y"));
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				header("Content-Type: text/plain");
				header("Content-Disposition: attachment; filename=\"$filename.txt\"");	
				echo"SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tPROMOTIONAL MESSAGE\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
			}	
			
			//echo"SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tMUZE ID\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tSHIPPING WEIGHT\tPROMOTIONAL MESSAGE\tFORMAT\tGENDER\tDEPARTMENT\tSIZE\tCOLOR\tAGE RANGE\tAGE GROUP\tAVAILABILITY\tSTOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)\tNRF SIZE\tNRF COLOR\tDISTRIBUTOR ID\tINGRAM PART #\tASIN\tZIP CODE\tOUTLET\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tPUBLISHER\tMOVIE DIRECTOR\tACTOR\tARTIST\tPICKUP\tPRICE TYPE\tTAX PERCENT\tTAX REGION\tMADE_IN\tMATERIAL\tSTYLE\tBINDING\tEDITION\tGENRE\tPAGES\tFILM TYPE\tFOCUS TYPE\tRESOLUTION\tZOOM\tMEGAPIXELS\tMEMORY\tPROCESSOR SPEED\tWIRELESS INTERFACE\tBATTERY LIFE\tCAPACITY\tOPERATING SYSTEM\tOPTICAL DRIVE\tRECOMMENDED USAGE\tSCREEN SIZE\tASPECT RATIO\tDISPLAY TYPE\tCOLOR OUTPUT\tMEMORY CARD SLOT\tLOAD TYPE\tFEATURE\tFUNCTIONS\tTECH SPECIFICATION LINK\tHEIGHT\tLENGTH\tWIDTH\tINSTALLATION\tOCCASION\tHEEL HEIGHT\tSHOE WIDTH\tGAME PLATFORM\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tPRONTO URL REPLACEMENT\tSHOPPING.COM URL REPLACEMENT\tSHOPZILLA URL REPLACEMENT\tSMARTER URL REPLACEMENT\tTHEFIND URL REPLACEMENT\tYAHOO SHOPPING URL REPLACEMENT\tGOOGLE BASE PRODUCT NAME REPLACEMENT\tGOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
						do {
			
			$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
			$count = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',true,'',$website, '', $limit, $start,'','',$is_available );
			$count = $count[0]['total'];
			if( is_array( $prods ) )
				foreach( $prods as $prod )
				{
					if($prod['include_singlefeed'] !== "N"){
						if($type == "file"){
							ob_start();
							tabSeperate( $prod, $is_available );
							$row = ob_get_contents();
							ob_end_clean();
							fputs($data_file,$row);
							$i++;
							if($i%17 == 0){
								file_put_contents($temp_status,$i.",".$count.",".$starttime.",".time());
							}
						}else{
							tabSeperate( $prod, $is_available );
						}
						$counter++;
					}
					unset($prod);
				}
			
				$start += $limit;
			} while ( $prods );
			
			if($type == "file"){
				$final_file = fopen("$CFG->dirroot/".$dirname."/singlefeed.txt","w");
				copy($temp_filename,"$CFG->dirroot/".$dirname."/singlefeed.txt");
				unlink($temp_filename);
				unlink($temp_status);
				fclose($data_file);
				echo 'Success! <a href="'.$CFG->baseurl.'/'.$dirname.'/singlefeed.txt">'.$CFG->baseurl.'/'.$dirname.'/singlefeed.txt</a>';
				file_put_contents("$CFG->dirroot/".$dirname."/status.txt","complete");
			}
			break;
		case 'screen':
			showFilterBox();
			
			//output data to screen
			$urlBegin = 'product_export.php?1';
			$hth = new HtmlTableHeading($urlBegin);

			$hth->addItem('','SF PRODUCT TYPE');
			$hth->addItem('','MPN (MANUFACTURER PART NUMBER)');
			$hth->addItem('','ISBN (IF BOOK)');
			$hth->addItem('','UPC (UNIVERSAL PRODUCT CODE)');
			$hth->addItem('','UNIQUE INTERNAL CODE');
			//$hth->addItem('','MUZE ID');
			$hth->addItem('','PRODUCT NAME');
			$hth->addItem('','PRODUCT DESCRIPTION');
			$hth->addItem('','PRODUCT PRICE');
			$hth->addItem('','PRODUCT URL');
			$hth->addItem('','IMAGE URL');
			$hth->addItem('','PRODUCT TYPE');
			$hth->addItem('','CATEGORY');
			$hth->addItem('','MANUFACTURER');
			$hth->addItem('','STOCK STATUS');
			$hth->addItem('','CONDITION (IF NOT NEW)');
			$hth->addItem('','PAYMENT TYPE');
			$hth->addItem('','KEYWORDS');
			$hth->addItem('','MSRP');
			$hth->addItem('','SALE PRICE');
			$hth->addItem('','SHIPPING COST');
			//$hth->addItem('','SHIPPING WEIGHT');
			$hth->addItem('','PROMOTIONAL MESSAGE');
			//$hth->addItem('','FORMAT');
			//$hth->addItem('','GENDER');
			//$hth->addItem('','DEPARTMENT');
			//$hth->addItem('','SIZE');
			//$hth->addItem('','COLOR');
			//$hth->addItem('','AGE RANGE');
			//$hth->addItem('','AGE GROUP');
			//$hth->addItem('','AVAILABILITY');
			//$hth->addItem('','STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)');
			//$hth->addItem('','NRF SIZE');
			//$hth->addItem('','NRF COLOR');
			//$hth->addItem('','DISTRIBUTOR ID');
			//$hth->addItem('','INGRAM PART #');
			//$hth->addItem('','ASIN');
			//$hth->addItem('','ZIP CODE');
			//$hth->addItem('','OUTLET');
			$hth->addItem('','CURRENCY');
			$hth->addItem('','DELIVERY NOTES');
			$hth->addItem('','AUTHOR');
			//$hth->addItem('','PUBLISHER');
			//$hth->addItem('','MOVIE DIRECTOR');
			//$hth->addItem('','ACTOR');
			//$hth->addItem('','ARTIST');
			//$hth->addItem('','PICKUP');
			//$hth->addItem('','PRICE TYPE');
			//$hth->addItem('','TAX PERCENT');
			//$hth->addItem('','TAX REGION');
			//$hth->addItem('','MADE_IN');
			//$hth->addItem('','MATERIAL');
			//$hth->addItem('','STYLE');
			//$hth->addItem('','BINDING');
			//$hth->addItem('','EDITION');
			//$hth->addItem('','GENRE');
			//$hth->addItem('','PAGES');
			//$hth->addItem('','FILM TYPE');
			//$hth->addItem('','FOCUS TYPE');
			//$hth->addItem('','RESOLUTION');
			//$hth->addItem('','ZOOM');
			//$hth->addItem('','MEGAPIXELS');
			//$hth->addItem('','MEMORY');
			//$hth->addItem('','PROCESSOR SPEED');
			//$hth->addItem('','WIRELESS INTERFACE');
			//$hth->addItem('','BATTERY LIFE');
			//$hth->addItem('','CAPACITY');
			//$hth->addItem('','OPERATING SYSTEM');
			//$hth->addItem('','OPTICAL DRIVE');
			//$hth->addItem('','RECOMMENDED USAGE');
			//$hth->addItem('','SCREEN SIZE');
			//$hth->addItem('','ASPECT RATIO');
			//$hth->addItem('','DISPLAY TYPE');
			//$hth->addItem('','COLOR OUTPUT');
			//$hth->addItem('','MEMORY CARD SLOT');
			//$hth->addItem('','LOAD TYPE');
			//$hth->addItem('','FEATURE');
			//$hth->addItem('','FUNCTIONS');
			//$hth->addItem('','TECH SPECIFICATION LINK');
			//$hth->addItem('','HEIGHT');
			//$hth->addItem('','LENGTH');
			//$hth->addItem('','WIDTH');
			//$hth->addItem('','INSTALLATION');
			//$hth->addItem('','OCCASION');
			//$hth->addItem('','HEEL HEIGHT');
			//$hth->addItem('','SHOE WIDTH');
			//$hth->addItem('','GAME PLATFORM');
			$hth->addItem('','QUANTITY');
			$hth->addItem('','GOOGLE EXPIRATION DATE');
			$hth->addItem('','GOOGLE EXPIRATION DATE AUTO RENEW');
			$hth->addItem('','BECOME CATEGORY');
			$hth->addItem('','NEXTAG CATEGORY');
			$hth->addItem('','PRICEGRABBER CATEGORY');
			$hth->addItem('','PRONTO CATEGORY');
			$hth->addItem('','SHOPPING.COM CATEGORY');
			$hth->addItem('','SHOPZILLA CATEGORY');
			$hth->addItem('','SMARTER CATEGORY');
			$hth->addItem('','BECOME URL REPLACEMENT');
			$hth->addItem('','GOOGLE BASE URL REPLACEMENT');
			$hth->addItem('','NEXTAG URL REPLACEMENT');
			$hth->addItem('','PRICEGRABBER URL REPLACEMENT');
			//$hth->addItem('','PRONTO URL REPLACEMENT');
			//$hth->addItem('','SHOPPING.COM URL REPLACEMENT');
			//$hth->addItem('','SHOPZILLA URL REPLACEMENT');
			//$hth->addItem('','SMARTER URL REPLACEMENT');
			//$hth->addItem('','THEFIND URL REPLACEMENT');
			//$hth->addItem('','YAHOO SHOPPING URL REPLACEMENT');
			$hth->addItem('','GOOGLE BASE PRODUCT NAME REPLACEMENT');
			$hth->addItem('','GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT');
			$hth->addItem('','INCLUDE YAHOO! SHOPPING');
			$hth->addItem('','INCLUDE SHOPZILLA');
			$hth->addItem('','INCLUDE BECOME');
			$hth->addItem('','INCLUDE PRICEGRABBER');
			$hth->addItem('','INCLUDE SMARTER');
			$hth->addItem('','INCLUDE PRONTO');
			$hth->addItem('','INCLUDE NEXTAG');
			$hth->addItem('','INCLUDE SHOPPING.COM');

			
			echo '<table cellspacing=1 cellpadding=5 class=listing >
			<tr class=listing_heading>';
			$hth->show();
			echo'</tr>';	
				
			$start = 0;
			$limit = 1000;
			do {
				$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
				if($prods){
					foreach( $prods as $prod )
					{
						if($prod['include_singlefeed'] !== "N" && Products::getProductMarkup($prod) > 9){
							$i = displayProductRow( $prod, $i, $is_available );
						}
					}
				}
				$start += $limit;
			} while( $prods );	
			echo'</table>';
			break;
		/*case 'file':
			//output to file
			
			$data_file = fopen("$CFG->dirroot/feeds/singlefeed.txt","w");

			$data = "SF PRODUCT TYPE\tMPN (MANUFACTURER PART NUMBER)\tISBN (IF BOOK)\tUPC (UNIVERSAL PRODUCT CODE)\tUNIQUE INTERNAL CODE\tPRODUCT NAME\tPRODUCT DESCRIPTION\tPRODUCT PRICE\tPRODUCT URL\tIMAGE URL\tPRODUCT TYPE\tCATEGORY\tMANUFACTURER\tSTOCK STATUS\tCONDITION (IF NOT NEW)\tPAYMENT TYPE\tKEYWORDS\tMSRP\tSALE PRICE\tSHIPPING COST\tPROMOTIONAL MESSAGE\tCURRENCY\tDELIVERY NOTES\tAUTHOR\tQUANTITY\tGOOGLE EXPIRATION DATE\tGOOGLE EXPIRATION DATE AUTO RENEW\tBECOME CATEGORY\tNEXTAG CATEGORY\tPRICEGRABBER CATEGORY\tPRONTO CATEGORY\tSHOPPING.COM CATEGORY\tSHOPZILLA CATEGORY\tSMARTER CATEGORY\tBECOME URL REPLACEMENT\tGOOGLE BASE URL REPLACEMENT\tNEXTAG URL REPLACEMENT\tPRICEGRABBER URL REPLACEMENT\tINCLUDE YAHOO! SHOPPING\tINCLUDE SHOPZILLA\tINCLUDE BECOME\tINCLUDE PRICEGRABBER\tINCLUDE SMARTER\tINCLUDE PRONTO\tINCLUDE NEXTAG\tINCLUDE SHOPPING.COM\t\n";
			fputs($data_file,$data);
			
			$start = 0;			
			$limit = 3000 / 10;
						
			do {
			
			$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
			
			if( is_array( $prods ) )
				foreach( $prods as $prod )
				{			
					if($prod['include_singlefeed'] !== "N" && Products::getProductMarkup($prod) > 9){		
						ob_start();
						tabSeperate( $prod );
						$data = ob_get_clean();
						fputs($data_file,$data);
					}
				}
			
				$start += $limit;
				
			} while ( $prods );
			
			fclose($data_file);
			
			break;*/
	}
	
}

function generateProductReviewsFeed( ){
	global $CFG;
	$CFG->dont_store_qs_queries = true;
	if( $_GET['sub_cat'] > 0 )
		$cat_id = $_GET['sub_cat'];
	else 
		$cat_id = $_GET['cat_id'];
	$brand_id = $_GET['brand_id'];	
	
	$is_active = $_GET['is_active'];
	if ($is_active == 'Y') $is_active = true;
	else if ($is_active == 'N') $is_active = false;
	else  $is_active = '';
	
	$is_available = $_GET['is_available'];
	/*if ($is_available == 'Y') $is_available = true;
	else if ($is_available == 'N') $is_available = false;
	else  $is_available = '';
	*/	
	
	$website = $_GET['website'];
	if ($website == 'Y') $website = true;
	else if ($website == 'N') $website = false;
	else  $website = '';	
	
	$is_featured = $_GET['is_website'];
	if ($is_featured == 'Y') $is_featured = true;
	else if ($is_featured == 'N') $is_featured = false;
	else  $is_featured = '';	
	
	if( $type == 'file' )
	{
		$is_active = 'Y';
		$is_featured = '';
		$website = '';
	}	
	
		//$filename="product_reviews";
		
		$limit = 500;
		$start = 0;
		/*
		header('Date: ' . date("D M j G:i:s T Y"));
		header('Last-Modified: ' . date("D M j G:i:s T Y"));
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Content-Type: text/plain");
		header("Content-Disposition: attachment; filename=\"$filename.txt\"");		
		*/
		
		$data_file = fopen("$CFG->dirroot/feeds/productreviews.txt","w");
		$data = "link\tid\tbrand\ttitle\tdescription\timage_link\tprice\tcategory\tquantity\tmodel_number\tupc\tadd_to_cart_link\r\n";
		fputs($data_file,$data);
		
		do {
		$prods = Products::get(0,'','','','',$cat_id,$is_active,'',$is_featured,0,$brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'',$website, '', $limit, $start,'','',$is_available );
		if( is_array( $prods ) )
			foreach( $prods as $row )
			{
				unset($output_row);
				$options = Products::getOptionsForProduct(  $row['id'] );
				if(!$options ){
					$product_url = Catalog::makeProductLink_( $row, true );
					$output_row = $product_url . "\t"; //link
					$output_row .= $row['id'] . "\t"; //id
					$output_row .= clean_value($row['brand_name']) . "\t"; //brand
					$output_row .= clean_value(StdLib::addEllipsis($row['name'],475)) . "\t"; //title
					$output_row .= clean_value($row['description']) . "\t"; //description
					$image_url = $CFG->baseurl . 'itempics/' . $row['id'] . $CFG->xlarge_suffix;
					$output_row .= $image_url . "\t"; //image_link
					
					if( $row['is_featured'] == 'Y' )
					{
						$orig_price = $row['price'];
						$row['price'] = $row['retail_price'];
						$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t";  //price
						$row['price'] = $orig_price;
					}
					else 
						$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t";  //price
					
					$cat_info = Products::getProductCatTree( $row['id'] );
					$main = count( $cat_info );
					$main_cat = Cats::get1($cat_info[$main-1]['value']);
					if( !$main_cat['name'] )
						$main_cat['name'] = 'Home';
					$cat_string = 'Home ';
					if($cat_info){
						foreach( $cat_info as $cat )
						{
							$c = Cats::get1( $cat['value'] );
							$cat_string .= '> ' . $c['name'] . ' ';
						}
					}
						
					$output_row .= clean_value( $cat_string ). "\t";  //category
					$output_row .= $row['is_active'] == 'Y' ? "1\t" : "0\t";  //quantity
					$output_row .= $row['vendor_sku']."\t";  //model number
					$output_row .= $row['upc']."\t";  //upc
					$output_row .= $CFG->baseurl."cart.php?action=add&product_id=".$row['id']."\t";  //add to cart link
					$data = $output_row."\r\n";
					fputs($data_file,$data);
					unset($row);
				}
				else{
					foreach($options as $opt){
						if($opt['id'] > 0 && $row['id'] > 0){
							$product_url = Catalog::makeProductLink_( $opt, true );
							$output_row = $product_url . "\t"; //link
							$output_row .= $row['id'] . "\t"; //id
							$row['description'] = str_replace( $row['vendor_sku'], $opt['vendor_sku'] . ' ' . ucwords( $opt['value'] ), $row['description'] );   	
							$output_row .= clean_value($row['brand_name']) . "\t"; //brand
							$output_row .= clean_value(StdLib::addEllipsis($row['name'],475)) . "\t"; //title
							$output_row .= clean_value($row['description']) . "\t"; //description
							$image_url = $CFG->baseurl . 'optionpics/' . $opt['id'] . $CFG->xlarge_suffix;
							
							$output_row .= $image_url . "\t"; //image_link
							
							$orig_price = $opt['additional_price'];
							$row['additional_price'] = $opt['additional_price'];
							$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t";  //price
							$row['additional_price'] = $orig_price;
									
							$cat_info = Products::getProductCatTree( $row['id'] );
							$main = count( $cat_info );
							$main_cat = Cats::get1($cat_info[$main-1]['value']);
							if( !$main_cat['name'] )
								$main_cat['name'] = 'Home';
							$cat_string = 'Home ';
							if($cat_info){
								foreach( $cat_info as $cat )
								{
									$c = Cats::get1( $cat['value'] );
									$cat_string .= '> ' . $c['name'] . ' ';
								}
							}
								
							$output_row .= clean_value( $cat_string ). "\t";  //category
					
							$output_row .= $row['is_active'] == 'Y' ? "1\t" : "0\t";  //quantity
							$output_row .= $opt['vendor_sku']."\t";  //model number
							$output_row .= $row['upc']."\t";  //upc
							$output_row .= $CFG->baseurl."cart.php?action=add&product_id=".$row['id']."\t";  //add to cart link
							
							$data = $output_row."\r\n";
							fputs($data_file,$data);
						}
					}
				}
			}
		
			$start += $limit;
		} while ( $prods );
		
		fclose($data_file);
}

function displayProductRowGoogle( $row, $numb, $show_available="" )
{
    global $CFG;

	if($show_available !== ""){ //if what you want for is available isn't what you get, don't show the row
		if($show_available != $row['is_available']){
			return false;
		}
	}

	$options = Products::getOptionsForProduct( $row['id'] );
	if( $numb % 2 == 0 )
		$class = 'listing';
	else
		$class = 'listingb';
	$numb++;

	
}

function displayProductRow( $row, $numb, $show_available="" )
{
	global $CFG;
	
	if($show_available !== ""){ //if what you want for is available isn't what you get, don't show the row
		if($show_available != $row['is_available']){
			return false;
		}
	}
	
	$options = Products::getOptionsForProduct( $row['id'] );
	if( $numb % 2 == 0 )
		$class = 'listing';
	else 
		$class = 'listingb';
	$numb++;	
	echo'<tr class="' . $class .'" >';
	echo'<td> A </td>';												 //A SF PRODUCT TYPE
	echo'<td>' . clean_value( $row['vendor_sku'] ) . '</td>'; //B MPN
	echo'<td></td>';													 //C ISBN
	echo'<td>' . clean_value( $row['upc'] ) . '</td>';					 //D UPC
	echo'<td>' . clean_value($row['vendor_sku'] ) . '</td>'; //E UNIQUE INTERNAL CODE
	$name = clean_value( $row['name'] );
	//echo'<td></td>';													 //F MUZE ID
	echo'<td>' . $name . '</td>';			 				 //G PRODUCT NAME
	
	if( $row['description'] )
		$desc = clean_value($row['description']);
	else 
		$desc = clean_value($row['name']);
	echo'<td>' . $desc . '</td>';							 //H PRODUCT DESCRIPTION
	
	//Markup was changed, need to modify this to get the right price for the different prices (sale/normal)
	//$markup = Products::getProductMarkup( $row );
//This is a parent product so I need to either list the lowest price of the options, or if no options just the price given
	if( $options )
	{
		//The options are being order by price ASC by default, so grab the first one and output that price here
		if( $row['is_featured'] == 'Y')
		{
			$orig_price = $options[0]['additional_price'];
			$row['additional_price'] = $options[0]['retail_price'];
			echo'<td>' . number_format( (Products::getProductMarkup($row) ), 2 ) . '</td>';  //I PRODUCT PRICE
			$row['additional_price'] = $orig_price;
		}
		else 
		{
			$orig_price = $options[0]['additional_price'];
			$row['additional_price'] = $options[0]['additional_price'];
			echo'<td>' . number_format( (Products::getProductMarkup($row) ), 2 ) . '</td>';  //I PRODUCT PRICE
			$row['additional_price'] = $orig_price;
		}
	}
	else 
	{
		if( $row['is_featured'] == 'Y' )
		{
			$orig_price = $row['price'];
			$row['price'] = $row['retail_price'];
			echo'<td>' .  number_format( Products::getProductMarkup($row), 2 ) . '</td>';    //I PRODUCT PRICE
			$row['price'] = $orig_price;
		}
		else 
			echo'<td>' . number_format( Products::getProductMarkup($row), 2 ) . '</td>';     //I PRODUCT PRICE
			//echo'<td>' . number_format( ($row['price'] * ($markup/100 + 1) ), 2 ) . '</td>';
	}
	$product_url = Catalog::makeProductLink_( $row, true );
	$image_url = $CFG->baseurl . 'itempics/' . $row['id'] . $CFG->xlarge_suffix;
	echo'<td>' . $product_url . '</td>'; //Product URL   	   //J PRODUCT URL
	echo'<td>' . $image_url . '</td>'; //Product Image URL   //K IMAGE URL
	
	$cat_info = Products::getProductCatTree( $row['id'] );
	$main = count( $cat_info );
	$main_cat = Cats::get1($cat_info[$main-1]['value']);
	if( !$main_cat['name'] )
		$main_cat['name'] = 'Home';
	$cat_string = 'Home ';
	foreach( $cat_info as $cat )
	{
		$c = Cats::get1( $cat['value'] );
		$cat_string .= '> ' . $c['name'] . ' ';
	}
	//$cat_structure = Cats::cats2Structure( $cats );

	echo'<td>' . $main_cat['name'] . '</td>'; //L Product Type, lowest lvl category
	echo'<td>' . $cat_string . '</td>'; //M Category Tree
	
	$brand = Brands::get1( $row['brand_id'] );
	echo'<td>' . $brand['name'] . '</td>'; //N Brand
	echo'<td> Y </td>'; //O Stock Status
	echo'<td> NEW </td>'; //P Condition
	echo'<td> Visa, Mastercard, PayPal, Check, Money Order, American Express, Discover </td>'; //Q Payment Type
	echo'<td>' . $row['keywords'] . '</td>'; //R Keywords
	
	echo'<td></td>'; //S MSRP

//Sale Price
	if( $options )
	{
		//The options are being order by price ASC by default, so grab the first one and output that price here
		if( $row['is_featured'] == 'Y' )
		{
			$orig_price = $options[0]['additional_price'];
			$row['additional_price'] = $options[0]['additional_price'];
			echo'<td>' . number_format( (Products::getProductMarkup($row) ), 2 ) . '</td>'; //T SALE PRICE
			$row['additional_price'] = $orig_price;
		}
		else 
			echo'<td></td>'; //T SALE PRICE
	}
	else 
	{
		if( $row['is_featured'] == 'Y' )
		{
			echo'<td>' .  number_format( Products::getProductMarkup($row), 2 ) . '</td>'; //T SALE PRICE
		}
		else 
			echo'<td></td>'; //T SALE PRICE
	}	
	//Shipping Cost
	$shipping_cost = Shipping::getProductShippingCost( $row['id'], 0, $row['vendor_sku'] );
	echo'<td>' . number_format($shipping_cost, 2) . '</td>'; //U Shipping Cost
	// echo'<td></td>'; //V Shipping Weight ( blank on rainbows example )
	
	if( $row['is_featured'] == 'Y' )
		echo'<td>'. $row['promo_message'] .'</td>';  //W Promotional Message (this needs to be changed, only certain options )
	else
		echo'<td></td>'; //W PROMOTIONAL MESSAGE - Not sure what they want here, in example always authorized dealer or rebate available
	
	/* echo'<td></td>'; //X Format
	echo'<td></td>'; //Y Gender
	echo'<td></td>'; //Z Department
	echo'<td></td>'; //AA Size
	echo'<td>N/A</td>'; //AB Color
	echo'<td></td>'; //AC AGE RANGE
	echo'<td></td>'; //AD AGE GROUP
	echo'<td></td>'; //AE AVAILABILITY
	echo'<td></td>'; //AF STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)
	echo'<td></td>'; //AG NRF Size
	echo'<td></td>'; //AH NRF COLOR
	echo'<td></td>'; //AI DISTRIBUTOR ID
	echo'<td></td>'; //AJ INGRAM PART #
	echo'<td></td>'; //AK ASIN
	echo'<td></td>'; //AL Zip Code
	echo'<td></td>'; //AM Outlet
	*/
	echo'<td> USD </td>'; //AN Currency
	echo'<td> Ships within 2-3 days. </td>'; //AO Delivery Notes ( their example is Item usually shipped within 24 hours. ) Eli had me change it
	
	echo'<td></td>'; //AP AUTHOR
	/*echo'<td></td>'; //AQ PUBLISHER
	echo'<td></td>'; //AR MOVIE DIRECTOR
	echo'<td></td>'; //AS ACTOR
	echo'<td></td>'; //AT ARTIST
	echo'<td></td>'; //AU PICKUP
	echo'<td></td>'; //AV PRICE TYPE
	echo'<td></td>'; //AW TAX PERCENT
	echo'<td></td>'; //AX TAX REGION
	echo'<td></td>'; //AY made_in
	echo'<td></td>'; //AZ material
	echo'<td></td>'; //BA style
	echo'<td></td>'; //BB binding
	echo'<td></td>'; //BC edition
	echo'<td></td>'; //BD genre
	echo'<td></td>'; //BE pages
	echo'<td></td>'; //BF Film type
	echo'<td></td>'; //BG focus type
	echo'<td></td>'; //BH resolution
	echo'<td></td>'; //BI zoom
	echo'<td></td>'; //BJ MEGAPIXELS
	echo'<td></td>'; //BK MEMORY
	echo'<td></td>'; //BL PROCESSOR SPEED
	echo'<td></td>'; //BM wireless interface
	echo'<td></td>'; //BN battery life
	echo'<td></td>'; //BO capacity
	echo'<td></td>'; //BP operating system
	echo'<td></td>'; //BQ optical drive
	echo'<td></td>'; //BR recommended usage
	echo'<td></td>'; //BS screen size
	echo'<td></td>'; //BT aspect ratio
	echo'<td></td>'; //BU display type
	echo'<td></td>'; //BV color output
	echo'<td></td>'; //BW memory card slot
	echo'<td></td>'; //BX load type
	echo'<td></td>'; //BY feature
	echo'<td></td>'; //BZ functions
	echo'<td></td>'; //CA Tech Specification Link
	echo'<td></td>'; //CB height
	echo'<td></td>'; //CC length
	echo'<td></td>'; //CD width
	echo'<td></td>'; //CE installation
	echo'<td></td>'; //CF occasion
	echo'<td></td>'; //CG heel height
	echo'<td></td>'; //CH shoe width
	echo'<td></td>'; //CI game platform
	*/
//	echo'<td></td>'; //CJ GOOGLE SHIPPING ---- this was in the rainbow example but not the singlefeed spec
	
	//GOOGLE base fields (singlefeed files these in for you)
	echo'<td></td>';   //CJ Quantity (always 99 in the example)
	echo'<td></td>';   //CK GOOGLE EXPIRATION DATE (1 month in advance?)
	echo'<td></td>';   //CL GOOGLE EXPRIATION DATE AUTO RENEW
	
	echo'<td></td>'; 	 //CM Become category
	echo'<td></td>';	 //CN NEXTAG Category
	echo'<td></td>';	 //CO PRICEGRABBER CATEGORY
	echo'<td></td>';	 //CP PRONTO CATEGORY
	echo'<td></td>';	 //CQ SHOPPING.COM CATEGORY
	echo'<td></td>';	 //CR SHOPZILLA CATEGORY
	echo'<td></td>';	 //CS SMARTER CATEGORY
		
	echo'<td></td>'; 	 //CT Become URL REPLACEMENT
	echo'<td></td>';	 //CU GOOGLE BASE URL REPLACEMENT
	echo'<td></td>';	 //CV NEXTAG URL REPLACEMENT
	echo'<td></td>';	 //CW PRICEGRABBER URL REPLACEMENT
	//echo'<td></td>';	 //CX PRONTO URL REPLACEMENT
	/*echo'<td></td>';	 //CY SHOPPING.COM URL REPLACEMENT
	echo'<td></td>';	 //CZ SHOPZILLA URL REPLACEMENT
	echo'<td></td>';	 //DA SMARTER URL REPLACEMENT
	echo'<td></td>';	 //DB THEFIND URL REPLACEMENT
	echo'<td></td>';	 //DC YAHOO SHOPPING URL REPLACEMENT */
	echo'<td></td>';	 //DD GOOGLE BASE PRODUCT NAME REPLACEMENT
	echo'<td></td>';	 //DE GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT
	
		echo'<td>'; //DF INCLUDE YAHHOO SHOPPING
			if( $row['include_yahoo'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DG INCLUDE SHOPZILLA
			if( $row['include_shopzilla'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DH INCLUDE BECOME
			if( $row['include_become'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DI INCLUDE PRICEGRABBER
			if( $row['include_pricegrabber'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DJ INCLUDE SMARTER
			if( $row['include_smarter'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DK INCLUDE PRONTO
			if( $row['include_pronto'] == 'N' )
				echo'N';
		echo'</td>';
		echo'<td>'; //DL INCLUDE NEXTAG
			if( $row['include_nextag'] == 'N' )
				echo'N';
		echo'</td>';

		echo'<td>'; //DM INCLUDE SHOPPING.COM
			if( $row['include_shopping.com'] == 'N' )
				echo'N';
		echo'</td>';

	echo'</tr>';
	
	//Now if there are options on this product, need to list each of the options
	
	if( $options )
	foreach( $options as $option )
	{ //almost the same as above, just different skus and price
		
		/*if($show_available !== ""){ //THIS IS DONE UP TOP NOW.
			$allowOption = isOptionAvailable($row,$option);
		}
		else{
			$allowOption = true;
		}*/
		
		//if($allowOption){
			if( $numb % 2 == 0 )
				$class = 'listing';
			else 
				$class = 'listingb';
			$numb++;
			echo'<tr class="' . $class .'" >';
			echo'<td> A </td>';												 //A SF PRODUCT TYPE
			echo'<td>' . clean_value( $option['vendor_sku'] ). '</td>'; //B MPN
			echo'<td></td>';													 //C ISBN
			echo'<td>' . clean_value( $option['upc'] ) . '</td>';													 //D UPC
			echo'<td>' . clean_value( $option['vendor_sku'] ) . '</td>'; //E UNIQUE INTERNAL CODE
			$name = clean_value( $row['name'] );
			//echo'<td></td>';													 //F MUZE ID
			echo'<td>' . $name . '</td>';			 				 //G PRODUCT NAME
			
			if( $row['description'] )
				$desc = clean_value($option['description']);
			else 
				$desc = clean_value($option['name']);
			echo'<td>' . $desc . '</td>';	
			
			if( $row['is_featured'] == 'Y' )
			{
				$orig_price = $option['additional_price'];
				$option['additional_price'] = $option['retail_price'];
				echo'<td>' . number_format( (Products::getProductMarkup($option) ), 2 ) . '</td>';
				$option['additional_price'] = $orig_price;
	//		echo'<td>' . number_format( ($option['retail_price'] * ($markup/100 + 1) ), 2 ) . '</td>';
			}
			else 
			{
				$orig_price = $option['additional_price'];
				$option['additional_price'] = $option['additional_price'];
				echo'<td>' . number_format( (Products::getProductMarkup($option) ), 2 ) . '</td>';
				$option['additional_price'] = $orig_price;
				//echo'<td>' . number_format( ($option['additional_price'] * ($markup/100 + 1) ), 2 ) . '</td>';
			}
			
			$product_url = Catalog::makeProductLink_( $option, true, false, $option['value'] );
			echo'<td>' . $product_url . '</td>'; //Product URL
			$image_url = $CFG->baseurl . 'optionpics/' . $option['id'] . $CFG->xlarge_suffix;
			echo'<td>' . $image_url . '</td>'; //Product Image URL
			
			echo'<td>' . $main_cat['name'] . '</td>'; //L Product Type, lowest lvl category
			echo'<td>' . $cat_string . '</td>'; //M Category Tree
			
			$brand = Brands::get1( $row['brand_id'] );
			echo'<td>' . $brand['name'] . '</td>'; //N Brand
			echo'<td> Y </td>'; //O Stock Status
			echo'<td> NEW </td>'; //P Condition
			echo'<td> Visa, Mastercard, PayPal, Check, Money Order, American Express, Discover </td>'; //Q Payment Type
			echo'<td>' . $row['keywords'] . '</td>'; //R Keywords
			
			echo'<td></td>'; //S MSRP
			
			if( $row['is_featured'] == 'Y' )
				echo'<td>'. number_format( (Products::getProductMarkup($row) ), 2 ) . '</td>'; //Sale Price
			else
				echo'<td></td>';
				
			echo'<td>' . number_format($shipping_cost, 2) . '</td>'; //U Shipping Cost
		//echo'<td></td>'; //V Shipping Weight ( blank on rainbows example )
		
		if( $row['is_featured'] == 'Y' )
			echo'<td>'. $row['promo_message'] .'</td>';  //W Promotional Message (this needs to be changed, only certain options )
		else
			echo'<td></td>'; //W PROMOTIONAL MESSAGE - Not sure what they want here, in example always authorized dealer or rebate available
		
		/*echo'<td></td>'; //X Format
		echo'<td></td>'; //Y Gender
		echo'<td></td>'; //Z Department
		echo'<td></td>'; //AA Size
		echo'<td>' . clean_value( $option['value'] ) . '</td>'; //AB Color
		echo'<td></td>'; //AC AGE RANGE
		echo'<td></td>'; //AD AVAILABILITY
		echo'<td></td>'; //AE AGE GROUP
		echo'<td></td>'; //AF STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)
		echo'<td></td>'; //AG NRF Size
		echo'<td></td>'; //AH NRF COLOR
		echo'<td></td>'; //AI DISTRIBUTOR ID
		echo'<td></td>'; //AJ INGRAM PART #
		echo'<td></td>'; //AK ASIN
		echo'<td></td>'; //AL Zip Code
		echo'<td></td>'; //AM Outlet
		*/
		echo'<td> USD </td>'; //AN Currency
		echo'<td> Ships within 2-3 days. </td>'; //AO Delivery Notes ( their example is Item usually shipped within 24 hours. ) Eli had me change it
		
		echo'<td></td>'; //AP AUTHOR
		/*echo'<td></td>'; //AQ PUBLISHER
		echo'<td></td>'; //AR MOVIE DIRECTOR
		echo'<td></td>'; //AS ACTOR
		echo'<td></td>'; //AT ARTIST
		echo'<td></td>'; //AU PICKUP
		echo'<td></td>'; //AV PRICE TYPE
		echo'<td></td>'; //AW TAX PERCENT
		echo'<td></td>'; //AX TAX REGION
		echo'<td></td>'; //AY made_in
		echo'<td></td>'; //AZ material
		echo'<td></td>'; //BA style
		echo'<td></td>'; //BB binding
		echo'<td></td>'; //BC edition
		echo'<td></td>'; //BD genre
		echo'<td></td>'; //BE pages
		echo'<td></td>'; //BF Film type
		echo'<td></td>'; //BG focus type
		echo'<td></td>'; //BH resolution
		echo'<td></td>'; //BI zoom
		echo'<td></td>'; //BJ MEGAPIXELS
		echo'<td></td>'; //BK MEMORY
		echo'<td></td>'; //BL PROCESSOR SPEED
		echo'<td></td>'; //BM wireless interface
		echo'<td></td>'; //BN battery life
		echo'<td></td>'; //BO capacity
		echo'<td></td>'; //BP operating system
		echo'<td></td>'; //BQ optical drive
		echo'<td></td>'; //BR recommended usage
		echo'<td></td>'; //BS screen size
		echo'<td></td>'; //BT aspect ratio
		echo'<td></td>'; //BU display type
		echo'<td></td>'; //BV color output
		echo'<td></td>'; //BW memory card slot
		echo'<td></td>'; //BX load type
		echo'<td></td>'; //BY feature
		echo'<td></td>'; //BZ functions
		echo'<td></td>'; //CA Tech Specification Link
		echo'<td></td>'; //CB height
		echo'<td></td>'; //CC length
		echo'<td></td>'; //CD width
		echo'<td></td>'; //CE installation
		echo'<td></td>'; //CF occasion
		echo'<td></td>'; //CG heel height
		echo'<td></td>'; //CH shoe width
		echo'<td></td>'; //CI game platform
		*/
	//	echo'<td></td>'; //CJ GOOGLE SHIPPING ---- this was in the rainbow example but not the singlefeed spec
		
		//GOOGLE base fields (singlefeed files these in for you)
		echo'<td></td>';   //CJ Quantity (always 99 in the example)
		echo'<td></td>';   //CK GOOGLE EXPIRATION DATE (1 month in advance?)
		echo'<td></td>';   //CL GOOGLE EXPRIATION DATE AUTO RENEW
		
		echo'<td></td>'; 	 //CM Become category
		echo'<td></td>';	 //CN NEXTAG Category
		echo'<td></td>';	 //CO PRICEGRABBER CATEGORY
		echo'<td></td>';	 //CP PRONTO CATEGORY
		echo'<td></td>';	 //CQ SHOPPING.COM CATEGORY
		echo'<td></td>';	 //CR SHOPZILLA CATEGORY
		echo'<td></td>';	 //CS SMARTER CATEGORY
			
		echo'<td></td>'; 	 //CT Become URL REPLACEMENT
		echo'<td></td>';	 //CU GOOGLE BASE URL REPLACEMENT
		echo'<td></td>';	 //CV NEXTAG URL REPLACEMENT
		echo'<td></td>';	 //CW PRICEGRABBER URL REPLACEMENT
		/*echo'<td></td>';	 //CX PRONTO URL REPLACEMENT
		echo'<td></td>';	 //CY SHOPPING.COM URL REPLACEMENT
		echo'<td></td>';	 //CZ SHOPZILLA URL REPLACEMENT
		echo'<td></td>';	 //DA SMARTER URL REPLACEMENT
		echo'<td></td>';	 //DB THEFIND URL REPLACEMENT
		echo'<td></td>';	 //DC YAHOO SHOPPING URL REPLACEMENT */
		echo'<td></td>';	 //DD GOOGLE BASE PRODUCT NAME REPLACEMENT
		echo'<td></td>';	 //DE GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT
		
			echo'<td>'; //DF INCLUDE YAHHOO SHOPPING
				if( $row['include_yahoo'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DG INCLUDE SHOPZILLA
				if( $row['include_shopzilla'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DH INCLUDE BECOME
				if( $row['include_become'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DI INCLUDE PRICEGRABBER
				if( $row['include_pricegrabber'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DJ INCLUDE SMARTER
				if( $row['include_smarter'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DK INCLUDE PRONTO
				if( $row['include_pronto'] == 'N' )
					echo'N';
			echo'</td>';
			echo'<td>'; //DL INCLUDE NEXTAG
				if( $row['include_nextag'] == 'N' )
					echo'N';
			echo'</td>';
	
			echo'<td>'; //DM INCLUDE SHOPPING.COM
				if( $row['include_shopping.com'] == 'N' )
					echo'N';
			echo'</td>';
	
		echo'</tr>';
		//}
	}
	return $numb;
}

function tabSeperateGoogle( $row, $is_available='' )
{
    global $CFG;

	$show_row = true; //if the row is bad, this row won't show.

	if($is_available !== ""){
		if($is_available != $row['is_available']){
			return false;
		}
	}

	$options = Products::getOptionsForProduct( $row['id'] );
	//Header Row
	//$output_row = "link\ttitle\tdescription\timage_link\tprice\tid\tcondition\tbrand\tpayment_notes\tproduct_type\tweight\tshipping\theight\twidth\tcapacity\ttax";
	
	$output_row = "";

	//Link
	$output_row .= Catalog::makeProductLink_( $row );
	$output_row .=  "\t";

	//Title
	$output_row .= clean_value($row['name']);
	$output_row .= "\t";
	
	//Description
	$output_row .= clean_value($row['description']);
	$output_row .= "\t";

	//Image Link
	$output_row .= Catalog::makeProductImageLink( $row['id'] );
	$output_row .= "\t";

	//Price
	if( $row['per_price'] > 0.00 )
	    $price = $row['per_price'];
	else
	    $price = $row['price'];

	$output_row .= clean_value($price);
	$output_row .= "\t";

	//ID - Vendor Sku
	$output_row .= clean_value($row['vendor_sku']);
	$output_row .= "\t";

	//Condition
	$output_row .= "new";
	$output_row .= "\t";

	//Brand
	$output_row .= clean_value($row['brand_name']);
	$output_row .= "\t";

	//Payment Notes
	$output_row .= "google checkout accepted";
	$output_row .= "\t";

	//Product Type

	$cats = Cats::getCatsForProduct( $row['id'] );

	$cat_tree = Cats::getCatsTree($cats[count($cats)-1]['id']);

	$bar = "";
	foreach ($cat_tree as $cat_info)
	{
	    $bar .= $cat_info[name] . " > ";
	}

	$output_row .= clean_value($bar);
	$output_row .= "\t";

	//weight
	$output_row .= clean_value($row['weight']) . ' pounds';
	$output_row .= "\t";

	//Shipping
	if( $price < $CFG->free_shipping_limit )
	    $shipping = ":::" . $CFG->flat_shipping_rate . "";
	else
	    $shipping = ":::0";
	$output_row .= $shipping;
	$output_row .= "\t";

	//height
	$output_row .= clean_value($row['height']);
	$output_row .= "\t";

	//width
	$output_row .= clean_value($row['width']);
	$output_row .= "\t";

	//capacity??
	$output_row .= "";
	

	//tax
	//$output_row .= "";


	trim( $output_row );
	$output_row .= "\r\n";

	echo $output_row;
}

function tabSeperate( $row, $is_available="" )
{
	global $CFG;
	
	$show_row = true; //if the row is bad, this row won't show.
	
	if($is_available !== ""){
		if($is_available != $row['is_available']){
			return false;
		}
	}
	
	$options = Products::getOptionsForProduct( $row['id'] );
	$output_row = "";
	$output_row .= "A\t";											 //A SF PRODUCT TYPE
	$output_row .= clean_value( $row['vendor_sku'] ). "\t";  //B MPN
	$output_row .= "\t";											 //C ISBN
	$output_row .= clean_value( $row['upc'] ). "\t";											 //D UPC
	$output_row .= clean_value( $row['vendor_sku'] ). "\t"; //E UNIQUE INTERNAL CODE
	$name = clean_value( $row['name'] );
	//$output_row .= "\t";													 //F MUZE ID
	$output_row .= $name . "\t";			 				 //G PRODUCT NAME
	
	if( $row['description'] )
		$desc = clean_value($row['description']);
	else 
		$desc = clean_value($row['name']);
	$output_row .= $desc . "\t";							 //H PRODUCT DESCRIPTION
	
	//Markup was changed, need to modify this to get the right price for the different prices (sale/normal)
	//$markup = Products::getProductMarkup( $row );
	//This is a parent product so I need to either list the lowest price of the options, or if no options just the price given
	if( $options )
	{
		//The options are being order by price ASC by default, so grab the first one and output that price here
		$price = 0.00;
		$correct_k = -1;
		foreach ($options as $k=>$option){
			if(Products::getProductMarkup($option) > 10){
				if($price == 0.00){ //no options have been chosen as "lowest"
					$price = number_format($option['additional_price'], 2);
					$correct_k = $k;
				}else{
					if(number_format($option['additional_price'],2) < $price){ //if you have a low price option, check if this is lower.
						$price = number_format($option['additional_price'], 2);
						$correct_k = $k;
					}
				}
			}
		}

//		if($correct_k > 0 && $price > 10){
//			$orig_price = $options[$k]['additional_price'];
//			$row['additional_price'] = $options[$k]['retail_price'];
//			$output_row .= $price . "\t";  //I PRODUCT PRICE
//			$row['additional_price'] = $orig_price;
//		}else{
//			$show_row = false;
//		}
	}
	else
	    $price = $row['price'];
//	else
//	{
//		if((Products::getProductMarkup($row) ) > 10){
//			if( $row['is_featured'] == 'Y' )
//			{
//				$orig_price = $row['price'];
//				$row['price'] = $row['retail_price'];
//				$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t";  //I PRODUCT PRICE
//				$row['price'] = $orig_price;
//			}
//			else
//				$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t";  //I PRODUCT PRICE
//				//echo'<td>' . number_format( ($row['price'] * ($markup/100 + 1) ), 2 ) . '</td>';
//		}else{
//			$show_row = false;
//		}
//	}

	if(Products::getProductMarkup($row) < 10){
		$show_row = false;
	}

	$output_row .= number_format( ( $price ), 2 ) . "\t";  //I PRODUCT PRICE

	$product_url = Catalog::makeProductLink_( $row, true );
	$image_url = Catalog::makeProductImageLink( $row['id'], false );
	$output_row .= $product_url . "\t"; //Product URL   	   //J PRODUCT URL
	$output_row .= $image_url . "\t"; //Product Image URL   //K IMAGE URL
	
	$cat_info = Products::getProductCatTree( $row['id'] );
	$main = count( $cat_info );
	$main_cat = Cats::get1($cat_info[$main-1]['value']);
	if( !$main_cat['name'] )
		$main_cat['name'] = 'Home';
	$cat_string = 'Home ';
	foreach( $cat_info as $cat )
	{
		$c = Cats::get1( $cat['value'] );
		$cat_string .= '> ' . $c['name'] . ' ';
	}
	//$cat_structure = Cats::cats2Structure( $cats );

	$output_row .= $main_cat['name'] . "\t"; //L Product Type, lowest lvl category
	$output_row .= $cat_string . "\t"; //M Category Tree
	
	$brand = Brands::get1( $row['brand_id'] );
	$output_row .= $brand['name'] . "\t"; //N Brand
	$output_row .= "Y\t"; //O Stock Status
	$output_row .= "NEW\t"; //P Condition
	$output_row .= "Visa, Mastercard, PayPal, Check, Money Order, American Express, Discover \t"; //Q Payment Type
	$output_row .= $row['keywords'] . "\t"; //R Keywords
	
	$output_row .=  number_format( $row['retail_price'], 2 ) . "\t"; //S MSRP

//Sale Price
//	if( $options )
//	{
//		//The options are being order by price ASC by default, so grab the first one and output that price here
//		if( $row['is_featured'] == 'Y' )
//		{
//			$orig_price = $options[$k]['additional_price'];
//			$row['additional_price'] = $options[$k]['additional_price'];
//			$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t"; //T SALE PRICE
//			$row['additional_price'] = $orig_price;
//		}
//		else
//			$output_row .= "\t"; //T SALE PRICE
//	}
//	else
//	{
		if( $row['is_featured'] == 'Y' )
		{
			$output_row .= number_format( (Products::getProductMarkup($row) ), 2 ) . "\t"; //T SALE PRICE
		}
		else 
			$output_row .= "\t"; //T SALE PRICE
//	}
	//Shipping Cost
	$shipping_cost = Shipping::getProductShippingCost( $row['id'], 0, $row['vendor_sku'] );
	$output_row .= number_format($shipping_cost, 2) . "\t"; //U Shipping Cost
///	$output_row .= "\t"; //V Shipping Weight ( blank on rainbows example )
	
	if( $row['is_featured'] == 'Y' )
		$output_row .= $row['promo_message'] ."\t";  //W Promotional Message (this needs to be changed, only certain options )
	else
		$output_row .= "\t"; //W PROMOTIONAL MESSAGE - Not sure what they want here, in example always authorized dealer or rebate available
	
///	$output_row .= "\t"; //X Format
///	$output_row .= "\t"; //Y Gender
///	$output_row .= "\t"; //Z Department
///	$output_row .= "\t"; //AA Size
///	$output_row .= "N/A\t"; //AB Color
///	$output_row .= "\t"; //AC AGE RANGE
///	$output_row .= "\t"; //AD AGE GROUP
///	$output_row .= "\t"; //AE AVAILABILITY
///	$output_row .= "\t"; //AF STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)
///	$output_row .= "\t"; //AG NRF Size
///	$output_row .= "\t"; //AH NRF COLOR
///	$output_row .= "\t"; //AI DISTRIBUTOR ID
///	$output_row .= "\t"; //AJ INGRAM PART #
///	$output_row .= "\t"; //AK ASIN
///	$output_row .= "\t"; //AL Zip Code
///	$output_row .= "\t"; //AM Outlet
	
	$output_row .= "USD\t"; //AN Currency
	$output_row .= "Ships within 2-3 days.\t"; //AO Delivery Notes ( their example is Item usually shipped within 24 hours. ) Eli had me change it
	
	$output_row .= "\t"; //AP AUTHOR
///	$output_row .= "\t"; //AQ PUBLISHER
///	$output_row .= "\t";//AR MOVIE DIRECTOR
///	$output_row .= "\t"; //AS ACTOR
///	$output_row .= "\t"; //AT ARTIST
///	$output_row .= "\t"; //AU PICKUP
///	$output_row .= "\t"; //AV PRICE TYPE
///	$output_row .= "\t";//AW TAX PERCENT
///	$output_row .= "\t"; //AX TAX REGION
///	$output_row .= "\t"; //AY made_in
///	$output_row .= "\t"; //AZ material
///	$output_row .= "\t"; //BA style
///	$output_row .= "\t"; //BB binding
///	$output_row .= "\t"; //BC edition
///	$output_row .= "\t"; //BD genre
///	$output_row .= "\t"; //BE pages
///	$output_row .= "\t";//BF Film type
///	$output_row .= "\t"; //BG focus type
///	$output_row .= "\t"; //BH resolution
///	$output_row .= "\t"; //BI zoom
///	$output_row .= "\t"; //BJ MEGAPIXELS
///	$output_row .= "\t"; //BK MEMORY
///	$output_row .= "\t"; //BL PROCESSOR SPEED
///	$output_row .= "\t"; //BM wireless interface
///	$output_row .= "\t"; //BN battery life
///	$output_row .= "\t";//BO capacity
///	$output_row .= "\t"; //BP operating system
///	$output_row .= "\t"; //BQ optical drive
///	$output_row .= "\t";//BR recommended usage
///	$output_row .= "\t"; //BS screen size
///	$output_row .= "\t"; //BT aspect ratio
///	$output_row .= "\t"; //BU display type
///	$output_row .= "\t"; //BV color output
///	$output_row .= "\t"; //BW memory card slot
///	$output_row .= "\t"; //BX load type
///	$output_row .= "\t"; //BY feature
///	$output_row .= "\t"; //BZ functions
///	$output_row .= "\t"; //CA Tech Specification Link
///	$output_row .= "\t"; //CB height
///	$output_row .= "\t"; //CC length
///	$output_row .= "\t"; //CD width
///	$output_row .= "\t"; //CE installation
///	$output_row .= "\t"; //CF occasion
///	$output_row .= "\t"; //CG heel height
///	$output_row .= "\t"; //CH shoe width
///	$output_row .= "\t"; //CI game platform
//	echo'<td></td>'; //CJ GOOGLE SHIPPING ---- this was in the rainbow example but not the singlefeed spec
	
	//GOOGLE base fields (singlefeed files these in for you)
	$output_row .= "\t";   //CJ Quantity (always 99 in the example)
	$output_row .= "\t";   //CK GOOGLE EXPIRATION DATE (1 month in advance?)
	$output_row .= "\t";   //CL GOOGLE EXPRIATION DATE AUTO RENEW
	
	$output_row .= "\t"; 	 //CM Become category
	$output_row .= "\t";	 //CN NEXTAG Category
	$output_row .= "\t";	 //CO PRICEGRABBER CATEGORY
	$output_row .= "\t";	 //CP PRONTO CATEGORY
	$output_row .= "\t";	 //CQ SHOPPING.COM CATEGORY
	$output_row .= "\t";	 //CR SHOPZILLA CATEGORY
	$output_row .= "\t";	 //CS SMARTER CATEGORY
		
	$output_row .= "\t"; 	 //CT Become URL REPLACEMENT
	$output_row .= "\t";	 //CU GOOGLE BASE URL REPLACEMENT
	$output_row .= "\t";	 //CV NEXTAG URL REPLACEMENT
	$output_row .= "\t";	 //CW PRICEGRABBER URL REPLACEMENT
///	$output_row .= "\t";	 //CX PRONTO URL REPLACEMENT
///	$output_row .= "\t";	 //CY SHOPPING.COM URL REPLACEMENT
///	$output_row .= "\t";	 //CZ SHOPZILLA URL REPLACEMENT
///	$output_row .= "\t";	 //DA SMARTER URL REPLACEMENT
///	$output_row .= "\t";	 //DB THEFIND URL REPLACEMENT
///	$output_row .= "\t";	 //DC YAHOO SHOPPING URL REPLACEMENT
///	$output_row .= "\t";	 //DD GOOGLE BASE PRODUCT NAME REPLACEMENT
///	$output_row .= "\t";	 //DE GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT
	
		 //DF INCLUDE YAHHOO SHOPPING
			if( $row['include_yahoo'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DG INCLUDE SHOPZILLA
			if( $row['include_shopzilla'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DH INCLUDE BECOME
			if( $row['include_become'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DI INCLUDE PRICEGRABBER
			if( $row['include_pricegrabber'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DJ INCLUDE SMARTER
			if( $row['include_smarter'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DK INCLUDE PRONTO
			if( $row['include_pronto'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DL INCLUDE NEXTAG
			if( $row['include_nextag'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DM INCLUDE SHOPPING.COM
			if( $row['include_shopping.com'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";

		$output_row .= "\n";
	
	//if($show_row){
		echo $output_row;
	//}
	
	
	//Now if there are options on this product, need to list each of the options
	if( $options ) 
	foreach( $options as $option )
	{ //almost the same as above, just different skus and price
	$output_row = "";
	$show_row = true;
	
		/*if($show_available !== ""){ //THIS IS NOW DONE UP TOP, NO MORE AVAILABLE PARENTS AND NOT AVAILABLE CHILDREN
			$allowOption = isOptionAvailable($row,$option);
		}
		else{
			$allowOption = true;
		}*/
		
		//if($allowOption){
	
		$output_row .="A\t";												 //A SF PRODUCT TYPE
		$output_row .= clean_value( $option['vendor_sku'] ) . "\t"; //B MPN
		$output_row .= "\t";												 //C ISBN
		$output_row .= clean_value( $row['upc'] ). "\t";													 //D UPC
		$output_row .= clean_value( $option['vendor_sku'] ) . "\t"; //E UNIQUE INTERNAL CODE
		$name = clean_value( $row['name'] );
	///	$output_row .= "\t";													 //F MUZE ID
		$output_row .= $name . "\t";			 				 //G PRODUCT NAME
		
		if( $row['description'] )
			$desc = clean_value($row['description']);
		else 
			$desc = clean_value($row['name']);
		$output_row .= $desc . "\t";	
		
		$orig_price = $option['additional_price'];
		if(Products::getProductMarkup($option) > 10){
			$output_row .= number_format( (Products::getProductMarkup($option) ), 2 ) . "\t";
		}else{
			$show_row = false;
		}
		
		$product_url = Catalog::makeProductLink_( $option, true, false, $option['value'] );
		$output_row .= $product_url . "\t"; //Product URL
		$image_url = $CFG->baseurl . 'optionpics/' . $option['id'] . $CFG->xlarge_suffix;
		$output_row .= $image_url . "\t"; //Product Image URL
		
		$output_row .= $main_cat['name'] . "\t"; //L Product Type, lowest lvl category
		$output_row .= $cat_string . "\t"; //M Category Tree
		
		$brand = Brands::get1( $row['brand_id'] );
		$output_row .= $brand['name'] . "\t"; //N Brand
		$output_row .= "Y\t"; //O Stock Status
		$output_row .= "NEW\t"; //P Condition
		$output_row .= "Visa, Mastercard, PayPal, Check, Money Order, American Express, Discover \t"; //Q Payment Type
		$output_row .= $row['keywords'] . "\t"; //R Keywords
		
		$output_row .= "\t"; //S MSRP
		
		if( $row['is_featured'] == 'Y' )
			$output_row .= number_format( (Products::getProductMarkup($option) ), 2 ) . "\t"; //Sale Price
		else
			$output_row .= "\t";
			
		$output_row .= number_format($shipping_cost, 2) . "\t"; //U Shipping Cost
	//$output_row .= "\t"; //V Shipping Weight ( blank on rainbows example )
	
	if( $row['is_featured'] == 'Y' )
		$output_row .= $row['promo_message'] ."\t";  //W Promotional Message (this needs to be changed, only certain options )
	else
		$output_row .= "\t"; //W PROMOTIONAL MESSAGE - Not sure what they want here, in example always authorized dealer or rebate available
	
///	$output_row .= "\t"; //X Format
///	$output_row .= "\t"; //Y Gender
///	$output_row .= "\t"; //Z Department
///	$output_row .= "\t"; //AA Size
///	$output_row .= $option['value'] . "\t"; //AB Color
///	$output_row .= "\t"; //AC AGE RANGE
///	$output_row .= "\t"; //AD AGE GROUP
///	$output_row .= "\t"; //AE AVAILABILITY
///	$output_row .= "\t"; //AF STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)
///	$output_row .= "\t"; //AG NRF Size
///	$output_row .= "\t"; //AH NRF COLOR
///	$output_row .= "\t"; //AI DISTRIBUTOR ID
///	$output_row .= "\t"; //AJ INGRAM PART #
///	$output_row .= "\t"; //AK ASIN
///	$output_row .= "\t"; //AL Zip Code
///	$output_row .= "\t"; //AM Outlet
	
	$output_row .= "USD\t"; //AN Currency
	$output_row .= "Ships within 2-3 days.\t"; //AO Delivery Notes ( their example is Item usually shipped within 24 hours. ) Eli had me change it
	
	$output_row .= "\t"; //AP AUTHOR
///	$output_row .= "\t"; //AQ PUBLISHER
///	$output_row .= "\t";//AR MOVIE DIRECTOR
///	$output_row .= "\t"; //AS ACTOR
///	$output_row .= "\t"; //AT ARTIST
///	$output_row .= "\t"; //AU PICKUP
///	$output_row .= "\t"; //AV PRICE TYPE
///	$output_row .= "\t";//AW TAX PERCENT
///	$output_row .= "\t"; //AX TAX REGION
///	$output_row .= "\t"; //AY made_in
///	$output_row .= "\t"; //AZ material
///	$output_row .= "\t"; //BA style
///	$output_row .= "\t"; //BB binding
///	$output_row .= "\t"; //BC edition
///	$output_row .= "\t"; //BD genre
///	$output_row .= "\t"; //BE pages
///	$output_row .= "\t";//BF Film type
///	$output_row .= "\t"; //BG focus type
///	$output_row .= "\t"; //BH resolution
///	$output_row .= "\t"; //BI zoom
///	$output_row .= "\t"; //BJ MEGAPIXELS
///	$output_row .= "\t"; //BK MEMORY
///	$output_row .= "\t"; //BL PROCESSOR SPEED
///	$output_row .= "\t"; //BM wireless interface
///	$output_row .= "\t"; //BN battery life
///	$output_row .= "\t";//BO capacity
///	$output_row .= "\t"; //BP operating system
///	$output_row .= "\t"; //BQ optical drive
///	$output_row .= "\t";//BR recommended usage
///	$output_row .= "\t"; //BS screen size
///	$output_row .= "\t"; //BT aspect ratio
///	$output_row .= "\t"; //BU display type
///	$output_row .= "\t"; //BV color output
///	$output_row .= "\t"; //BW memory card slot
///	$output_row .= "\t"; //BX load type
///	$output_row .= "\t"; //BY feature
///	$output_row .= "\t"; //BZ functions
///	$output_row .= "\t"; //CA Tech Specification Link
///	$output_row .= "\t"; //CB height
///	$output_row .= "\t"; //CC length
///	$output_row .= "\t"; //CD width
///	$output_row .= "\t"; //CE installation
///	$output_row .= "\t"; //CF occasion
///	$output_row .= "\t"; //CG heel height
///	$output_row .= "\t"; //CH shoe width
///	$output_row .= "\t"; //CI game platform
//	echo'<td></td>'; //CJ GOOGLE SHIPPING ---- this was in the rainbow example but not the singlefeed spec
	
	//GOOGLE base fields (singlefeed files these in for you)
	$output_row .= "\t";   //CJ Quantity (always 99 in the example)
	$output_row .= "\t";   //CK GOOGLE EXPIRATION DATE (1 month in advance?)
	$output_row .= "\t";   //CL GOOGLE EXPRIATION DATE AUTO RENEW
	
	$output_row .= "\t"; 	 //CM Become category
	$output_row .= "\t";	 //CN NEXTAG Category
	$output_row .= "\t";	 //CO PRICEGRABBER CATEGORY
	$output_row .= "\t";	 //CP PRONTO CATEGORY
	$output_row .= "\t";	 //CQ SHOPPING.COM CATEGORY
	$output_row .= "\t";	 //CR SHOPZILLA CATEGORY
	$output_row .= "\t";	 //CS SMARTER CATEGORY
		
	$output_row .= "\t"; 	 //CT Become URL REPLACEMENT
	$output_row .= "\t";	 //CU GOOGLE BASE URL REPLACEMENT
	$output_row .= "\t";	 //CV NEXTAG URL REPLACEMENT
	$output_row .= "\t";	 //CW PRICEGRABBER URL REPLACEMENT
///	$output_row .= "\t";	 //CX PRONTO URL REPLACEMENT
///	$output_row .= "\t";	 //CY SHOPPING.COM URL REPLACEMENT
///	$output_row .= "\t";	 //CZ SHOPZILLA URL REPLACEMENT
///	$output_row .= "\t";	 //DA SMARTER URL REPLACEMENT
///	$output_row .= "\t";	 //DB THEFIND URL REPLACEMENT
///	$output_row .= "\t";	 //DC YAHOO SHOPPING URL REPLACEMENT
///	$output_row .= "\t";	 //DD GOOGLE BASE PRODUCT NAME REPLACEMENT
///	$output_row .= "\t";	 //DE GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT
	
		 //DF INCLUDE YAHHOO SHOPPING
		if( $row['include_yahoo'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DG INCLUDE SHOPZILLA
			if( $row['include_shopzilla'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DH INCLUDE BECOME
			if( $row['include_become'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DI INCLUDE PRICEGRABBER
			if( $row['include_pricegrabber'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DJ INCLUDE SMARTER
			if( $row['include_smarter'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DK INCLUDE PRONTO
			if( $row['include_pronto'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DL INCLUDE NEXTAG
			if( $row['include_nextag'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";
		 //DM INCLUDE SHOPPING.COM
			if( $row['include_shopping.com'] == 'N' )
				$output_row .= "N";
		$output_row .= "\t";

		$output_row .= "\n";
		//if($show_row){
			echo $output_row;
		//}
		//}
	}
}

function clean_value( $value )
{

	$value = trim( $value );
	$value = strip_tags( $value );
	
	$value = str_replace( "\n", " ", $value );
	
	$value_array = explode( " ", $value);
	foreach( $value_array as $key => $v )
	{
		$value_array[$key] = trim( $v );
	}
	$value = implode( " ", $value_array );
	
	return $value;
}

function isOptionAvailable($row,$option){
	$opt_available = true;
	if( $row['additional_price'] > 0 )
			$price = $row['additional_price'];
	else
		$price = $row['price'];
		
	if($price == 0.00 && $row['final_price'] == 0.00){
		$opt_available = false;
		if((int) $row['id']){
			if($option['additional_price'] > 0 || $option['retail_price'] > 0 || $option['final_price'] > 0){
				$opt_available = true; //if the option is active and costs $$$, this is not a 0.00 product!
			}
		}
	}
	
	if($show_available == "Y"){
		if($opt_available){
			$allowOption = true;
		}
		else{
			$allowOption = false;
		}
	}
	else if($show_available == "N"){
		if($opt_available){
			$allowOption = false;
		}
		else{
			$allowOption = true;
		}
	}
	
	return $allowOption;
}

function unavailableProducts(){
	global $CFG;
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
	header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
	header ("Pragma: no-cache");
	header("Expires: 0");
	header('Content-Transfer-Encoding: none');
	header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
	header("Content-type: application/x-msexcel");                    // This should work for the rest
	header('Content-Disposition: attachment; filename="unavailable_products.xls"'); 
	header("Content-Type: application/force-download");
	header("Content-Transfer-Encoding: binary\n");
	
	echo "ID\tProduct Name\tVendor SKU\tURL\tIs Active\tIs Available\tIs Featured\r\n";
	
	$limit = 500;
	$start = 0;
	do{
		$prods = Products::get('','','','','','','','','','','','','','','','','','','','','','','','','','',$limit,$start,'','N','N');
		if($prods){
			foreach($prods as $prod){
				echo "$prod[id]\t$prod[name]\t$prod[vendor_sku]\t".$CFG->baseurl."$prod[vendor_sku].html\t"."$prod[is_active]\t$prod[is_available]\t$prod[is_featured]\r\n";
			}
		}
		$start += $limit;
	}while($prods);
}
?>