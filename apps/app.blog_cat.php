<?

class BlogCat{

	static function insert($info){
		return db_insert("blog_cats",$info);
	}

	static function update($id,$info){
		return db_update("blog_cats",$id,$info);
	}

	static function delete($id){
		return db_delete("blog_cats",$id);
	}

	static function getByO($o=""){
		global $CFG;

		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT blog_cats.*, bc.name AS parent_category ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(blog_cats.id)) as total ";
		}

		$from_ = " FROM blog_cats ";

		$join_ = " LEFT JOIN blog_cats bc ON bc.id = blog_cats.parent_id ";

		$where_ = " WHERE 1 ";

		if($o->id > 0){
			$where_ .= " AND blog_cats.id = [id] ";
		}
		if(isset($o->parent_id)){
			$where_ .= " AND blog_cats.parent_id = [parent_id] ";
		}
		if(isset($o->blog_id)){
			$join_ .= " LEFT JOIN blog_cat_mm bcm ON bcm.cat_id = blog_cats.id ";
			$where_ .= " AND bcm.blog_id = [blog_id] ";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY blog_cats.parent_id ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		$ret = $result;
		if (!isset($o->exclude_content) && $ret){
	        $program_sections = Templates::getAssignedSections($CFG->blog_cat_template_id);
	        if ($program_sections){
	        	foreach ($ret as $key=>$val){
	        		$program_content_info = YContent::get1PageContent($val['id'],'','blog_cat');
					foreach ($program_sections as $section) {

						foreach ($CFG->languages as $language){

							$ret[$key][$section['section_name']] = $program_content_info[$language][$section['section_name']];
							if ($ret[$key][$section['section_name']] == '')
								$ret[$key][$section['section_name']] = $ret[$key][$section['section_name']]['eng'];
						}
					}
				}
	        }
		}

		return $ret;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	static function getCatCheckboxes($blog_id,$field_name="cats[]"){
		$blog_id = (int)$blog_id;

		$o->blog_id = $blog_id;
		$sel_cats2 = self::getByO($o);
		$sel_cats = array();
		if($sel_cats2){
			foreach($sel_cats2 as $scat){
				$sel_cats[] = $scat['id'];
			}
		}

		$parent_cats = self::getCatStructure($sel_cats);

		$html = "";
		if($parent_cats)foreach($parent_cats as $cat_id => $cat){
			$s1 = ($cat['selected'] ? 'checked="checked"' : '');
			$html .= "<label class='maincat'><input type='checkbox' name='$field_name' value='$cat[id]' $s1/> $cat[name]</label><br />";
			if(is_array($cat['children'])){

				foreach($cat['children'] as $sub_cat){
					$s2 = ($sub_cat['selected'] ? 'checked="checked"' : '');
					$html .= "
						<label class='subcat'>
							<input type='checkbox' name='$field_name' value='$sub_cat[id]' $s2/> $sub_cat[name]
						</label><br />";
				}
			}
		}

		return $html;

	}

	static function getCatStructure($sel_cats=''){

		if(!$sel_cats) $sel_cats = array();

		$all_cats = self::getByO();
		if($all_cats)foreach($all_cats as $cat){

			if(in_array($cat['id'],$sel_cats)){
				$cat['selected'] = true;
			} else {
				$cat['selected'] = false;
			}

			if($cat['parent_id'] == 0){
				$parent_cats[$cat['id']] = $cat;
			} else {
				$parent_cats[$cat['parent_id']]['children'][] = $cat;
			}
		}

		return $parent_cats;
	}

}