<?php

class Tracking {
	
	static function trackInsert( $type, $type_id, $action='insert', $before='', $after='' )
	{
		global $CFG;
		
		$rec = Tracking::getRecord( $type, $type_id );
		
		if( $action == 'insert' )
			$action = 'insert ' . $type;

		if( $after == '' )
		{
			$after = serialize( $rec );
		}
		
		$info['type'] = $type;
		$info['type_id'] = $type_id;
		$info['user_id'] = $_SESSION['id'];
		$info['action'] = $action;
		$info['before'] = $before;
		$info['after'] = $after;
		
		return Tracking::insert( $info );
	}
	
	static function insert( $info )
	{
		return db_insert( 'tracking', $info, 'date' );
	}
	
	static function update( $id, $info )
	{
		return db_update('tracking', $id, $info, 'id', 'date' );
	}
	
	static function trackUpdate( $id=0, $type='', $type_id=0, $action='update' )
	{
		if( $id ) //Tracking the after, this is the ID of the tracking record
		{
			$tracking_rec = Tracking::get1( $id );
			$rec = Tracking::getRecord( $tracking_rec['type'], $tracking_rec['type_id'] );
		
			$after = serialize( $rec );
			
			$info['after'] = $after;
		//	echo'updating record <br />';
			return Tracking::update( $id, $info );
		}
		else //Create and return the new tracking ID
		{
			$rec = Tracking::getRecord( $type, $type_id );
			
			if( $action == 'update' )
				$action = 'update ' . $type;
			
			$before = serialize( $rec );
				
			$info['type'] = $type;
			$info['type_id'] = $type_id;
			$info['user_id'] = $_SESSION['id'];
			$info['action'] = $action;
			$info['before'] = $before;
			
			//echo'record insert, update <br />';
			return Tracking::insert( $info );
		}
	}
	
	static function insertOrderStatusChange($status_info)
	{
		return db_insert( 'order_status_change_history', $status_info);
	}
		
	static function trackDelete( $id=0, $type='', $type_id=0, $action='delete' )
	{
	    global $CFG;
	    
	    $rec = Tracking::getRecord( $type, $type_id );
	    
	    $action = $action . ' ' . $type;

	    $before = serialize( $rec );

	    $info['type'] = $type;
	    $info['type_id'] = $type_id;
	    $info['user_id'] = $_SESSION['id'];
	    $info['action'] = $action;
	    $info['before'] = $before;
	    $info['after'] = "DELETED";
	    //echo'record insert, update <br />';
	    return Tracking::insert( $info );
	}
	
	static function getRecord( $type, $type_id )
	{
		switch( $type )
		{
			case 'product':
			    $rec = Products::get1( $type_id );
			    break;
		    
			case 'order_charge':
				$rec = Orders::get1Charge( $type_id );
				break;			
			case'order_item':
				$rec = Orders::getItem( $type_id );
				break;			
			
			case 'order':
				$rec = Orders::get1( $type_id );
				break;
			
		}
		
		return $rec;
	}
	
	static function get( $id=0, $type_id=0, $type='', $action='', $user_id=0 )
	{
		global $CFG;
		
		$sql = " SELECT tracking.*, CONCAT( admin_users.first_name, ' ', admin_users.last_name ) as user_name ";
		
		$sql .= " FROM tracking ";
		$sql .= " LEFT JOIN admin_users on admin_users.id = tracking.user_id ";
		
		$sql .= " WHERE 1 ";
		
		if( $id )
		{
			$sql .= " AND tracking.id = " . (int) $id;
		}
		
		if( $type_id )
			$sql .= " AND tracking.type_id = " . (int) $type_id;
			
		if( $type !='' )
			$sql .= " AND tracking.type = '" . addslashes( $type ) . "' ";
			
		if( $action !='' )
			$sql .= " AND tracking.action = '" . addslashes( $action ) . "' ";
		
		if( $user_id )
			$sql .= " AND tracking.user_id = " . (int) $user_id;
			
		$sql .= " ORDER BY tracking.date DESC ";
			
		return db_query_array( $sql );
	}
	
	static function get1( $id )
	{
		global $CFG;
		
		return db_get1( Tracking::get( (int) $id ) );
	}
	
	static function compare( $id, $full_display=false )
	{
		global $CFG;
		
		$rec = Tracking::get1( $id );
		$change_found = false;
		if( $rec['after'] != '' )
			$after = unserialize( $rec['after'] );
		if( $rec['before'] != '' )
			$before = unserialize( $rec['before'] );

		if( $full_display )
		{
		    ob_start();
		    print_ar( $before );

		    $ret[0] = ob_get_clean();
		    ob_start();
		    print_ar( $after );

		    $ret[1] = ob_get_clean();

		    return $ret;
		}

		if( $rec['before'] == '' )
		{
			$before_table = 'New Record';	
			ob_start();
			?>
			<table>
			<?
			foreach( $after as $key => $value )
			{
				?>
				<tr>
					<td><b><?=$key?></b></td>
					<td><?=$value?></td>
				</tr>
				<?
			}
			?>
			
			</table>
			<?
			$after_table = ob_get_clean();
			$change_found = true;
		}
		else if( is_array( $before ) && is_array( $after ) ) 
		{
			$before_table = "<table>";
			$after_table = "<table>";
			
			foreach( $before as $key => $value )
			{
				if( $before[$key] != $after[$key] )
				{
					$change_found = true;
					$before_table .= "<tr><td><b>" . $key . "</b></td><td>" . $before[$key] ."</td></tr>";
					$after_table .= "<tr><td><b>" . $key . "</b></td><td>" . $after[$key] ."</td></tr>";
				}
			}
			$before_table .= "</table>";
			$after_table .= "</table>";
		}
		if( $change_found )
		{
			$ret[0] = $before_table;
			$ret[1] = $after_table;
		}
		else 
			$ret = false;
		
		return $ret;
	}
	
	static function getTrackingForOrder( $id )
	{
		//Grab all the types of tracking for everything associated with the order
		
		//Types:
		//order, order_ce, order_charge, order_exam, order_health, order_homestudy
		
		$sql = " SELECT tracking.* FROM tracking WHERE type = 'order' and type_id = " . (int) $id;
		
		$sql .= " UNION SELECT tracking.* FROM tracking
				  LEFT JOIN order_items on order_items.id = tracking.type_id WHERE tracking.type = 'order_item'
				  and order_items.order_id = " . (int) $id;
		
		
		$sql .= " ORDER BY date DESC ";
		
		//echo $sql;
		
		return db_query_array( $sql );
		
	}
	
	
	static function getTrackingForProduct( $id )
	{
		//Grab all the types of tracking for everything associated with the order
		
		//Types:
		//order, order_ce, order_charge, order_exam, order_health, order_homestudy
		
		$sql = " SELECT tracking.* FROM tracking WHERE type = 'product' and type_id = " . (int) $id;
		$sql .= " ORDER BY date DESC ";
		
		//echo $sql;
		
		return db_query_array( $sql );
		
	}
	
}

?>