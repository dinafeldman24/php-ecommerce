<?
class SearchTerms {
	static function insert($info) {
		return db_insert ( 'search_terms_history', $info);
	}
	static function update($id, $info) {
		return db_update ( 'search_terms_history', $id, $info, 'id');
	}
	static function delete($id) {
		return db_delete ( 'search_terms_history', $id );
	}
	static function get($id = 0, $keywords='', $start_date='', $end_date='',$group_by_term=true, $top_search=false, $order = '', $order_asc = '', $limit = 0, $start = 0, $extra_params='') {

		$sql .= "SELECT id, term, MAX( search_terms_history.date ) as most_recent_date, results_found_exact, results_found_loose, spelling_correction, brand_redirect, category_redirect, filters, filter_automatic, COUNT(*) as number_of_searches  ";
					
		$sql .= " FROM search_terms_history ";
		
		$sql .= " WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND search_terms_history.id = $id ";
		}
		
		if ($keywords != '') {
			$term_fields = Array('search_terms_history.term');
			$spell_fields = Array('search_terms_history.spelling_correction');
			$sql .= " AND (" . db_split_keywords($keywords,$term_fields,'AND',true);
			$sql .= " OR " . db_split_keywords($keywords,$spell_fields,'AND',true) .") ";
		}
		
		if ($extra_params != '') {
			switch($extra_params){
				case 'no_results':
					$sql .=" AND results_found_exact = 0 AND brand_redirect = 'N' AND category_redirect = 'N' ";
					break;
				case 'spelling':
					$sql .=" AND spelling_correction <> '' ";
					break;
			}
		}
		
		if(isset($start_date) || isset($end_date)){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('search_terms_history.date',$start_date,$end_date);
		}		
		
		if($group_by_term || $top_search){
			$sql .= ' GROUP BY search_terms_history.term, search_terms_history.filters ';
		}else{
			$sql .= ' GROUP BY search_terms_history.id ';
		}
		
		if ($order) {
			$sql .= " ORDER BY ";
			$sql .= addslashes ( $order );
			
			if ($order_asc !== '' && ! $order_asc) {
				$sql .= ' DESC ';
			}else{
				$sql .= " ". addslashes ( $order_asc ) ." ";
			}
		} elseif($top_search){
			$sql .= " ORDER BY COUNT(*) DESC";
		} else {
			$sql .= " ORDER BY search_terms_history.id DESC";
		}
		
		if ($limit > 0) {
			$sql .= db_limit ( $limit, $start );
		}
		//echo $sql;
		
		$ret = db_query_array ( $sql );
		return $ret;
	}
	static function get1($id) {
		$id = ( int ) $id;
		
		if (! $id)
			return false;
		$result = self::get ( $id );
		
		return is_array ( $result ) ? array_shift ( $result ) : false;
	}
}

