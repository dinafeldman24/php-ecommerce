<?
class AmazonCommissionHistory
{ 
	static function insert($info)
	{
		return db_insert('amazon_commission_history',$info);
	}
	
	static function delete($id)
	{
		return db_delete('amazon_commission_history',$id);
	}

	static function update($id,$info)
	{
		//print_ar($info);
		return db_update('amazon_commission_history',$id,$info,'order_item_id');
	}
	
	static function get($item_id='', $order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(amazon_commission_history.id)) AS total ";
		} else {
			$sql .= " amazon_commission_history.* ";
		}
	
		$sql .= " FROM amazon_commission_history ";
	
		$sql .= " WHERE 1 ";
	
		if ($item_id != "") {
			$sql .= " AND amazon_commission_history.order_item_id = '$item_id' ";
		}
	
		if (!$get_total){	
			
			if($order){
				$sql .= " ORDER BY ";
	
				$sql .= addslashes($order);
	
				if ($order_asc !== '' && !$order_asc) {
					$sql .= ' DESC ';
				}
			}
		}
	
		if ($limit > 0) {
			$sql .= db_limit($limit,$start);
		}
	
		if (!$get_total) {
			$ret = db_query_array($sql);
		} else {
			$ret = db_query_array($sql,'',true);
		}
	
		return $ret;
	}
	
	static function get1($item_id)
	{
		if (!$item_id) return false;
		$result = self::get($item_id);
	
		return $result[0];
	}
	
	static function getCommissionDifference($order_id){
		$sql = " SELECT SUM(commission_paid - expected_commission) as commission_difference
				FROM amazon_commission_history 
				LEFT JOIN order_items ON order_items.id = amazon_commission_history.order_item_id 
				WHERE order_id = $order_id ";
		$ret = db_query_array($sql);
		return $ret[0]['commission_difference'];
	}

}
?>