<?php

class OrderStatuses {
	
	function insert($info)
	{		
		return db_insert('order_statuses',$info);
	}
	
	function update($id,$info)
	{
		return db_update('order_statuses',$id,$info);
	}
	
	function delete($id)
	{
		return db_delete('order_statuses',$id);
	}
	
	//$id can be a comma separated string or a single id
	function get($id=0)
	{
		$sql = "SELECT order_statuses.*
				FROM order_statuses
				WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND order_statuses.id IN ($id) ";
		}
		
		$sql .= " GROUP BY order_statuses.id
				  ORDER BY order_statuses.step_no";
		
		return db_query_array($sql);
	}
	
	function get1($id)
	{
		$id = (int) $id;
		
		if (!$id) return false;
		
		$result = OrderStatuses::get($id);
		return $result[0];
	}
}

?>