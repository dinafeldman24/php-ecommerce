<?php

require_once $CFG->libdir . '/vendor/autoload.php';

class tcMailchimp{

    public $root  = 'https://api.mailchimp.com/3.0';
    public $apikey;
    public $api;

    function tcMailchimp($is_debug = false){
        global $CFG;
        $this->apikey = $CFG->mailchimp_api_key;
        $opts = array();
        if($is_debug){
            $opts['debug'] = true;
        }
        $this->api = new Mailchimp($this->apikey, $opts);
 //       $this->api->debug = true;
        $this->api->root = str_replace('2.0', '3.0', $this->api->root);
        curl_setopt($this->api->ch, CURLOPT_USERPWD,  "xxx:" . $this->apikey);  
    }

    public static function is_valid_email($email){
        $pattern = '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/';
        return (preg_match($pattern, $email) == 1);
    }

    public static function getUserHash($email){
        return md5(strtolower($email));
    }

    public static function is_email_on_list($email){
        $response =  self::get_email_status($email);
        if(!$response){
            return false;
        }
        return  $response['status'] == 'subscribed';
    }

    public static function get_email_status($email, $list_id = null){
        global $CFG;
        $mc = new tcMailchimp();       

        if(!$list_id){
            $list_id = $CFG->list_id_val;
        }
        $user_hash = self::getUserHash($email); 
        $url = 'lists/' . $list_id . '/members/' . $user_hash;

        try{
            $response = $mc->api->call($url);
        }
        catch(Mailchimp_HttpError $e){
            return $e->getMessage();
        }
        catch(Mailchimp_Error $e3){
            $info = curl_getinfo($mc->api->ch);
            if($info['http_code'] == 404){
                return false;
            }
            return $e3->getMessage();
        }
        catch(Error $e2){
            return $e2->getMessage();
        }
        return $response;
    }

    public static function add_or_update($email, $status = 'pending', $merge_fields = null, $list_id = null,$email_type='html', $interests = null, $new_email = null){
        global $CFG;
        $mc = new tcMailchimp();
        if(!$list_id){
            $list_id = $CFG->list_id_val;
        }
        $hash = self::getUserHash($email);
        $url = 'lists/' . $list_id . '/members/' . $hash;
        $method = 'PUT';
        $params = array('status' => $status,
            'email_address' => $email,
            'email_type' => $email_type
        );
        if($merge_fields){
            $params['merge_fields'] = $merge_fields;
        }
        if($interests){
            $params['interests'] = $interests;
        }
        //this is for updating existing user's email address
        if($new_email && $new_email != $email){
            $params['email_address'] = $new_email;
        }
        try{
            $response = $mc->api->call($url, $params, $method);
			//mail('tova@tigerchef.com','mailchimp call',var_export($response,true));
        }
        catch(Mailchimp_HttpError $e){
            return $e->getMessage();
        }
        catch(Mailchimp_Error $e3){
mail('rachel@tigerchef.com', 'Mailchimp Error', $e3->getMessage());
            return false;
        }
        return true;
    }
    
    static function unsubscribe_user($email_address, $list_id = null){
        if(!self::is_valid_email($email_address)){
            return 'Invalid Email Address';
        }
        if(!self::is_email_on_list($email_address)){
            return 'This email address is not on our mailing list';
        }
        global $CFG;
        $mc = new tcMailchimp();
        if(!$list_id){
            $list_id = $CFG->list_id_val;
        }
        $hash = self::getUserHash($email_address);
        $url = 'lists/' . $list_id . '/members/' . $hash;
        $method = 'PATCH';
        $params = array('status' => 'unsubscribed');
        try{
            $response = $mc->api->call($url, $params, $method);
        }
        catch(Mailchimp_HttpError $e){
            return $e->getMessage();
        }
        catch(Mailchimp_Error $e3){
mail('rachel@tigerchef.com', 'Mailchimp Error', $e3->getMessage());
            return 'We encountered an error unsubscribing this address from our list.';
        }
        return true;
    }
	
	public static function interest_categories($list_id = null,$category_id = null,$count='10',$offset=0){
        global $CFG;
        $mc = new tcMailchimp();       

        if(!$list_id){
            $list_id = $CFG->list_id_val;
        }
        $url = 'lists/' . $list_id . '/interest-categories';
		
		if($category_id){$url .= '/' . $category_id .'/interests' ;}
		
		if($count){$params[] = "count={$count}";} 
		if($offset){$params[] = "offset={$offset}";}
		if(is_array($params)){$url .= '?'.implode('&',$params);}

        try{
            $response = $mc->api->call($url);
        }
        catch(Mailchimp_HttpError $e){
            return $e->getMessage();
        }
        catch(Mailchimp_Error $e3){
            $info = curl_getinfo($mc->api->ch);
            if($info['http_code'] == 404){
                return false;
            }
            return $e3->getMessage();
        }
        catch(Error $e2){
            return $e2->getMessage();
        }
        return $response;
    }
	
	
	function ecommOrderAdd($order) {
        		
		return $this->api->ecomm->orderAdd($order);
    }
	
	function ecommOrderDel($order_id, $store_id) {
        		
		return $this->api->ecomm->orderDel($store_id,$order_id);
    }
 

}

?>
