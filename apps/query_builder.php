<?php

	class QueryBuilder {
		
		static function insert( $info )
		{
			$id = db_insert( 'query_builder', $info );
			return $id;
		}
		
		static function update( $id, $info )
		{
			return db_update( 'query_builder', $id, $info );
		}
				
		static function delete( $id )
		{
			return db_delete( 'query_builder', $id );
		}
		
		static function get( $id=0, $is_active='')
		{
			$sql = " SELECT query_builder.* ";
					
			$sql .= "  FROM query_builder ";

			$sql .= " WHERE 1 ";
			
			if( $id )
				$sql .= " AND query_builder.id = " . (int) $id;
			if( $is_active ){
				$sql .= " AND query_builder.is_active = '" . addslashes( $is_active ) . "' ";
			}
				
			return db_query_array( $sql );
		}
		
		static function get1( $id )
		{
			return db_get1( self::get( $id ) );
		}
		
		/**
		 * Get By Object Parameters
		 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
		 */
		static function getByO( $o=NULL){
			$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
		
			$select_ = "SELECT query_builder.* ";

			if($o->total){
				$select_ = "SELECT COUNT(DISTINCT(query_builder.id)) as total ";
			}

			$from_ = " FROM query_builder ";
			
			$where_ = " WHERE 1 ";
		
			if(isset($o->id)){
				$where_ .= " AND query_builder.id = [id] ";
			}
			if( isset( $o->is_active )  ){
				$where_ .= " AND `query_builder`.is_active = [is_active]";
			}
		
			if(!$o->total){
				if($o->order){
					$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
					if($o->order_asc){
						$orderby_ .= " ASC ";
					} else {
						$orderby_ .= " DESC ";
					}
				}
				else {
					$orderby_ = " ORDER BY query_builder.id DESC ";
				}
				if($o->limit){
					$limit_ .= db_limit($o->limit,$o->start);
				}
			}
		
			$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;
		
			$result = db_template_query($sql,$o);
		
			if($o->total){
				return (int)$result[0]['total'];
			}
		
			return $result;
		}
		
		static function get1ByO( $o )
		{
			$result = self::getByO( $o );
		
			if( $result )
				return $result[0];
		
			return false;
		}		
	}
	

?>