<? 
class HolidayDates
{
	function getAllIntoArray()
	{
		$holiday_dates_array = array();
		$sql = "SELECT * FROM holiday_dates";
		$recs = db_query_array($sql);
		if ($recs)
		{
			foreach ($recs as $one_date)
			{
				$holiday_dates_array[] = $one_date['date'];
			}
		}
		return $holiday_dates_array;
	}
}
?>