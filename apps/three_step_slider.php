<?php

	class ThreeStepSliders {
		
		static function insert( $info )
		{
			return db_insert( 'three_step_slider', $info );
		}
				
		static function update( $id, $info )
		{
			return db_update( 'three_step_slider', $id, $info );
		}
		
		static function delete( $id )
		{
			
			$qid = db_query("
			DELETE FROM three_step_slider_items
			WHERE slider_id = $id
			");
			return db_delete( 'three_step_slider', $id );
		}
		
		static function get( $id=0, $is_active='', $category_id='' )
		{
			$sql = " SELECT * FROM three_step_slider ";
			$sql .= " WHERE 1 ";
			
			if( $id )
				$sql .= " AND three_step_slider.id = " . (int) $id;
			if( $category_id )
				$sql .= " AND three_step_slider.category_id = " . (int) $category_id;
			if( $is_active ){
				$sql .= " AND three_step_slider.is_active = '" . addslashes( $is_active ) . "' ";
				$sql .= " AND ( (NOW() BETWEEN three_step_slider.start_date AND three_step_slider.end_date )
						OR (NOW()> three_step_slider.start_date AND three_step_slider.end_date = '0000-00-00 00:00:00')) ";
			}

				
			return db_query_array( $sql );
		}
		
		static function get1( $id )
		{
			return db_get1( self::get( $id ) );
		}
		
		/**
		 * Get By Object Parameters
		 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
		 */
		static function getByO( $o=NULL){
			$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
		
			$select_ = "SELECT three_step_slider.* ";
			if($o->total){
				$select_ = "SELECT COUNT(DISTINCT(three_step_slider.id)) as total ";
			}
		
			$from_ = " FROM three_step_slider ";
		
			$where_ = " WHERE 1 ";
		
			if(isset($o->id)){
				$where_ .= " AND three_step_slider.id = [id] ";
			}
		
			if( isset( $o->url )  ){
				$where_ .= " AND `three_step_slider`.url = [url]";
			}
			if( isset( $o->alt )  ){
				$where_ .= " AND `three_step_slider`.alt = [alt]";
			}
			if( isset( $o->is_active )  ){
				$where_ .= " AND `three_step_slider`.is_active = [is_active]";
			}

			if( isset( $o->query )  ){
				$where_ .= " AND ( `three_step_slider`.url LIKE '%".$o->query."%'
								OR `three_step_slider`.alt LIKE '%".$o->query."%'
								OR  `three_step_slider`.id LIKE '%".$o->query."%' )";
			}		
		
			if(!$o->total){
				if($o->order){
					$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
					if($o->order_asc){
						$orderby_ .= " ASC ";
					} else {
						$orderby_ .= " DESC ";
					}
				}
				else {
					$orderby_ = " ORDER BY three_step_slider.id DESC ";
				}
				if($o->limit){
					$limit_ .= db_limit($o->limit,$o->start);
				}
			}
		
			$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;
		
			$result = db_template_query($sql,$o);
		
			if($o->total){
				return (int)$result[0]['total'];
			}
		
			return $result;
		}
		
		static function get1ByO( $o )
		{
			$result = self::getByO( $o );
		
			if( $result )
				return $result[0];
		
			return false;
		}
		static function getSliderItems( $id )
		{
			$sql = " SELECT three_step_slider_items.*, three_step_slider.name, three_step_slider.title_section_1, three_step_slider.title_section_2, three_step_slider.title_section_3 
					FROM three_step_slider_items
					LEFT JOIN three_step_slider on three_step_slider_items.slider_id=three_step_slider.id
			        WHERE 1 
					AND three_step_slider_items.slider_id = " . (int) $id;
							
			$sql .= " ORDER BY three_step_slider_items.section, three_step_slider_items.order";	
			return db_query_array( $sql );
		}
		static function insertItem( $info )
		{
			return db_insert( 'three_step_slider_items', $info );
		}
		static function updateItem( $id, $info )
		{
			return db_update( 'three_step_slider_items', $id, $info );
		}
		
		static function deleteItem( $id )
		{
			return db_delete( 'three_step_slider_items', $id );
		}
		
		static function getItem( $id=0, $is_active='' )
		{
			$sql = " SELECT * FROM three_step_slider_items ";
			$sql .= " WHERE 1 ";
			
			if( $id )
				$sql .= " AND three_step_slider_items.id = " . (int) $id;
			if( $is_active ){
				$sql .= " AND three_step_slider.is_active = '" . addslashes( $is_active ) . "' ";
				$sql .= " AND ( (NOW() BETWEEN three_step_slider.start_date AND three_step_slider.end_date )
						OR (NOW()> three_step_slider.start_date AND three_step_slider.end_date = '0000-00-00 00:00:00')) ";
			}

				
			$result = db_query_array( $sql );
			if( $result )
				return $result[0];
		
			return false;
			
		}
		
	}

?>