<?php

/*****************
*   Class for interface
*   with option fonts
*   Hadassa Golovenshitz
*   Oct 26 2015
*
*****************/

class OptionFontMap{
    
    var $id = null;
    var $option_value_id;
    var $font_family;
    var $files;

    function OptionFontMap($id = null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    function populate($id){
        $this->id = $id;
        $font_map = self::get1($id);
        if(is_array($font_map)){
            $first_font = $font_map[0];
            $this->option_value_id = $first_font['option_value_id'];
            $this->font_family = $first_font['value'];
            foreach($font_map as $this_font){
                $files[] = array('file' => $this_font['file'],
                    'format' => $this_font['format']);   
            }
        }
    }

    static function get($id = null, $option_value_id = null){
        $sql = 'SELECT ofm.id as option_font_map_id, f.id as id, option_value_id, value,
            f.file, f.format FROM option_font_map ofm LEFT JOIN option_font_map_files
            f ON ofm.id = f.option_font_map_id WHERE 1 ';
        if($id){
            $sql .= 'AND ofm.id = ' . $id ;
        }
        if($option_value_id){
            $sql .= "AND option_value_id = $option_value_id";
        }
        return db_query_array($sql);
    }

    static function get1($id){
        return self::get($id);
    }

    static function getForOptionValue($option_value_id){
        return self::get(null, $option_value_id);
    }

    function insertOrUpdate($font_info){
        $insert = false;
        $first_font = $font_info[0];
        if(!isset($first_font['option_font_map_id'])){
            $insert = true;
        }
        $sql = "INSERT INTO option_font_map (option_value_id, option_type
            ,value) VALUES (" . $first_font['option_value_id'] . ", '" . $first_font['option_type']
            . "',  '" . $first_font['font_family'] . "') ON DUPLICATE KEY
            UPDATE option_type=VALUES(option_type), value=VALUES(value)";
        $res = db_query($sql);
        if(!$res){
            return false;
        }
        if($font_type == 'font'){
            return $res;
        }
        //we have files 
        if($insert){
            $option_font_map_id = mysql_insert_id();
        }
        elseif($first_font['option_font_map_id'] == 0){
            $res = db_query_array("SELECT id FROM option_font_map WHERE option_value_id = " . $first_font['option_value_id']);
            $this_row = $res[0];
            $option_font_map_id = $this_row['id'];
        }
        else{
            $option_font_map_id = $first_font['option_font_map_id'];
        }
        
        foreach($font_info as $curr){
            if(isset($curr['id']) && !empty($curr['id']) && $curr['id']> 0){
                $sql = 'UPDATE option_font_map_files SET file = \'' . $curr['file'] . '\', format = \'' . $curr['format'] . '\' WHERE id =  ' . $curr['id'];
            }
            else{
                $sql = 'INSERT INTO option_font_map_files (option_font_map_id, file, format) VALUES (' . $option_font_map_id . ', \'' . $curr['file'] . '\', \'' . $curr['format'] . '\')';
            }
            $res = db_query($sql);
            if(!$res){
                return false;
            }
        }
        self::regenerateFontCSS();
        return true;
}

static function getAllFonts($file_only = false){
    $sql = "SELECT f.value, fc.file, fc.format, ov.id as ov_id FROM option_values ov
         JOIN option_font_map f on f.option_value_id = ov.id
         LEFT JOIN option_font_map_files fc on f.id = fc.option_font_map_id";
    if($file_only){
        $sql .= " WHERE NOT(ISNULL(fc.id))";
    }
    $sql .= ' ORDER BY f.value, fc.option_font_map_id';
    return db_query_array($sql);
}

static function closeFontDeclaration($fs){
    return trim(trim($fs, ', '), ';') . " ;font-weight:normal; font-style:normal;}";  
}

static function regenerateFontCSS(){
    global $CFG;
    $all_fonts = self::getAllFonts(true);
    $fs = '';
    $current_font = '';
    foreach($all_fonts as $font){
        if($font['file'] == ''){
            continue;
        }
        if($font['value'] != $current_font){
            if($current_font != ''){
               $fs = self::closeFontDeclaration($fs);
            }
            $fs .=   "@font-face{font-family:\"{$font['value']}\";" ;
            $current_font = $font['value'];
        }
        $fs .= "src:url({$CFG->fonts_assets_url}/{$font['file']})";
        if(!empty($font['format'])){
            $fs .= " format('" . $font['format'] . "') ";    
        }
        $fs .= ";";
    }


    $fs = self::CloseFontDeclaration($fs);
    $search = array(";;", ';');
    $replace = array(';', ';' . PHP_EOL);
    $fs = str_replace($search, $replace, $fs);
    $file = $CFG->css_assets_dir . '/fonts.css'; 
    $num_bytes = file_put_contents($file, $fs);
    $cdn_file = 'fonts/fonts.css';
    $pull_zone = $CFG->max_cdn_assets_zone_id;
    //$mcdn = new TCMaxCDN();
      //  $purge_result = $mcdn->purgeFile($cdn_file, $pull_zone);
    }
}
