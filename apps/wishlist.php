<?php

class WishList
{   
    public static function insert($info)
	{		
		return db_insert('wishlist',$info);
	}
	
	public static function update($id,$info)
	{
		return db_update('wishlist',$id,$info);
	}
	
	public static function delete($id)
	{
		return db_delete('wishlist',$id);
	}
		
	public static function get($id=0, $customer_id=0, $product_id=0, $order='', $order_asc='')
	{
		$sql = "SELECT wishlist.*
				FROM wishlist
				WHERE 1 ";
		
		if ($id > 0) 
		{
			$sql .= " AND wishlist.id = $id ";
		}
			
		if ($customer_id > 0)
		{
		    $sql .= " AND wishlist.customer_id = $customer_id ";    
		}
		
		if ($product_id > 0)
		{
		    $sql .= " AND wishlist.product_id = $product_id ";    
		}
		
		$sql .= "ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'wishlist.id';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' DESC ';
		}
		
		return db_query_array($sql);
	}
	
	public static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = self::get($id,'','','','','','','');
		return $result[0];
	} 

	public static function get1_by_customer_and_product($customer_id, $product_id)
	{
	    if (!$customer_id || !$product_id)
	    {
	       return false;    
	    }
	    
	    $result = self::get(0, $customer_id, $product_id);
	    return $result[0];
	}
	
	public static function get_items_for_customer($customer_id)
	{
	    return self::get(0, $customer_id);    
	}
}

?>