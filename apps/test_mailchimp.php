<?php

require_once('application.php');
include('apps/mailchimp.php');
$emails = array('hgolov@gmail.com', 'hgolov1@gmail.com', 'hgolov@projectinspire.com');

$merge_fields = array('JOIN-DISCO' => '3DKW35F');
$response = tcMailchimp::add_or_update('hgolov@projectinspire.com', 'subscribed', $merge_fields);
echo 'the response: ' . var_export($response, 1) . PHP_EOL;
for($i = 0; $i < sizeof($emails); $i++){
    $is_on_list = tcMailchimp::is_email_on_list($emails[$i]);
    if($is_on_list) echo $emails[$i] . ' is on list ';
    else echo $emails[$i] . ' is not on list ';
    echo PHP_EOL;
}
?>
