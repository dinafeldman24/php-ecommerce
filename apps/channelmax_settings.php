<? 
class ChannelMaxSettings
{
	static function getByType($type)
	{
		$sql = "SELECT channelmax_settings.* 
				FROM channelmax_settings WHERE setting_type = '".$type."'";
		$result = db_query_array($sql);
		return $result;
	}
	
	static function get1($id)
	{
		$sql = "SELECT channelmax_settings.* 
				FROM channelmax_settings WHERE id = '".$id."'";
		$result = db_query_array($sql);
		return $result[0];
	}
	
	static function get1BySkuAndType($vendor_sku, $setting_type)
	{
		$sql = "SELECT channelmax_settings.* 
				FROM channelmax_settings WHERE vendor_sku = '".$vendor_sku."'
				AND setting_type = '$setting_type'";
		$result = db_query_array($sql);
		return $result[0];
	}
	
	static function insert($info,$date = '')
	{
		return db_insert('channelmax_settings',$info,$date);
	}

	static function update($id,$info,$date = '')
	{
	    return db_update('channelmax_settings',$id,$info,'id',$date);
	}
	
	static function delete($id)
	{
	    return db_delete('channelmax_settings',$id);
	}
}
?>