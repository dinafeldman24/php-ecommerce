<?php
//MaxCDN library was installed with Composer, the next
//line loads the library
require_once 'vendor/autoload.php';

class TCMaxCDN{

    var $api;

    function TCMaxCDN(){
        global $CFG;
        $this->api = new MaxCDN($CFG->max_cdn_alias,
                    $CFG->max_cdn_key,
                    $CFG->max_cdn_secret
        );
    }

    function purgeFile($filename, $pull_zone_id){
        $params = array('file' => $filename);
        $cmd ='/zones/pull.json/' . $pull_zone_id . '/cache';
        return $this->api->delete($cmd, $params);  
    }

    function purgeZone($pull_zone_id){
        $cmd = '/zones/pull.json/' . $pull_zone_id . '/cache';
        return $this->api->delete($cmd);
    }
}
