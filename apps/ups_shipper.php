<?

class UPSShipper{

	static function insert($info){
		return db_insert("ups_shippers",$info,'date_added');
	}

	static function update($id,$info){
		return db_update("ups_shippers",$id,$info);
	}

	static function delete($id){
		return db_delete("ups_shippers",$id);
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT ups_shippers.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(ups_shippers.id)) as total ";
		}

		$from_ = " FROM ups_shippers ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND ups_shippers.id = [id] ";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY ups_shippers.priority ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}
		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}


}