<? 
class CatContentMapping
{
	static function insert($info)
	{	
		return db_insert('cat_content_mapping',$info);
	}
	
	static function update($id,$info)
	{
		return db_update('cat_content_mapping',$id,$info);
	}

	static function delete($id)
	{
		return db_delete('cat_content_mapping',$id);
	}
	
	static function get($content_page_id=0, $cat_id=0, $orderby='')
	{
		$sql = "SELECT cat_content_mapping.content_page_id as id, cat_content_mapping.cat_id FROM cat_content_mapping, content
				WHERE cat_content_mapping.content_page_id = content.id";
		if ($content_page_id > 0) 
		{
			$content_page_id = (int) $content_page_id;
			$sql .= " AND cat_content_mapping.content_page_id = $content_page_id";
		}
		if ($cat_id > 0) 
		{
			$cat_id = (int) $cat_id;
			$sql .= " AND cat_content_mapping.cat_id = $cat_id";
		}
		
		if ($orderby != '') $sql .= " ORDER BY " . $orderby;
		else $sql .= " ORDER BY content.title ASC";
		return db_query_array($sql);
	}
	
	static function associateCatsWithResource( $content_page_id, $cat_ids )
	{
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";
		
		db_delete('cat_content_mapping',$content_page_id,'content_page_id');
		
		if (trim($cat_ids_str) == '') {
			return true;
		}

		// insert selected ones
		$info = array();
		$info['content_page_id'] = $content_page_id;

		foreach ($cat_ids as $cid) {
			$info['cat_id'] = $cid;
			self::insert($info);
		}

		return true;
	}
	
}
?>