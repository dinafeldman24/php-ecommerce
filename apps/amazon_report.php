<?php

class AmazonReport{

	const COL_ID = 'id';
	const COL_REQUEST_ID = 'request_id';
	const COL_REPORT_ID = 'report_id';
	const COL_DATE_REQUESTED = 'date_requested';
	const COL_IS_DOWNLOADED = 'is_downloaded';
	const COL_FILENAME = 'filename';


	static function insert($info, $date='')
	{
	    return db_insert('amazon_report',$info,$date);
	}

	static function update($id,$info,$date='')
	{
	    return db_update('amazon_report',$id,$info,'id',$date);
	}

	static function delete($id)
	{
	    return db_delete('amazon_report',$id);
	}

	static function get($id=0,$order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
		    $sql .= " COUNT(DISTINCT(amazon_report.id)) AS total ";
		} else {
		    $sql .= " amazon_report.* ";
		}

		$sql .= " FROM amazon_report ";

		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND amazon_report.id = $id ";
		}

		if (!$get_total){
		    $sql .= ' GROUP BY amazon_report.id ';

		    if($order){
		    	$sql .= " ORDER BY ";

	        	$sql .= addslashes($order);

		    	if ($order_asc !== '' && !$order_asc) {
		        	$sql .= ' DESC ';
		    	}
		    }
		}

		if ($limit > 0) {
		    $sql .= db_limit($limit,$start);
		}

		if (!$get_total) {
		    $ret = db_query_array($sql);
		} else {
		    $ret = db_query_array($sql,'',true);
		}

		return $ret;
	}

	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);

        return $result[0];
    }

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT amazon_report.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(amazon_report.id)) as total ";
		}

		$from_ = " FROM amazon_report ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND amazon_report.id = [id] ";
		}

		if($o->request_id != ''){
			$where_ .= " AND `amazon_report`.request_id = [request_id]";
		}
		if($o->report_id != ''){
			$where_ .= " AND `amazon_report`.report_id = [report_id]";
		}
		if(is_array($o->date_requested)){
			if($o->date_requested['start']){
				$o->date_requested_start = $o->date_requested['start'];
				$where_ .= " AND `amazon_report`.date_requested >= [date_requested_start]";
			}
			if($o->date_requested['end']){
				$o->date_requested_end = $o->date_requested['end'];
				$where_ .= " AND `amazon_report`.date_requested <= [date_requested_end]";
			}
		} else 		if($o->date_requested != ''){
			$where_ .= " AND `amazon_report`.date_requested = [date_requested]";
		}
		if($o->is_downloaded != ''){
			$where_ .= " AND `amazon_report`.is_downloaded = [is_downloaded]";
		}
		if($o->filename != ''){
			$where_ .= " AND `amazon_report`.filename = [filename]";
		}
		if($o->report_type != ''){
			$where_ .= " AND `amazon_report`.report_type = [report_type]";
		}
		if($o->is_imported != ''){
			$where_ .= " AND `amazon_report`.is_imported = [is_imported]";
		}
		if($o->has_id != '' && $o->has_id == true){
			$where_ .= " AND `amazon_report`.report_id != ''";
		}
		if($o->amazon_country != ''){
			$where_ .= " AND `amazon_report`.amazon_country = [amazon_country]";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY amazon_report.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having. $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	} 

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );
	    
	    if( $result )
		return $result[0];

	    return false;
	}


	static function checkReport( $reportInfo )
	{
	    $o->request_id = $reportInfo->getReportRequestId();

	    $reports = self::getByO( $o );

	    if( $reports )
	    {
		foreach( $reports as $report )
		{
		    if( $o->request_id == $report['request_id'] && $report['report_id'] == 0 )
		    {
			$info['report_id'] = $reportInfo->getReportId();

			self::update($report['id'], $info);
		    }
		}
	    }
	    else if ($reportInfo->getReportType() == "_GET_PAYMENT_SETTLEMENT_DATA_") /* check if there is a settlement report available */
	    {
	    	$o = new Object();
	    	$o->report_id = $reportInfo->getReportId();
	    	$report_already_there = AmazonReport::getByO($o);
	    	if (!$report_already_there)
	    	{
	    		$info['report_id'] = $reportInfo->getReportId();
	    		$info['date_requested'] = date('Y-m-d H:i:s');
	    		$info['report_type'] = 'settlement_report';
	    		self::insert($info);	    		
	    	}

	    }

	    return false;
	}

	/*
	 * Function to reset amazon orders to their original shipping and tax amounts
	 */
	static function resetShippingAndTax( $id )
	{
	    global $CFG;

	    $report = self::get1( $id );

	    $report_dir = $CFG->dirroot . '/edit/amazon_reports/';
	    $counter = 0;

	    $contents = file_get_contents( $report_dir . $report['filename'] );
	    $file_name = $report_dir . $report['filename'];
	    $contents = explode( "\r", $contents );
	    $rows = array();
	    //print_ar( $contents );
	    foreach( $contents as $c )
	    {
		//print_ar( $c );
		$rows[] = explode( "\t", $c );
	    }

	    $updated_orders = array();
	    foreach( $rows as $key => $row )
	    {
		$counter++;
		$columns = $row;


		if( strtoupper($columns[0]) == 'ORDER-ID' || !$columns[0] )
		{
			foreach( $columns as $key => $value )
			{
				$value = strip_tags( $value );
				$value = str_replace( '"', '', trim($value) );
				$value = str_replace( ' ', '_', trim($value) );
				$layout[strtolower($value)] = trim($key);
				//print_ar( $value );
			}
			continue;
		}

		$amazon_order_id = trim($columns[$layout['order-id']]);

		if( trim( $amazon_order_id ) == "" )
		    continue;

		$o = (object) array("amazon_order_id"=>$amazon_order_id);
		echo"Looking for amazon order " . $amazon_order_id . "\r\n";
		$current_order = Orders::get1ByO( $o );

		if( !$current_order )
		    continue;

		//Now to reset the order
		
		$order_info['tax_rate'] = $columns[$layout['item-tax']];
		$order_info['shipping'] = $columns[$layout['shipping-price']];
		$order_info['subtotal'] = $columns[$layout['item-price']];

		if( in_array( $current_order['id'], $updated_orders ) )
		{
		    //Order was already updated, multiple line items, so update...

		    $order_info['tax_rate'] = $order_info['tax_rate'] + $current_order['tax_rate'];
		    $order_info['shipping'] = $order_info['shipping'] + $current_order['shipping'];
		    $order_info['subtotal'] = $order_info['subtotal'] + $current_order['subtotal'];
		    Orders::update( $current_order['id'], $order_info );
		}
		else {
		    //Single line item order, or first time through, so reset
		    Orders::update( $current_order['id'], $order_info );

		    $updated_orders[] = $current_order['id'];
		}
	    }
	}

	static function importAmazonOrders( $id , $country)
	{
	    global $CFG;
	    $report = self::get1( $id );

	    $report_dir = $CFG->dirroot . '/edit/amazon_reports/';
	    $counter = 0;
	    $order_id = "";
	    if( $report['is_imported'] == 'N' )
	    {
		$contents = file_get_contents( $report_dir . $report['filename'] );
		$file_name = $report_dir . $report['filename'];
		$contents = explode( "\r", $contents );
		$rows = array();
		//print_ar( $contents );
		foreach( $contents as $c )
		{
		    //print_ar( $c );
		    $rows[] = explode( "\t", $c );
		}
		//$rows = explode( "\t", $contents ); //Tab separated		

		//echo "Processing File: " . $report['filename'] . "\r\n";

		foreach( $rows as $key => $row )
		{
		    $counter++;
		    $columns = $row;
		    //echo "up to $counter";
		    //if ($counter == 30) die();

		    if( strtoupper($columns[0]) == 'ORDER-ID' || !$columns[0] )
		    {
			    foreach( $columns as $key => $value )
			    {
				    $value = strip_tags( $value );
				    $value = str_replace( '"', '', trim($value) );
				    $value = str_replace( ' ', '_', trim($value) );
				    $layout[strtolower($value)] = trim($key);
				    //print_ar( $value );
			    }
			    continue;
		    }

		    if( trim($columns[$layout['order-id']]) == "" || trim($columns[$layout['sku']]) == "")
		    {
			//echo'no order id for ' . $report['filename'] . ' ' . $counter . "\r\n";
		//	echo "no sku or order-id--order ".trim($columns[$layout['order-id']]) . " sku " . trim($columns[$layout['sku']]);
			continue;
		    }

		    $amazon_order_id = trim($columns[$layout['order-id']]);

		    $sales_channel = trim($columns[$layout['sales-channel']]);
		    if ($sales_channel == "" && $country == "CA")
		    {
		    	$order_info['order_tax_option'] = $CFG->amazon_canada_store_id;
		    	$order_info['payment_type'] = "cc";		    	
		    }
		    else if ($sales_channel == "Amazon Checkout")
		    {
		    	$order_info['order_tax_option'] = $CFG->website_store_id;
		    	$order_info['payment_type'] = "cba";		    	
		    }
		    else 
		    {
		    	$order_info['order_tax_option'] = $CFG->amazon_store_id;
		    	$order_info['payment_type'] = "cc";		    	
		    }
		    
		    //$order_info['order_tax_option'] = $CFG->amazon_store_id;
		    $order_info['amazon_order_id'] = $amazon_order_id;
		    $order_item['order-item-id'] = $columns[$layout['order-item-id']];

		    $order_info['date'] = date( 'Y-m-d H:i:s', strtotime( $columns[$layout['purchase-date']] ) );
		    $order_info['email'] = $columns[$layout['buyer-email']];
		    $name_parts = explode(" ", $columns[$layout['buyer-name']]);

		    $order_info['billing_first_name'] = $columns[$layout['buyer-name']];
		    //$order_info['billing_last_name'] = $name_parts[1];

		    $order_info['billing_phone'] = $columns[$layout['buyer-phone-number']];

		    //Need to figure out how to handle this
		    //$order_info['shipping_service'] = $columns[$layout['ship-service-level']];
		    switch( $columns[$layout['ship-service-level']] )
		    {
			case 'Standard':
			    $order_info['shipping_id'] = 1;
			    break;
			case 'Expedited':
			    $order_info['shipping_id'] = 2;
			    break;
			case 'SecondDay':
			    $order_info['shipping_id'] = 3;
			    break;
			case 'NextDay':
			    $order_info['shipping_id'] = 4;
			    break;
		    }


		    $name_parts = explode( " ", $columns[$layout['recipient-name']] );
		    $order_info['shipping_first_name']= $columns[$layout['recipient-name']];
		    //$order_info['shipping_last_name']= $name_parts[1];

		    $order_info['shipping_address1'] = $columns[$layout['ship-address-1']];
		    $order_info['shipping_address2'] = $columns[$layout['ship-address-2']];

		    $order_info['shipping_city'] = $columns[$layout['ship-city']];
		    $order_info['shipping_state'] = $columns[$layout['ship-state']];
		    $order_info['shipping_zip'] = $columns[$layout['ship-postal-code']];
			$order_info['shipping_country'] = $columns[$layout['ship-country']];
		    $order_info['shipping_phone'] = $columns[$layout['ship-phone-number']];
		   	$order_info['promo_discount'] = abs($columns[$layout['item-promotion-discount']]);

		    //Need to see if I have the item	    
		    $item['sku'] = preg_replace('/'.$CFG->warehouse_sku_suffix.'$/', '', $columns[$layout['sku']]); // remove -WAREHOUSE from the sku if it's there
		    // also switch 0026-PPOK skus to 0026-PPA
		    $item['sku'] = str_replace("0026-PPOK", "0026-PPA", $item['sku']);
		    $item['name'] = $columns[$layout['product-name']];
		    $item['qty'] = $columns[$layout['quantity-purchased']];
		    $item['line_total'] = $columns[$layout['item-price']];
		    // to store for partial refund purposes
		    $item['shipping-price'] = $columns[$layout['shipping-price']];
		    $item['shipping-tax'] = $columns[$layout['shipping-tax']];
		    $item['gift-wrap-price'] = $columns[$layout['gift-wrap-price']];
		    $item['gift-wrap-tax'] = $columns[$layout['gift-wrap-tax']];
		    $item['item-promotion-discount'] = $columns[$layout['item-promotion-discount']];
		    $item['ship-promotion-discount'] = $columns[$layout['ship-promotion-discount']];
		    
		    $item['tax'] = $columns[$layout['item-tax']];
		    $item_merchandise_total = $item['line_total']; 
		    $item_tax_total = $item['shipping-tax'] + $item['gift-wrap-tax'] + $item['tax']; 
   		    // adding the discounts because they're negative in the file from Amazon 
		    $item_charge_total = $item['line_total'] + $item['tax'] + $item['shipping-price'] + $item['shipping-tax'] +$item['gift-wrap-price'] + $item['gift-wrap-tax'] + $item['item-promotion-discount'] + $item['ship-promotion-discount'];
		    $item_total_promotion_discount = abs($item['item-promotion-discount'] + $item['ship-promotion-discount']);
			$item_shipping_total = $item['shipping-price'] + $item['gift-wrap-price']; 
		    $item['gift_msg'] = $columns[$layout['gift-message-text']];
		    $item['amazon_gift_wrap'] = $columns[$layout['gift-wrap-type']];
		    //echo"Item qty = " . $item['qty'] . "\r\n";

		    $item['order_item_id'] = $columns[$layout['order-item-id']];

		    
		    //$shipping['tax'] = $colums[$layout['shipping-tax']];

		    $order_info['current_status_id'] = $CFG->charge_cc_at_id;

		    $o = (object) array("amazon_order_id"=>$amazon_order_id);
		    $current_order = Orders::get1ByO( $o );

		    if( !$current_order )
		    {
		    	// send email w/ profit of the previous order if negative
		    	if ($order_id != "") 
		    	{
		    		$prev_order_profit = Orders::getProfit($order_id, $CFG);
					if ($prev_order_profit[0] < 0) mail("rachel@tigerchef.com, joe@tigerchef.com,etty@tigerchef.com,jack@lionsdeal.com,dilan@tigerchef.com", "Profit for AMAZON order $order_id is $prev_order_profit[0]", $prev_order_profit[1]);
					Orders::determineApprovalStatus($order_id);			    		
		    	}
		    	
			//Only set imported from and date added when there is no current order
			$order_info['imported_from'] = $report['filename'];
			$order_info['date_added'] = date( 'Y-m-d G:i:s' );

			$order_id = Orders::insert( $order_info );
		    /*if ($order_info['shipping_state'] == "NY")
			{
    			$to = 'rivky@tigerchef.com';
    			mail($to, "NY Amazon Order Imported", "Order $order_id was imported with a ship-to state of NY", "From: $CFG->company_name <$CFG->orders_email>");				
			}*/
			$current_order = Orders::get1( $order_id );
		    }
		
		    if( $current_order )
		    {
			//No reason to go over this, if it was sent in a different file it was completed in that file
			if( $report['filename'] != $current_order['imported_from'] )
			    continue;

			//echo"Processing Order: ". $current_order['id'] ."\r\n";
			$o_items = Orders::getItems(0, $current_order['id'] );

			if( $o_items )
			{
			    foreach( $o_items as $o_item )
			    {
				//echo"Checking For Existing Item: " . $item['sku'] . " \r\n";

				//If you are in the initial import file, don't skip duplicate lines 
				//One of the newer amazon import files had 2 lines for 1 item

				$product = Products::get1( $o_item['product_id'] );
				if( strtoupper($product['vendor_sku']) == strtoupper($item['sku']) && $report['filename'] != $current_order['imported_from'] )
				{
				  //  echo 'Existing Item Found \r\n';
				    continue 2;
				}
			    }
			    if ($CFG->site_name == "lionsdeal")
				{ 
					$orig_sku = $item['sku']; 
					$item['sku'] = AmazonReport::doLionsdealSkuConversion($item['sku']);
				}	
				else $orig_sku = '';
					
			    //If I get here the item wasn't added
			    $product = Products::get1ByModel( $item['sku'], true );
			    if( !$product )
				$product = Products::get1ByModel( $item['sku'], false );
				if( !$product && $CFG->site_name == "lionsdeal") 
				{
					echo "up to getting by vendor sku name";
					$product = Products::get1ByVendorSkuName($orig_sku);
				}

			    if ($item['tax'] != "0")
			    {
			    	$zip_info = ZipCodes::get($current_order['shipping_zip']);

                    /* only charging sales tax to NY */
                    if ($zip_info[0]['state_code'] == 'NY')
                    {
                        $taxes          = new Taxes($zip_info[0]['zipcode'], $zip_info[0]['city']);
                        $tax_rate       = $taxes->getRate();
			    		$o_update['tax_rate'] = $tax_rate;			    		
                    }
                    else 
                    {
                    	mail("rachel@tigerchef.com", "tax charged on non-NY CBA order ".$current_order['id']);
                    }
			    }
			    else $o_update['tax_rate'] = 0;
			    $order_info['shipping'] = $columns[$layout['shipping-price']];

			    //Need to add shipping and tax from the item that is being added.
			    $new_shipping = $order_info['shipping'] + $current_order['shipping'];
//			    $new_tax = $order_info['tax_rate'] + $current_order['tax_rate'];
//			    $o_update['tax_rate'] = $new_tax;
			    $o_update['shipping'] = $new_shipping;

			    Orders::update( $current_order['id'], $o_update );

			    if( $product )
			    {
				if( $product['warning'] )
				{
				    Orders::updateWarning( $current_order['id'], $product['warning'] );
				}

				if( $product['name'] != $item['name'] )
				{
					$warning_msg = 'Possible Item Mismatch for ' . $item['sku'] . ':<br/>Amz Name: ' . $item['name'] . '<br/>Our Name: ' . $product['name'] . '<br />';
				    Orders::updateWarning( $current_order['id'], $warning_msg);
				}

				$order_item_id = self::addAmazonProductToOrder( $item, $product, $order_id );
			    }
			    else
			    {
				$order_item_id =  self::addMissingAmazonProduct( $item, $order_id , true, $orig_sku);
			    }
			}
			else {
				if ($item['tax'] != "0")
			    {
			    	$zip_info = ZipCodes::get($current_order['shipping_zip']);
                   	/* only charging sales tax to NY */
                   	if ($zip_info[0]['state_code'] == 'NY')
                   	{
                       	$taxes          = new Taxes($zip_info[0]['zipcode'], $zip_info[0]['city']);
                       	$tax_rate       = $taxes->getRate();
		    			$o_update['tax_rate'] = $tax_rate;			    		
                   	}
                   	else 
                   	{
                   		mail("rachel@tigerchef.com", "tax charged on non-NY CBA order ".$current_order['id']);
                   	}	
		    	}
		    	else $o_update['tax_rate'] = 0;
			    $order_info['shipping'] = $columns[$layout['shipping-price']];

			    //Need to add shipping and tax from the item that is being added.
//			    $new_shipping = $order_info['shipping'] + $current_order['shipping'];
//			    $new_tax = $order_info['tax_rate'] + $current_order['tax_rate'];
//			    $o_update['tax_rate'] = $order_info['tax_rate'];
			    $o_update['shipping'] = $order_info['shipping'];

			    Orders::update( $current_order['id'], $o_update );
				if ($CFG->site_name == "lionsdeal")
				{ 
					$orig_sku = $item['sku'];
					$item['sku'] = AmazonReport::doLionsdealSkuConversion($item['sku']);
				}	
				else $orig_sku = '';
				
			    $product = Products::get1ByModel( $item['sku'], true );
			    if( !$product )	$product = Products::get1ByModel( $item['sku'], false );

			    if( $product )
			    {
				if( $product['warning'] )
				{
				    Orders::updateWarning( $current_order['id'], $product['warning'] );
				}

				if( $product['name'] != $item['name'] )
				{
					$warning_msg = 'Possible Item Mismatch for ' . $item['sku'] . ':<br/>Amz Name: ' . $item['name'] . '<br/>Our Name: ' . $product['name'] . '<br />';
				    Orders::updateWarning( $current_order['id'], $warning_msg);
				}

				$order_item_id = self::addAmazonProductToOrder( $item, $product, $order_id );
			    }
			    else
			    {
				$order_item_id = self::addMissingAmazonProduct( $item, $order_id, true, $orig_sku );
			    }			    
			}
			    $chargeInfo = array(
			    'pid'      => 0,
			    'date'     => date('Y-m-d H:i:s'),
			    'order_id' => $order_id,
			    'action'   => 'sale',
			    'transaction_id' => $order_item_id,
			    'amount'   => $item_charge_total,
			    'response_code' => 1,
			    'response_msg' => 'Payment on Amazon.com',
			    'notes'    => 'Payment on Amazon.com'
		    );
		    $charge_id = Orders::insertCharge($chargeInfo);
		    Orders::insertOrderChargeBreakdown($charge_id, $item_merchandise_total , $item_shipping_total,$item_tax_total,$item_total_promotion_discount);		    
		    }
			
//		    Orders::recalcOrder( $current_order['id'] );

		}

		$report_info['is_imported'] = 'Y';
		AmazonReport::update( $id, $report_info );
	    // send email w/ profit of the previous order if negative
		if ($order_id != "") 
		{
			$prev_order_profit = Orders::getProfit($order_id, $CFG);
			if ($prev_order_profit[0] < 0) mail("rachel@tigerchef.com, joe@tigerchef.com", "Profit for AMAZON order $order_id is $prev_order_profit[0]", $prev_order_profit[1]);
			Orders::determineApprovalStatus($order_id);		    		
		}
	    }

	    return false;
	}

	static function addAmazonProductToOrder( $item, $product, $order_id )
	{
	    global $CFG;

	    if( $product['is_option'] )
	    {
		$order_item['product_id'] = $product['product_id'];
		$option_id = $product['id'];

		$option_info['product_option_id'] = $option_id;
		$option_info['value'] = $product['value'];
		$option_info['additional_price'] = $product['additional_price'];
	    }
	    else {
		$order_item['product_id'] = $product['id'];
		$option_id = 0;
	    }
//Seem to be a lot of duplicates with no order IDs not sure why
	    if( !$order_id )
		return false;
		
	    $order_item['order_id'] = $order_id;
	    
	    if( $item['qty'] )
		$order_item['price'] = $item['line_total'] / $item['qty'];

	    $order_item['gift_msg'] = $item['gift_msg'];
	    $order_item['amazon_gift_wrap'] = $item['amazon_gift_wrap'];

	    $order_item['order_item_id'] = $item['order_item_id'];

	    $order_item['qty'] = $item['qty'];

	    $order = Orders::get1( $order_id );
	    $sub_total = $order['subtotal'] + $item['line_total'];
	    Orders::update( $order_id, array('subtotal'=>$sub_total));

	    $inv = Inventory::get(0,0,0,'','',$product['id'],$option_id,0,$CFG->amazon_store_id );
	    Inventory::deduct( $inv['id'], $item['qty'] );

	    if($inv['qty'] >= $item['qty']){
		    if( $inv['type'] == 'inventory' )
		    {
			$order_item['order_item_status_id'] = $CFG->item_in_stock_status;
			$order_item['delivery_method'] = 'stock';
		    }
		    else {
			$order_item['order_item_status_id'] = $CFG->item_drop_ship_status;
			$product_suppliers  = ProductSuppliers::get($product['id']);

			if (is_array($product_suppliers))
			{
			    $order_item['delivery_method'] = $product_suppliers[0]['delivery_method'];
			}
			else
			{
			    $order_item['delivery_method'] = '';
			}
		    }

			Inventory::deduct( $inv['id'], $details['quantity_ordered'] );


		} else {
			$order_item['order_item_status_id'] = $CFG->item_drop_ship_status;
			$order_item['delivery_method'] = '';
		}

	    $oItem = Orders::insertItem( $order_item );
		AmazonOrderLines::insertAmazonOrderLineAmounts($item,$oItem);
	    if( $product['is_option'] )
	    {
		$option_info['order_item_id'] = $oItem;
		Orders::insertItemOption( $option_info );		
	    }
	    return $oItem;
	}

	static function addMissingAmazonProduct( $item, $order_id, $add_to_order=true, $orig_sku = '' )
	{
	    global $CFG;

	    if ($CFG->site_name != "lionsdeal") 
	    {	    	
	    	mail("rachel@tigerchef.com,joe@tigerchef.com", "adding missing amazon product for $order_id", "");
	    	return;
	    }
	    else 
	    {
	    	$new_product['name'] = $item['name'];
	    
	    	$new_product['mfr_part_num'] = AmazonReport::doLionsdealSkuConversion($orig_sku, true);
	    	$new_product['brand_id'] = AmazonReport::doLionsdealSkuConversion($orig_sku, false, true);
	    	$new_product['vendor_sku_name'] = $item['sku'];
	    	$new_product['is_active'] = 'N';
	    	$new_product['price'] = $item['line_total'] / $item['qty'];

	    	$new_product['added_by'] = $_SESSION['id'];
	    	$new_product['added_by_description'] = 'Amazon Listing Import';
		
		    $new_id = Products::insert( $new_product );
	    	$product = Products::get1( $new_id );
			mail("jack@lionsdeal.com, rachel@tigerchef.com", "Please add cost for new product id $new_id, inserted for order $order_id", "part num is " . $new_product['mfr_part_num'] . " and brand id is ". $new_product['brand_id']);
	    }
	    if( $add_to_order )
		return self::addAmazonProductToOrder($item, $product, $order_id);

	    return $new_id;
	}

	static function submitShippingConfirmation($country='US')
	{
	    global $CFG;
	    //Need to get all orders that have been shipped and create the shipping conirmation feed
	    $order_object->amazon_ship_confirm = 'N';
	    
	    if($country == "US")
	    {
	    	$order_object->order_tax_option = $CFG->amazon_store_id;
	    	// also get shipped Checkout by Amazon orders
	    	$order_object2->amazon_ship_confirm = 'N';
	    	$order_object2->order_tax_option = $CFG->website_store_id;
	    	$order_object2->payment_type = "cba";
	    	$order_object2->current_status_id = $CFG->shipped_order_status_id;
	    	$orders2 = Orders::getByO( $order_object2 );
	    }
	    else if ($country == "CA")
	    {
	    	$order_object->order_tax_option = $CFG->amazon_canada_store_id;
	    	$orders2 = array();
	    }
	    $order_object->current_status_id = $CFG->shipped_order_status_id;
	       
	    $orders = Orders::getByO( $order_object );
	    
	    if (sizeof($orders2) > 0)$orders_merged = array_merge($orders, $orders2);
	    else $orders_merged = $orders;
	    
	    if( $orders_merged)
	    {
		$filename = AmazonMWS::generateFeedFile('_POST_FLAT_FILE_FULFILLMENT_DATA_', '', $country);

		AmazonMWS::submitFeed( '_POST_FLAT_FILE_FULFILLMENT_DATA_', $filename , $country);
	    }
	    else {
		//echo 'No orders found.';
	    }

	    if( $orders_merged )
		foreach( $orders_merged as $order )
		{
		    $o_info['amazon_ship_confirm'] = 'Y';
		    $o_info['amazon_ship_file'] = $filename;
		    Orders::update( $order['id'], $o_info );
		}
	}
	
	static function importAmazonInventoryReport( $id )
	{
		global $CFG;
		$report = self::get1( $id );
	
		$report_dir = $CFG->dirroot . '/edit/amazon_reports/';
		$counter = 0;
		if( $report['is_imported'] == 'N' )
		{
			if (($handle = fopen( $report_dir . $report['filename'] , "r")) !== FALSE)
			{
				$row = 0;
				//echo "Processing File: " . $report['filename'] . "\r\n";
				while (($data = fgetcsv($handle, 3024, "\t")) !== FALSE)
				{					
					$row++;
					if ($row == 1)
					{	
						if ($data[0] != "seller-sku" || $data[1] != "quantity")
						{
							echo "Sorry--file format is incorrect.  Cannot import this file as the lite inventory file.";
							print_r($data);
						}
						AmazonInventoryFeed::empty_table();
						continue;
					}
											
					// seller-sku	quantity	price	product-id
					$info['sku'] = trim($data[0]);
					$info['asin'] = trim($data[3]);
					$info['price'] = trim($data[2]);
					$info['quantity'] = trim($data[1]);
					AmazonInventoryFeed::insert($info);
				}
				fclose($handle);
			}
			$report_info['is_imported'] = 'Y';
			AmazonReport::update( $id, $report_info );
		 }
	}
	
	static function importFBAOrders( $id )
	{
		global $CFG;
		$report = self::get1( $id );
	
		$report_dir = $CFG->dirroot . '/edit/amazon_reports/';
		$counter = 0;
		if( $report['is_imported'] == 'N' )
		{
			if (($handle = fopen( $report_dir . $report['filename'] , "r")) !== FALSE)
			{
				
				//echo "Processing FBA order File: " . $report['filename'] . "\r\n";
				while (($columns = fgetcsv($handle, 3024, "\t")) !== FALSE)
				{
					$counter++;
					$order_info = array();
					$item = array();
	//				echo "counter is $counter in new";
					if ($counter == 1)
					{																									
						if( strtoupper($columns[0]) == 'AMAZON-ORDER-ID' || !$columns[0] )
						{
							foreach( $columns as $key => $value )
							{
								$value = strip_tags( $value );
								$value = str_replace( '"', '', trim($value) );
								$value = str_replace( ' ', '_', trim($value) );
								$layout[strtolower($value)] = trim($key);
								//print_ar( $value );
							}
							continue;
						}
						else 
						{
							echo "invalid format of FBA orders file ";
							exit;
						}
						
					}
	
					if( trim($columns[$layout['amazon-order-id']]) == "")
					{
						echo'no order id for ' . $report['filename'] . ' ' . $counter . "\r\n";
						continue;
					}

					// for now, ignore fba dropships
					if ($columns[$layout['sales-channel']]== "Non-Amazon") 
					{
//						echo "gonna continue";
						continue;
					}
					
					$amazon_order_id = trim($columns[$layout['amazon-order-id']]);
					$order_info['order_tax_option'] = $CFG->amazon_fba_store_id;
					$order_info['amazon_order_id'] = $amazon_order_id;
					$order_item['order-item-id'] = $columns[$layout['amazon-order-item-id']];
	
					$order_info['date'] = date( 'Y-m-d H:i:s', strtotime( $columns[$layout['purchase-date']] ) );
					$order_info['email'] = $columns[$layout['buyer-email']];
					$name_parts = explode(" ", $columns[$layout['buyer-name']]);
	
					$order_info['billing_first_name'] = $columns[$layout['buyer-name']];
				//	$order_info['billing_last_name'] = $name_parts[1];
	
					$order_info['billing_phone'] = $columns[$layout['buyer-phone-number']];
	
					//Need to figure out how to handle this
					//$order_info['shipping_service'] = $columns[$layout['ship-service-level']];
					switch( $columns[$layout['ship-service-level']] )
					{
						case 'Standard':
							$order_info['shipping_id'] = 1;
							break;
						case 'Expedited':
							$order_info['shipping_id'] = 2;
							break;
						case 'SecondDay':
							$order_info['shipping_id'] = 3;
							break;
						case 'NextDay':
							$order_info['shipping_id'] = 4;
							break;
					}
	
	
					$name_parts = explode( " ", $columns[$layout['recipient-name']] );
					$order_info['shipping_first_name']= $columns[$layout['recipient-name']];
					//$order_info['shipping_last_name']= $name_parts[1];
	
					$order_info['shipping_address1'] = $columns[$layout['ship-address-1']];
					$order_info['shipping_address2'] = $columns[$layout['ship-address-2']];
	
					$order_info['shipping_city'] = $columns[$layout['ship-city']];
					$order_info['shipping_state'] = $columns[$layout['ship-state']];
					$order_info['shipping_zip'] = $columns[$layout['ship-postal-code']];
					$order_info['shipping_country'] = $columns[$layout['ship-country']];
					$order_info['shipping_phone'] = $columns[$layout['ship-phone-number']];
	
					//Need to see if I have the item
					$item['sku'] = $columns[$layout['sku']];
					$item['name'] = $columns[$layout['product-name']];
					$item['qty'] = $columns[$layout['quantity-shipped']];
					$item['line_total'] = $columns[$layout['item-price']];
					$item['tax'] = $columns[$layout['item-tax']];
	
					//echo"Item qty = " . $item['qty'] . "\r\n";
	
					$item['order_item_id'] = $columns[$layout['amazon-order-item-id']];
					//echo "here with order item id " . $item['order_item_id'];
	
					//$shipping['tax'] = $colums[$layout['shipping-tax']];
	
					$order_info['current_status_id'] = $CFG->shipped_order_status_id;
	
					$o = (object) array();
					$o->amazon_order_id = $amazon_order_id;
					$current_order = Orders::get1ByO( $o );
	
					if( !$current_order )
					{
						//Only set imported from and date added when there is no current order
						$order_info['imported_from'] = $report['filename'];
						$order_info['date_added'] = date( 'Y-m-d G:i:s' );
	
						$order_id = Orders::insert( $order_info );
						/*if ($order_info['shipping_state'] == "NY")
						{
							$to = 'rivky@tigerchef.com';
							mail($to, "NY Amazon Order Imported", "Order $order_id was imported with a ship-to state of NY", "From: $CFG->company_name <$CFG->orders_email>");
						}*/
						$current_order = Orders::get1( $order_id );
					}
	
				if( $current_order )
				{
					//No reason to go over this, if it was sent in a different file it was completed in that file
					if( $report['filename'] != $current_order['imported_from'] )
						continue;
	
					//echo"Processing Order: ". $current_order['id'] ."\r\n";
					$o_items = Orders::getItems(0, $current_order['id'] );
	
					if( $o_items )
					{
						foreach( $o_items as $o_item )
						{
							//echo"Checking For Existing Item: " . $item['sku'] . " \r\n";
	
							//If you are in the initial import file, don't skip duplicate lines
							//One of the newer amazon import files had 2 lines for 1 item
	
							$product = Products::get1( $o_item['product_id'] );
							if( strtoupper($product['vendor_sku']) == strtoupper($item['sku']) && $report['filename'] != $current_order['imported_from'] )
							{
								//  echo 'Existing Item Found with '.$report['filename'] .', '.$current_order['imported_from'].'\r\n';
								continue 2;
							}
						}
						//If I get here the item wasn't added												
						$product = Products::get1ByModel( $item['sku'], true );
						if( !$product )
						{
							$product = Products::get1ByModel( $item['sku'], false );
						}
						if( !$product )
						{
							$product = Products::get1ByMfrPartNum( $item['sku'], true );
						}
						if( !$product )
						{
							$product = Products::get1ByMfrPartNum( $item['sku'], false );
						}
						$order_info['tax_rate'] = $item['tax'];
						$order_info['shipping'] = $columns[$layout['shipping-price']];
	
						//Need to add shipping and tax from the item that is being added.
						$new_shipping = $order_info['shipping'] + $current_order['shipping'];
						$new_tax = $order_info['tax_rate'] + $current_order['tax_rate'];
						$o_update['tax_rate'] = $new_tax;
						$o_update['shipping'] = $new_shipping;
	
						Orders::update( $current_order['id'], $o_update );
	
						if( $product )
						{
							if( $product['warning'] )
							{
								Orders::updateWarning( $current_order['id'], $product['warning'] );
							}
	
							if( $product['name'] != $item['name'] )
							{
								$warning_msg = 'Possible Item Mismatch for ' . $item['sku'] . ':<br/>Amz Name: ' . $item['name'] . '<br/>Our Name: ' . $product['name'] . '<br />';
								Orders::updateWarning( $current_order['id'], $warning_msg);
							}
							self::addFBAProductToOrder( $item, $product, $current_order['id']);
						}
						else
						{
							self::addMissingAmazonProduct( $item, $current_order['id'],true,$orig_sku );
						}
					}
					else {
						$order_info['tax_rate'] = $item['tax'];
						$order_info['shipping'] = $columns[$layout['shipping-price']];
	
						//Need to add shipping and tax from the item that is being added.
						//			    $new_shipping = $order_info['shipping'] + $current_order['shipping'];
						//			    $new_tax = $order_info['tax_rate'] + $current_order['tax_rate'];
						$o_update['tax_rate'] = $order_info['tax_rate'];
						$o_update['shipping'] = $order_info['shipping'];
	
						Orders::update( $current_order['id'], $o_update );
	
						$product = Products::get1ByModel( $item['sku'], true );
						if( !$product )
						{
							$product = Products::get1ByModel( $item['sku'], false );
						}
						if( !$product )
						{
							$product = Products::get1ByMfrPartNum( $item['sku'], true );
						}
						if( !$product )
						{
							$product = Products::get1ByMfrPartNum( $item['sku'], false );
						}
						if( $product )
						{
							if( $product['warning'] )
							{
								Orders::updateWarning( $current_order['id'], $product['warning'] );
							}
	
							if( $product['name'] != $item['name'] )
							{
								$warning_msg = 'Possible Item Mismatch for ' . $item['sku'] . ':<br/>Amz Name: ' . $item['name'] . '<br/>Our Name: ' . $product['name'] . '<br />';
								Orders::updateWarning( $current_order['id'], $warning_msg);
							}
							self::addFBAProductToOrder( $item, $product, $current_order['id'] );
						}
						else
						{
							self::addMissingAmazonProduct( $item, $current_order['id'] );
						}
					}
				 			$chargeInfo = array(
			    			'pid'      => 0,
			    			'date'     => date('Y-m-d H:i:s'),
			    			'order_id' => $order_id,
			    			'action'   => 'sale',
			    			'transaction_id' => $order_item_id,
			    			'amount'   => $item['line_total'],
			    			'response_code' => 1,
			    			'response_msg' => 'FBA Amazon Payment',
			    			'notes'    => 'FBA Amazon Payment'
		    );
		    $charge_id = Orders::insertCharge($chargeInfo);
		    Orders::insertOrderChargeBreakdown($charge_id, $item['line_total'] , 0,$item['tax']);	
				}
	
				//		    Orders::recalcOrder( $current_order['id'] );
	
				} //end while
			}
	
			$report_info['is_imported'] = 'Y';
			AmazonReport::update( $id, $report_info );
		}
	
		return false;
	}
	static function addFBAProductToOrder( $item, $product, $order_id )
	{
		global $CFG;
	
		if( $product['is_option'] )
		{
			$order_item['product_id'] = $product['product_id'];
			$option_id = $product['id'];
	
			$option_info['product_option_id'] = $option_id;
			$option_info['value'] = $product['value'];
			$option_info['additional_price'] = $product['additional_price'];
		}
		else {
			$order_item['product_id'] = $product['id'];
			$option_id = 0;
		}
		//Seem to be a lot of duplicates with no order IDs not sure why
		if( !$order_id )
			return false;
	
		$order_item['order_id'] = $order_id;
		 
		if( $item['qty'] )
			$order_item['price'] = $item['line_total'] / $item['qty'];
	
		$order_item['gift_msg'] = $item['gift_msg'];
		$order_item['amazon_gift_wrap'] = $item['amazon_gift_wrap'];
	
		$order_item['order_item_id'] = $item['order_item_id'];
	
		$order_item['qty'] = $item['qty'];
	
		$order = Orders::get1( $order_id );
		$sub_total = $order['subtotal'] + $item['line_total'];
		Orders::update( $order_id, array('subtotal'=>$sub_total));
	
		$order_item['order_item_status_id'] = $CFG->item_drop_ship_status;
		$order_item['delivery_method'] = '';		
	
		$oItem = Orders::insertItem( $order_item );
	
		if( $product['is_option'] )
		{
			$option_info['order_item_id'] = $oItem;
			Orders::insertItemOption( $option_info );
	
			return $oItem;
		}
	}
	static function importAmazonFbaInventoryReport( $id )
	{
		global $CFG;
		$report = self::get1( $id );
	
		$report_dir = $CFG->dirroot . '/edit/amazon_reports/';
		$counter = 0;
		if( $report['is_imported'] == 'N' )
		{
			if (($handle = fopen( $report_dir . $report['filename'] , "r")) !== FALSE)
			{
				$row = 0;
				//echo "Processing FBA Inventory File: " . $report['filename'] . "\r\n";
				while (($data = fgetcsv($handle, 3024, "\t")) !== FALSE)
				{					
					$row++;
					if ($row == 1)
					{	
						if ($data[0] != "sku" || $data[10] != "afn-fulfillable-quantity")
						{
							echo "Sorry--file format is incorrect.  Cannot import this file as the FBA inventory file.";
							echo $data[10]."is 10th element of data";
							print_r($data);
							exit;
						}
						$header_data = array();
						for ($i = 0; $i < 18; $i++)
						{
							$fixed_col_name = str_replace("-", "_", $data[$i]);
							$header_data[$i] = $fixed_col_name; 
						}
						AmazonFbaInventory::empty_table();
						continue;
					}
					for ($i = 0; $i < 18; $i++)
					{
							$this_col_name = $header_data[$i];
							$info[$this_col_name] = $data[$i];
					}				
					
					AmazonFbaInventory::insert($info);
				}
				fclose($handle);
			}
			$report_info['is_imported'] = 'Y';
			AmazonReport::update( $id, $report_info );
		 }
	}
	static function importSettlementReport( $id )
	{		
		global $CFG;
		$comm = $CFG->amazon_commission;
		$epsilon = 0.00001;
		$report = self::get1( $id );

		$report_dir = $CFG->dirroot . '/edit/amazon_reports/';
		
		$counter = 0;
		if( $report['is_imported'] == 'N' )
		{
			$z = new XMLReader;
			$z->open($report_dir . $report['filename']);
			$doc = new DOMDocument;
			$emailBody = "";
			
			// getting the first and only SettlementData node and its children
			while ($z->read() && $z->name !== 'SettlementData');
			$settlement_data_node = simplexml_import_dom($doc->importNode($z->expand(), true));
			$emailBody .= "<br/>Total Deposit: " . $settlement_data_node->TotalAmount;
			$emailBody .= "<br/>Start Date: " . date('Y-m-d H:i:s', strtotime($settlement_data_node->StartDate));
			$emailBody .= "<br/>End Date: " . date('Y-m-d H:i:s', strtotime($settlement_data_node->EndDate));
			$emailBody .= "<br/>Deposit Date: " . date('Y-m-d H:i:s', strtotime($settlement_data_node->DepositDate));
			
			
			// move to the first <Order /> node
			while ($z->read() && $z->name !== 'Order');
			
			// now that we're at the right depth, hop to the next <Order/> until the end of the tree
			$counter = 0;
			while ($z->name === 'Order')
			{
    			$node = simplexml_import_dom($doc->importNode($z->expand(), true));
    			
    			$o = new Object();
    			$shipping_charged = $item_price_charged = $expected_item_commission = $expected_shipping_commission = 0;
    			$errors = false;
    			
    			$fulfillment_network = $node->Fulfillment->MerchantFulfillmentID;
    			    			
    			if ($fulfillment_network == "MFN")
    			{
    				foreach ($node->Fulfillment->Item as $this_item)
    				{
    					$o->order_item_id = $this_item->AmazonOrderItemCode;
    					$sku = $this_item->SKU;
    					$item_in_db = Orders::get1OrderItemByO($o);
    					
    					//print_ar($item_in_db);

    					if ($item_in_db)
    					{
    						if ($item_in_db['qty'] != $this_item->Quantity) 
    						{
    							if (!$errors) 
    							{
    								$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    								$errors = true;
    							}
    							$emailBody .= "<br/>Qty in file " . $this_item->Quantity . " does not match qty in database " . $item_in_db['qty'];    					
    						}
    					}
    					else 
    					{
  							if (!$errors) 
   							{
   								$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
   								$errors = true;
   							}
    						$emailBody .= "<br/>**Can't find item in database**";
    					}

    					foreach ($this_item->ItemPrice->Component as $price_component)
    					{
    						if ($price_component->Type == "Principal")
    						{
								$item_price_charged = floatval($price_component->Amount);
    							if ($item_in_db)
    							{
    								if (floatval($item_in_db['qty'] * $item_in_db['price']) != $item_price_charged && abs(floatval($item_in_db['qty'] * $item_in_db['price']) - $item_price_charged) > $epsilon) 
    								{
    									if (!$errors) 
    									{
    										$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    										$errors = true;
    									}
    									$emailBody .= "<br/>Price in file " . $item_price_charged . " does not match price in database " . ($item_in_db['price'] * $item_in_db['qty']);
    								}
    							}
    						}
    						else if ($price_component->Type == "Shipping")
    						{
    							$shipping_charged = floatval($price_component->Amount);
    						}
    						else if ($price_component->Type != "GiftWrap")
    						{
    						    if (!$errors) 
    							{
    								$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    								$errors = true;
    							}
    							$emailBody .= "<br/>Unknown ItemPrice Component Type " . $price_component->Type;  
    						}
    					}

    					foreach ($this_item->ItemFees->Fee as $fee)
    					{
    						$fee_charged = floatval($fee->Amount);
    						if ($fee->Type == "ShippingHB")
    						{
    							$expected_shipping_commission_1 = round(($shipping_charged * $comm * -1), 2);
    							$cents = ceil($shipping_charged * $comm * 100);
    							$cents *= -1;
    							$expected_shipping_commission_2 = round(($cents/100), 2);
    							
    							if ($fee_charged != $expected_shipping_commission_1 && $fee_charged != $expected_shipping_commission_2 && abs($fee_charged - $expected_shipping_commission_1) > .05)
    							{
    								if (!$errors) 
    								{
    									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    									$errors = true;
    								}
    								$emailBody .= "<br/>Expected shipping commission was $expected_shipping_commission_1  or $expected_shipping_commission_2, but " . $fee_charged . " was charged (" .
    									abs(round($fee_charged / $shipping_charged * 100)) . "%)";
    							}
    						}
    					    else if ($fee->Type == "Commission")
    						{
    							$expected_item_commission = round(($item_price_charged * $comm * -1), 2);
    							if ($fee->Amount != $expected_item_commission)
    							{
    							    if (!$errors) 
    								{
    									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    									$errors = true;
    								}
    								$rate = abs(round($fee_charged / $item_price_charged * 100));
    								$emailBody .= "<br/>Expected commission was $expected_item_commission, but " . $fee->Amount . " was charged (".
    								    $rate . "%)";
    								
    								//Save fee in amazon_commission_history
    								if($item_in_db){
    									$result = AmazonCommissionHistory::get($item_in_db['id']);
    									if($result){
    										AmazonCommissionHistory::update($item_in_db['id'], array('commission_paid' => ($fee->Amount + $result['commission_paid']), 'expected_commission' => ($expected_item_commission + $result['expected_commission'])));
    									}
    									else {
    										AmazonCommissionHistory::insert(array('order_item_id' => $item_in_db['id'], 'commission_paid' => $fee->Amount, 'expected_commission' => $expected_item_commission, 'commission_rate' => $rate));
    									}
    								}
    							}
    						}
							else 
							{
								if (!$errors) 
    							{
    								$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    								$errors = true;
    							}
								$emailBody .= "<br/>Unknown Fee Type" . $fee->Type;
							}
    					}
    				}	
    			}
    			else if ($fulfillment_network == "AFN")
    			{
    				$expected_item_commission = 0;
    				$commission_charged = 0;
    				$total_item_price_charged = 0;
    				
    				foreach ($node->Fulfillment->Item as $this_item)
    				{
    					$o->order_item_id = $this_item->AmazonOrderItemCode;
    					$sku = $this_item->SKU;    					
    					$item_in_db = Orders::get1OrderItemByO($o);

    					if ($item_in_db)
    					{
							$product_details = Products::get1($item_in_db['product_id']);
    						if ($item_in_db['qty'] != $this_item->Quantity) 
    						{
    						  	if (!$errors) 
    							{
    								$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    								$errors = true;
    							}
    							$emailBody .= "<br/>Qty in file " . $this_item->Quantity . " does not match qty in database " . $item_in_db['qty'];    					
    						}
    					}
    					else 
    					{
    					    if (!$errors) 
    						{
    							$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    							$errors = true;
    						}
    						$emailBody .= "<br/>**Can't find item in database**";
    					}

    					foreach ($this_item->ItemPrice->Component as $price_component)
    					{
    						if ($price_component->Type == "Principal")
    						{    							
								$item_price_charged = floatval($price_component->Amount);
								$total_item_price_charged += $item_price_charged;
								
								$expected_item_commission += round(($item_price_charged * $comm * -1), 2);
    							if ($item_in_db)
    							{
    								if (floatval($item_in_db['qty'] * $item_in_db['price']) != $item_price_charged && abs(floatval($item_in_db['qty'] * $item_in_db['price']) - $item_price_charged) > $epsilon) 
    								{
    								    if (!$errors) 
    									{
    										$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    										$errors = true;
    									}
    									$emailBody .= "<br/>Price in file " . $item_price_charged . " does not match price in database " . ($item_in_db['price'] * $item_in_db['qty']);
    								}
    							}
    						}
    						else if ($price_component->Type == "Shipping" || $price_component->Type == "GiftWrap")
    						{
    							// ignore shipping and giftwrap for FBA
    						}
    						else 
    						{
    							if (!$errors) 
								{
									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
									$errors = true;
								}
    							$emailBody .= "<br/>Unknown ItemPrice Component Type " . $price_component->Type;  
    						}
    					}

    					foreach ($this_item->ItemFees->Fee as $fee)
    					{
    						$fee_charged = floatval($fee->Amount);
							if ($fee->Type == "Commission")
    						{    							
    							$commission_charged += $fee_charged;    							
    						}
    						else if ($fee->Type == "FBAWeightBasedFee")
    						{
    							$weight_based_fee = $fee->Amount;
    							$expected_weight_based_fee = ($product_details['fba_weight_fee'] * -1);
								if ($weight_based_fee != $expected_weight_based_fee && abs($weight_based_fee - $expected_weight_based_fee) > .05)
    							{
    							    if (!$errors) 
    								{
    									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    									$errors = true;
    								}
    								$emailBody .= "<br/>Expected weight-based fee was $expected_weight_based_fee, but " . $weight_based_fee . " was charged";
    							}
    						}
    						else if ($fee->Type == "FBAPerUnitFulfillmentFee")
    						{
    							$per_unit_fulfillment_fee = $fee->Amount;
    							$expected_per_unit_fulfillment_fee = ($product_details['fba_pick_and_pack_fee'] * -1 * $this_item->Quantity);
								if ($per_unit_fulfillment_fee != $expected_per_unit_fulfillment_fee && abs($per_unit_fulfillment_fee - $expected_per_unit_fulfillment_fee) > .05)
    							{
    							    if (!$errors) 
    								{
    									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    									$errors = true;
    								}
    								$emailBody .= "<br/>Expected per-unit fulfillment fee was $expected_per_unit_fulfillment_fee, but " . $per_unit_fulfillment_fee . " was charged";
    							}
    						}
    						else if ($fee->Type == "FBAPerOrderFulfillmentFee")
    						{
    							$per_order_fulfillment_fee = $fee->Amount;
    							$expected_per_order_fulfillment_fee = ($product_details['fba_order_handling_fee'] * -1);
								if ($per_order_fulfillment_fee != $expected_per_order_fulfillment_fee && abs($per_order_fulfillment_fee - $expected_per_order_fulfillment_fee) > .05)
    							{
    							    if (!$errors) 
    								{
    									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    									$errors = true;
    								}
    								$emailBody .= "<br/>Expected per-order fulfillment fee was $expected_per_order_fulfillment_fee, but " . $per_order_fulfillment_fee . " was charged";
    							}
    						}
							else if ($fee->Type != "ShippingChargeback" && $fee->Type != "GiftwrapChargeback")
							{
								if (!$errors) 
								{
									$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
									$errors = true;
								}
								$emailBody .= "<br/>Unknown Fee Type" . $fee->Type;
							}
    					}
    				}	
    				if ($commission_charged != $expected_item_commission && abs($commission_charged - $expected_item_commission) > .05)
    				{
    					$actual_commission_percentage = abs(round($commission_charged / $total_item_price_charged * 100)); 
    				    if (!$errors) 
    					{
    						$emailBody .= "\n<br/><br/><b>".$node->AmazonOrderID . "($fulfillment_network) -- $sku</b>";
    						$errors = true; 
    					}
    					
//     					echo "<br>". $node->AmazonOrderID. ' order_item_code: '. $node->Fulfillment->Item->AmazonOrderItemCode . ' order_itme_id: ' . $item_in_db['id'];
//     					print_ar($item_in_db);
    					    					
    					$emailBody .= "<br/>Expected commission was $expected_item_commission, but " . $commission_charged . " was charged (".$actual_commission_percentage."%)";
    				
    					//Save commission in amazon_commission_history
    					if($item_in_db){
    						$result = db_get1(AmazonCommissionHistory::get($item_in_db['id']));
    						if($result){
    							AmazonCommissionHistory::update($item_in_db['id'], array('commission_paid' => $commission_charged + $result['commission_paid'], 'expected_commission' => $expected_item_commission + $result['expected_commission']));
    						}else{
    							AmazonCommissionHistory::insert(array('order_item_id' => $item_in_db['id'], 'commission_paid' => $commission_charged, 'expected_commission' => $expected_item_commission, 'commission_rate' => $actual_commission_percentage));
    						}
    					}
    				}
    			
    			}    			
    			
    			$z->next('Order');
			}
			//For now, skipping adjustments because no way to match them in our back-end
			$report_info['is_imported'] = 'Y';
			AmazonReport::update( $id, $report_info );
			//echo $emailBody;
			mail("rachel@tigerchef.com", "Amazon Settlement Report Output", $emailBody, "From: webmaster@tigerchef.com\r\nContent-type: text/html; charset=iso-8859-1");
		 }
	}
	static function doLionsdealSkuConversion($sku, $just_return_part_number = false, $just_return_brand_id = false)
	{
		//echo "\nitem sku before:" . $sku;
		if (substr($sku, 0, 7) == "adcraft") 
		{
			$brand_id = 1;
			$sku = str_replace("adcraft", "0001-", $sku);
		}
		
		list($before, $after) = explode('-', $sku, 2);
		
		if ($before == "wi") 
		{
			$brand_id = 21;
			$sku = "0021-". $after;
		}
		elseif ($before == "tg")
		{
			$brand_id = 223;
			$sku = "0223-". $after;
		}
		elseif ($before == "tc") 
		{
			$brand_id = 222;
			$sku = "0222-". $after;
		}
		elseif ($before == "og") 
		{
			$brand_id = 233;
			$sku = "0233-". $after;
		}
		elseif ($before == "ne") 
		{
			$brand_id = 216;
			$sku = "0216-". $after;
		}
		elseif ($before == "get")
		{
			$brand_id = 12;
			$sku = "0012-". $after;
		}
		elseif ($before == "fla") 
		{
			$brand_id = 10;
			$sku = "0010-". $after;
		}
		elseif ($before == "ap") 
		{
			$brand_id = 3;
			$sku = "0003-". $after;
		}
		elseif ($before == "cw") 
		{
			$brand_id = 7;
			$sku = "0007-". $after;
		}
		elseif ($before == "fd") 
		{
			$brand_id = 8;
			$sku = "0008-". $after;
		}
		elseif ($before == "ja") 
		{
			$brand_id = 17;
			$sku = "0017-". $after;
		}
		elseif ($before == "kr") 
		{
			$brand_id = 231;
			$sku = "0231-". $after;
		}
		elseif ($before == "ls")
		{
			$brand_id = 232;
			$sku = "0232-". $after;
		}
		/*{
			$query = "SELECT LPAD( CONVERT(  `products`.`brand_id` , CHAR( 5 ) ) , 4,  '0' ) as prefix  from products, product_suppliers WHERE product_suppliers.brand_id = 232 
			AND product_suppliers.product_id = products.id AND replace(product_suppliers.dist_sku, ' ', '') = '".$after."'";
			$result = db_query_array($query);
			print_r($result);
			if ($result) $prefix = $result[0]['prefix'] . "-";
			else $prefix = "0232-";
						
			$sku = $prefix . $after;
		}*/
	//	echo "\nitem sku after:" . $sku;
		if ($just_return_part_number) return $after;
		else if ($just_return_brand_id) return $brand_id; 						
		else return $sku;
	}
	static function parse_vendor_central_orders_file($full_file_name)
	{			
			global $CFG;
			
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, ",")) !== FALSE) 
    			{
        			$row++;
        			
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format											
						if (trim($data[0]) != "PO" || trim($data[1]) != "Vendor" || trim($data[2]) != "Ship to location")
						{
						//	print_r($data);
								showStatus("Sorry--file format is incorrect.  Cannot import this file as the Vendor Central Orders Feed.");
								break;
						}
						else 
						{
							continue;
						}												
					}
					else 
					{
						if ($data[0] == "") continue;
						unset($order_info);
						$customer_po_number = $data[0];
						$existing_order = Orders::get(0,0,'','',0,'','','','','',0,'',0,'','','','','',$CFG->vendor_central_store_id,'','',0,'','','','','',0,'','','','','','','','','',0,0,0,"",'','','',false, 0,0,'','','','',false,'','','',false,'', $customer_po_number);							 			   		

		    			if ($existing_order) 
		    			{
		    				$the_order_id = $existing_order[0]['id'];
		    				echo "\n<p>Order ".$customer_po_number." already is in the system as our order #" . $the_order_id."\n";
		    			}

		    			else 
		    			{
							$shipping_id = 1;						
							$shipping_address1 = $data[2];
							$order_date = $_REQUEST['order_date'] . " 12:00:00";
							$order_info = array(
			    				'date' => date('Y-m-d H:i:s', strtotime($order_date)), 
			    				'customer_id' => 0,
								'billing_first_name' => "Amazon Vendor Central",
			    				'shipping_first_name' => "Amazon Vendor Central",			    		
			    				'shipping_address1' => $shipping_address1,			    				    		
			    				'is_repeat_order' => 'N',
			    				'current_status_id' => $CFG->shipped_order_status_id,
			    				'order_tax_option' => $CFG->vendor_central_store_id,			    		
			    				'imported_from' => 'Vendor Central - ' . $full_file_name,
			   		 			'date_added' => date('Y-m-d H:i:s'),
			    				'shipping_id' => $shipping_id,
								'customer_po_number'=>$customer_po_number 
		    				 );
			   			  	$the_order_id = Orders::insert($order_info);
			   			  	echo "\n<p> Inserted Order #$the_order_id\n";
		    			}
		    		
		    			$cur_sku = $data[3];
		    			$cur_qty = $data[12];
		    			$cur_unit_price = preg_replace('/[\$,]/', '', $data[16]); 
		    			echo "\n<p>Here with qty $cur_qty of item $cur_sku with unit price $cur_unit_price";
		    			unset($product_id);
		    			unset($product_option_id);
		    			// first see if the sku matches an option that is not deleted and is active
		    			$product_option = ProductOptions::getByVendorSku($cur_sku, true, true);
			    		// 	if there was no such product option, see if it matches one that's not deleted but is not active
			    		if (!$product_option) $product_option = ProductOptions::getByVendorSku($cur_sku, true);
			    		// otherwise, just match it to a sku
		    			if ($product_option) 
		    			{
		    				$product_id = $product_option[0]['product_id'];
		    				$product_option_id = $product_option[0]['product_option_id'];
		    				// get the prod, too
		    				$the_prod = Products::get1($product_id);
		    			}
		    			else 
		    			{
		    				$product_option_id = '';
		    				$the_prod = Products::get1ByModel($cur_sku, true);
		    				if (!$the_prod) $the_prod = Products::get1ByModel($cur_sku, false);
		    				if (!$the_prod) $the_prod = Products::get1ByMfrPartNum($cur_sku, true);
		    				if (!$the_prod) $the_prod = Products::get1ByMfrPartNum($cur_sku, false);
		    				
		    				if (!$the_prod) 
		    				{
		    					mail ("rachel@tigerchef.com", "Vendor Central SKU $cur_sku on order $the_order_id does not exist in our system", "");
		    					continue;		    				 
		    				}
		    			}
		    			echo "\ngot the prod with id " . $the_prod['id'];
						$see_if_this_item_already_on_order = Orders::getOrderItemOption( $the_order_id,$the_prod['id'], $product_option_id);
						if ($see_if_this_item_already_on_order) 
						{
							echo "\n<p>This item is already on the order, going on to the next line...";
							continue;
						}
						else 
						{
		    				$order_item_info = array(
				    			'order_id' => $the_order_id,
				    			'product_id' => $the_prod['id'],
				    			'price' => $cur_unit_price,
				    			'qty' => $cur_qty,
				    			'order_item_status_id' => 1,
			    			);

			    			$oItem = Orders::insertItem($order_item_info);

				    		if($product_option)
				    		{
								$option = Products::getProductOption($product_option_id);
	
								$option_info = array(
						    		'order_item_id' => $oItem,
					    			'product_option_id' => $product_option_id,
					    			'value' => $option['value'],
				    				'additional_price' => $option['additional_price']
								);

								Orders::insertItemOption( $option_info );
			    			}	
							    		
		    		   		$chargeInfo = array(
			    				'pid'      => 0,
			    				'date'     => date('Y-m-d H:i:s'),
			    				'order_id' => $the_order_id,
			    				'action'   => 'sale',
			    				'transaction_id' => $oItem,
			    				'amount'   => ($cur_unit_price * $cur_qty),
			    				'response_code' => 1,
			    				'response_msg' => '',
			    				'notes'    => ''
		    				);
		    				$charge_id = Orders::insertCharge($chargeInfo);
		    				Orders::insertOrderChargeBreakdown($charge_id, ($cur_unit_price * $cur_qty));
		    				Orders::recalcOrder($the_order_id);
						} // end else this item is not alraedy on the order	    				    		
				} // end else we're not in the first row of the file
    		} // end while we're reading one line of the file at a time
		} // end if the file could be opened		    								
	} // end function parse_vendor_central_orders_file 			
	
}

?>