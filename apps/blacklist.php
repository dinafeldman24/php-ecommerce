<?php

class Blacklist{
	
	public static function get($id = 0, $keywords = '', $order = '', $order_asc = '', 
		$limit = 0, $start = 0, $count = false, $email = ''
	){
		
		if ($count)
			$sql = "SELECT COUNT(marketing_blacklist.id) AS total FROM marketing_blacklist ";
		else
			$sql = "SELECT marketing_blacklist.*
					FROM marketing_blacklist ";
			
		$sql .= " WHERE 1 ";
			
		if($id > 0){
			$sql .= " AND marketing_blacklist.id = $id ";
		}
		
		if($email){
			$sql .= " AND marketing_blacklist.email LIKE '$email' ";
		}
		
        if ($keywords && $keywords != '') {
            $fields = Array('marketing_blacklist.email');
            $sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
        }
		
        if(!$count)
			$sql .= " GROUP BY marketing_blacklist.id ";
		
		if($order){
			$sql .= " ORDER BY $order " . ($order_asc ? ' ASC ' : ' DESC ');
		}
		
		if($limit > 0){
			$sql .= db_limit($limit,$start);
		}

		return db_query_array($sql,'',$count);
		
	}
	
	public static function get1($id = 0){
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}
	
	public static function insert($info = ''){
		
		if(!$info)
			return false;
			
		return db_insert('marketing_blacklist', $info, 'date_added');
		
	}
	
	public static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
			
		return db_update('marketing_blacklist',$id,$info);
		
	}
	
	public static function delete($id = 0){
		
		if(!$id)
			return false;
			
		return db_delete('marketing_blacklist', $id);
		
	}

	public static function truncate(){
		return db_query("TRUNCATE table marketing_blacklist");
	}
	
}

?>