<?php

/*****************
*   Class for Product Variation Groups
*
*****************/

class ProductShowcase{
    
    function get($id=0, $name='', $is_active='', $limit=''){
		$sql = "SELECT *";
		$sql .= " FROM product_showcase ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND product_showcase.id = $id ";
		}
		if($name != ''){
			$name = mysql_real_escape_string($name);
			$sql .= " AND `product_showcase`.name = '$name'";
		}
		if($is_active != ''){
			$is_active = mysql_real_escape_string($is_active);
			$sql .= " AND `product_showcase`.is_active = '$is_active'";
		}
		
		$sql .= ' GROUP BY product_showcase.id ';
		if($limit != ''){
			$sql .= " LIMIT $limit";
		}
	    $ret = db_query_array($sql);
		return $ret;
    }
	function get1($id, $is_active='')
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = ProductShowcase::get($id,'',$is_active);
    	return $result[0];
	}
	function update($id, $info, $table='product_showcase'){
		return db_update($table,$id,$info);
    }
	function insert($info, $table='product_showcase'){
		 return db_insert($table ,$info);
    }
	function delete($id=0, $table='product_showcase'){
		return db_delete($table ,$id);
    }
	
	function get_showcase_lines($id){
		$sql = "SELECT product_showcase_lines.*, products.name ";
		$sql .= " FROM product_showcase_lines ";
		$sql .= " LEFT JOIN products on product_showcase_lines.product_id = products.id ";
		$sql .= " WHERE 1 ";
	    $sql .= " AND product_showcase_lines.showcase_id = $id ";
		$ret = db_query_array($sql);
		return $ret;
    }
	function get_showcase_products($id){
		$sql = "SELECT products.* ";
		$sql .= " FROM product_showcase_lines ";
		$sql .= " LEFT JOIN products on product_showcase_lines.product_id = products.id ";
		$sql .= " WHERE 1 ";
	    $sql .= " AND product_showcase_lines.showcase_id = $id ";
	    $sql .= " AND product_showcase_lines.is_active = 'Y' ";
		$ret = db_query_array($sql);
		return $ret;
    }
	
	
}




?>
