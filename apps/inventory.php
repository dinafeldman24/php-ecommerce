<?php

    class Inventory {

	static function insert( $info )
	{
	    return db_insert( 'inventory', $info, 'date_added' ); 

	}

	static function update( $id, $info )
	{
	    return db_update('inventory', $id, $info, 'id', 'date_added' );
	}

	static function delete( $id )
	{
	    return db_delete( 'inventory', $id ); 
	}

	static function get( $id=0, $limit=0, $start=0, $order='', $order_asc='',
			     $product_id=0, $product_option_id=0, $status=0, $store_id=0,
			     $total=false, $upc='', $to_upload='', $brand_id=0, $type='', $for_download_with_stock = false)
	{

	    if( $total ) 
		$sql = " SELECT count( inventory.id ) as total ";
		else if ($for_download_with_stock)
		{
			$sql = " SELECT products.id as product_id, brands.name as brand_name, products.vendor_sku as product_vendor_sku, product_options.vendor_sku as option_vendor_sku,product_options.value,
				 products.name as product_name,inventory.qty, if (product_options.vendor_price > 0, product_options.vendor_price, products.vendor_price) as vendor_price, if(product_options.additional_price > 0, product_options.additional_price, products.price) as product_price, inventory.date_added";		
		}
	    else {
		$sql = " SELECT inventory.*, products.name as product_name, products.vendor_sku as product_vendor_sku, store.name as store_name, store.id as store_id, product_options.value, product_options.vendor_sku as option_vendor_sku, products.id as product_id ";
	    }
	    $sql .= " FROM inventory ";

	    $sql .= " LEFT JOIN products on products.id = inventory.product_id ";
	    $sql .= " LEFT JOIN product_options on product_options.id = inventory.product_option_id ";
	    $sql .= " LEFT JOIN store on store.id = inventory.store_id ";
	    $sql .= " LEFT JOIN brands on brands.id = products.brand_id ";

	    $sql .= " WHERE 1 ";

	    if( $id )
		$sql .= " AND inventory.id = " . (int) $id;
	    if( $product_id )
		$sql .= " AND inventory.product_id = " . (int) $product_id;
	    if( $product_option_id )
		$sql .= " AND inventory.product_option_id = " . (int) $product_option_id;
	    if( $status )
		$sql .= " AND inventory.status = " . (int) $status;
	    if( $store_id )
		$sql .= " AND inventory.store_id = " . (int) $store_id;
        if ($upc != '')
        $sql .= " AND inventory.upc = " . sprintf("'%s'", mysql_real_escape_string($upc));

	    if( $to_upload )
		$sql .= " AND inventory.to_upload = '" . addslashes($to_upload) . "' ";

	    if( $brand_id )
	    {
		$sql .= " AND brands.id = " . (int) $brand_id;
	    }

	    if( $type )
	    {
		$sql .= " AND inventory.type = '" . addslashes( $type ) . "' ";
	    }
		if ($for_download_with_stock)
		{
			$sql .= " AND inventory.qty > 0";
		}
	    if( !$total )
	    {
		    if( $group_by )
		    {
			    $sql .= " GROUP BY $group_by ORDER BY ";
		    }
		    else
		    {
			    $sql .= " GROUP BY inventory.id
					      ORDER BY ";
		    }

		    if ($order == '') {
			    $sql .= 'inventory.id';
		    }
		    else {
			    $sql .= addslashes($order);
		    }

		    if ($order_asc !== '' && !$order_asc) {
			    $sql .= ' DESC ';
		    }
		
		    if( $limit )
		    {
			    $sql .=" limit $start, $limit ";
		    }
	    }
	    //echo $sql;
	    if( $total )
		return db_query_array( $sql,'', true );
	    else
		return db_query_array( $sql );
	}

	static function get1( $id )
	{
	    global $CFG;

	    $id = (int) $id;
	    if( !$id )
		return false;

	    return db_get1( self::get( $id ) );
	}
	
	static function insertShippedFromInventory($info){
		return db_insert('inventory_shipped_from', $info);
	}
	
	static function getShippedFromInventory($id = 0, $order_id = 0, $product_id = 0, $product_option_id = 0, $qty_to_remove = 0, $start_date = '', $end_date = ''){
	
		$sql = " SELECT inventory_shipped_from.*, products.name as product_name, products.vendor_sku as vendor_sku FROM inventory_shipped_from";

		$sql .= "LEFT JOIN products on products.id = inventory_shipped_from.product_id ";

	    $sql .= " WHERE 1 ";
	    if( $id )
			$sql .= " AND inventory_shipped_from.id = " . (int) $id;
	    if( $order_id )
			$sql .= " AND inventory_shipped_from.order_id = " . (int)$order_id;
	    if( $product_id )
	    	$sql .= " AND inventory_shipped_from.product_id = " . (int)$product_id;
	    if( $product_option_id )
	    	$sql .= " AND inventory_shipped_from.product_option_id = " . (int)$product_option_id;
	    if( $qty_to_remove)
	    	$sql .= " AND inventory_shipped_from.qty_to_remove = " . (int)$qty_to_remove;
	    if( $start_date != '' || $end_date != ''){
	    	if($start_date){
	    		$start_date .= " 00:00:00";
	    	}
	    	if($end_date){
	    		$end_date .= " 23:59:59";
	    	}
	    	$sql .= db_queryrange('inventory_shipped_from.date_removed', $start_date, $end_date);
	    }
	    
	    $sql .= ' ORDER by date_added ';

	    return db_query_array( $sql );
	}
	/**
	 * Get By Object Parameters
	 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
	 */
	static function getShippedFromInventoryByO($o=""){	
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
	
		$select_ = " SELECT inventory_shipped_from.*, products.vendor_sku as vendor_sku , products.name as product_name, product_options.value as option_value, product_options.vendor_sku as option_vendor_sku";

		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(inventory_shipped_from.id)) as total ";
		}
	
		$join_ .= " LEFT JOIN products on products.id = inventory_shipped_from.product_id ";
		
		$join_ .= " LEFT JOIN product_options on product_options.id = inventory_shipped_from.product_option_id ";
		
		$from_ = " FROM inventory_shipped_from ";
	
		$where_ = " WHERE 1 ";
	
		if(isset($o->id)){
			$where_ .= " AND inventory_shipped_from.id = [id] ";
		}
	
		if( isset( $o->product_id )  ){
			$where_ .= " AND `inventory_shipped_from`.product_id = [product_id]";
		}
		
		if( isset( $o->product_option_id )  ){
			$where_ .= " AND `inventory_shipped_from`.product_option_id = [product_option_id]";
		}
		
		if( isset( $o->qty_to_remove )  ){
			$where_ .= " AND `inventory_shipped_from`.qty = [qty_to_remove]";
		}
		
		if( isset( $o->order_id )  ){
			$where_ .= " AND `inventory_shipped_from`.order_id = [order_id]";
		}
		if( isset( $o->type )  ){
			$where_ .= " AND `inventory_shipped_from`.type = [type]";
		}
		if(isset($o->vendor_sku)){
			$where_ .= " AND (products.vendor_sku = [vendor_sku] ";
			$where_ .= " OR product_options.vendor_sku = [vendor_sku]) ";
		}
		if(isset($o->start_date) || isset($o->end_date)){
			if($o->start_date){
				$o->start_date .= " 00:00:00";
			}
			if($o->end_date){
				$o->end_date .= " 23:59:59";
			}
			$where_ .= db_queryrange('inventory_shipped_from.date_removed',$o->start_date,$o->end_date);
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY inventory_shipped_from.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}

			$groupby_ = " GROUP BY inventory_shipped_from.id ";
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);
		
		if($o->total){
			return (int)$result[0]['total'];
		}
	
		return $result;
	}

	static function insertMiss( $info )
	{
	    return db_insert( 'inventory_miss', $info );
	}

	static function deleteMiss( $id )
	{
	    return db_delete( 'inventory_miss', $id );
	}

	static function updateMiss($id, $info)
	{
	    return db_update('inventory_miss', $id, $info);
	}

	static function getMissing( $id=0, $upc='', $query='' )
	{
	    $sql = " SELECT inventory_miss.*, store.name as store_name FROM inventory_miss ";
	    $sql .= "LEFT JOIN store on store.id = inventory_miss.store_id ";

	    $sql .= " WHERE 1 ";
	    if( $id )
		$sql .= " AND inventory_miss.id = " . (int) $id;
	    if( $upc )
		$sql .= " AND inventory_miss.upc = '" . addslashes( $upc ) . "' ";

		if($query){
			$fields = Array('inventory_miss.upc','inventory_miss.vendor_sku','inventory_miss.brand','inventory_miss.description');
			$sql .= " AND " . db_split_keywords($query,$fields,'AND',true);
		}

	    $sql .= ' ORDER by id ';

	    return db_query_array( $sql );
	}

	static function get1Miss( $id )
	{
	    $id = (int) $id;
	    if( !$id )
		return false;

	    return db_get1( self::getMissing( $id ) );
	}

	static function getAmazonUpload( ){
	    global $CFG;

	    $inventory = Inventory::get(0,0,0,'','',0,0,0,$CFG->amazon_store_id,false,'','Y');


	    return $inventory;
	}

		static function canSplit( $id )
		{
			global $CFG;

			$inv = self::get1( $id );
			$store = Store::get1( $inv['store_id'] );

			if( $store['type'] == 'amazon' || $store['type'] == 'ebay' )
				return false;

			return true;
		}

		static function deduct( $id, $deduct )
		{
		    global $CFG;

		    $inv = Inventory::get1( $id );

		    $qty = $inv['qty'] - $deduct;

		    $info['qty'] = $qty;

		    return Inventory::update( $id, $info );
		}




    /**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = " SELECT inventory.*, products.name as product_name, products.vendor_sku as product_vendor_sku, store.name as store_name, store.id as store_id, product_options.value, products.id as product_id, admin_users.username, products.price as product_price, products.vendor_price as product_vendor_price, product_options.additional_price as product_option_price, product_options.vendor_price as product_option_vendor_price ";

		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(inventory.id)) as total ";
		}

		$join_ .= " LEFT JOIN products on products.id = inventory.product_id ";
		$join_ .= " LEFT JOIN product_options on product_options.id = inventory.product_option_id ";
		$join_ .= " LEFT JOIN store on store.id = inventory.store_id ";
		$join_ .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$join_ .= " LEFT JOIN admin_users on inventory.added_by = admin_users.id ";
		$from_ = " FROM inventory ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND inventory.id = [id] ";
		}

		if($o->keywords){
		    $fields = Array('inventory.product_id','store.name','products.upc','product_options.upc','products.name','brands.name','products.vendor_sku');
		    $where_ .= " AND " . db_split_keywords($o->keywords,$fields,'AND',true);
		}

		if( isset( $o->product_id )  ){
			$where_ .= " AND `inventory`.product_id = [product_id]";
		}
		if( isset( $o->product_option_id )  ){
			$where_ .= " AND `inventory`.product_option_id = [product_option_id]";
		}
		if( isset( $o->status )  ){
			$where_ .= " AND `inventory`.status = [status]";
		}
		if( isset( $o->qty )  ){
			$where_ .= " AND `inventory`.qty = [qty]";
		}
		if( isset( $o->store_id )  ){
			$where_ .= " AND `inventory`.store_id = [store_id]";
		}
		if( isset( $o->upc )  ){
			$where_ .= " AND `inventory`.upc = [upc]";
		}
		if( isset( $o->brand )  ){
			$where_ .= " AND `inventory`.brand = [brand]";
		}
		if( isset( $o->description )  ){
			$where_ .= " AND `inventory`.description = [description]";
		}
		if( isset( $o->store_name )  ){
			$where_ .= " AND `inventory`.store_name = [store_name]";
		}
		if( isset( $o->vendor_sku )  ){
			$where_ .= " AND `inventory`.vendor_sku = [vendor_sku]";
		}
		if( isset( $o->to_upload )  ){
			$where_ .= " AND `inventory`.to_upload = [to_upload]";
		}
		if(is_array($o->uploaded_date)){
			if($o->uploaded_date['start']){
				$o->uploaded_date_start = $o->uploaded_date['start'];
				$where_ .= " AND `inventory`.uploaded_date >= [uploaded_date_start]";
			}
			if($o->uploaded_date['end']){
				$o->uploaded_date_end = $o->uploaded_date['end'];
				$where_ .= " AND `inventory`.uploaded_date <= [uploaded_date_end]";
			}
		} else 		if( isset( $o->uploaded_date )  ){
			$where_ .= " AND `inventory`.uploaded_date = [uploaded_date]";
		}
		if( isset( $o->uploaded_by )  ){
			$where_ .= " AND `inventory`.uploaded_by = [uploaded_by]";
		}
		if( isset( $o->type )  ){
			$where_ .= " AND `inventory`.type = [type]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY inventory.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function addOnOrder( $id, $qty )
	{
	    global $CFG;

	    $inv = Inventory::get1( $id );

	    $on_order = $inv['on_order'];
	    $info['on_order'] = $on_order + $qty;

	    return Inventory::update( $id, $info );
	}
	
	static function getStockForAllListings($vendor_sku, $option_sku_main, $store_id, $pk='')
	{
		$sql = "SELECT id FROM products WHERE (vendor_sku LIKE '" . $vendor_sku . "@%' 
												OR vendor_sku = '$vendor_sku')";
// 		if($pk) {
// 			$sql .= " AND SUBSTRING_INDEX( vendor_sku,  '@', -1 ) = '$pk' ";
// 		} else {
// 			$sql .= " AND SUBSTRING( products.vendor_sku, -2 ) <>  'PK' ";
// 		}	

		if($pk) {
			$sql .= " AND vendor_sku REGEXP '$pk'";
		} else { 
			$sql .= " AND products.vendor_sku REGEXP '@[[:digit:]]+PK' = 0 ";
		}
		
		//echo $sql;
		$get_related_products = db_query_array($sql);
		$total_qty = 0;		
		if ($get_related_products)
		{
			foreach ($get_related_products as $related_prod)
			{
			    //echo "<br>related product: ";
			    //var_dump($related_prod);
				$no_option = false;
				if ($option_sku_main != "")
				{
					$get_related_options = db_query_array("SELECT id FROM product_options WHERE product_id = " . $related_prod['id'] . " AND
														(vendor_sku = '$option_sku_main' OR vendor_sku LIKE '".$option_sku_main . "@%')");
					if ($get_related_options) $option_id_to_use = $get_related_options[0]['id'];
					else $no_option = true;
					//echo "related option: $option_id_to_use <br>";
				}
				else $option_id_to_use = 0;
				
				if (!$no_option)
				{
					//echo "here with product " . $related_prod['id'] . " and option $option_id_to_use<br/>";
					
					$get_inventory = db_query_array("SELECT inventory.qty FROM inventory WHERE store_id = $store_id 
						AND product_option_id = $option_id_to_use 
						AND product_id = ". $related_prod['id']);					
					if ($get_inventory) $total_qty += $get_inventory[0]['qty'];
					//echo $total_qty . " is now total_qtyr<br/>";
					//echo "option_id = ". $option_id_to_use .", product_id =  ". $related_prod['id'];
				}
			}			
		}
		return $total_qty;
				
	}
	
    }

?>
