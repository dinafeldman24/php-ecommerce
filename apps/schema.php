<?

class Schema{
	function getSchema($type,$format='json-ld',$data){
		?>
		<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			<?
			switch ($type){
				case 'product':
					Schema::getProductSchema($type,$format,$data);
					break;
				case 'brand':
					Schema::getBrandSchema($type,$format,$data);
					break;
			}
			?>		
		}
		</script>
		<?
	}
	function getProductSchema($id,$format='json-ld',$data=null){
		?>
		  "@type": "Product",
		  "image": "<?=$data['product']['xlarge_image_url']?>",
		  "name": "<?echo  htmlspecialchars(str_replace("'","",$data['product']['name']), ENT_QUOTES)?>",
		  "url": "<?=Catalog::makeProductLink_($product);?>",
		  "brand": [{<?=Schema::getBrandSchema($data['brand']['id'],$format,$data)?>}],
		  "itemCondition": "NewCondition",
		  "offers":[
		  
		  {
			"@type": "Offer",
			"itemOffered":[{
				"@type":"IndividualProduct",
				"sku":"0016-2400CO-M-HE",
				"color":"Heather Grey"
			}],
			"price":"24.99",
			"priceCurrency":"USD"
			},
			{
			"@type": "Offer",
			"itemOffered":[{
				"@type":"IndividualProduct",
				"sku":"0016-2400CO-6X-HE",
				"color":"Navy"
			}],
			"price":"29.99",
			"priceCurrency":"USD"
		  }
		  
		  ]
		}
	<?
	}
	function getBrandSchema($id,$format='json-ld',$data=null){
		?>
			"@type": "Brand",
			"name": "<?=$data['brand']['name']?>",
			"logo": "<?=$data['brand']['logo_url']?>"
		<?
	}
}
?>