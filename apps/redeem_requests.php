<?php
class Redeem_Requests{

	static function insert($info)
	{
				$max_rewards = Rewards::getAvailableRewards($info['customer_id']);
                
        if($max_rewards - $info['amount'] < 0)
                  return false;
        if (!$info['date']) $info['date']=date('Y-m-d H:i:s');
				$id=db_insert('redeem_requests',$info);
        $amount = - abs($info['amount']);
				
				//insert new reward
				$reward_info = Array
				(
				    'customer_id'  => $info['customer_id'],
				    'points_qty'   => $amount,
				    'action_type'  => 'redeemed',
				    'action'       => 'gift_card',
				    'date'         => date('Y-m-d H:i:s'),
				    'order_id'     => $id,
				    'active'       => 'Y'
				);
	    					
				Rewards::insertReward($reward_info);

	    return $id;
	}

	static function update($id,$info)
	{
	    return db_update('redeem_requests',$id,$info);
	}

	static function delete($id)
	{
	    return db_delete('redeem_requests',$id);
	}

	static function get($id=0,$customer_id='',$gift_id = '', $status='',$order,$order_asc)
	{
		$sql = "SELECT redeem_requests.* , 
		        customers.first_name as customer_fname,
            customers.last_name as customer_lname, 
						reward_gifts.name as gift_card";

		$sql .= " FROM redeem_requests 
		          LEFT JOIN customers ON customers.id = redeem_requests.customer_id 
		          LEFT JOIN reward_gifts ON reward_gifts.id = redeem_requests.gift_id ";

		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND redeem_requests.id = $id ";
		}
		
		if ($customer_id > 0) {
		    $sql .= " AND redeem_requests.customer_id = $customer_id ";
		}
		
		if ($gift_id > 0) {
		    $sql .= " AND redeem_requests.gift_id = $gift_id ";
		}
		
		if ($status) {
		    $sql .= " AND redeem_requests.status = '".$status."'";
		}

	  if($order){
    	$sql .= " ORDER BY ";
	        	$sql .= addslashes($order);

		    	if ($order_asc !== '' && !$order_asc) {
		        	$sql .= ' DESC ';
		    	}
		 }			
    
    $ret = db_query_array($sql);

		return $ret;
	}

	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);

        return $result[0];
    }

}