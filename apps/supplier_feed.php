<? 
	// Created by RSunness Nov '11
	class SupplierFeed
	{
		static function import_feed_via_ftp($ftp_server, $ftp_user_name, $ftp_password, $remote_filename, $remote_dir = "", $local_dir = "/tmp", $not_older_than = '', $use_wildcard = false)
		{
			global $real_file_name;
			
			// set up basic connection
			$conn_id = ftp_connect($ftp_server);

			// login with username and password
			$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_password);

			if ($login_result)
			{
				if ($remote_dir != "")
				{
					// then do an ftp_chdir into it
					if (!ftp_chdir($conn_id, $remote_dir)) 
					{
						echo "Could not change directory to $remote_dir";
						return false; 				
					}
				}
				// if using a wildcard to get the file name, find its exact file name first
				if ($use_wildcard)
				{
					$list=ftp_nlist($conn_id, "$remote_filename");
					if (count($list))
					{						
						$newest_timestamp = 0;
						foreach ($list as $list_item)
						{
							$remote_filename = $list_item;
							$this_timestamp = ftp_mdtm($conn_id, $remote_filename);
							if ($this_timestamp > $newest_timestamp)
							{
								$newest_timestamp = $this_timestamp;
								$real_file_name = $remote_filename;
							}
						}
						// if did find one with today's date, also get rid of files older than 5 days old
						$list2=ftp_nlist($conn_id, "*.xml");
						if (count($list2))
						{						
							$earliest_to_keep = strtotime("-7 days");
							foreach ($list2 as $file)
							{
								$this_timestamp = ftp_mdtm($conn_id, $file);
								if ($this_timestamp < $earliest_to_keep)
								{
									ftp_delete($conn_id, $file);
								}
							}
													
						}						
					}
					else 
					{
						echo "No $ftp_server file exists with today's date in the file name.";
						print_r($list);
						return false;
					}												
				}
				// if passed in parameter not to download a file older than a certain time, check that the file 
				// on the server now is not older than that.  If it is older, do not download it and return false
				if ($not_older_than != '') 			
				{
					// get the last modified time
					$last_mod_time = ftp_mdtm($conn_id, $remote_filename);

					if ($last_mod_time != -1) 
					{
						if ($last_mod_time < $not_older_than)
						{
							echo "$remote_filename was too old to download.";
							return false;							
						}
					}	 
					else { echo "Couldn't get mdtime"; }
				}
					
				// try to download $remote_filename and save to $local_file
				$local_file = $local_dir. "/".$remote_filename;
				if (ftp_get($conn_id, $local_file, $remote_filename, FTP_BINARY)) 
				{
    				//echo "Successfully written to $local_file\n";
    				$return_val = true;    				
				} 
				else 
				{
    				echo "There was a problem--was trying to write $remote_filename to $local_file\n";
    				$return_val = false;
				}

				// close the connection
				ftp_close($conn_id);
				return $return_val;
			}	
			else 
			{
				echo "Couldn't even log into server";
				return false; 
			}
		}
		static function parse_xml_feed_MR($file_name)
		{			
			$destination_table = "product_feed_MR";
			$z = new XMLReader;
			$full_file_name = "/tmp/$file_name";
			
			SupplierFeed::empty_table($destination_table);			
			$z->open($full_file_name);

			$doc = new DOMDocument;

			// move to the first <product /> node
			while ($z->read() && $z->name !== 'Product');

			// now that we're at the right depth, hop to the next <Product/> until the end of the tree
			$counter = 0;
			while ($z->name === 'Product')
			{
    			// either one should work
    			//$node = new SimpleXMLElement($z->readOuterXML());
    			$node = simplexml_import_dom($doc->importNode($z->expand(), true));

    			// now you can use $node without going insane about parsing
    			//var_dump($node->element_1);
    			unset($info);
    			$info['dist_sku'] = $node->DistSKU;
    			$info['cust_sku'] = $node->CustSKU;
    			$info['upc'] = $node->UPC;
    			$info['brand'] = $node->Brand;
    			$info['mfr_model'] = $node->MfrModel;
    			$info['country_of_origin'] = $node->CntryOfOriginISOCode;
    			$info['msrp'] = $node->MSRP;
    			$info['map'] = $node->MAP;
    			$info['prod_name'] = $node->ProdName;
    			$info['hi_lite'] = $node->HiLite;
    			$info['warranty'] = $node->Warranty;
    			$info['prod_ht'] = $node->ProdHt;
    			$info['prod_width'] = $node->ProdWidth;
    			$info['prod_depth'] = $node->ProdDepth;
    			$info['ship_qty'] = $node->ShipQTY;
    			$info['ship_wt'] = $node->ShipWt;
    			$info['ship_ht'] = $node->ShipHt;
    			$info['ship_width'] = $node->ShipWidth;
    			$info['ship_depth'] = $node->ShipDepth;
    			$info['upsable'] = $node->UPSable;
    			$info['ups_dim_wt_dom'] = $node->UPSDimWt_Dom;
    			$info['ups_ovr_sz_flag_dom'] = $node->UPSOvrSzFlag_Dom;
    			$info['ups_ship_alone_flag'] = $node->UPSShipAloneFlag;
    			$info['cat_1_name'] = $node->Cat1Name;
				$info['cat_2_name'] = $node->Cat2Name;
				$info['cat_3_name'] = $node->Cat3Name;
				$info['cat_4_name'] = $node->Cat4Name;
				$info['image_xl'] = $node->ImageXL;
				$info['product_desc'] = $node->ProductDesc;
				$info['image_bi'] = $node->ImageBI;
				$info['image_sl'] = $node->ImageSL;
				    						
				$counter++;
				SupplierFeed::insert_row_into_table($destination_table, $info);				
			    // go to next <Product />
    			$z->next('Product');
			}
			
		} 	
		static function insert_row_into_table($destination_table, $info)
		{
			return db_insert($destination_table,$info);
		}
		static function update_row_in_table($destination_table, $id, $info)
		{
			return db_update($destination_table,$id,$info,"dist_sku");
		}		
		static function empty_table($destination_table)
		{
			return db_query("TRUNCATE TABLE $destination_table");
		}
		static function get_dist_sku_row($destination_table, $dist_sku)
		{
			$result = db_query_array("SELECT * from $destination_table WHERE dist_sku = '".$dist_sku."'");
			return $result[0];
		}
		static function getDataForAmzInvUpdates($vendor_prod_table, $vendor_prod_table_sku_field, $supplier_id)
		{
			if ($vendor_prod_table == "product_feed_WS") $sql = "SELECT product_id, if(vt.eta_date = '0000-00-00', qty_avail, 0) as qty ";
			else $sql = "SELECT product_id, qty_avail as qty ";
			$sql .=" FROM (products p, product_suppliers ps)
					LEFT JOIN $vendor_prod_table vt ON 
					ps.dist_sku = vt.$vendor_prod_table_sku_field
					WHERE p.id = ps.product_id 
					AND ps.brand_id = $supplier_id and is_deleted = 'N' AND p.vendor_sku NOT LIKE '%FBA'";		
			return db_query_array($sql);
		}
		static function get_P_and_A_updates_MR($destination_table)
		{
			global $CFG;
			
			$username = "TigerChef";
			$password = "TC2012!rc";
			$url = 'http://www.onlinesalescentral.com/httpservice/HttpSvc.ashx';
			$body = "UserID=$username&Password=$password&HttpSvcName=PriceAvail";
			$c = curl_init ($url);
			curl_setopt ($c, CURLOPT_POST, true);
			curl_setopt ($c, CURLOPT_POSTFIELDS, $body);
			curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
			$page = curl_exec ($c);
			curl_close ($c);			
			$filename = "/tmp/MR_PA_update.xml";
			$fp = fopen($filename, 'w');
			fwrite($fp, $page);
			fclose($fp);
			$z = new XMLReader;
			$z->open($filename);
			$doc = new DOMDocument;

			// move to the first <Product /> node
			while ($z->read() && $z->name !== 'Product');

			// now that we're at the right depth, hop to the next <Product/> until the end of the tree
			$counter = 0;
			while ($z->name === 'Product')
			{
    			// either one should work
    			//$node = new SimpleXMLElement($z->readOuterXML());
    			$node = simplexml_import_dom($doc->importNode($z->expand(), true));

    			// now you can use $node without going insane about parsing
    			//var_dump($node->element_1);
    			unset($info);
    			unset($id);
    			$id = $node->DistSKU;
    			$info['qty_avail'] = $node->QtyAvail;
    			$info['price'] = $node->Price;
    			SupplierFeed::update_row_in_table($destination_table, $id, $info);
    			// also want to update our system with the new price
    			// check if we have the product in our system mapped to M. Rothman
    			// if we do, check old cost and if doesn't match, update cost and price in the products table
    			$mapped_prod_info = ProductSuppliers::get(0, 403, '', '', '', '', '', '', $id);
    			if ($mapped_prod_info)
    			{    				
    				//$new_cost = (float) $info['price'] + 2.75;    				
    				$new_cost = (float) $info['price'];
    				$new_price = round($new_cost * 1.5,2);
    				
    				foreach($mapped_prod_info as $mapped_prod)
    				{    						
    					if (round($new_cost,2) != round((float) $mapped_prod['vendor_price'],2))
    					{    	    		    									    					
    							$info2['vendor_price'] = $new_cost;
    							$info2['price'] = $new_price;
	    						// only update if we don't have it in the warehouse
    							$in_warehouse = Inventory::get(0,0,0,'inventory.qty','',$mapped_prod['product_id'], 0,0,$CFG->default_inventory_location);
	    						$warehouse_record = $in_warehouse[0];
	    						if (!$warehouse_record['qty'] || $warehouse_record['qty'] == 0)
	    						{
	    							echo "\nBased on new cost of $new_cost (".$info['price']."), I'd change old cost " .$mapped_prod_info[0]['vendor_price']. " of $id and change its price to $new_price for product id ".$mapped_prod['the_product_id'];
	    							//print_r($mapped_prod_info);
    								Products::update($mapped_prod['the_product_id'], $info2);
	    						}
	    					//else echo "Not updating cost of " .$id;
    					}   					 
    				}
    			}
				// go to next <Product />
    			$z->next('Product');
			}
			
		}
		static function getCurrentStock($supplier_table_name, $qty_field, $sku_field_name, $sku)
		{
			$get_stock = db_query_array("SELECT $qty_field FROM $supplier_table_name
					WHERE $sku_field_name = '$sku'");
			if ($get_stock) $the_qty = $get_stock[0][$qty_field];
			else $the_qty = 0;				
			
			return $the_qty;
		}
		static function parse_csv_feed_SED($file_name)
		{
			global $CFG;
			$destination_table = "product_feed_SED";			
			$full_file_name = "/tmp/$file_name";				
			
			// we'll read through each row and 
			// if the part number is already in the table, just update the qty and price
			// also need to update the cost (and price, IY"H, once we have prices...) in the products table
			// if it's not yet in the table, insert it
			// When finished, change qty to 0 where timestamp is older than 36 hours old
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, ",")) !== FALSE) 
    			{
        			$row++;
        			//echo "up to row $row<br>";
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format
						// Part Number	Category	Manufacturer	MFG Part Number	Weight - lbs	Price	Sell Description	Technical Description	Qty On-hand	UPC	eTilize#												
						if ($data[0] != "Part Number" || $data[1] != "Category" || $data[5] != "Price" || $data[8] != "Qty On-hand")
						{							
							showStatus("Sorry--file format is incorrect.  Cannot import this file as the SED parts file.");
							exit;
						}
						else continue;						
					}
					else 
					{
						$info = array();
						$info['dist_sku'] = $data[0]; 
						$info['price'] = $data[5]; 
						$info['qty_avail'] = $data[8];
						$info['last_updated'] = date('Y-m-d H:i:s');
						$record = SupplierFeed::get_dist_sku_row($destination_table, $info['dist_sku']);
						if ($record) 
						{
							SupplierFeed::update_row_in_table($destination_table, $info['dist_sku'], $info);							
						}
						else 
						{
							$info['category'] = $data[1];
							$info['manufacturer'] = $data[2];
							$info['mfg_part_number'] = $data[3];
							$info['weight'] = $data[4];
							$info['description'] = $data[6];
							$info['technical_description'] = $data[7];
							$info['upc'] = $data[9];
							$info['etilize_number'] = $data[10];
							SupplierFeed::insert_row_into_table($destination_table, $info);
						}
						// also want to update our system with the new cost
		    			// check if we have the product in our system mapped to SED
		    			// if we do, check old cost and if doesn't match, update cost and 
		    			// price (once we have a formula for that) in the products table
    					$mapped_prod_info = ProductSuppliers::get(0, 672, '', '', '', '', '', '', '', $info['dist_sku']);
    					if ($mapped_prod_info)
    					{    				   				
    						$new_cost = (float) $info['price'];
    					//	$new_price = round($new_cost * 1.5,2);
    				
    						foreach($mapped_prod_info as $mapped_prod)
    						{    						
    							if (round($new_cost,2) != round((float) $mapped_prod['vendor_price'],2))
    							{    	  
    								$info2 = array();  		    									    					
    								$info2['vendor_price'] = $new_cost;
    								// only update if we don't have it in the warehouse
    								$in_warehouse = Inventory::get(0,0,0,'inventory.qty','',$mapped_prod['product_id'], 0,0,$CFG->default_inventory_location);
	    							$warehouse_record = $in_warehouse[0];
	    							if (!$warehouse_record['qty'] || $warehouse_record['qty'] == 0)
	    							{
	    								echo "\nBased on new cost of $new_cost, I'd change old cost " .$mapped_prod['vendor_price']. " of ". $info['dist_sku'] . "for SED product, product id ".$mapped_prod['the_product_id'];
    									Products::update($mapped_prod['the_product_id'], $info2);
	    							}
		    					}   					 
    						}
    					}						
					}
    			}
    			// Now update all rows where timestamp is > 36 hrs old and make their qty 0
    			db_query("UPDATE product_feed_SED SET qty_avail = 0 WHERE last_updated < DATE_SUB(NOW(), INTERVAL 36 HOUR)");
			}		
		}
		
		static function parse_xml_feed_FF($file_name)
		{			
			$destination_table = "product_feed_FF";
			$z = new XMLReader;
			$full_file_name = "/tmp/$file_name";
			
			SupplierFeed::empty_table($destination_table);			
			$z->open($full_file_name);

			$doc = new DOMDocument;

			// move to the first <LineItem /> node
			while ($z->read() && $z->name !== 'LineItem');

			// now that we're at the right depth, hop to the next <LineItem/> until the end of the tree
			$counter = 0;
			while ($z->name === 'LineItem')
			{
    			$node = simplexml_import_dom($doc->importNode($z->expand(), true));

       			unset($info);
    			$info['dist_sku'] = $node->InventoryLine[0]->VendorPartNumber;
    			$info['upc'] = $node->InventoryLine[0]->UPCCaseCode;
				$info['description'] = $node->ProductOrItemDescription[0]->ProductDescription;
				$info['qty_avail'] = $node->InventoryLineQuantities[0]->InventoryLineQuantity[0]->QuantityLine[0]->Quantity1;
				$info['qty_uom'] = $node->InventoryLineQuantities[0]->InventoryLineQuantity[0]->QuantityLine[0]->QtyUOM;;
				    						
				$counter++;
				SupplierFeed::insert_row_into_table($destination_table, $info);				
			    // go to next <LineItem />
    			$z->next('LineItem');
			}			
		}
		static function parse_csv_feed_FF($full_file_name)
		{			
			$destination_table = "product_feed_FF";
		
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, ",")) !== FALSE) 
    			{
        			$row++;
        			
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format
						// Part Number	Category	Manufacturer	MFG Part Number	Weight - lbs	Price	Sell Description	Technical Description	Qty On-hand	UPC	eTilize#												
						if ($data[0] != "ProductId" || $data[1] != "AvailableQuantity" || $data[2] != "Availability" || $data[3] != "NextAvailable" || $data[4] != "Status" || $data[5] != "OutOfStock" || $data[6] != "Orderable" || $data[7] != "EstArrivalDate")
						{
								showStatus("Sorry--file format is incorrect.  Cannot import this file as the Flash Furniture Feed.");
								break;
						}
						else 
						{
							SupplierFeed::empty_table($destination_table);
							continue;
						}												
					}
					else 
					{
						$info = array();						
												
						$info['dist_sku'] = $data[0]; 
						$info['qty_avail'] = $data[1];												    						
						$info['qty_uom'] = "EA";
						
						SupplierFeed::insert_row_into_table($destination_table, $info);
					}
    			}
			}
			else echo "Can not read file $full_file_name";					
		}
		
		static function parse_feed_WS($file_name) //WESTON
		{
			global $CFG;
			$destination_table = "product_feed_WS";			
			$full_file_name = "/tmp/$file_name";				
			
			// we'll read through each row and 
			// if the part number is already in the table, just update the qty and price
			// also need to update the cost (and price, IY"H, once we have prices...) in the products table
			// if it's not yet in the table, insert it
			// When finished, change qty to 0 where timestamp is older than 36 hours old
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, ",")) !== FALSE) 
    			{
        			$row++;
        			
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format
						// Part Number	Category	Manufacturer	MFG Part Number	Weight - lbs	Price	Sell Description	Technical Description	Qty On-hand	UPC	eTilize#												
						if ($data[0] != "Code" || $data[1] != "Desc" || $data[2] != "ETA_DATE" || $data[3] != "Stock_Available")
						{
								showStatus("Sorry--file format is incorrect.  Cannot import this file as the Weston Feed. Be sure you have the header row from Weston in your CSV file.");
								break;
						}
						else 
						{
							continue;
						}												
					}
					else 
					{
						$info = array();						
												
						if (strtoupper($data[3]) == "YES") $qty_avail = 20;
						else $qty_avail = 0;
						$info['dist_sku'] = $data[0]; 
						$info['desc'] = $data[1]; 
						if ($data[2]) 
						{							
							$info['eta_date'] = date('Y-m-d', strtotime($data[2]));
						}
						$info['last_updated'] = date('Y-m-d H:i:s');
						$info['qty_avail'] = $qty_avail;
						$record = SupplierFeed::get_dist_sku_row($destination_table, $info['dist_sku']);
						if ($record) 
						{
							SupplierFeed::update_row_in_table($destination_table, $info['dist_sku'], $info);							
						}
						else 
						{
							SupplierFeed::insert_row_into_table($destination_table, $info);
						}
						// also want to update our system with ETA date as restock date
		    			// check if we have the product in our system mapped to Weston
    					$mapped_prod_info = ProductSuppliers::get(0, 398, '', '', '', '', '', '', '', $info['dist_sku']);
    					if ($mapped_prod_info)
    					{    				   				    						   				
    						foreach($mapped_prod_info as $mapped_prod)
    						{    						
    							if ($info['eta_date'])
    							{
    								$info2 = array();  		    									    					
    								$info2['restock_date'] = $info['eta_date'];
	    							// only update if we don't have it in the warehouse
    								$in_warehouse = Inventory::get(0,0,0,'inventory.qty','',$mapped_prod['product_id'], 0,0,$CFG->default_inventory_location);
	    							$warehouse_record = $in_warehouse[0];
	    							if (!$warehouse_record['qty'] || $warehouse_record['qty'] == 0)
	    							{
	    								//echo "\nUpdating restock date of ". $info['dist_sku'] . "for Weston product, product id ".$mapped_prod['the_product_id'];
    									Products::update($mapped_prod['the_product_id'], $info2);
	    							}
    							}
		    					   					 
    						}
    					}									
					}
    			}
    			// Now update all rows where timestamp is > 36 hrs old and make their qty 0
    			db_query("UPDATE product_feed_WS SET qty_avail = 0 WHERE last_updated < DATE_SUB(NOW(), INTERVAL 36 HOUR)");
    			echo "File successfully imported.";
			}
			else echo "Could not open file $full_file_name";		
		}
		static function parse_feed_JR($file_name) //Johnson Rose
		{
			global $CFG;
			$destination_table = "product_feed_JR";			
			$full_file_name = "/tmp/$file_name";				
			
			// we'll read through each row and 
			// if the part number is already in the table, just update the qty and price
			// also need to update the cost (and price, IY"H, once we have prices...) in the products table
			// if it's not yet in the table, insert it
			// When finished, change qty to 0 where timestamp is older than 36 hours old
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, "\t")) !== FALSE) 
    			{
        			$row++;
        			
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format											
						if ($data[0] != "Item#" || $data[3] != "EnglishDesc" || $data[15] != "FlyerPrice" || $data[21] != "AvailInventory")
						{print_r($data);
								echo "Sorry--file format is incorrect.  Cannot import this file as the Johnson Rose Feed.";
								break;
						}
						else 
						{
							continue;
						}												
					}
					else 
					{
						$info = array();						

						$qty = (int)$data[21];
						if ($qty <= 5) $qty = 0;						
						$info['qty_avail'] = $qty;
						
						$info['dist_sku'] = trim($data[0], "'"); 
						$info['desc'] = $data[3]; 
						$info['case_qty'] = $data[5];
						$info['length'] = $data[6];
						$info['width'] = $data[7];
						$info['height'] = $data[8];
						$info['case_weight'] = $data[10];
						$info['price'] = $data[15];
						$info['upc'] = trim($data[17],"'");
						$info['last_updated'] = date('Y-m-d H:i:s');
						
						$record = SupplierFeed::get_dist_sku_row($destination_table, $info['dist_sku']);
						if ($record) 
						{
							SupplierFeed::update_row_in_table($destination_table, $info['dist_sku'], $info);							
						}
						else 
						{
							SupplierFeed::insert_row_into_table($destination_table, $info);
						}						    														
					}
    			}
    			// Now update all rows where timestamp is > 36 hrs old and make their qty 0
    			db_query("UPDATE product_feed_JR SET qty_avail = 0 WHERE last_updated < DATE_SUB(NOW(), INTERVAL 36 HOUR)");
    			echo "File successfully imported.";
			}
			else echo "Could not open file $full_file_name";		
		}
	static function parse_feed_CH($file_name) //Chroma
		{
			global $CFG;
			$destination_table = "product_feed_CH";			
			$full_file_name = "/tmp/$file_name";				
			
			// we'll read through each row and 
			// if the part number is already in the table, just update the qty and price
			// also need to update the cost (and price, IY"H, once we have prices...) in the products table
			// if it's not yet in the table, insert it
			// When finished, change qty to 0 where timestamp is older than 36 hours old
			if (($handle = fopen($full_file_name, "r")) !== FALSE) 
			{
				$row = 0;				
    			while (($data = fgetcsv($handle, 3024, ",")) !== FALSE) 
    			{
        			$row++;
        			
					if ($row == 1)
					{
						// check that header is correct, and we're importing a file in the proper format											
						if (trim($data[0]) != "Supplier ID" || trim($data[1]) != "Item Number" || trim($data[2]) != "Item Quantity On Hand")
						{
						//	print_r($data);
								showStatus("Sorry--file format is incorrect.  Cannot import this file as the Chroma Feed.");
								break;
						}
						else 
						{
							continue;
						}												
					}
					else 
					{
						if ($data[1] == "") continue;
						$info = array();						
						$qty = (int)$data[2];						
						if ($qty <= 5) $qty = 0;	
						$info['dist_sku'] = $data[1]; 
						$info['name'] = $data[7]; 
						$info['last_updated'] = date('Y-m-d H:i:s');
						$info['qty_avail'] = $qty;
						$record = SupplierFeed::get_dist_sku_row($destination_table, $info['dist_sku']);
						if ($record) 
						{
							SupplierFeed::update_row_in_table($destination_table, $info['dist_sku'], $info);							
						}
						else 
						{
							SupplierFeed::insert_row_into_table($destination_table, $info);
						}											
					}
    			}
    			// Now update all rows where timestamp is > 36 hrs old and make their qty 0
    			db_query("UPDATE product_feed_CH SET qty_avail = 0 WHERE last_updated < DATE_SUB(NOW(), INTERVAL 36 HOUR)");
    			echo "File successfully imported.";
			}
			else echo "Could not open file $full_file_name";		
		} 	 					
	}
?>