<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   Feb 8 2015
*
*****************/

class ProductOptionsDynamic {
    
    var $COLOR_OPTION_ID = 1;
    var $id = null;
    var $name;
    var $input_type;
    var $user_input;
    var $values;
    var $formula;
    static $input_types = array('Drop Down' => 'select',
        'Radio' => 'radio', 'Checkbox' => 'checkbox', 
        'Text' => 'text', 'Long Text' => 'textarea',
        'File Upload' => 'file' );
    static $user_inputs = array('Text' => 'text', 'Long Text' => 'textarea', 'File Upload' => 'file');
    static $input_labels = array('select' => 'Drop Down',
        'radio' => 'Radio', 'checkbox' => 'Checkbox',
        'text' => 'Text', 'textarea' => 'Long Text',
        'file' => 'File Upload');
    var $delete_error;

    function ProductOptionsDynamic($id=null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    function populate($id){
        $this->id = $id;
        $sql = "SELECT * FROM options WHERE id = $id";
        $this_info = db_query_array($sql);
        if(is_array($this_info)){
            $option = $this_info[0];
            $this->name = $option['name'];
            $this->input_type = $option['input_type'];
            $this->user_input = $option['input_type'];
            $this->formula = $option['formula'];
        }

    }

    /****************
    DB Interactions
    ***************/

    static function getAll(){
        $sql = "SELECT * FROM options";
        return db_query_array($sql);
    
    }

    function updateOrInsert(){
        if(!is_null($this->id)){
            $sql = "UPDATE options SET name = '" . $this->name
                . "', input_type = '" . $this->input_type .
                "', user_input = '" . $this->user_input .
                "', formula = '" . $this->formula .
                "' WHERE id = " . $this->id;
            echo 'the sql query: ' . $sql;
            return db_query($sql);
        }   
        else{
            $info = array('name' => $this->name,
             'input_type' => $this->input_type,
             'user_input' => $this->user_input);
            return db_insert('options', $info);
        }
    }

    function getValues($id = null){
      if(is_null($this->id)){
            if(is_null($id)){
                return false;   
            }
            $this->populate($id);
      }

        $sql = "SELECT id, option_value(options_id, value) as value, image_file FROM 
            option_values WHERE options_id = " . $this->id . " ORDER BY `order`, value ";
            $this->values = db_query_array($sql);
            return $this->values;
    }

    function delete(){
        return db_delete('options', $this->id);
    }

    /***
    Getters and Setters
    ****/

    function setInputType($input_type){
        $this->input_type = $input_type;
    }

    function getInputType(){
        return $this->input_type;
    }

    function setName($name){
        $this->name = $name;
    }

    function getName(){
        return $this->name;
    }

    function setUserInput($user_input){
        $this->user_input = $user_input;
    }

    function getUserInput(){
        return $this->user_input;
    }

    function getFormula(){
        return $this->formula;
    }

    function setFormula($formula){
        $this->formula = $formula;
    }

    /************
    Output Functions
    *************/

    static function showInputType($input_type){
        foreach( ProductOptionsDynamic::$input_types as $key => $val){
            if($val == $input_type){
                return $key;
            }
        }
        return '';
    }

    static function showUserInput($user_input){
        foreach( ProductOptionsDynamic::$user_inputs as $key=>$val){
            if($val == $user_input){
                return $key;
            }
        }
        return '';
    }

    function getInputTypesSelect($selected = ''){
        if(empty($selected)){
            $selected = $this->input_type;
        }
        $str = '<option value="">-Select-</option>';
        foreach(self::$input_types as $key => $val){
            $str .= "<option value='$val'";
            if($val == $selected){
                $str .= ' selected';
            }
            $str .= ">$key</option>";
        }
        return $str;
    }

    function getUserInputSelect($selected = ''){
        if(empty($selected)){
            $selected = $this->user_input;
        }
        $str = '<option value="">-Select-</option>';
        foreach(self::$user_inputs as $key => $val){
            if($val == ''){
                continue;
            }
            $str .= "<option value='$val'";
            if($val == $selected){
                $str .= ' selected';
            }
            $str .= ">$key</option>";
        }
        return $str;
    }

    static function getAllOptionsSelect($select_option = null){
        $all_options = self::getAll();
        $option_str = "<option value=''>Please Select</option>";
        foreach($all_options as $curr){
            $option_str .= '<option value="' . $curr['id'] . '">'
                . $curr['name'] . ' (' . self::$input_labels[$curr['input_type']] . ')'. '</option>';
        }
        return $option_str;
    }

    static function getOptionInputTypes(){
        $all_options = self::getAll();
        $input_types = array();
        foreach($all_options as $curr){
            $input_types[$curr['id']] = $curr['input_type'];
        }
        return $input_types;
    }

    static function getOptionInputTypesForJS($obj_name){
        $input_types = ProductOptionsDynamic::getOptionInputTypes();
        $ret = '';
        foreach($input_types as $key => $val){
            $ret .= "{$obj_name}[{$key}] = '{$val}';";
        }
        //HARD CODING COLOR
        $ret .= "{$obj_name}[1] = 'color';";
        return $ret;
    }
}




?>
