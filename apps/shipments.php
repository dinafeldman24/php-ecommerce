<?
/* Contains both classes Shipments and ShipmentItems */ 
class Shipments
{
	static function insert($info)
	{
		if(!$info) return false;
		db_insert('shipments', $info);
		$id = db_insert_id();

		return $id;		
	}
	static function update($id,$info)
	{
		return db_update('shipments',$id,$info, 'shipment_id');
	}
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('shipments', $id);
	}
	
	static function getByOrderNum($order_id)
	{
		$sql = "SELECT * FROM shipments where order_id = " . $order_id;
		$result = db_query_array($sql);
		return $result;		
	}
	static function getMaxIndexForOrderNum($order_id)
	{
		$sql = "SELECT MAX(counter) AS max_counter FROM shipments WHERE order_id = $order_id";
		$result = db_query_array($sql);
		if ($result && $result[0]['max_counter'] && $result[0]['max_counter'] != '') return $result[0]['max_counter'];
		else return 0;
	}	
	static function get($shipment_id='')
	{
		$query = "SELECT shipments.* FROM shipments WHERE 1";
		if ($shipment_id != '') $query .= " AND shipments.shipment_id = '" . $shipment_id . "'";
		
		return db_query_array($query);
	}
	static function get1($shipment_id)
	{
		$result = self::get($shipment_id);
		return $result[0];		
	}
}

class ShipmentItems
{
	static function insert($info)
	{
		if(!$info) return false;
		db_insert('shipment_items', $info);
		$id = db_insert_id();

		return $id;		
	}
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('shipment_items', $id);
	}	
	static function getByShipmentID($shipment_id)
	{
		$sql = "SELECT * FROM shipment_items where shipment_id = '$shipment_id'";
		$result = db_query_array($sql);
		return $result;		
	}
}
?>