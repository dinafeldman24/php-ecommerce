<?

class PurchaseOrder{

	static function insert($info){
		return db_insert("purchase_order",$info,'date_added');
	}

	static function update($id,$info){
		return db_update("purchase_order",$id,$info);
	}
	
	static function cancel($id)
	{		
		global $CFG;
		
		if(!$id){
			return false;
		}

		$po_info = $line_update_info = array();
		$po_info['po_cancelled'] = 'Y';
		$po_info['po_cancel_date'] = date('Y-m-d H:i:s');
		PurchaseOrder::update($id, $po_info);
		$o2 = new object();
		$o2->id = $id;
		$o2->get_cost_of_goods_sold = true;
		$po_data = PurchaseOrder::getByO($o2);
		$po_data_one = $po_data[0];
			
		// Insert cost of goods sold as a NEGATIVE for the whole PO
		// but first get rid of records from single-line cancellations
		if (PurchaseOrder::wasCostOfGoodsSoldAlreadyWritten($id) === true)
		{
			PurchaseOrder::deletePrevNegCostOfGoodsSold($id);
			$cogs_info = array("po_id"=>$id, "amount"=> ((-1) * $po_data_one['cost_of_goods']));
			PurchaseOrder::insertCostOfGoodsSold($cogs_info);
		}
			
		$po_lines = Orders::getItems(0,0,'','',0,'','',0,0,0,0,0,$id, '', '', 0, 0, "all", '', '', '', 'Y');
		if ($po_lines)
		{
			foreach ($po_lines as $one_line)
			{
				if ($one_line['order_id'] != 0 || $one_line['bundle_order_item_id'] != 0)
				{
					$sql = " SELECT * FROM order_items WHERE id = ".$one_line['the_order_item_id'];
					$row = db_query_array($sql,'',true);
					unset($row['id'], $row['order_id'], $row['bundle_order_item_id']);
					$new_item_id = Orders::insertItem($row);
					$sql2 = "UPDATE purchase_order_items SET item_id = $new_item_id, 
							po_line_cancelled = 'Y', po_line_cancel_date = NOW() WHERE po_id = $id and item_id = ".
							$one_line['the_order_item_id'];
			// also update the ID on any supplier invoice lines that had the old item ID
			db_query("UPDATE supplier_invoice_lines SET po_item_id = $new_item_id WHERE po_item_id = ".$one_line['the_order_item_id']);											
				}
				else 	
				{
					$sql2 = "UPDATE purchase_order_items SET 
							po_line_cancelled = 'Y', po_line_cancel_date = NOW() WHERE po_id = $id and item_id = ".
							$one_line['the_order_item_id'];					
				}
				db_query($sql2);
			}
		}
		?>
		<script>$j(document).ready(function(){$j('.overlayInner').load('send_template_emails.php?template_type=vendor&po_num=<?=$_REQUEST['id']?>&store_id=<?=$CFG->website_store_id?>',function(){$j('.overlayOuter,.overlayInner').fadeIn(300); })});</script>
		<?
	}

	static function cancelOneLine($id, $item_id)
	{		
		global $CFG;
		
		if(!$id || !$item_id){
			return false;
		}		
	
		$po_lines = Orders::getItems($item_id);
		if (!$po_lines) return false;
		$one_line = $po_lines[0];
		
		if ($one_line['order_id'] != 0 || $one_line['bundle_order_item_id'] != 0)
		{
			$sql = " SELECT * FROM order_items WHERE id = ".$one_line['the_order_item_id'];
			$row = db_query_array($sql,'',true);
			unset($row['id'], $row['order_id'], $row['bundle_order_item_id']);
			$new_item_id = Orders::insertItem($row);
			$sql2 = "UPDATE purchase_order_items SET item_id = $new_item_id, 
					po_line_cancelled = 'Y', po_line_cancel_date = NOW() WHERE po_id = $id and item_id = ".
					$one_line['the_order_item_id'];
			// also update the ID on any supplier invoice lines that had the old item ID
			db_query("UPDATE supplier_invoice_lines SET po_item_id = $new_item_id WHERE po_item_id = ".$one_line['the_order_item_id']);									
		}
		else 	
		{
			$sql2 = "UPDATE purchase_order_items SET 
					po_line_cancelled = 'Y', po_line_cancel_date = NOW() WHERE po_id = $id and item_id = ".
					$one_line['the_order_item_id'];					
		}
		db_query($sql2);
		if (PurchaseOrder::wasCostOfGoodsSoldAlreadyWritten($id) === true)
		{
			$cogs_info = array("po_id"=>$id, "amount"=> ((-1) * $one_line['cost_of_goods']));
			PurchaseOrder::insertCostOfGoodsSold($cogs_info);
		}			
		?>
		<script>$j(document).ready(function(){$j('.overlayInner').load('send_template_emails.php?template_type=vendor&po_num=<?=$_REQUEST['id']?>&store_id=<?=$CFG->website_store_id?>',function(){$j('.overlayOuter,.overlayInner').fadeIn(300); })});</script>
		<?
	}
	
	static function delete($id){
		$result = db_delete("purchase_order",$id);

		db_delete('purchase_order_items',$id,'po_id');

		return $result;
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		if ($o->limited) 
		{
			$select_ = "SELECT purchase_order.id, purchase_order.code, 
				purchase_order.supplier_id,purchase_order.use_master_case_where_applicable,";
		}
									
		else $select_ = "SELECT purchase_order.*,";
		
		$select_ .= "brands.name AS supplier,
					admin_users.username AS created_by_name,
					SUM(poi.item_qty) as total_units,
					SUM(poi.item_qty * IF(poi.cust_each IS NULL OR poi.cust_each = -1,oi.price,poi.cust_each)) as total_price,
					CONCAT(brands.name, ' ($',SUM(poi.item_qty * IF(poi.cust_each IS NULL OR poi.cust_each = -1,oi.price,poi.cust_each)),')') as supplier_total,
					CONCAT(brands.name, ' (',IF(code='',purchase_order.id,code),')') as supplier_concat					
					";
		if(isset($o->po_info_str)){
			$select_ .= ", CONCAT(brands.name, ' (', IF(code='',purchase_order.id,code),') ($', ROUND( IF(poi.item_qty IS NULL, 0 , SUM(poi.item_qty * IF(poi.cust_each >= 0, poi.cust_each, IF(product_options.vendor_price IS NULL OR product_options.vendor_price < 0 OR product_options.vendor_price LIKE '0.00', products.vendor_price, product_options.vendor_price) ))) , 2),')') AS po_info_str";					  
		}
		
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(purchase_order.id)) as total ";
		}
		if ($o->get_cost_of_goods_sold)
		{
			$select_ .= ", ROUND( IF(poi.item_qty IS NULL, 0 , SUM(poi.item_qty * IF(poi.cust_each >= 0, poi.cust_each, IF(product_options.vendor_price IS NULL OR product_options.vendor_price < 0 OR product_options.vendor_price LIKE '0.00', products.vendor_price, product_options.vendor_price) ))) , 2) AS cost_of_goods";			
		}
		$from_ = " FROM purchase_order ";

		$where_ = " WHERE 1 ";

		$join_ .= " LEFT JOIN brands ON brands.id = purchase_order.supplier_id ";
		$join_ .= " LEFT JOIN admin_users ON admin_users.id = purchase_order.created_by ";
		$join_ .= " LEFT JOIN purchase_order_items poi ON poi.po_id = purchase_order.id ";
		$join_ .= " LEFT JOIN order_items oi ON oi.id = poi.item_id ";
		
		if(isset($o->skuQuery) || isset($o->po_info_str) || isset($o->get_cost_of_goods_sold)){
			$join_ .= " LEFT JOIN order_item_options oio ON oio.order_item_id = oi.id ";
			$join_ .= " LEFT JOIN products ON oi.product_id = products.id ";
			$join_ .= " LEFT JOIN product_options ON oio.product_option_id = product_options.id ";
		}

		if(isset($o->id)){
			$where_ .= " AND purchase_order.id = [id] ";
		}
		if(isset($o->supplier_id)){
			$where_ .= " AND purchase_order.supplier_id = [supplier_id] ";
		}
		if(isset($o->unbilled_only)){
			$where_ .= " AND supplier_invoice_lines.line_id IS NULL";
			$join_ .= " LEFT JOIN supplier_invoice_lines ON supplier_invoice_lines.po_item_id = poi.item_id";
		
		}
		if(isset($o->not_id)){
			$where_ .= " AND purchase_order.id <> [not_id] ";
		}
		if(isset($o->code)){
			$where_ .= " AND purchase_order.code = [code] ";
		}
		if(isset($o->po_sent)){
			$where_ .= " AND purchase_order.po_sent = [po_sent] ";
		}
		if(isset($o->po_type)){
			$where_ .= " AND purchase_order.po_type = [po_type] ";
		}
		if(isset($o->not_in_qb)){
		    if ($o->not_in_qb === false) {
		        $where_ .= " AND purchase_order.quickbooks_id <> '' ";
		    }
		    else {
			    $where_ .= " AND purchase_order.quickbooks_id = '' ";
			}
		}
		if(isset($o->start_date) || isset($o->end_date)){
			$where_ .= db_queryrange('purchase_order.date_added',$o->start_date,$o->end_date);
		}
		if(isset($o->query)){
			$fields = Array('brands.name','purchase_order.code','purchase_order.id');
			$where_ .= " AND " . db_split_keywords($o->query,$fields,'AND',true);
		}
		if(isset($o->skuQuery)){
			$fields = Array('products.vendor_sku','product_options.vendor_sku');
			$where_ .= " AND " . db_split_keywords($o->skuQuery,$fields,'AND',true);
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY purchase_order.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}

			$groupby_ = " GROUP BY purchase_order.id ";
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		//$result = db_template_query($sql,$o,'',false,false,'',true);
		$result = db_template_query($sql,$o);
		

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($orig_id, $limited = false, $po_info_str=false){

		$id = $orig_id;
		$o->id = $id;
		if ($limited) $o->limited = true;
		if ($po_info_str) $o->po_info_str = true;
		
		$result = self::getByO($o);

		if(!$result){
			return self::getByCode($orig_id);
		}
		return $result[0];
	}

	static function getByCode($code){
		$o->code = $code;

		$result = self::getByO($o);

		return $result[0];
	}

	static function getOpenItemTotals(){
		$sql = "
SELECT COUNT( DISTINCT(oi.id) ) AS total_items,
SUM(IF (poi.item_qty IS NULL, oi.qty, (oi.qty - poi.item_qty) )) as total_qty,
IF( brand_id IS NULL , 0, brand_id ) AS brand_id,
brands.name as brand_name,
SUM(oi.price) as total_price,
SUM( inventory.qty ) as total_inventory
FROM  `order_items` oi
LEFT JOIN purchase_order_items poi ON poi.item_id = oi.id
LEFT JOIN product_suppliers ps ON ps.product_id = oi.product_id
LEFT JOIN brands ON brands.id = brand_id
LEFT JOIN inventory ON inventory.product_id = oi.product_id
WHERE (poi.item_id IS NULL OR (oi.qty - poi.item_qty > 0))
GROUP BY brand_id
ORDER BY  `total_items` DESC
			";
// or (oi.qty - poi.item_qty > 0)
// sum(IF (poi.item_qty IS NULL, oi.qty, (oi.qty - poi.item_qty) ))
		$results = db_query_array($sql);

		return $results;

	}


	static function getOpenItems($preferred_supplier='',$po_number='',$order_id='',$start_date='',$end_date=''){

		global $CFG;
		
		$preferred_supplier = (int)$preferred_supplier;
		$po_number = (int)$po_number;
		$order_id = (int)$order_id;

		$sql = "SELECT
		IF( supplier.id IS NULL , brands.id, supplier.id ) AS supplier_id,
		IF( supplier.id IS NULL, brands.name, supplier.name ) as supplier_name,
		oi.id as order_item_id,
		oi.product_id as product_id,
		oi.bundle_order_item_id,
		products.is_bundle,
		products.price, products.num_in_master_case, ";
	
		if($po_number > 0){
			$sql .= " IF(poi.item_qty IS NULL, oi.qty, poi.item_qty) as qty, oi.qty as total_on_order,";
		} else {
			$sql .= " IF(poi.item_qty IS NULL, oi.qty, (oi.qty - poi.item_qty)) as qty,";
		}
		
		$sql .= "
		oi.order_id,
		orders.date as order_date,poi.po_line_cancelled,
		products.vendor_sku,
		products.name as product_name,
		IF(ps.brand_id = '$preferred_supplier', 1, 0) as custom_sort,
		IF(poi.po_id = '$po_number' ";
		if($preferred_supplier){
			$sql .= " AND IF( supplier.id IS NULL , brands.id, supplier.id ) = '$preferred_supplier' ";
		}
		$sql .= " , 1, 0) as checked,
		IF(inventory.qty IS NULL, 0, inventory.qty) AS in_stock
FROM  `order_items` oi
LEFT JOIN products ON products.id = oi.product_id
LEFT JOIN purchase_order_items poi ON poi.item_id = oi.id
LEFT JOIN product_suppliers ps ON (ps.product_id = oi.product_id ";
	if($preferred_supplier){
			$sql .= " AND ps.brand_id = '$preferred_supplier' ";
		}
		$sql .= ") 
LEFT JOIN brands supplier ON supplier.id = ps.brand_id
LEFT JOIN brands ON brands.id = products.brand_id
LEFT JOIN orders ON orders.id = oi.order_id
LEFT JOIN order_item_options ON order_item_options.order_item_id = oi.id
LEFT JOIN inventory ON (inventory.product_id = oi.product_id and inventory.store_id = '$CFG->default_inventory_location'
AND ((inventory.product_option_id = 0 AND order_item_options.id IS NULL) OR inventory.product_option_id = order_item_options.product_option_id))
		WHERE 1
";

		if($po_number > 0){
			$sql .= " AND (poi.po_id = $po_number)";
		} else {
			$sql .= " AND (poi.item_id IS NULL OR oi.qty > 0)";
		}

		

		if($order_id){
			$sql .= " AND (oi.order_id = '$order_id' ) ";
		}

		//$sql .= " ORDER BY `custom_sort` DESC, order_date ASC ";
		$sql .= " ORDER BY supplier_name ASC ";

		
		
		if($start_date || $end_date){
			// Wrap Around For Date Range including Checked
			$sql = " SELECT * FROM ($sql) iTable
					  WHERE ( iTable.checked ";
			$sql .= db_queryrange('DATE(iTable.order_date)', $start_date, $end_date, 'OR');
			$sql .= " ) ";
		}
		
		//echo $sql;
		$results = db_query_array($sql);

		// Get Product Options
		if (is_array($results))
		{
			$arr = $results;
	       foreach ($arr as $key => $value)
	       {

			   $options = null;
			   
	       	 // DO NOT ADD IN OPTION PRICING.  IT'S ALREADY ADDED!!!!

	       	 $q = "SELECT *
	       	       FROM
	       	       order_item_options
	       	       LEFT JOIN product_options
	       	         ON order_item_options.product_option_id = product_options.id
	       	       LEFT JOIN options
	       	         ON options.id = product_options.option_id
	       	       WHERE order_item_options.order_item_id = ".$value['order_item_id'];

		       	 	if($value['order_item_id']){
		       	      $options = db_query_array($q);
		       	 	}

	       	 	  if ( is_array($options) && count($options) )
	       	 	  {
				    $arr[$key]['options'] = $options;
				    // $arr[$key]['orig_price'] = $cart[$key]['price'];
				    $arr[$key]['product_vendor_sku'] = $options[0]['vendor_sku'];

				    $arr[$key]['product_name'] .= " - " . $options[0]['value'];
				    /*

				    foreach ($options as $option)
				    {

				      if ($option['additional_price'] > 0)
				      {
				      	print_ar($option);
				        echo "adding: $option[additional_price] to arr[{$key}][total_price]}";

				      }
					  $arr[$key]['price'] += $option['additional_price'];
					  echo "price = {$arr[$key][price]}";
					  $arr[$key]['total_price'] += $option['additional_price'] * $arr[$key]['qty'];
				    }

				    */

				  }
				  
				  if($value['is_bundle'] =='Y'){
				  	$components = Orders::getItems(0,0,'','',0,'','',0,0,0,0,0,'','','',0,0,"all","","",'','N',false,false,$value['order_item_id']);
				  } else $components = '';
				  if(is_array($components)){	
				  	unset($arr[$key]);
				  
				  	foreach ($components as $component){
				  		$component['qty'] = !$component['item_qty'] ? $component['qty'] : ($component['qty'] - $component['item_qty']);
				  		$component['supplier_id'] = $component['brand_id'];
				  		$component['supplier_name'] = $component['brand_name'];
				  		$component['order_item_id'] = $component['id'];
				  		$component['price'] = $component['id'];
				  		$arr[] = $component;
				  	}
				  }
	       }
	       //echo $sql;
	       return $arr;

		} // End Product Options

		return $results;

	}

	static function linkItemsToPO($id,$order_items,$qty_array, $use_master_case_where_applicable){
		global $CFG;
		
		if(!$id){
			return false;
		}
		$result = Orders::getItems(0,0,'','',0,'','',0,0,0,0,0,$id, '', '', 0, 0, "all", '', '', '', 'Y');
		if ($result)
		{
			$cancelled_array = array();
			$cancelled_date_array = array();
			foreach ($result as $one_result)
			{
				$sql = " SELECT * FROM purchase_order_items WHERE item_id = ".$one_result['the_order_item_id'];
				$row = db_query_array($sql,'',true);
				
				$id_of_this_item = $one_result[the_order_item_id];
				$cancelled_array[$id_of_this_item] = $row['po_line_cancelled'];
				$cancelled_date_array[$id_of_this_item] = $row['po_line_cancel_date'];
				if ($row) PurchaseOrder::updateQtyForMasterCaseDeletion($row, $use_master_case_where_applicable);
			}
		}
		db_delete('purchase_order_items',$id,'po_id');

		if(is_array($order_items)){
			foreach($order_items as $oi){
				
				if($oi['is_bundle'] == 'Y'){
					$bundle_components = Orders::getItems(0,0,'','',0,'','',0,0,0,0,0,'','','',0,0,"all","","",'','N',false,false,$oi['id']);
					foreach ($bundle_components as $component){
						$component['po_id'] = $id;
						$component['item_id'] = (int)$oi;
						$component['item_qty'] = (int) $qty_array[$oi]; 
						$component['po_line_cancelled'] = ($cancelled_array[$component] ? $cancelled_array[$component] : 'N');
						$component['po_line_cancel_date'] = ($cancelled_date_array[$component] ? $cancelled_date_array[$component] : '0000-00-00 00:00:00');
						
						$info = PurchaseOrder::updateQtyForMasterCasePurchase($info, $use_master_case_where_applicable);
							
						db_insert('purchase_order_items',$info);
					}
				}else{
					$info['po_id'] = $id;
					$info['item_id'] = (int)$oi;
					$info['item_qty'] = (int) $qty_array[$oi];
					$info['po_line_cancelled'] = ($cancelled_array[$oi] ? $cancelled_array[$oi] : 'N');
					$info['po_line_cancel_date'] = ($cancelled_date_array[$oi] ? $cancelled_date_array[$oi] : '0000-00-00 00:00:00');
					
					$info = PurchaseOrder::updateQtyForMasterCasePurchase($info, $use_master_case_where_applicable);
												
					db_insert('purchase_order_items',$info);
				}
			}
		}

		return true;
	}


	static function updateLink($po_id,$item_id,$price){
		$po_id = (int)$po_id;
		$item_id = (int)$item_id;
		$price = (float)$price;
		$sql = " UPDATE purchase_order_items SET cust_each = '$price'
			WHERE po_id = '$po_id' AND item_id = '$item_id' ";

		return db_query($sql);
	}
	static function updateQtyForMasterCasePurchase($info, $use_master_case_where_applicable) 
	{
		global $CFG;
		$the_item = Orders::getItems($info['item_id']);				
		$the_ind_item = $the_item[0];
		if ($the_ind_item['num_in_master_case'] > 0 && $use_master_case_where_applicable == 'Y')				
		{
			if ($the_ind_item['order_id'] == 0)
			{
				$num_to_put_on_po = $info['item_qty'];		
				$num_to_put_in_inv = $info['item_qty'];
				//mail("rachel@tigerchef.com", "from function", "Num ordered: ". $info['item_qty'] . ", num in master: " . $the_ind_item['num_in_master_case'] . ", on PO: $num_to_put_on_po, in inv: $num_to_put_in_inv");						
			}
			else 
			{
				if ($info['item_qty'] != $the_ind_item['order_items_qty'])
				{
					$num_to_put_on_po = ceil(($the_ind_item['order_items_qty'] / $the_ind_item['num_in_master_case'])) * $the_ind_item['num_in_master_case'];			
					$num_to_put_in_inv = $num_to_put_on_po - $the_ind_item['order_items_qty'];
					//mail("rachel@tigerchef.com", "from function 1", "Num ordered: ". $info['item_qty'] . ", num in master: " . $the_ind_item['num_in_master_case'] . ", on PO: $num_to_put_on_po, in inv: $num_to_put_in_inv");
				}
				else 
				{
					$num_to_put_on_po = ceil(($info['item_qty'] / $the_ind_item['num_in_master_case'])) * $the_ind_item['num_in_master_case'];			
					$num_to_put_in_inv = $num_to_put_on_po - $info['item_qty'];
					//mail("rachel@tigerchef.com", "from function 2", "Num ordered: ". $info['item_qty'] . ", num in master: " . $the_ind_item['num_in_master_case'] . ", on PO: $num_to_put_on_po, in inv: $num_to_put_in_inv");
				}
		
				$info['item_qty'] = $num_to_put_on_po;
			}
			// insert the difference between the number for the PO and the number otherwise needed
			// into inventory or add to existing inventory record
			// Per Joe and Mr. Weber Aug 13, '14, don't change inventory
			/*
			$already_record = Inventory::get(0,0,0,'','', $the_ind_item['product_id'], $the_ind_item['product_options_id'], 0, $CFG->default_inventory_location);
			$info2 = array();
			$output= print_r($already_record, true);
			//mail('rachel@tigerchef.com', 'already_record', $output);
			if ($already_record)
			{
				$qty_already_there = $already_record[0]['qty'];
				$id_already_there = $already_record[0]['id'];
				//$info2['qty'] = $qty_already_there + $num_to_put_in_inv;
				$onorder_already_there = $already_record[0]['on_order'];
				$info2['on_order'] = $num_to_put_in_inv + $onorder_already_there;
				//mail("rachel@tigerchef.com", $info2['qty'], $onorder_already_there . " ". $num_to_put_in_inv . " " . $id_already_there);
				Inventory::update($id_already_there, $info2);
			}
			else 
			{					
				$info2['product_id'] = $the_ind_item['product_id'];
				$info2['product_option_id'] = $the_ind_item['product_options_id'];
				$info2['store_id'] = $CFG->default_inventory_location;
				$info2['on_order'] = $num_to_put_in_inv;
				Inventory::insert($info2);					
			}
			$output= print_r($info2, true);
			//mail('rachel@tigerchef.com', 'info2', $output);
			 
			 */
			
		}
		return $info;
	}
	
	static function updateQtyForMasterCaseDeletion($info, $use_master_case_where_applicable) 
	{
		global $CFG;
		$the_item = Orders::getItems($info['item_id']);				
		$the_ind_item = $the_item[0];
		
		if ($the_ind_item['num_in_master_case'] > 0 && $use_master_case_where_applicable == 'Y')			
		{
			if ($the_ind_item['order_id'] == 0)
			{
				$num_to_put_on_po = $info['item_qty'];		
				$num_to_put_in_inv = $info['item_qty'];
				//mail("rachel@tigerchef.com", "from function", "Num ordered: ". $info['item_qty'] . ", num in master: " . $the_ind_item['num_in_master_case'] . ", on PO: $num_to_put_on_po, in inv: $num_to_put_in_inv");						
			}
			else 
			{			
				$num_to_put_on_po = ceil(($the_ind_item['order_items_qty'] / $the_ind_item['num_in_master_case'])) * $the_ind_item['num_in_master_case'];
				$num_to_put_in_inv = $num_to_put_on_po - $the_ind_item['order_items_qty'];
				//mail("rachel@tigerchef.com", "from function", "Num ordered: ". $the_ind_item['order_items_qty'] . ", num in master: " . $the_ind_item['num_in_master_case'] . ", on PO: $num_to_put_on_po, in inv: $num_to_put_in_inv");		
			}
			// remove the difference between the number for the PO and the number otherwise needed
			// from inventory
			$already_record = Inventory::get(0,0,0,'','', $the_ind_item['product_id'], $the_ind_item['product_options_id'], 0, $CFG->default_inventory_location);
			$info2 = array();
			
			if ($already_record)
			{
				$qty_already_there = $already_record[0]['qty'];
				$id_already_there = $already_record[0]['id'];
				$info2['qty'] = $qty_already_there - $num_to_put_in_inv;
				Inventory::update($id_already_there, $info2);
			}
			// shouldn't be no record--unless deleting item AFTER num_in_master_case added to it
			else 
			{					
				//  do nothing 
			}
			
		}		
	}
	
	static function generatePO($po_id, $invoice_id, $type='po', $email=false){
	    global $CFG;
	    
	    if ($_REQUEST['dialog']) $dialog = $_REQUEST['dialog'];
	    else $dialog = 'no';
	//    $order          = Orders::get1($oid);
	//    $ostatus        = OrderStatuses::get1($order['current_status_id']);
	//	$customer       = Customers::get1($order['customer_id']);
		$po = PurchaseOrder::get1($po_id);

		$po_number = $po['code'] ? $po['code'] : $po['id'];

		if(!$po){
			die("Invalid Purchase Order.");
		}
		
		$orderitems     = Orders::getItems('','','','','','','','','','','','',$po_id);
		
		$invoice        = Invoices::get1($invoice_id);

		$finds    = array();
		$replaces = array();

		/* load the invoice template */
		$invoice_template = Invoices::get_invoice_template('',$type);
		$invoice_template_for_body = Invoices::get_invoice_template('',"po");
		//$invoice_template = str_ireplace('[barcode]', sprintf('<img src="%sbarcode.php?code=%012d&encoding=EAN&scale=1&mode=jpg">', $CFG->baseurl, $order['id']), $invoice_template);
		$finds[]    = '[po_date]';
		$replaces[] = db_date($po['date_added']);

		$finds[]    = '[po_num]';
		$replaces[] = $po['code'] ? $po['code'] : $po['id'];

		$finds[]    = '[ship_date]';
		$replaces[] = db_date($po['date_added']);



		$finds[]    = '[fob_point]';
		$replaces[] = '';

		$finds[]    = '[terms]';
		$replaces[] = 'Net';

		$finds[]    = '[frt_billing]';
		$replaces[] = '&nbsp;';

		$finds[]    = '[notes]';
		if($po['notes']){
		$replaces[] = '<div id="notes">
		 <p><strong>Special Instructions:</strong>
		 <textarea class="notes" name="info[notes]">'.$po['notes'].'</textarea>
	 </div>'; // $po['notes'];
		} else {
			$replaces[] = '';
		}

		$brand = Brands::get1($po['supplier_id']);

		/* MFG */
		$finds[]      = '[mfg_address]';
		if($po['cust_mfg_box']){
			$replaces[] = $po['cust_mfg_box'];
		} else {
			$replaces[]   = $brand['name']. "\r\n".
							$brand['address1']."\r\n".
							( $brand['address2'] ? ($brand['address2']."\r\n") : '' ).
							$brand['city']. ", ".
							$brand['state']. "  ".
							$brand['zip']. "\r\n\r\n".
							( $brand['phone'] ? "Phone: $brand[phone]" : '' ). "\r\n".
							( $brand['fax'] ? "Fax: $brand[fax]" : "" ). "\r\n";
		}


		/* Billing */
		$finds[]    = '[billing_address]';
		if($po['cust_bill_box']){
			$replaces[] = $po['cust_bill_box'];
		} else {
			$replaces[] = $invoice['bill_to_address'];
		}

		/* Buyer */
		$finds[]    = '[buyer_address]';
		if($po['cust_buyer_box']){
			$replaces[] = $po['cust_buyer_box'];
		} else {
			$replaces[] = $invoice['bill_to_address'];
		}


		/* place orders in */
		$order_items = '<tr><td colspan="6"></td></tr>';
		$grand_total          = 0.0;
		$ship_address_combo = array();

		$order_ids = array();


	    if ($orderitems)
	    {

            $order_items = '<style type="text/css">#order_items img{max-width:200px;max-height:200px;}</style>';

	    	foreach ($orderitems as $x => $oi)
	    	{
	    		$product              = Products::get1($oi['product_id']);
                if(isset($oi['product_options_vendor_sku']) && !empty($oi['product_options_vendor_sku'])){
                    $product_identifier = $oi['product_options_vendor_sku'];
                }
                else{
	    	        $product_identifier = $oi['product_vendor_sku'] ? $oi['product_vendor_sku'] : $product['vendor_sku'];
                }
                
                //filter by optional items
                if(isset($oi['options'][0]['optional_options']) && !empty($oi['options'][0]['optional_options'])){
                    $product_identifier .= $oi['options'][0]['optional_options'];
                }
                if(isset($oi['options'][0]['available_options_id']) && !empty($oi['options'][0]['available_options_id'])){
                    $product_identifier .= $oi['options'][0]['available_options_id'];
                }
                if(isset($oi['options'][0]['value']) && !empty($oi['options'][0]['value'])){
                    $product_identifier .= $oi['options'][0]['value'];
                }
                if(isset($oi['options'][0]['preset_value_ids']) && !empty($oi['options'][0]['preset_value_ids'])){
                    $product_identifier .= $oi['options'][0]['preset_value_ids'];
                }

	    		// Combine Items
				if(isset($orderitems2[$product_identifier])){
					$orderitems2[$product_identifier]['item_qty'] += $oi['item_qty'];
				} else {
					$orderitems2[$product_identifier] = $oi;					
				}
	    	}

	    	$freight_items = false;

	    	$orderitems = $orderitems2;
	    	
	    	$x = -1;
            $oi_ids = array();
	    	foreach ($orderitems as $sku => $oi)
	    	{
                if(in_array($oi['the_order_item_id'], $oi_ids)){
                    continue;
                }
                $additional_cost = 0.00;
                $oi_ids[] = $oi['order_item_id'];
				$x += 1;
	    		$item_id              = $oi['id'];
	    	    $product              = Products::get1($oi['product_id']);
	    	    //$product_sku          = $oi['product_vendor_sku'] ? $oi['product_vendor_sku'] : $product['vendor_sku'];
	    	    //$product_sku          = $oi['dist_sku'] ? $oi['dist_sku'] : $oi['mfr_part_num']; 
                $product_sku          = PurchaseOrder::getSKU($oi);

//	    	    		$oi['product_vendor_sku'] ? $oi['product_vendor_sku'] : $product['vendor_sku'];
	    	    $product_name         = $product['name'];
               if(isset($oi['options'])){
                   $product_name .= ' ' .ProductOptionCache::getDescriptionForOptions($oi['options'], $oi['product_id']);
                   $additional_cost = ProductAvailableOptions::getAdditionalCost($oi['options'][0]['optional_options']);
                    $additional_cost += ProductOptionCache::getCostForPO($oi['product_id'], $oi['options'][0]['product_option_id']);
                }    
 
                $product_unit_cost    = ( ( $oi['cust_each'] >= 0.0  && $oi['cust_each'] != '-1' ) ? $oi['cust_each'] : (($oi['option_vendor_price'] && $oi['option_vendor_price'] > 0 && $oi['option_vendor_price'] != "0.00") ? $oi['option_vendor_price'] : $oi['vendor_price']));
               $product_unit_cost    += $additional_cost;
	    	    $quantity             = $oi['item_qty'];
	    	    $price                = $product_unit_cost;
	    	    $item_total           = $price * $quantity;
				$grand_total         += ($quantity * $product_unit_cost);

				$order_ids[$oi['order_id']] = $oi['order_id'];
				$this_order_id = $oi['order_id'];
	    	    /* these have to be figured out */
	    	    $quantity_shipped     = 0;
	    	    $quantity_bo          = 0;

	    		    	    if($type == 'po'){
		    	    $order_items .= "<tr class='e".($x%1)."'>
							<td valign=top>".($x+1)."</td>
							<td valign=top class='qty'>$quantity</td>
							<td valign=top>$product_sku</td>
							<td valign=top>$product_name";
		    	    
					$order_items .=	"</td><td valign=top>";
					if ($oi['priced_per'] != "") $order_items .= $oi['priced_per'];
					else $order_items .= "&nbsp;";
					$order_items .= "</td><td class='po_each price'>$<input type='text' value='".number_format($product_unit_cost,2, '.','')."' name='each[$item_id]'/></td>
							<td class='line_total price'>$<input type='text' value='".number_format($item_total, 2, '.','')."' name='line_total[$item_id]' readonly='readonly' /></td>
							</tr>";
	    	    } else {
	    	    	$order_items .= "<tr class='e".($x%1)."'>
							<td valign=top>".($x+1)."</td>
							<td valign=top class='qty'>$quantity</td>
							<td valign=top>$product_sku</td>
							<td valign=top>$product_name</td>
							<td valign=top>";
	    	    	if ($oi['priced_per'] != "") $order_items .= $oi['priced_per'];
					$order_items .="</td><td class='po_each price'>$".number_format($product_unit_cost,2)."</td>
							<td class='line_total price'>$".number_format($item_total, 2)."</td>
							</tr>";
	    	    }
				$ship_addr = $oi['shipping_address1'] . " " .
							 $oi['shipping_address2'] . " " .
							 $oi['shipping_city'] . " " .
							 $oi['shipping_state'] . " " .
							 $oi['shipping_zip'];
				if(trim($ship_addr) == ""){
					$ship_addr = $invoice['bill_to_address'];
				}
				$ship_address_combo[$ship_addr] = $ship_addr;
	    	}
	    	
	    	//check if items on this PO require freight shipping
	    	if($oi['shipping_type'] == 'Freight'){
	    		$freight_items = true;
	    	}
	    }

	    if(count($order_ids) > 1){
			$multi_orders = true;
	    }
		if( $order_ids ){
	    	$order_id1 = array_pop($order_ids);
	    	$order1 = Orders::get1($order_id1);
		}
		
	    $finds[]    = '[ship_via]';

	    if($po['cust_ship_via']){			
			$replaces[] = $po['cust_ship_via'];

		} else if($multi_orders){

	    	$replaces[] = 'Best Method1';

	    } else if( $order1 ){
	    	
	    	if($freight_items && (strpos($order1['shipping_method'], 'Freight')===false || strpos($order1['shipping_method_2'], 'Freight')===false))
	    	{ //check if order if freight, and if a liftgate is required
	    		if($order1['liftgate_required'] == 'Y'){
	    			$replaces[] = "*Freight. Liftgate required*";
	    		} else {
	    			$replaces[] = "*Freight*";
	    		}
	    	} elseif ($order1['shipping_id'] !== '1')
	    	{ // if not ground shipping, bold the ship method.
	    		$replaces[] = "*".$order1['shipping_method']."*";
	    	}
	    	else
	    	{ 
	    		$replaces[] = "Ground";
	    	}

		} else {
			$replaces[] = 'Best Method2';
		}

	//
	//    print_pre($ship_address_combo);
	//    exit;

		/* Shipping */
		$to_us = false;
		$finds[]    = '[shipping_address]';
		if($po['cust_ship_box']){
			$replaces[] = $po['cust_ship_box'];
			if ($po['cust_ship_box'] == $invoice['bill_to_address'] || $po['cust_ship_box'] == "pick up") $to_us = true;
		} else if(count($ship_address_combo) > 1){
			// Different Ship addresses detected
			$replaces[] = $invoice['bill_to_address'];
			$to_us = true;
		} else  if (!$this_order_id) {
			$replaces[] = $invoice['bill_to_address'];
			$to_us = true;
		}
		else {
			// Only 1 shipping address
			foreach($orderitems as $first_item){ break; }
			$replaces[] = $first_item['shipping_first_name'] . " " . $first_item['shipping_last_name'] . "\r\n".
						  ( $first_item['shipping_company'] ? ($first_item['shipping_company'] . "\r\n") : '') .
						  $first_item['shipping_address1'] . "\r\n" .
						  ( $first_item['shipping_address2'] ? ($first_item['shipping_address2'] . "\r\n") : '') .
						  $first_item['shipping_city'] . ", " .
						  $first_item['shipping_state'] . "  " .
						  $first_item['shipping_zip'] . "\r\n\r\n" .
						  ( $first_item['shipping_phone'] ? ("Phone: ".$first_item['shipping_phone']) : '' );
		}

		$finds[]    = '[total]';
		$replaces[] = number_format($grand_total,2);

		$finds[]    = '[order_items]';
		$replaces[] = $order_items;

		$finds[]    = '[email_footer]';
		$replaces[] = '';
		$finds[] = '[ship_freight_disclaimer]';
		if (in_array($po['supplier_id'],$CFG->vendors_shipping_free_freight_to_us))
		{
			$replaces[] = "";	
		}
		else 
		{
			$replaces[] = "If shipping freight, please call or email to confirm. We will not take responsibility for any orders that are shipped freight and not confirmed.<br/>";
		}
		
		$finds[] = '[shipping_instructions_a]';
		$finds[] = '[shipping_instructions_b]';
		$finds[]    = '[ups_account_num]';
		$finds[]    = '[ship_bold_start]';
		$finds[]    = '[ship_bold_end]';
		
		if (in_array($po['supplier_id'],$CFG->vendors_shipping_on_own_account))
		{
			$replaces[] = "Please ship the least expensive way on your account.";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
		}
		else if (in_array($po['supplier_id'],$CFG->vendors_shipping_free_freight_to_us) && $to_us)
		{
			$replaces[] = "Please ship free freight on your account.";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
		}
		else if (in_array($po['supplier_id'],$CFG->vendors_shipping_own_fedex))
		{
			$replaces[] = "Please ship via your Fedex account";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
			$replaces[] = "";
		}
		else
		{
			$replaces[] = "If shipping UPS, please use our account - ";
			$replaces[] = ".  Please use our PO# in the UPS reference field when shipping out our orders.";	
		
			if (!$multi_orders && $order1['order_tax_option'] == $CFG->able_kitchen_store_id)
			{
				$shipper_info = UPSShipper::get1($CFG->able_kitchen_shipper_id);
				$replaces[] = $shipper_info['ups_account_number'];
				$replaces[] = "<b>";
				$replaces[] = "</b>";
			}
			else // otherwise, put our regular UPS account
			{
				$shipper_info = UPSShipper::get1($CFG->default_from_shipper);
				$replaces[] = $shipper_info['ups_account_number'];
				$replaces[] = "";
				$replaces[] = "";
			}
		}
		
		$invoice_template = str_ireplace($finds, $replaces, $invoice_template);
		$invoice_template_for_body = str_ireplace($finds, $replaces, $invoice_template_for_body);
		
		if($type == 'po_pdf'){

			/* ===============================================
			 * ========== GENERATE PDF =======================
			 * =============================================== */

			require_once("MPDF/mpdf.php");
			
			$po_sent_info = array();
			$po_sent_info['po_sent'] = 'Y';
			$po_sent_info['po_sent_date'] = date('Y-m-d H:i:s');
			self::update($po_id,$po_sent_info);
			// insert record for cost of goods sold.  first, get it.
			$o2 = new object();
			$o2->id = $po_id;
			$o2->get_cost_of_goods_sold = true;
			$po_data = PurchaseOrder::getByO($o2);
			$po_data_one = $po_data[0];
			
			// Insert cost of goods sold
			if (PurchaseOrder::wasCostOfGoodsSoldAlreadyWritten($po_id) === false)
			{
				$cogs_info = array("po_id"=>$po_id, "amount"=>$po_data_one['cost_of_goods']);
				PurchaseOrder::insertCostOfGoodsSold($cogs_info);
			}
			// Set page size and default font, size, margins
			$mpdf=new mPDF('','Letter','','',1,1,8,16);

			$mpdf->useOnlyCoreFonts = true; // reduces document size & processing time

			$mpdf->SetDisplayMode('fullpage'); // set default zoom level

			// LOAD a stylesheet
			//$stylesheet = file_get_contents('mpdfstyleA4.css');
			//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

			$stylesheet = file_get_contents($CFG->dirroot . "/invoices/pdf.css");

			$footer = '<div class="footer">Purchase Order '.($po['code'] ? $po['code'] : $po['id']).
					' &mdash; Page {PAGENO} of {nbpg} </div>';
			$mpdf->SetHTMLFooter( $footer );

			if(!$stylesheet){
				$stylesheet = '';
			}
			if(!$invoice_template){
				$invoice_template = '';
			}
			$stylesheet = utf8_encode($stylesheet);
			$invoice_template = utf8_encode($invoice_template);
			$mpdf->WriteHTML($stylesheet,1);
			$mpdf->WriteHTML($invoice_template,2);

			// $mpdf->WriteFixedPosHTML($footer,0,250,200,70);

			if($email){
							// ============ E-MAIL A PDF ===================
				$emailInfo  = $_REQUEST['emailInfo'];

				// Get PDF Binary Data
				$content = $mpdf->Output("","S");

				$content = chunk_split(base64_encode($content));
				$mailto = $emailInfo['to_name'] . " <".$emailInfo['to_email'].">";
				$to_emails_array = explode(",",$emailInfo['to_email']);								
				$to_emails_array = array_map('trim', $to_emails_array);
				$from_name = $emailInfo['from_name'];
				$from_mail = $emailInfo['from_email'];
				$replyto = $from_mail;
				$uid = md5(uniqid(time()));
				$subject = $emailInfo['subject'];
				$message = $emailInfo['message'];
				$filename = "PO_$po_number.pdf";
				$path = "/tmp/".$filename;
				$mpdf->Output($path,'F');
//$body_to_send = $subject."\n\n\n";
				if ($message)
				{
					$body_to_send .= "Notes:<br/>****$message****\n<br/>";
				}
				$body_to_send .= '<link rel="stylesheet" href="https://www.tigerchef.com/invoices/default.css" type="text/css" media="all" />';
				$body_to_send .= '<style>.invoice{margin-left:0px}</style>';
								
				$re = "/<textarea[^>]+>((\\s|.)+?)<\\/textarea>/m"; 
				$invoice_template_for_body = preg_replace_callback($re, function ($matches) { $the_return = '<span class="textarea_replacement">'. nl2br($matches[1]).'</span>'; return $the_return;}, $invoice_template_for_body, -1, $count);
				$re2 = "/<input((.|\\s)*?)value=[\\\"\\'](.*?)[\\\"\\']((.|\\s)*?)\\/>/m";
				$invoice_template_for_body = preg_replace_callback($re2, function ($matches) { $the_return = $matches[3]; return $the_return;}, $invoice_template_for_body, -1, $count);
				$body_to_send .= $invoice_template_for_body; 
require_once('swiftmailer/swiftmailer/lib/swift_required.php');

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);
$swift_message = Swift_Message::newInstance()
    ->setFrom(array($from_mail))
    ->setTo($to_emails_array)
    ->setSubject($subject)
    ->setEncoder(Swift_Encoding::get7BitEncoding())
    ->setBody($body_to_send, 'text/html')
    ->addPart(strip_tags($body_to_send), 'text/plain')
    ->attach(Swift_Attachment::fromPath($path))
    
;
/* this is the call without the message in the email body */
/*$swift_message = Swift_Message::newInstance()
    ->setFrom(array($from_mail))
    ->setTo($to_emails_array)
    ->setEncoder(Swift_Encoding::get7BitEncoding())
    ->setSubject($subject)
    ->setBody($message, 'text/html')
    ->addPart(strip_tags($message), 'text/plain')
    ->attach(Swift_Attachment::fromPath($path))
;*/
$mailer->send($swift_message);
				/*$header = "From: ".$from_name." <".$from_mail.">\r\n";
				$header .= "Reply-To: ".$replyto."\r\n";
				$header .= "MIME-Version: 1.0\r\n";
				$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
				$header .= "This is a multi-part message in MIME format.\r\n";
				$header .= "--".$uid."\r\n";
				$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
				$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
				$header .= $message."\r\n\r\n";
				$header .= "--".$uid."\r\n";
				//$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
				$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n";
				
				$header .= "Content-Transfer-Encoding: base64\r\n";
				$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
				$header .= $content."\r\n\r\n";
				$header .= "--".$uid."--";*/
			
				$header = "From: ".$from_name." <".$from_mail.">\n";
				$header .= "Reply-To: ".$replyto."\n";
				$header .= "MIME-Version: 1.0\n";
				$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
				$body = "Content-Transfer-Encoding: 7bit\n";
				$body .= "This is a multi-part message in MIME format.\n";
				$body .= "--".$uid."\n";
				$body .= "Content-type:text/plain; charset=iso-8859-1\n";
				$body .= "Content-Transfer-Encoding: 7bit\n\n";
				$body .= $message."\n\n";
				$body .= "--".$uid."\n";
				//$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
				$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n";
				
				$body .= "Content-Transfer-Encoding: base64\n";
				$body .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
				$body .= $content."\n\n";
				$body .= "--".$uid."--";
		//		$is_sent = mail($mailto, $subject, $body, $header);
				$is_sent = true;
				if($is_sent){
					$info = array();
		  			$info['purchase_order_id'] = $po_id; 
		  			$info ['user_id'] = $_SESSION ['id'];
		  			$info['notes'] = "PO emailed to " . $emailInfo['to_email'];
		  			if ($message != '') $info['notes'] .= " with message " . $message;		  				  		 				
		  			$note_id = PurchaseOrder::addNote ( $info );
					if ($order_id1 && !$multi_orders)
		  			{
		  			 	$order_info = Orders::get1($order_id1);
		  				if ($order_info && ($order_info['current_status_id'] == $CFG->charge_cc_at_id || $order_info['current_status_id'] == $CFG->on_hold_status_id || $order_info['current_status_id'] == $CFG->awaiting_fraud_screening_status_id))
		  				{
		  					$open_items = Orders::getNotOnPO($order_id1);
		  					if (!$open_items)
		  					{
		  						Orders::update ( $order_id1, array ('current_status_id' => $CFG->processing_order_status_id) );
		  					}
		  				}
		  			}
					return "A PDF of the PO was successfully e-mailed.";
				} else {
					return "ERROR: There was a problem e-mailing the PDF.";
				}				

			} else {
				// Output for Download
				if (strpos($order1['shipping_method'], "Ground") === false)
					$mpdf->Output("PO_".$po_number."_".str_replace(" ", "_", $order1['shipping_method']).".pdf","D");
				else 
					$mpdf->Output("PO_$po_number.pdf","D");
			}

			exit;
			//==============================================================
			//===============  END OF PDF ==================================
			//==============================================================



		} else if($type == 'po'){
			if (strpos($order1['shipping_method'], "Ground") === false && strpos($order1['shipping_method'], "Freight") === false && strpos($order1['shipping_method'], "Flat Rate") === false)
			{
				$rush_text = "RUSH--";	
				$shipping_method_text = "--".$order1['shipping_method'];
			}
			else 
			{
				$rush_text = "";
				$shipping_method_text = "";
			}
	?>

			<link rel="stylesheet" href="/invoices/default.css" type="text/css" media="all" />

			<form id="cust_po_form" method="post" onsubmit="return false;" action="ajax.purchase_order.php">
				<div>
					<input type="hidden" id="po_id" name="po_id" value="<?=$po_id?>" />
					<input type="hidden" id="invoice_id" name="invoice_id" value="<?=$invoice_id?>" />
					<?

					if($_REQUEST['email_prompt'] == 1){ ?>

						<div class="email_prompt">
							<h1>Submit PO By E-mail</h1>

							<p><label><strong>From</strong> Name: </label>    <input type="text" name="emailInfo[from_name]" value="<?=$CFG->purchasing_company_name?>" /></p>
							<p><label>Email: </label>   <input type="text" name="emailInfo[from_email]" value="<?=$CFG->company_po_email?>" /></p>
							<p><label><strong>To</strong> Name: </label>      <input type="text" name="emailInfo[to_name]" value="<?=$brand['name']?>" /></p>
							<p><label>Email: </label>     <input type="text" name="emailInfo[to_email]" value="<?=$brand['po_email'] ? $brand['po_email'] : $brand['email']?>" /></p>

							<p><label>Subject:</label> <input type="text" name="emailInfo[subject]" value="<?=$rush_text?><?= $CFG->po_email_subject_start?><?=$po_number?><?=$shipping_method_text?>" /> </p>
							<p><label>Message: </label>
								<textarea name="emailInfo[message]"><?=$po['email_message']?></textarea>
							</p>
														<p><label>&nbsp;</label>
							<?if($_REQUEST['in_dialog'] == 'yes'){

							}else{?>
							<input type="button" id="" title="Go back to the overview screen" value="&laquo; Go Back" onclick="window.location.href='/edit/purchase_orders.php?action=view&id=<?=$po_id?>';"/>
							<?php }?>
							<input type="button" id="send_po_by_email" value="Send &raquo;" onclick="submit_po_email('<?=$_REQUEST['in_dialog']?>','<?=$po_id?>');" /></p>
						</div>

					<? } else {
						ob_start();

						?>
						<div class="cust_buttons non_printing">
							<input type="button" id="" title="Go back to the overview screen" value="&laquo; Go Back" onclick="window.location.href='/edit/purchase_orders.php?action=view&id=<?=$po_id?>';"/>
							<input type="button" id="" title="Save your custom modifications into the PO" value="Save Data" onclick="po_save_form();"/>
							<input type="button" id="" value="Print Screen" onclick="window.print();" />
							<input type="button" id="" title="Download a PDF of this PO for faxing or archiving." value="Make PDF" onclick="po_make_pdf(<?=$po_id?>);" />
							<input type="button" id="" title="E-mail a PDF of this PO to the Supplier or other e-mail" value="Email PDF" onclick="po_email_pdf(<?=$po_id?>,'<?= $dialog?>');return false;" />
						<? if ($po['supplier_id'] == $CFG->winco_supplier_id && strtoupper($oi['shipping_country']) != "CANADA") {?>
							<input type="button" id="" title="Send this PO via EDI" value="Send PO via EDI" onclick="po_send_edi(<?=$po_id?>);" <?php if($po['po_sent'] == 'Y') echo " disabled";?>/>
						<? }?>	

						</div>
						<?
						$buttons_bar = ob_get_flush();
					}
					?>

					<?=$invoice_template?>


					<?=$buttons_bar?>
				</div>
			</form>

			<script type="text/javascript">
				$j(function($){
					$(".po_each input").bind("keyup",update_po_totals);
				});
			</script>

			<?php
		}
			

		/*
		print_ar($order);
		print_ar($customer);
		print_ar($orderitems);
		*/

	    // exit;
	}
    static function getSKU($oi){
        $prefixes = array('product_options_', '');
        $sku_options = array('dist_sku', 'mfr_part_num');
        foreach($prefixes as $prefix){
            foreach($sku_options as $curr){
                $field = $prefix . $curr;
                if(isset($oi[$field]) && !empty($oi[$field])
                    && $oi[$field] != 'NULL'){
                    return $oi[$field];   
                }
            }
        }
    }
static function getOrdersOnPO($po_id)
	{
		$sql = "SELECT DISTINCT order_id, order_tax_option, blind_invoice FROM (purchase_order_items, order_items, orders) 
				WHERE purchase_order_items.item_id = order_items.id AND purchase_order_items.item_qty > 0
				AND purchase_order_items.po_id = $po_id AND orders.id = order_items.order_id
				UNION
				SELECT DISTINCT order_items.order_id, order_tax_option, blind_invoice FROM (purchase_order_items, order_items, order_items oi2, orders) 
				WHERE purchase_order_items.item_id = oi2.id AND purchase_order_items.item_qty > 0
				AND purchase_order_items.po_id = $po_id AND oi2.order_id = 0 AND oi2.bundle_order_item_id = order_items.id AND orders.id = order_items.order_id
				";
		$result = db_query_array($sql);
		return $result;
	}
static function addNote($info)
	{
		return db_insert('purchase_order_notes', $info, 'date_added');
	}

	static function getNotes($purchase_order_id, $type='notes', $id=0, $cancellation_reason = false)
	{
		$sql = "SELECT purchase_order_notes.*,
					  CONCAT(admin_users.first_name, ' ',  admin_users.last_name) AS user_name
				    FROM purchase_order_notes
				    LEFT JOIN admin_users ON admin_users.id = purchase_order_notes.user_id
				    WHERE 1 ";
		if( $id )
			$sql .= " AND purchase_order_notes.id = $id ";
		
		if($cancellation_reason)
			$sql .= " AND purchase_order_notes.notes REGEXP 'REASON FOR CANCELLATION:' ";

		$sql .=" AND purchase_order_notes.purchase_order_id = $purchase_order_id and type = '$type'
				    ORDER BY date_added DESC";

		return db_query_array($sql);
	}

	static function hasNotes( $id )
  	{
  		$sql = " SELECT count( * ) as count from purchase_order_notes where order_id = $id ";
  		$rslt = db_query_array( $sql );

  		return $rslt[0]['count'];
  	}
	static function updateNote( $note_id, $info )
  	{
  		return db_update('purchase_order_notes', $note_id, $info );
  	}

  	static function deleteNote( $note_id )
  	{
  		return db_delete( 'purchase_order_notes', $note_id );
	}
	static function insertCostOfGoodsSold($info)
	{
		db_insert('cost_of_goods_sold', $info);
	}
	static function wasCostOfGoodsSoldAlreadyWritten($po_id)
	{
		$sql = "SELECT * FROM cost_of_goods_sold WHERE po_id = $po_id AND item_id = 0 and amount > 0";
		$result = db_query_array($sql);
		
		if ($result) return true;
		else return false;
	}		
	static function deletePrevNegCostOfGoodsSold($po_id)
	{
		$sql = "DELETE FROM cost_of_goods_sold WHERE po_id = $po_id and amount < 0";
		db_query($sql);		
	}


    
     static function sendViaEdi($po_id)
    {
    	global $CFG; 
    	
		$po = PurchaseOrder::get1($po_id);
		if ($po['supplier_id'] == $CFG->winco_supplier_id) 
		{
			EDIOperations::generateDataForWinco850($po_id);
			db_connect($CFG->dbhost, $CFG->dbname, $CFG->dbuser, $CFG->dbpass);
			//echo "hi";
			$po_sent_info = array();
			$po_sent_info['po_sent'] = 'Y';
			$po_sent_info['po_sent_date'] = date('Y-m-d H:i:s');
			self::update($po_id,$po_sent_info);
			$info = array();
		  	$info['purchase_order_id'] = $po_id; 
		  	$info ['user_id'] = $_SESSION ['id'];
		  	$info['notes'] = "PO sent to vendor via EDI";		  				  				  		 				
		  	$note_id = PurchaseOrder::addNote ( $info );
		  	
		  	$multi_orders = false;
		  	$order_ids = array();
			$orderitems     = Orders::getItems('','','','','','','','','','','','',$po_id);

			if ($orderitems)
	    	{
				foreach ($orderitems as $x => $oi)
	    		{
					$order_ids[$oi['order_id']] = $oi['order_id'];
				}
			}
			if(count($order_ids) > 1){
				$multi_orders = true;
	    	}
			if( $order_ids ){
	    		$order_id1 = array_pop($order_ids);	    	
			}
			if ($order_id1 && !$multi_orders)
		  	{
		  	$order_info = Orders::get1($order_id1);
		  	if ($order_info && ($order_info['current_status_id'] == $CFG->charge_cc_at_id || $order_info['current_status_id'] == $CFG->on_hold_status_id || $order_info['current_status_id'] == $CFG->awaiting_fraud_screening_status_id))
		  	{
		  		$open_items = Orders::getNotOnPO($order_id1);
		  		if (!$open_items)
		  		{
		  			Orders::update ( $order_id1, array ('current_status_id' => $CFG->processing_order_status_id) );
		  		}
		  	}
		  	}
		  	echo  "PO successfully sent via EDI";?>
		  	<br/>
		  	<input type="button" id="" title="Go back to the overview screen" value="&laquo; Go Back" onclick="window.location.href='/edit/purchase_orders.php?action=view&id=<?=$po_id?>';"/>
		  	<? 						
		}	
    }
}
	