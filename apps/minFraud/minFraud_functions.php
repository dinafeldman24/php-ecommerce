<?php
require("CreditCardFraudDetection.php");

// Set inputs and store them in a hash
// See http://www.maxmind.com/app/ccv for more details on the input fields

function sendDataForFraudCheck($order_id, $update_order_status = false)
{
	global $CFG;
	$h = array();
	$h["license_key"] = $CFG->maxMindLicenseKey;
	$order_info = Orders::get1($order_id);
	
	// Required fields
	$h["i"] = $order_info['ip_address'];             // set the client ip address
	$h["city"] = $order_info['billing_city'];             // set the billing city
	$h["region"] = $order_info['billing_state'];                 // set the billing state
	$h["postal"] = $order_info['billing_zip'];              // set the billing zip code
	$h["country"] = ($order_info['billing_country'] ? $order_info['billing_country'] : 'US');                // set the billing country
	$h["shipAddr"] = $order_info['shipping_address1'];
	$h["shipCity"] = $order_info['shipping_city'];
	$h["shipRegion"] = $order_info['shipping_state'];
	$h["shipPostal"] = $order_info['shipping_zip'];
	$h["shipCountry"] = ($order_info['shipping_country'] ? $order_info['shipping_country'] : 'US');
	$h["domain"] = substr(strrchr($order_info['email'], "@"), 1);		// Email domain
	$h["emailMD5"] = md5(strtolower($order_info['email']));
	$h["custPhone"] = $order_info['billing_phone'];
	$h["sessionID"] = $order_info['session_id'];
	$h["user_agent"] = $order_info['user_agent'];
	$h["txnID"] = $order_info['id'];
	$h["order_amount"] = $order_info['order_total'];	
	$h["order_currency"] = "USD";

	if ($order_info['cvv_match'] == "M" || $order_info['cvv_match'] == "MATCH") $h["cvv_result"] = "Y";
	
	// "Translate" AMEX one to standard codes
	else if ($order_info['avs_address'] == "ADDRESS_ZIP_MATCH") $order_info['avs_address'] = "Y";
	else if ($order_info['avs_address'] == "NO_MATCH") $order_info['avs_address'] = "N";
	else if ($order_info['avs_address'] == "ZIP_MATCH") $order_info['avs_address'] = "Z";
	else if ($order_info['avs_address'] == "ADDRESS_MATCH") $order_info['avs_address'] = "A";
	
	$h["avs_result"] = $order_info['avs_address'];

	// Create a new CreditCardFraudDetection object
	$ccfs = new CreditCardFraudDetection;

	// 	If you want to disable Secure HTTPS or don't have Curl and OpenSSL installed
	// 	uncomment the next line
	// $ccfs->isSecure = 0;

	// set the timeout to be five seconds
	$ccfs->timeout = 10;

	// uncomment to turn on debugging
	//$ccfs->debug = 1;

	// next we set up the input hash
	$ccfs->input($h);

	// then we query the server
	$ccfs->query();

	// then we get the result from the server
	$output = $ccfs->output();

	// then finally we print out the result
	$outputkeys = array_keys($output);
	$numoutputkeys = count($output);
	$output_string = "AVS response: ".$order_info['avs_address']."\n";
	for ($i = 0; $i < $numoutputkeys; $i++) 
	{		
  		$key = $outputkeys[$i];
  		$value = $output[$key];
  		$output_string .= $key . " = " . $value . "\n";
	}
	$info['fraud_score'] = $output['riskScore'];
	if ($update_order_status && $output['riskScore'] > $CFG->fraudScoreFraudThreshold)
	{
		$info['current_status_id'] = $CFG->awaiting_fraud_screening_status_id;
		//mail("rachel@tigerchef.com", "New 'Order Received' order came in--Order # $order_id", "Please do fraud checks on order $order_id.");
	} 
	Orders::update ( $order_info['id'], $info );
	$note_info['order_id'] = $order_id;
	$note_info['fraud_score_notes'] = $output_string;
	FraudScoreResponses::insert ( $note_info );
	//mail("rachel@tigerchef.com", "minFraud output for order $order_id", $output_string);
	return $output['riskScore'];
}


?>