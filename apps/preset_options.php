<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   August 11 2015
*
*****************/

class PresetOptions {
    
    var $id = null;
    var $option_id;
    var $option_name;
    var $name;

    function PresetOptions() {}

    static function updateOrInsert($info, $id=null){
        if(!is_null($id) && !empty($id) && $id){
            $res = db_update('preset_options',$id, $info);
            return true;
        }
        else{
            $res = db_insert('preset_options', $info);
        }
        return $res;
    }
    
    static function delete($id){
        return db_delete('preset_options', $id);
    }
    
    static function get($id = 0, $option_id = 0){
        $sql = "SELECT p.id, option_id, p.name as preset_name, o.name as option_name FROM preset_options p
            JOIN options o ON p.option_id = o.id WHERE 1";
        if($id){
            $sql .= " AND p.id = " . (int)$id;
        }
        if($option_id){
            $sql .= " AND p.option_id = " . (int)$option_id;
        }
        $sql .= " ORDER BY p.name ";
        return db_query_array($sql);

    }

    static function get1($id){
        return db_get1(self::get($id));
    }

    static function getVals($id, $active_only = false){
        $sql = "SELECT pov.*, if(ov.options_id = 1, c.id, '') as color_id  FROM preset_option_values pov JOIN option_values ov ON
            pov.options_values_id = ov.id                  
            LEFT JOIN option_color_map c ON ov.value = c.id
            WHERE preset_id = " . 
            (int)$id;
        if($active_only){
            $sql .= " AND active = 1";
        }
        $sql .= " ORDER BY display_order";
        return db_query_array($sql);
    }

    static function getForOptionIdForDropDown($option_id){
        $presets = self::get(0, $option_id);
        $ret = array();
        foreach($presets as $curr){
            $ret[] = array('id' => $curr['id'], 'name' => $curr['preset_name']);
        }
        return $ret;
    }

}
?>
