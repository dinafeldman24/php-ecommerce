<?php

class Occasions {
	
	function insert($info)
	{		
		return db_insert('occasions',$info);
	}
	
	function update($id,$info)
	{
		return db_update('occasions',$id,$info);
	}
	
	function delete($id)
	{
		return db_delete('occasions',$id);
	}
	
	function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='',$is_active='',$prod_ids='',$is_major='')
	{
		if (is_array($prod_ids)) {
			$prod_join = ',product_occasions';
		}
		else {
			$prod_join = '';
		}
		
		$sql = "SELECT occasions.*
				FROM occasions
				$prod_join
				WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND occasions.id = $id ";
		}
		if ($begins_with != '') {
			$sql .= " AND occasions.name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') {
			$fields = Array('occasions.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if ($is_active === true || $is_active === false) {
			$sql .= " AND occasions.is_active = '".($is_active ? 'Y' : 'N')."' ";
		}
		if ($is_major === true || $is_major === false) {
			$sql .= " AND occasions.is_major = '".($is_major ? 'Y' : 'N')."' ";
		}
		
		if (is_array($prod_ids)) {
			$sql .= " AND product_occasions.occasion_id = occasions.id AND product_occasions.product_id IN (".implode(',',$prod_ids).") ";
		}
		
		$sql .= " GROUP BY occasions.id
				  ORDER BY ";
		
		if ($order == '') {
			$sql .= 'occasions.name';
		}
		else {
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}
		
		//echo $sql;
		
		return db_query_array($sql);
	}
	
	function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Occasions::get($id);
		return $result[0];
	}
}

?>