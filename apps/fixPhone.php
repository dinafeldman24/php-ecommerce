<?php 

class FixPhone {
	static function correctPhone($phone){
		
		$phone = preg_replace( "/(#|extension|x|ext)/", "x", $phone ); 
		
		if( strlen($phone) > 10 ){
			if( $phone[0] == 1 ){
				//strip the leading 1
				$phone= substr( $phone, 1 );
			}
		}
		$phone = preg_replace( "/[^\+0-9x]/", "", $phone ); 
		return $phone;
	}
	
	static function displayPhone($phone){ 
		if (!(strpos($phone,'x') === false)){
			list($phone,$extension) = explode('x',$phone);
		}
		
		if (!(strpos($phone,'+') === false)){
			$country_code = '+';
		}
		
		$phone = preg_replace( "/[^0-9x]/", "", $phone );
		
		$phone = $country_code . "(" . substr($phone, 0 , 3) . ") " . substr($phone, 3 , 3) . "-" . substr($phone, 6 ) ; 
		
		if($extension){
			$phone .= " x " . $extension ;
		}
		
		return $phone;
	}

}



