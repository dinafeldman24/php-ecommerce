<?php

class Checkout
{
	var $passed_validation  = true;
	var $passed_validation_billing  = true;
	var $passed_validation_shipping  = true;
	
	var $validation         = array();
	var $err_msgs           = array();
	var $invalid_fields;
	var $text_to_print 		= '';
		
	var $sales_tax          = 0.00;
	var $tax_rate           = 0.00;
	var $shipping_cost      = 0.00;
	var $sub_total          = 0.00;
	var $grand_total        = 0.00;
	var $liftgate           = 0.00;
	var $min_charge			= 0.00;
	var $coupon_gift        = 0.00;
	var $shipping_discount  = 0.00;
	var $shipping_cost_options = array();
	var $cc_transaction_ok  = true;
	
	var $paypal_token 		= '';
	var $paypal_ec_obj		= null;
	var $paypal_address_status		= false;
	
	var $paypal_checkout = false;
	
	var $cart2;
	var $cart3;
	
	function __construct($paypal=false,$info=null) {
		
		global $cart;
		global $CFG;

		/* this is just the session data stored from the previous pages */
		if($_SESSION['CART2']){
			$this->cart2 = unserialize($_SESSION['CART2']);
		}else{
			$this->cart2 = array();
		}		
		$this->cart3 = unserialize($_SESSION['CART3']);
		//$cr = db_get1($cart->getCartReference());
		//if($cr['cart_reference'] == '6B20D369'){mail('tova@tigerchef.com','apps/checkout.php',var_export($cart->data,true));}

		if(!$this->cart2['shipping_zip'] && $cart->data['zipcode']){
			$this->cart2['shipping_zip'] = $cart->data['zipcode'];
		}
		if(!$this->cart2['shipping_location'] && $cart->data['shipping_location']){
			$this->cart2['shipping_location'] = $cart->data['shipping_location'];
		}				
	
		if($paypal){
			$this->paypal_checkout = true;
			$this->get_paypal_transaction_info($info);
			$this->cart2 = unserialize($_SESSION['CART2']);
		}
		
   }
	
    function insertOrder(&$cart, $order_obj)
    {
        global $CFG;
        global $ses_class;
		global $debug;
$debug['place_order8.3.0'] = time();
        $type                   = 'UPS';
        $cart_info              = $cart->get();
        $trucker_id             = 0;
        $is_repeat              = 'N';
        $cust_id                = 0;
        $zipcode                = 0;
        $grand_total            = 0.0;
        $last_package_id        = 0;
        $cart_package_ids[0]    = 0;
        $order_id               = 0;
        $billing                = $order_obj->billing;
        $shipping               = $order_obj->shipping;
$debug['place_order8.3.0.1'] = time();
        /*
        ** HANDLE FREIGHT
        */
        if (!empty($cart_info))
        {
            foreach ($cart_info as $item)
            {
                if($item['shipping_type'] == 'Freight')
                {
			$type = 'Freight';
			break;
		}
            }
        }
		// section commented out by RSunness Apr '11 because db tables for truckers & freight are not in place
        if($type == 'Freight')
        {
        	$type = '';
/*			$zipcode = $shipping['zip'];

			if(!$zipcode)
			{
				$type = '';
			}
			else
			{
				$trucker_id = Truckers::getTruckerByZipcode($zipcode);

				if(!$trucker_id)
				{
					$type = '';
				}

				$trucker = Truckers::get1($trucker_id);
				$type = $trucker['trucker_code'];
			}
			*/
		}

		/*
        ** INSERT CUSTOMER
        */
	if( !$_SESSION['cust_acc_id'] )
        {
            //$billing['password'] = RandomPassword::getRandomPassword();
	    //$cust_id = Customers::insert($billing, 'phone');
	    $cust_id = 0;
	    // commented out by RSunness Dec '11 because shouldn't log you in 
	    // if you just place an order without logging in
	    //$_SESSION['cust_acc_id'] = $cust_id;
        }

		//removing because it takes a long time
        //$is_repeat = Orders::get1ByEmail($billing['email']);
		if($is_repeat === true)
		{
			$is_repeat = 'Y';
		}
		else
		{
			$is_repeat = 'N';
		}

		 if(is_array($order_obj->prods_to_wrap))
		 {
		    $order_obj->gift_wrapped = 'Y';
		 }
		 
		if($_SESSION['cust_acc_id']>0 || $cust_id>0){ 
		    $total_earned_rewards = round(($order_obj->sub_total - $order_obj->discount - $order_obj->reward_points_used) * $CFG->rewards_pts_awarded_per_dollar) ;
		}
		
		
		/*if (
			strtoupper($billing['address1']) != strtoupper($shipping['address1']) ||
			strtoupper($billing['city']) != strtoupper($shipping['city']) ||
			strtoupper($billing['state']) != strtoupper($shipping['state']) ||
			strtoupper($billing['zip']) != strtoupper($shipping['zip']) 
		)
		{
			$status_for_order = $CFG->awaiting_fraud_screening_status_id;
		}
		else */$status_for_order = $CFG->new_order_status_id;
		
		if($order_obj->paypal_address_status){
			if ($order_obj->paypal_address_status == 'Confirmed'){
				$status_for_order = $CFG->new_order_status_id;
			}else{
				$status_for_order = $CFG->new_order_status_id;
			}
		}
		
		if($CFG->tc_emerchant_use_live == 'No' || $CFG->paypal_environment == "sandbox"){
			$status_for_order = $CFG->test_order_status_id;
		}		
		$debug['place_order8.3.1'] = time();				
		$order_info = array
		(
		  'customer_id'         => $_SESSION['cust_acc_id'] ? $_SESSION['cust_acc_id'] : $cust_id,
		  'billing_first_name'  => $billing['first_name'],
		  'billing_last_name'   => $billing['last_name'],
		  'billing_address1'    => $billing['address1'],
		  'billing_address2'    => $billing['address2'],
		  'billing_city'        => $billing['city'],
		  'billing_state'       => $billing['state'],
		  'billing_zip'         => $billing['zip'],
		  'billing_phone'     	=> $billing['phone'],
		  'billing_company'	=> $billing['company'],		  
		  'shipping_first_name' => $shipping['first_name'],
		  'shipping_last_name'  => $shipping['last_name'],
		  'shipping_address1'   => $shipping['address1'],
		  'shipping_address2'   => $shipping['address2'],
		  'shipping_city'       => $shipping['city'],
		  'shipping_state'      => $shipping['state'],
		  'shipping_zip'        => $shipping['zip'],
		  'shipping_phone'    	=> $shipping['phone'],
		  'shipping_company'	=> $shipping['company'],
		  'email'               => $billing['email'],
		  'location_type'	=> $shipping['location_type'],
		  'recommended_shipper' => $type,
		  'transaction_id'      => $order_obj->transaction_id,
		  'avs_address'         => $order_obj->avs_address,
		  'avs_zip'             => $order_obj->avs_zip,
		  'subtotal'            => $order_obj->sub_total,
		  'promo_discount'      => $order_obj->discount,
		  'reward_points_used'  => $order_obj->reward_points_used,
		  'rewards_pts_awarded_per_dollar' => $CFG->rewards_pts_awarded_per_dollar,
		  'liftgate_fee' 		=> $order_obj->liftgate_fee,
		  'liftgate_required' 	=> $order_obj->liftgate_required,
		  'tax_rate'            => $order_obj->tax_rate,
		  'is_repeat_order'     => $is_repeat,
		  'shipping'            => $order_obj->shipping_cost,
		  'current_status_id'   => $status_for_order,
		  'card_type'           => $order_obj->cc_type,
		  'order_tax_option'    => 1,
		  'last4'               => $order_obj->last4,
		  'cvv_match'			=> $order_obj->cvv_match,
		  'iavs'				=> $order_obj->iavs,
		  'notes'               => $order_obj->notes,
		  'gift_wrap_message'   => $order_obj->gift_wrap_msg,
		  'gift_wrapped'        => $order_obj->gift_wrapped,
		  'min_charges'			=> $order_obj->min_charges,
		  'sales_tax'			=> $order_obj->sales_tax,
		  'shipping_discount'	=> $order_obj->shipping_discount,
		  'http_referrer'		=> $ses_class->getReferrer(session_id()),
		  'payment_type'		=> $order_obj->payment_type
		);

		/**
		 * Set shipping method
		 */
		$method = db_get1( ShippingMethods::get('','','',$cart->data['shipping_method']) );
		$order_info['shipping_id'] = $method['id'];
		if($cart->data['shipping_method_2']){
			$method_2 = db_get1( ShippingMethods::get('','','',$cart->data['shipping_method_2']) );
			$order_info['shipping_id_2'] = $method_2['id'];
		}

		$debug['place_order8.3.2'] = time();
		/*
		** INSERT ORDER
		*/
        $order_id = Orders::insert($order_info);
		$debug['place_order8.3.3'] = time();
         /* PROMOS */
        $promos = $cart->getPromoCodes();
		$debug['place_order8.3.4'] = time();

        if ($promos) foreach($promos as $p) {
            //PromoCodes::useCoupon($p['promo_code_id']);
          	Orders::insertOrderPromoCode(array(
				'promo_code_id' => $p[promo_code_id],
				'order_id' => $order_id,
				'description'=>$p['promo_code_description'])
			);
        }
	$debug['place_order8.3.5'] = time();
	/* REDEEMED REWARDS */
	if($order_obj->reward_points_used > 0){
	    $reward_info = Array
                (
                    'customer_id'  => $_SESSION['cust_acc_id'] ? $_SESSION['cust_acc_id'] : $cust_id,
                    'points_qty'   => -($order_obj->reward_points_used * 100),
                    'action_type'  => 'redeemed',
                    'action'       => 'order',
                    'date'         => date('Y-m-d H:i:s'),
		    'order_id'     => $order_id
                );
	    			 		
	    Rewards::insertReward($reward_info);
	}

        /*
        ** INSERT ORDER ITEMS
        */
	//$total_earned_rewards = 0 ;
	$debug['place_order8.3.6'] = time();
	$temp_i=0;
        foreach ($cart_info as $cart_line)
        {
			$temp_i++;
			$debug['place_order8.3.6.0'][$temp_i][1] = time();
            if ($cart_line['package_uid'] != $last_package_id && $cart_line['package_uid'] > 0) {
                $package_line = $cart->getPackageLine($cart_line['package_uid']);
                $last_package_id = $cart_line['package_uid'];

                $package_info = Array(
                    'order_id' => $order_id,
                    'package_id' => $package_line['package_id'],
                    'price' => $package_line['price'],
                    'qty' => $package_line['qty']
                );

                $cart_package_ids[$cart_line['package_uid']] = Orders::insertPackage($package_info);
            }
	    $debug['place_order8.3.6.0'][$temp_i][2] = time();

	//	TE removed section - rewards are no longer calculated per product
	//    /* CALCULATE EARNED REWARDS FOR PRODUCT */
	//
	//    //calculate cost
	//    $cost_per_item = 0;
	//    if(is_array($cart_line['options'])){
	//	    $cost_per_item = ($cart_line['options'][0]['additional_price']>0) ? $cart_line['options'][0]['additional_price'] : $cart_line['cart_price'];
	//    } else {
	//	$cost_per_item = $cart_line['cart_price'];
	//    }
	//    $earned_rewards = Rewards::calculateEarnedRewardsForProduct($cart_line['product_id'],$cost_per_item * $cart_line['qty']);
	//    $total_earned_rewards += $earned_rewards;

            /*
            *********************************************************************
            ** HANDLE PRODUCTS IN INVENTORY AND PRODUCTS THAT NEED TO HAVE A
            ** PO MADE FOR THEM
            *********************************************************************
            */
            $delivery_method            = '';
            $total_qty_needed           = $cart_line['qty'];
            $purchase_order_qty         = 0;
            $stock_qty                  = 0;
            $all_stock                  = false;
            $order_with_po_and_stock    = false;

            /*
            ** INVENTORY
            */
            $inventory = Inventory::get(0,0,0,'','',$cart_line['product_id'],0,0,1,false,'','',0,'inventory');
            if (is_array($inventory) && $inventory[0]['qty'] > 0)
            {
                if ($total_qty_needed > $inventory[0]['qty'])
                {
                    $all_stock                  = false;
                    $stock_qty                  = $inventory[0]['qty'];
                    $purchase_order_qty         = $total_qty_needed - $stock_qty;
                    $order_with_po_and_stock    = true;
                }
                else
                {
                    $all_stock                  = true;
                    //$stock_qty                  = $inventory[0]['qty'];
		    $stock_qty			= $total_qty_needed;
                    $order_with_po_and_stock    = false;
                }

$debug['place_order8.3.6.0'][$temp_i][3] = time();
		Inventory::addOnOrder( $inventory[0]['id'], $stock_qty );
$debug['place_order8.3.6.0'][$temp_i][4] = time();
                /* insert an order item for the stock that is available */
                $line_info = array
                (
                    'order_id'          => $order_id,
                    'product_id'        => $cart_line['product_id'],
                    'price'             => $cart_line['cart_price'],
                    'qty'               => $stock_qty,
                    'delivery_method'   => 'stock',
                    'order_item_status_id' => $CFG->item_in_stock_status,
                	'is_gift' 			=> $cart_line['is_gift'],
					'item_shipping_type' => in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])?'Freight':'Standard'
		    //'reward_points_earned' => $earned_rewards
                );

                /* determine if this is gift-wrapped */
                if (is_array($order_obj->prods_to_wrap) && in_array($cart_line['product_id'], $order_obj->prods_to_wrap))
                {
                    $line_info['gift_wrapping_id'] = $order_obj->gift_wrapping_id;
                }
                $line_id = Orders::insertItem($line_info);
$debug['place_order8.3.6.0'][$temp_i][5] = time();
		if (is_array($cart_line['options'])) {
		    foreach ($cart_line['options'] as $option) {
//mail('hgolov@gmail.com', 'Cart line with optoins' , var_export($cart_line, 1));
//debuging wrong product option going into order
$product_options = db_query_array('select product_id from product_options where id = ' . $option['product_option_id']);
$product_option_id = $product_options[0]['product_id'];
if($product_option_id <> $cart_line['product_id']){
    $message = 'The cart line: ' . var_export($cart_line, 1) . 
        PHP_EOL . ' and the session: ' . var_export($_SESSION, 1);
    //mail('hgolov@gmail.com','Mismatch prod id between item and optoin', $message);
}
			$option_info = Array(
			    'order_item_id' => $line_id,
			    'product_option_id' => $option['product_option_id'],
			    'value' => Checkout::moveUserFile($option['user_input']),
			    'additional_price' => $option['additional_price'],
                'optional_options' => $option['available_options_id'],
                'preset_value_ids' => $option['preset_value_ids'],
    			);

            $message .= 'about to insert: '. var_export($option_info, 1) . PHP_EOL;
			$item_option_id = Orders::insertItemOption($option_info);
		    }
		}

		$debug['place_order8.3.6.0'][$temp_i][6] = time();
            }

            /*
            ** ALL OTHERS
            */
            if (!$all_stock)
            {
                /*
                ** just make sure that we don't do anything with the gift-wrap product it should default to this section
                ** since it won't be in inventory but we still want to include it as an item in the order for them
                ** to process
                */
                if ($CFG->gift_wrap_product_id != $cart_line['product_id'])
                {
                    $delivery_method    = 'N/A';
                    $product_suppliers  = ProductSuppliers::get($cart_line['product_id']);

                    if (is_array($product_suppliers))
                    {
                        $delivery_method = $product_suppliers[0]['delivery_method'];
                    }
                }
                else
                {
                    $delivery_method = '';
                }

                /* build the order item */
                $line_info = Array
                (
                    'order_id'          => $order_id,
                    'product_id'        => $cart_line['product_id'],
                    'price'             => $cart_line['cart_price'],
                    'qty'               => (!$order_with_po_and_stock) ? $cart_line['qty'] : $purchase_order_qty,
                    'delivery_method'   => $delivery_method,
                	'is_gift' 			=> $cart_line['is_gift'],
					'item_shipping_type' => in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])?'Freight':'Standard'
		    //'reward_points_earned' => $earned_rewards
                );

                if($delivery_method == 'dropship'){
                	$line_info['order_item_status_id'] = $CFG->item_drop_ship_status;
                } else if($delivery_method == 'pickup'){
                	$line_info['order_item_status_id'] = $CFG->item_in_stock_status;
                }

                /* lets see if we need to add the gift wrapping option to the order_item row we are just matching this
                   against the selection of products they chose to wrap */
                if (is_array($order_obj->prods_to_wrap) && in_array($cart_line['product_id'], $order_obj->prods_to_wrap))
                {
                    $line_info['gift_wrapping_id'] = $order_obj->gift_wrapping_id;
                }

                $line_id = Orders::insertItem($line_info);

		/*
		** OPTIONS
		*/
		if (is_array($cart_line['options'])) {
//mail('hgolov@gmail.com', 'Cart line with optoins' , var_export($cart_line, 1));
		    foreach ($cart_line['options'] as $option) {
			$option_info = Array(
			    'order_item_id' => $line_id,
			    'product_option_id' => $option['product_option_id'],
		    'additional_price' => $option['additional_price'],
                'optional_options' => $option['available_options_id'],
            );
            if($option['user_input']){

                $option_info['value'] = Checkout::moveUserFile($option['user_input']);
            }
            if(!empty($option['preset_value_ids'])){
                $option_info['preset_value_ids'] = $option['preset_value_ids'];
               }
			Orders::insertItemOption($option_info);
		    }
		}

            }
$debug['place_order8.3.6.0'][$temp_i][7] = time();
     		if($cart_line['cart_price'] == '0.00') mail("rachel@tigerchef.com, estee@tigerchef.com", "Free Item ".$cart_line['product_name']." purchased on order ".$order_id,"");   
        }
		$debug['place_order8.3.7'] = time();
		if($CFG->tc_emerchant_use_live != 'No'){
	require_once($CFG->dirroot . "/apps/minFraud/minFraud_functions.php"); 
		sendDataForFraudCheck ( $order_id, true );
		}
		$debug['place_order8.3.8'] = time();
		$debug['breakdown']['fraud_check'] = $debug['place_order8.3.8'] - $debug['place_order8.3.7'];
	if($_SESSION['cust_acc_id']>0 || $cust_id>0){
	    /* EARNED REWARDS */ 
	    if($total_earned_rewards > 0){
		$reward_info = Array
		    (
			'customer_id'  => $_SESSION['cust_acc_id'] ? $_SESSION['cust_acc_id'] : $cust_id,
			'points_qty'   => $total_earned_rewards,
			'action_type'  => 'earned',
			'action'       => 'order',
			'date'         => date('Y-m-d H:i:s'),
			'order_id'     => $order_id,
			'active'       => 'N'
		    );
						    
		Rewards::insertReward($reward_info);
		
		$rewards_for_email = $total_earned_rewards;
	    }
	}
$debug['place_order8.3.9'] = time();
		/*
		 * E-mail Customer a Receipt of the Order!!!
		 */

		$vars['order_id'] = $order_id;
		$vars['full_name'] = $billing['first_name'] . ' ' . $billing['last_name'];
		if($rewards_for_email > 0){
		    
		$vars ['earned_rewards'] = '<tr><td style="padding:15px;" align="center">
				    <p>You earned ' . $rewards_for_email . ' points for this order! <br />
				    <small>Reward points are issued 45 days after placing an order.</small></p></td></tr>';
		
		} else {
		    $vars ['earned_rewards'] = '';
		}
		$new_acct_pwd = isset($billing['password']) ? $billing['password'] : '';

		//if( !$CFG->in_testing ) $bcc_email = "pk1kxc@service.yotpo.com";
		/*if( !$CFG->in_testing ) $bcc_email = "bits.roirevolution+tiger_chef@gmail.com";
 		else */$bcc_email = "";
 		
		$E = TigerEmail::sendOne($billing['first_name'] . ' ' . $billing['last_name'],
                                    $billing['email'], 'order_receipt', $vars, '',
                                    true, '', '', $new_acct_pwd, $bcc_email);

		$E = TigerEmail::sendOne($billing['first_name'] . ' ' . $billing['last_name'],
                                    $CFG->orders_email, 'order_receipt', $vars, 'COPY: Order Confirmation',
                                    true, '', '', $new_acct_pwd);

		/*$E = TigerEmail::sendOne($billing['first_name'] . ' ' . $billing['last_name'],
                                    'jay@tigerchef.com', 'order_receipt', $vars, 'COPY: Order Confirmation',
                                    true, '', '', $new_acct_pwd);*/
		
        $E = TigerEmail::sendOne($billing['first_name'] . ' ' . $billing['last_name'],
 	                               'ezra@tigerchef.com', 'order_receipt', $vars, 'COPY: Order Confirmation',
                                    true, '', '', $new_acct_pwd);                                               
                                                  
		//foreach ($cart_info as $item){
		//	$product = Products::get1( $item['product_id'] );
		//	$plink = Catalog::makeProductLink_($product);
		//}
		$debug['place_order8.3.10'] = time();
     
        return $order_id;
    }
    
    static function moveUserFile($user_input){
        global $CFG;
        $user_input_array = json_decode($user_input, 1);
        foreach($user_input_array as $product_available_option_id => $info){
            if($info[0] == 'file'){
                $tmp_file = $info[1]; 
                $old_file =  $CFG->cart_upload_dir . '/' . $tmp_file;
                $new_file = $CFG->order_upload_dir . '/' . $tmp_file;
                mail('hgolov@gmail.com', 'User file upload', 'Orig: ' . $tmp_file . ' and new: ' .$new_file);
                //make sure it exists
                if(file_exists($old_file)){
                    //make sure the target doesn't - it should never happen, but you never know
                    $i = 0;
                    while(file_exists($new_file)){
                        $new_file = $CFG->order_upload_dir . '/' . $i++ . '_' . $tmp_file;
                    }
                    if(!rename($old_file, $new_file)){
                       $rename_error = error_get_last();
                       $message =' could not rename ' . $old_file .' to ' . $new_file. PHP_EOL  ;
                       $message .= PHP_EOL . ' and the perms: old file: ' . fileperms($old_file) . ' and the upload dir: ' . fileperms($CFG->order_upload_dir)
                        .PHP_EOL . 'The error:' . var_export($rename_error, 1);
                       if(!copy($old_file, $new_file)){
                            $copy_error = error_get_last();
                            $message .= PHP_EOL . 'could not copy either, error: ' . var_export($copy_error, 1) ;
                       }
                      else{
                        $message .= 'was ablt to copy';
                      }
                      mail('hgolov@gmail.com','Could not move user upload file', $message);

                    }
                    else{
                        chmod($new_file, 0666);
                        unlink($old_file);
                    }
                }
                else{
                    mail('hgolov@gmail.com', 'Missing uploaded file', var_export($user_input_array, 1) . PHP_EOL . ' missing: ' . $old_file);
                }
                $user_input_array[$product_available_option_id][1] = str_replace($CFG->order_upload_dir, '', $new_file);
            }
        }
        return json_encode($user_input_array);
    }
    function populate_address_fields()
    {
    	$shipping_address   = null;
    	$billing_address    = null;
    
    	/* since i didn't follow protocol when I started by using the info[fieldname] scheme, I have to monkey-*** this
    	 and store in the $_REQUEST */
    	$shipping_fields_to_store = array
    	(
    			'shipping_fname'        => 'first_name',
    			'shipping_lname'        => 'last_name',
    			'shipping_company'      => 'company',
    			'shipping_address'      => 'address1',
    			'shipping_address2'     => 'address2',
    			'shipping_city'         => 'city',
    			'shipping_state'        => 'state',
    			'shipping_zip'          => 'zip',
    			'shipping_home_phone'   => 'phone',
    			'shipping_location'	    => 'location_type'
    	);
    
    	$billing_fields_to_store = array
    	(
    			'billing_fname'         => 'first_name',
    			'billing_lname'         => 'last_name',
    			'billing_company'       => 'company',
    			'billing_address'       => 'address1',
    			'billing_address2'      => 'address2',
    			'billing_city'          => 'city',
    			'billing_state'         => 'state',
    			'billing_zip'           => 'zip',
    			'billing_home_phone'    => 'phone'
    	);
    
    	/* now populate the $_REQUEST variable with these values if we have information from the db */
    	$shipping_address   = Customers::getPrimaryShippingAddress($_SESSION['cust_acc_id']);
    	$billing_address    = Customers::getPrimaryBillingAddress($_SESSION['cust_acc_id']);
    
    	if (is_array($shipping_address))
    	{
    		foreach ($shipping_fields_to_store as $field_name => $mapped_field_name)
    		{
    			if ($field_name == "shipping_location") $_REQUEST[$field_name] = strtolower($shipping_address[0][$mapped_field_name]);
    			else $_REQUEST[$field_name] = $shipping_address[0][$mapped_field_name];
    		}
    	}
    
    	if (is_array($billing_address))
    	{
    		foreach ($billing_fields_to_store as $field_name => $mapped_field_name)
    		{
    			$_REQUEST[$field_name] = $billing_address[0][$mapped_field_name];
    		}
    	}
    
    	/* set the email */
    	if(empty($_REQUEST['shipping_email'])){
    		$_REQUEST['shipping_email'] = $_SESSION['cust_username'];
    	}
    
    	return true;
    } 
    
    function get_paypal_transaction_info($info,$ajax = false){
    	global $CFG;
    	global $cart;
    	
    	if($info){
    	 
	    	$this->paypal_ec_obj = new PaypalCc();
	    	
	    	foreach ($info as $key => $field){
	    		$info[$key] = htmlentities($field);
	    	}
	    	    	 
	    	$this->paypal_ec_obj->setTOKEN($info['token']); 
	    	$this->paypal_token = $info['token'];
	    	$checkout_details = $this->paypal_ec_obj->get_express_checkout_details();    	 
	    	
			//check if it was a sandbox order
			if ($checkout_details == "failure") {
				//save original error message
				$original_error = $this->paypal_ec_obj->getL_LONGMESSAGE0();

				if($this->paypal_ec_obj->getL_ERRORCODE0() == 10411){

					//try changing to sandbox
					$this->paypal_ec_obj->set_up_environment('sandbox');
					$checkout_details = $this->paypal_ec_obj->get_express_checkout_details();

					//if it still doesn't work, change it back to live environment
					if ($checkout_details == "failure") {
						//change back
						$this->paypal_ec_obj->set_up_environment('live');
					}
				}
			}
			
if ($checkout_details == "failure")
	    	{
	    		
	    		$err_msgs = sprintf('<span>%s</span>', $original_error);
	    		if($ajax){
	    			return array('error'=>$err_msgs);
	    		}else{
	    			echo '<div class="errorBox">'.$err_msgs .'</div>';
	    			return false;
	    		}
	    	} else {
	    		$cart->data['paypal_ec_token'] = $this->paypal_ec_obj->getTOKEN();
	    		$cart->data['paypal_ec_payerid'] = $info['PayerID'];
	    		$this->paypal_ec_obj->setPAYERID($info['PayerID']);
	    		
	    		//options are none/Confirmed/Unconfirmed
	    		$this->paypal_address_status = $checkout_details['PAYMENTREQUEST_0_ADDRESSSTATUS'];
	    		
	    		//$selected_shipping_methods = explode('/',$checkout_details['SHIPPINGOPTIONNAME']);
	    		
	    		//$shipping_method_info = ShippingMethods::get(0,'','','','','',trim($selected_shipping_methods[0])) ;   		
	    		
	    		//if($shipping_method_info){
	    		//	$cart->data['shipping_method'] = $shipping_method_info[0]['code'];

	    		//}
	    		
	    		//if($shipping_method_info[1]){
	    		//	$cart->data['shipping_method_2'] = $CFG->freight_shipping_code;

	    		//}
	    		
	    		$cart2 = array();
	    		
	    		$cart2['shipping_fname'] = $checkout_details['PAYMENTREQUEST_0_SHIPTONAME'];
	    		$cart2['shipping_address'] = $checkout_details['PAYMENTREQUEST_0_SHIPTOSTREET'];
	    		$cart2['shipping_address2'] = $checkout_details['PAYMENTREQUEST_0_SHIPTOSTREET2'];
	    		$cart2['shipping_city'] = $checkout_details['PAYMENTREQUEST_0_SHIPTOCITY'];
	    		$cart2['shipping_state'] = $checkout_details['PAYMENTREQUEST_0_SHIPTOSTATE'];
	    		$cart2['shipping_zip'] = $checkout_details['PAYMENTREQUEST_0_SHIPTOZIP'];
	    		$cart2['shipping_home_phone'] = $checkout_details['PHONENUM'] ? $checkout_details['PHONENUM'] : $checkout_details['PAYMENTREQUEST_0_SHIPTOPHONENUM'];
	    		$cart2['shipping_email'] = $checkout_details['EMAIL']; 
	    		
	    		$cart2['billing_fname'] = $checkout_details['FIRSTNAME'];
	    		$cart2['billing_lname'] = $checkout_details['LASTNAME'];
	    		$cart2['billing_home_phone'] = $cart2['shipping_home_phone'];
				
				$cart->data['zipcode'] = $cart2['shipping_zip'];
	    		
	//     		if($checkout_details['SHIPPINGCALCULATIONMODE'] == 'FlatRate'){
	//     			// use the shipping rates returned by pp since the callback failed  	
	
	//     			$this->used_flat_rate = true;
	//     			$this->override['shipping'] = $checkout_details['PAYMENTREQUEST_0_SHIPPINGAMT'];
	    			 
	//     			//only charging tax to NY
	// //     			if( $cart2['shipping_state'] == 'NY'){
	// //     				$cart->data['override']['tax'] = $checkout_details['PAYMENTREQUEST_0_TAXAMT'];
	// //     			} else {
	// //     				$cart->data['override']['tax'] = 0.00;
	// //     			}
	//     		}
	//     		else{
	//     			$this->used_flat_rate = false;
	//     			$this->override = null;
	//     		}
				//$this->override['shipping'] = $checkout_details['PAYMENTREQUEST_0_SHIPPINGAMT'];
	    		
	    		$_SESSION['CART2'] = serialize($cart2);
	    		$_SESSION['CART2'] = serialize($cart2);
	    		
	    		//$cart->data['special_comments'] = $checkout_details['PAYMENTREQUEST_0_NOTETEXT'];
	    	}
	    	return true;
    	}
    } 
    
    function show_yomtov_message(){
    	global $CFG;
    	if($CFG->yom_tov_checkout_message && time() < strtotime($CFG->yom_tov_message_end_date) && time() > strtotime($CFG->yom_tov_message_start_date))
    	{
    		?>
        	<div class="holiday_msg">
        		<?=$CFG->yom_tov_checkout_message?>
        	</div>
        	<?
        }
    }
    
    /**
     * Calculates all the costs for the order
     * and returns the cost breakdown table
     * @return string
     */
    function create_cost_breakdown_table(){
		
		global $cart;
		
		$this->calculate_all();

		return $cart->create_cost_breakdown_table($this);
    }
    
    
	function display_cart_items($mobile = false, &$item_count=0, &$subtotal=0)
    { 
    	global $cart;
    	global $CFG;
    	$items = $cart->get();
    	 
    	$this_option = $this_product = $prev_option = $this_optional = $prev_optional = $prev_product = $prev_cart_item = $prev_b_type = "";
    	
			$show_title = false; 
    		$b_reg_function = array($cart, "showProductLineB_checkout_charger");
			$b_case_function = array($cart, "showProductLineBCase_checkout_charger");
			$a_function = array($cart, "showProductLineA_checkout_charger");    
    	

    	ob_start();
    	include($CFG->dirroot . "/includes/case_cart_include.php");
    	    	
    	return  ob_get_clean();
    }
    
    function create_signin(){
    	ob_start();
		if(!$_SESSION['cust_acc_id']){
			$customer = Customers::getIdByEmail($this->cart2['shipping_email']);
		
			// If this email belongs to a customer, give them the option to login
			if($customer){
				?>
				<div class="sign-up-checkout-end">
					Looks like we have an account for <?php echo $this->cart2['shipping_email'];?>. 
					You can <a href="#" onclick="$j('#signin_box').removeClass('hidden');return false;">sign in</a> or use the confirm order button to place your order without logging in.
				</div>
				<div class="account-row hidden" id="signin_box">
					<div class="label-holder">
						<div class="label-box">
							<label for="account_step_password">Password: </label>
						</div>
					</div>
					<div class="field-holder">
						<input class="text" name="account_step_password"
							id="account_step_password" value="" type="password" />
							<div>We will sign you in when you confirm your order.</div>
					</div>
					<div>
						<a class="forget-link" href="#" onclick="TINY.box.show({url:'popuplogin.php',post:'action=forgot_password&link=<?=$_SERVER['PHP_SELF']?>&request=<?=$_SERVER['PHP_SELF']?>&email=<?=$this->cart2['shipping_email']?>',opacity:20,topsplit:3});return false;">Forgot your password?</a>	
					</div>
				</div>
				<?php 
			}else{ // give them the option to create an account								
				?>
				<div class="sign-up-checkout-end">
					<div class="account-row">
						<label class="email_offer"
								onclick="if($j('#create_account').prop('checked')){$j('#pass_box').removeClass('hidden');}else{$j('#pass_box').addClass('hidden');$j('#account_step_password').val('');$j('#account_step_password_conf').val('');}">
								<div class="coc-account"><input class="create_account" name="create_account"
								id="create_account" type="checkbox" /> 
								create an account now and save my info for future visits. (optional)</div>
						</label>
					</div>
				</div>
				<div class="account-row hidden" id="pass_box">
				<div class="label-holder">
					<div class="label-box">
						<label for="account_step_password">Password: </label>
					</div>
				</div>
				<div class="field-holder">
					<input class="text" name="account_step_password"
						id="account_step_password" value="" type="password" />
					
				</div>
				<div class="label-holder">
					<div class="label-box">
						<label for="account_step_password_conf">Confirm Password: </label>
					</div>
				</div>
				<div class="field-holder">
					<input class="text" name="account_step_password_conf"
						id="account_step_password_conf" value="" type="password" />
				</div>
			</div>
			<?php 
			}
		}
		return  ob_get_clean();
    }
    
    function get_shipping_info($zip_code){
    
    	return Shipping::getShippingOptions($zip_code,'cart',$this->paypal_token);
    	
    }
    
    function calculate_sub_total()
    {
    	global $cart;
    	
    	$this->sub_total = $cart->getCartTotal(true,true,"","",'', $this->paypal_token);
    }
    
	function calculate_coupon_gift()
    {
    	global $cart;
    
    	/* Promo/Coupon Code */
    	$coupon_gift_arr = $cart->discountCalc($this->shipping_cost);
    	$coupon_gift_arr['shipping_discount'] = $this->shipping_discount;
    	$this->coupon_gift_arr = $coupon_gift_arr;

    	$this->coupon_gift = $coupon_gift_arr['overall_discount'];
    	$this->coupon_gift = round($this->coupon_gift, 2);
    	if (session_id() == "4bhi3l0aklloq3nsip6a4abft6")
    	{
    		$output = print_r($coupon_gift_arr, true);
    		//mail("rachel@tigerchef.com", "output", $output);
    	}
    }
    
	function calculate_grand_total()
    {
    	global $cart;
    
    	//$this->min_charge = $cart->calcMinCharges();
    	$this->grand_total  = round(($this->sub_total + $this->sales_tax + $this->shipping_cost + $this->liftgate) - $this->coupon_gift + $this->min_charge - $cart->data['rewards_using'],2);
    
    	if( $this->grand_total < 0 )
    		$this->grand_total = 0;
    
    	return ;
    }
    
    function validate_required_cc_fields()
    {
    	global $err_msgs;
    	global $passed_validation;
    	global $invalid_fields;
    
//     	$this->passed_validation = true;
//     	$this->err_msgs = array();
    	
    	/* check credit card number */
    	if (!isset($_REQUEST['cc_number']) || $_REQUEST['cc_number'] == null)
    	{
    		$this->err_msgs[] = array('field'=>'cc_number', 'msg'=> sprintf('<div class="error_msg">%s</div>', "Credit card number not entered."));
    		$this->passed_validation = false;
    	}elseif(!filter_var($_REQUEST['cc_number'],FILTER_VALIDATE_INT)){
			$this->err_msgs[] = array('field'=>'cc_number', 'msg'=> sprintf('<div class="error_msg">%s</div>', "Credit card number not valid."));
    		$this->passed_validation = false;
    	}
    
    	/* check the name for the card */
    	if (!isset($_REQUEST['cc_name']) || $_REQUEST['cc_name'] == null)
    	{
    		$this->err_msgs[] = array('field'=>'cc_name', 'msg'=> sprintf('<div class="error_msg">%s</div>', "Name on card not entered."));
    		$this->passed_validation = false;
    	}
    
    	/* check the expiration month and year */
    	if (!$_REQUEST['cc_expiration_month'] || !$_REQUEST['cc_expiration_year'])
    	{
    		if (!$_REQUEST['cc_expiration_month']) {$field = "cc_expiration_month";}
    		$this->err_msgs[] = array('field'=>$field, 'msg'=> sprintf('<div class="error_msg">%s</div>', "Please enter a valid expiration month."));
    		if (!$_REQUEST['cc_expiration_year']) {$field = "cc_expiration_year";}
			$this->err_msgs[] = array('field'=>$field, 'msg'=> sprintf('<div class="error_msg">%s</div>', "Please enter a valid expiration year."));    		
    		
    		$this->passed_validation = false;
    	}
    
    	/* check the name for the ccv */
    	if (!isset($_REQUEST['cc_ccv']) || $_REQUEST['cc_ccv'] == null)
    	{
    		$this->err_msgs[] = array('field'=>'cc_ccv', 'msg'=> sprintf('<div class="error_msg">%s</div>', "CCV not entered."));
    		$this->passed_validation = false;
    	}
    
    	return true;
    }
    
    function calculate_all()
    {
    	global $cart;
		global $debug;
    $debug['place_order5.0'] = time();
	
    	$this->calculate_coupon_gift();
		$debug['place_order5.1'] = time();
		$debug['breakdown']['promos'] = $debug['place_order5.1'] - $debug['place_order5.0'];
    	$this->calculate_sales_tax_and_shipping();
    $debug['place_order5.2'] = time();
	$debug['breakdown']['shipping'] = $debug['place_order5.2'] - $debug['place_order5.1'];
//     	if($this->paypal_checkout && $this->used_flat_rate){
//     		// if the callback failed we need to use overrides
//     		$this->shipping_cost = $this->override['shipping'];
    
//     		// we need to calculate new tax based on shipping costs used
//     		$zip_info = ZipCodes::get($this->cart2['shipping_zip']);
//     		// only charging sales tax to NY
//     		if ($zip_info[0]['state_code'] == 'NY')
//     		{
//     			$taxes          = new Taxes($zip_info[0]['zipcode'], $zip_info[0]['city']);
//     			$tax_rate       = $taxes->getRate();
//     			$sales_tax	= $cart->getTax( $tax_rate,'','',$this->shipping_cost);
//     			$this->sales_tax = round($sales_tax, 2);
//     		}else{
//     			$this->sales_tax = 0.00;
//     		}
//     	}
//     	mail('tova@tigerchef.com', 'calculate all 2', var_export($this,true));
    	 
    	$this->calculate_sub_total();
		$debug['place_order5.3'] = time();
    	$this->calculate_grand_total();
    	 $debug['place_order5.4'] = time();
    }
    
function calculate_sales_tax_and_shipping()
    {
    	global $cart;
    	global $CFG;
		global $debug;
		global $discounted_shipping_message_to_user;

    	$zip_code = $_POST['zip_code'] ? $_POST['zip_code'] : $this->cart2['shipping_zip'] ;

		
    	if(!$zip_code && $this->paypal_checkout){
    		$zip_code = $CFG->default_customer_zipcode;
    	} 

    	$debug['place_order5.1.0'] = time();
    	if ($zip_code)


    	{
    		if ($this->paypal_checkout)	$calc_shipping= Shipping::calcShipping(0, 'cart', '', $zip_code,'', false, '', false, '', $this->paypal_token);
    		
    		$zip_info = ZipCodes::get($zip_code);
			$debug['place_order5.1.1'] = time();
    		/* shipping */
    		if($this->paypal_checkout && $this->override['shipping']){
    			$this->shipping_cost = $this->override['shipping'];
    		}else{

				$time1 = microtime(true);

				//$method = ($cart->data['shipping_method'])? $cart->data['shipping_method'] : 'GND';				
				$cart->data['shipping_method'] = ($cart->data['shipping_method'])? $cart->data['shipping_method'] : 'GND';				

				$this->shipping_cost_options = Shipping::getShippingOptions($zip_code,'cart',$this->paypal_token);
		
				$this->shipping_cost = $this->shipping_cost_options[$cart->data['shipping_method']]['cost'];
				
					    			    		
	    		// Store in Session
	    		$cart->data['shipping_cost'] = round($this->shipping_cost,2);
    			//$cart->data['shipped_weight'] = $weight;

			}
    		$debug['place_order5.1.5'] = time();
    		/* only charging sales tax to NY */
    		if ($zip_info[0]['state_code'] == 'NY')
    		{
    			$taxes          = new Taxes($zip_info[0]['zipcode'], $zip_info[0]['city']);

    			$this->tax_rate       = $taxes->getRate();
    			//$sales_tax      = $cart->getCartTotal() * ($tax_rate * .01);
    			$sales_tax	= $cart->getTax( $this->tax_rate,'','',$this->shipping_cost);
    			$this->sales_tax = round($sales_tax, 2);
    		}
          $debug['place_order5.1.6'] = time();

          
		  $this->shipping_cost=floatval(preg_replace('/[^\d.]/', '', $this->shipping_cost));      
          
		  if ($this->shipping_cost < $discounted_shipping_message_to_user['original_price']){
    	        $this->original_shipping_cost=$discounted_shipping_message_to_user['original_price'];
          }
          
          else  $this->original_shipping_cost=$this->shipping_cost;
    }
    else
    	{
    		$this->tax_rate       = 0.0;
    		$this->sales_tax      = 0.0;
    		$this->shipping_cost  = 0.0;
    	}
    
    	$liftgate = Shipping::calcLiftgateFee('','cart');
    	$this->liftgate = $cart->data['charge_liftgate_fee'] ? $liftgate : 0.0 ;
    $debug['place_order5.1.7'] = time();
    	return true;
    }
	
    function createConfirmOrder(){
    
    	global $cart;
    
    	$this->calculate_all();
    
    	$return['sign_in'] = $this->create_signin();
    	$return['cart_items'] = $this->display_cart_items(true);
    	$return['cost_table'] = $this->create_cost_breakdown_table();
    
    	if(!$this->paypal_checkout){
    		echo json_encode($return);
    	} else {
    
    		$shipping_address = "<span class='name'>".$this->cart2['shipping_fname']."</span>";
    		$shipping_address .= "<address>".$this->cart2['shipping_address']."<br />";
    		if($this->cart2['shipping_address2']){
    			$shipping_address .= $this->cart2['shipping_address2']."<br />";
    		}
    		$shipping_address .= $this->cart2['shipping_city']. ", " . $this->cart2['shipping_state'] . " " . $this->cart2['shipping_zip']. "</address>";
    		$shipping_address .= "<span class='email'>". $this->cart2['shipping_email'] ."</span>";
    
    		$return['shipping_address'] = $shipping_address;

			$shipping_costs = $this->get_shipping_info($this->cart2['shipping_zip']);
			$this->liftgate = Shipping::calcLiftgateFee('','cart');

			$return['shipping_method'] = Shipping::get_shipping_display($shipping_costs,$cart->data['shipping_method'],$this->shipping_discount,$this->tax_rate,true,true);


			$return['email'] = $this->cart2['shipping_email'];
    		return $return;
    	}
    }
    
    function getShippingMethodString(){
    
    	global $cart,$CFG;
    
    	$method = db_get1( ShippingMethods::get('','','',$cart->data['shipping_method']) );
    	if($cart->data['shipping_method_2']) $method2 = db_get1( ShippingMethods::get('','','',$cart->data['shipping_method_2']) );
    
    	$items = $cart->get();
    	$ships_in = null;
    
    	if($items){
    		foreach($items as $i){
    			$prod = Products::get1($i['product_id']);
    			if($ships_in === null){
    				$ships_in = $prod['ships_in'];
    			} else if($ships_in != $prod['ships_in']){
    				$varies = "Processing time may vary for this order.";
    				break;
    			}
    		}
    	}
    
    	$str = "<ul><li><span>Shipping method: </span><span>".$method[name] ;
    	if($method2) {
    		$str .= " / $method2[name]";
    	}
    	$str .= "<span></li>";
    
    	if($cart->data['charge_liftgate_fee']){
    		$str .= "<li> You requested a liftgate for this order </li>";
    	}
    	$str .= "<li>";
    	if($varies){
    		$str .= $varies;
    	} else {
			if(!$ships_in){ $ships_in = $CFG->default_ships_in;}
    		$ships_in = ShipsIn::get1($ships_in);
    		$str .= "This order processes in " .  $ships_in['name'] . ". ";
    	}
    	$str .= "</li></ul>";
    
    	return $str;
    }
    
    function showProductLineARedesignCheckout($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
    	global $CFG;
    	global $cart;
    
    	$product_info       = Products::get1($cart_line['product_id']);
    	$product_link       = Catalog::makeProductLink_($product_info, $product_info['cat_id']);
    	$product_image_link = Catalog::makeProductImageLink($product_info['id']);
    	$vsku               = $product_info['mfr_part_num'];
    	$brand              = Brands::get1($product_info['brand_id']);
    
    	if( $cart_line['options'] )
    	{
    		//Only 1 option can be selected
    		$option = $cart_line['options'][0];
    		$vsku = $option['mfr_part_num'] . ' - ' . $option['value'];
    	}
    
    	if ($show_title_box)
    	{
    		?>
            <ul class="shoping-list">
    			<li class="heading">
    				<div class="col-1">Product</div>
    				<div class="col-2">Quantity</div>
    				<div class="col-3">Unit Price</div>
    				<div class="col-4">Price</div>
    			</li>
            <?
    	    }
    
    	    ?>			
    			<li>
    			<div class="col-1">
    				<div class="holder">
    				<div class="img-holder">				
                    <? if ($product_info['is_active'] == 'Y') {?><a href="<?=$product_link?>"><? } ?><img src="<?=$product_image_link?>" alt="<?=$product_info['mfr_part_num']?>" width="102" height="98" /><? if ($product_info['is_active'] == 'Y') {?></a><? } ?>
                    	</div>                                            
                            <div class="description">
                            <? if ($product_info['is_active'] == 'Y') {?><a href="<?=$product_link?>"><? } ?><span>ITEM # (<?=$vsku ? $vsku : $product_info['mfr_part_num']?>)<? if ($product_info['is_active'] == 'Y') {?></span></a><? } ?>
                            <strong><?=str_replace('�','&reg;',$product_info['name'])?></strong> 
                            <span>by <?=$brand['name']?></span>                        
    			<?
    	        if (strpos($product_info['promo_msg'], "do not qualify for free shipping") !== false) echo "<span class='no_free_ship'>This item does not qualify for free shipping.</span>";
    			if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
    				echo "<span class='restock_date'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
    			}
    			$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
    			if($ships_in){
    				echo "<span class='ships_in'>";
    				if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d"))  && $product_info['restock_date_show_on_website'] == 'Y'){
    					echo "Once in stock this item ships in ";
    				}else{
    					echo "Ships in ";
    				}
    				echo $ships_in . "</span>";
    			}
    			if($cart->data['shipping_type']['standard_brands'] && $cart->data['shipping_type']['freight_brands']){
    				echo "<span class='freight'>";
    				if(in_array($cart_line['brand_id'],$cart->data['shipping_type']['freight_brands'])){
    					echo "Ships freight";
    				}else{
    					echo "Ships via standard carrier";	
    				}
    				echo "</span>";
    			}
    
    			?>
    			</div>
    			</div>
    			</div>
            <?
        }
    
	/**
	 * @return string - link for page to redirect them to
	 */
	public static function make_continue_shopping_link()
	{
		global $CFG;
		$product    = null;
		if ($_REQUEST['product_id'] != '')
		{
			$product = Products::get1($_REQUEST['product_id']);
			if ($product)
			{
				return Catalog::makeProductLink_($product);
			}
		}
		else if ($_SESSION['prev_prod_page_link'] != "" &&
			strpos($_SESSION['prev_prod_page_link'],'cart.php')===false &&
			strpos($_SESSION['prev_prod_page_link'],'checkout.php')===false) {
			return $_SESSION['prev_prod_page_link'];
		}

		$referer = $_SERVER['HTTP_REFERER'];
		if ($referer != "" &&
			strpos($referer,'cart.php')===false &&
			strpos($referer,'checkout.php')===false) {
			return $referer;
		}else{
			return "/";
			//return "javascript:history.go(-1)";
		}

	}    
    
}

?>
