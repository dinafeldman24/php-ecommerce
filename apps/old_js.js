$j(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'ajax/ajax.product_info.php';
        var uploadButton = $j('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $j(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $j('[id^="file_upload"]').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        var file_container = $j(e.target).closest('.listing_available_colors_v1.option-selector-holder.file-upload').find('.files');
        data.context = $j('<div/>').appendTo(file_container);
        console.log('ADD FILE: the context:' , data.context);
        $j.each(data.files, function (index, file) {
            var node = $j('<p/>')
                    .append($j('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        console.log('PROCESS ALWAYS the data: ', data);
        data.context = $j(e.target).closest('.listing_available_colors_v1.option-selector-holder.file-upload').find('.files');
        console.log('the context:', data.context);
        var index = data.index,
            file = data.files[index],
            node = $j(data.context.children()[index].children[index]);
            console.log('the node:', node);
       
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($j('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
           node.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $j('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
 //       data.context = $j(e.target).closest('.listing_available_colors_v1.option-selector-holder.file-upload').find('.files');
   //     console.log('UPLOAD DONE: the data:', data.context); 
        $j.each(data.result.files, function (index, file) {
                console.log(':the child of ', index, ':', $j(data.context.children()[index]));
            if (file.url) {
                var link = $j('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);

                $j(data.context.children()[index])
                    .wrap(link);
                $j('#option_customization_' + file.option_id).val(file.url);
            } else if (file.error) {
                var error = $j('<span class="text-danger"/>').text(file.error);
                $j(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        $j('#progress .progress-bar').css('width','0px');
        });
    }).on('fileuploadfail', function (e, data) {
        $j('#progress .progress-bar').css('width','0px');
        $j.each(data.files, function (index) {
            var error = $j('<span class="text-danger"/>').text('File upload failed.');
            $j(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$j.support.fileInput)
        .parent().addClass($j.support.fileInput ? undefined : 'disabled');
});
</script>

