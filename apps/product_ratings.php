<?php

class Product_Ratings
{
   public static function get($id = 0, $product_id = 0, $customer_id = 0, $rating = 0, $approved='Y')
    {
        $sql = "SELECT product_reviews.* 
                FROM product_reviews
                WHERE 1 ";

        if ($id > 0)
        {
            //$sql .= " AND product_ratings.id = $id ";
        }

        if ($product_id > 0)
        {
            $sql .= " AND product_reviews.product_id = $product_id ";
        }

        if ($customer_id > 0)
        {
            $sql .= " AND product_reviews.customer_id = $customer_id ";
        }

        if ($rating > 0)
        {
            $sql .= " AND product_reviews.rating = $rating ";
        }
				
        if ($approved)
        {
            $sql .= " AND product_reviews.approved = '{$approved}' ";
        }

        return db_query_array($sql);
    }

    public static function get_rating_for_product($product_id)
    {
        /* make sure we are dealing with good data */
        if (!$product_id && !is_int($product_id))
        {
            return null;
        }

        $sql = "SELECT count( * ) AS ratings_total, sum( rating ) AS ratings_sum, sum( rating ) /5 AS avg_rating, CAST( ((sum( rating ) / COUNT( * ) ) *20) AS UNSIGNED) AS percentage, 
				product_reviews.approved AS approved
				FROM product_reviews WHERE 1
				AND product_reviews.approved = 'Y'
				AND product_reviews.product_id = {$product_id}
				AND product_reviews.rating > 0
				GROUP BY product_reviews.product_id";
                

        $result = db_query_array($sql);
        if ($result)
        {
            return $result[0];
        }
        else
        {
            return false;
        }
    }

    public static function get_rating_by_product_and_customer($product_id, $customer_id)
    {
        return self::get(0, $product_id, $customer_id);
    }

    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info, $product_id, $customer_id)
    {
        $info['product_id']     = $product_id;
        $info['customer_id']    = $customer_id;

			  if(!Product_Reviews::get_review_by_product_and_customer($product_id,$customer_id)){ //If there's only a rating and no review create review record
				Product_Reviews::insert(array('approved' => 'Y'), $product_id, $customer_id);//Automatically approve rating with no review.
				}
        return db_insert('product_reviews', $info);
    }

/*    public static function delete($id)
    {
        $info['rating']='';
        return db_update('product_reviews', $id, $info);
    }*/

    public static function update($id, $info)
    {
  //      return db_update('product_reviews', $id, $info );
    }
}

?>