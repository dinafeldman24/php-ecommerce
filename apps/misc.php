<?
function hbar()
{
	ob_start();

	?>
	<table width="100%" cellspacing=0 cellpadding=0 border=0>
	<tr>
		<td><img src="/pics/hbar1.gif" width=385 height=2 alt=""></td>
		<td bgcolor="#334668" width="100%"><img src="/pics/spacer.gif" width=1 height=2 alt=""></td>
		<td><img src="/pics/hbar2.gif" width=385 height=2 alt=""></td>
	</tr>
	</table>
	<?

	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

function spaceToNbsp($str)
{
  return preg_replace("/ /", "&nbsp;", $str);
}

function isSSL()
{
  return ($_SERVER['SERVER_PORT'] == "443");
}

function ellipsis($str, $length)
{
	if (strlen($str) > $length)
	  return substr($str, 0, $length) . "...";
	else
	  return $str;
}

function swap_vals(&$_1, &$_2)
{
	$tmp = $_1;
	$_1 = $_2;
	$_2 = $tmp;
}

function assert_order(&$_1, &$_2)
{
	if (!$_1 || !$_2) return;
	if ($_1 > $_2) {
		$tmp = $_1;
		$_1 = $_2;
		$_2 = $tmp;
	}
}

function linkizeText($str)
{
   $str = preg_replace('#(http://)([^\s]*)#', '<a href="\\1\\2">\\1\\2</a>', $str);
   if($www) {
       $str = preg_replace('=(www.)([^\s]*)=', '<a href="http://\\1\\2">\\1\\2</a>', $str);
   }
   return $str;
}

function prettyDate($unix_time, $with_time = true)
{
	$time = "g:iA";
	if (!$with_time) $time = "";
	return date("M j, Y $time", $unix_time);
}


function formatPhoneNumber( $pnumb )
{
	require_once("RB/Format.php");

	return Format::phone($pnumb,true,1);
	//Take a 10 digit phone number and format it nicely for display
//	if( !$pnumb )
//		return;
//	$pnumb = ereg_replace ('[^0-9]+', '', $pnumb );
//	$ret = "(" . substr($pnumb, 0, 3 ) . ")" . substr( $pnumb, 3, 3 ) . "-" . substr($pnumb, 6);
//	return $ret;
}
?>