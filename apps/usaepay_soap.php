<?php


class emerchantSoap{


    var $sandbox_wsdl = '';
    var $live_wsdl = 'https://www.usaepay.com/soap/gate/6E5FD372/usaepay.wsdl';
    var $is_sandbox=false;
    var $source_key;
    var $pin;
    var $token;
    var $client;

    function createClient(){
        $wsdl = ($this->is_sandbox) ? $this->sandbox_wsdl :
                $this->live_wsdl;
        $this->client = new SoapClient($wsdl);
    }

    function createSecurityToken(){
        if(empty($this->source_key) || empty($this->pin)){
            throw new Exception('Please set source key and pin');
        }
        // generate random seed value
        $seed=mktime() . rand();

        // make hash value using sha1 function
        $clear= $this->source_key . $seed . $this->pin;
        $hash=sha1($clear);

        // assembly ueSecurityToken as an array
        $token=array(
            'SourceKey'=>$this->source_key,
            'PinHash'=>array(
                'Type'=>'sha1',
                'Seed'=>$seed,
                'HashValue'=>$hash
            ),
            'ClientIP'=>$_SERVER['REMOTE_ADDR'],
        );

        $this->token = $token;

}

    function getSecurityToken(){
        if(empty($this->token)){
            $this->createSecurityToken();
        }
        return $this->token;
    }


}
