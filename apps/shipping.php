<?php

class Shipping {

	function insertRate($info)
	{
		return db_insert('shipping_rates',$info,'',true);
	}

	function updateRate($type_id,$amount_id,$info)
	{
		$keys = Array ('rate_type_id','rate_amount_id'); 
		$id = Array ($type_id,$amount_id);
		return db_update('shipping_rates',$id,$info,$keys);
	}

	function delete($id) 
	{
		return db_delete('shipping_rates',$id);   
	}

	function lookupRates($amt)
	{

		$sql = "SELECT id
				FROM shipping_rate_amounts
				WHERE amount >= $amt
				ORDER BY amount
				LIMIT 1";

		$result = db_query_array($sql,'',true);

		$sql = "SELECT shipping_rates.*,shipping_rate_types.type
				FROM shipping_rates
				LEFT JOIN shipping_rate_types ON shipping_rates.rate_type_id = shipping_rate_types.id
				LEFT JOIN shipping_rate_amounts ON shipping_rates.rate_amount_id = shipping_rate_amounts.id
				WHERE shipping_rates.rate_amount_id = '$result[id]'
				ORDER BY shipping_rate_types.order_fld";

		$result = db_query_array($sql);

		return $result;
	}

	function getFlatRate($total)
	{
		$result = self::lookupRates($total);

		return $result[0]['shipping_rate'];
	}

	function getRates($type_id=0,$amount_id=0)
	{
		$sql = "SELECT shipping_rates.*,shipping_rate_types.type,shipping_rate_amounts.amount
				FROM shipping_rates
				LEFT JOIN shipping_rate_types ON shipping_rates.rate_type_id = shipping_rate_types.id
				LEFT JOIN shipping_rate_amounts ON shipping_rates.rate_amount_id = shipping_rate_amounts.id
				WHERE 1 ";

		if ($type_id > 0) {
			$sql .= " AND shipping_rates.rate_type_id  = $id ";
		}
		if ($amount_id > 0) {
			$sql .= " AND shipping_rates.rate_amount_id  = $id ";
		}

		$sql .= " ORDER BY shipping_rate_amounts.amount, shipping_rate_types.order_fld ";

		return db_query_array($sql);
	}

	function getRate($type_id,$amount_id)
	{
		$type_id = (int) $type_id;
		$amount_id = (int) $amount_id;
		if (!$type_id || !$amount_id) return false;
		$result = Shipping::getRate($type_id,$amount_id);
		return $result[0];
	}

	function getRateTypes($id=0)
	{
		$sql = "SELECT shipping_rate_types.*
				FROM shipping_rate_types
				WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND shipping_rate_types.id = $id ";
		}

		$sql .= " ORDER BY order_fld";

		return db_query_array($sql);
	}

	function getRateType($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Shipping::getRateTypes($id);
		return $result[0];
	}

	function insertRateType($info)
	{
		return db_insert('shipping_rate_types',$info);
	}

	function updateRateType($id,$info)
	{
		return db_update('shipping_rate_types',$id,$info);
	}

	function deleteRateType($id)
	{
		return db_delete('shipping_rate_types',$id);
	}


	function buildRatesGrid($rates)
	{
		if (!is_array($rates))
			return false;
		foreach ($rates as $rate) {
			$rates_grid[$rate[rate_amount_id]][$rate[rate_type_id]] = $rate[shipping_rate];
		}
		return $rates_grid;
	}

	function getRateAmounts($id=0, $orderby = 'asc')
	{
		$sql = "SELECT shipping_rate_amounts.*
				FROM shipping_rate_amounts
				WHERE 1";

		if ($id > 0) {
			$sql .= " AND shipping_rate_amounts.id = $id ";
		}
		$sql .= " ORDER BY amount $orderby";
		//echo $sql;
		return db_query_array($sql);
	}

	function getRateAmount($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Shipping::getRateAmounts($id);
		return $result[0];
	}

	function insertRateAmount($info)
	{
		return db_insert('shipping_rate_amounts',$info);
	}

	function updateRateAmount($id,$info)
	{
		return db_update('shipping_rate_amounts',$id,$info);
	}

	function deleteRateAmount($id)
	{
		return db_delete('shipping_rate_amounts',$id);
	}

	function getShippingMatrix()
	{
	   $q = "SELECT * from shipping_rate_amounts AS SRA, shipping_rate_types as SRT, shipping_rates as S
		 WHERE SRA.id = S.rate_amount_id AND SRT.id = S.rate_type_id ORDER BY rate_type_id, rate_amount_id";
    
	   return db_query_array($q);
	}
    
	function printShippingMatrix($shiprates)
	{
	  $prev_type = "";
    
	 echo "<table>
	       <tr>
	       <th>Order Amount</th>
	       <th width='20'></th>
	       <th>Shipping Cost</th>
	       </tr>
	      ";
    
	 for ($cnt = 0; $cnt < sizeof($shiprates); $cnt++)
	 {
	       $shiprate = $shiprates[$cnt];
	       if ($shiprate['type'] != $prev_type)
	       {
		 echo "<tr><td colspan='2'><strong>$shiprate[type]</strong></td></tr>";
		 $prev_type = $shiprate['type'];
	       }
	       echo "<tr><td>\$$shiprate[amount]</td><td></td><td>\$$shiprate[shipping_rate]</td></tr>";
	 }
    
	echo "</table>";
    
	}

	
	function getShippingRates( $id = 0, $type='cart', $info='', $zip_code='', $product_qty=0, $hardCodedMethod = "", $onlyOnNonDiscounted = false, $cart_reference='', $paypal_token='', $options='', $all_rates=false){

		global $CFG;
		$shipper = UPSShipper::get1($CFG->default_from_shipper);
		$myRate = new upsRate();
		$myRate->setCredentials($CFG->ups_key,$CFG->ups_user,$CFG->ups_pass,$shipper['ups_account_number']);
		
		//$quote = $myRate->getRate('10901','90036','',13,'Shop');
		//mail('tova@tigerchef.com', 'shippingratestr', var_export($quote));
	}
	
	static function calcShipping( $id = 0, $type='cart', $info='', $zip_code='', $product_qty=0, $method_override=false, $hardCodedMethod = "", $onlyOnNonDiscounted = false, $cart_reference='', $paypal_token='', $options='', $all_rates=false, &$err_msg='')
	{		
		global $CFG, $cart, $cart2;		
		$curtime = date('Y-m-d H:i:s');

		if (($CFG->disable_expedited_shipping_start_date && $CFG->disable_expedited_shipping_end_date) && $curtime > $CFG->disable_expedited_shipping_start_date && $curtime <= $CFG->disable_expedited_shipping_end_date) $CFG->disable_expedited_shipping = '1';
		$zip_code = substr($zip_code, 0, 5);

		//clear freight brands
		if($all_rates){
			unset($cart->data['shipping_type']);
		}
		
		// get destination info, and make sure it is a valid zipcode
		$dest_zip_info = ZipCodesUSA::get_city_state_from_zip($zip_code);
		if(!$dest_zip_info){
			$err_msg = "$zip_code is not a valid US zipcode.";
			return 0;
		}
		if(!ZipCodesUSA::is_valid_destination('',$dest_zip_info)){
			$err_msg = "We do not ship to " . $dest_zip_info['StateName'];
			return 0;
		}
		
		$time_begin = microtime(true);
		
 		if($type == 'orders'){
			$order = Orders::get1( $id );
		}
		
		if($hardCodedMethod != ""){
			$method = $hardCodedMethod;
		} else {
			if($type == 'orders' && !$all_rates){
				$method = ShippingMethods::get1($order['shipping_id']);
				$method2 = ShippingMethods::get1($order['shipping_id_2']);
		
				$method = $method['code'];
				$method2 = $method2['code'];
					
			}elseif(!$all_rates){
				$method = $cart->data['shipping_method'];
				$method2 = $cart->data['shipping_method_2'];
			}
		}
		if (!$method && !$all_rates) $method = "GND";
		
		$items = Shipping::getItemsForShipping( $type, $method, $id, $product_qty, $onlyOnNonDiscounted, $cart_reference, $paypal_token);		
		
		if($type == 'orders'){
			if(!$zip_code)
			{
				$zip_code = $order['shipping_zip'];
			}
			$subtotal = $order['subtotal'];
			$shipping_po_box = preg_match("/^box[^a-z]|(p.?[- ]?o.?[- ]?|post office )b(.|ox)/i", $order['shipping_address1']);
		}elseif($type == 'cart'){
			if(!$zip_code){
				$zip_code = $cart->data['zipcode'];
			}
			$subtotal = $cart->getCartTotal(true,true,"","",'', $paypal_token);
			if($cart_reference){
				$shipping_po_box = ($options['po_box'] == 'po_box') ? true : false ;
			}else{
				$shipping_po_box = ($cart->data['shipping_po_box'] == 'po_box') ? true : false ;
			}
		}
		
		if($CFG->free_shipping_subtotal_min_active){
			if($subtotal > $CFG->free_shipping_subtotal_min_amount){
				$free_shipping_subtotal_min_qualifies = true;
			}
		}		
		
		//make sure the zipcode is only 5 digits
		if( stristr( $zip_code, '-' ) )
		{
			$parts = explode( '-', $zip_code );
			$zip_code = $parts[0];
		}
			
		if($shipping_po_box && $type != 'product'){
			$validate_po_box = Shipping::validate_po_box('',$type,$id);
			if(!$validate_po_box['can_ship']){
				// if some items can't be shipped to a po box don't calculate shipping
				return 0;
			}
		}
			
		if($type == 'orders'){
			if($order['current_status_id'] == $CFG->government_quote_status_id){
				$quote = 0;
				foreach ($items as $item){
					if($method == $CFG->freight_shipping_code || $method2 == $CFG->freight_shipping_code){
						if($item['shipping_type'] == 'Freight'){
							$quote += $item['bid_shipping'];
						}
					}
					if($item['shipping_type'] !== 'Freight'){
						$quote += Shipping::calcShipping($item['product_id'],'product','',$item['shipping_zip'],$item['qty'],false,$method);
					}
				}
				return $quote;
			}
		}
		
		//set some variables
		$shipper = UPSShipper::get1($CFG->default_from_shipper);	
		$methodInfo = ShippingMethods::get('', '', '',$method,'','');
		$comOrRes = "RES";		
		//$debug['shipper'] = $shipper;
		//$debug['methodinfo'] = $methodInfo;
		
		if(!$CFG->backend && $CFG->disable_expedited_shipping && !$method){
			$methodInfo = ShippingMethods::get('', '', '','GND','','');
			$methodInfo2 = ShippingMethods::get('', '', '','FRT','','');
			$methodInfo = array_merge($methodInfo,$methodInfo2);
		}
		$debug['backend'] = $CFG->backend;
		$debug['disable_expedited'] = $CFG->disable_expedited_shipping;
		//$debug['methodinfo'] = $methodInfo;
		
		//group the items by brand - items that ship from our warehouse should be grouped with the TC brand
		$warehouse_brands = explode(',', $CFG->mfrs_ship_from_warehouse);
		
		foreach ($items as $item){
			
			if(in_array($item['brand_id'], $warehouse_brands)){
				$brand_group = 0;
			
// 			if($item['brand_id'] == '33' || $item['brand_id'] == '123' || $item['brand_id'] == '83' ){
// 				$brand_group = '26' ; 
			}else{
				$brand_group = $item['brand_id'];
			}
			$items_by_brand[$brand_group][] = $item;
			
			/*
			//if this product ships freight this brand will ship freight
			if(strtolower($item['shipping_type']) == 'freight'){
				$debug['freight_item'] = $item['id'] . '=' . $item['shipping_type'];
				$freight_brands[$brand_group] = true ;
			}
			*/
			
		}		






		require_once("$CFG->libdir/upsRate.php");
		$myRate = new upsRate();
		$myRate->setCredentials($CFG->ups_key,$CFG->ups_user,$CFG->ups_pass,$shipper['ups_account_number']);
		//mail("dina.websites@gmail.com","upsinfo",$CFG->ups_key." ".$CFG->ups_user." ".$CFG->ups_pass." ".$shipper['ups_account_number']);
		
		require_once("$CFG->libdir/unishippers.php");
		$myFreightRate = new UniShippers();
		
		$final_quote = array();
		$debug['all_rates'] = $all_rates;
		//calculate the quote for each brand
		foreach($items_by_brand as $brand_id => $items)
		{
			$debug[$brand_id]['brand_id'] = $brand_id;
			//echo '<br><br>THIS IS A NEW BRAND: ' . $brand_id . '<br>';
			$quote = 0;
			$brand_quote = array();	
			
			if($brand_id == 0){
				$brand_origin_zips = $CFG->warehouse_zip;
				
			}else{
				$brand_origin_zips = $items[0]['product_info']['brand_origin_zipcodes'];
		    
				//get brand handling fee
				$mfr_handling_fee = $items[0]['product_info']['mfr_handling_fee'];
				$debug[$brand_id]['mfr_handling_fee'] = $mfr_handling_fee;
			}
			
			if($brand_origin_zips){
				$brand_origin_zips = explode(',', $brand_origin_zips);
			}else{
				$brand_origin_zips = array($shipper['zip']);
			}		
			$time_weight1 = microtime(true);
			$weight_info = Shipping::calcShippingWeight($type,$id,$product_qty,$items,$onlyOnNonDiscounted,$method,$cart_reference,$paypal_token,$free_shipping_subtotal_min_qualifies);
			$debug[$brand_id]['time_weight'] = microtime(true) - $time_weight1;				
			
			$weight = $weight_info['weight'];
			$free_weight = $weight_info['free_weight'];
			$product_dimensions = $weight_info['dimensions'];
			
			if( !$weight ){ $weight = 0; }
			$weight = ceil($weight);
			
			if( $weight == 0){
				$quote = 0.00;
			}
			
			$debug[$brand_id]['weight'] = $weight_info;
			$debug['freight_weight'] = $CFG->weight_for_freight_shipping;
			
			//get city and state for origin zipcode
			$origin_zips_info = ZipCodesUSA::get_city_state_from_zip($brand_origin_zips);
			//mail('tova@tigerchef.com', 'zipinfo', var_export($dest_zip_info,true).var_export($origin_zips_info,true));
			
			// calculate freight quote
			$freight_quote = 0;
			if($weight_info['freight_weight'] > 0 || $weight_info['free_freight_weight'] > 0){
				
				$shipping_data['shipping_type']['freight_brands'][] = $brand_id;
				
				if($weight_info['freight_weight'] == 0 && $weight_info['free_freight_weight'] > 0){
					$has_free_freight = true;
					continue;
				}

				if($CFG->use_freight_formula){
					$freight_quote = $weight_info['freight_weight'] * $CFG->freight_formula_per_pound_fee ;
				}else{
					//get freight quote from unishippers				
					$time_freight_rate = microtime(true);
					$myFreightRate->setCredentials($CFG->unishippersSGICN);
					$freight_class = $weight_info['freight_class'] ? $weight_info['freight_class'] : $CFG->default_freight_class;
					foreach ($origin_zips_info as $origin_zip){
						$freight_brand_quote = $myFreightRate->getRate($origin_zip['CityName'], $origin_zip['StateAbbr'], $origin_zip['ZipCode'], $dest_zip_info['CityName'], $dest_zip_info['StateAbbr'], $dest_zip_info['ZipCode'], $weight_info['freight_weight'],$freight_class);
						$debug[$brand_id]['freight'][$origin_zip['ZipCode']] = $freight_brand_quote;
						
						$freight_quote = ($freight_quote > 0) ? min($freight_quote,$freight_brand_quote) : $freight_brand_quote ;
					}
					//if it fails use flat freight rate
					if($freight_quote == 0){
						$freight_quote = Shipping::getDefaultFreightCost($items,$zip_code);
						if(!$CFG->in_testing){
							mail($CFG->error_email,'ALERT - Unishippers Rating Failed',var_export($myFreightRate->debug,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
						}
					}
				}
				$final_quote['FRT']['cost'] += $freight_quote;
				
				$debug[$brand_id]['freightcost'] = $final_quote['FRT']['cost'];
				$debug[$brand_id][$origin_zip['ZipCode']]['freight_time_rate'] =  microtime(true) - $time_freight_rate ;
				$debug['total_freight_rating_time'] += $debug[$brand_id][$origin_zip['ZipCode']]['freight_time_rate'];
				
				
			} elseif($weight > 0 && $method != $CFG->freight_shipping_code) {
				
				$has_ups_items = true;
				$shipping_data['shipping_type']['standard_brands'][] = $brand_id;
				
				//get UPS rate
			foreach ($origin_zips_info as $origin_zip){ 
					$time_rate = microtime(true);
					
					if(!$CFG->backend && $CFG->disable_expedited_shipping && $CFG->use_flat_rate_shipping){
						$quote_values['GND'] = $CFG->flat_shipping_rate;
						$debug['testing'] = $CFG->flat_shipping_rate;
					}else{
					
						if($all_rates){
							
							$ups_response = $myRate->getRate($origin_zip['ZipCode'],$dest_zip_info['ZipCode'],'',$weight_info['pkgs'],'Shop','',$origin_zip['StateAbbr'],$dest_zip_info['StateAbbr']);
							
							//$ups_response['Response']['ResponseStatusCode'] = 0;
							
							if($ups_response['Response']['ResponseStatusCode'] == 0){
								//alert that something failed
								if(!$CFG->in_testing){
									mail($CFG->error_email,'UPS rating failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
								}else{
									mail('tova@tigerchef.com','UPS rating failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
								}
	
								if($CFG->use_flat_rate_shipping && (!$method || $method == 'GND')){
									$quote_values['GND'] = $CFG->flat_shipping_rate;
								}else{
									// use the rates stored in the db
									foreach ($methodInfo as $shipping_method){
										$quote_values[$shipping_method['code']] = Shipping::getUPSRateFromDB($weight_info['weight'],$zip_code,$shipping_method,$comOrRes);
										if($quote_values[$shipping_method['code']] === false){
											unset ($quote_values[$shipping_method['code']]);
										}
									}
								}	
							}else{
								
								$quote_options = $ups_response['RatedShipment'];
								
								foreach ($methodInfo as $current_method){
									foreach ($quote_options as $option){
										if($current_method['api_code'] == $option['Service']['Code']){
											$debug['api_pairs'][] = $current_method['api_code'] . ' ' . $option['Service']['Code'];
											if(isset($option['NegotiatedRates'])){
												$quote_values[$current_method['code']] = $option['NegotiatedRates']['NetSummaryCharges']['GrandTotal']['MonetaryValue'];
											}else{
												if(!$CFG->in_testing){
													mail($CFG->error_email,'ALERT - UPS Negotiated Rates Failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
												}
												$quote_values[$current_method['code']] = $option['TotalCharges']['MonetaryValue'];
											}
										}
									}
								}
							}
						}else{
							$debug['api_code'] = $methodInfo[0]['api_code'];
							$ups_response = $myRate->getRate($origin_zip['ZipCode'],$dest_zip_info['ZipCode'],$methodInfo[0]['api_code'],$weight_info['pkgs'],'Rate','',$origin_zip['StateAbbr'],$dest_zip_info['StateAbbr']);
							
							if($ups_response['Response']['ResponseStatusCode'] == 0){
								//alert that something failed
								if(!$CFG->in_testing){ 
									mail($CFG->error_email,'UPS rating failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
								}else{
									mail('tova@tigerchef.com','UPS rating failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
								}
								
								// use the rates stored in the db
								$quote = Shipping::getUPSRateFromDB($weight,$zip_code,$methodInfo[0],$comOrRes);
							}else{
								if(isset($ups_response['RatedShipment']['NegotiatedRates'])){
									$quote_values[$method] = $ups_response['RatedShipment']['NegotiatedRates']['NetSummaryCharges']['GrandTotal']['MonetaryValue'];
								}else{
									if(!$CFG->in_testing){
										mail($CFG->error_email,'ALERT - UPS Negotiated Rates Failed',var_export($ups_response,true).var_export($debug,true).var_export($_SESSION,true).var_export($cart->get(),true));
									}
									$quote_values[$method] = $ups_response['RatedShipment']['TotalCharges']['MonetaryValue'];
								}
							}					
						}
					}	
					$debug[$brand_id][$origin_zip['ZipCode']]['origin_zip'] = $origin_zip;
					$debug[$brand_id][$origin_zip['ZipCode']]['quotevalues'] = $quote_values;
					$debug[$brand_id][$origin_zip['ZipCode']]['quoteoptions'] = $quote_options; 
					$debug[$brand_id][$origin_zip['ZipCode']]['time_rate'] =  microtime(true) - $time_rate ;
					$debug['total_rating_time'] += $debug[$brand_id][$origin_zip['ZipCode']]['time_rate'];
				
					//save the quote for this brand
					if(is_array($quote_values)){
						foreach ($quote_values as $key=>$value){
							$brand_quote[$key] = ($brand_quote[$key] > 0) ? min($brand_quote[$key],$value) : $value ;
						}
					}else{
						$brand_quote[$method] = ($brand_quote[$method] > 0) ? min($brand_quote[$method],$quote) : $quote ;
					}
				}
			}else{
				$brand_quote[$method] = 0 ;
			}
			
			$debug[$brand_id]['brand_quote'] = $brand_quote;				
			
			foreach ($brand_quote as $key=>$value){
				$final_quote[$key]['cost'] += $brand_quote[$key];
				$final_quote[$key]['cost'] += $mfr_handling_fee; 
			}
			
		} //end brand foreach	

		//GND flat rate shipping
		if($CFG->use_flat_rate_shipping){
			if($has_ups_items && $final_quote['GND']['cost'] > 0){
				$final_quote['GND']['cost'] = $CFG->flat_shipping_rate;
			}
			
			if($weight_info['custom_shipping_fees']){
				$flat_rate_charge += $weight_info['custom_shipping_fees'];
			}
			
			$final_quote['GND']['cost'] += $weight_info['custom_shipping_fees'];

			$debug['flat']['GND_cost'] = $final_quote['GND']['cost'];
			$debug['flat']['GND_fee'] = $CFG->flat_shipping_rate;
			$debug['flat']['custom'] = $weight_info['custom_shipping_fees'];
		}
		
		if($final_quote['FRT']['cost'] && $CFG->use_freight_formula){
			$final_quote['FRT']['cost'] += $CFG->freight_formula_base_fee;
		}		
		
// 		//cap freight shipping at max_ground_shipping_cost
// 		if($CFG->use_flat_rate_shipping){$max_cost = $CFG->max_ground_shipping_cost + $CFG->flat_shipping_rate;}
// 		else{$max_cost = $CFG->max_ground_shipping_cost + $final_quote['GND']['cost']; /*if we stop using flat rate - this needs to be checked*/}

		if( ($final_quote['FRT']['cost'] > $CFG->max_ground_shipping_cost) && ($all_rates || $method == 'FRT' )){ 
			//$final_quote['FRT']['cost'] = $CFG->max_ground_shipping_cost; 
			
			
			//cap freight shipping at max_ground_shipping_cost
			if($CFG->use_flat_rate_shipping){$final_quote['FRT']['cost'] = $CFG->max_ground_shipping_cost - $CFG->flat_shipping_rate;}
			else{
				if(!$all_rates){
					//get the ground shipping charge for this order
					//$ground_cost = Shipping::calcShipping( $id, $type, $info, $zip_code, $product_qty, false, "GND", $onlyOnNonDiscounted, $cart_reference, $paypal_token, $options, false);
				}else{
					$ground_cost = $final_quote['GND']['cost'];
				}
				$final_quote['FRT']['cost'] = $CFG->max_ground_shipping_cost - $ground_cost;
			}
		}
		
		$debug['subtotal'] = $subtotal;
		
		
		$debug['total_time'] = microtime(true) - $time_begin;
		$debug['extra_time'] = $debug['total_time'] - $debug['total_rating_time'] - $debug['total_freight_rating_time'];
		
		
		if($subtotal < $CFG->small_order_handling_charge_limit){
			$order_handling_fee = $CFG->small_order_handling_charge;
		}
		
		$debug['order_handling_fee'] = $order_handling_fee;
		
		if($all_rates){
			if($type =='orders'){			
				/*
				if( is_array($shipping_data['shipping_type']['freight_brands']) && is_array($shipping_data['shipping_type']['standard_brands']) ){
					$info['shipping_id_2'] = $CFG->freight_shipping_id;
				}elseif( is_array($shipping_data['shipping_type']['freight_brands']) ){
					$info['shipping_id'] = $CFG->freight_shipping_id;
					$info['shipping_id_2'] = 0;
				}else{
					$info['shipping_id_2'] = 0;
				}
				*/
				if( is_array($shipping_data['shipping_type']['freight_brands']) ){
					$info['shipping_id'] = $CFG->freight_shipping_id;
				}elseif($order['shipping_id'] == $CFG->freight_shipping_id){
					$order['shipping_id'] = 1; //set it to GND
				}
				$info['shipping_id_2'] = 0;
				if($order['shipping_id'] != $info['shipping_id'] || $order['shipping_id_2'] != $info['shipping_id_2']){
					Orders::update($id, $info);
				}
				
				foreach($items as $item){
					if(in_array($item['brand_id'],$shipping_data['shipping_type']['freight_brands']) && $item['item_shipping_type']!=='Freight'){
						Orders::updateItem($item['id'],array('item_shipping_type' => 'Freight'));
					}
					if(in_array($item['brand_id'],$shipping_data['shipping_type']['standard_brands']) && $item['item_shipping_type']!=='Standard'){
						Orders::updateItem($item['id'],array('item_shipping_type' => 'Standard'));
					}
				}
			}else{
				$cart->data['shipping_type'] = $shipping_data['shipping_type'];
			}
		}
		//mail('dina.websites@gmail.com','shipping debug ', var_export($debug,true));
		if(!$all_rates){
			$final_quote[$method]['cost'] = $final_quote[$method]['cost'] * $methodInfo[0]['adjustment_percentage'];
			if($method != 'FRT'){
				if($method != 'GND'  || ($method == 'GND' && !$CFG->use_flat_rate_shipping)){
					$final_quote[$method]['cost'] += $order_handling_fee;
				}
			}
			$debug['final_quote'] = $final_quote;
			//mail('dina.websites@gmail.com','shipping debug - '. $method, var_export($debug,true));
			return round($final_quote[$method]['cost'],2);
		}else{
								
			foreach ($methodInfo as $shipping_method){		
				if($final_quote[$shipping_method['code']]){		
					$final_quote[$shipping_method['code']]['info'] = $shipping_method; 
					$final_quote[$shipping_method['code']]['cost'] *= $shipping_method['adjustment_percentage'];
					if($shipping_method['code'] != 'GND'  || ($shipping_method['code'] == 'GND' && !$CFG->use_flat_rate_shipping)){
						$final_quote[$shipping_method['code']]['cost'] += $order_handling_fee;
					}
					
					$final_quote[$shipping_method['code']]['cost'] = round($final_quote[$shipping_method['code']]['cost'],2);
				}
				
				if($shipping_method['code']=='FRT'){
					$freight_info = $shipping_method;
				}
			}
			
			if($has_free_freight && !$final_quote['FRT']){
				$final_quote['FRT']['cost'] = 0;
				$final_quote['FRT']['info'] = $freight_info;
			}
			
			$debug['final_quote'] = $final_quote;
			if($CFG->backend){
				//mail('tova@tigerchef.com','shipping debug - all', var_export($debug,true));
			}
			return $final_quote;
		}
		
	}
	
	function getUPSRateFromDB($weight,$zip_code,$methodDataArray,$comOrRes){
		global $CFG;
				
		$method = $methodDataArray['code'];
		
		if($methodDataArray['name'] == 'FRT'){
			return false;
		}elseif(strpos($methodDataArray['name'], 'UPS') !== FALSE){ //make sure the method is UPS
			require_once("$CFG->libdir/upsRateCalculator.php");
			if( $weight > 150 ) {
			
				//New method, with all rates stored in the database:
				// 1) get the zone for this method's id
				$theZone = UPSRateCalculator::getZipZone($methodDataArray['id'],$zip_code);
					
				//echo "$theZone is theZone for id $methodDataArray[id] and zipcode $zip_code<br>";
				if ($theZone == 0) return 0; // doesn't ship there
				else // if we got a zone for this method and zip
				{
					// get the rate
					$newQuote = UPSRateCalculator::getUpsRateOver150($theZone, $weight, $comOrRes);
				}
			} else {
			
				// get the zone for this method's id
				$theZone = UPSRateCalculator::getZipZone($methodDataArray['id'],$zip_code);
			
				if ($theZone == 0) return 0; // doesn't ship there
				else // if we got a zone for this method and zip
				{
					// get the rate
					$newQuote = UPSRateCalculator::getUpsRate($methodDataArray['rate_table_name'],$theZone, $weight, $comOrRes);
				}
			}
		}else{
			return false;
		}
		
		$quote = round($newQuote, 2);
			
		if($method && $method != "GND"){
			if ($comOrRes == "RES") $quote = $quote + $CFG->ups_air_residential_surcharge;
			$quote = $quote + ($quote * $CFG->ups_air_fuel_surcharge);
			$quote = $quote + $CFG->expedite_ship_fee;
		} else  { // for ground--don't add if free shipping
			if ( $quote > 0)
			{
				if ($comOrRes == "RES") $quote = $quote + $CFG->ups_ground_residential_surcharge;
				$quote = $quote + ($quote * $CFG->ups_ground_fuel_surcharge);
			}
		}
		
		return $quote;
	}
	
	function getDefaultFreightCost($items){
		
		global $CFG;
		
		// calculate freight quote
		$freight_quote = 0;
		if ($items) {
			foreach ($items as $item)
			{
				// don't charge shipping for gift items
				if ($item['is_gift'] == 'Y'){ continue; }
	
				$product_info = $item['product_info'];
				//print_ar($product_info);
				
				if($product_info['weight'] == 0.00){
					$product_info['weight'] = "1.00"; // default to 1 lb for unknown weight
				}
				$total_product_weight = Weight::multiply($product_info['weight'], $item['qty']);
				
				// calculate freight weight separatly
				if(	Products::chargeShipping( $product_info ) &&
					(	strtolower($product_info['shipping_type']) == 'freight' ||
							($product_info['is_breakable']==='Y' && $total_product_weight > $CFG->ups_breakable_weight_limit ) ||
							($product_info['weight'] >  $CFG->ups_max_single_item_weight)||
							($total_product_weight >  $CFG->weight_for_freight_shipping)
					)
				){
					if ($product_info['free_shipping'] == 'Y'){					
						continue;
					}elseif ($product_info['freight_override']>0){
						if ($product_info['freight_override'] > $freight_quote){
							$freight_quote = $product_info['freight_override'];
						}
					} elseif ($product_info['brand_freight_override']>0){
						if ($product_info['brand_freight_override'] > $freight_quote){
							$freight_quote = $product_info['brand_freight_override'];
						}
					} elseif ($CFG->freight_ship_fee > $freight_quote){
						$freight_quote = $CFG->freight_ship_fee;
					}
				}
			}
		}
		
		return $freight_quote;
	}

	static function OLDcalcShipping( $id = 0, $type='cart', $info='', $zip_code='', $product_qty=0, $method_override=false, $hardCodedMethod = "", $onlyOnNonDiscounted = false, $cart_reference='', $paypal_token='', $options='')
	{
		global $CFG, $cart, $cart2;
	
		if($type == 'orders'){
			$order = Orders::get1( $id );
		}
	
		if($hardCodedMethod != ""){
			$method = $hardCodedMethod;
		} else {
			if($type == 'orders'){
				$method = ShippingMethods::get1($order['shipping_id']);
				$method2 = ShippingMethods::get1($order['shipping_id_2']);
	
				$method = $method['code'];
				$method2 = $method2['code'];
					
			}else{
				$method = $cart->data['shipping_method'];
				$method2 = $cart->data['shipping_method_2'];
			}
		}
		if (!$method) $method = "GND";
	
		$items = Shipping::getItemsForShipping( $type, $method, $id, $product_qty, $onlyOnNonDiscounted, $cart_reference, $paypal_token);
		$weight_info = Shipping::calcShippingWeight($type,$id,$product_qty,$items,$onlyOnNonDiscounted,$method,$cart_reference,$paypal_token);
		$weight = $weight_info['weight'];
		$free_weight = $weight_info['free_weight'];
	
		if($weight > $CFG->weight_for_freight_shipping){
			$method = $CFG->freight_shipping_code;
			$method2 = '';
			if($type == 'cart'){
				$cart->data['shipping_method'] = $CFG->freight_shipping_code;
				$cart->data['shipping_method_2'] = '';
			}
		}
	
		if($type == 'orders'){
			if(!$zip_code)
			{
				$zip_code = $order['shipping_zip'];
			}
			$shipping_po_box = preg_match("/^box[^a-z]|(p.?[- ]?o.?[- ]?|post office )b(.|ox)/i", $order['shipping_address1']);
		}elseif($type == 'cart'){
			if(!$zip_code){
				$zip_code = $cart->data['zipcode'];
			}
			if($cart_reference){
				$shipping_po_box = ($options['po_box'] == 'po_box') ? true : false ;
			}else{
				$shipping_po_box = ($cart->data['shipping_po_box'] == 'po_box') ? true : false ;
			}
		}
	
		if($shipping_po_box && $type != 'product'){
			$validate_po_box = Shipping::validate_po_box('',$type,$id);
			if(!$validate_po_box['can_ship']){
				// if some items can't be shipped to a po box don't calculate shipping
				return 0;
			}
		}
	
		if($type == 'orders'){
			if($order['current_status_id'] == $CFG->government_quote_status_id){
				$quote = 0;
				foreach ($items as $item){
					if($method == $CFG->freight_shipping_code || $method2 == $CFG->freight_shipping_code){
						if($item['shipping_type'] == 'Freight'){
							$quote += $item['bid_shipping'];
						}
					}
					if($item['shipping_type'] !== 'Freight'){
						$quote += Shipping::calcShipping($item['product_id'],'product','',$item['shipping_zip'],$item['qty'],false,$method);
					}
				}
				return $quote;
			}
		}
	
		// calculate freight quote
		$freight_quote = 0;
		if($method == $CFG->freight_shipping_code || $method2 == $CFG->freight_shipping_code || $weight > $CFG->weight_for_freight_shipping){
			if ($items) {
				foreach ($items as $item)
				{
					// don't charge shipping for gift items
					if ($item['is_gift'] == 'Y'){ continue; }
						
					$product_info = $item['product_info'];
					//print_ar($product_info);
	
					if(strtolower($product_info['shipping_type']) == 'freight' || $weight > $CFG->weight_for_freight_shipping){
						if ($product_info['free_shipping'] == 'Y'){
							continue;
						}elseif ($product_info['freight_override']>0){
							if ($product_info['freight_override'] > $freight_quote){
								$freight_quote = $product_info['freight_override'];
							}
						} elseif ($product_info['brand_freight_override']>0){
							if ($product_info['brand_freight_override'] > $freight_quote){
								$freight_quote = $product_info['brand_freight_override'];
							}
						} elseif ($CFG->freight_ship_fee > $freight_quote){
							$freight_quote = $CFG->freight_ship_fee;
						}
					}
				}
			}
	
		}
		if( $method == 'GND' && $CFG->use_flat_rate_shipping )
		{
			$charge_shipping = false;
			if( ($type == 'cart' || $type == 'orders') && $items ) {
				foreach( $items as $item )
				{
					$charge_shipping = Products::chargeShipping( $item['product_id'] );
				}
				 
				if( $charge_shipping )
					return $CFG->flat_shipping_rate;
				else
					return 0.00;
			} else {
				$product_info = Products::get1($id);
	
				$charge_shipping = Products::chargeShipping( $id );
				if( $charge_shipping )
					return $CFG->flat_shipping_rate;
				else
					return 0.00;
			}
		}
		elseif( $shipping_po_box && $method == 'GND' && !$method2 )
		{
			//add per order charge
			$quote = $CFG->po_box_order_charge;
				
			//add per pound charge
			$quote += ceil($weight) * $CFG->po_box_per_pound_charge;
				
			return $quote;
		} else {
	
			if($method != $CFG->freight_shipping_code){
	
				if( !$weight )
					return 0.00;
				$weight = ceil($weight);
	
				//Duplicate code from ajax.calculate_shipping.php, but I have to recalc if they added something else.
				$shipper = UPSShipper::get1($CFG->default_from_shipper);
	
				$methodInfo = ShippingMethods::get('', '', '',$method,'','');
	
				if ($cart2['shipping_location'] == 'residential'
						|| (!$cart2['shipping_location'] && !$cart->data['shipping_location'] && !$order['location_type'])
						|| (!$cart2['shipping_location'] && $cart->data['shipping_location'] == 'residential' )
						|| $order['location_type'] == 'Residential'
								|| $options['shipping_location'] == 'Residential')
				{
					$comOrRes = "RES";
				} else {
					$comOrRes = "COM";
				}
	
				$orig_weight = $weight;
					
				if( stristr( $zip_code, '-' ) )
				{
					$parts = explode( '-', $zip_code );
					$zip_code = $parts[0];
				}
	
				//$weight -= $free_weight;
	
				if( $weight == 0 ) {
					$quote = 0.00;
				} else if( $weight > 150 ) {
						
					//New method, with all rates stored in the database:
					// 1) get the zone for this method's id
					$methodDataArray = $methodInfo[0];
					require_once("$CFG->libdir/upsRateCalculator.php");
					$theZone = UPSRateCalculator::getZipZone($methodDataArray['id'],$zip_code);
	
					//echo "$theZone is theZone for id $methodDataArray[id] and zipcode $zip_code<br>";
					if ($theZone == 0) return 0; // doesn't ship there
					else // if we got a zone for this method and zip
					{
						// get the rate
						$newQuote = UPSRateCalculator::getUpsRateOver150($theZone, $weight, $comOrRes);
					}
				} else {
					//echo "in this else";
					$rate = new UpsQuote();
					$rate->upsProduct($method); // See upsProduct() function for codes
					$rate->origin($shipper['zip'], "US"); // Use ISO country codes!
					$rate->dest($zip_code, "US"); // Use ISO country codes!
					$rate->rate("RDP"); // See the rate() function for codes
					$rate->container("CP"); // See the container() function for codes
					$rate->weight($weight);
					$rate->rescom($cart2['shipping_location'] == 'residential' ? "RES" : "COM"); // See the rescom() function for codes
					//$quote = $rate->getQuote();
					//print_ar( $quote );
						
					require_once("$CFG->libdir/upsRate.php");
					$myRate = new upsRate();
					$myRate->setCredentials($CFG->ups_key,$CFG->ups_user,$CFG->ups_pass,$shipper['ups_account_number']);
					//$quote = $myRate->getRate($shipper['zip'],$zip_code,$methodInfo[0]['api_code'],$weight);
												
					//New method, with all rates stored in the database:
					// 1) get the zone for this method's id
					$methodDataArray = $methodInfo[0];
					require_once("$CFG->libdir/upsRateCalculator.php");
					$theZone = UPSRateCalculator::getZipZone($methodDataArray['id'],$zip_code);
					//echo "$theZone is theZone for id $methodDataArray[id] and zipcode $zip_code<br>";
	
					if ($theZone == 0) return 0; // doesn't ship there
					else // if we got a zone for this method and zip
					{
						// get the rate
						//$weight -= $free_weight;
						$newQuote = UPSRateCalculator::getUpsRate($methodDataArray['rate_table_name'],$theZone, $weight, $comOrRes);
					}
					 
				}
					
				$newQuote = round($newQuote, 2);
				$quote = $newQuote;
	
				if($method && $method != "GND"){
					if ($comOrRes == "RES") $quote = $quote + $CFG->ups_air_residential_surcharge;
					$quote = $quote + ($quote * $CFG->ups_air_fuel_surcharge);
					$quote = $quote + $CFG->expedite_ship_fee;
				} else  { // for ground--don't add if free shipping
					if ( $quote > 0)
					{
						if ($comOrRes == "RES") $quote = $quote + $CFG->ups_ground_residential_surcharge;
						$quote = $quote + ($quote * $CFG->ups_ground_fuel_surcharge);
					}
				}
	
			}
			//add freight cost
			$quote = $quote + $freight_quote;
	
		}
	
		// Store in Session
		if($type == 'cart'){
			$cart->data['shipping_cost'] = round($quote,2);
			$cart->data['shipped_weight'] = $orig_weight;
		}
	
		return $quote;
	}
	static function calcLiftgateFee($id=0,$type='cart',$cart_reference=''){
		
		global $CFG;
		global $cart;
		
		/*
		if($type == 'cart'){
			$items = $cart->get('','','',"all",'','',$cart_reference);
		}else{
			$items = Orders::getItems(0 , $id);
		}

		$liftgate = false;
		foreach ($items as $item){
			$product_info = Products::get1($item['product_id']);
			
			if($product_info['liftgate'] == 'Y'){
				$liftgate = true;
				break;
			}
		}
		*/
		$liftgate = true;

		return $liftgate ? $CFG->liftgate_fee : '0.0';
	}
	

	function getItemsForShipping( $type, $method, $id='', $product_qty='',$onlyOnNonDiscounted=false, $cart_reference='', $paypal_token='', $group_by_brand=false){
		global $cart;
		
		switch ($type) {
			case 'product':
				$items = array(array('product_id' => $id, 'qty'=>$product_qty));
				break;
			case 'cart':
				if ($onlyOnNonDiscounted && $method == "GND" ){
					$items = $cart->get('', '', '', "nonQualifying", "shipping", '', $cart_reference, $paypal_token);
					//cheaper	
				} else { 
					$items = $cart->get('', '', '', '', '', '', $cart_reference, $paypal_token);
					//everything
					//all rates - should be here 
				}
				//mail('tova@tigerchef.com', 'non qualifying ', var_export($items,true). var_export($onlyOnNonDiscounted,true).var_export($method,true));
				
				break;
			case 'orders':
				if ($onlyOnNonDiscounted && $method == "GND" ){
					$items = Orders::getItems(0,$id,'','',0,'','',0,0,0,0,0,'','','',0,0,"nonQualifying","shipping");
				} else {
					$items = Orders::getItems(0 , $id);
				}
				break;
		}
		
		foreach ($items as $key=>$item){
			$items[$key]['product_info'] = Products::get1($item['product_id']);
		}
		
		return $items;
	}
	
	
	
	function calcShippingWeight($type,$id='',$product_qty='',$items='', $onlyOnNonDiscounted=false,$method='',$cart_reference='',$paypal_token='',$free_shipping_subtotal_min_qualifies=false){
		
		global $CFG;
				
		if(!$items){
			$items = Shipping::getItemsForShipping( $type, $method, $id, $product_qty, $onlyOnNonDiscounted, $cart_reference, $paypal_token); 
		}
		
		
		foreach ($items as $item)
		{
			$item_names[] = array('name'=>$item['name'],'qty'=>$item['qty'],'weight'=>$item['weight'],'brand'=>$item['brand_id']);
		}		
		
		//if(!$method){$method = 'GND';}
		
		$weight = $free_weight = $free_freight_weight = $freight_weight = $total_ups_weight = 0.00;
		// Calc Weight
		if ($items) {
			foreach ($items as $item)
			{
				// don't charge shipping for gift items
				if ($item['is_gift'] == 'Y'){ continue; }
					
				$product_info = $item['product_info'];
				//print_ar($product_info);
				
				if($product_info['weight'] == 0.00){
					$product_info['weight'] = "1.00"; // default to 1 lb for unknown weight
				}
				
				$total_product_weight = Weight::multiply($product_info['weight'], $item['qty']);
				
				/*
				 * We provide the dimensions to the API - don't need to add into weight - TE 1/15
					
				// if dimensions (width, height, and length) are set for the item,
				// check if it would have dimensional weight for UPS
				if ($product_info['width'] > 0 && $product_info['height'] > 0 && $product_info['length'] > 0)
				{
				$dimensions_multiplied = round($product_info['width']) * round($product_info['height']) * round($product_info['length']);
				if ($dimensions_multiplied > 5184) $product_info['weight'] = round($dimensions_multiplied / 166);
				}
				*/
				
				$charge_shipping = Products::chargeShipping( $product_info['id'] );
				$debug[$item['id']]['name'] = $product_info['name'];
				$debug[$item['id']]['charge_shipping1'] = $charge_shipping;
				$debug[$item['id']]['shipping_type']= $product_info['shipping_type'];
				
				// calculate freight weight separatly
				if(	
				(	strtolower($product_info['shipping_type']) == 'freight' ||
						($product_info['is_breakable']==='Y' && $total_product_weight > $CFG->ups_breakable_weight_limit ) ||
						$product_info['weight'] >  $CFG->ups_max_single_item_weight
				)
				){
					if( $charge_shipping ){
						$freight_weight += $total_product_weight;
						$freight_class = min($freight_class,$product_info['freight_class']);
					}else{
						$free_freight_weight += $total_product_weight;
					}
				//} elseif ( $method == 'GND' && !$charge_shipping ) {
				//	//Skip on this condition - ground shipping and the item has free shipping, don't calculate the weight in
				//	$free_weight += $total_product_weight;
				} else{
					$debug['onlynondiscounted']= $onlyOnNonDiscounted;
					if($free_shipping_subtotal_min_qualifies && $onlyOnNonDiscounted){
						$charge_shipping = false;  
					}
					$debug[$item['id']]['charge_shipping2'] = $charge_shipping;

					if(!$charge_shipping && $onlyOnNonDiscounted){
						$free_weight += $total_product_weight;
					}else{
						$total_ups_weight += $total_product_weight;
						
                        // if dimensions (width, height, and length) are set for the item,
                        // check if it would have dimensional weight for UPS
                        $largest_item['dimensional_weight'] = 0;
                        if ($product_info['pkg_width'] > 0 && $product_info['pkg_height'] > 0 && $product_info['pkg_length'] > 0)
                        {
                            $dimensional_weight = round($product_info['pkg_width'] * $product_info['pkg_height'] * $product_info['pkg_length'] / $CFG->dimensional_weight_divisor);

                            //save dimensions of largest item
                            if( ($dimensional_weight < $CFG->dimensional_weight_limit) &&  ($dimensional_weight > $largest_item['dimensional_weight'])){
                                $largest_item['dimensional_weight'] = $dimensional_weight;
                                $largest_item['dimensions'] = array('length'=>$product_info['pkg_length'],'width'=>$product_info['pkg_width'],'height'=>$product_info['pkg_height']);
                            }
                        }

						if($product_info['ships_in_own_box']=='Y' || $dimensional_weight > $CFG->dimensional_weight_limit){
							$weight_all_pkgs += $total_product_weight;
							for($i = 1; $i <= $item['qty']; $i++){
								$pkgs[] = array('weight'=>$product_info['weight'],'dimensions'=>array('length'=>$product_info['pkg_length'],'width'=>$product_info['pkg_width'],'height'=>$product_info['pkg_height']));

								}
						} else {
							$weight += $total_product_weight;
							$weight_all_pkgs += $total_product_weight;
						}
					}
				}

				if($product_info['shipping_cost']>0){
					$custom_shipping_fees += $product_info['shipping_cost']; 
				}
				
				$debug[$item['id']]['freight_weight'] = $freight_weight;
				$debug[$item['id']]['free_freight_weight'] = $free_freight_weight;
				$debug[$item['id']]['weight'] = $weight;
				$debug[$item['id']]['free_weight'] = $free_weight;
				
			}
			//mail('tova@tigerchef.com','shipping weight2','onlyOnNonDiscounted:'.var_export($onlyOnNonDiscounted,true)." method:".var_export($method,true). "  ".var_export($weight,true)."  ".var_export($free_weight,true));	
		}
		$debug['freight_weight'] = $freight_weight;
		$debug['total_ups_weight'] = $total_ups_weight;
		// if we have freight - the whole brand will ship freight
		// freight weight will be the sum of all the weights
		if($freight_weight>0 || $total_ups_weight > $CFG->weight_for_freight_shipping || $free_weight > $CFG->weight_for_freight_shipping){
			
			if($charge_shipping){
				$freight_weight += $total_ups_weight;
				$total_ups_weight = 0.00;
			}else{
				$free_freight_weight = $total_ups_weight ? $total_ups_weight : $free_weight;
			}
		}
			
		//devide the ups weight into packages for ups
		if($total_ups_weight > 0){
			$num_pkgs = ceil($weight/$CFG->ups_max_pkg_weight);
			for($i=0; $i<$num_pkgs; $i++){
                $pkgs[] = array('weight'=> $weight / $num_pkgs,'dimensions'=>$largest_item['dimensions']);
			}
		}
		//mail('dina.websites@gmail.com','frt shipping - '.$product_info['sku'],var_export($debug,true));
		$return_array = array(	'weight'=>$weight_all_pkgs,
								'pkgs'=>$pkgs,
								'free_weight'=>$free_weight,
								'freight_weight'=>$freight_weight,
								'free_freight_weight'=>$free_freight_weight,
								'freight_class'=>$freight_class,
								'custom_shipping_fees'=>$custom_shipping_fees, 
								'dimensions'=>array('length'=>$product_info['length'],'width'=>$product_info['width'],'height'=>$product_info['height'])
						);
		
		return $return_array;
	}
	
	function getShippingOptions($zip_code,$type='cart',$paypal_token='',$id='',$cart_reference='',$shipping_method='',$options='',$shipping_options = null){
	
		global $CFG;
	
		if($type == 'orders'){
			$order = Orders::get1( $id );
	
			$shipping_po_box = preg_match("/^box[^a-z]|(p.?[- ]?o.?[- ]?|post office )b(.|ox)/i", $order['shipping_address1']);
			$selected_method = ShippingMethods::get1($order['shipping_id']);
			$selected_method = $selected_method['code'];
			$order_method = $selected_method;
			
			$selected_method2 = ShippingMethods::get1($order['shipping_id_2']);
			$selected_method2 = $selected_method2['code'];
			
			//for old orders with shipping_method_2
			if($selected_method2 == 'FRT'){
				Orders::update($id,array('shipping_id'=>7, 'shipping_id_2'=>0));
			}
			
		}elseif($type == 'cart'){
			if($cart_reference){
				$cart = new Cart();
				$cart_items = $cart->get('','','','','','',$cart_reference);
				$cart->data['shipping_po_box'] = $options['po_box'];
				$cart->data['shipping_location'] = $options['shipping_location'];
			}else{
				global $cart;
				$selected_method = $cart->data['shipping_method'];
				//$selected_method2 = $cart->data['shipping_method_2'];
			}
			$shipping_po_box = ($cart->data['shipping_po_box'] == 'po_box') ? true : false ;
		}
		
		$zip_code = trim($zip_code);
		//$debug['shipping_options1']=$shipping_options;
		if (!$selected_method){ $selected_method = "GND"; }
	//print_ar($shipping_options);
		if($shipping_options == null){
			//$shipping_options = Shipping::calcShipping( $id, $type, '', $zip_code, 0, false, "", false, '', '', '', true);
			$shipping_options = Shipping::calcShipping($id, $type, '', $zip_code,'', false, '', false, $cart_reference, $paypal_token,$options,true,$err_msg);
			$debug['in_if']=true;
			if($err_msg){
				$shipping_costs[] = Array(
						'descr' => '<span>'. $err_msg .'</span>',
						'descr_str' => $err_msg,
						'error_msg' => $err_msg);
				return $shipping_costs;
			}
			//$debug['shipping_options1.1']=$shipping_options;

			if($shipping_options['GND']){
				//for ground - we have to calculate the rate with promocodes
				$shipping_options['GND']['cost'] = Shipping::calcShipping($id, $type, '', $zip_code, 0, true, "GND", true, $cart_reference, $paypal_token, $options);
			}
			if($shipping_options['FRT']){
				//for ground - we have to calculate the rate with promocodes
				$shipping_options['FRT']['cost'] = Shipping::calcShipping($id, $type, '', $zip_code, 0, true, "FRT", true, $cart_reference, $paypal_token, $options);
			}
		}
		//$debug['shipping_options2']=$shipping_options;

		$freight_cost = $shipping_options['FRT']['cost'];
						
		//If there is a freight charge - combine FRT and GND and only give FRT option
		if(isset($shipping_options['FRT']) && isset($shipping_options['GND'])){
			$frt_option = $shipping_options['FRT'];
			$frt_option['cost'] += $shipping_options['GND']['cost'];
			unset($shipping_options);
			$shipping_options['FRT'] = $frt_option;
		}
		
		foreach ($shipping_options as $shipping_option){

			if($shipping_option['info']['code'] == 'FRT'){
				$total_shipping = round($shipping_option['cost'],2);
			}else{
				$total_shipping = round($shipping_option['cost'] + $freight_cost,2);
			}
	
			$quote = number_format($total_shipping, 2);
			$option_cost = number_format($shipping_option['cost'],2);
			
			if($total_shipping == 0){
				$shipping_option['info']['descr'] = "FREE SHIPPING";  
			} 
	
			$name = $shipping_option['info']['descr'];
			$descr ='<span>'. $shipping_option['info']['descr'] . "</span><span class=\"price\">$" . $option_cost . '</span>';
			$descr_str = $shipping_option['info']['descr'] . " - $" . $option_cost;

			$shipping_costs[$shipping_option['info']['code']] = Array(
					'name' => $shipping_option['info']['descr'],
					'descr' => $descr,
					'descr_str' => $descr_str,
					'code' => $shipping_option['info']['code'],
					'cost' => number_format($total_shipping,2),
					'options_cost' =>$option_cost,
					'name' => $name,
					'id' => $shipping_option['info']['id'],
					'adjusted_sales_tax' => '0.00',
					'cost_unformated' => $total_shipping,
					'info' => $shipping_option['info']['id']. ":" . $total_shipping,
					'data' => array('name' => 'options_cost', 'value' => $option_cost)
			);
			 
		}
		
		$cart->data['shipping_method'] = ($cart->data['shipping_method']) ? $cart->data['shipping_method'] : 'GND';
		/*
		if($freight_cost){
			if(!isset($shipping_costs['GND'])){
				$cart->data['shipping_method'] = 'FRT';
			}else{
				$cart->data['shipping_method'] = 'GND';
			}
		}
		*/ 
		$debug['shipping_costs'] = $shipping_costs;

		if(!isset($shipping_costs[$selected_method])){
			$keys = array_keys($shipping_costs);
			if($shipping_options['FRT'] && !in_array('GND',$keys)){
				$selected_method = 'FRT';
			}else{
				$selected_method = 'GND';
			}
		}
		if($type == 'orders' && $order_method != $selected_method){
			$method = db_get1(ShippingMethods::get(0,'','',$selected_method));
			Orders::update($id,array('shipping_id'=>$method['id']));
			
			/*
			var_dump( array($method,$selected_method) );
			$order2 = Orders::get1( $id );
			$test_method = ShippingMethods::get1($order2['shipping_id']);
			var_dump($test_method);
			*/
		}elseif($type == 'cart'){
			$cart->data['shipping_method'] = $selected_method;
		}
		$debug['cart_shipping_method'] = $cart->data['shipping_method'];
		//mail('tova@tigerchef.com','shipping options',var_export($debug,true));

		return $shipping_costs;
	}
	function get_shipping_display($shipping_costs,$sel_method,$shipping_discount,$sales_tax_rate_to_use,$radio_buttons = true,$show_liftgate_options = false, $type = 'cart', $id){
	
		global $cart;
		global $CFG;
		
		if(!$sel_method || $sel_method == 'undefined'){ $sel_method = 'GND'; }
		
		//$return_str .= '<pre>'.var_export($sel_method,true).'</pre>';
		//$return_str .= '<pre>'.var_export($shipping_costs,true).'</pre>';
		
		
		if(isset($shipping_costs[0]['error_msg'])){
			$return_str = "<div class='error_msg'>{$shipping_costs[0]['error_msg']}</div>";
			return $return_str;
		}
			
		if($type == 'orders'){
			$order = Orders::get1( $id );
			$has_standard_shipping = ($sel_method != $CFG->freight_shipping_id)?true:false;
			$selected_method = ShippingMethods::get1($order['shipping_id']);
			$sel_method_code = $selected_method['code'];
		}else{
			//$has_standard_shipping = is_array($cart->data['shipping_type']['standard_brands'])?true:false;
			$has_standard_shipping = isset($shipping_costs['GND'])?true:false;
			if(!$sel_method || $sel_method == 'undefined'){ $sel_method = $cart->data['shipping_method']; }
			$sel_method_code = $sel_method;
		}	
				
		$validate_po_box = Shipping::validate_po_box();
		
		if($validate_po_box['can_ship']){

			$freight_cost = $shipping_costs['FRT'];			
			unset($shipping_costs['FRT']);
			
			if($freight_cost){
				
				if($has_standard_shipping){
					$cost = $shipping_costs[$selected_method['id']]['cost'];
				}else{
					$cost = $freight_cost['cost'];
				}
				/*
				if($cost > 0){
					$name = "{$freight_cost['name']} Shipping";
				}else{
					$name = $freight_cost['name'];
				}
				*/
				$return_str .= "<span class='hidden' id='shipping_method' data-shipping-method='FRT'></span>";
				$return_str .= "<div class='freight-shipping-cost col-1'>{$freight_cost['name']}:</div>";

				$return_str .= "<div class='freight-shipping-cost col-2'><span>$</span><span class='cost freight_amt'>{$cost}</span></div>";
			}else{
				if($type == 'orders'){
					$return_str .= StdLib::getTagFromHashtable("info[shipping_id]",$shipping_costs,$selected_method['id'],'id','descr_str','radio','changeShipping(this)', true, "ship_radio",'options_cost');
				}else{
					if(count($shipping_costs) > 1){
						if($radio_buttons){
							$return_str .= StdLib::getTagFromHashtable("shipping_method",$shipping_costs,$sel_method,'code','descr_str','radio','calcShippingStep3(this,'.$shipping_discount.','.$sales_tax_rate_to_use.')', true, "ship_radio",'options_cost');
						}else{
							$i = $group_num = 1;
							foreach ($shipping_costs as $shipping_option){
								$group_class = 'group-'.$group_num;
								$return_str .= '<div class="shipping-button ' . $group_class;
								$return_str .= ($shipping_option['code']==$sel_method)?' active':'';
								$return_str .= '" type="button"  data-shipping-method="'.$shipping_option['code'].'" ';
								$return_str .= 'onclick="calcShippingStep3(this,'.$shipping_discount.','.$sales_tax_rate_to_use.')"';
								$return_str .= '				><ul><li>'.$shipping_option['name'].'</li><li>$'.$shipping_option['cost'].'</li></ul></div>';
									
								if(($i++ % 2)==0){$group_num++;}
							}
						}
					}else{
						$keys = array_keys($shipping_costs);
						if($shipping_costs[$keys[0]]['error_msg']){
							return $shipping_costs[$keys[0]]['error_msg'];
						}
						if($shipping_costs[$keys[0]]['cost'] == 0){
							return "FREE SHIPPING - $0.00" . "<span class='hidden' id='shipping_method' data-shipping-method='{$sel_method}'></span>";
						}
						
						$return_str .= "<div class='ups-shipping-choices col-1'>{$shipping_costs[$keys[0]]['name']}:</div>";
						$return_str .= "<span class='hidden' id='shipping_method' data-shipping-method='{$sel_method}'></span>";
						
						if(!$CFG->backend && $CFG->disable_expedited_shipping){
							$return_str .= "<div class='ups-shipping-choices col-2'><span class='cost'><span>$</span><span class='standard_shipping_amt'>{$shipping_costs[$keys[0]]['cost']}</span></div>";
							$return_str .= "<div class='disabled-expedited-shipping-msg'>Please <a href='/{$CFG->contact_us_url}'>contact us</a> for expedited shipping options</div>"; 
						}
					}
				}
			}
			
			//$return_str .= var_export($has_standard_shipping,true);
			//$return_str .= var_export($freight_cost,true);
			
			//check if any items on the order require liftgate charges
			if($freight_cost){
				
				$return_str .= '<span class="hidden" id="charge_liftgate"></span>';	
				
				$liftgate_fee = Shipping::calcLiftgateFee('','cart');
				//if($liftgate_fee>0){
					
				if($show_liftgate_options){
					$return_str .= '<div style="clear:both;"></div>';	
					$return_str .= '<span class="shipping-question">Do you need a liftgate?</span>';
					$return_str .= '<div class="liftgate-holder">';
					$return_str .= '<div class="ship_radio">';
					$return_str .= '<input type="radio" name="liftgate" id="liftgate_rdbtn_Y" value="liftgate_Y" onclick="toggle_liftgate(this,'.$sales_tax_rate_to_use.')"' ;
					$return_str .= ($cart->data['charge_liftgate_fee']) ? 'checked' : '' ;
					$return_str .= '/><label for="liftgate_rdbtn_Y">Yes, please provide a liftgate for delivery - $ ';
					$return_str .= (is_numeric($liftgate_fee)) ? number_format($liftgate_fee, 2) : "0.00" ;
					$return_str .= '</label>';
					$return_str .= '</div>';
					$return_str .= '<div class="ship_radio">';
					$return_str .= '<input type="radio" name="liftgate" id="liftgate_rdbtn_N" value="liftgate_N" onclick="toggle_liftgate(this,'.$sales_tax_rate_to_use.')"';
					$return_str .= ($cart->data['charge_liftgate_fee'] === false)?'checked':'' ;
					$return_str .= '/><label for="liftgate_rdbtn_N">No, I have a liftgate/loading dock </label>';
					$return_str .= '</div>';
					$return_str .= '</div>';
				}
				
				//}
			}
					
		}else{
			$return_str = "<span class='error_msg'>" . $validate_po_box['msg'] . "</span>";
		}
		 	
		return $return_str;
	}
	
	function getShippingOptionsCombined($zip_code,$type='cart',$paypal_token='',$id='',$cart_reference='',$shipping_method='',$options='',$shipping_options = null){
    
    	global $CFG; 
    	 
    	if($type == 'orders'){
    		$order = Orders::get1( $id );
    		
    		$shipping_po_box = preg_match("/^box[^a-z]|(p.?[- ]?o.?[- ]?|post office )b(.|ox)/i", $order['shipping_address1']);
    		$selected_method = ShippingMethods::get1($order['shipping_id']);
    		//$selected_method2 = ShippingMethods::get1($order['shipping_id_2']); 
    		$selected_method = $selected_method['code'];
    		//$selected_method2 = $selected_method2['code'];
    	}elseif($type == 'cart'){
    		if($cart_reference){
    			$cart = new Cart();
    			$cart_items = $cart->get('','','','','','',$cart_reference);
    			$cart->data['shipping_po_box'] = $options['po_box'];
    			$cart->data['shipping_location'] = $options['shipping_location'];    			
    		}else{
    			global $cart;
	    		$selected_method = $cart->data['shipping_method'];
	    		//$selected_method2 = $cart->data['shipping_method_2'];
    		}
    		$shipping_po_box = ($cart->data['shipping_po_box'] == 'po_box') ? true : false ;
    	}

    	if (!$selected_method){ $selected_method = "GND"; }
    	   
    	if($shipping_options == null){    		
    		//$shipping_options = Shipping::calcShipping( $id, $type, '', $zip_code, 0, false, "", false, '', '', '', true);
    		$shipping_options = Shipping::calcShipping($id, $type, '', $zip_code,'', false, '', false, $cart_reference, $paypal_token,$options,true,$err_msg);
    		
    		if($err_msg){
    			$shipping_costs[] = Array(
    					'descr' => '<span>'. $err_msg .'</span>',
    					'descr_str' => $err_msg );
    			return $shipping_costs;
    		}
    		
    		if($shipping_options['GND']){
	    		//for ground - we have to calculate the rate with promocodes
	    		$shipping_options['GND']['cost'] = Shipping::calcShipping($id, $type, '', $zip_code, 0, true, "GND", true, $cart_reference, $paypal_token, $options);
    		}    	
    	}
    	 
      	//check if this order has freight items
    	if($shipping_options['FRT']){
	    	$freight_method = $shipping_options['FRT'];
	    	unset($shipping_options['FRT']);
    	}
    	//check if this order is flat rate GND
//     	if($shipping_options['flat_rate_GND']){
//     		$flat_rate = $shipping_options['flat_rate_GND']['cost'];
//     		unset($shipping_options['flat_rate_GND']);
//     	}
    	 
    	//check if this order is only freight
    	if(count($shipping_options)==0){
    		
    		$shipping_costs[] = Array(
    				'descr' => '<span>'. $freight_method['info']['descr'] . '</span><span class="price">$' . number_format($freight_method['cost'],2) .'</span>',
    				'descr_str' => $freight_method['info']['descr'] . " - $" . number_format($freight_method['cost'],2),
    				'code' => $CFG->freight_shipping_code,
    				'cost' => number_format($freight_method['cost'],2),
    				'name' => $freight_method['descr'],
    				'adjusted_sales_tax' => '0.00',
    				'cost_unformated' => $freight_method['cost'],
    				'id' => $freight_method['info']['id'],
    				'info' => $CFG->freight_shipping_id. ":" . $freight_method['cost']
    		);
    		return $shipping_costs;
    	}
    	
    	foreach ($shipping_options as $shipping_option){
    		
    		$freight_cost = $freight_method['cost'];
    		
//     		if($flat_rate){
//     			if($shipping_option['info']['code'] == 'GND'){
//     				$freight_cost = 0;
//     				$shipping_option['cost'] = $flat_rate;
//     			}
//     		}
    		
    		$total_shipping = round($shipping_option['cost'] + $freight_cost,2);
    		if($shipping_option['info']['code'] == 'GND' && $total_shipping > $CFG->max_ground_shipping_cost){
    			$total_shipping = $CFG->max_ground_shipping_cost;
    		}
    		$quote = number_format($total_shipping, 2);
    		
    		if($freight_method){
    			$name = $shipping_option['info']['descr'] . ' / ' . $freight_method['info']['descr'];
    			$descr = '<span>'. $shipping_option['info']['descr'] . ' / '.$freight_method['info']['descr'].'</span><span class="price">$' . number_format($quote,2);
    			$descr_str = $shipping_option['info']['descr'] . ' / '.$freight_method['info']['descr'].' - $' . $quote;
    		}else{
    			$name = $shipping_option['info']['descr'];
    			$descr ='<span>'. $shipping_option['info']['descr'] . "</span><span class=\"price\">$" . $quote . '</span>';
    			$descr_str = $shipping_option['info']['descr'] . " - $" . $quote;
    		}
    		
    		$shipping_costs[] = Array(
    				'descr' => $descr,
    				'descr_str' => $descr_str,
    				'code' => $shipping_option['info']['code'],
    				'cost' => number_format($total_shipping,2),
    				'name' => $name,
    				'id' => $shipping_option['info']['id'],
    				'adjusted_sales_tax' => '0.00',
    				'cost_unformated' => $total_shipping,
    				'info' => $shipping_option['info']['id']. ":" . $total_shipping
    		);    		
    			
    	} 
    	return $shipping_costs;
    }

    function validate_po_box($shipping_address='',$type='cart',$order_id=0, $po_box_address='', $cart_reference='',$paypal_token=''){
    
    	global $CFG;
    	global $cart;
    	global $checkout_obj;
//     	mail('tova@tigerchef.com', 'validate po1', var_export($shipping_address,true).var_export($paypal_token,true));
    	 
    	//if we got a shipping address, clear the cart setting so we don't rely on previous settings
    	if($shipping_address && $cart->data['shipping_po_box']){
    		unset($cart->data['shipping_po_box']);
    	}    	 
    	
    	if ( $po_box_address || ($type == 'cart' && $cart->data['shipping_po_box']) || preg_match("/^box[^a-z]|(p.?[- ]?o.?[- ]?|post office )b(.|ox)/i", $shipping_address)){ // then it is a PO box
    		//mail('tova@tigerchef.com', 'validate po2', var_export($shipping_address,true).var_export($paypal_token,true).var_export($not_po_box_allowed_items,true));
    		
			// BEGIN - prevent shipping to PO boxes in all cases -- TE
            if($type == 'cart'){
                $cart->data['shipping_po_box'] = 'po_box';
            }
            return array('can_ship' => false, 'msg' => '<div class="error_msg">Please use a non-PO Box Shipping address.</div>');
            // END - prevent shipping to PO boxes in all cases -- TE
			
    		//get items that can't ship to po boxes
    		if($type == 'orders'){
    			$not_po_box_allowed_items = Orders::getItems(0,$order_id,'','',0,'','',0,0,0,0,0,'','','',0,0,'not_po_box_allowed');
    		} else {
    			$cart->data['shipping_po_box'] = 'po_box';
    			$not_po_box_allowed_items = $cart->get('','','','not_po_box_allowed','','',$cart_reference,$paypal_token);  
    		}
    		
    		if($not_po_box_allowed_items){
    			foreach ($not_po_box_allowed_items as $item){
    				$stock = db_get1(Inventory::get(0,0,0,'','',$item['product_id'],$item['product_option_id'],0,$CFG->default_inventory_location));
    				if($item['qty'] > $stock['qty']){
    					$item_str .= "<li>" . $item['mfr_part_num'] . "</li>";
    				}
    			}
    			if($item_str){
	    			$msg = sprintf('<div class="error_msg">%s<ul>%s</ul>%s</div>', 'The following items cannot be shipped to a PO box:',$item_str,'Please use a non-PO Box Shipping address.');
	    			return array('can_ship' => false, 'msg' => $msg);
    			}
    		}
    		//mail('tova@tigerchef.com', 'validate po4', var_export($not_po_box_allowed_items,true).var_export($item_str,true).var_export($stock,true));
    		
			//Check if the order is heavier than 
    		$weight_info = Shipping::calcShippingWeight($type,$order_id,'','','','',$cart_reference,$paypal_token);
    		if($weight_info['weight'] > $CFG->weight_for_freight_shipping){
    			$msg = sprintf('<div class="error_msg">%s<br>%s</div>', 'This order is too heavy to ship to a PO box.','Please use a non-PO Box Shipping address.');
    			return array('can_ship' => false, 'msg' => $msg);
    		}
    		//mail('tova@tigerchef.com', 'validate po5', var_export($shipping_address,true).var_export($paypal_token,true).var_export($not_po_box_allowed_items,true));
    		
    		
    	} else {
    		unset($cart->data['shipping_po_box']);
    	}  
//     	mail('tova@tigerchef.com', 'validate po', var_export($shipping_address,true).var_export($paypal_token,true).var_export($not_po_box_allowed_items,true));
    	 
    	return array('can_ship' => true);
    }
    
    /**
     * Make sure we ship to this destination
     * @return boolean
     * @param unknown $zipcode
     */
    function check_shipping_destination($zipcode){
    	$zipcode = substr($zipcode, 0, 5); 
    	$qualified_destinations = getFullStatesList(true); //we only ship to continental US
    	$state_code = ZipCodesUSA::get_state_code_for_zip($zipcode);
    	if(array_key_exists($state_code, $qualified_destinations)){
    		return true;
    	}else{
    		return false;
    	}
    }
	
   /**
     * Blcok ebay Global Shipping Program
     * @return boolean
     * @param unknown $zipcode
     */
    function block_address($zipcode,$address='',$city='',$state=''){
    	$zipcode = substr($zipcode, 0, 5);
		$address = trim(strtoupper($address));

		if(($zipcode == '41025' || $zipcode == '41018') &&
			trim(strtoupper($state)) == 'KY' &&
			trim(strtoupper($city)) == 'ERLANGER' &&
			strpos($address,'1850 AIRPORT EXCHANGE')!==false && strpos($address,'200')!==false
			){
			return true;
		}
		return false;
    } 
	/**
     * Use UPS API to make sure an address is a valid street address
     * @return array of suggestions if the address isn't valid
     * @param array $address
     */
	function validate_address($address){
		global $CFG;
		$shipper = UPSShipper::get1($CFG->default_from_shipper);
		
		//only validate the main part of the zip
		$address['zip'] = substr(trim($address['zip']),0,5);
		
		require_once("$CFG->libdir/upsValidate.php");
		$myValidate = new upsValidate();
		$myValidate->setCredentials($CFG->ups_key,$CFG->ups_user,$CFG->ups_pass,$shipper['ups_account_number']);
		$ups_response = $myValidate->validateAddress($address);
		
		//print_ar($address);
		//print_ar($ups_response);
		
		if($ups_response['Response']['ResponseStatusCode'] == 0){
			$return['message'] = 'ups address validation failed';
			$return['status'] = "Unknown";
		}
		if(is_array($ups_response['Response']['Error'])){
			$return['message'] = var_export($ups_response['Response']['Error'],true);
			$return['status'] = "Unknown";
		}
		if(isset($ups_response['AddressKeyFormat'])){
			foreach($ups_response['AddressKeyFormat'] as $key=>$suggestion){
				if(is_int($key)){
					if(is_array($suggestion['AddressLine'])){
						$suggestion['AddressLine'] = implode(' ', $suggestion['AddressLine']);
					}
					
					$return['suggestions'][] = array(	'shipping_address1' => $suggestion['AddressLine'],
													'shipping_city' => $suggestion['PoliticalDivision2'],
													'shipping_state' => $suggestion['PoliticalDivision1'],
													'shipping_zip' => $suggestion['PostcodePrimaryLow']
												);
				}else{
					$one_address = true;
				}
			}
			if($one_address){
				if(is_array($ups_response['AddressKeyFormat']['AddressLine'])){
					$ups_response['AddressKeyFormat']['AddressLine'] = implode(' ', $ups_response['AddressKeyFormat']['AddressLine']);
				}
					
				$return['suggestions'][] = array(	'shipping_address1' => $ups_response['AddressKeyFormat']['AddressLine'],
													'shipping_city' => $ups_response['AddressKeyFormat']['PoliticalDivision2'],
													'shipping_state' => $ups_response['AddressKeyFormat']['PoliticalDivision1'],
													'shipping_zip' => $ups_response['AddressKeyFormat']['PostcodePrimaryLow']
												);
			}
			
			foreach($return['suggestions'] as $suggestion){
				if(strtoupper($address['address1']) == strtoupper($suggestion['shipping_address1'] ) && 
					strtoupper($address['city']) == strtoupper($suggestion['shipping_city']) &&
					strtoupper($address['state']) == strtoupper($suggestion['shipping_state']) &&
					strtoupper($address['zip']) == strtoupper($suggestion['shipping_zip']) ){
					$match_found = true;
					break;
				}
			}
		}
		if(is_array($ups_response['NoCandidatesIndicator'])){
			$return['message'] = "Address was not found, and no replacements were found";
			$return['status'] = "NoCandidate";
		}elseif(count($return['suggestions'])>1 || !$match_found ){
			// return the suggestions
			$return['message'] = "Address is ambiguous";
			$return['status'] = "Ambiguous";
		}else{
			if(isset($return['suggestions'])){
				unset($return['suggestions']);
			}
			$return['message'] = "Address is valid";
			$return['status'] = "Valid";
		}
		
		/*
		if(is_array($ups_response['AmbiguousAddressIndicator'])){
			// return the suggestions
			$return['message'] = "Address is ambiguous";
			$return['status'] = "Ambiguous";
		}
		if(is_array($ups_response['ValidAddressIndicator'])){
			$return['message'] = "Address is valid";
			$return['status'] = "Valid";
		}*/


		return $return;
	}
	
    function getShipType( $items, $shipping )
    {
    	if( !$items )
    		return false;

    	$is_local = Shipping::checkLocal( $shipping['zip'] );

    	$is_freight = false;

    	foreach( $items as $item )
    	{
    		if( $item['order_id'] )
    		{
    			//Need to get product to check ship type
    			$product = Products::get1( $item['product_id'] );
    			if( strtolower($product['shipping_type']) == strtolower('freight') )
    				$is_freight = true;
    		}
    		else if( strtolower($item['shipping_type']) == strtolower('freight') )
    			$is_freight = true;
    	}

    	if( is_array($is_local) && !$is_freight )
    	{
    	//	echo'local';
    		return 'ups';
    	}
    	elseif( is_array($is_local) )
    	{
    		return 'local';
    	}
    	elseif( $is_freight )
    	{
    		return 'freight';
    	}

    	return 'ups';
    }

    function getShippingCost( $type, $items, $cost )
    { //Need to include quantities of the items ( 2 of same is still 2! )
    	global $CFG;
    	//print_ar( $type );
    	//print_ar( $items );
    	if( !$items )
    		return false;

    	$shipping_amt = 0;
    	$first = true;
    	$ship_items = 0;

    	$cost['cut_off'] = 0.00 + preg_replace('/[^0-9.]/', "", $cost['cut_off'] );


    	if( $cost['order_total'] > number_format($cost['cut_off'],2,'.','') )
    		return $cost['flat_rate'];
    	if( $type != 'local' )
    	{
	    	foreach( $items as $item )
	    	{
	    		//print_ar( $item );
	    		if( $item['free_shipping'] == 'Y' )
	    			continue;
	    		else
	    			$ship_items++;
	    		if( $first )
	    		{
	    			$first = false;
	    			$shipping_amt += $item['weight'] * $cost[$type . '_first'];
	    			if( $item['qty'] > 1 )
	    			{
	    				$shipping_amt += ( $item['qty'] -1 ) * $item['weight'] * $cost[$type . '_add'];
	    			}
	    		}
	    		else
	    		{
	    			$shipping_amt += ( $item['qty'] ) * $item['weight'] * $cost[$type . '_add'];
	    		}
	    	}
    	}
    	else
    	{
    		foreach( $items as $item )
	    	{
	    		if( $item['free_shipping'] == 'Y' )
	    			continue;
	    		else
	    			$ship_items++;
	    		if( $first )
	    		{
	    			$first = false;
	    			$shipping_amt += $cost[$type . '_first'];
	    			if( $item['qty'] > 1 )
	    			{
	    				$shipping_amt += ( $item['qty'] -1 ) * $cost[$type . '_add'];;
	    			}
	    		}
	    		else
	    		{
	    			$shipping_amt += $item['qty'] * $cost[$type . '_add'];
	    		}
	    	}
    	}
    	//echo $ship_items;
    	if( !$ship_items )
    		return 0;

    	if( $shipping_amt > $CFG->max_shipping )
    		$shipping_amt = $CFG->max_shipping;

    	switch( $type )
    	{
    		case 'freight':
    			if( $shipping_amt < $cost['freight_min'] )
    				return $cost['freight_min'];
    			else
    				return $shipping_amt;
    			break;
    		case 'ups':
    			if( $shipping_amt < $cost['ups_min'] )
    				return $cost['ups_min'];
    			else
    				return $shipping_amt;
    			break;
    		case 'local':
    			return $shipping_amt;
    			break;
    	}
    }

    function insertLocalZip( $zip )
    {
    	return db_insert( 'local_zips', array('zipcode'=>$zip),'',true );
    }

    function checkLocal( $zip )
    {
    	$sql = " SELECT * FROM local_zips where zipcode = '$zip' ";
    	return db_query_array( $sql );
    }

    //this is only used in product_export. Needs to be tweaked when I fixed that page.
    function getProductShippingCost( $id, $option=0, $model_number='' )
    {
    	global $CFG;

				$cost['cut_off'] = $CFG->east_cut_off;
				$cost['flat_rate'] = $CFG->east_flat;
				$cost['freight_first'] = $CFG->frieght_cost_1st_east;
				$cost['freight_add'] = $CFG->frieght_cost_add_east;
				$cost['ups_first'] = $CFG->ups_cost_1st_east;
				$cost['ups_add'] = $CFG->ups_cost_add_east;
				$cost['local_first'] = $CFG->local_cost_1st_east;
				$cost['local_add'] = $CFG->local_cost_add_east;
				$cost['freight_min'] = $CFG->freight_min_east;
				$cost['ups_min'] = $CFG->ups_min_east;

				if( !$model_number )
					$product = Products::get1( $id );
				else
					$product = Products::get1ByModel( $model_number );

				if( $product['free_shipping'] == 'Y' )
					return 0;

				$markup = Products::getProductMarkup( $product );
				$price = number_format( (Products::getProductMarkup($product) ), 2 );
				if( $option )
				{
					$option = Products::getProductOption( $option );
					$price = number_format( (Products::getProductMarkup($product) ), 2 );
				}
				//print_ar( $product );

				if( $price > $CFG->east_cut_off )
					return $CFG->east_flat;
				$type = strtolower( $product['shipping_type'] );

				$shipping_amt += $product['weight'] * $cost[$type . '_first'];

			switch( $type )
    	{
    		case 'freight':
    			if( $shipping_amt < $cost['freight_min'] )
    			{
    				$ret = $cost['freight_min'];
    				unset( $cost );
    				return $ret;
    			}
    			else
    			{
    				unset( $cost );
    				return $shipping_amt;
    			}
    			break;
    		case 'ups':
    			if( $shipping_amt < $cost['ups_min'] )
    			{

    				$ret = $cost['ups_min'];
    				unset( $cost );
    				return $ret;
    			}
    			else
    			{
    				unset( $cost );
    				return $shipping_amt;
    			}
    			break;
    	}
    }

    static function displayShippingInfo( $type )
    {
    	$ret = "";
    	switch( $type )
    	{
    		case 'UPS':
  				$ret .= "<p>This item is shipped via UPS Ground. You will receive an email confirmation once your order has been shipped which will provide the contact info of UPS Ground and usually a tracking number and website information enabling you to track your shipment online.</p>";
  				$ret .= "<p>If you are in the Metro New York and Metro New Jersey area  our local shipping company  will make the delivery. They will contact you to set up an appointment once the order arrives in your area. They will work around your schedule and will give you a 1 hour window for a delivery time. Your shipping costs will be a flat $50.00 for the first item and $25.00 each additional item. An additional installation service is available in the Metro New York and Metro New Jersey area.";
	    		//This would need an ajax call
					//To see if you are eligible for our the Metro New York and Metro New Jersey delivery please enter in your zip code here ______
    			break;
    		case 'Freight':
    			$ret .= "<p>This item is shipped via common truck freight carrier. You will receive an email confirmation once your order has been shipped which will provide the name and contact information of the carrier used and a tracking number and website information enabling you to track your shipment online. The freight carrier will contact you to set up an appointment once the order arrives in your area. They will work around your schedule and will give you a 1- 2 hour window for a delivery time. All our deliveries include bringing the product over the threshold  of your house. If you are a second floor or have multiple steps please call to make arrangements before placing your order. For shipping estimates please click on the �Add to cart� button located next to the item and enter in your zip code.</p>";
    			$ret .= "<p>If you are in the Metro New York and Metro New Jersey area  our local shipping company  will make the delivery. They will contact you to set up an appointment once the order arrives in your area. They will work around your schedule and will give you a 1 hour window for a delivery time. Your shipping costs will be a flat $50.00 for the first item and $25.00 each additional item. An additional installation service is available in the Metro New York and Metro New Jersey area . </p>";
    			//This would need an ajax call
					//To see if you are eligible for our the Metro New York and Metro New Jersey delivery please enter in your zip code here ______

    			break;
    	}
    	$ret .= "<p>For installations or removals in the Metro New York and Metro New Jersey area please call us at 1-800-499-8009 for information.</p>";
    	return $ret;
    }

    static function generateShippingLabel( $order_id, $qty=1, $shipping_id )
    {
    	global $CFG;
    	$order = Orders::get1( $order_id );
    	$tracking = Orders::getTracking( $shipping_id );
    	$tracking = array_shift( $tracking );

    	$ret = "<div class='shipping_label'>";
    	$ret .="<table>";
    	$ret .="<tr><td colspan=2><img src='/images/logo_white.gif' /></td></tr>";
    	$ret .="<tr><th>Shipper:</th>";
    	//Not sure if this is store specific or warehouse. For now I will hard code it,
    	//can change to store specific easy enough
    	$ret .="<td>Rainbow Appliance</td></tr>";
    	$ret .="<tr><td></td><td>210 River Road</td>";
    	$ret .="<tr><td></td><td>Clifton, NJ 07014</td>";
    	$ret .="<tr><td></td><td>1-800-499-8009</td>";
    	$ret .="<tr><td></td><td>www.rainbowappliance.com</td>";
    	$ret .="<tr></tr>";

    	$ret .="<tr><th>Ship To:</th><td><b>" . $order['shipping_first_name'] . ' ' . $order['shipping_last_name'] . "</b></td>";
    	$ret .="<tr><td></td><td><b>" . $order['shipping_address1'] . "</b></td>";
    	if( $order['shipping_address2'] )
    		$ret .="<tr><td></td><td><b>" . $order['shipping_address2'] . "</b></td>";
    	$ret .="<tr><td></td><td><b>" . $order['shipping_city'] . ", " . $order['shipping_state'] . " " . $order['shipping_zip'] . "</b></td>";
    	$ret .="<tr><td></td><td><b>" . formatPhoneNumber($order['shipping_phone']) . "</b></td>";
    	$ret .="<tr></tr>";
    	$ret .="<tr><th>Order #:</th><td><b>" . $CFG->order_prefix . $order['id'] ."</b></td></tr>";
    	$ret .="<tr><th>Products:</th>";
    	$order_items = Orders::getItemsForTracking( $shipping_id );
    	$shipping_info = Orders::getTracking( $shipping_id );
    	$shipping_info = array_shift( $shipping_info );

    	$ret .="<td>";
    	$ret .="<table>";
    	foreach( $order_items as $item )
    	{
    		if( $item['options'] )
			{
			//	if( $item['options'][0]['additional_price'] )
			//	{
			//		$item['price'] = $item['options'][0]['additional_price'];
			//		$item['item_total'] = $item['price'] * $item['qty'];
			//	}
				if( $item['options'][0]['value'] )
				{
					$item['color'] = $item['options'][0]['value'];
				}
				else
					$item['color'] = "N/A";
				$item['product_vendor_sku'] = $item['options'][0]['vendor_sku'];
			}
			else
				$item['color'] = "N/A";

    			$ret .="<tr>";
    			$ret .= "<td>" . $item['brand_name'] . '</td>';
    			$ret .= '<td><b>' . $item['qty'] . '</b></td> ';
    			$ret .= "<td>" . $item['product_vendor_sku'] . '</td>';

    			$ret.="</tr>";
    	}
    	$ret .="</table>";
    	$ret .="</td></tr>";
    	$ret .="<tr><th>Carrier:</th><td>" . $shipping_info['name'] ."</td></tr>";
    	$ret .="<tr><th style='font-size: 120%; vertical-align: bottom;'>Pro#</th><td style='font-size: 175%; font-weight:bold;'>" . $tracking['tracking_number'] ."</td></tr>";
    	$ret .="</table></div>";
    	return $ret;
    }

    static function generateBillOfLading( $order_id, $shipping_id )
    {
    	global $CFG;
    	$order = Orders::get1( $order_id );

    	$driver_notes = Orders::getNotes( $order_id, 'driver_notes');
    	/*$instructions = "";
    	if( $driver_notes )
    		foreach( $driver_notes as $note )
    		{
    			$instructions .= $note['notes'] . "<br />";
    		}
    	else
    		$instructions = "None";
    	*/ //Changed by Eli Request to always say "residential delivery" on 3/26

    	$instructions = "Residential Delivery";

    	$tracking = Orders::getTracking( $shipping_id );
    	$tracking = array_shift( $tracking );

    	$ret = "<table width=100% class='bill_lading'>";
    	$ret .= "<tr class='header_row'><td colspan=2><img src='/images/logo_white.gif' /></td>
    	<td colspan=2 align=center class='title'>Bill of Lading</td>";

    	$ret .= "<td class='title'>Pro#</td>";
    	$ret .= "<td class='title'>" . $tracking['tracking_number'] . "</td>";

    	$ret .= "</tr>";
    	$ret .= "<tr>";
    	$ret .= "<td><span class='title'>Order Date:</span><br /><span class='data'>" . db_date($order['date']) ."</span></td>";
    	$ret .= "<td><span class='title'>Carrier:</span><br /><span class='data'>" . $tracking['name'] ."</span></td>";
    	//Where am I getting the service level from?
    	$ret .= "<td><span class='title'>Service Level:</span><br /><span class='data'>Standard</span></td>";
    	//Which PO # am I using?
    	$ret .= "<td><span class='title'>PO #:</span><br /><span class='data'>" . $CFG->order_prefix . $order['id'] . "</span></td>";
    	$ret .= "<td><span class='title'>Date:</span><br /><span class='data'>" . date('m/d/Y') ."</span></td>";
    	$ret .= "<td><span class='title'>Order Total:</span><br /><span class='data'>" . $order['order_total'] . "</span></td></tr>";
    	$ret .= "<tr class=black><th class=black colspan=3>SHIP TO:</th><th class=black colspan=3>FROM:</th></tr>";
    	$ret .= "<tr><td colspan=3><span class='title'>Company:</span><span class='data'>" . $order['shipping_company'] ."</span></td>";
    	$ret .= "<td colspan=3><span class='title'>From:</span><span class='data'>Rainbow Appliance</span></td></tr>";
    	$ret .= "<tr><td colspan=3><span class='title'>Contact:</span><span class='data'>" . $order['shipping_first_name'] . ' ' . $order['shipping_last_name'] ."</span></td>";
    	$ret .= "<td colspan=3><span class='title'>Address:</span><span class='data'>210 River Road</span></td></tr>";
    	$ret .= "<tr><td colspan=3><span class='title'>Addres:</span><span class='data'>" . $order['shipping_address1'] ."</span></td>";
    	$ret .= "<td><span class='title'>City:</span><span class='data'>Clifton</span></td>";
    	$ret .= "<td><span class='title'>State:</span><span class='data'>NJ</span></td>";
    	$ret .= "<td><span class='title'>Zip:</span><span class='data'>07014</span></td></tr>";
    	$ret .= "<tr><td colspan=3><span class='title'>Addres:</span><span class='data'>" . $order['shipping_address2'] ."</span></td>";
    	$ret .= "<td colspan=2><span class='title'>Phone:</span><span class='data'>973-471-2001</span></td>";
    	$ret .= "<td><span class='title'>Fax:</span><span class='data'>973-471-2008</span></td></tr>";
    	$ret .= "<tr><td><span class='title'>City:</span><span class='data'>" . $order['shipping_city'] . "</span></td>";
    	$ret .= "<td><span class='title'>State:</span><span class='data'>" . $order['shipping_state'] . "</span></td>";
    	$ret .= "<td><span class='title'>Zip:</span><span class='data'>" . $order['shipping_zip'] . "</span></td>";
    	$ret .= "<td rowspan=2 colspan=3><span class='title'>Instructions:</span><span class='data'>" . $instructions ."</span></td></tr>";
    	$ret .= "<tr><td colspan=2><span class='title'>Phone:</span><span class='data'>" . formatPhoneNumber($order['shipping_phone']) . "</span></td>";
    	$ret .= "<td><span class='title'>Phone:</span><span class='data'>" . formatPhoneNumber($order['shipping_phone2']) . "</span></td></tr>";

    	$ret .= "<tr class=black><th class=black colspan=6 align=left>Bill Freight Charges To:</th></tr>";
    	$ret .= "<tr><td colspan=3><span class='title'>Name:</span><span class='data'>Rainbow Appliance & Electronics</span></td>";
    	$ret .= "<td colspan=3><span class='title'>Address:</span><span class='data'>210 River Rd</span></td></tr>";
    	$ret .= "<tr>";
    	$ret .= "<td><span class='title'>City:</span><span class='data'>Clifton</span></td>";
    	$ret .= "<td><span class='title'>State:</span><span class='data'>NJ</span></td>";
    	$ret .= "<td><span class='title'>Zip:</span><span class='data'>07014</span></td>";
    	$ret .= "<td colspan=2><span class='title'>Phone:</span><span class='data'>973-471-2001</span></td>";
    	$ret .= "<td><span class='title'>Fax:</span><span class='data'>973-471-2008</span></td></tr>";

    	$ret .= "<tr class=black><th class=black colspan=6 align=left>Products:</th></tr>";
    	$ret .= "<tr><th>Quantity</th><th>Model #</th><th colspan=3>Description</th><th>Weight</th></tr>";

    	$order_items = Orders::getItemsForTracking( $shipping_id );
    	$total_weight = 0;
    	if( $order_items )
	    	foreach( $order_items as $item )
	    	{
	    		if( $item['options'] )
				{
				//	if( $item['options'][0]['additional_price'] )
				//	{
				//		$item['price'] = $item['options'][0]['additional_price'];
				//		$item['item_total'] = $item['price'] * $item['qty'];
				//	}
					if( $item['options'][0]['value'] )
					{
						$item['color'] = $item['options'][0]['value'];
					}
					else
						$item['color'] = "N/A";
					$item['product_vendor_sku'] = $item['options'][0]['vendor_sku'];
				}
				else
					$item['color'] = "N/A";

	    		$ret .= "<tr><td>" . $item['tracking_qty'] . "</td><td>". $item['product_vendor_sku'] . "</td>";
	    		$ret .= "<td colspan=3>" . $item['product_name'] . "</td><td>" . ceil($item['weight'] * $item['qty'] * $CFG->weight_modifier ) . "</td></tr>";
	    		$total_weight += ceil($item['weight'] * $item['qty'] * $CFG->weight_modifier );
	    	}

    	$ret .= "<tr><th colspan=5 align=right>Total Weight:</th><td>" . $total_weight . "</td></tr>";
    	$ret .="</table>";
    	$ret .="<p>Received at the point of origin on the date specified, from the consignor mentioned herein,
    	the property herein described, in apparent good order, except as noted (contents and condition of contents
    	of packages in good condition), marked, consigned, and destined, as indicated above, which the carder agrees
    	to carry and to deliver to the consignee at the said destination, if on its route or otherwise to deliver to
    	another carrier on the route to said destination. It is mutually agreed as to each carrier of all or any of
    	the goods over all or any portion of the route to destination, and as to each party of any time interested in
    	all or any of the goods, that every service to be performed here under shall be subject to all the conditions
    	not prohibited by law, whether printed or written, are hereby agreed by the consignor and accepted for himself
    	and his assigns.</p>";


    	$ret .= "<h3 align=center>Inspected And Received This Unit In Perfect Condition With No Damage To Unit.</h3>";
    	$ret .="<br /><br />";
    	$ret .= "<table width=100% style='border-collapse: collapse'>";
    	$ret .="<tr>
    	<td style='width: 15%' align=right>Customer:</td><td style='border-bottom: 1px solid #000;'></td>
    	<td style='width: 15%' align=right>Unites Received:</td><td style='border-bottom: 1px solid #000;'></td>
    	<td style='width: 15%' align=right>Pallets Received:</td><td style='border-bottom: 1px solid #000;'></td></tr>";

    	$ret .="</table>";

    	$ret .="<br /><br /><br />";
    	$ret .="<table width=100% style='border-collapse: collapse'>";

    	$ret .="<tr>
    	<td style='width: 15%' align=right>Shipper Sign:</td><td style='border-bottom: 1px solid #000;'></td>
    	<td style='width: 15%' align=right>Carrier Sign:</td><td style='border-bottom: 1px solid #000;'></td>
    	<td style='width: 15%' align=right>Date:</td><td style='border-bottom: 1px solid #000;'></td></tr>";

    	$ret .="</table>";

    	return $ret;
    }

    static function generateInvoice( $order_id )
    {
    	global $CFG;
    	$order = Orders::get1( $order_id );

    	$store = Stores::get1( $order['order_tax_option'] );

    	$status = OrderStatuses::get1( $order['current_status_id'] );

    	$tracking = Orders::getTracking( 0, $order['id'] );
    	if( $tracking )
    		$last_tracking = $tracking[count( $tracking ) -1];
    	else
    	{
    		$last_tracking['ship_date'] = '';
    		$last_tracking['tracking_number'] = 'N/A';
    		$last_tracking['name'] = 'N/A';
    	}
    	$po_numbs = Orders::getPO( 0, $order['id'] );
    	if( $po_numbs )
    		$last_po = $po_numbs[count( $po_numbs ) - 1];
    	else
    		$last_po['po_number'] = 'N/A';

    	if( $last_po['po_number'] == '' )
    		$last_po['po_number'] = 'N/A';

    	$sales_name = explode( " ", $order['sales_name'] );
			$order['sales_initials'] = substr( $sales_name[0], 0, 1) . substr( $sales_name[1], 0, 1);
    	if( !$order['sales_initials'] )
    		$order['sales_initials'] = '-';
    	$ret ="<table width=100%>";
    	$ret .="<tr class='header_row'>";
    	$ret .= "<td>";
	    	$ret .="<table>";
	    	$ret .="<tr><td><b>$store[invoice_name]</b</td></tr>";
	    	$ret .="<tr><td><b>$store[address1]</b</td></tr>";
	    	if( $store['address2'] )
	    		$ret .="<tr><td><b>$store[address2]</b</td></tr>";
	    	$ret .="<tr><td><b>$store[city] $store[state] $store[zip]</b</td></tr>";
	    	$ret .="<tr><td><b>$store[phone]</b</td></tr>";
	    	$ret .="<tr><td><b>www.rainbowappliance.com</b</td></tr>";
	    	$ret .="</table>";
    	$ret .="</td>";
    	$ret .="<td colspan=2><img src='/images/logo_white.gif' /><br /><p align=center><b>Customer Invoice</b></p></td>";
    	$ret .="<td>";
    		$ret .= "<table class='order_sumary'>";
    		$ret .= "<tr class='header_row'><th colspan=2>Sales Order</th></tr>";
    		$ret .= "<tr><td>Order #:</td><td>" . $CFG->order_prefix . $order['id'] . "</td></tr>";
    		$ret .= "<tr><td>Order Date:</td><td>" . db_date($order['override_date'] == '0000-00-00 00:00:00' ? $order['date'] : $order['override_date']) . "</td></tr>";
    		$ret .= "<tr><td>Store:</td><td>" . $store['name'] . "</td></tr>";
    		if( $order['account_number'] )
    			$ret .= "<tr><td>Account #:</td><td>" . $order['account_number'] . "</td></tr>";
    		$ret .= "</table>";
    	$ret .= "</td>";
    	$ret .="</tr>";

    	$ret .="<td colspan=2>";
    		$ret .="<table class='invoice_cust_info'>";
    		$ret .="<tr><th>Sold To</th></tr>";
    		$ret .="<tr><td>" . $order['billing_first_name'] . ' ' . $order['billing_last_name'] . ' ' . formatPhoneNumber($order['billing_phone']) . "</td></tr>";
    		$ret .="<tr><td>" . $order['billing_address1'] . "</td></tr>";
    		if( $order['billing_address2'] )
    			$ret .="<tr><td>" . $order['billing_address2'] . "</td></tr>";
    		$ret .="<tr><td>" . $order['billing_city'] . ' ' . $order['billing_state'] . ' ' . $order['billing_zip'] . "</td></tr>";
    		$ret .="</table>";
    	$ret .="</td>";
    	$ret .="<td colspan=2>";
    		$ret .="<table class='invoice_cust_info'>";
    		$ret .="<tr><th>Ship To</th></tr>";
    		$ret .="<tr><td>" . $order['shipping_first_name'] . ' ' . $order['shipping_last_name'] . ' ' . formatPhoneNumber($order['shipping_phone']) . ' ' . formatPhoneNumber($order['shipping_phone2']) . ' ' . formatPhoneNumber($order['shipping_phone3']) ."</td></tr>";
    		$ret .="<tr><td>" . $order['shipping_address1'] . "</td></tr>";
    		if( $order['shipping_address2'] )
    			$ret .="<tr><td>" . $order['shipping_address2'] . "</td></tr>";
    		$ret .="<tr><td>" . $order['shipping_city'] . ' ' . $order['shipping_state'] . ' ' . $order['shipping_zip'] . "</td></tr>";
    		$ret .="</table>";
    	$ret .="</td>";
    	$ret .="</tr>";
    	$ret .="</table>";

    	$ret .="<table width=100% class='print_invoice_items'>";
    	$ret .="<tr><th>Order #</th><th>Sls</th><th>Ship Date</th><th>Ship Via</th><th>Tracking #</th><th>PO #</th><th>Status</th></tr>";
    	$ret .="<tr>";
    	$ret .="<td>". $CFG->order_prefix . $order['id'] ."</td>";
    	$ret .="<td>" . $order['sales_initials'] . "</td>";
    	$ret .="<td>" . db_date($last_tracking['ship_date']) . "</td>";
    	$ret .="<td>" . $last_tracking['name'] . "</td>";
    	$ret .="<td>" . $last_tracking['tracking_number'] . "</td>";
    	$ret .="<td>" . $last_po['po_number'] . "</td>";
    	$ret .="<td>" . $status['name'] . "</td>";
    	$ret .="</tr>";
    	$ret .="</table>";
    	$ret .="<br /><br />";
    	$ret .="<table width=100% class='print_invoice_items'>";
    	$ret .= "<tr><th>Quantity</th><th>Model #</th><th colspan=3>Description</th><th>Unit Price</th><th>Total</th></tr>";

    	$order_items = Orders::getItems( 0, $order_id );
    	$total_weight = 0;
    	foreach( $order_items as $item )
    	{
    		//print_ar( $item );
    		$ret .= "<tr><td>" . $item['qty'] . "</td><td>". $item['product_vendor_sku'] . "</td>";
    		$ret .= "<td colspan=3>" . $item['product_name'] . "</td><td class='price'>$" . number_format($item['price'], 2) . "</td>";
    		$ret .= "<td class='price'>$" . number_format($item['item_total'], 2) . "</td>";
    		$ret .="</tr>";
    	}
    	$ret .= "<tr><td>-</td><td></td>";
		$ret .= "<td colspan=3>Shipping Charge</td><td class='price'>$" . number_format($order['shipping'], 2) . "</td>";
		$ret .= "<td class='price'>$" . number_format($order['shipping'], 2) . "</td>";
		$ret .="</tr>";

		$ret .= "<tr><td>-</td><td></td>";
		$ret .= "<td colspan=3>Tax</td><td class='price'>$" . number_format($order['order_tax'], 2) . "</td>";
		$ret .= "<td class='price'>$" . number_format($order['order_tax'], 2) . "</td>";
		$ret .="</tr>";

    	$ret .="</table>";

    	$ret .="<table width=100% class='print_invoice_items'>";
    	$ret .="<tr><th colspan=6 align=right>Invoice Total</th><th align=right>$" . number_format($order['order_total'], 2) . "</th></tr>";
    	$payments = Payments::get( 0, $order_id, '','','',0, true );
    	$ct = count( $payments );
    	$ret .="<tr><td colspan=4 rowspan=$ct width=60% style='border: 0;'></td>";
    		//List payments
    		$first = true;
    		if( is_array( $payments ) )
	    		foreach( $payments as $p )
	    		{
	    			if( $first )
	    			{
	    				$first = false;
	    			}
	    			else
	    			{
	    				$ret .= "<tr>";
	    			}
	    			$ret .= "<td class=''>"  . db_date( $p['date_added'] ) . "</td>";
	    			$ret .= "<td class=''>".  $p['payment_method'] . ' - ' . $p['action'] . "</td>";
	    			$ret .= "<td class='price'>$" . number_format($p['amount'], 2) . "</td>";
	    			$ret .= "</tr>";
	    		}
    		$ret .="<tr><td class='blank' colspan=5></td>

    		<td class='price'><b>BALANCE DUE</b></td>";
    		$balance = Orders::getOrderBalance( $order );
    		$ret .="<td class='price'><b>$" . number_format( $balance, 2 ) ."</b></td></tr>";
    		$ret .="</table>";
    	$ret .="</td>";
    	$ret .="</tr>";

    	$ret .="</table>";


    	$ret .= "<table class='all_stores' width='100%'>";

    	$store_list = Stores::get();
    	foreach( $store_list as $store )
    	{
    		$ret .= "<td>";
	    		$ret .="<table>";
		    	$ret .="<tr><td><b>$store[invoice_name]</b</td></tr>";
		    	$ret .="<tr><td><b>$store[address1]</b</td></tr>";
		    	if( $store['address2'] )
		    		$ret .="<tr><td><b>$store[address2]</b</td></tr>";
		    	$ret .="<tr><td><b>$store[city] $store[state] $store[zip]</b</td></tr>";
		    	$ret .="<tr><td><b>$store[phone]</b</td></tr>";

		    	$ret .="</table>";
    		$ret .= "</td>";
    	}

    	$ret .= "</table>";

    	return $ret;
    }
}


/*

Copyright (c) 2000, Jason Costomiris
All rights reserved.

Don't be scared, it's just a BSD-ish license.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by Jason Costomiris.
4. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

class UpsQuote{

    function upsProduct($prod){
       /*

         1DM == Next Day Air Early AM
     1DA == Next Day Air
     1DP == Next Day Air Saver
     2DM == 2nd Day Air Early AM
     2DA == 2nd Day Air
     3DS == 3 Day Select
     GND == Ground
     STD == Canada Standard
     XPR == Worldwide Express
     XDM == Worldwide Express Plus
     XPD == Worldwide Expedited

    */
      $this->upsProductCode = $prod;
    }

    function origin($postal, $country){
      $this->originPostalCode = $postal;
      $this->originCountryCode = $country;
    }

    function dest($postal, $country){
      $this->destPostalCode = $postal;
          $this->destCountryCode = $country;
    }

    function rate($foo){
      switch($foo){
        case "RDP":
          $this->rateCode = "Regular+Daily+Pickup";
          break;
        case "OCA":
          $this->rateCode = "On+Call+Air";
          break;
        case "OTP":
          $this->rateCode = "One+Time+Pickup";
          break;
        case "LC":
          $this->rateCode = "Letter+Center";
          break;
        case "CC":
          $this->rateCode = "Customer+Counter";
          break;
      }
    }

    function container($foo){
          switch($foo){
        case "CP": // Customer Packaging
          $this->containerCode = "00";
          break;
               case "ULE": // UPS Letter Envelope
          $this->containerCode = "01";
          break;
        case "UT": // UPS Tube
          $this->containerCode = "03";
          break;
        case "UEB": // UPS Express Box
          $this->containerCode = "21";
          break;
        case "UW25": // UPS Worldwide 25 kilo
          $this->containerCode = "24";
          break;
        case "UW10": // UPS Worldwide 10 kilo
          $this->containerCode = "25";
          break;
      }
    }

    function weight($foo){
      $this->packageWeight = $foo;
    }

    function rescom($foo){
          switch($foo){

        case "RES": // Residential Address
          $this->resComCode = "1";
          break;
        case "COM": // Commercial Address
          $this->resComCode = "2";
          break;
          }
    }

    function getQuote(){
          $upsAction = "3"; // You want 3. Don't change unless you are sure.
      $url = join("&",
               array("http://www.ups.com/using/services/rave/qcostcgi.cgi?accept_UPS_license_agreement=yes",
                     "10_action=$upsAction",
                     "13_product=$this->upsProductCode",
                     "14_origCountry=$this->originCountryCode",
                     "15_origPostal=$this->originPostalCode",
                     "19_destPostal=$this->destPostalCode",
                     "22_destCountry=$this->destCountryCode",
                     "23_weight=$this->packageWeight",
                     "47_rateChart=$this->rateCode",
                     "48_container=$this->containerCode",
                     "49_residential=$this->resComCode"
           )
                );
                
//echo $url;            
                
      $result = file_get_contents($url);
      $result = explode("%", $result);
      
//  print_r($result);             	
        $errcode = substr($result[0], -1);
        switch($errcode){
          case 3:
            $returnval = $result[8];
                break;
          case 4:
            $returnval = $result[8];
            break;
          case 5:
            $returnval = $result[1];
            break;
          case 6:
            $returnval = $result[1];
            break;
        }
          if(! $returnval) { $returnval = "error"; }
      return $returnval;
    }
}


?>