<?php


class VendorInvoice {

    static function insert( $info )
	{
	    return db_insert( 'vendor_invoices', $info );
	}

	static function update( $id, $info )
	{
	    return db_update( 'vendor_invoices', $id, $info );
	}

	static function delete( $id )
	{
	    return db_delete( 'vendor_invoices', $id );
	}

	static function get( $id=0, $vendor_id=0 )
	{
	    $sql = " Select vendor_invoices.*, brands.name as vendor_name";
	    $sql .= " FROM vendor_invoices ";
	    $sql .= " LEFT JOIN brands on brands.id = vendor_invoices.vendor_id ";

	    $sql .= " WHERE 1 ";

	    if( $id )
		$sql .= " AND vendor_invoices.id = " . (int) $id;
	    if( $vendor_id )
		$sql .= " AND vendor_invoices.vendor_id = " . (int) $vendor_id;

	    return db_query_array( $sql );
	}

	static function get1( $id )
	{
	    $id = (int) $id;
	    if( !$id )
		return false;

	    return db_get1( self::get( $id ) );
	}

}

?>
