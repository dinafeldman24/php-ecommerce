<? 
class Deals
{
	static function get($id=0, $current_only = false, $product_id = 0, $sold_out = '')
	{
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND deals.id = $id ";
		}
		if ($product_id > 0) {
		    $sql .= " AND deals.product_id = $product_id ";
		}				
		if ($sold_out != "") {
		    $sql .= " AND deals.sold_out = '$sold_out' ";
		}				
		if ($current_only) $sql .= " AND NOW() BETWEEN deals.start_time and deals.end_time AND deals.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}

	static function seeIfCurrentDealForProd ($product_id)
	{
		$ret = self::get(0, true, $product_id);
		if ($ret) return true;
		else return false;
	}
	
	static function seeIfCurrentNotSoldOutDealForProd ($product_id)
	{
		$ret = self::get(0, true, $product_id, 'N');
		if ($ret) return true;
		else return false;
	}
	
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('deals', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
			
		return db_insert('deals', $info);
	}

	static function update($id = 0, $info = '')
	{

		if(!$info || !$id)
			return false;
		
		$deal = db_query_array("SELECT * FROM deals WHERE deals.id = $id");
		$deal = $deal[0];
		 
		if($deal['active'] == "Y"){
			
			if($info['product_id'] != $deal['product_id']){
				self::deactivate_deal($id);
				$ret = db_update('deals',$id,$info);
				self::activate_deal($id);
			} else if($info['deal_price'] != $deal['deal_price']){
				//update deals table
				$ret = db_update('deals',$id,$info);
				//update products table
				db_query_array("UPDATE products, deals SET products.price = deals.deal_price
					WHERE deals.id = $id AND products.id = deals.product_id");
				
			}
			else{
				$ret = db_update('deals',$id,$info);
			}
		}else{
			$ret = db_update('deals',$id,$info);
		}
		return $ret;
	}
	static function getJustExpiredAndSoldOut()
	{
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() > deals.end_time 
	    		AND deals.sold_out = 'Y' AND deals.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	static function getJustExpiredAndNotSoldOut()
	{
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() > deals.end_time 
	    		AND deals.sold_out = 'N' AND deals.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function getNeedToBeActivated()
	{
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() BETWEEN deals.start_time AND deals.end_time
	    			AND deals.sold_out = 'N' AND deals.active = 'N'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function getActiveButSoldOut()
	{
		global $CFG;
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals LEFT JOIN inventory ON (deals.product_id = inventory.product_id 
				  AND deals.product_option_id = inventory.product_option_id 
				  AND inventory.store_id = $CFG->default_inventory_location)";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() BETWEEN start_time AND end_time";
	    $sql .= " AND deals.sold_out = 'N' AND deals.active = 'Y' 
	    		AND (inventory.qty <= 0 OR inventory.qty IS NULL)";
		
	    $ret = db_query_array($sql);
		//echo $sql;
		return $ret;
	}
	static function getActiveButNoLongerSoldOut()
	{
		global $CFG;
		$sql = "SELECT ";
	    $sql .= " deals.* ";		
		$sql .= " FROM deals LEFT JOIN inventory ON (deals.product_id = inventory.product_id 
				  AND deals.product_option_id = inventory.product_option_id 
				  AND inventory.store_id = $CFG->default_inventory_location)";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() BETWEEN start_time AND end_time";
	    $sql .= " AND deals.sold_out = 'Y' AND deals.active = 'Y' 
	    		AND inventory.qty > 0 AND inventory.qty IS NOT NULL";
		//echo $sql;
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function deactivate_deal($id)
	{
		$sql = "UPDATE products, deals SET products.price = deals.old_price, 
				products.inventory_limit = deals.old_inventory_limit, deals.active = 'N', 
		 
				products.case_discount  = CASE WHEN (deals.old_case_discount > 0) THEN 'Y' ELSE products.case_discount END,
				products.case_price_percent  = CASE WHEN (deals.old_case_discount > 0) THEN deals.old_case_discount ELSE products.case_price_percent END
		 
				WHERE deals.id = $id AND products.id = deals.product_id";
		db_query($sql);				
	}
	static function deactivate_already_sold_out_deal($id)
	{
		$sql = "UPDATE deals SET deals.active = 'N' 
				WHERE deals.id = $id";
		db_query($sql);		
	}
		
	static function activate_deal($id,$limit_by_inventory=true)
	{
		$sql = "UPDATE products, deals SET deals.old_price = products.price, products.price = deals.deal_price, 
				deals.old_inventory_limit = products.inventory_limit, deals.active = 'Y', ";
		
		if($limit_by_inventory){ // not having inventory limit sometimes if not having deals just from warehouse
			$sql .= " products.inventory_limit = 'Y',";
		}
				
		$sql .= " deals.old_case_discount = CASE WHEN (products.case_discount = 'Y' AND products.case_price_percent > 0) THEN products.case_price_percent ELSE 0 END, 
				products.case_discount  = CASE WHEN (products.case_discount = 'Y' AND products.case_price_percent > 0) THEN 'N' ELSE products.case_discount END,
				products.case_price_percent  = CASE WHEN (products.case_discount = 'Y' AND products.case_price_percent > 0) THEN 0 ELSE products.case_price_percent END 
 
				WHERE deals.id = $id AND products.id = deals.product_id";

		db_query($sql);		
	}
	
	static function mark_deal_as_sold_out($id)
	{
		$sql = "UPDATE products, deals SET products.price = deals.old_price, 
				products.inventory_limit = deals.old_inventory_limit,
				deals.sold_out = 'Y' 
				WHERE deals.id = $id AND products.id = deals.product_id";
		db_query($sql);				
	}
	static function mark_deal_as_no_longer_sold_out($id)
	{
		$sql = "UPDATE products, deals SET products.price = deals.deal_price, 
				products.inventory_limit = 'Y',
				deals.sold_out = 'N' 
				WHERE deals.id = $id AND products.id = deals.product_id";
		db_query($sql);				
	}
	static function get_min_time_of_deal_expiration()
	{
		$sql = "SELECT min(end_time) as min_exp";	
		$sql .= " FROM deals ";
		$sql .= " WHERE 1 ";
		$sql .= " AND NOW() BETWEEN deals.start_time and deals.end_time AND deals.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret[0]['min_exp'];
	}
}
?>