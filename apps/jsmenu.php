<?php

class JSMenu {
	var $_sections;
	var $_show_print;
	var $_show_rb;

	function JSMenu($show_print = false, $show_rb = false)
	{
		$this->_show_print = $show_print;
		$this->_show_rb = $show_rb;
	}

	function addSection($name)
	{
		return $this->_sections[] = new JSMenuSection($name);
	}

	function display()
	{

		global $print_url;

		//echo "FOO: " . $print_url;

		$page = explode('/',$_SERVER["SCRIPT_NAME"]);
		$page = (is_array($page)) ? $page[count($page)-1] : '';
		echo "<script language=\"JavaScript\">\r\n<!--\r\nvar menu = new Array();\r\n";

		$x=0;

		if (is_array($this->_sections)) {
			$menustr = '';

			foreach ($this->_sections as $section) {
				$menustr .= "menu[$x] = '";
				$subsections = $section->getContents();
				if (is_array($subsections)) {
					foreach ($subsections as $subsection) {
						$ss = $subsection->toString();
						if ($page == $subsection->getUrl()) {
							$ss = "<b>$ss</b>";
							$master_section = $x;
						}
						$menustr .= "$ss &nbsp; &#8226; &nbsp;";
					}

					$menustr = substr($menustr,0,-15);
				}
				else
					$menustr .= '&nbsp;';

				$menustr .= "';\r\n";
				++$x;
			}

			if ($this->_show_print) {
			++$x;
			}

			$menustr .= "menu[$x] = '<a href=\"?action=logout\" class=\"navlink\">Click Here</a> to logout';";

			if ($this->_show_rb) {
				++$x;
				$menustr .= "menu[$x] = '<a href=\"http://www.rustybrick.com/client_center.php\" target=\"_blank\" class=\"navlink\">Click Here</a> for the RustyBrick Client Center (opens new window)';";
			}


			echo $menustr;
		}
		?>

		function changeMenu(section) {
			for (var x=0;x<<?= count($this->_sections) + 1 ?>;x++) {
				document['left'+x].src = '/shared_files/pics/tab_left.gif';
				document['right'+x].src = '/shared_files/pics/tab_right.gif';
				document.getElementById('middle'+x).style.backgroundImage = "url(/shared_files/pics/tab_middle.gif)";
				document.getElementById('nav_section_'+x).className='navlink';
			}

			document['left'+section].src = '/shared_files/pics/tab_left_b.gif';
			document['right'+section].src = '/shared_files/pics/tab_right_b.gif';
			document.getElementById('middle'+section).style.backgroundImage = "url(/shared_files/pics/tab_middle_b.gif)";
			document.getElementById("nav_section_"+section).className='navlink selected';

			if (document.all)
				submenu.innerHTML=menu[section];
			else if (document.getElementById)
				document.getElementById("submenu").innerHTML=menu[section];
		}

		// -->
		</script>
		<?

		echo '<table border=0 background="/shared_files/pics/nav_bg2.gif" cellspacing=0 cellpadding=0 width="100%"><tr><td height=62 align=center>';
		echo '<table border=0 cellspacing=0 cellpadding=3 background=""><tr>';

		$x=0; 

		if (is_array($this->_sections)) {
			echo "<td rowspan=1 width=\"50%\">&nbsp;</td>";

			foreach ($this->_sections as $section) {
				echo "<td><table class=\"backend_menu\" id=\"middle$x\" border=0 cellspacing=0 cellpadding=0 background=\"/shared_files/pics/tab_middle.gif\"><tr>
						<td><img name=\"left$x\" src=\"/shared_files/pics/tab_left.gif\" width=5 height=29></td>
						<td nowrap><b><a href=\"#\" id=\"nav_section_$x\" onClick=\"changeMenu($x);return false;\" class=\"navlink\">&nbsp;".$section->getName()."&nbsp;</a></b></td>
						<td><img name=\"right$x\" src=\"/shared_files/pics/tab_right.gif\" width=5 height=29></td>
					</tr></table></td>";
				++$x;
			}



			if ($this->_show_print) {
			  echo "
				<td>
				<table class=\"backend_menu\" id=\"middle$x\" border=0 cellspacing=0 cellpadding=0 background=\"/shared_files/pics/tab_middle.gif\"><tr>
							<td><img name=\"left$x\" src=\"/shared_files/pics/tab_left.gif\" width=5 height=29></td>
							<td nowrap background='/shared_files/pics/tab_middle.gif'><a href=\"#\" id=\"nav_section_$x\" onClick=\"openPrintWd('$print_url',718,400,true);return false;\" target=\"_new\" class=\"navlink\"><img src=\"/shared_files/pics/print.gif\" width=20 height=20 border=0 alt=\"Print\"></a></td>
							<td><img name=\"right$x\" src=\"/shared_files/pics/tab_right.gif\" width=5 height=29></td>

				</tr></table></td>";
			  ++$x;
			}



			echo "
			<td".(!$this->_show_rb ? " width=\"50%\" nowrap" : '')."><table class=\"backend_menu\" id=\"middle$x\" border=0 cellspacing=0 cellpadding=0 background=\"/shared_files/pics/tab_middle.gif\"><tr>
						<td><img name=\"left$x\" src=\"/shared_files/pics/tab_left.gif\" width=5 height=29></td>
						<td nowrap><a href=\"#\" id=\"nav_section_$x\" onClick=\"changeMenu($x);return false;\" class=\"navlink\"><img src=\"/shared_files/pics/box/key.gif\" width=20 height=20 border=0 alt=\"Logout\"></a></td>
						<td><img name=\"right$x\" src=\"/shared_files/pics/tab_right.gif\" width=5 height=29></td>


					</tr></table></td>";

			++$x;

			if ($this->_show_rb) {
				echo "<td width=\"50%\" nowrap>
				      <table class=\"backend_menu\" id=\"middle$x\" border=0 cellspacing=0 cellpadding=0 background=\"/shared_files/pics/tab_middle.gif\"><tr>
				      <td><img name=\"left$x\" src=\"/shared_files/pics/tab_left.gif\" width=5 height=29></td>
							<td nowrap></td>
							<td></td>
						</tr></table></td>";
			}
		}



		$total_sections = count($this->_sections) + 1;
		if ($this->_show_print) $total_sections++;
		if ($this->_show_rb) $total_sections++;

		?>
		</tr>
		<tr>
			<td align=center colspan=<?= $total_sections + 1 ?>>&nbsp;&nbsp;<span id="submenu"><b>Please make a selection from the top menu.</b></span></td>
		</tr>
		</table>

		</td></tr></table>
		<? if (isset($master_section)) { ?>
		<script language="Javascript">
		<!--
		changeMenu(<?= $master_section ?>);
		// -->
		</script>
		<? }
	}
}

class JSMenuSection {
	var $_name;
	var $_contents;

	function JSMenuSection($name)
	{
		if (!$name)
			$name = '<i>untitled</i>';

		$this->_name = $name;
	}

	function addItem($url,$name,$target='')
	{
		$this->_contents[] = new JSMenuItem($url,$name,$target);
	}

	function getName() 		{ return $this->_name; }
	function getContents() 	{ return $this->_contents; }

	function toString()
	{
		$str = "<b>$this->_name</b><ol>";
		foreach ($this->_contents as $item) {
			$str .= '<li>'.$item->toString();
		}
		$str .= '</ol>';

		return $str;
	}
}

class JSMenuItem {
	var $_url;
	var $_target;
	var $_name;

	function JSMenuItem($url,$name,$target='')
	{
		$this->_url = $url;
		$this->_target = $target;
		$this->_name = $name;
	}

	function getUrl()    { return $this->_url; }
	function getTarget() { return $this->_target; }
	function getName()   { return $this->_name; }

	function toString()
	{
		return "<a href=\"$this->_url\"".(($this->_target == '') ? '' : " target=\"$this->_target\"")." class=\"navlink\">".addslashes(htmlentities($this->_name,ENT_COMPAT,'UTF-8'))."<\/a>";
	}
}

?>