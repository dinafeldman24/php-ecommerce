<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of weight
 *
 * @author ken
 */


/**
 *
 */
class Weight {

	/**
	 *
	 * @param <type> $weight format is lbs.oz,
	 * @param <type> $var
	 * @return <type> rounded up to the nearest lbs
	 *
	 * @example A weight of 5 lbs and 4 oz is 5.4, the oz part may not exceed 16.
	 *
	 */
	static public function multiply($weight,$var){
		$lbs_oz = explode('.',self::format($weight));

		$decimal_weight = ($lbs_oz[0]*$var) + ($lbs_oz[1]*$var/16);

		$lbs_oz= explode('.',$decimal_weight);

		//real calc		$calc_weight = $lbs_oz[0].'.'.($lbs_oz[1]*16);
		//round up 1 lbs
		if ($lbs_oz[1]>0)
			return $lbs_oz[0]+1;
		else
			return $lbs_oz[0];

		//return floatval($calc_weight);
	}
	
	static public function multiply_for_ups($weight,$var){
		$lbs_oz = explode('.',self::format($weight));

		$decimal_weight = ($lbs_oz[0]*$var) + ($lbs_oz[1]*$var/16);

		$lbs_oz= explode('.',$decimal_weight);

		//real calc		$calc_weight = $lbs_oz[0].'.'.($lbs_oz[1]*16);
		//round up 1 lbs
		return $lbs_oz;

		//return floatval($calc_weight);
	}
	
	/**
	 *
	 * @param <type> $weight_1 weight is expressed as 14lbs, 3oz has the format 14.3
	 * @param <type> $weight_2
	 * @return <type> rounded up to the nearest lbs
	 */
	static public function subtract($weight_1, $weight_2){
		$lbs_oz_1 = explode('.',self::format($weight_1));
		$lbs_oz_2 = explode('.',self::format($weight_2));


		$decimal_weight_1 = ($lbs_oz_1[0] ) + ($lbs_oz_1[1] / 16);
		$decimal_weight_2 = ($lbs_oz_2[0] ) + ($lbs_oz_2[1] / 16);

		$decimal_weight = $decimal_weight_1-$decimal_weight_2;

		$lbs_oz = explode('.',$decimal_weight);

		if ($lbs_oz[1]>0)
			return $lbs_oz[0]+1;
		else
			return $lbs_oz[0];

/*		$calc_weight = $lbs_oz[0].'.'.($lbs_oz[1]*16);

		return floatval($calc_weight); */
	}
	/**
	 *
	 * @param <type> $weight_1 weight is expressed as 14lbs, 3oz has the format 14.3
	 * @param <type> $weight_2
	 * @return <type> rounded up to the nearest lbs
	 */
	static public function add($weight_1, $weight_2){
		$lbs_oz_1 = explode('.',self::format($weight_1));
		$lbs_oz_2 = explode('.',self::format($weight_2));


		$decimal_weight_1 = ($lbs_oz_1[0] ) + ($lbs_oz_1[1] / 16);
		$decimal_weight_2 = ($lbs_oz_2[0] ) + ($lbs_oz_2[1] / 16);

		$decimal_weight = $decimal_weight_1+$decimal_weight_2;

		$lbs_oz = explode('.',$decimal_weight);

		if ($lbs_oz[1]>0)
			return $lbs_oz[0]+1;
		else
			return $lbs_oz[0];
		/*
		 * $calc_weight = $lbs_oz[0].'.'.($lbs_oz[1]*16);

		return floatval($calc_weight);
		*/
	}

	static function format($weight){
		$lbs_oz = explode('.',$weight);
		if ($lbs_oz[1]>0){
			$stub = strval($lbs_oz[1]);
			//$stub = rtrim($stub, '0'); //TE - we don't want to trim the 0 - 0.10 (10oz) is not the same as 0.1 (1oz)
			return $lbs_oz[0].'.'.$stub;

		}
		else
			return($weight);


	}

	static function display($weight){
		$lbs = $oz = null;
		$lbs_oz = explode('.',$weight);

		if ($lbs_oz[0]>0){
			$lbs = $lbs_oz[0].' lb';
			$lbs .= ($lbs_oz[0]>1)?'s':null;
		}


		if ($lbs_oz[1]>0){
			$oz = intval($lbs_oz[1])." oz";
			//$oz .= ($lbs_oz[1]>1)?"s":null;
		}
		
		return trim("$lbs $oz");



	}
}
/* testing critiera
 *  and a end comment to test 

include ('../application.php'); //for debut used to be at the top of the file

print_ar(Weight::multiply(.5,1));
print_ar(Weight::multiply(.5,2));
print_ar(Weight::multiply(.5,3));
print_ar(Weight::multiply(.5,4));
print_ar(Weight::multiply(.5,5));
print_ar(Weight::multiply(.5,6));

print_ar(Weight::subtract(17.4, 8.8));
print_ar(Weight::add(17.8, 8.8));
 /*
 */
?>
