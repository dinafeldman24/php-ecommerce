<?

Class Promo_Groups {

public static function get($id=0,$name=''){
	
	$sql= "SELECT promo_groups.* FROM promo_groups WHERE 1 ";
	
	if ($id >0 )
		$sql .= " AND promo_groups.id = ".$id;
	
	if ($name)
		$sql .= " AND promo_groups.name LIKE '".$name."'";

    $sql .= " ORDER BY promo_groups.id DESC ";

    return db_query_array($sql);	
}

 public static function get1($id='')
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }
	
	
	   public static function insert($info='', $group_array='')
    {
        if (!$info['group_list'] && $group_array)
			$info['group_list']=json_encode($group_array);
	    
		 if (is_array ($info['group_list']))
			$info['group_list']=json_encode($info['group_list']);
	    
		return db_insert('promo_groups', $info,'',true);
    }

	
	   public static function delete($id)
    {
        return db_delete('promo_groups', $id);
    }

    public static function update($id=0, $info='', $group_array='')
    {

         if (!$info['group_list'] && $group_array)
			$info['group_list']=json_encode($group_array);
	    
		 if (is_array ($info['group_list']))
			$info['group_list']=json_encode($info['group_list']);
		
        return db_update('promo_groups', $id, $info );
    }

	
}

?>