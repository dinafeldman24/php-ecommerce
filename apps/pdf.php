<?php

abstract class PDF {

	private $data;
	private $creator = 'RB';
	private $author = 'Tiger Chef';
	private $title = 'PDF Library Created Doc';
	private $options = array('nocopy'=>'nocopy','noassemble'=>'noassemble','nomodify'=>'nomodify','noaccessible'=>'noaccessible');
	private $pdf_buffer;
	private $password;
	private $auto_print = false;

	public function fillFields($data)
	{
		if (is_array($data)) {
			foreach ($data as $key => $val) {
				$this->data[$key] = $val;
			}
		}
	}

	public function setAuthor($author)
	{
		$this->author = $author;
	}

	public function setCreator($creator)
	{
		$this->creator = $creator;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function enableAutoPrintOption()
	{
		$this->auto_print = true;
	}

	public function autoPrintOptionEnabled()
	{
		return $this->auto_print;
	}

	public function setOptionNoPrint()
	{
		$this->options['noprint'] = 'noprint';
	}

	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function showPDF($filename='pdfdoc.pdf')
	{
		$this->parentCompile();
//		$this->lock();

		if ($this->pdf_buffer) {
			$len=strlen($this->pdf_buffer);
			header("Content-type: application/pdf");
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		    header("Content-Length: $len");
		    header("Content-Disposition: inline; filename=\"$filename\"");
			echo $this->pdf_buffer;
			unset($this->pdf_buffer);
		}
		else {
			throw new PDFFormException("No Buffer");
		}
	}

	public function savePDF($filename)
	{
		$this->parentCompile();
		//$this->lock();

		if ($this->pdf_buffer) {
			file_put_contents($filename,$this->pdf_buffer);
			unset($this->pdf_buffer);
		}
		else {
			throw new PDFFormException("No Buffer");
		}
	}

	public function setMasterPassword($password)
	{
		$this->password = $password;
	}

	private function parentCompile()
	{
		try {
			$pdflib = new PDFlib();

			// set licenses

			//$pdflib->set_parameter('license','B600602-010500-722786-2F3CC4');
			//$pdflib->set_parameter('license','B210702-010500-721071-A579C6');
			
			//$pdflib->set_parameter('license','L700602-010000-117099-N754G2-SX4KC2');

			// add v 1.6 compat and other defaults

			$optlist = 'compatibility=1.6 openmode=none pagelayout=onecolumn destination {type=fitwidth}';



			if ($pdflib->begin_document("", $optlist) == 0) {
				throw new PDFFormException($pdflib->get_errmsg());
		    }

		    // This line is required to avoid problems on Japanese systems

		    $pdflib->set_parameter("hypertextencoding", "winansi");

		    // some info settings

		    $pdflib->set_info("Creator", $this->creator);
		    $pdflib->set_info("Author", $this->author);
		    $pdflib->set_info("Title", $this->title);


		    $this->compile($pdflib);

		    $optlist = "viewerpreferences {}";

		    if ($this->auto_print) {
		    	$print_action = $pdflib->create_action('JavaScript','script {this.print(true);}');
		    	$optlist .= " action {open $print_action}";
			}

		    $pdflib->end_document($optlist);

		    $this->pdf_buffer = $pdflib->get_buffer();
		}
	    catch (PDFlibException $e) {
		    throw new PDFFormException("PDFlib exception occurred: [" . $e->get_errnum() . "] " . $e->get_apiname() . ": " . $e->get_errmsg());
		}

	    unset($pdflib);
	}

	abstract protected function compile(&$pdflib);

	private function lock()
	{
		try {
			$plop = new PLOP();

			//$plop->set_parameter('license','B210702-010500-721071-A579C6');
			//$plop->set_parameter('license','B210702-010500-721072-B09369');
			$plop->set_parameter('license','L300702-010000-114867-2EN952-CJZ522');
			//$plop->set_parameter('license','L210702-010000-721072-5592F8');

		    if (!$plop->open_mem($this->pdf_buffer,'')) {
				throw new PDFFormException($plop->get_errmsg());
		    }

		    if (!$this->password) {
		    	//$this->password = md5(uniqid(rand(),1));
		    	$this->password = 'bizlic';
		    }

		   // $optlist = "masterpassword {$this->password} keylength 128 ";

		   // $optlist .= 'permissions {'.implode(' ',$this->options) . '}';

		    if (!$plop->create_file('', $optlist)) {
				throw new PDFFormException($plop->get_errmsg());
		    }

		    $this->pdf_buffer = $plop->get_buffer();
		    $plop->close();
		    $plop->delete();
		}
		catch (PLOPlibException $e) {
			throw new PDFFormException("PLOPlib exception occurred: [" . $e->get_errnum() . "] " . $e->get_apiname() . ": " . $e->get_errmsg());
		}

	    unset($plop);
	}

	public function getPassword()
	{
		return $this->password;
	}
}

class PDFFormException extends Exception {

   public function __construct($message, $code = 0)
   {
       parent::__construct($message, $code);
   }

   public function __toString()
   {
       return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
}

?>