<? 
	class WebsiteOrderCancellations
	{
		static function getFromLastThreeDays()
		{
			$sql = "SELECT * FROM website_order_cancellations WHERE last_updated > date_sub(now(), interval 3 day)";
			$result = db_query_array($sql);
			return $result;
		}
	}
?>