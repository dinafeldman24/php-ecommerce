<?php
/*****************
*   Wrapper for eMerchant
*   Gateway CC Processing
*   Requires usaepay.php
*   Hadassa Golovenshitz
*   Jan 15 2015
*
*****************/

class eMerchantCc{

    var $tran;
    var $refnum;
    var $result;
    var $authcode;
    var $avs_result;
    var $cvv2_result;
    var $cardref;
    var $error;
	var $errorcode;
    var $transactionStatus;
    var $is_test=false;
    var $usesandbox=false;
    
    function eMerchantCc($gateway=''){
        global $CFG;
		$tran=new umTransaction;
		if($gateway=='americard'){
			if($CFG->tc_emerchant_use_live == 'No'){
            $tran->key=$CFG->tc_americard_key_test;
            $tran->pin=$CFG->tc_americard_pin_test;
            $tran->usesandbox=true;
            $this->is_test = true;
			}
			else{
				$tran->key=$CFG->tc_americard_key_live;
				$tran->pin=$CFG->tc_americard_pin_live;
			}
		}
		elseif($gateway=='fidelity'){
			if($CFG->tc_fidelity_use_live == 'No'){
            $tran->key=$CFG->tc_fidelity_key_test;
            $tran->pin=$CFG->tc_fidelity_pin_test;
            $tran->usesandbox=true;
            $this->is_test = true;
          
			}
			else{
				$tran->key=$CFG->tc_fidelity_key_live;
				$tran->pin=$CFG->tc_fidelity_pin_live;
			}
		}
    	elseif($gateway=='cardknox-usaepay-emulator'){
    		$tran->gatewayurl = "https://x1.cardknox.com/gate";
			if($CFG->cardknox_usaepay_emulation_use_live == 'No'){
            $tran->key=$CFG->cardknox_key_test;            
            $tran->usesandbox=true;
			}
			else{
				$tran->key=$CFG->cardknox_key_live;
			}			
		}
		else{
			if($CFG->tc_emerchant_use_live == 'No'){
				$tran->key=$CFG->tc_emerchant_key_test;
				$tran->pin=$CFG->tc_emerchant_pin_test;
				$tran->usesandbox=true;
	            $this->is_test = true;
			}
			else{
				$tran->key=$CFG->tc_emerchant_key_live;
				$tran->pin=$CFG->tc_emerchant_pin_live;
			}
		}
		$tran->ip=$_SERVER['REMOTE_ADDR'];   // This allows fraud blocking on the customers ip address 
		$tran->testmode=$this->is_test;    
        $tran->ignoresslcerterrors=$this->is_test;
        $this->tran = $tran;
    }

    
    function getAuthAtCheckout($cc_num, $exp_date, $checkout_obj){
        $tran = $this->tran;
		$tran->card=$cc_num;
		$tran->exp=$exp_date;
		$tran->amount=$checkout_obj->grand_total;
		$tran->invoice = $this->generateInvoiceNumber();
		$tran->cardholder=$checkout_obj->cart2['billing_fname'] . ' ' . $checkout_obj->cart2['billing_lname'];
		$tran->street=$checkout_obj->cart2['billing_address'];
		$tran->billstreet=$checkout_obj->cart2['billing_address'];
		$tran->billcity=$checkout_obj->cart2['billing_city'];
		$tran->billstate=$checkout_obj->cart2['billing_state'];
		$tran->billzip=substr($checkout_obj->cart2['billing_zip'], 0, 5);
		$tran->zip=substr($checkout_obj->cart2['billing_zip'], 0, 5);
		$tran->description='';
		$tran->cvv2=$checkout_obj->cart3['cc_ccv'];
		$tran->shipfname=$checkout_obj->cart2['shipping_fname'];
		$tran->shiplname=$checkout_obj->cart2['shipping_lname'];
		$tran->shipstreet=$checkout_obj->cart2['shipping_address'];
		$tran->shipstreet2=$checkout_obj->cart2['shipping_address2'];
		$tran->shipcity=$checkout_obj->cart2['shipping_city'];
		$tran->shipstate=$checkout_obj->cart2['shipping_state'];
		$tran->shipzip=substr($checkout_obj->cart2['shipping_zip'], 0, 5);
		$this->tran = $tran;
        $success = $this->doAction('authonly');
        return ($success) ? 'success' : 'failure';
	}

    function capture($ref, $amount){
        $original_payments = Payments::get(0, 0, '', $ref);
        $original_payment = $original_payments[0];
        if($original_payment['is_captured']){
            $token = $original_payment['token'];
            return $this->saleByToken($amount, $token);
        }
        $success = $this->doAction('capture', $ref, $amount);
        if($success){
            $info  = array('is_captured' => 1);
            $res = Payments::update($original_payment['id'], $info);
        }
        return $success;
    }

    function saleByRef($amount, $ref=null){
        return $this->doAction('sale', $ref, $amount);
    }

    function saleByToken($amount, $token){
        return $this->doAction('sale', null, $amount, null, $token);
    }
    
    function sale($cc_num, $exp, $amount, $fields, $token=null){
        $tran = $this->tran;
		$tran->invoice = $fields['ordernum'];
        $tran->card = $cc_num;
        $tran->exp = $exp;
		$tran->cardholder=$fields['first_name'] . ' ' . $fields['last_name'];
		$tran->street=$fields['streetaddress'];
		$tran->billstreet=$fields['streetaddress'];
		$tran->billcity=$fields['city'];
		$tran->billstate=$fields['state'];
		$tran->billzip=$fields['zipcode'];
		$tran->zip=$fields['zipcode'];
		$tran->description='';
		$tran->cvv2=$fields['ccv2'];
		$tran->shipfname=$fields['shipto_fname'];
		$tran->shiplname=$fields['shipto_lname'];
		$tran->shipstreet=$fields['shipto_street'];
		$tran->shipstreet2=$fields['shipto_street2'];
		$tran->shipcity=$fields['shipto_city'];
		$tran->shipstate=$fields['shipping_state'];
		$tran->shipzip=$fields['shippingto_zipcode'];
        return $this->doAction('sale', null, round($amount, 2), null, $token);
    }

    function creditvoid($ref, $amount, $notes, $token=''){
        return $this->doAction('creditvoid', $ref, $amount, $notes, $token);
    }

    function refundadjust($ref, $amount, $notes, $token=''){    	
        return $this->doAction('refund:adjust', $ref, $amount, $notes, $token);
    }   
	function cccreditvoid($ref, $amount, $notes, $token=''){
        return $this->doAction('refund', $ref, $amount, $notes, $token);
    }
	function ccvoid($ref, $amount, $notes, $token=''){
        return $this->doAction('void', $ref, $amount, $notes, $token);
    }
    function doAction($action, $ref=null, $amount=null, 
        $notes=null, $token=null, $order_id = null){
        $token = trim($token);
        $tran = $this->tran;
        if($token != null && !empty($token)){
            if(is_null($order_id)){
                 $order_id = $this->generateInvoiceNumber();
            }
            $tran->card=$token;
            $tran->exp='0000';
        }
        if(!is_null($ref)){
            $tran->refnum = $ref;
        }
        if(!is_null($amount)){
            $tran->amount=$amount;
        }
        if(!is_null($notes)){
            $tran->custom1=$notes;
        }
        if(!is_null($order_id)){
            $tran->invoice = $order_id;
        }
		$tran->command=$action;
        $tran->savecard=true;
        $tran->Process();
        $success = (int)$tran->errorcode == 0;
        $this->setResponseValues($tran);
        $this->setTransStatus($success);
        $this->tran = $tran;
        return $success;
    }

    function setRefNum($ref){
        $this->refnum = $ref;
    }

    function getRefNum(){
        return $this->refnum;
    }

    function setAuthCode($code){
        $this->authcode = $code;
    }

    function getAuthCode(){
        return $this->authcode;
    }

    function setError($error){
        $this->error = $error;
    }

    function getError(){
        return $this->error;
    }

    function getErrorcode()
    {
        return $this->errorcode;
    }

    public function setErrorcode($errorcode)
    {
        $this->errorcode = $errorcode;
    }

    function setResult($res){
        $this->result = $res;
    }

    function getResult($res){
        return $this->result;
    }

    function setTransStatus($is_passed){
        $this->transactionStatus = $is_passed;
    }
 
    function getTransStatus($is_passed){
        return $this->transactionStatus;
    }
 
    function setAvsRes($res){
        $this->avs_result = $res;
    }

    function getAvsResult(){
        return $this->avs_result;
    }

    function setCvv2Res($res){
        $this->cvv2_result = $res;
    }   

    function getCvv2Result($res){
        return $this->cvv2_result;
    }

    function setCardRef($ref){
        $this->cardref = $ref;
    }

    function getCardRef($ref){
        return $this->cardref;
    }

    function setResponseValues($tran){
        $this->setRefNum($tran->refnum);
        $this->setAuthCode($tran->authcode);
        $this->setError($tran->error);
		$this->setErrorCode($tran->errorcode);
        $this->setResult($tran->result);
        $this->setAvsRes($tran->avs_result);
        $this->setCvv2Res($tran->cvv2_result);
        $this->setCardRef($tran->cardref);
    }   
    
    function generateInvoiceNumber(){
        return substr(md5(session_id().time()), 0, 17); 
    }
}





?>
