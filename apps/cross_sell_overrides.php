<? 
class CrossSellOverrides
{
	static function get($id=0, $base_prod_id=0, $cross_sell_prod_id=0, $order='',$order_asc='', $is_active_and_not_deleted = '')
	{
		$sql = "SELECT ";
	    $sql .= " cross_sell_overrides.* ";		
		$sql .= " FROM cross_sell_overrides, products ";
		$sql .= " WHERE 1 AND cross_sell_overrides.cross_sell_prod_id = products.id";

		if ($id > 0) {
		    $sql .= " AND cross_sell_overrides.id = $id ";
		}		
		if ($base_prod_id > 0) {
		    $sql .= " AND cross_sell_overrides.base_prod_id = $base_prod_id ";
		}
		if ($cross_sell_prod_id > 0) {
		    $sql .= " AND cross_sell_overrides.cross_sell_prod_id = $cross_sell_prod_id ";
		}	
		if ($is_active_and_not_deleted == 'Y') {
		    $sql .= " AND products.is_active = 'Y' AND products.is_deleted = 'N'";
		}	
		if($order){
			$sql .= " ORDER BY ";
		
			$sql .= addslashes($order);
		
			if ($order_asc !== '' && !$order_asc) {
				$sql .= ' DESC ';
			} 
		}		
	    $ret = db_query_array($sql);
		return $ret;
	}
	
	static function get1($id = 0){
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}
	
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('cross_sell_overrides', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
			
		return db_insert('cross_sell_overrides', $info);
	}
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
			
		return db_update('cross_sell_overrides',$id,$info);
		
	}
}
?>