<?

class Blog{

	static function insert($info){

		if(!isset($info['date_added'])){
			$d = "date_added";
		}
		$result = db_insert("blog",$info,$d);
		//Content::updateSitemap();
		return $result;
	}

	static function update($id,$info){
		if(!isset($info['date_modified'])){
			$d = "date_modified";
		}
		$result = db_update("blog",$id,$info,'id',$d);
		//Content::updateSitemap();
		return $result;
	}

	static function delete($id){

		db_delete("blog_cat_mm",$id,'blog_id');
		db_delete("blog_tags",$id,'blog_id');
		return db_delete("blog",$id);
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT blog.*, prim_cat.name as primary_category, ";
		$select_ .= " IF(date_active = '0000-00-00 00:00:00', date_added, date_active) as date_active ";
		$select_ .= ", CONCAT(admin_users.first_name,' ',admin_users.last_name) as blog_author ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(blog.id)) as total ";
		}

		$from_ = " FROM blog ";

		$join_ .= " LEFT JOIN admin_users ON admin_users.id = blog.author_id ";
		$join_ .= " LEFT JOIN blog_cats prim_cat ON prim_cat.id = blog.primary_cat_id ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND blog.id = [id] ";
		}
		if(isset($o->is_active)){
			$where_ .= " AND blog.is_active = [is_active] ";

			if($o->is_active == "Y"){
				$where_ .= " AND blog.date_active < NOW() ";
			} else if($o->is_active == "N"){
				$where_ .= " AND blog.date_active > NOW() ";
			}
		}
		if(isset($o->cat_id)){
			$join_ .= " LEFT JOIN blog_cat_mm bcat ON bcat.blog_id = blog.id ";
			$join_ .= " LEFT JOIN blog_cats ON blog_cats.id = bcat.cat_id ";
			$where_ .= " AND ( bcat.cat_id = [cat_id] OR blog_cats.parent_id = [cat_id] ) ";

			$groupby_ = " GROUP BY blog.id ";
		}
		if(isset($o->author_id)){
			$where_ .= " AND blog.author_id = [author_id] ";
		}
		if(isset($o->month)){
			$where_ .= " AND DATE_FORMAT(date_active,'%Y-%m') = [month] ";
		}
		if(isset($o->search)){
			$name = str_replace("%","",$o->search);
			$name = str_replace("_","",$name);
			$name = str_replace("\\","",$name);
			$name = db_esc($name);

			$join_ .= " LEFT JOIN contentnew ON contentnew.page_id = blog.id ";
			$join_ .= " LEFT JOIN blog_tags bt ON bt.blog_id = blog.id ";
			$joined_tags = true;

			$where_ .= " AND contentnew.page_type =  'blog' ";
			$where_ .= " AND ". db_split_keywords($name,array(
				'blog.meta_keywords',
				'blog.meta_description',
				'contentnew.text',
				'bt.tag'
				),'AND',true);

			if(!$o->total){
				$groupby_ = " GROUP BY blog.id ";
			}

		}

		if(isset($o->url)){
			$o->url = trim($o->url,'/');
			$where_ .= " AND blog.url = [url] ";
		}

		if(isset($o->tag)){
			if(!$joined_tags){
				$join_ .= " LEFT JOIN blog_tags bt ON bt.blog_id = blog.id ";
			}

			$o->tag = str_replace("-"," ",$o->tag);

			$where_ .= " AND bt.tag = [tag] ";

			if(!$o->total){
				$groupby_ = " GROUP BY blog.id ";
			}

		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY blog.date_active DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having. $orderby_. $limit_;

		$result = db_template_query($sql,$o,'','','','',false);



		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function getByUrl($url){
		$o->url = $url;

		return db_get1( self::getByO($o) );
	}

	/**
	 * Return blog's tags as string with separator or array of tags
	 *
	 * @param unknown_type $blog_id
	 * @param unknown_type $separator
	 * @return unknown
	 */
	static function getBlogTags($blog_id, $separator=null){
		$o->id = $blog_id;
		$sql = " SELECT tag FROM blog_tags WHERE blog_id = [id] GROUP BY tag ";
		$results = db_template_query($sql,$o);

		if($results){
			if($separator === null){
				return $results;
			}

			$ret = "";
			foreach($results as $r){
				$ret .= $r['tag'] . $separator;
			}

			return trim($ret,$separator);
		}

		return "";
	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	static function assignCats($blog_id,$cat_array){
		$blog_id = (int)$blog_id;

		if(!$blog_id){
			return false;
		}

		db_delete("blog_cat_mm",$blog_id,"blog_id");

		if(is_array($cat_array))foreach($cat_array as $cat_id){
			$info['blog_id'] = $blog_id;
			$info['cat_id'] = $cat_id;
			db_insert("blog_cat_mm",$info);
		}

		return;
	}

	static function assignTags($blog_id,$tag_array){
		$blog_id = (int)$blog_id;

		if(!$blog_id){
			return false;
		}

		db_delete("blog_tags",$blog_id,"blog_id");

		$tag_array = explode(",",$tag_array);

		if(is_array($tag_array))foreach($tag_array as $tag){
			$info['blog_id'] = $blog_id;
			$info['tag'] = strtolower(trim($tag));

			if($info['tag']){
				db_insert("blog_tags",$info);
			}
		}

		return;
	}

	static function getAuthors($id=''){
		$sql = " SELECT CONCAT(u.first_name,' ',u.last_name) as author_name, author_id ";
		$sql .= " FROM blog ";
		$sql .= " LEFT JOIN admin_users u ON u.id = author_id ";

		$sql .= " WHERE 1 ";
		if($id > 0){
			$id = (int)$id;
			$sql .= " AND u.id = '$id' ";
		}

		$sql .= " AND blog.is_active = 'Y' ";
		$sql .= " AND blog.date_active < NOW() ";


		$sql .= " GROUP BY author_id ";

		return db_query_array($sql);
	}

	static function getArchiveMonthLinks(){
		global $CFG;

		$sql = "SELECT
					count(blog.id) as count,
					DATE_FORMAT(blog.date_active,'%Y-%m') as publish_date,
					DATE_FORMAT(blog.date_active,'%M %Y') as pretty_date
					FROM `blog`

					WHERE 1

					AND blog.is_active = 'Y'
		            AND blog.date_active < NOW()

					GROUP BY DATE_FORMAT(blog.date_active,'%M %Y')
					ORDER BY publish_date DESC
					";

		// echo $sql;

		return db_query_array($sql);

	}

	static function updateBlogRSS()
	{
		global $CFG;

		$o->is_active = "Y";
		$o->limit = 15;
		$recent_blogs = self::getByO($o);

		$rss = '<?xml version="1.0" encoding="ISO-8859-1"'.'?'.">\r\n";
		$rss .= "
	<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
	  <channel>
	  	<atom:link href=\"$CFG->blog_rss\" rel=\"self\" type=\"application/rss+xml\" />
	    <title>$CFG->company_name Blog</title>
	    <description>The leading online source for restaurant equipment and supply. Free shipping over $50. Corporate clients welcome.</description>
	    <link>".Catalog::blogLink(0)."</link>
	    <language>en</language>
	    <copyright>Copyright &#169; 2009-".date('Y')." $CFG->company_name &#183; All Rights Reserved</copyright>
	    <webMaster>$CFG->company_email</webMaster>
	    ";

	    if($recent_blogs)foreach ($recent_blogs as $blog) {
	    	$the_content = YContent::get1PageContent($blog['id'],'','blog');
			$the_content = $the_content[$CFG->use_lang];
			$blog_text = $the_content['body'];
			$break_pos = strpos($the_content['body'],"[more]");
			if($break_pos !== false){
				$blog_text = substr($blog_text,0,$break_pos) . " <a href='".Catalog::blogLink($blog)."'>...continue reading</a></p>";
			} else {
				// $blog_text .= " <img src='/images/small_square.png' alt='end of story' style='float:right;'/>";
			}
	    	$rss .= "
<item>
  <title>".xmlentities($the_content['window_title'])."</title>
  <description>".xmlentities($blog_text.'<hr/>Source: <a href="'.Catalog::blogLink($blog['id']).'">'.$the_content['link_title'].'</a> at <a href="'.$CFG->baseurl.'">'.$CFG->company_name.'</a>.')."</description>
  <link>".Catalog::blogLink($blog['id'])."</link>
  <pubDate>".db_date($blog['date_active'],'D, d M Y H:i:s T')."</pubDate>
  <guid>".Catalog::blogLink($blog['id'])."</guid>
</item>
	    ";
	    }

	    $rss .= "
	  </channel>
	</rss>";


		$fp = fopen($CFG->blog_write_path, 'w');
		fwrite($fp, $rss);
		fclose($fp);
	}


	static function getTagCloud(){

		// Get Total Tags
		$sql = " SELECT COUNT(tag) as total FROM blog_tags
				 LEFT JOIN blog ON blog.id = blog_id
				 WHERE
				 	blog.is_active = 'Y'
		            AND blog.date_active < NOW()
		";
		$recset = db_query_array($sql,'',true);
		$total = max(1,(int)$recset['total']);

		$sql = " SELECT * FROM (
				SELECT tag, COUNT(tag) as total, COUNT(tag)/$total as fraction
				 FROM blog_tags
				 LEFT JOIN blog ON blog.id = blog_id
				 WHERE
				 	blog.is_active = 'Y'
		            AND blog.date_active < NOW()
				 GROUP BY tag
				 ORDER BY total DESC
				 LIMIT 30 ) t1

				 ORDER BY tag ASC

				 ";
//		echo $sql;

		$recset = db_query_array($sql);

		return $recset;
	}

	static function getLegacyComments($blog_id,$get_total=false){

		$blog_id = (int)$blog_id;

		$sql  = " SELECT blog_comments.* FROM blog_comments ";
		$sql .= " LEFT JOIN blog ON blog.legacy_id = blog_comments.comment_post_ID ";
		$sql .= " WHERE blog.id = '$blog_id' ";
		$sql .= " AND comment_type <> 'pingback' AND comment_approved = 1
			ORDER BY comment_date ASC
		";

		$result = db_query_array($sql);

		if($get_total){
			return $result ? count($result) : 0;
		}

		return $result;
	}

}