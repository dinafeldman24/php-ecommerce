<?
class RefundCodes {
	static function insert($info) {
		return db_insert ( 'refund_codes', $info);
	}
	static function update($id, $info) {
		return db_update ( 'refund_codes', $id, $info, 'id');
	}
	static function delete($id) {
		return db_delete ( 'refund_codes', $id );
	}
	static function get($id = 0, $order_id = 0, $order = '', $order_asc = '', $limit = 0, $start = 0, $get_total = false, $code = '') {
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(refund_codes.id)) AS total ";
		} else {
			$sql .= " refund_codes.* ";
		}
		
		$sql .= " FROM refund_codes ";
		
		$sql .= " WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND refund_codes.id = $id ";
		}
		
		if ($order_id) {
			$sql .= " AND `refund_codes`.order_id = $order_id";
		}
		
		if ($code) {
			$sql .= " AND `refund_codes`.code LIKE '$code'";
		}
		
		if (! $get_total) {
			$sql .= ' GROUP BY refund_codes.id ';
			
			if ($order) {
				$sql .= " ORDER BY ";
				
				$sql .= addslashes ( $order );
				
				if ($order_asc !== '' && ! $order_asc) {
					$sql .= ' DESC ';
				}
			} else {
				$sql .= " ORDER BY refund_codes.id DESC";
			}
		}
		
		if ($limit > 0) {
			$sql .= db_limit ( $limit, $start );
		}
		
		if (! $get_total) {
			$ret = db_query_array ( $sql );
		} else {
			$ret = db_query_array ( $sql );
			return $ret [0] ['total'];
		}
		return $ret;
	}
	static function get1($id) {
		$id = ( int ) $id;
		
		if (! $id)
			return false;
		$result = self::get ( $id );
		
		return is_array ( $result ) ? array_shift ( $result ) : false;
	}
	
	static function get1ByCode($code) {
		if (! $code)
			return false;
		$result = self::get(0,0,'','',0,0,false,$code);
		
		return is_array ( $result ) ? array_shift ( $result ) : false;
	}
}

