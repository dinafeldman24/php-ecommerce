<?php

class Review_Comments
{
    public static function get($id = 0, $review_id = 0, $customer_id = 0,$approved='',$sent_email='')
    {
        $sql = "SELECT review_comments.*
                FROM review_comments
                WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND review_comments.id = $id ";
        }

        if ($review_id > 0)
        {
            $sql .= " AND review_comments.review_id = $review_id ";
        }

        if ($customer_id > 0)
        {
            $sql .= " AND review_comments.user_id = $customer_id ";
        }

   			if ($approved != '')
        {
            $sql .= " AND review_comments.approved = '{$approved}' ";
        }
				
					if ($sent_email != '')
        {
            $sql .= " AND review_comments.sent_email = '{$sent_email}' ";
        }

        
            $sql .= " ORDER BY review_comments.ts DESC ";

        return db_query_array($sql);
    }
		
	


    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info, $review_id, $customer_id)
    {
        $info['review_id']= $review_id;
        $info['user_id']  = $customer_id;   
				$safecontent=mysql_real_escape_string($info['content'] );
				$info['content']=$safecontent;
				if (!$info['approved']){
				$info['approved']='N';
        }
	        return db_insert('review_comments', $info,'',true);
    }

    public static function delete($id)
    {
        return db_delete('review_comments', $id);
    }

    public static function update($id, $info)
		
    {
    		if ($info['content']){
				$safecontent=mysql_real_escape_string($info['content'] );
				$info['content']=$safecontent;
				}
						
        return db_update('review_comments', $id, $info );
    }


}
?>