<?php

class Customers {

	static function insert($info, $type='email')
	{
		if( isset( $info['phone'] ) )
		{
			//need to strip all non numeric chars
			$info['phone'] = preg_replace('/[^0-9]+/','', $info['phone'] );
		}
		
		if( $type == 'email' )
			$cust = Customers::get(0,'','','','','',$info['email']);
		else
			$cust = Customers::searchByPhone( $info['phone'] );

		if(is_array($cust) && count($cust))
		{//Since Customers Don't ever log in, if this is found I should probably update the customer info to the newly inputted info
		 //Otherwise you could end up with some orders going to the wrong place no?
			if( count( $cust ) == 1 )
				return $cust[0]['id'];
			else //found more than 1 with a match, for now just return first one, not a real big deal since customers dont login
				return $cust[0]['id'];
		}

		$id = db_insert('customers',$info);
		
		//encrypt password and update
		if(isset($info['password'])){
			
			$info2['password'] = md5(md5($id) . $info['password'] . $id);
			self::update($id,$info2,false);
		}
		
		return $id;
		
	}

	static function update($id,$info,$encrypt=true)
	{
		if( isset( $info['phone'] ) )
		{
			//need to strip all non numeric chars
			$info['phone'] = ereg_replace('[^0-9x]+','', $info['phone'] );
		}
		
		if(isset($info['password'])  && $encrypt){
			//encrypt password
			$info['password'] = md5(md5($id) . $info['password'] . $id);
		}
		
		return db_update('customers',$id,$info);
	}

	static function delete($id)
	{
		db_delete('customer_shipping_addresses', $id, 'customer_id');
		return db_delete('customers',$id);
	}

	static function get($id=0,$begins_with='',$keywords='',$status='',$order='',$order_asc='', $email = '', $password = '', $get_active_only = false, $rep = '', $role_id = '', $institution_type_id = '', $start_date = '', $end_date = '',$limited=false)
	{
			if ($limited) $sql = "SELECT customers.id, customers.first_name, customers.last_name, customers.email, 
						customers.city, customers.state,customers.account_system, customers.sign_up_date, customers.account_activated, customers.phone, ";
		else  $sql = "SELECT customers.*,";	
		$sql .= " COUNT(DISTINCT orders.id) AS num_orders,
					customers.id AS cust_id, 
					customer_roles.caption AS role_caption,
					customer_institution_types.caption AS institution_type_caption,
					admin_users.first_name as rep_first_name, admin_users.last_name as rep_last_name  
				FROM customers
				LEFT JOIN orders ON orders.customer_id = customers.id
				LEFT JOIN customer_roles ON customers.role_id = customer_roles.id
				LEFT JOIN customer_institution_types ON customers.institution_type_id = customer_institution_types.id
				LEFT JOIN admin_users ON customers.dedicated_rep = admin_users.id
				WHERE 1 ";
	
		if ($id > 0) {
			$id = (int)$id;
			$sql .= " AND customers.id = $id ";
		}
		if ($email != '') {
			$sql .= " AND customers.email = '".db_esc($email)."' ";
		}
		//if ($password != '') {
		//	$sql .= " AND customers.password = '".db_esc($password)."' ";
		//}
		//if ($password != '') {
		//	$sql .= " AND customers.password = '".db_esc(md5($password))."' ";
		//}
		if ($password != '') {
			if(!$id && $email != ''){
				$id = Customers::getIdByEmail($email);
			}
			if($id >0){
				$sql .= " AND customers.password = '".db_esc(md5(md5($id) . $password . $id))."' ";
			} else {
				//SHOULDN'T BE AN ELSE ????
			}
		}
		if ($begins_with != '') {
			$sql .= " AND customers.last_name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') {
			$fields = Array('customers.last_name','customers.first_name','customers.email','customers.city','customers.state');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}

		if ($get_active_only)
		{
			$sql .= " AND (customers.account_system = 'old' OR (customers.account_system = 'new' and customers.account_activated = 'Y'))";
			
		}
		if ($rep != '')
		{
			$sql .= " AND customers.dedicated_rep = $rep";
		}
		if ($role_id != '')
		{
			$sql .= " AND customers.role_id = $role_id";
		}
		if ($institution_type_id != '')
		{
			$sql .= " AND customers.institution_type_id = $institution_type_id";
		}		
		if ($start_date != '' && $end_date != '')
		{
			$sql .= " AND customers.sign_up_date BETWEEN '$start_date' AND '$end_date'";
		}
		$sql .= " GROUP BY customers.id
				  ORDER BY ";

		if ($order == '') {
			$sql .= 'customers.last_name,customers.first_name';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

		return db_query_array($sql);
	}

	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Customers::get($id);
		return $result[0];
	}


	static function getByLogin($email, $password, $active_only)
	{
		if ($email=='' || $password=='') return false;
		
		$id = Customers::get('','','','','','',$email);
		$result = Customers::get(0,'','','','','',$email,$password, $active_only);
		return $result[0];
	}
	
	function getIdByEmail($email)
	{
		$sql = "SELECT id FROM customers WHERE customers.email = '".db_esc($email)."' ";
		$id = db_query_array($sql);
		return $id[0]['id'];
	}
	
	static function getAddresses($customer_id, $id = '')
	{
		$sql = "SELECT * FROM customer_shipping_addresses WHERE customer_id = '$customer_id' ";
		if($id != '') 		$sql .= " AND id = '$id' ";
		$sql .= ' ORDER BY name';

		return db_query_array($sql);
	}

	static function getPrimaryBillingAddress($customer_id, $id = '')
	{
		$sql = "SELECT * FROM customer_shipping_addresses WHERE customer_id = '$customer_id' AND billing = 'Y' AND primary_address = 'Y'";
		if($id != '') 		$sql .= " AND id = '$id' ";
		$sql .= ' ORDER BY name';

		return db_query_array($sql);
	}

	static function getPrimaryShippingAddress($customer_id, $id = '')
	{
		$sql = "SELECT * FROM customer_shipping_addresses WHERE customer_id = '$customer_id' AND billing = 'N' AND primary_address = 'Y'";
		if($id != '') 		$sql .= " AND id = '$id' ";
		$sql .= ' ORDER BY name';

		return db_query_array($sql);
	}

	static function addAddress($customer_id, $info)
	{
		$info['customer_id'] = $customer_id;
		return db_insert('customer_shipping_addresses',$info);
	}

	static function updateAddress($address_id, $info)
	{
	    return db_update('customer_shipping_addresses', $address_id, $info);
	}

	static function deleteAddress($customer_id,$id)
	{
		$sql = "DELETE FROM customer_shipping_addresses WHERE id = '$id' AND customer_id = '$customer_id' ";
		return db_query($sql);
	}

	static function searchByPhone( $phone_numb )
	{
		//phone numbers should all be stripped of leading 1s and are just digits
		if( strlen( $phone_numb ) > 10 )
			if( $phone_numb[0] == 1 )
			{//strip the leading 1 for the search
				echo $phone_numb;
				$phone_numb = substr( $phone_numb, 1 );
			}

		$sql = " SELECT * FROM customers WHERE phone like '$phone_numb' ";
		$results = db_query_array( $sql );
		return $results;
	}
	static function getOneByHash($hash)
	{
		$hash = mysql_real_escape_string($hash);
		$sql = "SELECT * FROM  `customers` WHERE MD5( CONCAT( first_name,' ',last_name,' ', email,' ',id ) ) = '".$hash."'";
		$result = db_query_array( $sql );
		
		return $result;		
	}
	
	static function getPendingRewardsEmailChoice($customer_id){
		$sql = "SELECT email_pending_rewards FROM customers
			WHERE id = '$customer_id' ";
		$result = db_query_array( $sql );

		return $result[0]['email_pending_rewards'];
	}

	static function seeIfEssensaMember($order_id='',$customer_id='')
	{
		if ($customer_id){
			$cust_id = $customer_id;
		} elseif($order_id){
			$order_info = Orders::get1($order_id);
			$cust_id = $order_info['customer_id'];
		} else {
			$cust_id = $_SESSION['cust_acc_id'];
		}
		
		$see_if_active_essensa_member_sql = "SELECT * FROM essensa_members, customers WHERE
			customers.id = " . $cust_id ." AND
			customers.ein_number = essensa_members.gpo_id AND essensa_members.member_status = 'Active'";
		$see_if_active_essensa_member = db_query_array($see_if_active_essensa_member_sql);
			
		if ($see_if_active_essensa_member) return true;
		else return false;
	}
	static function isVerifiedBuyer($customer_id, $product_id) {
	  
		$sql ="SELECT orders. * , order_items.id AS order_id, order_items.product_id AS product_id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id
          WHERE order_items.product_id ={$product_id}
          AND orders.customer_id ={$customer_id}
					AND orders.current_status_id =4";   
	 return db_query_array($sql);
	 }
	
	 static function getSavedCards($cust_id){
        $save_card_sql = "SELECT * FROM customer_payment_information
            WHERE is_active = 1 AND customer_id = $cust_id";
        return db_query_array($save_card_sql);

     }

     static function saveCard($cust_id, $card_token, $last_four){
        $info = array('customer_id' => $cust_id,
            'card_token' => $card_token,
            'last_four' => $last_four
            );
        db_insert('customer_payment_information', $info);
     }
		 
	 static function get1bykey($key=''){
		 if (!$key) return false;

     		$sql = "SELECT customers.* from customers where customers.key='".db_esc($key)."'";
		    $alreadythere=db_query_array($sql);
				return $alreadythere[0];
		 } 
    static function getStatementLines($cust_id, $from_date = '2015-01-08', $to_date = '' ){
        global $CFG;
        if(empty($to_date)){                                           
            $to_date = date('Y-m-d');                                  
        }                                                              
        $sql =  "select oc.id as id, o.id as order_id, date_format(oc.date, '%m/%d/%y') as transaction_date, oc.date, oc.amount, 'credit' as line_type, 
        'xx' as due_date, 0 as days_late, '' as shipping_name
        FROM order_charges oc JOIN orders o ON oc.order_id = o.id 
        WHERE o.customer_id = $cust_id and oc.action in ('capture', 'sale') 
        AND DATE(oc.date) BETWEEN '$from_date' AND  '$to_date'
        AND o.current_status_id NOT IN " .$CFG->status_to_exclude_from_sales_reports_string . "
        UNION 
        select o.id as id, o.id as order_id, date_format(date, '%m/%d/%y') as transaction_date, date, " . str_replace('orders.', 'o.', Orders::order_total_syntax()) . " as amount, 'debit' as line_type, 
        date_format(date_add(date, interval c.fill_days day), '%m/%d/%y') as due_date, 
        datediff(now(), date_add(date, interval c.fill_days day)) as days_late, concat(shipping_first_name, ' ', shipping_last_name) as shipping_name 
        FROM orders o join customers c on o.customer_id = c.id 
        WHERE customer_id = $cust_id 
            and DATE(date) BETWEEN '$from_date' AND '$to_date'
        AND o.current_status_id NOT IN " .$CFG->status_to_exclude_from_sales_reports_string . "
        order by date";
        return db_query_array($sql);
    }

    static function getCustomerBalance($cust_id, $from_date = '2015-01-08', $to_date = ''){
        if(empty($to_date)){
            $to_date = date('Y-m-d');
        }
        $sql = "SELECT @total_orders := SUM(" . Orders::order_total_syntax() . ") as total_orders,
            @total_payments := SUM(order_total_charges(orders.id)) as total_payments, 
            IFNULL(ROUND(@total_orders - @total_payments, 2), 0.00) as balance 
            FROM  orders JOIN customers ON orders.customer_id = customers.id
            WHERE orders.date BETWEEN '$from_date' AND '$to_date'";
        $arr = db_query_array($sql);
        $balance = $arr[0]['balance'];
        return $balance;           
    }
}
?>
