<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   Feb 15 2015
*
*****************/

class ProductAvailableOptions {
    
    var $id = null;
    var $option_value_id;
    var $option_value;
    var $option_id;
    var $option_name;
    var $product_id;
    var $product_name;
    var $optional;
    var $additional_price;
    var $essensa_additional_price;
    var $parent_id;
    var $product_available_option_groups_id;
    var $required_if_visible;
    var $is_default;
    private static $select = 'SELECT p.name as product_name,
            p.id as product_id, option_value(ov.options_id, ov.value) as value, 
            ov.id as value_id, o.name as option_name, pa.is_default,
            o.id as option_id, additional_price, pal.optional,
            pa.id as pa_id, pa.essensa_additional_price, pa.additional_cost, 
            pa.product_available_option_groups_id as group_id,

             pag.title as group_name, pag.hide_children, pag.hide_until_required_set as disable_group,
             pal.label as option_label, presets.name as preset_name, presets.id as preset_id 
            FROM product_available_options pa 
            JOIN products p ON pa.product_id = p.id 
            LEFT JOIN option_values ov ON pa.options_values_id = ov.id
            LEFT JOIN preset_options presets ON presets.id = pa.preset_id
            LEFT JOIN options o ON ov.options_id = o.id
            LEFT JOIN product_available_option_groups pag ON
                pa.product_available_option_groups_id = pag.id
            LEFT JOIN product_available_option_labels pal ON  pa.product_available_option_labels_id = pal.id ';
    function ProductAvailableOptions($id=null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    /*********************
    *
    *
    * DB Functions
    *
    *
    ********************/


    function populate($id){
        $this->id = $id;
        $sql = self::$select . ' WHERE pa.id = ' . $id;
        $info = db_query_array($sql);
        if(!is_array($info)){
            return;
        }
        $info = $info[0];
        $this->option_value_id = $info['value_id'];
        $this->option_value = $info['value'];
        $this->product_name = $info['product_name'];
        $this->product_id = $info['product_id'];
        $this->option_name = $info['option_name'];
        $this->option_id = $info['option_id'];
        $this->optional = $info['optional'];
        $this->is_default = $info['is_default'];
        $this->additional_price = $info['additional_price'];
        $this->essensa_additional_price = $info['essensa_additional_price'];
    }

    static function updateOrInsert($info, $id=null){
        if(!is_null($id) && !empty($id) && $id){
            //need to update null value separately because parent id is a foreign key and for some reason db_update sets null to empty string instead of null and causes foreign key constraint failure.
            $foreign_keys = array('parent_id', 'product_available_option_groups_id');
            foreach($foreign_keys as $key){
                if(array_key_exists($key, $info) && is_null($info[$key])){
                    db_query("UPDATE product_available_options SET $key = NULL WHERE id = " . (int)$id);
                    unset($info[$key]);
                }
            }

            $res = db_update('product_available_options',$id, $info);
        }
        else{
            $res = db_insert('product_available_options', $info);
        }
        return $res;
    }
    
    function delete(){
        //first delete all product_options with this option
        $sql = "DELETE FROM product_options WHERE product_id = "
            . $this->product_id . " AND  product_option_values_set 
            LIKE '%," . $this->id . ",%'";
        $res = db_query($sql);
        if(!$res){
            return false;
        }
        return db_delete('product_available_options', $this->id);
    }
    
    static function updateOptionGroup($option_id, $group_id){
        if(!is_null($group_id)){
            $group_id = (int)$group_id;
        }
        $sql = "UPDATE product_available_options SET product_available_option_groups_id = " . $group_id . " WHERE id = " . (int) $option_id;
        return db_query($sql);
    }

    static function getOptionsForProduct($product_id, $is_optional = null){
        if(empty($product_id)){
            return;
        }
       $sql =  'SELECT 
            ifnull(option_value(presets.option_id, ov2.value), option_value(ov.options_id, ov.value)) as value, 
            product_available_option_labels_id as label_id, paol.label as option_label,
            ifnull(preset_vals.is_default, pa.is_default) as is_default,
            ifnull(ov.id, ov2.id) as value_id, trim(ifnull(o.name, o2.name)) as option_name, ifnull(o.id, o2.id) as option_id,
            p.price as product_price, p.essensa_price as product_essensa_price,
            ifnull(preset_vals.additional_price, pa.additional_price) as additional_price,
             ifnull(preset_vals.essensa_additional_price, pa.essensa_additional_price) as essensa_additional_price, additional_cost, 
            paol.optional, paol.is_preview, pa.id as pa_id, pa.preset_id as preset_id,
            if(isnull(presets.option_id), if(ov.options_id = 1, ov.value , 0), if(presets.option_id = 1, ov2.value, 0)) as color_number,
            paol.parent_id, ifnull(o.input_type, o2.input_type) as input_type, ifnull( if(o.name = \'Color\', \'color\', o.input_type), if(o2.name = \'Color\', \'color\', o2.input_type)) as edit_input_type, 
            product_available_option_groups_id as group_id, pag.title as group_name, hide_children, hide_until_required_set as disable_group,
            paol.required_if_visible, related_product, related_product_option, related_products.name as related_product_name, paol.error_label as error_label,
            pa.preset_id, presets.name as preset_name, preset_vals.id as preset_value_id,
            if(isnull(o.name), if(o2.formula = \'font\', 1, 0), if(o.formula = \'font\', 1, 0)) as is_font,
			if(isnull(ov.id), ov2.image_file, ov.image_file) as image_file
            FROM    
            product_available_options pa 
            JOIN products p ON pa.product_id = p.id
            LEFT JOIN option_values ov ON pa.options_values_id = ov.id
            LEFT JOIN preset_options presets ON pa.preset_id = presets.id
            LEFT JOIN preset_option_values preset_vals ON preset_vals.preset_id = presets.id
            LEFT JOIN options o ON ov.options_id = o.id
            LEFT JOIN options o2 ON presets.option_id = o2.id
            LEFT JOIN option_values ov2 ON preset_vals.options_values_id = ov2.id
            LEFT JOIN product_available_option_labels paol ON pa.product_available_option_labels_id = paol.id
            LEFT JOIN product_available_option_groups pag ON
            pa.product_available_option_groups_id = pag.id
            LEFT JOIN products related_products on pa.related_product = related_products.id
            WHERE pa.product_id = ' . $product_id;
        if(!is_null($is_optional)){
            $sql .= " AND paol.optional = " . $is_optional;
        }
        $sql .= " ORDER BY pa.display_order,  preset_vals.display_order, paol.optional, o.name, ov.order, option_name, value ";
 //     echo 'the sql: ' . $sql;
        return db_query_array($sql);

    }

    //When using presets, there are many less rows in product avaialble options
    //than values. When using the edit form it causes too many rows to show,
    //making it difficult to navigate the page.
    //When using presets, there are many less rows in product avaialble options
    //than values. When using the edit form it causes too many rows to show,
    //making it difficult to navigate the page.
    static function getNumRowsForProduct($product_id){
        $num_rows_arr = db_get1(db_query_array('SELECT COUNT(1) AS num FROM product_available_options where product_id = ' . (int)$product_id ));
        return $num_rows_arr['num'];
    }

    static function getValuesForOption($option_id){
        $options = ProductOptionValues::getForOption($option_id);
        $presets = PresetOptions::getForOptionIdForDropDown($option_id);
        return array('values' => $options, 'presets' => $presets);
    }

    static function getSelectForOption($option_id){
        $options_and_presets = self::getValuesForOption($option_id);
        $str = '<option value=\'\'>-Select-</option>';
        foreach($options_and_presets['values'] as $id => $values){
            $str .= "<option value='{$values['id']}'>{$values['name']}</option>";
        }
        if(sizeof($options_and_presets['presets'])){
            $str .= '<optgroup label=\'Preset\'>';
            foreach($options_and_presets['presets'] as $id => $values){
                $str .= "<option value='{$values['id']}'>${values['name']}</option>";
            }
            $str .= '</optgroup>';
        }
        return $str;
    }




    static function getForOptionForm($product_id){
        return self::getOptionsForProduct($product_id, 0);
    }

    static function getAdditionalCost($option_ids){
        if(empty($option_ids) || $option_ids == ','){
            return 0.00;
        }
        $sql = "SELECT SUM(additional_cost) as cost FROM product_available_options WHERE
        id IN ($option_ids)";
        $cost = db_get1(db_query_array($sql));
        return $cost['cost'];
    }
	
	 static function getAdditionalPricebyOptionSet($option_ids){
        if(empty($option_ids)){
            return 0.00;
        }
        $sql = "SELECT SUM(additional_price) as add_price FROM product_available_options WHERE
        id IN ($option_ids)";
        $add_price = db_get1(db_query_array($sql));
        return $add_price['add_price'];
    }
   /************************
   *
   *
   *    Getters and Setters
   *
   *
   ************************/

    function setProductId($product_id){
        $this->product_id = $product_id;
    }

    function getProductId(){
        return $this->product_id;
    }

   function getOptionId(){
        return $this->option_id;
   }

   function setOptionId($option_id){
       $this->option_id = $option_id;
   }

   function getOptionValueId(){
        return $this->option_value_id;
   }

   function setOptionValueId($ov_id){
        $this->option_value_id = $ov_id;
   }

    function getOptional(){
        return $this->optional;
    }

    function setOptional($optional){
        $this->optional =(int)$optional;
    }

    function isOptional(){
        return $this->optional > 0;    
    }

    function getAdditionalPrice(){
        return $this->additional_price ;
    }
    
    function setAdditionalPrice($additional_price){
        $this->additional_price = $additional_price;
    }

    function getEssensaAdditionalPrice(){
        return $this->essensa_additional_price ;
    }
    
    function setEssensaAdditionalPrice($essensa_additional_price){
        $this->essensa_additional_price = $essensa_additional_price;
    }

    static function getAllForProduct($product_id){
       $sql = self::$select . ' WHERE p.id = ' . $product_id;
       return db_query_array($sql);
    }


    /************************
    *
    * Display Functions
    *
    ***********************/

    static function getForProductPage($product_id){
        $info = self::getOptionsForProduct($product_id);
        $options = array();
        $current_option = '';
        $fonts = array();
        $this_option = array();
        foreach($info as $curr){
            //set all set options in cache - if this isn't set
            //then don't show it unless it's optional
            if(!in_array($curr['pa_id'], ProductOptionCache::$options_set[$product_id]) && !$curr['optional']){
//mail('hgolov@gmail.com', 'Skipping option', var_export($curr, 1));
                continue;
            }
            if(!$curr['optional'] && (!empty($curr['preset_value_id']) && !in_array($curr['preset_value_id'], ProductOptionCache::$required_preset_ids))){
//mail('hgolov@gmail.com', 'Skipping option', var_export($curr, 1));
                continue;
            }
            if($curr['option_label'] != $current_option 
                && sizeof($this_option) > 0){
                $options[$current_option] = $this_option;
                $this_option = array();
            }
            if($curr['option_label'] != $current_option){
               $current_option = $curr['option_label'];
            }
            if($curr['is_font'] == '1'){
                $fonts[] = $curr['value_id'];
            }
            $this_option[] = array(
                'option_label_id' =>  $curr['label_id'],
                'option_id' => $curr['option_id'],
                'option_name' => $curr['option_label'],
                'id' => $curr['pa_id'],
                'label' => $curr['value'],
                'optional' => $curr['optional'],
                'additional_price' => $curr['additional_price'],
                'essensa_additional_price' => $curr['essensa_additional_price'],
                'color_id' => $curr['color_number'],
                'input_type' => $curr['input_type'],
                'new_version_input_type' => $curr['new_version_input_type'],
                'parent_id' => $curr['parent_id'],
                'group_id' => $curr['group_id'],
                'group_name' => $curr['group_name'],
                'hide_group' => $curr['hide_children'],
                'disable_group' => $curr['disable_group'],
                'required_if_visible' => $curr['required_if_visible'],
                'product_essensa_price' => $curr['product_essensa_price'],
                'product_price' => $curr['product_price'],
                'preset_id' => $curr['preset_id'],
                'preset_value_id' => $curr['preset_value_id'],
                'is_preview' => $curr['is_preview'],
                'is_font' => $curr['is_font'],
                'value_id' => $curr['value_id'],
                'error_label' => $curr['error_label'],
                'is_default' => $curr['is_default'],
				'image_file' => $curr['image_file'],
                'price_text' => $curr['price_text'],
                );
        }
        if(sizeof($this_option) > 0){
            $options[$current_option] = $this_option;
        }
        if(sizeof($fonts) > 0){
            ProductOptionCache::setFontFamilies(join(',',$fonts));
        }
        return $options;
    }

    static function getDropdownsForEditForm($product_id, $product_selected_options = '', $product_presets = ''){
        $options = self::getForOptionForm($product_id);
        $html = '<input type="hidden" name="info[product_option_values_set]" id="option_values_set" value="' . $product_selected_options . '"/>
        <input type="hidden" name="info[preset_value_ids]" id="preset_value_ids" value=\'' . $product_presets . '\'/>';
        $this_option = '';
        $select_options = '<option value="">-Select-</option>';
        $all_options = array();
        $id_to_name = 'var option_id_to_names = {};';
  foreach($options as $curr){
            //echo 'the option; ' .var_export($curr, 1);
            if(!empty($this_option) && $curr['option_label'] != $this_option){
            $html .= $curr['option_label'] . ": <select name='options[$label_id]' id='options_$label_id' rel='selected_options' data-option-name='$this_option'>$select_options</select>&nbsp;";
            $select_options = '<option value="">-Select-</option>';
            $this_option = $curr['option_label'];
            $id_to_name .= 'option_id_to_names["' . $curr['option_id'] . '"] = "' . $this_option . '";';
            $label_id = $curr['label_id'];
            }
            elseif(empty($this_option)){
                $this_option = $curr['option_label'];
                $label_id = $curr['label_id'];
            }
            $all_options[$curr['pa_id']] = $label_id;
            $select_options .= "<option value='{$curr['pa_id']}' data-preset-id='{$curr['preset_value_id']}'>{$curr['value']}</option>";
        }

        $html .= $this_option . ": <select name='options[$label_id]' id='options_$label_id' rel='selected_options' data-option-name='" . $this_option . "'>$select_options</select>&nbsp;";
        $html .= "<script type='text/javascript'>
        $id_to_name var selected_presets = {};
            \$j('select[rel=\"selected_options\"] ').change(function(){setSelectedOptions();});
          ";
         if(!empty($product_selected_options)){
            $selected_ids = explode(',', $product_selected_options);
            $presets = json_decode($product_presets, true);
            foreach($selected_ids as $curr){
                if(!empty($curr)){
 //               $html .= "\$j('#options_" . $all_options[$curr] .
   //                 "').val($curr);";
                  $html .= "\$j('#options_" . $all_options[$curr] .
                  " option[value=\"$curr\"][data-preset-id=\""
                  . $presets[$curr] . "\"]').prop('selected', 'selected');";
                }
            }
        }
        $html .=  "</script>";
        return $html;

    }

    static function updateOrder($product_id, $order_info){
        //here we need to update the sort order for 
        //row with product id and opton_value_id
        //the order info comes with a list of option
        //value ids in correct order
        $sql = 'INSERT INTO product_available_options(id, display_order) VALUES ';
        foreach($order_info as $key => $val){
            $sql .= "({$val[0]}, $key), ";
        }
        $sql = rtrim($sql, ', ') . ' ON DUPLICATE KEY UPDATE '
            . ' display_order = VALUES(display_order)';
        $res = db_query($sql);
        echo json_encode(array('res' => var_export($res, 1)));
    }

    static function updateNewParentIds($temporary_labels){
       $labels = explode("~~", $temporary_labels);
       if(!sizeof($labels)){
            return;
       }
       $labels_to_vals = array();
       foreach($labels as $info){
            $this_info = explode('!!', $info);
            $labels_to_vals[$this_info[1]] = $this_info[0];
       }
       $in_clause = "'" . implode("','", array_keys($labels_to_vals )) . "'";
       $sql = "SELECT  concat(product_available_option_labels_id, ':', if(options_values_id = 0,'', options_values_id), ':', if(preset_id = 0, '', preset_id)) as combo, id 
       FROM product_available_options WHERE 
     concat(product_available_option_labels_id, ':', if(options_values_id = 0,'', options_values_id), ':', if(preset_id = 0, '', preset_id)) 
       IN ($in_clause)";
       $parents = db_query_array($sql);
       if(!sizeof($parents)){
            die('the sql: ' . $sql . '<br/>brought no results.');
            return;
       }
       $update_sql = "INSERT INTO product_available_option_labels(id, parent_id) VALUES ";
       foreach($parents as $curr){
            $update_sql .= "(" . $labels_to_vals[$curr['combo']] . ', ' . $curr['id'] . '), ';
       }
       $update_sql = substr($update_sql, 0, -2) . ' ON DUPLICATE KEY UPDATE '
        . ' parent_id = VALUES(parent_id) ';
        $res = db_query($update_sql);
    }

    function copyOptions($source_id, $target_ids, $copy_type = null, $sku_label = null, $sku_value = null, $sku_value_field = null){
        if($sku_value_field){
            $sku_value_source_field = $sku_value_field == 'preset_value_ids'? 'preset_id' : 'options_values_id';
        }
        $option_count = db_query_array("SELECT count(1) as num_options  FROM product_available_options WHERE product_id = $source_id");
        $num_options = $option_count[0];
        if($num_options['num_options'] == 0){
            return 'No options to copy';
        }
        
        $target_product_ids = explode(',', trim(str_replace(' ', '',$target_ids), ','));
        $message = '';
        foreach($target_product_ids as $target){
            $target_options = db_query_array("SELECT count(1) as num_options FROM product_available_options WHERE product_id = $target");
            $num_options = $target_options[0];
            if($num_options['num_options'] > 0){
                $message .= 'This product (' . $target . ') already has options.<br/>';
                continue;
            }
            //duplicate product available options
            $insert_available_options = "INSERT INTO product_available_options (product_id, options_values_id, additional_price, optional, essensa_additional_price, preset_id, display_order, is_default) 
                SELECT $target, options_values_id, pa.additional_price, pa.optional, pa.essensa_additional_price, preset_id, pa.display_order, is_default
                FROM product_available_options pa JOIN product_available_option_labels pl 
                ON pa.product_available_option_labels_id = pl.id 
                WHERE pa.product_id = $source_id";
            $res = db_query($insert_available_options);
            if(!$res){
                return( 'Error inserting product options.' . mysql_error());       
            }
            //get a map of old option to new option, and parent_ids
            $get_available_options = "SELECT pa1.id as old_id, pa1.parent_id as old_parent_id, pa2.id as new_id,
                pg.id as group_id,  pg.parent_id as group_parent, pl.id as label_id, pl.parent_id as label_parent,
                pa2.options_values_id, pa2.preset_id
                FROM product_available_options pa1 JOIN product_available_options pa2 ON 
                (pa1.options_values_id = pa2.options_values_id and pa1.preset_id = pa2.preset_id and pa1.display_order = pa2.display_order)
                LEFT JOIN product_available_option_groups pg ON pa1.product_available_option_groups_id = pg.id 
                LEFT JOIN product_available_option_labels pl on pa1.product_available_option_labels_id = pl.id
                WHERE pa1.product_id = $source_id AND pa2.product_id = $target"; 
            $available_options = db_query_array($get_available_options);
            $available_options_map = array();
            $ids_to_parents = array();
            $groups = array();
            $group_parents = array();
            $labels = array();
            $label_parents = array();
            $avail_opt_groups_insert = "INSERT INTO product_available_options (id, product_available_option_groups_id) VALUES ";
            $avail_opt_labels_insert = "INSERT INTO product_available_options (id, product_available_option_labels_id) VALUES ";
            $sku_labels = array();
            $sku_values = array();
            foreach($available_options as $curr){
                $available_options_map[$curr['old_id']] = $curr['new_id'];
                //check if we receive null or 'null'
                if(!is_null($curr['parent_id'])){
                    $ids_to_parents[$curr['old_id']] = $curr['parent_id'];
                }
                //if there is a group id, create a new group if it hasn't been created and write the sql to update the new row
                //with the new group id
                if(!is_null($curr['group_id'])){
                    if( !isset($groups[$curr['group_id']])){
                        $sql = "INSERT INTO product_available_option_groups (product_id, title, description, hide_until_required_set)
                            SELECT $target, title, description, hide_until_required_set FROM product_available_option_groups WHERE
                           id = {$curr['group_id']}";
                        $ret =db_query($sql);
                        if(!$ret){
                            die('Error inserting option groups');
                        }
                        $groups[$curr['group_id']] = db_insert_id();
                    }
                    if(!is_null($curr['group_parent'])){
                        $group_parents[$curr['group_id']] = $curr['group_parent'];
                    }
                    $avail_opt_groups_insert .= "(" . $curr['new_id'] . ", " . $groups[$curr['group_id']] . "), ";
                }
                //create a new label if it hasn't been created and write the sql to update the new row
                //with the new label id
                if(!is_null($curr['label_id'])){
                    if(!isset($labels[$curr['label_id']])){
                        $sql = "INSERT INTO product_available_option_labels (product_id, option_id, label, message_above, message_below, optional, 
                        required_if_visible, is_preview) 
                        SELECT $target, option_id, label, message_above, message_below, optional, required_if_visible, is_preview 
                        FROM  product_available_option_labels WHERE id = {$curr['label_id']}";
                        $ret = db_query($sql);
                        if(!$ret){
                            return 'Error creating option labels';
                        }
                        $labels[$curr['label_id']] = db_insert_id();
                        if($curr['label_id'] == $sku_label && $curr[$sku_value_source_field] == $sku_value){
                            $sku_values[$target] = $curr['new_id'];
                        }
                    }
                    if(!is_null($curr['label_parent']) && !empty($curr['label_parent']) && $curr['label_parent'] > 0){
                        $label_parents[$curr['label_id']] = $curr['label_parent'];
                    }
                    $avail_opt_labels_insert .= "(" . $curr['new_id'] . ", " . $labels[$curr['label_id']] . "), ";
                }
            }

            //if there were groups, update them now
            if(sizeof($groups)){
                $avail_opt_groups_insert = substr($avail_opt_groups_insert, 0, -2) . ' ON DUPLICATE KEY UPDATE product_available_option_groups_id = VALUES(product_available_option_groups_id)';
                $ret = db_query($avail_opt_groups_insert);
                if(!$ret){
                    return 'Error update option groups';
                }
                //if any of the groups had parents, update those
                if(sizeof($group_parents)){
                    $sql = "INSERT INTO product_available_option_groups (id, parent_id) VALUES ";
                    foreach($group_parents as $old_child => $old_parent){
                       $sql .= "(" . $groups[$old_child] . ", " . $groups[$old_parent] . '), ';
                    }
                    $sql = substr($sql, 0, -2) . " ON DUPLICATE KEY UPDATE parent_id = VALUES(parent_id)";
                    $ret = db_query($sql);
                    if(!$ret){
                        return 'Error creating option groups';
                    }
                }
            }
            if(sizeof($labels)){
                $avail_opt_labels_insert = substr($avail_opt_labels_insert, 0, -2) . ' ON DUPLICATE KEY UPDATE product_available_option_labels_id = VALUES(product_available_option_labels_id)';
                $ret = db_query($avail_opt_labels_insert);
                if(!$ret){
                    return 'Error update option labels';
                }
                if(sizeof($label_parents)){
                    $sql = "INSERT INTO product_available_option_labels (id, parent_id) VALUES ";
                    foreach($label_parents as $old_child => $old_parent){
                        if(!empty($labels[$old_child]) && !empty($available_options_map[$old_parent])){ 
                       $sql .= "(" . $labels[$old_child] . ", " . $available_options_map[$old_parent] . '), ';
                    }
                    }
                    $sql = substr($sql, 0, -2) . " ON DUPLICATE KEY UPDATE parent_id = VALUES(parent_id)";
                    $ret = db_query($sql);
                    if(!$ret){
                        return 'Error matching labels to parents';
                    }

                }
            }
            //generate new combinations for product options
            Products::generateProductOptionsForAvailableOptions($target);
            if($copy_type == 'flash_furniture' && $sku_label && $sku_value){
                $search_for =','. $sku_values[$target] . ',';
                $field = $sku_value_field ;
                $sql = "UPDATE product_options 
                SET vendor_sku = concat(left(vendor_sku, locate(substring_index(vendor_sku, '-', -1), vendor_sku)-1),'EMB-',substring_index(vendor_sku, '-', -1))
                WHERE product_id = $target AND $field LIKE '%$search_for%' ";
                $updated = db_query($sql);
            }
            $message .= "$target copied successfuly<br/>";
        }//end foreach
        return $message;
    }

}

?>
