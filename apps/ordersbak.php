<?php

class Orders {

	static function insert($info)
	{
		$info['ip_address']	= $_SERVER['REMOTE_ADDR'];

		if (isset($_COOKIE)) {
			foreach($_COOKIE as $key => $val)
				$info['cookie_info'] .= "$key=$val; ";
		}

		// Fix State
		if(isset($info['billing_state'])){
			$info['billing_state'] = Util::fixState($info['billing_state']);
		}
		if(isset($info['shipping_state'])){
			$info['shipping_state'] = Util::fixState($info['shipping_state']);
		}


		$info['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$info['session_id'] = session_id();

		if( isset( $info['date'] ) )
			$order_id = db_insert('orders',$info );
		else
			$order_id = db_insert('orders',$info,'date');

		Tracking::trackInsert( 'order', $order_id );

		return $order_id;
	}

	static function update($id,$info,$date='',$run_tracking=true)
	{
	    if ($run_tracking) {
            $tracking_id = Tracking::trackUpdate( 0, 'order', $id, 'update' );
    
            // Fix State
            if(isset($info['billing_state'])){
                $info['billing_state'] = Util::fixState($info['billing_state']);
            }
            if(isset($info['shipping_state'])){
                $info['shipping_state'] = Util::fixState($info['shipping_state']);
            }
		}

		$result = db_update('orders',$id,$info,'id',$date);

        if ($run_tracking) {
            $tracking = Tracking::trackUpdate( $tracking_id );
    
            $new_order = Orders::get1( $id );
    
            $customer = Customers::get1( $new_order['customer_id'] );
    
            self::updateCIM( $id, array('cim_profile_id'=>$customer['cim_profile_id']) );
        }
        
		return $result;
	}

	static function updateCIM( $id, $info )
	{
	    return db_update('orders',$id,$info );
	}

	/* NOTE: THIS WILL NOT PROPERLY DELETE OPTIONS.  NOT FIXING NOW. Y U NO FIX !? */
	static function delete($id)
	{

		db_delete('order_items',$id,'order_id');
		return db_delete('orders',$id);
	}

	static function deletePackage($id)
	{

		$id = (int) $id;

		$q = "SELECT * FROM order_items
				  WHERE order_package_id = $id";

    	$recs = db_query_array($q);

    	// yes, I know this is lousy code...

    	if (is_array($recs))
    	  foreach ($recs as $rec)
    	  {
    	    Orders::deleteItem($rec['id']);
    	  }
		return true;
	}

	static function getOrdersForQuickbooks()
    {
    	global $CFG;
        $sql = "SELECT orders.id AS order_id,
                    store.quickbooks_name,
                    store.quickbooks_type
                FROM orders
                LEFT JOIN store ON store.id = orders.order_tax_option
                WHERE orders.quickbooks_id = '' 
                AND orders.order_tax_option = $CFG->website_store_id AND orders.payment_type <> 'cba'";
        
        return db_query_array($sql);
    }
    
	static function get( $id=0,$customer_id=0,$order='',$order_asc='',$order_status_id=0,$start_date='',$end_date='',
		$name_begins_with = '', $phone = '', $email = '', $list_mgr_id = 0, $is_corporate = '', $sales_id=0,
		$first_name = '', $last_name='', $city='', $state='', $zip='', $store_numb=0, $po_numb='', $release_date='',
		$supplier=0, $tracking_numb='', $payment_method='', $shipped_balance='', $avs='', $last4='', $brand_id=0, $model_numb='',
		$damage='', $delivery_issue='', $past_damage='', $coming_back='', $file_claim_ups='', $file_claim_frt='', $rewview='', $balance='', $shipper=0,
		$has_balance = 0, $sales_account_id=0, $reference_number="", $third_party_order='', $ebay_id='', $trucker_id='',
		$get_total=false, $limit=0, $start=0,
		$amazon_order_id = ''
		)
	{
		$where ="";
	//I need to calc the remaining balance into this sql
		if( $get_total ){
			$sql = " SELECT COUNT(DISTINCT orders.id) as total FROM orders ";
		}
		else{
$sql = "SELECT orders.*, orders.transaction_id as order_transaction_id,

				IF(
					   (orders.subtotal + orders.min_charges - orders.promo_discount - orders.shipping_discount
						+ orders.shipping +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount + orders.min_charges + orders.shipping - orders.shipping_discount) ) )
					   >= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - orders.shipping_discount
						+ orders.shipping +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount + orders.min_charges + orders.shipping - orders.shipping_discount))),2) , 0
				   )

				AS order_total,
						round((0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount + orders.min_charges + orders.shipping - orders.shipping_discount)),2) AS order_tax,
					   customers.first_name AS customer_first_name,
					   customers.last_name AS customer_last_name,
					   IF( customers.email IS NOT NULL, customers.email, orders.email ) AS customer_email,
					   customers.phone AS customer_phone,
					   order_statuses.name AS order_status_name,

					   order_charges.transaction_id,
		 				 store.name as store_name,
		 				 accounts.name as account_name,
		 				 CONCAT( admin_users.first_name, ' ', admin_users.last_name ) as sales_name,
		 				 shipping_methods.descr AS shipping_method
				FROM orders ";
								}

		//If they wanted a model number or a brand, I need to join the order items table and make sure it has something from that
		//brand/model
			$sql .= " LEFT JOIN order_items ON order_items.order_id = orders.id ";
			$sql .= " LEFT JOIN products ON products.id = order_items.product_id ";
			$sql .= " LEFT JOIN store on store.id = orders.order_tax_option ";
			$sql .= " LEFT JOIN admin_users on admin_users.id = sales_id ";
			$sql .= " LEFT JOIN accounts on accounts.id = orders.sales_account_id ";
			$sql .= " LEFT JOIN shipping_methods ON shipping_methods.id = orders.shipping_id ";

		if( $supplier || $shipper  || ( $po_numb && $tracking_numb ) )
		{
			$sql .= " LEFT JOIN purchase_order_items poi on poi.item_id = order_items.id
					  LEFT JOIN purchase_order ON purchase_order.id = poi.po_id
					  LEFT JOIN product_suppliers ON products.id = product_suppliers.product_id ";
			$sql .= " LEFT JOIN order_tracking ON order_tracking.order_id = orders.id ";
		}
		elseif( $po_numb || $release_date )
		{
			$sql .= " LEFT JOIN order_po ON order_po.order_id = orders.id ";
		}
		elseif ( $tracking_numb )
		{
			$sql .= " LEFT JOIN order_tracking ON order_tracking.order_id = orders.id ";
		}
		if( $payment_method )
		{
			$sql .= " LEFT JOIN order_payments ON order_payments.order_id = orders.id ";
		}
		if( $brand_id )
		{
			$where .= " AND products.brand_id = $brand_id ";
		}
		if( $model_numb )
		{
			$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id ";
			$where .= " AND products.vendor_sku IS NOT NULL AND ( products.vendor_sku like '%$model_numb%' OR product_options.vendor_sku like '%$model_numb%' ) ";
		}

		$sql .= "	LEFT JOIN customers ON customers.id = orders.customer_id
				LEFT JOIN order_statuses ON order_statuses.id = orders.current_status_id
				LEFT JOIN order_charges ON (order_charges.order_id = orders.id AND order_charges.action = 'authorize')
				LEFT JOIN order_charges as oc2 ON (oc2.order_id = orders.id AND oc2.action = 'capture')
				WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND orders.id = $id ";
		}
		if ( $third_party_order )
		{
			$sql .= " AND orders.3rd_party_order_number = $third_party_order ";
		}
		if ($customer_id == -1){
			$customer_id = 0;
			$sql .= " AND orders.customer_id = $customer_id ";
		}elseif ($customer_id > 0) {
			$sql .= " AND orders.customer_id = $customer_id ";
		}

		if (is_array($order_status_id) && count($order_status_id) > 0)
		{
		    $status_ids = implode(',', $order_status_id);
		    $sql .= " AND orders.current_status_id IN (" . $status_ids . ")";
		}

		if (!is_array($order_status_id) && $order_status_id > 0) {
			$sql .= " AND orders.current_status_id = $order_status_id ";
		}
		/*
		if ($start_date != '' && $end_date != '') {
			$sql .= " AND orders.date BETWEEN '$start_date' AND '$end_date' ";
		}*/

		// i doubt this will cause any problems, and it does more..
		// - Added the 00:00:00 and 23:59:59 to get anything on the day if start and end are the same
		if( $start_date != '' || $end_date != '' )
		{
			if( $start_date != '' )
				$start_date .= ' 00:00:00';
			if( $end_date != '' )
				$end_date .= ' 23:59:59';
			$sql .= db_queryrange('orders.date', $start_date, $end_date);
		}

		if ($name_begins_with) {
			$name_begins_with = addslashes($name_begins_with);
			$sql .= " AND ( customers.last_name LIKE '$name_begins_with%' ";
			$sql .= " OR orders.billing_last_name LIKE '$name_begins_with%' ";
			$sql .= " OR orders.shipping_last_name LIKE '$name_begins_with%' ";
			$sql .= " OR orders.billing_first_name LIKE '%$name_begins_with%' ";
			$sql .= " OR orders.shipping_first_name LIKE '%$name_begins_with%' ) ";
		}
		if ($phone) {
			$sql .= db_restrict('customers.phone', $phone);
		}
		if ($email) {
			$sql .= " AND ( customers.email = '" . addslashes( $email ) . "' OR orders.email = '" . addslashes( $email ) . "' )";
		}
		if ($list_mgr_id) {
			$sql .= db_restrict('orders.list_mgr_id', $list_mgr_id);
		}
		if ($is_corporate) {
			$sql .= db_restrict('orders.is_corporate', $is_corporate);
		}
		if( $sales_id )
		{
			$sql .= " AND sales_id = $sales_id ";
		}

//Need to add all the extra filters for the view orders screen
		if( $first_name )
			$sql .= " AND ( orders.billing_first_name = '$first_name' OR orders.shipping_first_name = '$first_name' ) ";
		if( $last_name )
			$sql .= " AND ( orders.billing_last_name = '$last_name' OR orders.shipping_last_name = '$last_name' ) ";
		if( $city )
			$sql .= " AND ( orders.billing_city = '$city' OR orders.shipping_city = '$city' ) ";
		if( $state )
			$sql .= " AND ( orders.billing_state = '$state' OR orders.shipping_state = '$state' ) ";
		if( $zip )
			$sql .= " AND ( orders.billing_zip = '$zip' OR orders.shipping_zip = '$zip' ) ";
		if( $payment_method)
			$sql .= " AND order_payments.payment_method = '$payment_method' ";
		if( $last4 )
			$sql .= " AND orders.last4 = '$last4' ";
		if( $avs == 'Y' )
			$sql .= " AND orders.avs_address = 'Y' AND orders.avs_zip = 'Y' ";
		elseif( $avs == 'N' )
			$sql .= " AND orders.avs_address <> 'Y' AND orders.avs_zip <> 'Y' ";
		if( $store_numb )
		{
			if( $store_numb == 999 )
			{
				$store_numb = '1, 3';
			}

			if( is_int( $store_numb ) )
				$sql .= " AND store.id = $store_numb ";
			else
			{
				$sql .= " AND store.id in ( $store_numb )";
			}
		}
		if( $release_date )
			$sql .= " AND DATE( order_po.release_date ) = DATE( '$release_date' ) ";

		if( $damage )
		{
			$sql .= " AND orders.damage = '$damage' ";
			$order = "damage_date";
		}
		if( $delivery_issue )
			$sql .= " AND orders.delivery_issue = '$delivery_issue' ";
		if( $past_damage )
			$sql .= " AND orders.past_damage = '$past_damage' ";
		if( $coming_back )
			$sql .= " AND orders.coming_back = '$coming_back' ";
		if( $file_claim_ups )
			$sql .= " AND orders.file_claim_ups = '$file_claim_ups' ";
		if( $file_claim_frt )
			$sql .= " AND orders.file_claim_frt = '$file_claim_frt' ";
		if( $rewview )
			$sql .= " AND orders.review = '$rewview' ";

		if( $supplier )
		{
			$sql .= " AND (purchase_order.supplier_id = $supplier OR product_suppliers.brand_id = $supplier) ";
		}
		if( $shipper )
		{
			$sql .= " AND order_tracking.shipper_id = $shipper ";
		}
		if( $po_numb )
		{
			$sql .= " AND order_po.po_number = '$po_numb' ";
		}
		if( $tracking_numb )
		{
			$sql .= " AND order_tracking.tracking_number = '$tracking_numb' ";
		}
		if( $sales_account_id )
		{
			$sql .= " AND orders.sales_account_id = $sales_account_id ";
		}
		if( $reference_number )
		{
			$sql .= " AND orders.reference_number = $reference_number ";
		}

		if( $ebay_id )
		{
			$sql .= " AND orders.ebay_order_id = '$ebay_id' ";
		}
		if($amazon_order_id){
			$amazon_order_id = trim(db_esc($amazon_order_id));
			$sql .= " AND TRIM(orders.amazon_order_id) = '$amazon_order_id' ";
		}


		$sql = $sql . $where;

		if(!$get_total ){
			$sql .= " GROUP BY orders.id
					  ORDER BY ";

			if ($order == '') {
				$sql .= 'orders.date';
			}
			else {
				$sql .= addslashes($order);
			}
			//echo $sql;
			if ($order_asc == '' || !$order_asc) {
				$sql .= ' DESC ';
			}
		}

		if ($limit > 0) {
			$sql .= db_limit($limit,$start);
		}

		//echo $sql;

		//if balance is set, grab the sql result array and strip out all entries without a balance.
		if( $balance ) {
			$i = 0;
			if( $get_total ){
				$arr = db_query_array($sql);
				return $arr[0]['total'];
			}
			else{
				$arr = db_query_array($sql);
			}

			if(is_array($arr)) {
				foreach($arr as $order) {
					if (Orders::getOrderBalance( $order ) > 0) {
						$sql_results[$i] = $order;
						$i++;
					}
				}
				return $sql_results;
			}
			else {
				return $arr;
			}
		}
		else {
			if( $get_total ){
				$arr = db_query_array($sql);
				return $arr[0]['total'];
			}
			else{
				return db_query_array($sql);
			}
		}

	}

	static function get1($id)
	{
		$id = (int) $id;

		if (!$id) 
		{
			return false;
		}

		$result = Orders::get($id);
		return $result[0];
	}

	static function getOrderPackage($id)
	{
	  $id = (int) $id;
	  $q = "SELECT * from order_packages WHERE id = $id";
	  return db_query_array($q);
	}

	static function getOrderVendors($order_id, $order = '', $order_asc = '')
	{
		$q = " SELECT vendors.* FROM order_items LEFT JOIN products ON (products.id = order_items.product_id) LEFT JOIN vendors ON (vendors.id = products.vendor_id) WHERE 1 ";

		if ($order_id)
		  $q .= db_restrict('order_id', $order_id);

	  $q .= db_orderby($order, $order_asc);

		$q .= " GROUP BY vendors.id ";

		//echo $q;

		return db_query_array($q);
	}

	/**
	* @return unknown
	* @param unknown $id
	* @param unknown $order_id
	* @param unknown $order
	* @param unknown $order_asc
	* @param unknown $vendor_id
	* @param unknown $begin_date
	* @param unknown $end_date
	* @param unknown $list_mgr_entry_id
	* @desc Enter description here...
	*/
	static function getItems($id=0,$order_id=0,$order='',$order_asc='', $vendor_id = 0, $begin_date = '', $end_date = '',
										$list_mgr_entry_id = 0, $list_mgr_id = 0, $product_id = 0,$status_id=0, $tracking_id = 0,
						$po_id='',$charged='',$store_id='')
	{				
		$sql = "SELECT cats.id AS cat_id,
		               cats.name AS cat_name,
		               orders.*,
		               order_items.*,";
		if($po_id > 0){
			$po_id = (int)$po_id;
			$sql .= "poi.item_qty, (order_items.price * poi.item_qty) AS item_total,";
		}		               
		else $sql .= "poi.item_qty, (order_items.price * order_items.qty) AS item_total,";
		
		$sql .= " products.name AS product_name,
					   products.vendor_sku AS product_vendor_sku,
					   products.weight,
					   products.free_shipping,
					   products.ships_in,
   					   order_item_statuses.name AS order_item_status_name,
   					   brands.name as brand_name,
   					   products.description,
   					   products.local_warehouse,
   					   order_items_tracking.qty as tracking_qty,
   					   product_options.upc as product_upc,
   					   poi.po_id as po_id,
   					   IF(purchase_order.code <> '', purchase_order.code, poi.po_id ) AS po_number,
   					   poi.cust_each AS cust_each,
   					   inventory.qty AS in_stock,
					   order_items.order_item_id as amazon_order_item_id,
					   products.vendor_price, product_options.vendor_sku as product_options_vendor_sku,
					   order_items.price as order_items_price, product_options.id as product_options_id, products.case_discount,
					   product_options.value as value,
					   products.case_price_per_piece, products.num_pcs_per_case,
					   product_options.vendor_price as option_vendor_price

				FROM
		        order_items
		        LEFT JOIN order_item_options ON order_items.id = order_item_options.order_item_id
		        LEFT JOIN product_options    ON product_options.id = order_item_options.product_option_id
				LEFT JOIN products           ON products.id = order_items.product_id
		        LEFT JOIN product_cats       ON product_cats.product_id = products.id
		        LEFT JOIN cats               ON product_cats.cat_id = cats.id
            	LEFT JOIN orders ON (order_items.order_id = orders.id)
            	LEFT JOIN customers ON (customers.id = orders.customer_id)
		 		LEFT JOIN order_item_statuses ON (order_item_statuses.id = order_items.order_item_status_id)
		 		LEFT JOIN brands on brands.id = products.brand_id
		 		LEFT JOIN order_items_tracking on order_items_tracking.order_item_id = order_items.id
				LEFT JOIN purchase_order_items AS poi ON poi.item_id = order_items.id
				LEFT JOIN purchase_order     ON purchase_order.id = poi.po_id
				LEFT JOIN inventory          ON (inventory.product_id = products.id
					AND inventory.product_option_id = if (product_options.id, product_options.id, 0)";
				if ($store_id) $sql .= " AND store_id = '$store_id'";
				
				$sql .= ") WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND order_items.id = $id ";
		}
		if ($order_id > 0) {
			$sql .= " AND order_items.order_id = ".(int) $order_id." ";
		}

		if ($vendor_id) {
		  $sql .= db_restrict('vendor_id', $vendor_id);
		}

		if ($begin_date && $end_date) {
			$sql .= " AND date BETWEEN '$begin_date' AND '$end_date' ";
		}

		if ($status_id > 0) {
			$sql .= " AND order_items.order_item_status_id = $status_id ";
		}

		if ($product_id) {
			$sql .= db_restrict('order_items.product_id', $product_id);
		}
		if( $tracking_id )
			$sql .= " AND order_items_tracking.order_tracking_id = $tracking_id ";

		if($po_id > 0){
			$po_id = (int)$po_id;
			$sql .= " AND poi.po_id = '$po_id' ";
		}

		if($charged){
			$sql .= db_restrict("charged",$charged);
		}

		$sql .= " GROUP BY order_items.id, product_cats.product_id
				  ORDER BY ";

		if ($order == '') {
//			$sql .= 'order_items.id';
			$sql .= 'order_items.product_id, order_item_options.product_option_id';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

	//echo $sql;

		$arr = db_query_array($sql);


		if (is_array($arr))
		{
	       foreach ($arr as $key => $value)
	       {

	       	 // DO NOT ADD IN OPTION PRICING.  IT'S ALREADY ADDED!!!!

	       	 $q = "SELECT *
	       	       FROM
	       	       order_item_options
	       	       LEFT JOIN product_options
	       	         ON order_item_options.product_option_id = product_options.id
	       	       LEFT JOIN options
	       	         ON options.id = product_options.option_id
	       	       WHERE order_item_options.order_item_id = ".$value[id];

		       	 	if($value[id]){
		       	      $options = db_query_array($q);
		       	 	}

	       	 	  if (is_array($options)&&count($options))
	       	 	  {
				    $arr[$key]['options'] = $options;
				    $arr[$key]['orig_price'] = $cart[$key]['price'];
				    $arr[$key]['product_vendor_sku'] = $options[0]['vendor_sku'];

				    $arr[$key]['product_name'] .= " - " . $options[0]['value'];
				    /*

				    foreach ($options as $option)
				    {

				      if ($option['additional_price'] > 0)
				      {
				      	print_ar($option);
				        echo "adding: $option[additional_price] to arr[{$key}][total_price]}";

				      }
					  $arr[$key]['price'] += $option['additional_price'];
					  echo "price = {$arr[$key][price]}";
					  $arr[$key]['total_price'] += $option['additional_price'] * $arr[$key]['qty'];
				    }

				    */

				  }
	       }
	       return $arr;

		}
		else
		  return null;
	}


	static function getPackageLine($id)
	{
		$sql = "SELECT order_packages.*,
					packages.name AS package_name,
					packages.main_product_id
				FROM order_packages
				LEFT JOIN packages ON packages.id = order_packages.package_id
				WHERE order_packages.id = $id";

		return db_query_array($sql,'',true);
	}


	static function showPackageMainLine($package_line,$color_bg = false)
	{


		$package_info = Array(
			'id' => $package_line['package_id'],
			'name' => $package_line['package_name']
		);

?>
	  <tr style="background-color: #f8f8f8;">
	  	<td class=cart_rowl style="background-color: #f8f8f8;">
	  	<?= '<a style="font-weight: bold;" href="'.Catalog::makePackageLink($package_info).'">"'.$package_line['package_name'].'" Package</a>' ?>
	  	</td>
		<td class=cart_row>1</td>
	  	<td class=cart_row>$<?=number_format($package_line['price'],2)?></td>
	  	<td class=cart_rowr>
	  	$<?= number_format($package_line['line_total'], 2)?>
	  	</td>
	  </tr>
<?

	}


function showProductLine($cart_line,$color_bg = false, $javascript_off = false, $img = true, $remove=false, $sales=false, $prev_name = "" )
	{

		global $CFG;

		$baseurl = (isSSL()) ? $CFG->sslurl : $CFG->baseurl;

		$product_info = Products::get1($cart_line['product_id']);
		$url = Catalog::makeProductLink_($product_info, $product_info['cat_id'], $product_info['occasion_id']);
		if( $sales )
		{
			//print_ar( $cart_line );
			$url = 'sales.php?action=line_item&cur_line=' . $cart_line['id'];
			$target='orderframe';
		}
		else
			$target='';
		$options = '';
		$price = $cart_line['price'];
		if (is_array($cart_line['options'])) {

			foreach ($cart_line['options'] as $option) {
				$options .= "$option[name]: $option[value]<br>";

			}
			$cart_line['item_total'] = $price * $cart_line['qty'];
		}

		if( !$sales )
		{
	  ?>
	  <tr>
	  	<td class=cart_rowl>

	  		<a href='<?=$url?>' target="<?=$target?>">
	  		<?if( $img )
	  		{
	  			?><img src='<?= $baseurl ?>itempics/<?=$cart_line[product_id]?><?=$CFG->home_suffix?>' border=0 align="left" style="margin: 0px 10px;"><?
	  		}?>
	  		</a>
	  		<?  
	  		echo "prev_name is $prev_name and ".$cart_line['product_name'];
	  		if ($prev_name == "" || $prev_name != $cart_line['product_name'])
	  		{?>
	  		<a href='<?=$url?>' target="<?=$target?>" style='font-weight: bold;'><?=$cart_line[product_name]?></a>
	  		<?echo'<br>';?>
	  		<?= $product_info['vendor_sku'] ?>
	  		<?echo'<br>';?>
	  		<?= $options ?>
			<? } ?>
	  	</td>

	  	<td class=cart_row><?= $cart_line['qty'] ?></td>
	  	<td class=cart_row>$<?=number_format($price,2)?></td>
	  	<td class=cart_rowr>$<?= number_format($cart_line['item_total'], 2)?>
	  	</td>
	  </tr>
	  <?
		}
		else
		{
			?>
	  <tr>
	  	<td class=cart_rowl>
	  		<?=$product_info['vendor_sku']?>
	  	</td>
	  	<td class=cart_row>

	  		<a href='<?=$url?>' target="<?=$target?>">
	  		<?if( $img )
	  		{
	  			?><img src='<?= $baseurl ?>itempics/<?=$cart_line[product_id]?><?=$CFG->home_suffix?>.jpg' border=0 align="left" style="margin: 0px 10px;"><?
	  		}?>
	  		</a>
	  		<a href='<?=$url?>' target="<?=$target?>" style='font-weight: bold;'><?=$cart_line[product_name]?></a>
	  		<?echo'<br>';?>
	  		<?= $options ?>

	  	</td>

	  	<td class=cart_row><?= $cart_line['qty'] ?></td>
	  	<td class=cart_row>$<?=number_format($price,2)?></td>
	  	<td class=cart_rowr>$<?= number_format($cart_line['item_total'], 2)?>
	  	</td>
	  </tr>
	  <?
		}

	}



	static function showPackageLine($cart_line,$color_bg = false, $javascript_off = false)
	{
		global $CFG;

		$baseurl = (isSSL()) ? $CFG->sslurl : $CFG->baseurl;

		$product_info = Products::get1($cart_line['product_id']);
		$url = Catalog::makeProductLink_($product_info, $product_info['cat_id'], $product_info['occasion_id']);

		$options = '';

		if (is_array($cart_line['options'])) {

			foreach ($cart_line['options'] as $option) {
				if($option['vendor_sku']) $options .= "$option[vendor_sku]<br>";
				$options .= "$option[value]<br>";
			}
		}

	  ?>
	  <tr>
	  	<td class=cart_rowl>

	  		<a href='<?=$url?>'><img src='<?= $baseurl ?>itempics/<?=$cart_line[product_id]?><?=$CFG->home_suffix?>.jpg' border=0 align="left" style="margin: 0px 10px;"></a>
	  		<a href='<?=$url?>' style='font-weight: bold;'><?=$cart_line[product_name]?></a><br>
	  		<?= $product_info['vendor_sku'] ?><br>
	  		<?= $options ?>

	  	</td>

	  	<td class=cart_row><?= $cart_line['qty'] ?></td>
	  	<td class=cart_row>--</td>
	  	<td class=cart_rowr>--</td>
	  </tr>
	  <?

	}



	static function getItem($id)
	{
		$id = (int) $id;

		if (!$id) return false;

		$result = Orders::getItems($id);
		return $result[0];
	}

	static function insertItem($info)
	{
		$item_id = db_insert('order_items',$info);

		//Orders::recalcOrder( $info['order_id'] );
		Tracking::trackInsert( 'order_item', $item_id );
		return $item_id;
	}

	static function updateItem($id,$info)
	{
		$tracking_id = Tracking::trackUpdate( 0, 'order_item', $id, 'update' );
		$result = db_update('order_items',$id,$info);
		$tracking = Tracking::trackUpdate( $tracking_id ); 
		return $result;
	}

	static function updateItemByOrderId($order_id, $info)
	{
	    db_update('order_items', $order_id, $info, 'order_id');
	}

	static function deleteItem($id)
	{
		db_delete( 'order_item_options', $id, 'order_item_id' );
		return db_delete('order_items',$id);
	}

	static function recalcOrder($id, $sales=false, $recalc_shipping = true )
	{
	    //Global Override -- No more auto recalcing shipping period.
	    $recalc_shipping = false;

		// we need this to reference to old subtotal to calc shipping.
		//$oldorder = Orders::get1($id);
		//used to determine if shipping override was enabled
		//$shipping_override=$oldorder[shipping_override];
		global $CFG;

		$cur_order = Orders::get1( $id );
		$store_id  = $cur_order['order_tax_option'];
		$items = Orders::getItems(0,$id);

		$lastpackageid = 0;
		$total = 0;

		if(is_array($items))
		foreach($items as $item)
		{
			//print_ar( $item );
			if ($item['order_package_id'] == 0)
			{
				//This isn't accounting for any options that may increase the price of this product

		//		if (is_array($item['options'])) {
		//			foreach ($item['options'] as $option) {
		//				$total += $option['additional_price'];
		//			}
		//		}
		//		else
		//		{
				 $total += ($item['qty'] * $item['price']) + $item['product_shipping_price'];
		//		}
				$total += $item['product_shipping_price'];
			  //echo $total;
			}
			else
			{
			  	if ($item['order_package_id'] != $lastpackageid)
			  	{
			  	   $lastpackageid = $item['order_package_id'];
			  	   $order_package = Orders::getOrderPackage($item['order_package_id']);
			  	   $total += ($order_package[0]['price'] * $order_package[0]['qty']);
			  	}
			}
		}
		Orders::update($id, array ( 'subtotal' => $total ));
		if( $recalc_shipping && in_array( $store_id, $CFG->recalc_shipping_stores ) )
		{
			Orders::update($id, array ( 'shipping' => Shipping::calcShipping($id, 'order') ));
		}

		$order = Orders::get1($id);		
//	if( !$sales )
//	{
		/*
		if(strtoupper($shipping['state']) == 'NY'){
			$tax = new Tax(substr($shipping['zip'],0,5), $shipping['city']);
			$tax_rate = $tax->getRate();
		}
		else*/

//		if( strtoupper($order['shipping_state']) == 'NJ' )
//		{
//			$store = Store::get1( $order['order_tax_option'] );
//			$tax_rate = $store['tax_rate'];
////			$tax_rate = 7;
//		}else {
//		    $tax_rate = 0;
//		}

		$tax_rate = $order['tax_rate'];
		
		if( $order['override_tax'] == 'Y' )
		{
		    $tax_rate = $order['tax_rate'];

		}
		else {
		    
		    //Don't touch amazon or ebay orders
		    if( $order['ebay_order_id'] == '' && $order['amazon_order_id'] == '' ) {
		    
			if( $order['is_tax_exempt'] == 'N'  && in_array( $store_id, $CFG->recalc_tax_stores) ) {

			    if( strtoupper($order['shipping_state']) == strtoupper('NY') )
			    {
				$taxes          = new Taxes(substr($order['shipping_zip'], 0, 5), $order['shipping_city']);
				$tax_rate       = $taxes->getRate();			       
			    }
			}
		    }
		}
		//Removed this rule 7/22 by request from Estee
		//else
		 //   $tax_rate = 0;

//	}
//	else
//	{
//		if( $order['order_tax_option'] )
//		{
//			$store = Stores::get1( $order['order_tax_option'] );
//			$tax_rate = $store['tax_rate'];
//		}
//		else
//			$tax_rate = 0;
//	}
	    Orders::update($id, array ('tax_rate' => $tax_rate, 'subtotal' => $total ));
	}

	static function insertItemOption($info)
	{
		return db_insert('order_item_options',$info);
	}

	static function updateItemOption($id,$info)
	{
		return db_update('order_item_options',$id,$info);
	}

	static function deleteItemOption($id)
	{
		return db_delete('order_item_options',$id);
	}

	static function insertPackage($info)
	{
		return db_insert('order_packages',$info);
	}

	static function updatePackage($id,$info)
	{
		return db_update('order_packages',$id,$info);
	}

	/* THIS WILL NOT WORK RIGHT, REWRITTEN ABOVE -- JAIMIE.
	static function deletePackage($id)
	{
		return db_delete('order_packages',$id);
	}*/

	//

	static function getHistory($id=0,$order_id=0)
	{
		$sql = "SELECT order_history.*,
					order_statuses.name AS order_status_name,
					admin_users.first_name,
					admin_users.last_name
				FROM order_history
				LEFT JOIN order_statuses ON order_statuses.id = order_history.status_id
				LEFT JOIN admin_users ON admin_users.id = order_history.user_id
				WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND order_history.id = $id ";
		}
		if ($order_id > 0) {
			$sql .= " AND order_history.order_id = $order_id ";
		}

		$sql .= " ORDER BY date";

		return db_query_array($sql);
	}

	static function getHistoryLine($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Orders::getHistory($id);
		return $result[0];
	}

	static function insertHistory($info)
	{
		if( $info['status_id'] )
			Orders::update($info['order_id'],array('current_status_id'=>$info['status_id']));

		return db_insert('order_history',$info,'date');
	}
	/*
	static function updateHistory($id,$info)
	{
		return db_update('order_history',$id,$info);
	}

	static function deleteHistory($id)
	{
		return db_delete('order_history',$id);
	}*/

	static function getCharges($order_id, $type='')
	{
		$sql = "SELECT order_charges.*,
					admin_users.first_name,
					admin_users.last_name
				FROM order_charges
				LEFT JOIN admin_users ON admin_users.id = order_charges.user_id
				WHERE order_charges.order_id = $order_id ";

		if( $type )
			$sql .= " AND order_charges.action = '$type' ";

		$sql .= " ORDER BY date";

		return db_query_array($sql);
	}

	static function get1Charge($charge_id){

		$charge_id = (int)$charge_id;

		$sql = " SELECT order_charges.*,
					admin_users.first_name,
					admin_users.last_name
				FROM order_charges
				LEFT JOIN admin_users ON admin_users.id = order_charges.user_id ";
		$sql .= " WHERE order_charges.id = '$charge_id' ";

		return db_get1( db_query_array($sql) );
	}

	static function isChargeVoided($id){

		$id = (int)$id;

		$sql = " SELECT order_charges.* FROM order_charges
			WHERE ( action = 'void' OR action = 'credit' ) AND pid = '$id' ";

		return is_array( db_query_array($sql) );
	}
	
	static function isAuthCaptured($id){

		$id = (int)$id;

		$sql = " SELECT order_charges.* FROM order_charges
			WHERE ( action = 'capture') AND pid = '$id' ";

		return is_array( db_query_array($sql) );
	}
	
	static function addNote($info)
	{
		return db_insert('order_notes', $info, 'date_added');
	}

	static function getNotes($order_id, $type='notes', $id=0)
	{
		$sql = "SELECT order_notes.*,
					  CONCAT(admin_users.first_name, ' ',  admin_users.last_name) AS user_name
				    FROM order_notes
				    LEFT JOIN admin_users ON admin_users.id = order_notes.user_id
				    WHERE 1 ";
		if( $id )
			$sql .= " AND order_notes.id = $id ";

		$sql .=" AND order_notes.order_id = $order_id and type = '$type'
				    ORDER BY date_added DESC";

		return db_query_array($sql);
	}

	static function insertCharge($info)
	{
		if($info['date']){
			$dateStr = '';
		} else {
			$dateStr = 'date';
		}
		return db_insert('order_charges',$info,$dateStr);
//		return db_insert_id();
	}

	static function updateCharge( $id, $info )
	{
	    if($info['date']){
			$dateStr = '';
		} else {
			$dateStr = 'date';
		}
		return db_update('order_charges',$id, $info,'id',$dateStr);

	}

	static function sendVendorInvoices($order_id)
	{
		global $CFG;

		/*
		$vendors = Orders::getOrderVendors($order_id);

		//print_ar($vendors);

		foreach($vendors as $vendor) {
			//print_ar($vendor);
		  $the_invoice = Orders::makeVendorInvoice($order_id, $vendor['id']);

		  $contacts = Vendors::getContacts(0, $vendor['id'], 'Y');
		  if ($contacts) foreach ($contacts as $c) {
		    $send_email = new SendEmail();
		    $send_email->setTextBody($the_invoice);
		    $send_email->setHTMLBody($the_invoice);
		    $send_email->setFrom("1.800.DESSERT", $CFG->orders_email);
		    $send_email->setTo("$c[title] $c[first_name] $c[last_name]", $c['email']);
		    $send_email->setSubject("1.800.DESSERT Order $order_id dispatch to $vendor[name]");
		    if ($CFG->in_testing) {
		    	$send_email->setTo('XX', "jsirovic@rustybrick.com");
		    }
		    $send_email->send();
		    //echo "SEND";
		  }
		}
		*/
	}

	static function makeVendorInvoice($order_id, $vendor_id)
	{
		global $CFG;

    ob_start();

    $vendor = Vendors::get1($vendor_id);

    $order = Orders::get1($order_id);

    //print_ar($order);

	  $customer = Customers::get1($order['customer_id']);

	  //print_ar($customer);

	  $orderitems = Orders::getItems(0, $order_id, '', '', $vendor_id);

	  echo "<pre>";

		foreach ($orderitems as $item) {

	  if ($vendor['email_format'] == 'standard') {

		  /******************************* standard ***********************************/


		  //print_ar($item);

		  echo "Order ID: $order_id";
		  $line = "\n--------------------------------------------------------------------------\n";

		  	echo $line;

		  	if (!$item['options']) {
		  		echo "$item[product_vendor_sku] - ";
		  	}

		  	echo "$item[product_name] - QTY: $item[qty]";

		  	if ($item['options']) foreach ($item['options'] as $option) {
		  		echo "\n$option[name]: <b>$option[vendor_sku]</b> $option[value] ";
		  		//print_ar($item[options]);
		  	}

		  	echo "\n\n";

		  	echo "Recipient:\n";
		  	echo "$item[recip_company]\n";
		  	echo "$item[recip_first_name] $item[recip_last_name]\n";
		  	echo "$item[recip_addr_1]\n";
		  	if ($item[recip_addr_2])
		  	  echo "$item[recip_addr_2]\n";
		  	echo "$item[recip_city], $item[recip_state] $item[recip_zip]\n\n";

		  	echo "Message: " . ($item[message] ? $item[message] : "NONE") . "\n\n";

		  	echo "Ship method: $item[shipping_name]\n";

		  	if ($item['deliver_date'] != '0000-00-00')
		  		echo "Ship By: $item[ship_date]\n";
		  	else
		  		echo "Ship By: UNDEFINED\n";

		  	if ($item['deliver_date'] != '0000-00-00')
		  	  echo "Deliver By: $item[deliver_date]\n";
		  	else
		  	  echo "Deliver By: UNDEFINED\n";

		  	if ($item[recip_phone])
		  	  echo "Recipient Phone: $item[recip_phone]\n";

		  	if ($customer['first_name'] || $customer['last_name']) {
		  		echo "Billing Name: $customer[first_name] $customer[last_name]\n";
		  	}

		  	if ($customer[phone])
		  	  echo "Billing Phone: $customer[phone]";

	 		  echo $line;

		  	/******************************* standard ***********************************/

		  } else {

		  	/******************************* pseudoxml ***********************************/

		  	echo "\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"	 ;
		  	echo ":orderid:$order_id\n";

		  	// this will never happen anyway with this vendor, but i added this while i was here.
		  	if (!$item['options']) {
		  		echo ":productsku:$item[product_vendor_sku]\n";
		  	} else {
		  		echo ":productsku:{$item['options'][0]['vendor_sku']}\n";
		  	}

		  	echo ":productname:$item[product_name]\n";
		  	echo ":qty:$item[qty]\n";
		  	echo ":company:$item[recip_company]\n";
		  	echo ":firstname:$item[recip_first_name]\n";
		  	echo ":lastname:$item[recip_last_name]\n";
		  	echo ":address:$item[recip_addr_1]\n";
		  	echo ":apt:$item[recip_addr_2]\n";
		  	echo ":city:$item[recip_city]\n";
		  	echo ":state:$item[recip_state]\n";
		  	echo ":zip:$item[recip_zip]\n";
		  	echo ":shipmethod:$item[shipping_name]\n";
		  	echo ":shipby:$item[ship_date]\n";
		  	if ($item['deliver_date'] != '0000-00-00')
		  	  echo ":deliverby:$item[deliver_date]\n";
		  	else
		  	  echo ":deliverby:UNDEFINED\n";
		  	echo ":recipphone:$item[recip_phone]\n";
	  		echo ":billingname:$customer[first_name] $customer[last_name]\n";
		  	echo ":billingphone:$customer[phone]\n";
		  	echo "\n:begin_message:\n$item[message]\n:end_message:\n";
		  	echo "\n:begin_special_instructions:\n";
	      echo "\n:end_special_instructions:\n";
		  	echo "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"	  ;
		  }

	  }
		  /******************************* pseudoxml ***********************************/

	  echo "</pre>";

	  $ret = ob_get_contents();
	  ob_end_clean();
	  return $ret;

	}


  function makeInvoiceForEmail($oid, $display = false, $javascript_off = false)
  {
	global $CFG;

    ob_start();

    $order          = Orders::get1($oid);
    $ostatus        = OrderStatuses::get1($order['current_status_id']);
	$customer       = Customers::get1($order['customer_id']);
	$orderitems     = Orders::getItems(0,$oid);

    echo "<table border='0' width='99%'>".
    "<tr>".
    "<td width='100' valign='top'><strong>Ship&nbsp;To:</strong></td>".
    "<td valign='top'>".
      str_replace(" ", "&nbsp;", "$order[shipping_first_name] $order[shipping_last_name]<br>");

    if( $order['shipping_company'] )
	echo $order['shipping_company'] . '<br>';
    echo  str_replace(" ", "&nbsp;", "$order[shipping_address1]<br>");
    if (strlen($order['shipping_address2']) > 0)
      echo "$order[shipping_address2]<br>";
    echo "$order[shipping_city], $order[shipping_state] $order[shipping_zip]<Br>".
    "</td>".
    "<td rowspan='2' width='150'></td>".
    "<td width='100' valign='top'><strong>Bill&nbsp;To:</strong></td>".
    "<td valign='top'>" .
       str_replace(" ", "&nbsp;", "$order[billing_first_name] $order[billing_last_name]<br>");
    if( $order['billing_company'] )
	echo $order['billing_company'] . '<br>';
       str_replace(" ", "&nbsp;", "$order[billing_address1]<br>");
    if (strlen($order['billing_address2']) > 0)
      echo "$order[billing_address2]<br>";
    echo "$order[billing_city], $order[billing_state] $order[billing_zip]<Br>".
    "</td>".
    "</tr>".
    "</table>";

    echo "<br>";

    echo "<table border=0>".
    "<tr>".
    "<td width='100'><strong>Order&nbsp;#:</strong></td>".
    "<td align='left'>{$CFG->order_prefix}$order[id]</td>".
    "</tr>".
    "<tr>".
    "<td><strong>Ordered on:</strong></td>".
    "<td align='left'>" . db_date($order[date],'F j, Y (D)') . "</td>" .
    "</tr>".
    "<tr>".
    "<td><strong>Order status:</strong></td>".
    "<td align='left'>" . $ostatus['name'] . "</td>" .
    "</tr>".
    "</table>";

		if (is_array($orderitems))
			$orderitems_count = sizeof($orderitems);
		else
			$cart_count = 0;

		$last_package_id = 0;

		echo "<br>";

		echo "<table class='invoice' border='0' cellspacing='0' cellpadding='5' width=99%>".

		"<tr>
			<td style='font-weight:bold;height:30px;'>Product Name</td>
			<td style='font-weight:bold;height:30px;'>Quantity</td>
			<td style='font-weight:bold;height:30px;'>Price</td>
			<td style='font-weight:bold;height:30px;'>Total</td>
		</tr>
		";
		if( is_array( $orderitems ) )
		{
			$prev_name = "";
			foreach ($orderitems as $order_line) {
				if ($order_line['order_package_id'] != $last_package_id && $order_line['order_package_id'] > 0) {
					$bg_color = !$bg_color;
					$package_line = Orders::getPackageLine($order_line['order_package_id']);
					$package_line['line_total'] = $package_line['price'] * $package_line['qty'];
					$grand_total += $package_line['line_total'];
					$last_package_id = $order_line['order_package_id'];
					//echo "this is a primary package line<br>";
					Orders::showPackageMainLine($package_line,$bg_color);
				}

				if ($order_line['order_package_id'] > 0) {
					Orders::showPackageLine($order_line,$bg_color, $javascript_off);
				}
				else {
					$bg_color = !$bg_color;
					Orders::showProductLine($order_line, $bg_color, $javascript_off, true, false, false, $prev_name);
					$prev_name = $order_line['product_name'];
					$grand_total += $order_line['line_total'];
				}
			}
		}
		echo "</table>";

		echo "<br>";

	    echo "<table>".
		"<tr>".
		"<td width='100'>".
	    "<strong>Subtotal:</strong>".
	    "</td>".
	    "<td>\$".
	    number_format($order['subtotal'],2).
	    "</td>".
	    "</tr>";

	    $order['promo_discount'] += $order['shipping_discount'];
	    if($order['promo_discount'] > 0){
		    echo "<tr>".
                    "<td></td>".
		    "<td>".
		    "<strong>Discount:</strong>".
		    "</td>".
		    "<td>\$".
		    number_format($order['promo_discount'],2).
		    "</td>".
		    "</tr>";
	    }
	    

	    if($order['min_charges'] > 0){
		    echo "<tr>".
		    "<td>".
		    "<strong>Minimum Surcharge:</strong>".
		    "</td>".
		    "<td>\$".
		    number_format($order['min_charges'],2).
		    "</td>".
		    "</tr>";
	    }

	    echo
		"<tr>".
	    "<td>".
	    "<strong>Tax rate:</strong>".
	    "</td>".
	    "<td>".
	    $order['tax_rate']. "% $" . number_format($order['tax_rate'] * ($order['subtotal'] - $order['promo_discount']) * 0.01, 2) .
	    "</td>".
	    "</tr>".
		"<tr>".
	    "<td>".
	    "<strong>Shipping:</strong>".
	    "</td>".
	    "<td>".
	    "\$" . number_format($order['shipping'],2).
	    "</td>".
	    "</tr>".
		"<tr>".
	    "<td>".
	    "<strong>Total:</strong>".
	    "</td>".
	    "<td>".
	    "\$" . number_format($order['order_total'], 2) .
	    "</td>".
	    "</tr>".
	    "</table>
	    <br>";

	    echo "
	       <table>
	           <tr width='300'>
	               <td> <strong>Order Notes</strong> </td>
	           </tr>
	           <tr width='300'>
	               <td>" . $order['notes'] . "</td>
	           </tr>
	       </table>

	    ";

	  	$html = ob_get_contents();
		ob_end_clean();

		if ($display)
		  echo $html;

		return $html;
  }

  function makeInvoiceForDisplay($oid, $display = false, $javascript_off = false)
  {
	global $CFG;

    ob_start();

    $order          = Orders::get1($oid);
    $ostatus        = OrderStatuses::get1($order['current_status_id']);
	$customer       = Customers::get1($order['customer_id']);
	$orderitems     = Orders::getItems(0,$oid);

    ?>
	<div class="invoice">

		<div style="clear:both"></div>

        <textarea class="address" readonly="readonly">
        Bill To:

<?=$order['billing_first_name']?> <?=$order['billing_last_name'] . "\n"?>
<?
if( $order['billing_company'] ) {
    echo $order['billing_company'] . "\n";
}
?>
<?=$order['billing_address1'] . "\n"?>
<?=($order['billing_address2']) ? $order['billing_address2'] . "\n" : ''?>
<?=$order['billing_city']?>, <?=$order['billing_state']?> <?=$order['billing_zip'] . "\n"?>

        Phone: <?=$order['billing_phone'] . "\n"?>
        </textarea>

                <textarea class="address" readonly="readonly">
        Ship To:

<?=$order['shipping_first_name']?> <?=$order['shipping_last_name'] . "\n"?>
<?
if( $order['shipping_company'] ) {
    echo $order['shipping_company'] . "\n";
}
?>
<?=$order['shipping_address1'] . "\n"?>
<?=($order['shipping_address2']) ? $order['shipping_address2'] . "\n" : ''?>
<?=$order['shipping_city']?>, <?=$order['shipping_state']?> <?=$order['shipping_zip'] . "\n"?>

        Phone: <?=$order['shipping_phone'] . "\n"?>
        </textarea>

		<div class="customer">

            <table class="meta">
                <tr>
                    <td class="meta-head">Order #</td>
                    <td><div class="due"><?=$CFG->order_prefix.$order[id]?></div></td>
                </tr>

                <tr>
                    <td class="meta-head">Status</td>
                    <td><div class="due"><?=$ostatus['name']?></div></td>
                </tr>

                <tr>

                    <td class="meta-head">Date</td>
                    <td><div class="due"><?=db_date($order[date],'F j, Y')?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Total Due</td>
                    <td><div class="due">$<?=number_format($order['order_total'], 2)?></div></td>
                </tr>

            </table>

		</div>

		<table class="items">

		  <tr>
		      <th>SKU</th>
		      <th>Name</th>
		      <th>Unit Cost</th>
		      <th>Qty</th>
		      <th>Price</th>
		  </tr>

		  <?
		$items = $orderitems;
        
    	$this_option = $this_product = $prev_option = $prev_product = $prev_cart_item = $prev_b_type = "";
     	$b_reg_function = "Orders::showInvoiceLineBReg";
 		$b_case_function = "Orders::showInvoiceLineBCase";
 		$a_function = "Orders::showInvoiceLineA";        		
		 		
 		include "/var/www/vhosts/tigerchef.com/htdocs/includes/case_cart_include.php";
          ?>

		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal</td>
		      <td class="total-value"><div class="subtotal">$<?=number_format($order['subtotal'],2)?></div></td>
		  </tr>

		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Discount</td>
		      <td class="total-value"><div class="subtotal">$<?=number_format($order['promo_discount'] + $order['shipping_discount'],2)?></div></td>
		  </tr>

		  <?
		  if( $order['min_charges'] > 0 )
		  {
		      ?>
		      <tr>
			  <td colspan="2" class="blank"> </td>
			  <td colspan="2" class="total-line">Minimum Surcharge:</td>
			  <td class="total-value"><div class="subtotal">$<?=number_format($order['min_charges'],2)?></div></td>
		      </tr>
		      <?
		  }
		  ?>
          <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Shipping</td>
		      <td class="total-value"><div class="total">$<?=number_format($order['shipping'],2)?></div></td>
		  </tr>

          <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Tax</td>
		      <td class="total-value"><div class="total"><?=$order['tax_rate'] . "%<br/> $" . number_format($order['tax_rate'] * ($order['subtotal'] - $order['promo_discount'] + $order['shipping'] - $order['shipping_discount'] + $order['min_charges'] ) * 0.01, 2)?></div></td>
		  </tr>

		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td style="background-color:#eee" colspan="2" class="total-line">Total</td>
		      <td style="background-color:#eee" class="total-value"><div class="total">$<?=number_format($order['order_total'], 2)?></div></td>
		  </tr>

		</table>
	</div>


    <div class="invoice" style="width:400px;">

		<div style="clear:both"></div>

		<table class="items">

		  <tr>
		      <th>Order Notes</th>
		  </tr>

		  <tr class="item-row">
		      <td class="item-name"><div class="delete-wpr"><textarea readonly="readonly"><?=$order['notes']?></textarea></div></td>
		  </tr>

		</table>
	</div>
    <?

	  	$html = ob_get_contents();
		ob_end_clean();

		if ($display)
		  echo $html;

		return $html;
  }

  function makeInvoice($oid, $display = false, $javascript_off = false)
  {
	  global $CFG;

    ob_start();

    $order = Orders::get1($oid);

    $ostatus = OrderStatuses::get1($order['current_status_id']);

	  $customer = Customers::get1($order['customer_id']);

	  $orderitems = Orders::getItems(0,$oid);


    echo "<table border=0>".
    "<tr>".
    "<!--<td width='100' valign='top'><strong>Ship&nbsp;To:</strong></td>".
    "<td valign='top'>".
      spaceToNbsp("$order[shipping_first_name] $order[shipping_last_name]<br>");

    if( $order['shipping_company'] ) {
	 echo spaceToNbsp("$order[shipping_company]<br>");
    }

      echo spaceToNbsp("$order[shipping_address1]<br>");
    if (strlen($order['shipping_address2']) > 0)
      echo "<br>$order[shipping_address2]<br>";
    echo "$order[shipping_city], $order[shipping_state] $order[shipping_zip]<Br>".
    "</td>".
    "<td rowspan='2' width='50'></td>-->".
    "<td valign='top'><strong>Bill&nbsp;To:</strong></td>".
    "<td rowspan='2' width='20'></td>".
    "<td valign='top'>" .
       spaceToNbsp("$customer[first_name] $customer[last_name]<br>");
    if( $order['billing_company'] ) {
	 echo spaceToNbsp("$order[billing_company]<br>");
    }
     echo  spaceToNbsp("$customer[address1]<br>");
    if (strlen($customer['address2']) > 0)
      echo "<br>$customer[address2]<br>";
    echo "$customer[city], $customer[state] $customer[zip]<Br>".
    "</td>".
    "</tr>".
    "</table>";

    echo "<br>";

    echo "<table border=0>".
    "<tr>".
    "<td width='100'><strong>Order&nbsp;id:</strong></td>".
    "<td align='left'>{$CFG->order_prefix}$order[id]</td>".
    "</tr>".
    "<tr>".
    "<td><strong>Ordered on:</strong></td>".
    "<td align='left'>" . db_date($order[date],'F j, Y (D)') . "</td>" .
    "</tr>".
    "<tr>".
    "<td><strong>Order charge status:</strong></td>".
    "<td align='left'>" . $ostatus['name'] . "</td>" .
    "</tr>".
    "</table>";

		if (is_array($orderitems))
			$orderitems_count = sizeof($orderitems);
		else
			$cart_count = 0;

		$last_package_id = 0;

		echo "<br>";

		echo "<table class='invoice' cellspacing='0' cellpadding='5' width=90%>".

		"<tr class=cart_header>
			<!-- <th width=50>Picture</th> -->
			<!-- <th width=50>Option</th> -->
			<th>Product Name/Options</th>
			<th>Recipient</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Shipping</th>
			<th>Total</th>
			<th>Status</th>
		</tr>";

		foreach ($orderitems as $order_line) {
			if ($order_line['order_package_id'] != $last_package_id && $order_line['order_package_id'] > 0) {
				$bg_color = !$bg_color;
				$package_line = Orders::getPackageLine($order_line['order_package_id']);
				$package_line['line_total'] = $package_line['price'] * $package_line['qty'];
				$grand_total += $package_line['line_total'];
				$last_package_id = $order_line['order_package_id'];
				/** this will NOT work properly, but packages arent part of this site really .. **/
				Orders::showPackageMainLine($package_line,$bg_color);
			}

			if ($order_line['order_package_id'] > 0) {

				/** this will NOT work properly, but packages arent part of this site really .. **/
				Orders::showPackageLine($order_line,$bg_color, $javascript_off);

			}
			else {
				/** this is the only part that's fully implemented **/
				$bg_color = !$bg_color;
				Orders::showProductInquiryLine($order_line, $bg_color, $javascript_off);
				$grand_total += $order_line['line_total'];
			}
		}

		echo "</table>";


	  	$html = ob_get_contents();
		ob_end_clean();

		if ($display)
		  echo $html;

		return $html;
  }

	static function showProductInquiryLine($cart_line,$color_bg = false, $javascript_off = false)
	{

		global $CFG;

		$baseurl = (isSSL()) ? $CFG->sslurl : $CFG->baseurl;

		$options = '';

		if (is_array($cart_line['options'])) {
			$optionid = '';
			foreach ($cart_line['options'] as $option) {
				if (file_exists('optionpics/'.$option['product_option_id'].$CFG->thumbnail_suffix)) {
					$optionid = $option['product_option_id'];
				}

				if ($option['is_upgrade'] == 'Y') {
					$option['value'] .= ' <span class=upgrade>UPGRADE (+$'.number_format($option['additional_price'],2).')</span>';
				} else if ($option['additional_price'] > 0)
				{
				  	$option['value'] .= ' <span class=upgrade>ADDITIONAL (+$'.number_format($option['additional_price'],2).')</span>';
				}


				$options .= "<small><b>$option[name]:</b> $option[value]</small><br>";
			}
		}

		if ($color_bg)
			$class = ' class="cart_alt"';
		else
			$class = '';

		echo '<tr valign=top'.$class.' height=1>';


		/*
		<td align=center rowspan=1 class=cart_border_top>';

		if (file_exists('itempics/'.$cart_line['product_id'].$CFG->thumbnail_suffix)) {
			if ($javascript_off == false)
			{
			  echo '<a href="#" onclick="showLarge(\''.$cart_line['product_id'].'\');return false;"><img src="' . $baseurl . 'itempics/'.$cart_line['product_id'].$CFG->thumbnail_suffix.'" width=50 height=67 alt="" border=0></a>';
			}
			else
			{
			  echo '<a href="' . $baseurl . 'itempics/'.$cart_line['product_id'].xlarge_suffix . '"><img src="' . $baseurl . 'itempics/'.$cart_line['product_id'].$CFG->thumbnail_suffix.'" width=50 height=67 alt="" border=0></a>';
			}
		}
		else {
			echo '-';
		}

		echo '</td> <!-- <td align=center rowspan=1 class=cart_border_top> -->';

		*/

		/*
		if ($optionid) {
		  if ($javascript_off == false)
			echo '<a href="#" onclick="showLargeOption(\''.$optionid.'\');return false;"><img src="' . $baseurl . 'optionpics/'.$optionid.$CFG->thumbnail_suffix.'" width=50 height=67 alt="" border=0></a>';
		  else
		    echo '<a href="' . $baseurl . 'optionpics/'.$optionid.$CFG->xlarge_suffix . '"><img src="' . $baseurl . 'optionpics/'.$optionid.$CFG->thumbnail_suffix.'" width=50 height=67 alt="" border=0></a>';
		}
		else {
			echo '-';
		}*/

		$product_info = Products::get1($cart_line['product_id']);

		if ($cart_line['item_status_requested'] == 'N')
		  $status = "NO INQUIRY";
		else {
			$status = ($cart_line[order_item_status_name] ? $cart_line[order_item_status_name] : "PROCESSING INQUIRY");
		}

		echo '<!-- </td> -->
		  <td rowspan=1 class=cart_border_top><a class=cart_product href="'.Catalog::makeProductLink_($product_info).'">'.$cart_line['product_name'].'</a>'
		  . (($options) ? "<br>$options" : '') . '</td>
			  			  <td align=center class=cart_border_top>' . $cart_line['recip_first_name'] . " " . $cart_line['recip_last_name'] . '</td>
		    <td align=right class=cart_border_top>$'.number_format($cart_line['price'],2).'</td>
			  <td align=center class=cart_border_top>' . $cart_line['qty'] . '</td>

		    <td align=right class=cart_border_top>$'.number_format($cart_line['total_shipping'],2).'</td>
			  <td align=right class=cart_border_top>$'.number_format($cart_line['total_price_with_shipping'],2).'</b></td>
		  	<td align=right class=cart_border_top><a href="#legend"><u>'. $status . '</u></a></td>
		</tr>';

		if ($cart_line[order_item_status_notes]) {
			echo '<tr>
			  		<td colspan=7><b>Notes:</b> ' . $cart_line[order_item_status_notes]. '</td>
						</tr>
						';
		}
	}

  function orderItemStatusLegend()
  {

  	ob_start();
  	$ois = OrderItemStatuses::get();

  	?><br>
  	  <table cellpadding=8 cellspacing=0 class='invoice' width=90%>
  	  <tr class=cart_header><td colspan=2 align='center'>LEGEND</td></tr>

  	<?

  	foreach ($ois as $s) { ?>

  		<tr><td><nobr><b><?=$s[name]?></b></nobr></td><td><?=$s[description]?></td></tr>

  	<? }

  	?> <tr><td><nobr><b>NO INQUIRY</b></nobr></td><td>You did not request that we look into the status of this item.</td></tr> <?
  	?> <tr><td><nobr><b>PROCESSING</b></nobr></td><td>We are still inquiring about this item.</td></tr> <?

  	?> </table> <?
  	return ob_get_clean();

  }

function getOrderItemStatusRows($id = 0, $order_id = 0, $item_status_updated = '', $inquiry_status = '', $order = '', $order_asc = '')
{

	// inner join is deliberately a restrict condition.

	$delete_q = "DROP TABLE IF EXISTS _newest_tickets";

	db_query($delete_q);

	$temp_q = "CREATE TEMPORARY TABLE _newest_tickets SELECT MAX(id) as id FROM order_inquiries GROUP BY order_id";

	db_query($temp_q);

	$q = " SELECT
					vendors.id as vendor_id,
					vendors.name as vendor_name,
	 				products.name as product_name,
					products.vendor_sku as product_vendor_sku,
					products.vendor_sku_name as product_vendor_sku_name,
					orders.id as order_id,
					orders.date as order_date,
					customers.id as customer_id,
					customers.first_name as customer_first_name,
					customers.last_name as customer_last_name,
					order_items.id as order_item_id,
					order_items.qty as order_item_qty,
					order_items.ship_date as order_item_ship_date,
					order_items.deliver_date as order_item_deliver_date,
					order_items.order_item_status_id as order_item_status_id,
					order_item_statuses.id as order_item_status_id,
					order_item_statuses.name as order_item_status_name,
					order_items.shipping_id as order_item_shipping_id,
	        shipping_methods.name as shipping_method_name,
					order_inquiries.id as order_inquiry_id,
					order_inquiries.date_inquired as order_inquiry_date,
					order_inquiries.phone as order_inquiry_phone,
					order_inquiries.is_call_requested as order_inquiry_is_call_requested,
					order_inquiries.is_call_dispatched as order_inquiry_is_call_dispatched,
					order_inquiries.date_dispatched as order_inquiry_date_dispatched,
					order_inquiries.inquiry_status as order_inquiry_status,
					order_inquiries.comment as order_inquiry_comment,
					order_inquiries.phone as order_inquiry_phone,
	 				order_inquiries.email as order_inquiry_email

				FROM orders
				 LEFT JOIN customers ON (customers.id = orders.customer_id)
	       LEFT JOIN order_items ON (order_items.order_id = orders.id)
				 LEFT JOIN products ON (order_items.product_id = products.id)
				 LEFT JOIN vendors ON (vendors.id = products.vendor_id)
	       LEFT JOIN order_item_statuses ON ( order_item_statuses.id = order_items.order_item_status_id)
	       LEFT JOIN shipping_methods ON (shipping_methods.id = order_items.shipping_id)

				 INNER JOIN _newest_tickets ON (order_inquiries.order_id = orders.id)

				 LEFT JOIN order_inquiries ON (order_inquiries.id = _newest_tickets.id)

				WHERE order_items.item_status_requested = 'Y'";

	//echo $q;

	if ($id) {
		$q .= db_restrict('order_items.id', $id);
	}

	if ($order_id) {
		$q .= db_restrict('orders.id', $order_id);
	}

	if ($item_status_updated) {
		$q .= db_restrict('order_items.item_status_updated', $item_status_updated);
	}

	if ($inquiry_status) {
		$q.= db_restrict('inquiry_status', $inquiry_status);
	}

	$q .= db_orderby($order, $order_asc);

	//echo $q;

	return db_query_array($q)	;
}

function insertOrderPromoCode($info)
{
	if ($info['promo_code_id'] == 154) mail("7188691069@VTEXT.COM", "Promo Code VCW953154478G was used", "On order".$info['order_id']);
	return db_insert('order_promo_codes', $info);
}

function getOrderPromoCodes($order_id)
{
	$q = " SELECT order_promo_codes.*, promo_codes.code as promo_code_code, promo_codes.description as promo_code_description FROM order_promo_codes LEFT JOIN promo_codes ON (promo_codes.id = order_promo_codes.promo_code_id)WHERE 1 ";

	$q .= db_restrict('order_id', $order_id);

	//echo $q;

	return db_query_array($q);
}

function checkPaymentInfo( $order_id )
{
	$order = self::get1($order_id);
	if( $order['card_number'] && $order['exp_month'] && $order['exp_year'] )
		return true;
	else
		return false;
}

function checkPaymentAddress( $order_id )
{
	$order = self::get1($order_id);
	if(  $order['billing_first_name']
		&& $order['billing_last_name'] && $order['billing_address1'] && $order['billing_city'] && $order['billing_state']
		&& $order['billing_zip'] && $order['billing_phone'] )
		return true;
	else
		return false;
}

function checkShippingAddress( $order_id )
{
	$order = self::get1($order_id);
	if(  $order['shipping_first_name']
		&& $order['shipping_last_name'] && $order['shipping_address1'] && $order['shipping_city'] && $order['shipping_state']
		&& $order['shipping_zip'] && $order['shipping_phone'] )
		return true;
	else
		return false;
}

	static function displayOrderDetails($oid, $display = false, $javascript_off = false)
  {
	  global $CFG;

    ob_start();

    $order = Orders::get1($oid);

    $ostatus = OrderStatuses::get1($order['current_status_id']);

	  $customer = Customers::get1($order['customer_id']);

	  $orderitems = Orders::getItems(0,$oid);

	  //print_ar($orderitems);

    /*echo "<table border=0>".
    "<tr>".
    "<td><strong>Order&nbsp;#:</strong></td>".
    "<td align='left'>{$CFG->order_prefix}$order[id]</td>".
    "</tr>".
    "<tr>".
    "<td><strong>Ordered on:</strong></td>".
    "<td align='left'>" . db_date($order[date],'F j, Y (D)') . "</td>" .
    "</tr>".
    "<tr>".
    "<td><strong>Order status:</strong></td>".
    "<td align='left'>" . $ostatus['name'] . "</td>" .
    "</tr>".
    "</table>";*/

		if (is_array($orderitems))
			$orderitems_count = sizeof($orderitems);
		else
			$cart_count = 0;

		$last_package_id = 0;

		echo "<br>";
		echo'<table width="100%">';
		echo'<tr>';
		echo'<td align="center" width="75%">';
		echo "<table class='invoice' border='0' cellspacing='0' cellpadding='5'>".

		"<tr>
			<th class=cart_headerl>SKU</th>
			<th class=cart_header>Product Name</th>
			<th class=cart_header>Quantity</th>
			<th class=cart_header>Price</th>
			<th class=cart_header>Total</th>
		</tr>";
	if( is_array( $orderitems ) )
		foreach ($orderitems as $order_line) {
			if ($order_line['order_package_id'] != $last_package_id && $order_line['order_package_id'] > 0) {
				$bg_color = !$bg_color;
				$package_line = Orders::getPackageLine($order_line['order_package_id']);
				$package_line['line_total'] = $package_line['price'] * $package_line['qty'];
				$grand_total += $package_line['line_total'];
				$last_package_id = $order_line['order_package_id'];
				//echo "this is a primary package line<br>";
				Orders::showPackageMainLine($package_line,$bg_color);
			}

			if ($order_line['order_package_id'] > 0) {
				Orders::showPackageLine($order_line,$bg_color, $javascript_off);
			}
			else {
				$bg_color = !$bg_color;
				Orders::showProductLine($order_line, $bg_color, $javascript_off, false, false, true);
				$grand_total += $order_line['line_total'];
			}
		}

		echo "</table>";
		echo'</td>';
		echo'<td align="right">';
		//echo "<br>";

	    echo "<table align='right'>".
		"<tr>".
		"<td width='100'>".
	    "<strong>Subtotal:</strong>".
	    "</td>".
	    "<td>\$".
	    number_format($order['subtotal'],2).
	    "</td>".
	    "</tr>";

	    $order['promo_discount'] += $order['shipping_discount'];
	    
	    if($order['promo_discount'] > 0){
		    echo "<tr>".
		    "<td>".
		    "<strong>Discount:</strong>".
		    "</td>".
		    "<td>\$".
		    number_format($order['promo_discount'],2).
		    "</td>".
		    "</tr>";
	    }

	    echo
		"<tr>".
	    "<td>".
	    "<strong>Tax rate:</strong>".
	    "</td>".
	    "<td><select size='1' name='tax_rate' onchange='this.form.submit();'>
	    <option value='0'" . ($order['order_tax_option'] == 0?" selected ":"") . ">No Tax 0.0%</option>
	    <option value='1'" . ($order['order_tax_option'] == 1?" selected ":"") . ">Passaic 3.5%</option>
	    <option value='2'" . ($order['order_tax_option'] == 2?" selected ":"") . ">Teneck 7.0%</option>
	    <option value='3'" . ($order['order_tax_option'] == 3?" selected ":"") . ">Website 7.0%</option>
	    <option value='4'" . ($order['order_tax_option'] == 4?" selected ":"") . ">Teneck signed Passaic 3.5%</option>" .
	    "</td>".
	    "</tr>".
		"<tr>".
	    "<td>".
	    "<strong>Shipping:</strong>".
	    "</td>".
	    "<td>".
	    "\$" . number_format($order['shipping'],2).
	    "</td>".
	    "</tr>".
		"<tr>".
	    "<td>".
	    "<strong>Total:</strong>".
	    "</td>".
	    "<td>".
	    "\$" . number_format($order['order_total'], 2) .
	    "</td>".
	    "</tr>";
	echo'</table>';
	echo'</td>';
	echo'</tr>';
	echo'</table>';
	echo'</table>';
	echo' <br />';
	echo'<table align="right">';
	echo'<tr>';
	echo'<td>';
	echo Buttons::actionButtonLink('','Save As Quote','_parent', 100);
	echo'</td>';
	echo'<td>';
	echo Buttons::actionButtonLink('','Complete Order','_parent', 100);
	echo'</td>';
	echo'<td>';
	echo Buttons::actionButtonLink('','Cancel Order','_parent', 100);
	echo'</td>';
	echo'<td>';
	echo Buttons::actionButtonLink('','Close','_parent', 100);
	echo'</td>';
	echo'</tr>';
	echo'</table>';
//	 echo Buttons::actionButtonLink('sales.php?action=add_item','Add Item','_parent', 150);
//	 echo Buttons::actionButtonLink('sales.php?action=add_discount','Add Discount','_parent', 150);
//	 echo Buttons::actionButtonLink('sales.php?action=complete_order','Complete Order','_parent', 150);
	  	$html = ob_get_contents();
		ob_end_clean();

		if ($display)
		  echo $html;

		return $html;
  }

  function clearCustomer( $order_id )
  {
  	//echo'clearing ' . $order_id;
  	$info = array();
  	$info['customer_id'] = $info['billing_first_name'] = $info['billing_last_name'] = $info['billing_company'] = $info['billing_address1']
  	= $info['billing_address2'] = $info['billing_city'] = $info['billing_state'] = $info['billing_zip'] = $info['billing_phone'] = '';

  	$info['shipping_first_name'] = $info['shipping_last_name'] = $info['shipping_company'] = $info['shipping_address1']
  	= $info['shipping_address2'] = $info['shipping_city'] = $info['shipping_state'] = $info['shipping_zip'] =
  	$info['shipping_phone'] = $info['shipping_phone2'] = $info['shipping_phone3'] = '';

  	Orders::update( $order_id, $info );
  }

  function addCustomerToOrder( $cust_id, $order_id )
  {
  	$info = array();

  	$cust = Customers::get1( $cust_id );

  	$info['customer_id'] = $cust_id;
  	$info['billing_first_name'] = $cust['first_name'];
  	$info['billing_last_name'] = $cust['last_name'];
  	$info['billing_company'] = $cust['company'];
  	$info['billing_address1']	= $cust['address1'];
  	$info['billing_address2'] = $cust['address2'];
  	$info['billing_city'] = $cust['city'];
  	$info['billing_state'] = $cust['state'];
  	$info['billing_zip'] = $cust['zip'];
  	$info['billing_phone'] = $cust['phone'];

  	$info['shipping_first_name'] = $cust['first_name'];
  	$info['shipping_last_name'] = $cust['last_name'];
  	$info['shipping_company'] = $cust['company'];
  	$info['shipping_address1'] = $cust['address1'];
  	$info['shipping_address2'] = $cust['address2'];
  	$info['shipping_city'] = $cust['city'];
  	$info['shipping_state'] = $cust['state'];
  	$info['shipping_zip'] = $cust['zip'];
  	$info['shipping_phone'] = $cust['phone'];

  	Orders::update( $order_id, $info );

  }

  function insertNoteHistory( $info )
  {
  	return db_insert('order_notes_history',$info,'date_added');
  }

  function getLastNote( $order_id, $type )
  {
  	$sql = " SELECT order_notes_history.*, concat(admin_users.first_name, ' ', admin_users.last_name) as admin_user
					  	FROM order_notes_history, admin_users where order_id = $order_id and type = '$type'
					  	and order_notes_history.user_id = admin_users.id order by date_added DESC limit 1 ";

  	$results = db_query_array( $sql );
  	if( $results )
  	{
  		return $results[0];
  	}
  	else
  		return false;
  }

  function getItemOption( $order_item )
  {
  	$sql = " SELECT * FROM order_item_options WHERE order_item_id = $order_item ";
  	$results = db_query_array( $sql );
  	if( $results )
  		return $results[0];
  	else
  		return false;
  }

  static function getOrderBalance( $order )
  {
  	$total = $order['order_total'];
  	$balance = $total;
  	$sql = " SELECT * FROM order_payments where order_id = $order[id] and action in ('capture', 'sale') and response_code = 0 ";
  	$rslt = db_query_array( $sql );

  	if( $rslt )
	  	foreach( $rslt as $p )
	  	{

	  		$balance = number_format($balance,2, '.', '' ) - number_format( $p['amount'], 2, '.', '' );

	  	}

	$sql = " SELECT * FROM order_payments where order_id = $order[id] and action = 'credit' and response_code = 0 ";
	$rslt = db_query_array( $sql );
  	if( $rslt )
	  	foreach( $rslt as $p )
	  	{

	  		$balance = number_format($balance,2, '.', '') + number_format( $p['amount'], 2, '.', '');

	  	}

  	return $balance;
  }

  static function displayNotes( $order_id )
  {
		global $CFG;
		if( !$_SESSION['current_order'] )
			$_SESSION['current_order'] = $order_id;
		$order = Orders::get1( $order_id );
		$driver_note = Orders::getLastNote( $order_id, 'driver_notes' );
		$note = Orders::getLastNote( $order_id, 'notes' );
		//print_ar( $driver_note );
		//I want to create a note interface that will be available through out the entire process.
		//tabs on the side of the screen for notes/driver notes, slide out interface
		//Should really do this in the orders app, or a new app since I need to use it in multiple files
		?>
			<script type="text/javascript">
			var note_history_displayed = false;
			var driver_history_displayed = false;
				static function displayOrderNotes( which )
				{
					new Effect.toggle( which, 'slide', { scaleX: false, duration: .5 });
					switch( which )
					{
						case 'order_driver_notes':
							if( driver_history_displayed && which == 'order_driver_notes' )
								new Effect.toggle( 'driver_note_history', 'slide', { scaleX: false, duration: .5 });
							break;
						default:
							if( note_history_displayed && which == 'order_notes')
								new Effect.toggle( 'note_history', 'slide', { scaleX: false, duration: .5 });
					}
				}

				static function saveNote( which )
				{

					var params = Form.serialize( which ) + "&id=<?=$order_id?>" ;

					switch( which )
					{
						case 'driver_note_form':
							$('driver_note').value = '';
						//	displayOrderNotes( 'order_driver_notes' );
							var div = 'driver_note_history';
							break;
						default:
							$('note').value = '';
						//	displayOrderNotes( 'order_notes' );
							var div = 'note_history';
					}

					var url = 'ajax.add-note.php';
					var ajax = new Ajax.Updater(
					div,
					url,
					{method: 'post', parameters: params, evalScripts: true }
					);
				}
				static function viewHistory( which, link )
				{
					switch( which )
					{
						case 'driver_note_history':
							if( driver_history_displayed )
							{
								link.innerHTML = 'View Driver Note History';
								driver_history_displayed = false;
							}
							else
							{
								link.innerHTML = 'Hide Driver Note History';
								driver_history_displayed = true;
							}
							break;
						default:
							if( note_history_displayed )
							{
								link.innerHTML = 'View Note History';
								note_history_displayed = false;
							}
							else
							{
								link.innerHTML = 'Hide Note History';
								note_history_displayed = true;
							}
					}
					displayOrderNotes( which );
				}
				static function editNote( id )
				{
					$('note').value = $('note_text_' + id).innerHTML;
					$('note_id').value = id;
				}

			</script>
			<div class="note_tab" id="order_notes_tab">
				<div class="tab"  onclick="displayOrderNotes( 'order_notes' );">
					<p><em>N</em><em>O</em><em>T</em><em>E</em><em>S</em></p>
				</div>
				<div class="note_window" id="order_notes" style="display: none;">
					<form name="note_form" id="note_form" action="">
					<input type="hidden" name="note_id" value="" id="note_id" />
						<h3>Enter New Note for Order: <?=$CFG->order_prefix . $order['id'] ?> </h3>
						<textarea class="order_note" name="note" id="note"></textarea>
						<input type="hidden" name="type" value="notes" />
						<br />
						<button type="button" onclick="saveNote( 'note_form' );">Save Note</button>
						<button type="button" onclick="displayOrderNotes( 'order_notes' );">Close</button>
						<br />
						<br />
						<a href="javascript:void(0);" onclick="viewHistory('note_history', this);">View Note History</a>
					</form>
				</div>
				<div class="note_history" id="note_history" style="display:none;">
				<?
						$order_notes = Orders::getNotes($order_id);

						$hth = new HtmlTableHeading($urlBegin);
						$hth->addItem('date','Date');
						$hth->addItem('user','User');
						$hth->addItem('notes','Notes');
						$hth->addItem('','Edit');
						echo '
							<table cellspacing=1 cellpadding=5 >
							<tr class=listing_heading>';

						$hth->show();

						//echo '<th>' . Buttons::actionButtonLink("orders.php?action=add_notes&id=$order_id", 'ADD', '_top') . '</th>';

						echo '</tr>';
						$bgcolor = '';
						if (is_array($order_notes))
						foreach($order_notes as $row) {

							//print_ar($row);

							$bgcolor = ($bgcolor == 'listing') ? 'listingb' : 'listing';
							echo "<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\" valign=top>
									<td>".db_date($row[date_added],'F j, Y h:i a')."</td>
									<td>$row[user_name]</td>
									<td id='note_text_$row[id]' >$row[notes]</td>
									<td><button type='button' onclick='editNote( $row[id] );'>Edit</button>
								 </tr>";
						}
						echo '</table>';
					?>
				</div>
			</div>
<? /*
			<div class="note_tab" id="order_driver_notes_tab">
				<div class="tab"  onclick="displayOrderNotes( 'order_driver_notes' );">
					<p><em>D</em><em>R</em><em>I</em><em>V</em><em>E</em><em>R</em></p>
				</div>
				<div class="note_window" id="order_driver_notes" style="display: none;">
					<form name="driver_note_form" id="driver_note_form" action="">
					<input type="hidden" name="type" value="driver_notes" />
						<h3>Enter New Driver Note for Order: <?=$CFG->order_prefix . $order['id'] ?></h3>
						<textarea class="order_note" name="note" id="driver_note"></textarea>
						<br />
						<button type="button" onclick="saveNote( 'driver_note_form' );">Save Note</button>
						<button type="button" onclick="displayOrderNotes( 'order_driver_notes' );">Close</button>
						<br />
						<br />
						<a href="javascript:void(0);" onclick="viewHistory('driver_note_history', this);">View Driver Note History</a>
						</form>
				</div>
				<div class="note_history" id="driver_note_history" style="display:none;">
				<?
						$order_notes = Orders::getNotes($order_id, 'driver_notes');

						$hth = new HtmlTableHeading($urlBegin);
						$hth->addItem('date','Date');
						$hth->addItem('user','User');
						$hth->addItem('notes','Notes');
						echo '
							<table cellspacing=1 cellpadding=5 >
							<tr class=listing_heading>';

						$hth->show();

						//echo '<th>' . Buttons::actionButtonLink("orders.php?action=add_notes&id=$order_id", 'ADD', '_top') . '</th>';

						echo '</tr>';
						$bgcolor = '';
						if (is_array($order_notes))
						foreach($order_notes as $row) {

							//print_ar($row);

							$bgcolor = ($bgcolor == 'listing') ? 'listingb' : 'listing';
							echo "<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\" valign=top>
									<td>".db_date($row[date_added],'F j, Y h:i a')."</td>
									<td>$row[user_name]</td>
									<td colspan='2'>$row[notes]</td>
								 </tr>";
						}
						echo '</table>';
					?>
				</div>
			</div>
	*/?>
		<?
  }

  static function hasNotes( $id )
  {
  	$sql = " SELECT count( * ) as count from order_notes where order_id = $id ";
  	$rslt = db_query_array( $sql );

  	return $rslt[0]['count'];
  }

  static function getPO( $id=0, $order_id=0 )
  {
  	$sql = "SELECT order_po.*, truckers.name FROM order_po ";
		$sql .= "LEFT JOIN truckers on truckers.id = order_po.supplier_id ";

  	$sql .= "WHERE 1 ";
  	if( $id )
  		$sql .= " AND id = $id ";

  	if( $order_id )
  		$sql .= " AND order_id = $order_id ";

  	return db_query_array( $sql );
  }

  static function getTracking( $id=0, $order_id=0, $cust_id='',$email_sent='', $start_date='', $end_date='',$total='',$limit='',$start='')
  {

  	$sql = "SELECT order_tracking.*,
  				   ups_shippers.name as shipper,
  				   sm.tracking_url,
  				   SUM(oit.qty) as num_units,
  				   orders.date,
  				   orders.date as order_date,
  				   store.name as store_name,
  				   sm.name as service_name ";
	if($total){
		$sql = " SELECT COUNT(DISTINCT(order_tracking.id)) as total ";
	}
  	$sql .= " FROM order_tracking ";
  	$sql .= " LEFT JOIN shipping_methods sm ON sm.id = order_tracking.shipper_id ";
//  	$sql .= "LEFT JOIN truckers on truckers.id = order_tracking.shipper_id ";
	$sql .= " LEFT JOIN orders ON orders.id = order_tracking.order_id ";
	$sql .= " LEFT JOIN store ON store.id = orders.order_tax_option "; // tigerchef, ebay, amazon...
	$sql .= " LEFT JOIN order_items_tracking oit ON oit.order_tracking_id = order_tracking.id ";
	$sql .= " LEFT JOIN ups_shippers ON ups_shippers.id = order_tracking.ship_from_id ";

  	$sql .= "WHERE 1 ";
  	if( $id ){
  		$id = (int)$id;
  		$sql .= " AND order_tracking.id = '$id' ";
  	}

  	if($cust_id){
  		$cust_id = (int)$cust_id;
  		$sql .= " AND orders.customer_id = '$cust_id' ";
  	}

  	if( $order_id ){
  		$order_id = (int)$order_id;
  		$sql .= " AND order_tracking.order_id = '$order_id' ";
  	}

  	if($email_sent){
  		$email_sent = db_esc($email_sent);
  		$sql .= " AND order_tracking.email_sent = '$email_sent' ";
  	}

  	if($start_date || $end_date){
  		$sql .= db_queryrange('orders.date',$start_date,$end_date);
  	}


  	if(!$total){
  		$sql .= " GROUP BY order_tracking.id ";
  	}
  	$sql .= " ORDER BY order_tracking.id DESC ";

        if ($limit > 0) {
			$sql .= db_limit($limit,$start);
		}

//    echo " **** $sql **** ";
  	$result = db_query_array( $sql );
  	if($total){
  		return (int)$result[0]['total'];
  	}

  	return $result;
  }

  static function insertPO( $info )
  {
  	ReviewNotifications::checkNewStatus($info['order_id'], $info['release_date']);
  	return db_insert( 'order_po', $info );
  }

  static function insertTracking( $info )
  {
  	return db_insert( 'order_tracking', $info );
  }

  static function updatePO( $id, $info )
  {
  	return db_update( 'order_po', $id, $info );
  }

  static function updateTracking( $id, $info, $date='' )
  {
  	return db_update( 'order_tracking', $id, $info, 'id',$date );
  }

	static function updateTrackingItems($tracking_id,$items_array){
		if(!$tracking_id){
			return;
		}

		db_delete('order_items_tracking',$tracking_id,'order_tracking_id');

		if(is_array($items_array)){
			foreach($items_array as $item_id => $qty){
				if ($qty > 0)// added by RSunness because was always inserting 0 records for lines that didn't ship with that tracking_id
				{
					db_insert('order_items_tracking',array(
					'order_tracking_id' => $tracking_id,
					'order_item_id' => $item_id,
					'qty' => $qty
					));
				}
			}
		}

		return true;
	}

  static function getUsedTracking()
  {
  	$sql = "SELECT tracking_number from order_tracking where 1 ";

  	return db_query_array( $sql );
  }

  static function updateNote( $note_id, $info )
  {
  	return db_update('order_notes', $note_id, $info );
  }

  static function deleteNote( $note_id )
  {
  	return db_delete( 'order_notes', $note_id );
  }

  static function listPayments( $order_id, $brief=false )
	{
		//Want to make a table similar to the order notes table which just lists each payment form
		$payments = Payments::getOrderPayments( $order_id );
		$order = Orders::get1( $order_id );
		//print_ar($payments);
		echo "<script type='text/javascript' onload='updateOrderSummaryRefresh( )'></script>";
		if( !$payments && !$brief )
			choosePayment( $order_id );
		elseif( !$brief )
		{
			$urlBegin ='';
			$hth = new HtmlTableHeading($urlBegin);
			$hth->addItem('','Date Authorized');
			$hth->addItem('payment_method', 'Card');
			$hth->addItem('last4', 'Last 4' );

			$hth->addItem('action','Action');
			$hth->addItem('amount','Amount');


			$hth->addItem('transaction_id','Transaction ID');
			$hth->addItem('', 'AVS' );
			$hth->addItem('response_msg','Response Message');
			echo '
				<table cellspacing=1 cellpadding=5 class=listing>
				<tr class=listing_heading>';

			$hth->show();

			echo '<th align=left colspan=2>' . Buttons::actionButtonLink("sales.php?action=add_payment&id=$order_id", 'ADD') . '</th>';

			echo '</tr>';
			$bgcolor = '';
			$total_payments = 0;
			$amount_captured = 0;
			$amount_authorized = 0;
			$balance = $order['order_total'];
			if (is_array($payments))
			{

				foreach($payments as $row) {
					unset( $auth_row );
					unset( $capture_row );
					unset( $main_row );
					if( $row['auth_id'] )
					{
						$auth_row = Payments::get1( $row['auth_id'] );
						if( $row['action'] == 'capture' )
							$capture_row = $row;

						$main_row = $capture_row;
						if( !$capture_row['response_code'] )
							$amount_captured += $capture_row['amount'];
					}
					elseif( $row['action'] == 'authorize' )
					{
						$auth_row = $row;
						$capture_row = "";
						$main_row = $auth_row;
						$amount_authorized += $auth_row['amount'];
						$captured = Payments::get(0,$order_id, 'capture', '','', $auth_row['id'] );
						if( $captured )
							continue;
					}
					elseif( $row['action'] == 'sale' )
					{
						$auth_row = $row;
						$capture_row = $row;
						$main_row = $row;
						if( !$capture_row['response_code'] )
							$amount_captured += $capture_row['amount'];
					}
					elseif( $row['action'] == 'credit' )
					{
						$amount_captured -= $row['amount'];
						$auth_row = $row;
						$main_row = $row;
						$auth_row['non_cc'] = true;
						$capture_row = $row;
					}
					else //Non CC payment
					{
						$auth_row = $row;
						//print_ar( $auth_row );
						$auth_row['non_cc'] = true;
						$main_row = $row;

						if( $auth_row['action'] == 'capture' )
						{

							$capture_row = $auth_row;
							$amount_captured += $capture_row['amount'];
							$capture_row['date_added'] = $auth_row['payment_date'];
						}
						else
						{
							$amount_authorized += $auth_row['amount'];
						}
					}

					//print_ar($row);

					$bgcolor = ($bgcolor == 'listing') ? 'listingb' : 'listing';
					echo "<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\" valign=top>

							<td align=center>" . db_date($auth_row['date_added']) . "</td>
							<td align=center>$auth_row[payment_method]" ;
							echo"</td>
							<td align=right>$auth_row[last4]</td>";
							if( $main_row['action'] == 'Sale' )
								echo'<td>Authorize & Capture</td>';
							else
								echo'<td>' . $main_row['action'] . '</td>';
							if( $auth_row['action'] == 'credit')
								$auth_row['amount'] = $auth_row['amount'] * -1;
							echo'<td>' . $auth_row['amount'] . '</td>';
							//echo'<td>' . db_date($capture_row['date_added'] ) . '</td>';
							echo'<td>' . $main_row['transaction_id'] . '</td>';
							echo'<td>' . $main_row['avs_address'] . $main_row['avs_zip'] .'</td>';
							echo'<td>' . $main_row['response_msg']  .'</td>';
						//	echo"<td align=left>" . Buttons::actionButtonLink("sales.php?action=edit_payment&id=$row[id]", 'EDIT') . "</td>
						//Need to determine action and put correct buttons here, Capture and Void or Credit
						if( $capture_row && !$auth_row['non_cc'] )
						{
							if( $capture_row['response_code'] == 0 && $capture_row['is_credited'] != 'Y')
								echo'<td>' . Buttons::actionButtonLink("", 'Credit','','','confirmCredit ('.$row[id].','.$main_row[id].', ' . $order_id . ')',true) .'</td>';
							else
								echo'<td></td>';
							echo'<td></td>';
						}
						else
						{
							//print_ar($_SESSION['current_order']);
							//print_ar($auth_row['id']);
							$captured = Payments::get(0,$order_id, 'capture', '','', $auth_row['id']);
							//print_ar( $captured );
							$invoice = Orders::makeInvoice($order_id, false, true);
							//echo 'Captured:';
							//print_ar($captured);
							//echo 'Auth Row:';
							//print_ar($auth_row);
							if(!$captured && $auth_row['payment_method']=='Paypal Checkout')
							{
								echo'<td>' . Buttons::actionButtonLink("sales.php?action=capture_paypal&row_id=$row[id]&payment_id=$main_row[id]&id=$order_id", 'Capture') .'</td>';
								echo'<td>' . Buttons::actionButtonLink("sales.php?action=void_authorization&row_id=$row[id]&payment_id=$main_row[id]&id=$order_id", 'Void') .'</td>';
							}
							elseif( !$captured && !$auth_row['non_cc'] )
							{
								echo'<td>' . Buttons::actionButtonLink("sales.php?action=capture_payment&row_id=$row[id]&payment_id=$main_row[id]&id=$order_id", 'Capture') .'</td>';
								echo'<td>' . Buttons::actionButtonLink("sales.php?action=void_authorization&row_id=$row[id]&payment_id=$main_row[id]&id=$order_id", 'Void') .'</td>';
							}
							elseif( $auth_row['non_cc'] )
							{
								echo'<td>' . Buttons::actionButtonLink("sales.php?action=edit_payment&payment_id=$main_row[id]&id=$order_id", 'Edit') .'</td>';
								echo'<td></td>';
							}
							else
								echo'<td></td><td></td>';
						}
						echo" </tr>";
					//$total_payments += $row['amount'];
					//$balance -= $row['amount'];
				}
				echo'<tr style="background: #fff">';
				echo'<td colspan=3></td><th>Total Authorized:</th>';
				echo'<td align=right><b>$'. number_format($amount_authorized, 2) . '</b></td>';

				echo'<td><b>Driver Notes:</b></td>';
				echo'<td colspan="4">'.$order[driver_notes].'</td></tr>';

				echo'<tr style="background: #fff">';
				echo'<td colspan=3></td><th>Total Captured:</th>';
				echo'<td align=right><b>$'. number_format($amount_captured, 2) . '</b></td></tr>';

				echo'<tr style="background: #fff">';
				echo'<td colspan=3></td><th>Remaining Balance:</th>';

				$balance = $balance - $amount_captured;

				if( $balance == 0 )
					$bg = 'white';
				else if( $balance > 0 )
					$bg = 'red';
				else
					$bg = 'green';

				echo'<td align=right style="background:' . $bg . '"><b>$'. number_format($balance, 2) . '</b></td>';

				echo'</tr>';


			}
			echo '</table>';
		}
		elseif( $brief )
		{
		//	echo'<table class="brief_payment">';
		//	echo'<tr><th>Type</th>';
		//	echo'<th>Last 4</th>';
		//	echo'<th>Action</th>';
		//	echo'<th>AVS</th>';
		//	echo'<th>Amount</th></tr>';
			if (is_array($payments))
			{
				echo'<div class="orders_payments">';
				echo'<table>';

				foreach( $payments as $payment )
				{
					echo'<tr>';
					echo'<td>' . $payment['payment_method'] . '</td>';
					echo'<td align=right>' . $payment['last4'] .'</td>';
					if( $payment['action'] == 'Sale' )
						echo'<td class="center">Authorize & Capture</td>';
					else
						echo'<td class="center">' . $payment['action'] . '</td>';

					echo '<td class="center">' . $payment['avs_address'] . $payment['avs_zip'] . '</td>';

					echo'<td class="right">' . $payment['amount'] . '</td>';
				}

				echo'</table>';
				echo'</div>';
				/*
				foreach($payments as $row) {
					unset( $auth_row );
					unset( $capture_row );
					unset( $main_row );
					if( $row['auth_id'] )
					{
						$auth_row = Payments::get1( $row['auth_id'] );
						if( $row['action'] == 'capture' )
							$capture_row = $row;

						$main_row = $capture_row;
						if( !$capture_row['response_code'] )
							$amount_captured += $capture_row['amount'];
					}
					elseif( $row['action'] == 'authorize' )
					{
						$auth_row = $row;
						$capture_row = "";
						$main_row = $auth_row;
						$amount_authorized += $auth_row['amount'];
						$captured = Payments::get(0,$_SESSION['current_order'], 'capture', '','', $auth_row['id'] );
						if( $captured )
							continue;
					}
					elseif( $row['action'] == 'sale' )
					{
						$auth_row = $row;
						$capture_row = $row;
						$main_row = $row;
						if( !$capture_row['response_code'] )
							$amount_captured += $capture_row['amount'];
					}
					else //Non CC payment
					{
						$auth_row = $row;
						$auth_row['non_cc'] = true;
						$main_row = $row;
						if( $auth_row['action'] == 'capture' );
						{
							$capture_row = $auth_row;
							$amount_captured += $capture_row['amount'];
							$capture_row['date_added'] = $auth_row['payment_date'];
						}
					}

					echo "<tr valign=top>
							<td align=center>$auth_row[payment_method]" ;
							echo"</td>
							<td align=right>$auth_row[last4]</td>";
							if( $main_row['action'] == 'Sale' )
								echo'<td>Authorize & Capture</td>';
							else
								echo'<td>' . $main_row['action'] . '</td>';

							echo'<td>' . $main_row['avs_address'] . $main_row['avs_zip'] . '</td>';
							echo'<td>' . $auth_row['amount'] . '</td>';

						echo" </tr>";
				*/

				}

			//	echo'</tr>';
			}
			//echo '</table>';
	}

	static function listSuppliers( $order_id, $brief = false )
	{
		if( $brief )
		{
			$po_numbs = Orders::getPO( 0, $order_id );

			if( $po_numbs )
			{
				echo'<div class="orders_suppliers">';
				echo '<table>';
				foreach( $po_numbs as $po )
				{
					if( $po['po_number'] == '' )
						continue;
					echo'<tr>';
					echo'<td>' . $po['name'] . '</td>';
					echo'<td class="center">' . $po['po_number'] . '</td>';
					echo'<td class="right">' . db_date($po['release_date'], 'm/d/y') . '</td>';
					echo'</tr>';
				}
				echo'</table>';
				echo'</div>';
			}
		}
	}

	static function listShippers( $order_id, $brief = false )
	{
		if( $brief )
		{
			$tracking_numbs = Orders::getTracking( 0, $order_id );
			if( $tracking_numbs )
			{
				echo'<div class="orders_shippers">';
				echo'<table>';
				foreach( $tracking_numbs as $tracking )
				{
					if( $tracking['tracking_number'] == '' )
						continue;
					echo'<tr>';
					echo'<td>' . $tracking['name'] . '</td>';
					echo'<td class="center">' . $tracking['tracking_number'] . '</td>';
					echo'<td class="right">' . db_date($tracking['ship_date'], 'm/d/y') . '</td>';
					echo'</tr>';
				}
				echo'</table>';
				echo'</div>';
			}
		}
	}


	static function updateShippedItems( $order_id, $item_id, $tracking_id, $qty )
	{
		global $CFG;
		if( !$qty )
			return false;
		$item = Orders::getItem( $item_id );

		Orders::updateItem( $item_id, array('qty_shipped' => $item['qty_shipped'] + $qty ) );
		$total_shipped = $item['qty_shipped'] + $qty;
		if( $total_shipped == $item['qty'] )
		{
			Orders::updateItem( $item_id, array('order_item_status_id'=> $CFG->fully_shipped_item ) );
		}
		else
		{
			Orders::updateItem( $item_id, array('order_item_status_id'=> $CFG->partially_shipped_item ) );
		}
		Orders::updateItemTracking( $tracking_id, $item_id, $qty );

		// Get Inventory
		$inv = db_get1( Inventory::get('','','','','',$item['product_id'],'','',$item['order_tax_option']) );

		// Update Inventory Only if a Row Exists AND the Store is TigerChef
		if($inv && $item['order_tax_option'] == $CFG->website_store_id){
			$newStock = $inv['qty'] - $qty;
			$newOnOrder = $inv['on_order'] - $qty;
			if($newStock < 0){
				$warning = "<h2>Warning! You are shipping more $item[product_name] than you currently have in stock! Going along with it...</h2>";
			}

			// Decrement Stock NOW
			$newInfo['qty'] = max(0,$newStock);
			$newInfo['on_order'] = max(0, $newOnOrder );
			Inventory::update($inv['id'],$newInfo);
		}

		return $warning;
	}

	static function updateItemTracking( $tracking_id, $item_id, $qty )
	{
		$sql  = " UPDATE order_items_tracking SET qty = $qty ";
		$sql .= " WHERE order_tracking_id = '$tracking_id' AND order_item_id = '$item_id' ";

		db_query( $sql );
		if( !db_affected_rows() )
		{
			$info['order_tracking_id'] = $tracking_id;
			$info['order_item_id'] 		 = $item_id;
			$info['qty']							 = $qty;
			return db_insert( 'order_items_tracking', $info );
		}
		else
			return true;
	}

	static function getItemsForTracking( $tracking_id )
	{
		if($tracking_id){
			return Orders::getItems( 0, 0, '', '', 0, '', '', 0, 0, 0, 0, $tracking_id );
		} else {
			return false;
		}
	}

	static function getOrderIdFromGoogleCheckoutOrderNumber($google_order_number)
	{
		$sql = "SELECT * from orders WHERE orders.3rd_party_order_number=$google_order_number";
		$result = db_query_array($sql);
		//$result = Orders::get( 0, 0, '', '', 0, '', '', 0, 0, 0, 0, '' , 0, '', '', '', '', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', 0, 0, 0, '', $google_order_number );
		return $result;
	}

	static function getByEbayID( $ebay_id )
	{
		return Orders::get(0,0,'','',0,'','','','','',0,'',0,'','','','','',0,'','',0,'','','','','',0,'','','','','','','','','',0,0,0,'','', $ebay_id );
	}

	static function checkFreightShipper($order_id = 0, $update_order = false){


		$order_id = (int) $order_id;
		if(!$order_id)
			return false;

		$type = 'UPS';
		$items = self::getItems('',$order_id);

		if(!empty($items)){
			foreach ($items as $item){
				$prod = Products::get1($item['product_id']);
				if($prod['shipping_type'] == 'Freight'){
					$type = 'Freight';
				}
			}
		}

		if(!$update_order){
			return $type;
		}
		else{
			if($type == 'Freight'){
				$order = self::get1($order_id);
				$zipcode = $order['shipping_zip'];

				if(!$zipcode){
					Orders::update($order_id,array('recommended_shipper'=>''));
					return array('error' =>"Shipping zipcode not set!");
				}

				$trucker_id = Truckers::getTruckerByZipcode($zipcode);

				if(!$trucker_id){
					Orders::update($order_id,array('recommended_shipper'=>''));
					return array('error' =>"No Trucker Found for Zipcode: ".$zipcode);
				}

				$trucker = Truckers::get1($trucker_id);

				$type = $trucker['trucker_code'];
			}

			Orders::update($order_id,array('recommended_shipper'=>$type));

			return $type;
		}
	}

	static function get1ByEmail($email=''){
		if(trim($email) != ''){
			$order = Orders::get(0,0,'','',0,'','','','',$email);
		}
		else{
			return false;
		}

		if(is_array($order) && !empty($order)){
			return true;
		}
		else{
			return false;
		}
	}

    static function getPurchasedItemsByCustomer( $cust_id )
    {

        $sql = " SELECT order_items.*, orders.date as order_date, products.name, products.vendor_sku
                        FROM order_items
                        LEFT JOIN orders on orders.id = order_items.order_id
                        LEFT JOIN products on products.id = order_items.product_id
                        WHERE 1 AND orders.customer_id = $cust_id ORDER BY order_items.order_id DESC, order_items.id ";

        //echo $sql;

        return db_query_array( $sql );
    }

    static function displayItems( $items )
    {
        $last_package_id = 0;

        $hth = new HtmlTableHeading($urlBegin);
        $hth->addItem('order_id','Order ID',($order=='order_id'),$order_asc);
        $hth->addItem('product_id','Product ID',($order=='product_id'),$order_asc);
        $hth->addItem('order_date','Date of Order',($order=='order_date'),$order_asc);
        $hth->addItem('name','Product',($order=='name'),$order_asc);
        $hth->addItem('vendor_sku','SKU',($order=='vendor_sku'),$order_asc);
        $hth->addItem('qty','QTY',($order=='state'),$order_asc);
        $hth->addItem('price','Price',($order=='num_orders'),$order_asc);


        echo '<table cellspacing=1 cellpadding=5 class=listing>
            <tr class=listing_heading>';

        $hth->show();
        $i= 0;
        foreach ($items as $order_line) {
            $bgcolor = ($bgcolor == 'listing') ? 'listingb' : 'listing';
    //      $i++;
    //      if( $i == 10 )
    //          break;
            //print_ar( $order_line );
            if( $order_line['order_package_id'] == 0 && $order_line['product_id'] == 0 && $order_line['gift_id'] > 0)
            {
                //This is a custom line, a gift certificate need to get cost from the gift details
                Orders::showCertLine( $order_line, $bg_color, $javascript_off );
                //print_ar( $order_line );

            }
            else
            {
                if ($order_line['order_package_id'] != $last_package_id && $order_line['order_package_id'] > 0) {

                    $package_line = Orders::getPackageLine($order_line['order_package_id']);
                    $package_line['line_total'] = $package_line['price'] * $package_line['qty'];
                    $last_package_id = $order_line['order_package_id'];
                    //echo "this is a primary package line<br>";
                    //print_ar( $package_line );
                    //Orders::showPackageMainLine($package_line,$bg_color);
                    echo"<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\">";
                    echo'<td align="center"><a target="_top" href="orders.php?action=view&id=' . $order_line['order_id'] . '">' . $order_line['order_id'] . '</td>';
                    echo'<td align="center"><a target="_top" href="packages.php?action=view&id=' . $package_line['package_id'] . '">' . $package_line['package_id'] . '</a></td>';
                    echo'<td align="center">' . db_date($order_line['order_date']) . '</td>';
                    echo'<td align="center">' . $package_line['package_name'] . '</td>';
                    echo'<td align="center"> - </td>';
                    echo'<td align="center">' . $package_line['qty'] . '</td>';
                    echo'<td align="center">$' . $package_line['line_total'] . '</td>';
                    echo'</tr>';
                }

                if ($order_line['order_package_id'] > 0) {
                    //Orders::showPackageLine($order_line,$bg_color, $javascript_off);
                    //print_ar( $order_line );
                    echo"<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\">";
                    echo'<td align="center"> - </td>';
                    echo'<td align="center"><a target="_top" href="products.php?action=view&id=' . $order_line['product_id'] . '">' . $order_line['product_id'] . '</a></td>';
                    echo'<td align="center"> - </td>';
                    echo'<td align="center">' . $order_line['name'] . '</td>';
                    echo'<td align="center">' . $order_line['vendor_sku'] . '</td>';
                    echo'<td align="center">' . $order_line['qty'] . '</td>';
                    echo'<td align="center"> - </td>';
                    echo'</tr>';
                }
                else {
                    $bg_color = !$bg_color;
                    //print_ar( $order_line );
                    //Orders::showProductLine($order_line, $bg_color, $javascript_off, true);
                    echo"<tr class=$bgcolor onmouseover=\"setPointer(this, 'listinghover')\" onmouseout=\"setPointer(this, '$bgcolor')\">";
                    echo'<td align="center"><a target="_top" href="orders.php?action=view&id=' . $order_line['order_id'] . '">' . $order_line['order_id'] . '</a></td>';
                    echo'<td align="center"><a target="_top" href="products.php?action=view&id=' . $order_line['product_id'] . '">' . $order_line['product_id'] . '</a></td>';
                    echo'<td align="center">' . db_date($order_line['order_date']) . '</td>';
                    echo'<td align="center">' . $order_line['name'] . '</td>';
                    echo'<td align="center">' . $order_line['vendor_sku'] . '</td>';
                    echo'<td align="center">' . $order_line['qty'] . '</td>';
                    if( $order_line['token_purchase'] == 'Y' )
                        echo'<td align="center">' . number_format($order_line['price'], 0) . ' Tokens</td>';
                    else
                        echo'<td align="center">$' . $order_line['price'] . '</td>';
                    echo'</tr>';
                }
            }
        }
        echo'</table>';
    }

    static function getTrackedItemQtys($order_id,$tracking_id='', $key=''){
    	$tracking_id = (int)$tracking_id;
    	$order_id = (int) $order_id;

    	$sql = " SELECT products.name as product_name, IF(oit.qty IS NULL, 0, oit.qty) as qty_tracked ";
    	$sql .= ", (order_items.qty - (SELECT SUM(qty) FROM order_items_tracking inner1 WHERE inner1.order_item_id = order_items.id )) as qty_untracked,
    			order_items.*
				  FROM order_items ";
		$sql .= " LEFT JOIN order_items_tracking oit ON oit.order_item_id = order_items.id AND
					oit.order_tracking_id = $tracking_id ";
		$sql .= " LEFT JOIN products ON products.id = order_items.product_id
				  WHERE
				  order_items.order_id = $order_id ";

    	if($tracking_id){
//			$sql .= " AND oit.order_tracking_id = $tracking_id ";
    	}

//    	echo "<p>$sql</p>";

    	return db_query_array($sql, $key);
    }

    /**
     * Use advanced heuristics to get the latest transaction id for a given order id.
     *
     * @param unknown_type $order_id
     */
    static function getLatestTransactionID($order_id){
    	$order_id = (int)$order_id;

    	$sql = "SELECT * FROM (
SELECT orders.transaction_id as order_tx, orders.date as tx_date
FROM orders
WHERE orders.id = '$order_id'
UNION
SELECT op.transaction_id, op.date_added
FROM order_payments op
WHERE op.order_id = '$order_id'
) uni1 ORDER BY tx_date DESC LIMIT 1";

    	$result = db_get1( db_query_array($sql) );

    	return $result['order_tx'];
    }

    /**
     * Send a tracking information e-mail to the customer, utilizing order_tracking table.
     *
     * @param int $row_id
     * @return bool
     */
    static function sendTrackingNotification($row_id){
    	global $CFG;

    	if(!$row_id){
    		return false;
    	}
    	$track_row = db_get1( Orders::getTracking($row_id) );
    	if(!$track_row){
    		return false;
    	}


    	$order = Orders::get1($track_row['order_id']);

		include_once('RB/email_notify.php');

		$info['comments'] = "<p>Your Order #$order[id] has shipped!  The tracking # is $track_row[tracking_number].</p>";
/*	// Removing Account Link because it doesn't belong in Amazon or eBay e-mails
<p>You can view more detailed order information by logging in to <a href='".Catalog::myAccount()."'>your account</a>.";
*/

		$info['baseurl'] = $CFG->baseurl;
		$info['order_prefix'] = $CFG->order_prefix;
		$info['order_id'] = $order['id'];
		$info['order_num'] = $order['id'];
		$info['order_status'] = $order['order_status_name'];
		$info['comments_html'] = nl2br($info['comments']);

		if ($CFG->in_testing) {
			$order['customer_email'] = 'mbutler@rustybrick.com';
		}

		if(!$order['id']){
			echo "<p>Invalid Order ID in the Tracking Record.</p>";
			return false;
		}

		if(!validate_email($order['customer_email'])){
			echo "<p>Invalid E-mail Address on Order $order[id]. Tried [$order[customer_email]]</p>";
			return false;
		}



		/* $notify = new EmailNotify($CFG->company_name,$CFG->company_email,
								  "$order[customer_first_name] $order[customer_last_name]",$order['customer_email'],
								  'Your Order','order_history.txt',$info,'order_history.html');
		$notify->send(); */

		$E = TigerEmail::sendOne("$order[customer_first_name] $order[customer_last_name]",$order['customer_email'],'order_history',$info);

		if($E){
			$uInfo['email_sent'] = "Y";
			Orders::updateTracking($track_row['id'],$uInfo);
		}

		return $E;

    }

    /**
     * Void a shipment, utilizing order_tracking table.
     *
     * @param int $row_id
     * @return bool
     */
    static function voidShipment($row_id){
    	global $CFG;

    	if(!$row_id){
    		return false;
    	}
    	$track_row = db_get1( Orders::getTracking($row_id) );
    	if(!$track_row){
    		return false;
    	}

    	require_once('RB/ups.php');
    	$ups = new UPS($CFG->ups_key,$CFG->ups_user,$CFG->ups_pass,$CFG->in_testing,0);

    	try{
    		if($track_row['multi_tracking']){
    			$tracking_num = explode(',',$track_row['multi_tracking']);
    		} else {
    			$tracking_num = $track_row['tracking_number'];
    		}
    		if($CFG->in_testing){
    			$ups->ShipVoid($tracking_num); //'1Z12345E0390817264');
    		} else {
    			$ups->ShipVoid($tracking_num);
    		}
    	}catch(Exception $e){
    		echo "<p>".$e->getMessage()."</p>";
    		return false;
    	}

    	db_delete("order_tracking",$track_row['id'],'id');
    	db_delete("order_items_tracking",$track_row['id'],'order_tracking_id');
    	return true;

    }

    static function getCCLast4ByOrderId($order_id){
    	$order_id = (int)$order_id;
    	if(!$order_id){
    		return '';
    	}
    	$sql = " SELECT * FROM order_payments
    		WHERE order_id = '$order_id' AND last4 <> ''
    		ORDER BY id DESC LIMIT 1 ";

    	$result = db_query_array($sql,'',true);

    	return $result['last4'];
    }

    static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT orders.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(orders.id)) as total ";
		}

		$from_ = " FROM orders ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND orders.id = [id] ";
		}

		if(is_array($o->date)){
			if($o->date['start']){
				$o->date_start = $o->date['start'];
				$where_ .= " AND `orders`.date >= [date_start]";
			}
			if($o->date['end']){
				$o->date_end = $o->date['end'];
				$where_ .= " AND `orders`.date <= [date_end]";
			}
		} else 		if($o->date != ''){
			$where_ .= " AND `orders`.date = [date]";
		}
		if($o->customer_id >  0 ){
			$where_ .= " AND `orders`.customer_id = [customer_id]";
		}
		if($o->billing_first_name != ''){
			$where_ .= " AND `orders`.billing_first_name = [billing_first_name]";
		}
		if($o->billing_last_name != ''){
			$where_ .= " AND `orders`.billing_last_name = [billing_last_name]";
		}
		if($o->billing_company != ''){
			$where_ .= " AND `orders`.billing_company = [billing_company]";
		}
		if($o->billing_address1 != ''){
			$where_ .= " AND `orders`.billing_address1 = [billing_address1]";
		}
		if($o->billing_address2 != ''){
			$where_ .= " AND `orders`.billing_address2 = [billing_address2]";
		}
		if($o->billing_city != ''){
			$where_ .= " AND `orders`.billing_city = [billing_city]";
		}
		if($o->billing_state != ''){
			$where_ .= " AND `orders`.billing_state = [billing_state]";
		}
		if($o->billing_zip != ''){
			$where_ .= " AND `orders`.billing_zip = [billing_zip]";
		}
		if($o->billing_phone != ''){
			$where_ .= " AND `orders`.billing_phone = [billing_phone]";
		}
		if($o->shipping_first_name != ''){
			$where_ .= " AND `orders`.shipping_first_name = [shipping_first_name]";
		}
		if($o->shipping_last_name != ''){
			$where_ .= " AND `orders`.shipping_last_name = [shipping_last_name]";
		}
		if($o->shipping_company != ''){
			$where_ .= " AND `orders`.shipping_company = [shipping_company]";
		}
		if($o->shipping_address1 != ''){
			$where_ .= " AND `orders`.shipping_address1 = [shipping_address1]";
		}
		if($o->shipping_address2 != ''){
			$where_ .= " AND `orders`.shipping_address2 = [shipping_address2]";
		}
		if($o->shipping_city != ''){
			$where_ .= " AND `orders`.shipping_city = [shipping_city]";
		}
		if($o->shipping_state != ''){
			$where_ .= " AND `orders`.shipping_state = [shipping_state]";
		}
		if($o->shipping_zip != ''){
			$where_ .= " AND `orders`.shipping_zip = [shipping_zip]";
		}
		if($o->shipping_phone != ''){
			$where_ .= " AND `orders`.shipping_phone = [shipping_phone]";
		}
		if($o->shipping_phone2 != ''){
			$where_ .= " AND `orders`.shipping_phone2 = [shipping_phone2]";
		}
		if($o->shipping_phone3 != ''){
			$where_ .= " AND `orders`.shipping_phone3 = [shipping_phone3]";
		}
		if($o->transaction_id != ''){
			$where_ .= " AND `orders`.transaction_id = [transaction_id]";
		}
		if($o->avs_address != ''){
			$where_ .= " AND `orders`.avs_address = [avs_address]";
		}
		if($o->avs_zip != ''){
			$where_ .= " AND `orders`.avs_zip = [avs_zip]";
		}
		if($o->cvv_match != ''){
			$where_ .= " AND `orders`.cvv_match = [cvv_match]";
		}
		if($o->iavs != ''){
			$where_ .= " AND `orders`.iavs = [iavs]";
		}
		if($o->ip_address != ''){
			$where_ .= " AND `orders`.ip_address = [ip_address]";
		}
		if($o->cookie_info != ''){
			$where_ .= " AND `orders`.cookie_info = [cookie_info]";
		}
		if($o->user_agent != ''){
			$where_ .= " AND `orders`.user_agent = [user_agent]";
		}
		if($o->session_id != ''){
			$where_ .= " AND `orders`.session_id = [session_id]";
		}
		if($o->subtotal >  0 ){
			$where_ .= " AND `orders`.subtotal = [subtotal]";
		}
		if($o->promo_discount >  0 ){
			$where_ .= " AND `orders`.promo_discount = [promo_discount]";
		}
		if($o->tax_rate >  0 ){
			$where_ .= " AND `orders`.tax_rate = [tax_rate]";
		}
		if($o->is_repeat_order != ''){
			$where_ .= " AND `orders`.is_repeat_order = [is_repeat_order]";
		}
		if($o->shipping >  0 ){
			$where_ .= " AND `orders`.shipping = [shipping]";
		}
		if($o->location_type != ''){
			$where_ .= " AND `orders`.location_type = [location_type]";
		}
		if($o->shipping_id >  0 ){
			$where_ .= " AND `orders`.shipping_id = [shipping_id]";
		}
		if($o->shipping_charged != ''){
			$where_ .= " AND `orders`.shipping_charged = [shipping_charged]";
		}
		if($o->current_status_id >  0 ){
			$where_ .= " AND `orders`.current_status_id = [current_status_id]";
		}
		if($o->list_mgr_id >  0 ){
			$where_ .= " AND `orders`.list_mgr_id = [list_mgr_id]";
		}
		if($o->card_type != ''){
			$where_ .= " AND `orders`.card_type = [card_type]";
		}
		if($o->last4 != ''){
			$where_ .= " AND `orders`.last4 = [last4]";
		}
		if($o->sales_id >  0 ){
			$where_ .= " AND `orders`.sales_id = [sales_id]";
		}
		if($o->driver_notes != ''){
			$where_ .= " AND `orders`.driver_notes = [driver_notes]";
		}
		if($o->notes != ''){
			$where_ .= " AND `orders`.notes = [notes]";
		}
		if($o->order_tax_option >  0 ){
			$where_ .= " AND `orders`.order_tax_option = [order_tax_option]";
		}
		if($o->po_numb != ''){
			$where_ .= " AND `orders`.po_numb = [po_numb]";
		}
		if($o->amount >  0 ){
			$where_ .= " AND `orders`.amount = [amount]";
		}
		if($o->check_numb >  0 ){
			$where_ .= " AND `orders`.check_numb = [check_numb]";
		}
		if($o->payment_type != ''){
			$where_ .= " AND `orders`.payment_type = [payment_type]";
		}
		if(is_array($o->release_date)){
			if($o->release_date['start']){
				$o->release_date_start = $o->release_date['start'];
				$where_ .= " AND `orders`.release_date >= [release_date_start]";
			}
			if($o->release_date['end']){
				$o->release_date_end = $o->release_date['end'];
				$where_ .= " AND `orders`.release_date <= [release_date_end]";
			}
		} else 		if($o->release_date != ''){
			$where_ .= " AND `orders`.release_date = [release_date]";
		}
		if($o->damage != ''){
			$where_ .= " AND `orders`.damage = [damage]";
		}
		if($o->delivery_issue != ''){
			$where_ .= " AND `orders`.delivery_issue = [delivery_issue]";
		}
		if($o->past_damage != ''){
			$where_ .= " AND `orders`.past_damage = [past_damage]";
		}
		if($o->coming_back != ''){
			$where_ .= " AND `orders`.coming_back = [coming_back]";
		}
		if($o->file_claim_ups != ''){
			$where_ .= " AND `orders`.file_claim_ups = [file_claim_ups]";
		}
		if($o->file_claim_frt != ''){
			$where_ .= " AND `orders`.file_claim_frt = [file_claim_frt]";
		}
		if($o->review != ''){
			$where_ .= " AND `orders`.review = [review]";
		}
		if(is_array($o->damage_date)){
			if($o->damage_date['start']){
				$o->damage_date_start = $o->damage_date['start'];
				$where_ .= " AND `orders`.damage_date >= [damage_date_start]";
			}
			if($o->damage_date['end']){
				$o->damage_date_end = $o->damage_date['end'];
				$where_ .= " AND `orders`.damage_date <= [damage_date_end]";
			}
		} else 		if($o->damage_date != ''){
			$where_ .= " AND `orders`.damage_date = [damage_date]";
		}
		if(is_array($o->delivery_issue_date)){
			if($o->delivery_issue_date['start']){
				$o->delivery_issue_date_start = $o->delivery_issue_date['start'];
				$where_ .= " AND `orders`.delivery_issue_date >= [delivery_issue_date_start]";
			}
			if($o->delivery_issue_date['end']){
				$o->delivery_issue_date_end = $o->delivery_issue_date['end'];
				$where_ .= " AND `orders`.delivery_issue_date <= [delivery_issue_date_end]";
			}
		} else 		if($o->delivery_issue_date != ''){
			$where_ .= " AND `orders`.delivery_issue_date = [delivery_issue_date]";
		}
		if(is_array($o->past_damage_date)){
			if($o->past_damage_date['start']){
				$o->past_damage_date_start = $o->past_damage_date['start'];
				$where_ .= " AND `orders`.past_damage_date >= [past_damage_date_start]";
			}
			if($o->past_damage_date['end']){
				$o->past_damage_date_end = $o->past_damage_date['end'];
				$where_ .= " AND `orders`.past_damage_date <= [past_damage_date_end]";
			}
		} else 		if($o->past_damage_date != ''){
			$where_ .= " AND `orders`.past_damage_date = [past_damage_date]";
		}
		if(is_array($o->coming_back_date)){
			if($o->coming_back_date['start']){
				$o->coming_back_date_start = $o->coming_back_date['start'];
				$where_ .= " AND `orders`.coming_back_date >= [coming_back_date_start]";
			}
			if($o->coming_back_date['end']){
				$o->coming_back_date_end = $o->coming_back_date['end'];
				$where_ .= " AND `orders`.coming_back_date <= [coming_back_date_end]";
			}
		} else 		if($o->coming_back_date != ''){
			$where_ .= " AND `orders`.coming_back_date = [coming_back_date]";
		}
		if(is_array($o->file_claim_ups_date)){
			if($o->file_claim_ups_date['start']){
				$o->file_claim_ups_date_start = $o->file_claim_ups_date['start'];
				$where_ .= " AND `orders`.file_claim_ups_date >= [file_claim_ups_date_start]";
			}
			if($o->file_claim_ups_date['end']){
				$o->file_claim_ups_date_end = $o->file_claim_ups_date['end'];
				$where_ .= " AND `orders`.file_claim_ups_date <= [file_claim_ups_date_end]";
			}
		} else 		if($o->file_claim_ups_date != ''){
			$where_ .= " AND `orders`.file_claim_ups_date = [file_claim_ups_date]";
		}
		if(is_array($o->file_claim_frt_date)){
			if($o->file_claim_frt_date['start']){
				$o->file_claim_frt_date_start = $o->file_claim_frt_date['start'];
				$where_ .= " AND `orders`.file_claim_frt_date >= [file_claim_frt_date_start]";
			}
			if($o->file_claim_frt_date['end']){
				$o->file_claim_frt_date_end = $o->file_claim_frt_date['end'];
				$where_ .= " AND `orders`.file_claim_frt_date <= [file_claim_frt_date_end]";
			}
		} else 		if($o->file_claim_frt_date != ''){
			$where_ .= " AND `orders`.file_claim_frt_date = [file_claim_frt_date]";
		}
		if(is_array($o->review_date)){
			if($o->review_date['start']){
				$o->review_date_start = $o->review_date['start'];
				$where_ .= " AND `orders`.review_date >= [review_date_start]";
			}
			if($o->review_date['end']){
				$o->review_date_end = $o->review_date['end'];
				$where_ .= " AND `orders`.review_date <= [review_date_end]";
			}
		} else 		if($o->review_date != ''){
			$where_ .= " AND `orders`.review_date = [review_date]";
		}
		if($o->sales_account_id >  0 ){
			$where_ .= " AND `orders`.sales_account_id = [sales_account_id]";
		}
		if($o->reference_number != ''){
			$where_ .= " AND `orders`.reference_number = [reference_number]";
		}
		if(is_array($o->override_date)){
			if($o->override_date['start']){
				$o->override_date_start = $o->override_date['start'];
				$where_ .= " AND `orders`.override_date >= [override_date_start]";
			}
			if($o->override_date['end']){
				$o->override_date_end = $o->override_date['end'];
				$where_ .= " AND `orders`.override_date <= [override_date_end]";
			}
		} else 		if($o->override_date != ''){
			$where_ .= " AND `orders`.override_date = [override_date]";
		}
		if($o->site_name != ''){
			$where_ .= " AND `orders`.site_name = [site_name]";
		}
		if($o->title != ''){
			$where_ .= " AND `orders`.title = [title]";
		}
		if($o->ebay_order_id != ''){
			$where_ .= " AND `orders`.ebay_order_id = [ebay_order_id]";
		}
		if($o->recommended_shipper != ''){
			$where_ .= " AND `orders`.recommended_shipper = [recommended_shipper]";
		}
		if($o->gift_wrapped != ''){
			$where_ .= " AND `orders`.gift_wrapped = [gift_wrapped]";
		}
		if($o->gift_wrap_message != ''){
			$where_ .= " AND `orders`.gift_wrap_message = [gift_wrap_message]";
		}
		if($o->cim_profile_id != ''){
			$where_ .= " AND `orders`.cim_profile_id = [cim_profile_id]";
		}
		if($o->amazon_order_id >  0 ){
			$where_ .= " AND `orders`.amazon_order_id = [amazon_order_id]";
		}

		if( $o->amazon_ship_confirm != '' )
		{
		    $where_ .= " AND `orders`.amazon_ship_confirm = [amazon_ship_confirm]";
		}

		if( $o->google_checkout_order_number != '' )
		{
		    $where_ .= " AND `orders`.google_checkout_order_number = [google_checkout_order_number]";
		}

		if( $o->review_email_sent != '' )
		{
		    $where_ .= " AND `orders`.review_email_sent = [review_email_sent]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY orders.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having. $orderby_. $limit_;
                //print_ar($sql);
		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function assignItemToPO($item_id,$po_id, $item_qty){

		$item_id = (int)$item_id;
		if($item_id){
			$sql = " SELECT * FROM purchase_order_items WHERE item_id = $item_id ";
			$row = db_query_array($sql,'',true);
			if($row['cust_each']){
				$info['cust_each'] = $row['cust_each'];
			} else {
				$info['cust_each'] = '-1';
			}

			db_delete("purchase_order_items",$item_id,'item_id');

			if($po_id){
				$info['po_id']   = $po_id;
				$info['item_id'] = $item_id;
				$info['item_qty'] = $item_qty;				
				db_insert("purchase_order_items",$info);
			}
		}

	}

	static function isNotWebsiteOrder($row_or_id){
		global $CFG;

		if(is_array($row_or_id)){
			$row = $row_or_id;
		} else {
			$row = Orders::get1((int)$row_or_id);
		}
		if(!$row){
			return false;
		}

		return ($row['order_tax_option'] != $CFG->website_store_id || $row['payment_type'] == "cba");
	}

	static function getItemsForPO($po_id){

		$sql = "SELECT cats.id AS cat_id,
		               cats.name AS cat_name,
		               orders.*,
		               order_items.*,";
		if($po_id > 0){
			$po_id = (int)$po_id;
			$sql .= " SUM(poi.item_qty) as sum_qty,
				(order_items.price * poi.item_qty) AS item_total,";
		}
		
		 else $sql .= "SUM(order_items.qty) as sum_qty,
		 		(order_items.price * order_items.qty) AS item_total,";
		 
		 $sql .= "
		         	   products.name AS product_name,
					   products.vendor_sku AS product_vendor_sku,
					   products.weight,
					   products.free_shipping,
   					   order_item_statuses.name AS order_item_status_name,
   					   brands.name as brand_name,
   					   products.description,
   					   products.local_warehouse,
   					   order_items_tracking.qty as tracking_qty,
   					   product_options.upc as product_upc,
   					   poi.po_id as po_id,
   					   IF(purchase_order.code <> '', purchase_order.code, poi.po_id ) AS po_number,
   					   SUM(poi.cust_each) AS cust_each,
   					   inventory.qty AS in_stock

				FROM
		        order_items
		        LEFT JOIN order_item_options ON order_items.id = order_item_options.order_item_id
		        LEFT JOIN product_options    ON product_options.id = order_item_options.product_option_id
				LEFT JOIN products           ON products.id = order_items.product_id
		        LEFT JOIN product_cats       ON product_cats.product_id = products.id
		        LEFT JOIN cats               ON product_cats.cat_id = cats.id
            	LEFT JOIN orders ON (order_items.order_id = orders.id)
            	LEFT JOIN customers ON (customers.id = orders.customer_id)
		 		LEFT JOIN order_item_statuses ON (order_item_statuses.id = order_items.order_item_status_id)
		 		LEFT JOIN brands on brands.id = products.brand_id
		 		LEFT JOIN order_items_tracking on order_items_tracking.order_item_id = order_items.id
				LEFT JOIN purchase_order_items AS poi ON poi.item_id = order_items.id
				LEFT JOIN purchase_order     ON purchase_order.id = poi.po_id
				LEFT JOIN inventory          ON inventory.product_id = products.id

				WHERE 1 ";


		if($po_id > 0){
			$po_id = (int)$po_id;
			$sql .= " AND poi.po_id = '$po_id' ";
		}

		$sql .= " GROUP BY order_items.id
				  ORDER BY ";

		if ($order == '') {
			$sql .= 'order_items.id';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

//		echo "<p>$sql</p>";

		$arr = db_query_array($sql);

		return $arr;

		if (is_array($arr))
		{
	       foreach ($arr as $key => $value)
	       {

	       	 // DO NOT ADD IN OPTION PRICING.  IT'S ALREADY ADDED!!!!

	       	 $q = "SELECT *
	       	       FROM
	       	       order_item_options
	       	       LEFT JOIN product_options
	       	         ON order_item_options.product_option_id = product_options.id
	       	       LEFT JOIN options
	       	         ON options.id = product_options.option_id
	       	       WHERE order_item_options.order_item_id = ".$value[id];

		       	 	if($value[id]){
		       	      $options = db_query_array($q);
		       	 	}

	       	 	  if (is_array($options)&&count($options))
	       	 	  {
				    $arr[$key]['options'] = $options;
				    $arr[$key]['orig_price'] = $cart[$key]['price'];
				    $arr[$key]['product_vendor_sku'] = $options[0]['vendor_sku'];

				    $arr[$key]['product_name'] .= " - " . $options[0]['value'];
				    /*

				    foreach ($options as $option)
				    {

				      if ($option['additional_price'] > 0)
				      {
				      	print_ar($option);
				        echo "adding: $option[additional_price] to arr[{$key}][total_price]}";

				      }
					  $arr[$key]['price'] += $option['additional_price'];
					  echo "price = {$arr[$key][price]}";
					  $arr[$key]['total_price'] += $option['additional_price'] * $arr[$key]['qty'];
				    }

				    */

				  }
	       }
	       return $arr;

		}
		else
		  return null;


	}
	
	static function updateWarning( $id, $warning )
	{
	    $order = self::get1( $id );
	    $cur_warning = $order['warning_txt'];

	    $info['has_warning'] = 'Y';
	    $info['warning_txt'] = $cur_warning . $warning;

	    return self::update( $id, $info );
	}

	static function getProfit($order_id, $cfg_obj){		
		
		$order_id = (int)$order_id;

		$sql = <<<QUERY
			SELECT
			orders.promo_discount,
			SUM(oi.price * oi.qty) as revenue,
			SUM(products.vendor_price * oi.qty) as cost,
			(SUM((products.vendor_price + products.fba_pick_and_pack_fee + products.fba_weight_fee + products.fba_storage_fee + fba_inbound_shipping_cost)* oi.qty) + products.fba_order_handling_fee) as fba_cost,
			SUM(oi.price * oi.qty - products.vendor_price * oi.qty) as simple_profit,
			SUM(oi.price * oi.qty * orders.tax_rate/100) as sales_tax,
			orders.shipping,
			orders.order_tax_option,
			orders.payment_type,
			IF(
					   (orders.subtotal + orders.min_charges - orders.promo_discount - orders.shipping_discount
						+ orders.shipping +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount + orders.shipping - orders.shipping_discount + orders.min_charges) ) )
					   >= 0, (orders.subtotal + orders.min_charges - orders.promo_discount - orders.shipping_discount
						+ orders.shipping +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount + orders.shipping - orders.shipping_discount + orders.min_charges))) , 0
				   )

			AS order_total,
			orders.card_type
			FROM order_items oi
			LEFT JOIN orders ON oi.order_id = orders.id
			LEFT JOIN products ON products.id = oi.product_id
			WHERE 1
			AND oi.order_id = '$order_id'
QUERY;

				$sql2 = <<<QUERY2
			SELECT
			SUM(ot.shipping_cost) as shipping_cost
			FROM orders LEFT JOIN order_tracking ot ON ot.order_id = orders.id
			WHERE 1
			AND orders.id = '$order_id'
QUERY2;

				$sql3 = <<<QUERY3
			SELECT
			(SUM(qty) -1) as sum_mr_qty_after_first
			FROM order_items LEFT JOIN product_suppliers ps 
			ON order_items.product_id = ps.product_id
			WHERE 1
			AND order_items.order_id = '$order_id'
			AND ps.brand_id = $cfg_obj->m_rothman_supplier_id
QUERY3;
		$result = db_query_array($sql,'',true);
		$result2 = db_query_array($sql2,'',true);
		$result3 = db_query_array($sql3,'',true);
		
		if ($result3['sum_mr_qty_after_first'] > 0)
		{
			$result['cost'] -= ($cfg_obj->m_rothman_fee * $result3['sum_mr_qty_after_first']); 
			$result['fba_cost'] -= ($cfg_obj->m_rothman_fee * $result3['sum_mr_qty_after_first']);
		}
		
		// RSunness added this section Jan '12
		$eligible_for_commission = $result['shipping'] + $result['revenue'];
		$commission_paid = 0;
		
		if ($result['order_tax_option'] == $cfg_obj->website_store_id)
		{
			if ($result['payment_type'] == "cba")
			{
				if ($result['order_total'] >= 10)
				{
					$commission_paid = $cfg_obj->cba_per_trans_10_or_more + ($result['order_total'] * $cfg_obj->cba_comm_10_or_more);					
				}
				else // < $10
				{
					$commission_paid = $cfg_obj->cba_per_trans_under_10 + ($result['order_total'] * $cfg_obj->cba_comm_under_10);					
				}
			}
			else 
			{				
				if ($result['card_type'] == "amex") $commission_paid = $result['order_total'] * $cfg_obj->cc_proc_rate_amex;
				else $commission_paid = $result['order_total'] * $cfg_obj->cc_proc_rate_non_amex;
			}
		}
		else if (in_array($result['order_tax_option'],$cfg_obj->ebayStoreIds))
		{
			$first_fifty_amount = $fifty_to_thousand_amount = $over_thousand_amount = 0;
			if ($eligible_for_commission < 50) $first_fifty_amount = $eligible_for_commission;
			else 
			{
				$first_fifty_amount = 50;
				if ($eligible_for_commission < 1000) $fifty_to_thousand_amount = $eligible_for_commission - 50;
				else 
				{
					$fifty_to_thousand_amount = 950;
					$over_thousand_amount = $eligible_for_commission - 1000;
				}
			}
			$commission_paid = ($cfg_obj->ebay_first_fifty_commission * $first_fifty_amount) +
							   ($cfg_obj->ebay_fifty_to_thousand_commission * $fifty_to_thousand_amount) +
							   ($cfg_obj->ebay_over_thousand_commission * $over_thousand_amount);				
		}
		else if ($result['order_tax_option'] == $cfg_obj->amazon_store_id)
		{
			$commission_paid = $eligible_for_commission * $cfg_obj->amazon_commission;
		}
		else if ($result['order_tax_option'] == $cfg_obj->buy_dot_com_store_id)
		{
			$commission_paid = $eligible_for_commission * $cfg_obj->buy_dot_com_commission;
		}
		else if ($result['order_tax_option'] == $cfg_obj->newegg_store_id)
		{
			$commission_paid = $eligible_for_commission * $cfg_obj->newegg_commission;
		}	
		else if ($result['order_tax_option'] == $cfg_obj->amazon_fba_store_id)
		{
			$eligible_for_commission = $result['revenue']; // on FBA, we don't make money on the shipping
			$commission_paid = $eligible_for_commission * $cfg_obj->amazon_commission;
		}			
		else  // anything else (shouldn't be anything else, but...) 
		{
		}

		if (!$result2['shipping_cost']) $result2['shipping_cost'] = "0.00";		
	
		if ($result['order_tax_option'] == $cfg_obj->amazon_fba_store_id) 
		{
			$profit_breakdown_string = "<font color='green'>$eligible_for_commission [item";
			$profit_breakdown_string .=  " pd] - $commission_paid [comm/ccfees]- $result[fba_cost] [item cost incl. fba fees] - $result[promo_discount] [discount] - $result2[shipping_cost] [shipping cost]</font>";
			$profit = round(($eligible_for_commission - $commission_paid - $result['fba_cost'] - $result['promo_discount'] - $result2['shipping_cost']),2);
		}
		else 
		{
			$profit_breakdown_string = "<font color='green'>$eligible_for_commission [item";
			$profit_breakdown_string .= "+shipping";
			$profit_breakdown_string .=  " pd] - $commission_paid [comm/ccfees]- $result[cost] [item cost] - $result[promo_discount] [discount] - $result2[shipping_cost] [shipping cost]</font>";
			$profit = round(($eligible_for_commission - $commission_paid - $result['cost'] - $result['promo_discount'] - $result2['shipping_cost']),2);
		}
		$profit_array = array();
		$profit_array[0] = $profit;
		$profit_array[1] = $profit_breakdown_string;
		unset($cfg_obj);
		return $profit_array;

	}
	
    /**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getTrackingByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT order_tracking.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(order_tracking.id)) as total ";
		}

		$from_ = " FROM order_tracking ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND order_tracking.id = [id] ";
		}

		if( isset( $o->order_id )  ){
			$where_ .= " AND `order_tracking`.order_id = [order_id]";
		}
		if( isset( $o->tracking_number )  ){
			$where_ .= " AND `order_tracking`.tracking_number = [tracking_number]";
		}
		if( isset( $o->shipper_id )  ){
			$where_ .= " AND `order_tracking`.shipper_id = [shipper_id]";
		}
		if(is_array($o->ship_date)){
			if($o->ship_date['start']){
				$o->ship_date_start = $o->ship_date['start'];
				$where_ .= " AND `order_tracking`.ship_date >= [ship_date_start]";
			}
			if($o->ship_date['end']){
				$o->ship_date_end = $o->ship_date['end'];
				$where_ .= " AND `order_tracking`.ship_date <= [ship_date_end]";
			}
		} else 		if( isset( $o->ship_date )  ){
			$where_ .= " AND `order_tracking`.ship_date = [ship_date]";
		}
		if( isset( $o->shipping_cost )  ){
			$where_ .= " AND `order_tracking`.shipping_cost = [shipping_cost]";
		}
		if( isset( $o->ship_from_id )  ){
			$where_ .= " AND `order_tracking`.ship_from_id = [ship_from_id]";
		}
		if( isset( $o->email_sent )  ){
			$where_ .= " AND `order_tracking`.email_sent = [email_sent]";
		}
		if( isset( $o->multi_tracking )  ){
			$where_ .= " AND `order_tracking`.multi_tracking = [multi_tracking]";
		}

		if( isset( $o->shipped_barcode )  ){
			$where_ .= " AND `order_tracking`.shipped_barcode = [shipped_barcode]";
		}



		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY order_tracking.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1TrackingByO( $o )
	{
	    $result = self::getTrackingByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function getUnshippedItems( $o_id )
	{
	    global $CFG;

	    $unshipped_statuses = $CFG->unshipped_item_statuses;

	    $o = (object) array();
	    $o->order_id = $o_id;
	    $o->order_item_status_string = $unshipped_statuses;
	    
	    return self::getOrderItemByO( $o );
	}



	/**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getOrderItemByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT order_items.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(order_items.id)) as total ";
		}

		$from_ = " FROM order_items ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND order_items.id = [id] ";
		}

		if( isset( $o->order_id )  ){
			$where_ .= " AND `order_items`.order_id = [order_id]";
		}
		if( isset( $o->product_id )  ){
			$where_ .= " AND `order_items`.product_id = [product_id]";
		}
		if( isset( $o->price )  ){
			$where_ .= " AND `order_items`.price = [price]";
		}
		if( isset( $o->qty )  ){
			$where_ .= " AND `order_items`.qty = [qty]";
		}
		if( isset( $o->order_package_id )  ){
			$where_ .= " AND `order_items`.order_package_id = [order_package_id]";
		}
		if( isset( $o->item_status_requested )  ){
			$where_ .= " AND `order_items`.item_status_requested = [item_status_requested]";
		}
		if( isset( $o->item_status_updated )  ){
			$where_ .= " AND `order_items`.item_status_updated = [item_status_updated]";
		}
		if(is_array($o->date_item_status_updated)){
			if($o->date_item_status_updated['start']){
				$o->date_item_status_updated_start = $o->date_item_status_updated['start'];
				$where_ .= " AND `order_items`.date_item_status_updated >= [date_item_status_updated_start]";
			}
			if($o->date_item_status_updated['end']){
				$o->date_item_status_updated_end = $o->date_item_status_updated['end'];
				$where_ .= " AND `order_items`.date_item_status_updated <= [date_item_status_updated_end]";
			}
		} else 		if( isset( $o->date_item_status_updated )  ){
			$where_ .= " AND `order_items`.date_item_status_updated = [date_item_status_updated]";
		}

		if( isset( $o->order_item_status_string ) )
		{
		    $where_ .= " AND `order_items`.order_item_status_id IN ([order_item_status_string])";
		}

		if( isset( $o->order_item_status_id )  ){
			$where_ .= " AND `order_items`.order_item_status_id = [order_item_status_id]";
		}
		if( isset( $o->order_item_status_notes )  ){
			$where_ .= " AND `order_items`.order_item_status_notes = [order_item_status_notes]";
		}
		if( isset( $o->discount_type )  ){
			$where_ .= " AND `order_items`.discount_type = [discount_type]";
		}
		if( isset( $o->discount_amt )  ){
			$where_ .= " AND `order_items`.discount_amt = [discount_amt]";
		}
		if( isset( $o->qty_shipped )  ){
			$where_ .= " AND `order_items`.qty_shipped = [qty_shipped]";
		}
		if( isset( $o->gift_wrapping_id )  ){
			$where_ .= " AND `order_items`.gift_wrapping_id = [gift_wrapping_id]";
		}
		if( isset( $o->delivery_method )  ){
			$where_ .= " AND `order_items`.delivery_method = [delivery_method]";
		}
		if( isset( $o->amazon_gift_wrap )  ){
			$where_ .= " AND `order_items`.amazon_gift_wrap = [amazon_gift_wrap]";
		}
		if( isset( $o->gift_msg )  ){
			$where_ .= " AND `order_items`.gift_msg = [gift_msg]";
		}
		if( isset( $o->charged )  ){
			$where_ .= " AND `order_items`.charged = [charged]";
		}
		if( isset( $o->order_item_id )  ){
			$where_ .= " AND `order_items`.order_item_id = [order_item_id]";
		}
		if( isset( $o->ebay_id )  ){
			$where_ .= " AND `order_items`.ebay_id = [ebay_id]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY order_items.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1OrderItemByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	/**
	 * Double check that an order item belongs to an order
	 */
	static function validateOrderItemWithOrder($order_item_id, $order_id){

		$order_item_id = (int)$order_item_id;
		$order_id      = (int)$order_id;

		$sql = " SELECT id, order_id FROM order_items ";
		$sql .= " WHERE id = '$order_item_id' AND order_id = '$order_id' ";

		$result = db_query_array($sql);
		return is_array($result);
		
	}
	
	static function getNotOnPO($order_id,$include_on_winco_po=false)
	{
		global $CFG;
		
		$sql = "SELECT order_items.product_id, product_option_id, ";
		if ($include_on_winco_po) $sql .= "if( purchase_order.supplier_id IN ('33')
		AND purchase_order.code LIKE '%-5%', order_items.qty, ";
		
		$sql .= "IF( poi.item_qty, order_items.qty - poi.item_qty, order_items.qty )";
		
		if ($include_on_winco_po) $sql .= ")"; 
		 
		$sql .= "AS qty_open, products.vendor_sku,product_options.vendor_sku AS option_vendor_sku,
				order_items.id as oi_id, poi.item_qty
				FROM order_items 
				LEFT JOIN purchase_order_items poi ON order_items.id = poi.item_id ";
		
		if ($include_on_winco_po) $sql .= "LEFT JOIN purchase_order ON poi.po_id = purchase_order.id ";
		
		$sql .= "LEFT JOIN order_item_options oio ON order_items.id = oio.order_item_id
				LEFT JOIN products ON order_items.product_id = products.id
				LEFT JOIN product_options ON product_options.id = oio.product_option_id  
				WHERE order_items.order_id = '$order_id'
				AND (poi.item_qty IS NULL OR (order_items.qty - poi.item_qty) > 0 ";
		
		if ($include_on_winco_po) $sql .= " OR (purchase_order.supplier_id 
				IN " . $CFG->po_pickup_vendors ." AND purchase_order.code LIKE '%".$CFG->winco_po_pattern."%')" ;
		$sql .= ")";
		// echo $sql;
		$recs = db_query_array($sql);
		return $recs;	
	}
	
	static function didEveryItemShip($order_id)
	{
				$sql = "SELECT order_items.product_id, product_option_id,
				IF(oit.qty, order_items.qty - oit.qty, order_items.qty) 
				AS qty_open, products.vendor_sku,product_options.vendor_sku AS option_vendor_sku,
				order_items.id as oi_id
				FROM order_items 
				LEFT JOIN order_items_tracking oit ON order_items.id = oit.order_item_id 
				LEFT JOIN order_item_options oio ON order_items.id = oio.order_item_id
				LEFT JOIN products ON order_items.product_id = products.id
				LEFT JOIN product_options ON product_options.id = oio.product_option_id  
				WHERE order_items.order_id = '$order_id'
				AND (oit.qty IS NULL OR (order_items.qty - oit.qty) > 0)";

	//	echo $sql; 
		$recs = db_query_array($sql);
		if (count($recs) > 0) return false;
		else return true;	
	}
	
	static function getPosFromThisSupplier($order_id, $supplier_id)
	{
		global $CFG;
		
		$sql = "SELECT order_items.product_id, product_option_id, ";
			 
		$sql .= "products.vendor_sku,product_options.vendor_sku AS option_vendor_sku,
				order_items.id as oi_id, poi.item_qty
				FROM order_items 
				LEFT JOIN purchase_order_items poi ON order_items.id = poi.item_id ";
		
		$sql .= "LEFT JOIN purchase_order on poi.po_id = purchase_order.id
				LEFT JOIN order_item_options oio ON order_items.id = oio.order_item_id
				LEFT JOIN products ON order_items.product_id = products.id
				LEFT JOIN product_options ON product_options.id = oio.product_option_id  
				WHERE order_items.order_id = '$order_id'
				AND  supplier_id = '$supplier_id'";
		
		//echo $sql;
		$recs = db_query_array($sql);
		return $recs;		
	}
	
  function makeMyAccountOrderDetailsDisplay($oid)
  {
	global $CFG;

    $orders          = Orders::get($oid,$_SESSION['cust_acc_id']);
    if (is_array($orders))
    {
    	$order = $orders[0];
    }
    else 
    {
    	echo "Invalid order";
    	exit;
    }
    $ostatus        = OrderStatuses::get1($order['current_status_id']);
	$customer       = Customers::get1($order['customer_id']);
	$orderitems     = Orders::getItems(0,$oid);
	$trackingnums 	= Orders::getTracking(0, $oid);
    ?>
    <form action="<?=$CFG->sslurl . $_SERVER['PHP_SELF'] . "?action=quick_reorder&order_id=$oid"?>"" method="POST">
    <input type="submit" name="Quick Re-order" value="Quick Re-order" class="qr_submit"> <small> Items will be added to your cart. You will be able to change quantities and edit order information.</small><br />
        <div class="quick_reorder">
        <div class="qr_order_info"><span>Order #:</span> <?=$CFG->order_prefix.$order[id]?><br/>
		<span>Order Date:</span> <?=db_date($order[date],'F j, Y')?><br/>
		<span>Status:</span> <?=$ostatus['name']?><br />
   
        <span>Order Total:</span> $<?=number_format($order['order_total'], 2)?><br/>
        <span>Shipping Method:</span> <?= $order['shipping_method']?><br/>
        <?
			if (is_array($trackingnums))			
			{	
				?>
				<span>Tracking Number(s):</span>
				<?
				$counter = 0;
				foreach ($trackingnums as $this_tracking_num)
				{
					if ($counter > 1) echo "<br/>";
					echo "<a target=_blank href='".str_replace('[tracking_number]',$this_tracking_num['tracking_number'],$this_tracking_num['tracking_url'])."'>".$this_tracking_num['tracking_number']."</a>"	;
					$counter++;		
				}
			}
        ?>
      </div>
      
      <div class="qr_billto"><span>Bill To:</span><br /><br />
      <?=$order['billing_first_name']?> <?=$order['billing_last_name'] . "<br/>\n"?>
	<?
	if( $order['billing_company'] ) {
    	echo $order['billing_company'] . "<br/>\n";
	}
	?>
	<?=$order['billing_address1'] . "<br/>\n"?>
	<?=($order['billing_address2']) ? $order['billing_address2'] . "<br/>\n" : ''?>
	<?=$order['billing_city']?>, <?=$order['billing_state']?> <?=$order['billing_zip'] . "<br/>\n"?>
	Phone: <?=$order['billing_phone'] . "<br/>\n"?>
      </div>
      <div class="qr_shipto"><span>Ship To:</span><br/><br />
	<?=$order['shipping_first_name']?> <?=$order['shipping_last_name'] . "<br/>\n"?>
	<?
	if( $order['shipping_company'] ) {
	    echo $order['shipping_company'] . "<br/>\n";
	}
	?>
	<?=$order['shipping_address1'] . "<br/>\n"?>
	<?=($order['shipping_address2']) ? $order['shipping_address2'] . "<br/>\n" : ''?>
	<?=$order['shipping_city']?>, <?=$order['shipping_state']?> <?=$order['shipping_zip'] . "<br/>\n"?>
	
    Phone: <?=$order['shipping_phone'] . "<br/>\n"?>
        
</div>   <div class="qr_items">
        <? 
        $items = $orderitems;
        $show_title = true;
    	$this_option = $this_product = $prev_option = $prev_product = $prev_cart_item = $prev_b_type = "";
     	$b_reg_function = "Orders::showOrderDetailsDisplayLineBReg";
 		$b_case_function = "Orders::showOrderDetailsDisplayLineBCase";
 		$a_function = "Orders::showOrderDetailsDisplayLineA";        		
		
 		
 		include "/var/www/vhosts/tigerchef.com/htdocs/includes/case_cart_include.php";
        
?>
        </div>
        </div>
        <input type="submit" name="Quick Re-order" value="Quick Re-order" class="qr_submit"> <small> Items will be added to your cart. You will be able to change quantities and edit order information.</small>        
  	</form>	 
  <?	
	}
	
	static function showOrderDetailsDisplayLineA($this_item, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

		$product              = Products::get1($this_item['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];
//        $product_unit_cost    = $product['price'];
        $quantity             = $this_item['qty'];
        $price = $product_unit_cost = $this_item['price'];
        $item_total           = $price * $quantity;

	 	if( $this_item['options'] )
	 	{
	    	$option = $this_item['options'][0];
	    	$product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
	  	}
       	
       	$name = $product_name;
        $name = preg_replace('/[^A-z0-9]/s','-',$name);
  		$path = $CFG->baseurl . 'itempics/' . $name . '-' . $this_item['product_id'] . $CFG->thumbnail_suffix;
  		$path_on_server = $CFG->dirroot . '/itempics/' . $name . '-' . $this_item['product_id'] . $CFG->thumbnail_suffix;
  		if (!file_exists($path_on_server)) $path =$CFG->baseurl . 'images/imagecomingsoon_t.jpg'; 
      	?>
        	<ul>
        	<li><a href='<?=Catalog::makeProductLink_($product)?>'  target='qr_window'><img class="qr_img" src="<?= $path?>" alt="<?=$product_name?>"></a></li>

        <? 
    }
    static function showOrderDetailsDisplayLineBReg($this_item, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

		$product              = Products::get1($this_item['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];
//        $product_unit_cost    = $product['price'];
        $quantity             = $this_item['qty'];
        $price = $product_unit_cost = $this_item['price'];
        $item_total           = $price * $quantity;

	 	if( $this_item['options'] )
	 	{
	    	$option = $this_item['options'][0];
	    	$product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
	  	}
         
         ?>
            <li class="qr_qty"><?=$quantity?></li>
        	<li class="qr_desc"><a href='<?=Catalog::makeProductLink_($product)?>' target='qr_window'><?=$product_name . " (" . $product_sku . ")"?></a></li>
        	<li class="qr_price">$<?=number_format($price, 2)?> each</li>      
        	</ul>
     <? 
    }
    static function showOrderDetailsDisplayLineBCase($prev_cart_line, $cart_line)
    {
        global $CFG;

		$product              = Products::get1($prev_cart_line['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];

	 	if( $prev_cart_line['options'] )
	 	{
	    	$option = $prev_cart_line['options'][0];
	    	$product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
	  	}
        
         ?>
            <li class="qr_qty"><?=($cart_line['qty'] + $prev_cart_line['qty'])?></li>
			<li class="qr_desc"><a href='<?=Catalog::makeProductLink_($product)?>' target='qr_window'><?=$product_name . " (" . $product_sku . ")"?></a></li>
        	<li class="qr_price"><?=$cart_line['qty'] . " @ $" . number_format($cart_line['order_items_price'], 2) ." ea. <br>" .  $prev_cart_line['qty'] . " @ $" . number_format($prev_cart_line['order_items_price'],2) . " ea."?></li>      
        	</ul>
        <?
    }
	function showInvoiceLineA($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
        global $CFG;

        $product              = Products::get1($cart_line['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];
        //$product_unit_cost    = $product['price'];
        $quantity             = $cart_line['qty'];
        $price  = $product_unit_cost = $cart_line['price'];
        $item_total           = $price * $quantity;

		  if( $cart_line['options'] )
		  {
		    $option = $cart_line['options'][0];

		    $product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
		  }
		          ?>
        		  <tr class="item-row">
        		      <td class="item-name"><div class="delete-wpr"><textarea readonly="readonly"><?=$product_sku?></textarea></div></td>
        		      <td class="description"><textarea readonly="readonly"><?=$product_name?></textarea></td>        		   
		          <?        
    }
    function showInvoiceLineBReg($cart_line, $show_title_box = false, $color_bg = false, $updateable = true)
    {
    	global $CFG;

        $product              = Products::get1($cart_line['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];
        //$product_unit_cost    = $product['price'];
        $quantity             = $cart_line['qty'];
        $price  = $product_unit_cost = $cart_line['price'];
        $item_total           = $price * $quantity;

		  if( $cart_line['options'] )
		  {
		    $option = $cart_line['options'][0];

		    $product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
		  }
		          ?>
    	
 <td><textarea class="invoice_cost" readonly="readonly">$<?=number_format($price, 2)?></textarea></td>
        		      <td><textarea class="invoice_qty" readonly="readonly"><?=$quantity?></textarea></td>
        		      <td><span class="invoice_price">$<?=number_format($item_total, 2)?></span></td>
        		  </tr>
<?         		      
    }
    function showInvoiceLineBCase($prev_cart_line, $cart_line)
    {
	global $CFG;

        $product              = Products::get1($cart_line['product_id']);
        $product_sku          = $product['vendor_sku'];
        $product_name         = $product['name'];
        //$product_unit_cost    = $product['price'];
        $quantity             = $cart_line['qty'];
        $price  = $product_unit_cost = $cart_line['price'];
        $item_total           = $price * $quantity;

		  if( $cart_line['options'] )
		  {
		    $option = $cart_line['options'][0];

		    $product_sku = $option['vendor_sku'] . ' - ' . $option['value'];
		  }
		          ?>
    	
 <td><textarea class="invoice_cost" readonly="readonly"><?=$cart_line['qty'] . " @ $" . number_format($cart_line['price'], 2) ."\n" .  $prev_cart_line['qty'] . " @ $" . number_format($prev_cart_line['price'],2)?></textarea></td>
        		      <td><textarea class="invoice_qty" readonly="readonly"><?=($cart_line['qty'] + $prev_cart_line['qty'])?></textarea></td>
        		      <td><span class="invoice_price">$<?=number_format(($cart_line['item_total'] + $prev_cart_line['item_total']),2)?></span></td>
        		  </tr>
<?         		      
    }

static function getEstShipDateForGoogleTrustedStores($order_id)
    {
    	$max_days = Orders::getMaxShipsInDays($order_id);
    	$skipdates = HolidayDates::getAllIntoArray();
    	$max_ships_in_date = Orders::addDays(time(), $max_days, array("Saturday","Sunday"), $skipdates);
    	
    	return $max_ships_in_date;
    }
    static function getMaxShipsInDays($order_id)
    {
    	$order_items = Orders::getItems(0, $order_id);
    	if ($order_items)
    	{
    		$max_lead_days = 0;
    		$lead_days = 0;
    		foreach ($order_items as $order_item)
    		{
    			$lead_time_whole = $order_item['ships_in'];
    			// since ships_in is in format 1-2 days or 2-3 weeks, need to split it up to get number of days
    			$lead_time_part = explode("-", $lead_time_whole);
    			$lead_time_split = explode(" ", $lead_time_part[1]);
    			$lead_num = $lead_time_split[0];
    			$lead_unit = $lead_time_split[1];
    			if (strtoupper($lead_unit) == "WEEKS") $unit_multiplier = 5; // cuz 1 week = 5 business days
    			else $unit_multiplier = 1;
    			$lead_days = $lead_num * $unit_multiplier;
    			if ($lead_days > $max_lead_days) $max_lead_days = $lead_days;
    			echo "lead_days is $lead_days for $lead_time_whole";
    		}    		
    	}    	
    	echo "$max_lead_days is max_lead_days"; 
    	return $max_lead_days; 
    }
    
    static function addDays($timestamp, $days, $skipdays=NULL, $skipdates=NULL){ 
   	 	// $skipdays: array (Monday-Sunday) eg. array("Saturday","Sunday") 
   		// $skipdates: array (YYYY-mm-dd) eg. array("2012-05-02","2015-08-01"); 
    	$i = 1; 
    	if (date('H') >= 12)
    	{
    		$timestamp = strtotime("+12 hour" ,$timestamp);
    		if(in_array(date("l",$timestamp), $skipdays) || in_array(date("Y-m-d",$timestamp), $skipdates)){ 
            	$days++; 
    		}
    	}
    	while($days >= $i){ 
        	$timestamp = strtotime("+1 day" ,$timestamp); 
        	if(in_array(date("l",$timestamp), $skipdays) || in_array(date("Y-m-d",$timestamp), $skipdates)){ 
            	$days++; 
        	} 
        	$i++; 
    	} 
    
    	return date('Y-m-d',$timestamp); 
} 
}
?>