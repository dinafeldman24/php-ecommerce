<?php

class TigerEmail extends SendEmail{

	public static $store_emails = true;
	public $template_name = '';

	// send an e-mail from company, using HTML file template and info array.
	// returns Email object on success sending, false on failure of sending
	static function sendOne($to_name,$to_email,$template_name,$vars='',$subject='',$stripHTMLforText=true,
                                $from_name='', $from_email='', $new_acct_pwd='', $bcc_email = '', $reply_to='', $cc_email = '', $preset_text = '', $attachment = '') {
		global $CFG;
 				
		$to_email = trim($to_email);
		$to_name = trim($to_name);
		$to_name = trim(str_replace(",", "", $to_name));
		$to_name = trim(str_replace(":", "", $to_name));
		$to_name = trim(str_replace("/", "", $to_name));
		$to_name = trim(str_replace('"', "", $to_name));
		if(!$to_email){
			return;
		}

		if($from_email){
			$auto_set_from = false;
			if(!$from_name){
				$from_name = '';
			}
		} else {
			$auto_set_from = true;
			$from_name = $CFG->company_name;
			$from_email = $CFG->company_email;
		}

		if($vars['order_id']){
			$order1   = Orders::get1($vars['order_id']);
			$store_id = ( $order1['order_tax_option'] ? $order1['order_tax_option'] : 1 );
		} /*else*/
		 if($vars['store_id']){
			$store_id = $vars['store_id']; 
		}
		$template = EmailTemplate::getByName($template_name,$store_id);
		if(!$subject){
			$subject = $template['subject'];
		}

		$E = new TigerEmail();
		$E->setFrom($from_name,$from_email);
		$E->setTo($to_name,$to_email);
		$E->setSubject($subject);
		if ($bcc_email) $E->setBcc($bcc_email); 
		if ($cc_email) $E->setCc($cc_email);
		if ($reply_to) $E->setReplyTo($reply_to);
		if ($preset_text) $html_body = nl2br($preset_text);		
		else $html_body = $template['html'];
		
		if($attachment != '' && is_array($attachment))
		{
			foreach ($attachment as $att)
			{
				$E->addFileAttachment($att['file_path'], $att['email_file_name'], $att['mime_type']);	
			}
		}
		

		if(!$stripHTMLforText){
			$E->setTextBody($txt_body);
		}
		
		$E->setHTMLBody($html_body);

		// Build Vars
		$vars = TigerEmail::setupVars($vars, $new_acct_pwd);

		// Set From/To Name based on Store
		if($vars['order_id'] > 0){
			$theOrder = Orders::get1((int)$vars['order_id']);
			if($vars['store_id']) $theStore = Store::get1($vars['store_id']);
			else $theStore = Store::get1($theOrder['order_tax_option'] ? $theOrder['order_tax_option'] : 1);
			if($auto_set_from){
				$E->setFrom($theStore['name'],$theStore['service_email']);
			}
		}

		$E->populateTemplate($vars,true);

		$html_body = self::convertRelativeLinks($E->_html_body);
		$E->setHTMLBody( $html_body );
		
		if($CFG->in_testing){
			//return $E;

			$E->setSubject( "TESTING: " . $E->_subject );
		}

		$result = $E->send($stripHTMLforText);

		if($result){
			if(self::$store_emails){
				$E->template_name = $template_name;
				$E->store();
			}
			return $E;
		}

		return false;
	}

	static function setupVars($vars="", $new_acct_pwd){
		global $CFG;

		$vars['baseurl'] = $CFG->baseurl;
		$vars['base_url'] = $CFG->baseurl;
		$vars['global_header'] = TigerEmail::getHeader();
		$vars['global_footer'] = TigerEmail::getFooter();
		$vars['tigerchef_global_header'] = TigerEmail::getHeader();
		$vars['tigerchef_global_footer'] = TigerEmail::getFooter();

		if($vars['order_id']){
			$vars = self::buildOrderFields($vars['order_id'],$vars);
		}

        $vars['new_acct_pwd'] = '';
        
        if ($new_acct_pwd) {
            $vars['new_acct_pwd'] = "<br/><span style='font-size:120%;'>A new account has been created for you.<br/>To log in, please use your email address as your user name, and <strong>$new_acct_pwd</strong> as your password.</span>";
        }

		return $vars;
	}

	static function getHeader($store_id=1){
		$tmp = EmailTemplate::getByName("global_header",$store_id);
		if(!$tmp){
			$tmp = EmailTemplate::getByName("global_header",1);
		}

		return (string) $tmp['html'];
	}
	static function getFooter($store_id=1){
		$tmp = EmailTemplate::getByName("global_footer",$store_id);
		if(!$tmp){
			$tmp = EmailTemplate::getByName("global_footer",1);
		}
		return (string) $tmp['html'];
	}
	
	public function store(){
		$email['to'] = $this->_to_name . " <" . $this->_to_email . ">";
		$email['from'] = $this->_from_name . " <" . $this->_from_email . ">";
		$email['subject'] = $this->_subject;
		$email['body'] = $this->_html_body . "\n\n" . $this->_text_body;
		$email['template_name'] = $this->template_name;

		return TigerEmail::insert($email);
	}

	static function insert($info){
		return db_insert("email",$info,'date_sent');
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT email.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(email.id)) as total ";
		}

		$from_ = " FROM email ";

		$where_ = " WHERE 1 ";

		if($o->id > 0){
			$where_ .= " AND email.id = [id] ";
		}

		if($o->to){
			$where_ .= " AND email.to LIKE '%".db_esc($o->to)."%' ";
		}
		if($o->from){
			$where_ .= " AND email.from LIKE '%".db_esc($o->from)."%' ";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order) . "";
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY email.id ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	/* Plain text for e-mail */
	static function buildOrderFields($order_id,$vars){
		global $CFG;

		$order1 = Orders::get1($order_id);
		if(!$order1){
			return $vars;
		}
        if($order1['foreign_currency_code'] && $order1['foreign_currency_code'] != $CFG->default_localization['currency_code']){
            $ig_object = new IGlobalLocalization();
            $localization = $ig_object->get_localization($order1['foreign_currency_code']);
            //we are going to display the price that is already in the foreign currency, so change conversion to 1
            $localization['conversion_rate'] = 1;
            $order['subtotal'] = $order1['items_total_foreign'];
            $order['promo_discount'] = $order1['promo_discount_foreign'];
            $order['reward_points_used'] = $order1['reward_points_used_foreign'];
            $order['shipping'] = $order1['shipping_total_foreign'];
            $order['tax_rate'] = $order1['duty_taxes_total_foreign'];
            $order['order_total'] = $order1['grand_total_foreign'];
        }else{
            $localization = $CFG->default_localization;
        }
		
		$vars['first_name'] = $order1['billing_first_name'];
		$vars['last_name'] = $order1['billing_last_name'];
		//$vars['full_name'] = $vars['first_name'] . " " . $vars['last_name'];
		$vars['customer_name'] = $order1['billing_first_name'] . " " . $order1['billing_last_name'];
		$vars['order_date'] = db_date($order1['date'],'m/d/Y h:i A');
		$vars['order_date_simple'] = db_date($order1['date'],'m/d/Y');
		$vars['shipping_method'] = $order1['shipping_method'];
		$vars['order_num'] = $order1['id'];
		$vars['store'] = $order1['store_name'];
		$vars['amazon_order_id'] = $order1['amazon_order_id'];
		$vars['order_store_id'] = $order1['order_tax_option'];
		$vars['store_address_box'] = $order1['store_address_box'];
		
		if($order1['shipping_method'] && $order1['shipping_method_2']){
			$vars['shipping_method'] .= " / " . $order1['shipping_method_2'];
		}

		if($order1['payment_type'] == 'paypal'){
			$vars['billing_address'] = "Paid with paypal";
		}else{
			$vars['billing_address'] =
				$order1['billing_first_name'] . ' ' . $order1['billing_last_name'] . "<br />" .
				( $order1['billing_company'] ? ( $order1['billing_company'] . '<br />' ) : '' ) .
				( $order1['billing_address1'] ? ( $order1['billing_address1'] . '<br />' ) : '' ) .
				( $order1['billing_address2'] ? ( $order1['billing_address2'] . '<br />' ) : '' ) .
				( $order1['billing_city'] . ', ' . $order1['billing_state'] . ' ' . $order1['billing_zip'] . '<br />' ) .
                ( $order1['billing_country'] ? ( $order1['billing_country'] . '<br />' ) : '' ) .
				( $order1['billing_phone'] ? ( FixPhone::displayPhone($order1['billing_phone']) . '<br />' ) : '' ) .
				( $order1['customer_email'] ? ( $order1['customer_email'] . '<br />' ) : '' );
		}

		$vars['shipping_address'] =
			$order1['shipping_first_name'] . ' ' . $order1['shipping_last_name'] . "<br />" .
			( $order1['shipping_company'] ? ( $order1['shipping_company'] . '<br />' ) : '' ) .
			( $order1['shipping_address1'] ? ( $order1['shipping_address1'] . '<br />' ) : '' ) .
			( $order1['shipping_address2'] ? ( $order1['shipping_address2'] . '<br />' ) : '' ) .
			( $order1['shipping_city'] . ', ' . $order1['shipping_state'] . ' ' . $order1['shipping_zip'] . '<br />' ) .
            ( $order1['shipping_country'] ? ( $order1['shipping_country'] . '<br />' ) : '' ) .
            ( $order1['shipping_phone'] ? ( FixPhone::displayPhone($order1['shipping_phone']) . '<br />' ) : '' );
		

		if ($vars['order_item_id'])
		{			 
			$one_item_info = Orders::getItems($vars['order_item_id']);
			$vars['restock_date'] = db_date($one_item_info[0]['restock_date'],'m/d/Y');
			$vars['item_name'] = $one_item_info[0]['product_name'];
		}
		if($vars['tracking_id']){
			$items = Orders::getItemsForTracking($vars['tracking_id']);
		}else{
			$items = Orders::getItems('',$order_id);
		}
		if ($CFG->site_name == "lionsdeal")
		{
		$items_str = '<table width="600" border="0" cellspacing="0" cellpadding="15" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
		<tr style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  text-align:left; color:#78daf7; margin-left:-15px; margin-right:-15px;">	
			<td>Item Name</td><td></td>
			<td>Qty</td><td></td>
			<td>Price</td><td></td>
			<td>Total</td>
		</tr>';
		$subtotal = 0.00;
		$prev_name = "";
		if($items)foreach($items as $item){
			$items_str .= "
				<tr>
				<td>";
			$product_info = Products::get1($item['product_id']);
			$plink=Catalog::makeProductLink_($product_info);
			if ($item[product_name] != $prev_name || is_array($item['options']))
			{ 
				if ($item['is_active'] == 'Y' && $item['is_deleted'] == 'N') $items_str .= "<a href='".$plink."' style='color: #000000;'>";
				$items_str .= "<strong>" . $item['product_name'] . "</strong>";
                if(is_array($item['options'])){
                    $items_str .= "<span style='display:block;'>" . ProductOptionCache::getDescriptionForOptions($item['options'], $item['product_id']) . "</span>";
                }
				if ($item['is_active'] == 'Y' && $item['is_deleted'] == 'N') $items_str .= "</a>";
			}
			else $items_str .= "&nbsp;";	
				
			
			if(!$vars['tracking_id']){
				//Code for ships in and restock date
				if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
					$items_str .= "<br><span style='font-size: 11px; color: #f0af53;'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
				}
				$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
				if($ships_in){
					$items_str .= "<br><span style='font-size: 11px; color: #39a15e;'>";
					if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
						$items_str .= "Once in stock this item ships in ";
					}else{
						$items_str .= "Ships in ";
					}
					$items_str .= $ships_in . "</span>";
				}
				
				//code for freight
				if($order1['shipping_method'] && $order1['shipping_method_2']){
					$items_str .= "<br><span style='font-size: 11px; color: #617e8a;'>";
					if($item['item_shipping_type'] == 'Freight'){
						$items_str .= "Ships freight";
					}else{
						$items_str .= "Ships via standard carrier";	
					}
					$items_str .= "</span>";
				}
			}
				
			$items_str .= "</td><td></td>
				<td>$item[qty]</td><td></td>
				<td>".number_format($item['price'],2)."</td><td></td>
				<td>".number_format($item['item_total'],2)."</td>
				</tr>
			";
			$subtotal += $item['item_total'];
			$prev_name = $item['product_name'];
		}
		$items_str .= "</table>";
		
		$charges_str = '<table width="500" border="0" cellspacing="0" cellpadding="2" style="margin-left:-2px;">
			<tr>
				<td width="73">Order Subtotal: </td>
				<td width="115">$&nbsp;'.number_format($subtotal,2).'</td>
			</tr>
			';

		if($order1['min_charges'] > 0){
			$charges_str .= "<tr>".
			"<td>".
			"Minimum Surcharge:".
			"</td>".
			"<td>\$&nbsp;".
			number_format($order1['min_charges'],2).
			"</td>".
			"</tr>";
		}		
		
		$order1['promo_discount'] += $order1['shipping_discount'];
		if($order1['promo_discount'] > 0){
			$charges_str .= "<tr>".
			"<td><strong>Discount:</strong>".
			"</td>".
			"<td>-\$&nbsp;".
			number_format($order1['promo_discount'],2).
			"</td>";
			$promos = Orders::getPromoCodes ( $order_id );
			if ($promos)
			{
				$counter = 0;
				$charges_str .= "<td>";
				foreach($promos as $one_promo)
				{
					$counter++;
					if ($counter > 1) $charges_str .= ", ";
					$charges_str .= $one_promo['promo_code_code'];
				}
				$charges_str .= "</td>";			
			}
			$charges_str .= "</tr>";
		}
		
		if($order1['reward_points_used'] > 0){
			$charges_str .= "<tr>".
			"<td><strong>Reward Points Redeemed:</strong>".
			"</td>".
			"<td>-\$&nbsp;".
			number_format($order1['reward_points_used'],2).
			"</td>".
			"</tr>";
		}
		
		if($order1['liftgate_fee'] > 0){
			$charges_str .= "<tr>".
					"<td>".
					"Liftgate Fee:".
					"</td>".
					"<td>\$&nbsp;".
					number_format($order1['liftgate_fee'],2).
					"</td>".
					"</tr>";
		}
		
		$charges_str .= "<tr>
				<td>Shipping:</td>
				<td>\$&nbsp;".number_format($order1['shipping'],2)."</td>
			</tr>
			<tr>
				<td>Tax:</td>
				<td>\$&nbsp;".number_format($order1['order_tax'], 2)."</td>
			</tr> ";
		
		$charges_str .= "
			<tr>
				<td><strong>Order Total:</strong></td>
				<td><strong>\$&nbsp;".number_format($order1['order_total'],2)."</strong></td>
			</tr>
		";

		$charges_str .= "</table>";
		}
		else 
		{$items_str = '<table width="600" border="0" cellspacing="0" cellpadding="15" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
		<tr style="font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  text-align:left; background-color:#ececec; color:#000; margin-left:-15px; margin-right:-15px;">	
			<td>Item Name</td><td></td>
			<td>Quantity</td><td></td>
			<td>Price Each</td><td></td>
			<td>Total</td>
		</tr>';
		$subtotal = 0.00;
		$prev_name = "";
		
		if($items)foreach($items as $item){
            //if(is_object($ig_object)){
                //$item['price'] = (float)$item['foreign_price'];
                $item['item_total'] = $item['qty'] * $item['price'];//* (float)$item['foreign_price'] ;
          //  }

            $items_str .= "
				<tr>
				<td>";
			$product_info = Products::get1($item['product_id']);
			$plink=Catalog::makeProductLink_($product_info);
			if ($item[product_name] != $prev_name || is_array($item['options']))
			{ 
				if ($item['is_active'] == 'Y' && $item['is_deleted'] == 'N') $items_str .= "<a href='".$plink."' style='color: #000000;'>";
				$items_str .= "<strong>" . $item['product_name'] . "</strong>";
                if(is_array($item['options'])){
                    $items_str .= "<span style='display:block;'>" . ProductOptionCache::getDescriptionForOptions($item['options'], $item['product_id']) . "</span>";
                }
				if ($item['is_active'] == 'Y' && $item['is_deleted'] == 'N') $items_str .= "</a>";
			}
			else $items_str .= "&nbsp;";	
				
			
			if(!$vars['tracking_id']){
				//Code for ships in and restock date
				if(strtotime($product_info['restock_date']) > strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
					$items_str .= "<br><span style='font-size: 11px; color: #f0af53;'>Due in stock " . date('F d, Y', strtotime($product_info['restock_date'])) . "</span>";
				}
				$ships_in = ShipsIn::longProcessingTime($product_info['ships_in']);
				if($ships_in){
					$items_str .= "<br><span style='font-size: 11px; color: #39a15e;'>";
					if(strtotime($product_info['restock_date'])> strtotime(date("Y-m-d")) && $product_info['restock_date_show_on_website'] == 'Y'){
						$items_str .= "Once in stock this item ships in ";
					}else{
						$items_str .= "Ships in ";
					}
					$items_str .= $ships_in . "</span>";
				}
				
				//code for freight
				if($order1['shipping_method'] && $order1['shipping_method_2']){
					$items_str .= "<br><span style='font-size: 11px; color: #617e8a;'>";
					if($item['item_shipping_type'] == 'Freight'){
						$items_str .= "Ships freight";
					}else{
						$items_str .= "Ships via standard carrier";	
					}
					$items_str .= "</span>";
				}
			}
				
			$items_str .= "</td><td></td>
				<td>$item[qty]</td><td></td>
				<td>$".number_format($item['price'],2)."</td><td></td>
				<td>$".number_format($item['item_total'],2)."</td>
				</tr>
			";
			$subtotal += $item['item_total'];
			$prev_name = $item['product_name'];
		}
		$items_str .= "</table>";
		
		$charges_str = '<table width="500" border="0" cellspacing="0" cellpadding="2" style="margin-left:-2px;">
			<tr>
				<td width="73">Order Subtotal: </td>
				<td width="115">$'.number_format($subtotal,2).'</td>
			</tr>
			';

		if($order1['min_charges'] > 0){
			$charges_str .= "<tr>".
			"<td>".
			"Minimum Surcharge:".
			"</td>".
			"<td>$".
                number_format($order1['min_charges'],2).
			"</td>".
			"</tr>";
		}		
		
		$order1['promo_discount'] += $order1['shipping_discount'];
		if($order1['promo_discount'] > 0){
			$charges_str .= "<tr>".
			"<td><strong>Discount:</strong>".
			"</td>".
			"<td>-$".
                number_format($order1['promo_discount'],2).
			"</td>";
			$promos = Orders::getPromoCodes ( $order_id );
			if ($promos)
			{
				$counter = 0;
				$charges_str .= "<td>";
				foreach($promos as $one_promo)
				{
					$counter++;
					if ($counter > 1) $charges_str .= ", ";
					$charges_str .= $one_promo['promo_code_code'];
				}
				$charges_str .= "</td>";			
			}
			$charges_str .= "</tr>";
		}
		
		
		if($order1['liftgate_fee'] > 0){
			$charges_str .= "<tr>".
					"<td>".
					"Liftgate Fee:".
					"</td>".
					"<td>".
                $order1['liftgate_fee'].
					"</td>".
					"</tr>";
		}
		
		$charges_str .= "<tr>
				<td>Shipping:</td>
				<td>$".number_format($order1['shipping'],2)."</td>
			</tr>
			<tr>
				<td>Tax:</td>
				<td>$".number_format($order1['order_tax'],2)."</td>
			</tr> ";
		
		$charges_str .= "
			<tr>
				<td><strong>Order Total:</strong></td>
				<td><strong>$".number_format($order1['order_total'],2)."</strong></td>
			</tr>
		";
		$charges_str .= "</table>";
			
		
		}
		$vars['order_items'] = $items_str;
		$vars['order_charges'] = $charges_str;
		
//		if(in_array( $order['order_tax_option'],)

		if ($vars['this_is_template']) $store = Store::get1( $order1['order_tax_option'] ? $order1['order_tax_option'] : 1 );
		else if($vars['store_id']) $store = Store::get1($vars['store_id']);
		else $store = Store::get1( $order1['order_tax_option'] ? $order1['order_tax_option'] : 1 );
		$vars['global_header'] = TigerEmail::getHeader($store['id']);
		$vars['global_footer'] = TigerEmail::getFooter($store['id']);
		$vars['store_name'] = $store['name'];

		if(trim($order1['ebay_order_id']) != ''){
			$vars['global_header'] .= "<p>eBay #: ".$order1['ebay_order_id'] . "</p>\r\n";
		} else if(trim($order1['amazon_order_id']) != ''){
			$vars['global_header'] .= "<p>Amazon #: ".$order1['amazon_order_id'] . "</p>\r\n";
		} else if(trim($order1['google_checkout_order_number']) != ''){
			$vars['global_header'] .= "<p>Google Checkout #: ".$order1['google_checkout_order_number'] . "</p>\r\n";
		}

		return $vars;
	}


	/**
	 * Convert relative URLs to absolute URLs
	 *
	 * @param String $str
	 * @return String
	 */
	static function convertRelativeLinks($str){
		global $CFG;

		$finds[] = 'src="/';
		$finds[] = 'src=\'/';
		$finds[] = 'href="/';
		$finds[] = 'href=\'/';

		$replaces[] = 'src="'.$CFG->baseurl;
		$replaces[] = 'src=\''.$CFG->baseurl;
		$replaces[] = 'href="'.$CFG->baseurl;
		$replaces[] = 'href=\''.$CFG->baseurl;

		$str = str_replace($finds,$replaces,$str);

		return $str;
	}

	static function sendAdminEmail($msg,$subject='Error Occurred'){
		global $CFG;

		$vars['body'] = $msg;
		$E = self::sendOne($CFG->company_name,$CFG->company_email,'blank',$vars,$subject);
	}

	static function setupText($template_name, $vars='') {
		global $CFG;
		$vars['this_is_template'] = true;
		//print_r($vars);
		if($vars['order_id']){
			$order1   = Orders::get1($vars['order_id']);
			$store_id = ( $order1['order_tax_option'] ? $order1['order_tax_option'] : 1 );
		} /*else*/
		 if($vars['store_id']){
			$store_id = $vars['store_id']; 
		}
		$template = EmailTemplate::getByName($template_name,$store_id);
		$subject = $template['subject'];
		
		$E = new TigerEmail();
		$E->setSubject($subject);		
		$html_body = $template['html'];

		if(!$stripHTMLforText){
			$E->setTextBody($txt_body);
		}
		
		$E->setHTMLBody($html_body);

		// Build Vars
		//$vars = TigerEmail::setupVars($vars, $new_acct_pwd);				

		$vars['baseurl'] = $CFG->baseurl;
		$vars['base_url'] = $CFG->baseurl;

		if($vars['order_id']){
			$vars = self::buildOrderFields($vars['order_id'],$vars);
		}

		if ($vars['order_store_id'] != $CFG->website_store_id && in_array($vars['order_store_id'],$CFG->ebayStoreIds))
		{
			$vars['global_footer'] = TigerEmail::getFooter($vars['order_store_id']);			
		}
		if($vars['purchase_order_id']){
			$vars = self::buildPurchaseOrderFields($vars['purchase_order_id'],$vars);
		}		
		$E->populateTemplate($vars,true);

		$html_body = self::convertRelativeLinks($E->_html_body);
		$E->setHTMLBody( $html_body );
		
		return $E;

	}
	
	static function buildPurchaseOrderFields($po_id,$vars){
		global $CFG;

		$po = PurchaseOrder::get1($po_id);
		if(!$po){
			return $vars;
		}

		$po_id = $po['id'];
		$store_info = Store::get1($vars['store_id']);
		$vars['po_number'] = $po['code'];
		$vars['customer_ship_to'] = $po['cust_ship_box'];
		$vars['po_date'] = db_date($po['po_sent_date'],'m/d/Y');
		$vars['operator_fname'] = Admin::get1 ( $_SESSION ['id'], 'first_name' );
		if ($CFG->site_name == "lionsdeal") $vars['operator_email'] = $CFG->company_email;
		else $vars['operator_email'] = Admin::get1 ( $_SESSION ['id'], 'email' );
		if ($CFG->site_name == "lionsdeal") $vars['operator_phone'] = $CFG->company_phone;
		else $vars['operator_phone'] = $CFG->company_phone . " x" . Admin::get1 ( $_SESSION ['id'], 'ext' );
		$vars['store_name'] = $CFG->purchasing_company_name;
		$default_from_email_lname = Admin::get1 ( $_SESSION ['id'], 'last_name' );		
		if ($vars['order_item_id'])
		{			 
			$one_item_info = Orders::getItems($vars['order_item_id']);		
			$vars['item_name'] = $one_item_info[0]['product_name'];
		}
		
		return $vars;
	}

	public function sendErrorEmail($email_to='',$subject='',$info=''){
        global $CFG;

        $keys_to_check = array('data');
        $fields_to_mask = array('cc_num','cc_ccv');

        foreach($_REQUEST as $key=>$value){
            if($decoded_value = json_decode($value)){
                $info['REQUEST_DUMP'][$key] = $decoded_value;
            }else{
                $info['REQUEST_DUMP'][$key]=$value;
            }
        }

        $info['SESSION_ID'] = session_id();
        $info['SESSION_DUMP'] = $_SESSION;
        $info['DB_SESSION_INFO'] = db_query_array("select * from sessions WHERE session_id = '".session_id()."'");
        $info['SERVER_DUMP'] = $_SERVER;

        $subject = $CFG->site_abbrev . " - " . $subject;
        if($CFG->in_testing){
            $subject =  'TESTING: ' . $subject;
        }

        if($info['REQUEST_DUMP']['jquery_URL']){
            $subject .= ' - status:'. $info['REQUEST_DUMP']['jquery_status'] .' - '. $info['REQUEST_DUMP']['jquery_URL'];
        }

        //hide cc info
        if(isset($info['REQUEST_DUMP']['jquery_request'])){
            $elems = explode('&', $info['REQUEST_DUMP']['jquery_request']->data);
            foreach( $elems as $k=>$val ) {
                $val = trim($val);
                list($val_name,$val_vlaue) = explode('=', $val);
                if(in_array($val_name,$fields_to_mask)){
                    $elems[$k] = preg_replace('/\d/','X',$val);
                }
            }
            $info['REQUEST_DUMP']['jquery_request']->data = implode('&',$elems);
        }

        if(!$email_to){
            $email_to = $CFG->error_email;
        }

        $body = var_export($info,true);

        $headers = "From: {$CFG->webmaster_email}\r\n";
        mail($email_to,$subject,$body,$headers);

    }
	
}
	
