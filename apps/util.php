<?
function htmlentities_utf($str)
{
	return htmlentities($str,ENT_COMPAT,'UTF-8');
}

/**
 * Not Search Engine Friendly! Only use for Backend!
 *
 * @param string $docTitle
 */
function addTitleJS($docTitle){
	$docTitle = htmlentities_utf($docTitle);
	?>
	<script type="text/javascript">
	document.title += ' - <?=$docTitle?>';
	</script>
	<?
}


function getRewriteUrl($str){
	$str = str_replace("'",'',$str);
	$str = str_replace(array("&amp;","&"),' and ',$str);
	$str = preg_replace("/[^A-Za-z0-9]/",'-',$str);
	$str = preg_replace("/--+/",'-',$str);

	return strtolower(trim($str,'-'));
}

function doubleSecureRedirect(){
	global $CFG;

	if($_SERVER['HTTPS'] == false && !$CFG->in_testing){
    	header("Location: ".$CFG->sslurl."cart2.php?PHPSESSID=".session_id());
    	exit;
    }
}

/**
 * Redirect to a URL using headers or javascript
 *
 * @param unknown_type $url
 */
function redirectClean($url,$http_code=''){
	global $CFG;

	$url = trim($url);

	if(headers_sent()){
		// Redirect Via Meta and Javascript
		?>
		<meta http-equiv='Refresh' content='0; url=<?=$url?>' />
		<script type="text/javascript"> window.location.href="<?=$url?>"; </script>
		<?php
	} else {
		if($http_code){
			switch($http_code){
				case 404:
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
					break;
				case 301:
					header($_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently");
					break;
			}
		}
		header("Location: ".$url);
	}

	exit;
}

/**
 * Redirect to the secure version (HTTPS) of the current Request URI
 *
 * Redirect AGAIN to remove the PHPSESSID from the URL.
 *
 * Must be called AFTER session is started
 *
 */
function secureRedirect(){
	global $CFG;

	if( empty( $_SERVER['HTTPS'] ) ){
		// Redirect WITH PHPSESSID
		redirectClean($CFG->sslurl .
			trim($_SERVER['PHP_SELF'],"/") . "?" . "PHPSESSID=" . session_id()
			. "&" . $_SERVER['QUERY_STRING']);
	} else if(isset($_GET['PHPSESSID'])){
		// Redirect WITHOUT PHPSESSID
		$params = $_GET;
		unset($params['PHPSESSID']);
		$cleanURL = $CFG->sslurl . trim($_SERVER['PHP_SELF'],"/") . "?";
		if(is_array($params) && count($params) > 0){
			foreach($params as $k => $v){
				$cleanURL .= "$k=" . urlencode($v) . "&";
			}
		}
		$cleanURL = trim($cleanURL,'&?');

		redirectClean($cleanURL);
	}
}

function validate_us_phone ($phone_number)
{
	$replace = array( ' ', '-', '/', '(', ')', ',', '.', '=', '*'); //etc; as needed
    return preg_match( '/^((00|\+)([0-9]{10,})|1?([0-9]{10}))((?:#|x\.?|ext\.?|extension)[0-9]{1,4})?$/i', str_replace( $replace, '', $phone_number));	 
	
}

class Util {

	static function fixState($state){
		$state = trim( preg_replace("~[^A-Za-z\s]~","",ucwords(strtolower($state))) );

		if(strlen($state) <= 2){
			return strtoupper($state);
		}
		
		$state = preg_replace("~\s\s+~"," ",$state);
		$allstates = getFullStatesList('', true, '', '', '');
		$allstates = array_flip($allstates);

		if( $allstates[$state] ){
			// Get 2 char abbrev of state
			$state = $allstates[$state];
		}


		return $state;
	}
	static function number_to_words ($x)
	{
	     $nwords = array(  "", "one", "two", "three", "four", "five", "six", 
		      	  "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", 
		      	  "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", 
		     	  "nineteen", "twenty", 30 => "thirty", 40 => "forty",
	                     50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
	                     90 => "ninety" );
						 
	     if(!is_numeric($x))
	     {
	         $w = '#';
	     }else if(fmod($x, 1) != 0)
	     {
	         $w = '#';
	     }else{
	         if($x < 0)
	         {
	             $w = 'minus ';
	             $x = -$x;
	         }else{
	             $w = '';
	         }
	         if($x < 21)
	         {
	             $w .= $nwords[$x];
	         }else if($x < 100)
	         {
	             $w .= $nwords[10 * floor($x/10)];
	             $r = fmod($x, 10);
	             if($r > 0)
	             {
	                 $w .= ' '. $nwords[$r];
	             }
	         } else if($x < 1000)
	         {
	             $w .= $nwords[floor($x/100)] .' hundred';
	             $r = fmod($x, 100);
	             if($r > 0)
	             {
	                 $w .= ' '. Util::number_to_words($r);
	             }
	         } else if($x < 1000000)
	         {
	         	$w .= Util::number_to_words(floor($x/1000)) .' thousand';
	             $r = fmod($x, 1000);
	             if($r > 0)
	             {
	                 $w .= ' ';
	                 if($r < 100)
	                 {
	                     $w .= ' ';
	                 }
	                 $w .= Util::number_to_words($r);
	             }
	         } else {
	             $w .= Util::number_to_words(floor($x/1000000)) .' million';
	             $r = fmod($x, 1000000);
	             if($r > 0)
	             {
	                 $w .= ' ';
	                 if($r < 100)
	                 {
	                     $word .= ' ';
	                 }
	                 $w .= Util::number_to_words($r);
	             }
	         }
	     }
	     return $w;
	}
	
	function send_csv_mail ($csvData, $body, $to = 'youraddress@example.com', $subject = 'Test email with attachment', $from = 'webmaster@example.com',$fileName = 'attachment') {
	
		// This will provide plenty adequate entropy
		$multipartSep = '-----'.md5(time()).'-----';
	
		// Arrays are much more readable
		$headers = array(
				"From: $from",
				"Reply-To: $from",
				"Content-Type: multipart/mixed; boundary=\"$multipartSep\""
		);
		// Make the attachment
		$attachment = chunk_split(base64_encode(Util::create_csv_string($csvData)));
	
		// Make the body of the message
		$body = "--$multipartSep\r\n"
		. "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
		. "Content-Transfer-Encoding: 7bit\r\n"
		. "\r\n"
		. "$body\r\n"
		. "--$multipartSep\r\n"
		. "Content-Type: text/csv\r\n"
		. "Content-Transfer-Encoding: base64\r\n"
		. "Content-Disposition: attachment; filename=\"". $fileName . "_". date('Ymd'). ".csv\"\r\n"
		. "\r\n"
		. "$attachment\r\n"
		. "--$multipartSep--";
	
		// Send the email, return the result
		return @mail($to, $subject, $body, implode("\r\n", $headers));
	
	}
	
	function create_csv_string($data) {
	
		// Open temp file pointer
		if (!$fp = fopen('php://temp', 'w+')) return FALSE;
	
		// Loop data and write to file pointer
		foreach ($data as $line) fputcsv($fp, $line);
	
		// Place stream pointer at beginning
		rewind($fp);
	
		// Return the data
		return stream_get_contents($fp);
	
	}
	
	/**
	 * Function to get the client IP address
	 */
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		
		return $ipaddress;
	}
}

class US_Federal_Holidays
{
    private $year, $list, $next_year;
    const ONE_DAY = 86400; // Number of seconds in one day
 
    function __construct($year = null, $timezone = 'America/Chicago')
    {
        try
        {
            if (! date_default_timezone_set($timezone))
            {
                throw new Exception($timezone.' is not a valid timezone.');         
            }
 
            $this->year = (is_null($year))? (int) date("Y") : (int) $year;
            $this->next_year = (is_null($year))? (int) date("Y") + 1 : (int) $year + 1;
            if (! is_int($this->year) || $this->year < 1997)
            {
                throw new Exception($year.' is not a valid year. Valid values are integers greater than 1996.');
            }
         
            $this->set_list();
        }
 
        catch(Exception $e)
        {
            echo $e->getMessage();
            exit();
        }
    }
 
    private function adjust_fixed_holiday($timestamp)
    {
        $weekday = date("w", $timestamp);
        if ($weekday == 0)
        {
            return $timestamp + self::ONE_DAY;
        }
        if ($weekday == 6)
        {
            return $timestamp - self::ONE_DAY;
        }
        return $timestamp;
    }
 
    private function set_list()
    {
        $this->list = array
        (
            array
            (
                "name" => "New Year's Day", 
                        // January 1st, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 1, 1, $this->year))
                ), 
            array
            (
                "name" => "Birthday of Martin Luther King, Jr.", 
                        // 3rd Monday of January
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 1, 1, $this->year))
                ),
            array
            (
                "name" => "Wasthington's Birthday", 
                        // 3rd Monday of February
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 2, 1, $this->year))
                ),
            array
            (
                "name" => "Memorial Day ", 
                        // last Monday of May
                "timestamp" => strtotime("last Monday of May $this->year")
                ),
            array
            (
                "name" => "Independence day ", 
                        // July 4, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 7, 4, $this->year))
                ),
            array
            (
                "name" => "Labor Day ", 
                        // 1st Monday of September
                "timestamp" => strtotime("first Monday of September $this->year")
                ),
            array
            (
                "name" => "Columbus Day ", 
                        // 2nd Monday of October
                "timestamp" => strtotime("2 Mondays", mktime(0, 0, 0, 10, 1, $this->year))
                ),
            array
            (
                "name" => "Veteran's Day ",
                        // November 11, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 11, 11, $this->year))
                ),  
            array
            (
                "name" => "Thanksgiving Day ", 
                        // 4th Thursday of November
                "timestamp" => strtotime("4 Thursdays", mktime(0, 0, 0, 11, 1, $this->year))
                ),
            array
            (
                "name" => "Christmas ", 
                        // December 25 every year, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 12, 25, $this->year))
            ),
            array
            (
                "name" => "Next New Year's Day", 
                        // January 1st, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 1, 1, $this->next_year))
                ), 
            array
            (
                "name" => "Next Birthday of Martin Luther King, Jr.", 
                        // 3rd Monday of January
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 1, 1, $this->next_year))
                ),
            array
            (
                "name" => "Next Wasthington's Birthday", 
                        // 3rd Monday of February
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 2, 1, $this->next_year))
                ),
            array
            (
                "name" => "Next Memorial Day ", 
                        // last Monday of May
                "timestamp" => strtotime("last Monday of May $this->next_year")
                ),
            array
            (
                "name" => "Next Independence day ", 
                        // July 4, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 7, 4, $this->next_year))
                ),
            array
            (
                "name" => "Next Labor Day ", 
                        // 1st Monday of September
                "timestamp" => strtotime("September $this->next_year first Monday")
                ),
            array
            (
                "name" => "Next Columbus Day ", 
                        // 2nd Monday of October
                "timestamp" => strtotime("2 Mondays", mktime(0, 0, 0, 10, 1, $this->next_year))
                ),
            array
            (
                "name" => "Next Veteran's Day ",
                        // November 11, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 11, 11, $this->next_year))
                ),  
            array
            (
                "name" => "Next Thanksgiving Day ", 
                        // 4th Thursday of November
                "timestamp" => strtotime("4 Thursdays", mktime(0, 0, 0, 11, 1, $this->next_year))
                ),
            array
            (
                "name" => "Next Christmas ", 
                        // December 25 every year, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 12, 25, $this->next_year))
            )
        );
    }
 
    public function get_list()
    {
        return $this->list;
    }
 
    public function is_holiday($timestamp)
    {
        foreach ($this->list as $holiday)
        {
           if ($timestamp == $holiday["timestamp"]) return true;
        }
     
        return false;
    }
}

function getWorkingDays($startDate,$endDate,$holidays){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    // RSunness--don't want to add 1
    $days = ($endDate - $startDate) / 86400;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = $days % 7;

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}
function addNumBusinessDays($startDate,$numDays,$holidays){
	$numDaysUpTo = $numDays;
	$dateUpTo = strtotime($startDate);
	$dayOfWeekUpTo = date("N", $dateUpTo);
     
    $secondsInADay = 24 * 60 * 60;
        
    $shippedAlready = true;
	if ($dayOfWeekUpTo == 6 || $dayOfWeekUpTo == 7){ $shippedAlready = false;} 
    else if (in_array($dateUpTo, $holidays)) { $shippedAlready = false;}
    while ($numDaysUpTo > 0)
    {
    	$dateUpTo += $secondsInADay;
    	$dayOfWeekUpTo = date("N", $dateUpTo);
    	$dateOfDateUpTo = date('Y-m-d', $dateUpTo);
    	if ($dayOfWeekUpTo == 6 || $dayOfWeekUpTo == 7){/* don't decrement num days left if is a Saturday or Sunday*/} 
    	else if (in_array($dateOfDateUpTo, $holidays)) {/* don't decrement num days left if is a holiday*/}
    	else // regular business day
    	{
    		if(!$shippedAlready) $shippedAlready = true; 
    		else $numDaysUpTo--;    		
    	}
    }       

    return date ('Y-m-d', $dateUpTo);
}
?>
