<?php

class Brands_Contacts 
{
	
	public static function insert($info)
	{		
		return db_insert('brands_contacts',$info);
	}
	
	public static function update($id,$info)
	{
		return db_update('brands_contacts',$id,$info);
	}
	
	public static function delete($id)
	{
		return db_delete('brands_contacts',$id);
	}
	
	public static function get($id=0,$brand_id=0,$begins_with='',$keywords='',$order='',$order_asc='', $name='')
	{
		$sql = "SELECT brands_contacts.*
				FROM brands_contacts
				WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND brands_contacts.id = $id ";
		}
		if ($brand_id > 0)
		{
		  $sql .= " AND brands_contacts.brand_id = $brand_id ";
		}
		if ($begins_with != '') {
			$sql .= " AND brands_contacts.title LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') {
			$fields = Array('brands_contacts.title');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if( $name != '' )
			$sql .= " AND brands_contacts.title = '$name' ";
						
		$sql .= " GROUP BY brands_contacts.id
				  ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'brands_contacts.title';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' DESC ';
		}
		
		return db_query_array($sql);
	}
	
	public static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = self::get($id);
		return $result[0];
	}
}

?>