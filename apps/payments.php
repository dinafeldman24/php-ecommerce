<?php

	class Payments {
		static function insert($info)
		{
			return db_insert('order_payments',$info,'date_added');
		}

		static function update($id,$info)
		{
			return db_update('order_payments',$id,$info);
		}

		static function delete($id)
		{
			return db_delete('order_payments',$id);
		}

		static function getOrderPayments( $order_id )
		{
			return Payments::get( 0, $order_id, '', '', 'order_payments.date_added', 0, false, true );
		}

		static function get( $id=0, $order_id=0, $action='', $transaction_id='', $order='', $auth_id=0, $received_only = false, $order_asc='' )
		{
			$sql = " SELECT order_payments.*, concat( first_name, ' ', last_name) as user FROM order_payments ";
			$sql .= " LEFT JOIN admin_users on admin_users.id = order_payments.user_id ";
			$sql .= " WHERE 1 ";

			if( $id )
				$sql .= " AND order_payments.id = $id ";

			if( $order_id )
				$sql .= " AND order_id = $order_id ";

			if( $action && !$received_only ) //received only is going to use action as well so wouldn't jive!
				$sql .= " AND action = '$action' ";

			if( $received_only )
			{
				$sql .= " AND ( action in ('sale', 'capture') ) "; //Need to add a field to mark payments recieved that aren't CC
			}

			if( $transaction_id )
				$sql .= " AND transaction_id = '$transaction_id' ";

			if( $auth_id )
				$sql .= " AND auth_id = $auth_id ";

			if( $order )
			{
				$sql .= " ORDER BY " . $order;
				if( $order_asc )
				{
					$sql .= " ASC ";
				}
				else {
					$sql .= " DESC ";
				}
			}


			return db_query_array( $sql );
			//return $sql;
		}

		static function get1($id)
		{
			$id = (int) $id;

			if (!$id) return false;

			$result = Payments::get($id);
			return $result[0];
		}

		static function getLastCCTransaction( $order_id )
		{
			$payments = Payments::get( 0, $order_id, '', '', 'date_added', 0, false, false );

			if( $payments )
				return $payments[0];
			else
				return false;
		}
				static function getGatewayByToken( $last4='', $token='')
		{
			$sql = " SELECT order_payments.* FROM order_payments ";
			$sql .= " WHERE 1 ";

			if( $last4 )
				$sql .= " AND order_payments.last4 = '$last4' ";
			if( $token )
				$sql .= " AND order_payments.token = '$token' ";

			$result = db_query_array( $sql );
			return($result[0]['gateway']);
		}
	}

?>