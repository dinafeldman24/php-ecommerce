<?

class InvoiceVendor{

	static function insert($info){
		return db_insert("invoice_vendor",$info,'date_added');
	}

	static function update($id,$info){
		return db_update("invoice_vendor",$id,$info,'id','date_modified');
	}

	static function delete($id){
		return db_delete("invoice_vendor",$id);
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT invoice_vendor.*, (due - paid) as balance, brands.name as brand_name,
					SUM(poi.item_qty * IF(poi.cust_each IS NULL,oi.price,poi.cust_each)) AS po_total
		";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(invoice_vendor.id)) as total ";
		}

		$from_ = " FROM invoice_vendor ";
		$join_ .= " LEFT JOIN brands ON brands.id = invoice_vendor.brand_id ";
		$join_ .= " LEFT JOIN purchase_order_items poi ON poi.po_id = invoice_vendor.po_id ";
		$join_ .= " LEFT JOIN order_items oi ON oi.id = poi.item_id ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND invoice_vendor.id = [id] ";
		}
		if(isset($o->min_balance)){
			$where_ .= " AND (due - paid) >= [min_balance] ";
		}
		if(isset($o->query)){
			$fields = Array('brands.name');
			$where_ .= " AND " . db_split_keywords($o->query,$fields,'AND',true);
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY invoice_vendor.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}

			$groupby_ = " GROUP BY invoice_vendor.id ";
		}



		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	static function payInFull($id){
		$id = (int)$id;

		$sql = " UPDATE invoice_vendor SET paid = due WHERE id = '$id' ";

		return db_query($sql);
	}

}