<?php

include_once( 'RB/core/encoding.php');
$suburban27_returns_text = "<br/><br/><center><h2>Shipping & Returns</h2></center><br/><br/>
<b>Returns & Exchanges</b><br/><br/>
 Suburban27 is committed to making your online shopping experience a pleasurable one. We will gladly accept returns on all unused items that are still in their original packaging, with the exception of custom items and equipment with optional accessories. The customer is responsible for any shipping costs. If free shipping was offered, a standard charge of $7.95 will be applied. Certain items will incur a restocking fee. Please contact customer service to find out if the item you are ordering will have a restocking fee.
<br/><br/>  
We will not accept returns after 30 days of delivery. Please notify us as soon as possible if you wish to return an item.
<br/><br/> 
We will provide you with a Return Authorization (RA) number. Please note that we WILL NOT accept returns without a return authorization number. We ask that you please mark this number clearly on the box that you are returning. Also, please include a copy of the invoice that you received with your purchase along with the item you are returning.
<br/><br/>
To exchange an item, you will need to place a new order and return the item you no longer want. You are responsible for any restocking fees on the returned item, as well as shipping on the new item. 
<br/><br/> 
<b>Damaged Items</b>
<br/><br/> 
Suburban27 does all it can to assure your item is shipped safely and securely. In the rare circumstance that your item is received damaged or defective, or it is simply not what you ordered, please notify us within 48 hours. We will then provide you with a Return Authorization (RA) number and a prepaid packing slip. Please mark the box clearly with this number and include a copy of the invoice you received along with the item you are returning.
<br/><br/> 
Due to the fragility of glass and china, we cannot guarantee their condition upon arrival to you. Therefore, we ask that any orders of glass or china be inspected upon arrival. If your order is defective in any way please refuse the delivery or notify us within 48 hours.
<br/><br/> 
<b>Canceling an Order</b>
<br/><br/> 
Please contact us immediately if you wish to cancel an order. If your order has not yet been processed it will be canceled and NO FEE shall apply. If your order has been processed, please see our Return Policy (above).
<br/><br/> 
We apologize but we cannot accept any returns without an RA number.
<br/><br/> 
<b>Standard Shipping</b>
<br/><br/> 
Most orders are shipped within 1-4 business days with the exception of custom or oversized items. Suburban27 ships with UPS, FedEx, and the United States Postal Service, as well as other major shipping companies. In keeping with our commitment of great service to our customers, we will choose the method of delivery based on what we determine is the least expensive and most effective. In the event that there is an unexpected delay in the shipping or processing of an order, we will notify the customer via e-mail.
<br/><br/>
Packages that are shipped ground are generally delivered within 5-10 business days from the time the order was placed. The shipping fees will be determined according to the volume and weight of the item, as well as the Zip Codes of both the origin and destination of the item. For a multiple item order the shipping fee for each item will be determined individually depending on the location from where it will ship. Please note that these items may also be delivered at different times.
<br/><br/> 
<b>Expedited Shipping</b>
<br/><br/> 
Many of our items can be offered to our customers as fast as Next Day, 2 Day or 3 Day shipping. Please contact us to see if this option is available, as there are some items, due to size and/or destination, which cannot be expedited. Please take into account that it takes some time to process an order. Expedited shipping only applies to the actual shipping itself. 
<br/><br/> 
<b>Tracking</b>
<br/><br/> 
Once an order has been processed, the customer shall receive an e-mail from Suburban27 containing all shipping and tracking information. You can also log in to our website with your email and password and track your package in the My Account section. If you forgot your password, simply click on Forgot Password and a temporary password will be emailed to you.
<br/><br/> 
<b>New York Shipping</b>
<br/><br/> 
Suburban27 is based out of New York. We are therefore required to charge sales tax on any orders that are shipped within the state.
<br/><br/> 
<b>International Shipping</b>
<br/><br/> 
We ship all of our products worldwide. Processing time for international shipments is 6-12 business days.
";

class EbayAPI{

	const COMPAT_LEVEL = 705;
	static $latest_request_xml;
	static $latest_response_xml;

    public static function addItem($itemTitle = '', $itemDescription = '', $primaryCategory = 0, $location = '',
    	$startPrice = '0.00', $buyItNowPrice = '0.00', $listingType = '', $listingDuration = '', $quantity = 0, $verify = false, $gallery_url = '', $paypal_address = '',
	$store_id = 0, $secondaryCategory = 0, $primaryStoreCategory=0, $secondaryStoreCategory=0, $sku='', $weight=0, $length=0, $width=0, $height=0, $dispatch_max_time=2, $priced_per = ''
    ) {
    	global $CFG;
    	global $suburban27_returns_text;

	if( !$store_id )
	{
	    showStatus( 'Store ID Required.');
	    return false;
	}

    	if($verify){
			$verb = 'VerifyAddItem';
    	} else {
    		$verb = 'AddItem';
    	}

	$store = Store::get1( $store_id );

	//print_ar( $store );

	$CFG->ebay_token = $store['ebay_token'];
	$CFG->ebay_dev_id = $store['ebay_dev_id'];
	$CFG->ebay_app_id = $store['ebay_app_id'];
	$CFG->ebay_cert_id = $store['ebay_cert_id'];
	$CFG->ebay_paypal_email = $store['paypal_email'];
	$paypal_address = $store['paypal_email'];

	//print_ar( $CFG );

    	$verified = false;
    	$ret_fees = array();

	/*
	 * Having encoding issues when sending title's / Descriptions, so going to encode them down to plain text
	 */
	
	 $itemTitle = Encoding::strip_utf8_for_email_subject( $itemTitle );
	 $itemTitle = substr($itemTitle, 0, 80);
	 $itemDescription = Encoding::strip_utf8_for_email_subject( $itemDescription );
	if ($priced_per != '') $itemDescription .= "<li>Sold as $priced_per</li>";
		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8" ?>';
		$requestXmlBody .= "<${verb}Request xmlns=\"urn:ebay:apis:eBLBaseComponents\">";
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
		$requestXmlBody .= '<Item>';
		$requestXmlBody .= '<Site>US</Site>';
		$requestXmlBody .= '<PrimaryCategory>';
		$requestXmlBody .= "<CategoryID>$primaryCategory</CategoryID>";
		$requestXmlBody .= '</PrimaryCategory>';
// Secondary Categories Cost More, so we aren't listing this now.
//		if( $secondaryCategory ) {
//		    $requestXmlBody .= '<SecondaryCategory>';
//		    $requestXmlBody .= "<CategoryID>$secondaryCategory</CategoryID>";
//		    $requestXmlBody .= '</SecondaryCategory>';
//		}
		
		if( $primaryStoreCategory || $secondaryStoreCategory )
		{
		    $requestXmlBody .= '<Storefront>';

		    $requestXmlBody .= "<StoreCategory2ID>$secondaryStoreCategory</StoreCategory2ID>";
		    $requestXmlBody .= "<StoreCategoryID>$primaryStoreCategory</StoreCategoryID>";
		    
		    $requestXmlBody .= '</Storefront>';
		}


		if($gallery_url){
			$requestXmlBody .= "<PictureDetails>";
			$requestXmlBody .= "<GalleryType>Gallery</GalleryType>";
			$requestXmlBody .= "<GalleryURL>$gallery_url?".time()."</GalleryURL>";
			$requestXmlBody .= "<PictureURL>$gallery_url?".time()."</PictureURL>";
			$requestXmlBody .= "</PictureDetails>";
		}
		if($listingType=='Chinese'){
			$requestXmlBody .= "<BuyItNowPrice currencyID=\"USD\">$buyItNowPrice</BuyItNowPrice>";
		}
		$requestXmlBody .= '<Country>US</Country>';
		$requestXmlBody .= '<Currency>USD</Currency>';
		$requestXmlBody .= "<ListingDuration>$listingDuration</ListingDuration>";
		$requestXmlBody .= "<ListingType>$listingType</ListingType>";
		$requestXmlBody .= "<Location><![CDATA[$location]]></Location>";
		$requestXmlBody .= '<PaymentMethods>PayPal</PaymentMethods>';
		$requestXmlBody .= "<PayPalEmailAddress>$paypal_address</PayPalEmailAddress>";
		$requestXmlBody .= "<Quantity>$quantity</Quantity>";
		$requestXmlBody .= '<RegionID>0</RegionID>';
		$requestXmlBody .= '<ConditionID>1000</ConditionID>';
		$requestXmlBody .= "<StartPrice>$startPrice</StartPrice>";
		$requestXmlBody .= '<ShippingTermsInDescription>True</ShippingTermsInDescription>';
		//$requestXmlBody .= "<Title><![CDATA[".str_replace('"', '&quot;', $itemTitle)."]]></Title>";
		$requestXmlBody .= "<Title><![CDATA[".$itemTitle."]]></Title>";
		
		$itemTitle = "<h1>".$itemTitle."</h1>";
		$gallery_url = "<img src='".$gallery_url."'>";
			
		if ($store_id == 3) // suburban27
		{				
			$desc = $itemDescription . $suburban27_returns_text . "<br/>".$gallery_url;	
		}
		else if ($store_id == 4) //sellitforless
		{
			$desc = "<center>$itemTitle.$itemDescription.<br/>Most orders shipped same business day.<br/>$gallery_url</center>";				
		}
		else if ($store_id == 5) //tigerchef
		{
			$desc = "<center>$itemTitle.$itemDescription.<br/>Most orders shipped same business day.<br/>$gallery_url</center>";				
		}
		$requestXmlBody .= "<Description><![CDATA[$desc]]></Description>";
				
		$requestXmlBody .= "<ReturnPolicy>";
		$requestXmlBody .= "<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>";
		$requestXmlBody .= "<RefundOption>MoneyBack</RefundOption>";
		$requestXmlBody .= "<ReturnsWithinOption>Days_30</ReturnsWithinOption>";
		$requestXmlBody .= "<ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>";
		$requestXmlBody .= "</ReturnPolicy>";
		$requestXmlBody .= "<DispatchTimeMax>$dispatch_max_time</DispatchTimeMax>";
		$requestXmlBody .= "<ShippingDetails>";
		if ($weight == 0)
		{
			$requestXmlBody .= "<ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingService>UPSGround</ShippingService>";
			$requestXmlBody .= "<ShippingSurcharge currencyID=\"USD\">30.00</ShippingSurcharge>";
			if($startPrice > $CFG->ebay_free_shipping_limit || $buyItNowPrice > $CFG->ebay_free_shipping_limit){
				$requestXmlBody .= "<ShippingServiceCost currencyID=\"USD\">0</ShippingServiceCost>";
				$requestXmlBody .= "<FreeShipping>true</FreeShipping>";
			} else{
				$requestXmlBody .= "<ShippingServiceCost currencyID=\"USD\">" . $CFG->ebay_shipping_amount . "</ShippingServiceCost>";
				$requestXmlBody .= "<FreeShipping>false</FreeShipping>";
			}
			$requestXmlBody .= "<ShippingServiceAdditionalCost currencyID=\"USD\">2.99</ShippingServiceAdditionalCost>";
			$requestXmlBody .= "</ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingType>Flat</ShippingType>";
		}
		else 
		{
// begin old 

/*
			$requestXmlBody .= "<CalculatedShippingRate>
        	<OriginatingPostalCode>10901</OriginatingPostalCode>";
			if ($length > 0 && $width > 0 && $height > 0) // use dimensions if we have them
			{
		 		$requestXmlBody .= " 
				<PackageDepth>$height</PackageDepth>
        		<PackageLength>$length</PackageLength>
        		<PackageWidth>$width</PackageWidth>";
			}
        	$requestXmlBody .= "<ShippingPackage>PackageThickEnvelope</ShippingPackage>";
			list($lbs,$oz) = explode('.', $weight);
			$oz = number_format($oz);			
			if($oz >= 16){
				$lbs += floor($oz/16);
				$oz = $oz % 16;
			}
			$requestXmlBody .= "
        		<WeightMajor unit=\"lbs\">$lbs</WeightMajor>
        		<WeightMinor unit=\"oz\">$oz</WeightMinor>
      		</CalculatedShippingRate>
      		<ShippingServiceOptions>
	        	<ShippingService>UPSGround</ShippingService>
    	    	<ShippingServicePriority>1</ShippingServicePriority>
      		</ShippingServiceOptions>
      		<ShippingServiceOptions>
        		<ShippingService>UPS3rdDay</ShippingService>
	        	<ShippingServicePriority>2</ShippingServicePriority>
      		</ShippingServiceOptions>
      		<ShippingServiceOptions>
	        	<ShippingService>UPS2ndDay</ShippingService>
    	    	<ShippingServicePriority>3</ShippingServicePriority>
      		</ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingType>Calculated</ShippingType>";
			*/
//end old
//begin new		
		list($lbs,$oz) = explode('.', $weight);
			$oz = number_format($oz);			
			if($oz >= 16){
				$lbs += floor($oz/16);
				$oz = $oz % 16;
			}
		  // Items weight under 1 pound Shipping should for first piece $4.99 and each additional piece $0.99
		  // Expedited $35 + 3.99 each addl. piece
		  if ($lbs == 0 && $oz > 0)
		  {
		  	$per_item_weight_charge_standard = .99;
		  	$single_item_standard_charge = 4.99 + $per_item_weight_charge_standard;		  		 
		  
		  	$per_item_weight_charge_expedited = 3.99;
		  	$single_item_expedited_charge = 35 + $per_item_weight_charge_expedited;
		  }
		  else 
		  {
		  	if ($oz > 0) $lbs_to_use = $lbs + 1;
		  	else $lbs_to_use = $lbs;
		  
		  	$per_shipment_standard = 6.49;
		  	$per_lb_standard = .49;
		  
		  	$per_item_weight_charge_standard = round($lbs_to_use * $per_lb_standard, 2);
		  	$single_item_standard_charge = $per_shipment_standard + $per_item_weight_charge_standard;		  		 
		  
		  	$per_shipment_expedited = 50;
		  	$per_lb_expedited = 2;
		  
		  	$per_item_weight_charge_expedited = round($lbs_to_use * $per_lb_expedited, 2);
		  	$single_item_expedited_charge = $per_shipment_expedited + $per_item_weight_charge_expedited;
		  }
			$requestXmlBody .= "<CalculatedShippingRate>
        		<WeightMajor unit=\"lbs\">$lbs</WeightMajor>
        		<WeightMinor unit=\"oz\">$oz</WeightMinor>
        		<OriginatingPostalCode>10901</OriginatingPostalCode>
      		</CalculatedShippingRate>";
      		$requestXmlBody .= "<RateTableDetails/>";
        	//<DomesticRateTable>Default</DomesticRateTable>
      		//</RateTableDetails>";
		 //$requestXmlBody .= "<ShippingType>FlatDomesticCalculatedInternational</ShippingType>";
		 $requestXmlBody .= "<ShippingType>Calculated</ShippingType>";
		  $requestXmlBody .= "<ShippingServiceOptions>        
        <ShippingService>UPSGround</ShippingService>
        <ShippingServicePriority>1</ShippingServicePriority>";
        //$requestXmlBody .= "<ShippingServiceCost>$single_item_standard_charge</ShippingServiceCost>       
        //<ShippingServiceAdditionalCost>$per_item_weight_charge_standard</ShippingServiceAdditionalCost>        		  
      	$requestXmlBody .= "</ShippingServiceOptions><ShippingServiceOptions>        
        <ShippingService>UPS2ndDay</ShippingService>
        <ShippingServicePriority>2</ShippingServicePriority>";
        //$requestXmlBody .= "<ShippingServiceCost>$single_item_expedited_charge</ShippingServiceCost>
        //<ShippingServiceAdditionalCost>$per_item_weight_charge_expedited</ShippingServiceAdditionalCost>        		  
      	$requestXmlBody .= "</ShippingServiceOptions>
		<InternationalShippingServiceOption>
		    <ShippingService>USPSPriorityMailInternational</ShippingService>
            <ShippingServicePriority>1</ShippingServicePriority>
        <ShipToLocation>Worldwide</ShipToLocation>        
       </InternationalShippingServiceOption>";			  	
//end new			
		}
		$requestXmlBody .= "</ShippingDetails>";
		
		$requestXmlBody .= "<SKU>$sku</SKU>";
		$requestXmlBody .= '</Item>';
		$requestXmlBody .= "</{$verb}Request>";

		$log_info['verb']	= $verb;
		$log_info['request']	= $requestXmlBody;
		$log_info['user_id']	= $_SESSION['id'];
		//$log_info['product_id'] = 0;
		$log_info['title']	= $itemTitle;
		
		$log_id = EbayLog::insert( $log_info, 'date_sent' );

		$responseDoc = self::getResponse($verb,$requestXmlBody,false,$log_id);

		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		$errors_found = false;
		//if there are error nodes
		if($errors->length > 0)
		{
		    echo '<P><B>eBay returned the following error(s) While attempting to List: ' . $itemTitle . ':</B>';
		    for( $i=0; $i < $errors->length; $i++ )
		    {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item($i)->getElementsByTagName('ErrorCode');
			$severityCode = $errors->item( $i )->getElementsByTagName( 'SeverityCode');
			$shortMsg = $errors->item($i)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item($i)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			
			echo '<P>'. $code->item(0)->nodeValue. ' (' . $severityCode->item(0)->nodeValue .') : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0)
				echo '<BR>'. str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			if( $severityCode != 'Warning' )
			    $errors_found = true;
		    }
		    if( $errors_found )
			return false;
		}
		//
		////no errors or just warnings
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
              $acks = $response->getElementsByTagName("Ack");
              $ack   = $acks->item(0)->nodeValue;

              if($ack == 'Success'){
              	$verified = true;
              }

              //echo "Ack = $ack <BR />\n";   // Success if successful

              $endTimes  = $response->getElementsByTagName("EndTime");
              $endTime   = $endTimes->item(0)->nodeValue;
              //echo "endTime = $endTime <BR />\n";

              $itemIDs  = $response->getElementsByTagName("ItemID");
              $itemID   = $itemIDs->item(0)->nodeValue;

	      EbayLog::update( $log_id, array('ebay_item_id'=>$itemID) );
              //echo "itemID = $itemID <BR />\n";


              $feeNodes = $responseDoc->getElementsByTagName('Fee');
              foreach($feeNodes as $feeNode) {
                $feeNames = $feeNode->getElementsByTagName("Name");
                if ($feeNames->item(0)) {
                    $feeName = $feeNames->item(0)->nodeValue;
                    $fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
                    $fee = $fees->item(0)->nodeValue;
                    if ($fee > 0.0) {
                    	$ret_fees[$feeName] = $fee;
                        /*if ($feeName == 'ListingFee') {
                          printf("<B>$feeName : %.2f </B><BR>\n", $fee);
                        } else {
                          printf("$feeName : %.2f <BR>\n", $fee);
                        } */
                    }  // if $fee > 0
                } // if feeName
              } // foreach $feeNode

            } // foreach response

		 // if $errors->length > 0

		if($verify){
			return $verified;
		}

		if($verified){
			return array(
				'id' => $itemID,
				'end_time' => $endTime,
				'fees' => $ret_fees
			);
		} else {
			return false;
		}

    }

    public static function getResponse($verb = '',$requestXmlBody = '', $debug = false, $log_id=0){
    	global $CFG;
        //Create a new eBay session with all details pulled in from included keys.php
        $session = new eBaySession($CFG->ebay_token, $CFG->ebay_dev_id, $CFG->ebay_app_id, $CFG->ebay_cert_id,
				    $CFG->ebay_server_url, self::COMPAT_LEVEL, 0, $verb);

//echo $CFG->ebay_token.", ".$CFG->ebay_dev_id.", ".$CFG->ebay_app_id.", ".$CFG->ebay_cert_id.", ".$CFG->ebay_server_url;

		//send the request and get response
		$responseXml = $session->sendHttpRequest($requestXmlBody);

		EbayLog::update( $log_id, array('response'=>$responseXml ) );

		self::$latest_request_xml = $requestXmlBody;
		self::$latest_response_xml = $responseXml;

		if($debug){
		//if(1){
			echo "<pre>$responseXml</pre>";
		}

		if(stristr($responseXml, 'HTTP 404') || $responseXml == '')
			die('<P>Error sending request');

		$responseDoc = new DomDocument();
		$responseDoc->loadXML($responseXml);
		return $responseDoc;
    }

    public static function getItem($itemId = 0,$store=''){
    	global $CFG;


		$verb = 'GetItem';

    	$verified = false;
    	$ret_fees = array();

    	if(is_array($store)){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
  		$requestXmlBody .= "<ItemID>$itemId</ItemID>";
		$requestXmlBody .= '</GetItemRequest>';


        $responseDoc = self::getResponse($verb,$requestXmlBody,false);

        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');

		//if there are error nodes
		if($errors->length > 0)
		{
			return false;
			echo '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			echo '<P>', $code->item(0)->nodeValue, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0)
				echo '<BR>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = date('Y-m-d h:i:s',strtotime($endTimes->item(0)->nodeValue));
				//echo "endTime = $endTime <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				//echo "itemID = $itemID <BR />\n";

				$currentPrice = $response->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
				$bidCount = $response->getElementsByTagName('BidCount')->item(0)->nodeValue;
				$reserveMet = $response->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
				$quantitySold = $response->getElementsByTagName('QuantitySold')->item(0)->nodeValue;
            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return array(
				'id' => $itemID,
				'end_time' => $endTime,
				'current_price' => $currentPrice,
				'bid_count' => $bidCount,
				'reserve_met' => ($reserveMet == 'true' ? 'Y' : 'N'),
				'quantity_sold' => $quantitySold
			);
		} else {
			return false;
		}
    }

    public static function reviseItem($itemId = 0, $quantitySold = false, $itemTitle = '', $itemDescription = '',
				      $startPrice = '0.00', $buyItNowPrice = '0.00', $gallery_url = '', $primaryCategory=0, $secondaryCategory=0,
				      $primaryStoreCategory=0, $secondaryStoreCategory=0, $store_id=0, $qty='',$sku='', $weight=0, $length=0, $width=0, $height=0, $dispatch_max_time=2, $priced_per = '')
    {

    	global $CFG;
		global $suburban27_returns_text;
    	if(!$itemId)
    		return false;

    	$verb = 'ReviseItem';
    	$verified = false;

	$store = Store::get1( $store_id );

	//print_ar( $store );

	$CFG->ebay_token = $store['ebay_token'];
	$CFG->ebay_dev_id = $store['ebay_dev_id'];
	$CFG->ebay_app_id = $store['ebay_app_id'];
	$CFG->ebay_cert_id = $store['ebay_cert_id'];
	$CFG->ebay_paypal_email = $store['paypal_email'];
	$paypal_address = $store['paypal_email'];

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8" ?>';
		$requestXmlBody .= "<${verb}Request xmlns=\"urn:ebay:apis:eBLBaseComponents\">";
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
		$requestXmlBody .= '<Item>';
		$requestXmlBody .= "<ItemID>$itemId</ItemID>";
		$requestXmlBody .= '<Site>US</Site>';

		$requestXmlBody .= '<ConditionID>1000</ConditionID>';

		if( $primaryCategory ) {
		    $requestXmlBody .= '<PrimaryCategory>';
		    $requestXmlBody .= "<CategoryID>$primaryCategory</CategoryID>";
		    $requestXmlBody .= '</PrimaryCategory>';
		}
		/*if( $secondaryCategory ) {
		    $requestXmlBody .= '<SecondaryCategory>';
		    $requestXmlBody .= "<CategoryID>$secondaryCategory</CategoryID>";
		    $requestXmlBody .= '</SecondaryCategory>';
		}*/

		if( $primaryStoreCategory || $secondaryStoreCategory )
		{
		    $requestXmlBody .= '<Storefront>';

		    $requestXmlBody .= "<StoreCategory2ID>$secondaryStoreCategory</StoreCategory2ID>";
		    $requestXmlBody .= "<StoreCategoryID>$primaryStoreCategory</StoreCategoryID>";
		    
		    $requestXmlBody .= '</Storefront>';
		}


		if($gallery_url){
			$requestXmlBody .= "<PictureDetails>";
			$requestXmlBody .= "<GalleryType>Gallery</GalleryType>";
			$requestXmlBody .= "<GalleryURL>$gallery_url?".time()."</GalleryURL>";
			$requestXmlBody .= "<PictureURL>$gallery_url?".time()."</PictureURL>";
			$requestXmlBody .= "</PictureDetails>";
		}
		if($buyItNowPrice > 0){
			$requestXmlBody .= "<BuyItNowPrice currencyID=\"USD\">$buyItNowPrice</BuyItNowPrice>";
		}
		if(is_numeric($quantitySold)){
			$requestXmlBody .= "<QuantitySold>$quantitySold</QuantitySold>";
		}
		if($startPrice > 0){
			$requestXmlBody .= "<StartPrice currencyID=\"USD\">$startPrice</StartPrice>";
		}
		if($itemTitle){
			//$requestXmlBody .= "<Title><![CDATA[".str_replace('"', '&quot;', $itemTitle)."]]></Title>";
			$itemTitle = substr($itemTitle, 0, 80);
			$requestXmlBody .= "<Title><![CDATA[".$itemTitle."]]></Title>";
		}
		if($itemTitle || $itemDescription){
			$itemTitle_formatted = "<h1>".$itemTitle."</h1>";
			$gallery_url_formatted = "<img src='".$gallery_url."'>";
			if ($priced_per != '') $itemDescription .= "<li>Sold as $priced_per</li>"; 
			if ($store_id == 3) // suburban27
			{
				
				$desc = $itemDescription .  "<br/>".$gallery_url_formatted . $suburban27_returns_text;	
			}
			else if ($store_id == 4) //sellitforless
			{
				$desc = "<center>$itemTitle_formatted.$itemDescription.<br/>Most orders shipped same business day.<br/>$gallery_url_formatted</center>";				
			}
			else if ($store_id == 5) //tigerchef
			{
				$desc = "<center>$itemTitle_formatted.$itemDescription.<br/>Most orders shipped same business day.<br/>$gallery_url_formatted</center>";				
			}
			$requestXmlBody .= "<Description><![CDATA[$desc]]></Description>";
		}
		if($qty){
			$requestXmlBody .= "<Quantity><![CDATA[$qty]]></Quantity>";
		}
		if($sku){
			$requestXmlBody .= "<SKU>$sku</SKU>";
		}
		$requestXmlBody .= "<DispatchTimeMax>$dispatch_max_time</DispatchTimeMax>";
		// NEW!  Adding revision of shipping costs
		
		$requestXmlBody .= "<ShippingDetails>";
		if ($weight == 0)
		{
			$requestXmlBody .= "<ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingService>UPSGround</ShippingService>";
			$requestXmlBody .= "<ShippingSurcharge currencyID=\"USD\">30.00</ShippingSurcharge>";
			if($startPrice > $CFG->ebay_free_shipping_limit || $buyItNowPrice > $CFG->ebay_free_shipping_limit){
				$requestXmlBody .= "<ShippingServiceCost currencyID=\"USD\">0</ShippingServiceCost>";
				$requestXmlBody .= "<FreeShipping>true</FreeShipping>";
			} else{
				$requestXmlBody .= "<ShippingServiceCost currencyID=\"USD\">" . $CFG->ebay_shipping_amount . "</ShippingServiceCost>";
				$requestXmlBody .= "<FreeShipping>false</FreeShipping>";
			}
			$requestXmlBody .= "<ShippingServiceAdditionalCost currencyID=\"USD\">2.99</ShippingServiceAdditionalCost>";
			$requestXmlBody .= "</ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingType>Flat</ShippingType>";
		}
		else 
		{
// begin old 

/*
			$requestXmlBody .= "<CalculatedShippingRate>
        	<OriginatingPostalCode>10901</OriginatingPostalCode>";
			if ($length > 0 && $width > 0 && $height > 0) // use dimensions if we have them
			{
		 		$requestXmlBody .= " 
				<PackageDepth>$height</PackageDepth>
        		<PackageLength>$length</PackageLength>
        		<PackageWidth>$width</PackageWidth>";
			}
        	$requestXmlBody .= "<ShippingPackage>PackageThickEnvelope</ShippingPackage>";
			list($lbs,$oz) = explode('.', $weight);
			$oz = number_format($oz);			
			if($oz >= 16){
				$lbs += floor($oz/16);
				$oz = $oz % 16;
			}
			$requestXmlBody .= "
        		<WeightMajor unit=\"lbs\">$lbs</WeightMajor>
        		<WeightMinor unit=\"oz\">$oz</WeightMinor>
      		</CalculatedShippingRate>
      		<ShippingServiceOptions>
	        	<ShippingService>UPSGround</ShippingService>
    	    	<ShippingServicePriority>1</ShippingServicePriority>
      		</ShippingServiceOptions>
      		<ShippingServiceOptions>
        		<ShippingService>UPS3rdDay</ShippingService>
	        	<ShippingServicePriority>2</ShippingServicePriority>
      		</ShippingServiceOptions>
      		<ShippingServiceOptions>
	        	<ShippingService>UPS2ndDay</ShippingService>
    	    	<ShippingServicePriority>3</ShippingServicePriority>
      		</ShippingServiceOptions>";
			$requestXmlBody .= "<ShippingType>Calculated</ShippingType>";
			*/
//end old
//begin new		
		list($lbs,$oz) = explode('.', $weight);
			$oz = number_format($oz);			
			if($oz >= 16){
				$lbs += floor($oz/16);
				$oz = $oz % 16;
			}
		  // Items weight under 1 pound Shipping should for first piece $4.99 and each additional piece $0.99
		  // Expedited $35 + 3.99 each addl. piece
		  if ($lbs == 0 && $oz > 0)
		  {
		  	$per_item_weight_charge_standard = .99;
		  	$single_item_standard_charge = 4.99 + $per_item_weight_charge_standard;		  		 
		  
		  	$per_item_weight_charge_expedited = 3.99;
		  	$single_item_expedited_charge = 35 + $per_item_weight_charge_expedited;
		  }
		  else 
		  {
		  	if ($oz > 0) $lbs_to_use = $lbs + 1;
		  	else $lbs_to_use = $lbs;
		  
		  	$per_shipment_standard = 6.49;
		  	$per_lb_standard = .49;
		  
		  	$per_item_weight_charge_standard = round($lbs_to_use * $per_lb_standard, 2);
		  	$single_item_standard_charge = $per_shipment_standard + $per_item_weight_charge_standard;		  		 
		  
		  	$per_shipment_expedited = 50;
		  	$per_lb_expedited = 2;
		  
		  	$per_item_weight_charge_expedited = round($lbs_to_use * $per_lb_expedited, 2);
		  	$single_item_expedited_charge = $per_shipment_expedited + $per_item_weight_charge_expedited;
		  }
			$requestXmlBody .= "<CalculatedShippingRate>
        		<WeightMajor unit=\"lbs\">$lbs</WeightMajor>
        		<WeightMinor unit=\"oz\">$oz</WeightMinor>
        		<OriginatingPostalCode>10901</OriginatingPostalCode>
      		</CalculatedShippingRate>";
      		$requestXmlBody .= "<RateTableDetails/>";
        	//<DomesticRateTable>Default</DomesticRateTable>
      		//</RateTableDetails>";
		 //$requestXmlBody .= "<ShippingType>FlatDomesticCalculatedInternational</ShippingType>";
		 $requestXmlBody .= "<ShippingType>Calculated</ShippingType>";
		  $requestXmlBody .= "<ShippingServiceOptions>        
        <ShippingService>UPSGround</ShippingService>
        <ShippingServicePriority>1</ShippingServicePriority>";
        //$requestXmlBody .= "<ShippingServiceCost>$single_item_standard_charge</ShippingServiceCost>       
        //<ShippingServiceAdditionalCost>$per_item_weight_charge_standard</ShippingServiceAdditionalCost>        		  
      	$requestXmlBody .= "</ShippingServiceOptions><ShippingServiceOptions>        
        <ShippingService>UPS2ndDay</ShippingService>
        <ShippingServicePriority>2</ShippingServicePriority>";
        //$requestXmlBody .= "<ShippingServiceCost>$single_item_expedited_charge</ShippingServiceCost>
        //<ShippingServiceAdditionalCost>$per_item_weight_charge_expedited</ShippingServiceAdditionalCost>        		  
      	$requestXmlBody .= "</ShippingServiceOptions>
		<InternationalShippingServiceOption>
		    <ShippingService>USPSPriorityMailInternational</ShippingService>
            <ShippingServicePriority>1</ShippingServicePriority>
        <ShipToLocation>Worldwide</ShipToLocation>        
       </InternationalShippingServiceOption>";		
		  	
//end new			
		}
		$requestXmlBody .= "</ShippingDetails>";
		
		$requestXmlBody .= '</Item>';
		$requestXmlBody .= "</{$verb}Request>";
		//echo $requestXmlBody; 
		 //exit;

        $responseDoc = self::getResponse($verb,$requestXmlBody,false);

        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
//echo $responseDoc->saveXML();
		//if there are error nodes
		if($errors->length > 0)
		{
			//return false;
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			// if is first time trying to revise it and get error that can't revise title
			// because item already sold, try to revise it again, leaving out the name
			if ($code->item(0)->nodeValue == "10039" && $itemTitle != '')
			{
				//echo "<br/>Had error that can't change title, trying again without changing title";
				EbayAPI::reviseItem($itemId, $quantitySold, '', $itemDescription,
				      $startPrice, $buyItNowPrice, $gallery_url, $primaryCategory, $secondaryCategory,
				      $primaryStoreCategory, $secondaryStoreCategory, $store_id, $qty,$sku, $weight, $length, $width, $height);				
				return true;
					
			} 
			//Display code and shortmessage
			echo '<P><B>eBay returned the following error(s):</B>';
			echo '<br/>', $code->item(0)->nodeValue, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0)
				echo '<BR>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				//echo "endTime = $endTime <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				//echo "itemID = $itemID <BR />\n";


				$currentPrice = $response->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
				$bidCount = $response->getElementsByTagName('BidCount')->item(0)->nodeValue;
				$reserveMet = $response->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
				$quantitySold = $response->getElementsByTagName('QuantitySold')->item(0)->nodeValue;
            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return true;
		} else {
			return false;
		}

    }

	public static function getSellerEvents($ModTimeFrom = '', $ModTimeTo = '',$store=''){
    	global $CFG;

		$verb = 'GetSellerEvents';

    	$verified = false;
		$ret = array();

		if($store){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetSellerEventsRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
  		$requestXmlBody .= "<ModTimeFrom>$ModTimeFrom</ModTimeFrom>";
  		$requestXmlBody .= "<ModTimeTo>$ModTimeTo</ModTimeTo>";
		$requestXmlBody .= '</GetSellerEventsRequest>';

        $responseDoc = self::getResponse($verb,$requestXmlBody);


        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');


		//if there are error nodes
		if($errors->length > 0)
		{
			$err = '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$err .= '<P>'. $code->item(0)->nodeValue. ' : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it

			if(count($longMsg) > 0)
				$err .= '<BR>' . str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			return array('error' => $err);

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$itemArrays  = $response->getElementsByTagName("ItemArray");
				foreach($itemArrays as $itemArray){
					$items = $itemArray->getElementsByTagName('Item');
					foreach($items as $item){
						$itemID = $item->getElementsByTagName('ItemID')->item(0)->nodeValue;
						$quantitySold = $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;

						if((int)$quantitySold == 0)
							continue;

						$ret[$itemID] = array(
							'quantity_sold' => $quantitySold
						);
					}
				}

            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return $ret;
		} else {
			return false;
		}
	}

	//unused for now
	public static function getOrders($CreateTimeFrom  = '', $CreateTimeTo  = '', $order_ids = '', $store_id=''){
    	global $CFG;

		if( !$store_id )
			return false;

		$store = Store::get1( $store_id );

		if($store){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}


		$verb = 'GetOrders';

    	$verified = false;
    	$ret_fees = array();

		$CreateTimeFrom = date( 'c', strtotime( 'August 15, 2011') );
		$CreateTimeTo = date( 'c' );
		
		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
  		//$requestXmlBody .= "<CreateTimeFrom>$CreateTimeFrom</CreateTimeFrom>";
  		//$requestXmlBody .= "<CreateTimeTo>$CreateTimeTo</CreateTimeTo>";
		
		if( $order_ids ) {
		    if( !is_array( $order_ids ) )
			$order_ids = array( $order_ids );
		    
		    $requestXmlBody .= '<OrderIDArray>';
		    foreach( $order_ids as $o_id )
		    {
			$requestXmlBody .= '<OrderID>' . $o_id . '</OrderID>';
		    }
		    $requestXmlBody .= '</OrderIDArray>';
		}
		
		
		$requestXmlBody .= '<OrderRole>Seller</OrderRole>';
		$requestXmlBody .= '<OrderStatus>Completed</OrderStatus>';

		$requestXmlBody .= '</GetOrdersRequest>';
//echo $requestXmlBody;
        $responseDoc = self::getResponse($verb,$requestXmlBody,false);
        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');

		//if there are error nodes
		if($errors->length > 0)
		{
			echo '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			echo '<P>', $code->item(0)->nodeValue, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			return false;

			if(count($longMsg) > 0)
				echo '<BR>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
	    
	    $orders = array();
	    
            foreach ($responses as $response) {
		
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

//				$endTimes  = $response->getElementsByTagName("EndTime");
//				$endTime   = date('Y-m-d h:i:s',strtotime($endTimes->item(0)->nodeValue));
//				//echo "endTime = $endTime <BR />\n";
//
//				$itemArrays  = $response->getElementsByTagName("ItemArray");
//				foreach($itemArrays as $itemArray){
//					$items = $itemArray->getElementsByTagName('Item');
//					foreach($items as $item){
//						$itemID = $item->getElementsByTagName('ListingStatus')->item(0)->nodeValue;
//						echo "$itemID<br/>";
//						echo $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;
//
//					}
//				}
//				$itemIDs  = $response->getElementsByTagName("ItemID");
//				$itemID   = $itemIDs->item(0)->nodeValue;
//				//echo "itemID = $itemID <BR />\n";
//
//				$linkBase = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";
//				//echo "<a href=$linkBase" . $itemID . ">$itemTitle</a> <BR />";
//
//				$currentPrice = $response->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
//				$bidCount = $response->getElementsByTagName('BidCount')->item(0)->nodeValue;
//				$reserveMet = $response->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
//				$quantitySold = $response->getElementsByTagName('QuantitySold')->item(0)->nodeValue;
				
				$orders	    = $response->getElementsByTagName( 'OrdersArray' )->item(0);
				//$order	    = $orders->getElementsByTagName( 'Order' )->item(0);

				$subTotal = $response->getElementsByTagName('Subtotal')->item(0)->nodeValue;

				$order_id = $response->getElementsByTagName('OrderID')->item(0)->nodeValue;
				
				$tax_rate = $response->getElementsByTagName('SalesTaxPercent')->item(0)->nodeValue;
				$shipping = $response->getElementsByTagName('ShippingServiceSelected')->item(0);
				$shipping_cost = $shipping->getElementsByTagName('ShippingServiceCost')->item(0)->nodeValue;

//				print_ar($tax_rate);
//				print_ar($shipping_cost);
//				print_ar( $subTotal );
				
				$orders[ $order_id ] = array( 
				    'order_id'	=> $order_id,
				    'shipping'	=> $shipping_cost,
				    'subtotal'	=> $subTotal,
				    'tax_rate'	=> $tax_rate,
				);
            } // foreach response

		} // if $errors->length > 0


		return $orders;
	}

	/**
	 * Note: for a given eBay ID, might return multiple transactions for multi-item listings.
	 *
	 * @param unknown_type $itemId
	 * @param unknown_type $ModTimeFrom
	 * @param unknown_type $ModTimeTo
	 * @param unknown_type $store
	 * @return unknown
	 */
	public static function getGetItemTransactions ($itemId = 0, $ModTimeFrom  = '', $ModTimeTo  = '',$store='', $tx_id=''){
    	global $CFG;

		$verb = 'GetItemTransactions';

    	$verified = false;
    	$ret_fees = array();

    	if($store){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetItemTransactions xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
		if($ModTimeFrom){
  			$requestXmlBody .= "<ModTimeFrom>$ModTimeFrom</ModTimeFrom>";
		}
		if($ModTimeTo){
  			$requestXmlBody .= "<ModTimeTo>$ModTimeTo</ModTimeTo>";
		}

		$requestXmlBody .= "  <IncludeContainingOrder>true</IncludeContainingOrder>";

		$requestXmlBody .= "<ItemID>$itemId</ItemID>";
		if($tx_id){
			$requestXmlBody .= "<TransactionID>$tx_id</TransactionID>";
		}
		$requestXmlBody .= '</GetItemTransactions>';

        $responseDoc = self::getResponse($verb,$requestXmlBody,false);

        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');

		//if there are error nodes
		if($errors->length > 0)
		{
			$err = '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$err .= '<P>'. $code->item(0)->nodeValue. ' : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it

			if(count($longMsg) > 0)
				$err .= '<BR>' . str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			return array('error' => $err);

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = date('Y-m-d h:i:s',strtotime($endTimes->item(0)->nodeValue));
				//echo "endTime = $endTime <BR />\n";

				$itemArrays  = $response->getElementsByTagName("ItemArray");
				foreach($itemArrays as $itemArray){
					$items = $itemArray->getElementsByTagName('Item');
					foreach($items as $item){
						$itemID = $item->getElementsByTagName('ListingStatus')->item(0)->nodeValue;
						echo "$itemID<br/>";
						echo $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;

					}
				}
				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				//echo "itemID = $itemID <BR />\n";

				$transactions = $response->getElementsByTagName('Transaction');

				foreach($transactions as $tran){

					$orderID = $response->getElementsByTagName('OrderID')->item(0)->nodeValue;
					$currentPrice = $response->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
					$bidCount = $response->getElementsByTagName('BidCount')->item(0)->nodeValue;
					$reserveMet = $response->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
					$quantityPurchased = $tran->getElementsByTagName('QuantityPurchased')->item(0)->nodeValue;
					$date = $tran->getElementsByTagName('CreatedDate')->item(0)->nodeValue;

					$amountPaid                  = $response->getElementsByTagName('AmountPaid')->item(0)->nodeValue;
					$isPaid                      = ( 'NoPaymentFailure' == $tran->getElementsByTagName('eBayPaymentStatus')->item(0)->nodeValue ) && $amountPaid > 0.00;

					if($CFG->in_testing){
						$isPaid = true;
					}

					// unique transactionID
					$transactionID  = $tran->getElementsByTagName('TransactionID')->item(0)->nodeValue;

					$buyer_node = $tran->getElementsByTagName('Buyer')->item(0);

					$shippingAddress['name']     = $buyer_node->getElementsByTagName('Name')->item(0)->nodeValue;
					$shippingAddress['email']    = $buyer_node->getElementsByTagName('Email')->item(0)->nodeValue;
					$shippingAddress['phone']    = $buyer_node->getElementsByTagName('Phone')->item(0)->nodeValue;
					$shippingAddress['street']   = $buyer_node->getElementsByTagName('Street1')->item(0)->nodeValue;
					$shippingAddress['street2']  = $buyer_node->getElementsByTagName('Street2')->item(0)->nodeValue;
					$shippingAddress['city']     = $buyer_node->getElementsByTagName('CityName')->item(0)->nodeValue;
					$shippingAddress['state']    = $buyer_node->getElementsByTagName('StateOrProvince')->item(0)->nodeValue;
					$shippingAddress['zip']      = $buyer_node->getElementsByTagName('PostalCode')->item(0)->nodeValue;

					$retArray[$transactionID] = array(
						'id'		    => $itemID,
						'transaction_id'    => $transactionID,
						'tx_id'		    => $transactionID,
						'end_time'	    => $date,
						'current_price'	    => $currentPrice,
						'bid_count'	    => $bidCount,
						'reserve_met'	    => ($reserveMet == 'true' ? 'Y' : 'N'),
						'quantity_sold'	    => $quantityPurchased,
						'shippingAddress'   => $shippingAddress,
						'is_paid'	    => $isPaid,
						'amount_paid'	    => $amountPaid,
						'order_id'	    => $orderID
					);

				}

            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return $retArray;
		} else {
			return false;
		}
	}

	public static function getSellerList($store='',$page_number=1){
		global $CFG;

		$verb = 'GetSellerList';

		$perPage = 200;

    	$verified = false;
		$ret = array();

		if(is_array($store)){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}
		print_ar( $CFG->ebay_token );

		// Get Everything Ending between 14 days ago and in the Next 60 Days
		$start_date = date('Y-m-d\TH:i:s.000\Z',strtotime('-14 days'));
		$end_date = date('Y-m-d\TH:i:s.000\Z',strtotime("+60 days"));

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<GranularityLevel>Medium</GranularityLevel>';
		$requestXmlBody .= '<Pagination>
								<EntriesPerPage>'.$perPage.'</EntriesPerPage>
								<PageNumber>'.$page_number.'</PageNumber>
							</Pagination>';
		$requestXmlBody .= '<EndTimeFrom>'.$start_date.'</EndTimeFrom>';
		$requestXmlBody .= '<EndTimeTo>'.$end_date.'</EndTimeTo>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
		$requestXmlBody .= '</GetSellerListRequest>';

		echo $requestXmlBody;

        $responseDoc = self::getResponse($verb,$requestXmlBody,false);

        //get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');


		//if there are error nodes
		if($errors->length > 0)
		{
			$err = '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$codeStr = $code->item(0)->nodeValue;

			if($codeStr == 340){
				// Page Number Out Of Range -- just return false so the loop can exit
				return false;
			}
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$err .= '<P>'. $code->item(0)->nodeValue. ' : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it

			if(count($longMsg) > 0)
				$err .= '<BR>' . str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			return array('error' => $err);

		} else { //no errors
			//get results nodes

			$retArray = array();
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$itemArrays  = $response->getElementsByTagName("ItemArray");
				foreach($itemArrays as $itemArray){
					$items = $itemArray->getElementsByTagName('Item');
					foreach($items as $item){
						$itemID = $item->getElementsByTagName('ItemID')->item(0)->nodeValue;
						$quantity = $item->getElementsByTagName('Quantity')->item(0)->nodeValue;
						$quantitySold = $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;
						$itemName = $item->getElementsByTagName('Title')->item(0)->nodeValue;
						$price = $item->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
						$item_url = $item->getElementsByTagName('ViewItemURL')->item(0)->nodeValue;
						$StartDate = $item->getElementsByTagName('StartTime')->item(0)->nodeValue;
						$EndDate = $item->getElementsByTagName('EndTime')->item(0)->nodeValue;

						$retArray[] = array(
							'item_id' => $itemID,
							'quantity' => $quantity,
							'quantitySold' => $quantitySold,
							'item_name' => $itemName,
							'price'  => $price,
							'item_url' => $item_url,
							'start_time' => $StartDate,
							'end_time' => $EndDate
						);

					}
				}

            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return $retArray;
		} else {
			return false;
		}
	}



	/**
	 * Note: for a given eBay ID, might return multiple transactions for multi-item listings.
	 *
	 * @param unknown_type $itemId
	 * @param unknown_type $ModTimeFrom
	 * @param unknown_type $ModTimeTo
	 * @param unknown_type $store
	 * @return unknown
	 */
	public static function getGetSellerTransactions ($itemId = 0, $ModTimeFrom  = '', $ModTimeTo  = '',$store='', $tx_id='', $page_number = 1){
    	global $CFG;

		$verb = 'GetSellerTransactions';

    	$verified = false;
    	$ret_fees = array();

    	if($store){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}
		
		
		$perPage = 200;

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= '<GetSellerTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";
//		if($ModTimeFrom){
//  			$requestXmlBody .= "<ModTimeFrom>$ModTimeFrom</ModTimeFrom>";
//		}
//		if($ModTimeTo){
//  			$requestXmlBody .= "<ModTimeTo>$ModTimeTo</ModTimeTo>";
//		}
		$requestXmlBody .= '<Pagination>
					    <EntriesPerPage>'.$perPage.'</EntriesPerPage>
					    <PageNumber>'.$page_number.'</PageNumber>
				    </Pagination>';
		$requestXmlBody .= "  <NumberOfDays>1</NumberOfDays>";

		$requestXmlBody .= "  <IncludeContainingOrder>true</IncludeContainingOrder>";

		//$requestXmlBody .= "<ItemID>$itemId</ItemID>";
//		if($tx_id){
//			$requestXmlBody .= "<TransactionID>$tx_id</TransactionID>";
//		}
		$requestXmlBody .= '</GetSellerTransactionsRequest>';

	        $responseDoc = self::getResponse($verb,$requestXmlBody,false);

		$errors = $responseDoc->getElementsByTagName('Errors');

		//if there are error nodes
		if($errors->length > 0)
		{
			$err = '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$err .= '<P>'. $code->item(0)->nodeValue. ' : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it

			if(count($longMsg) > 0)
				$err .= '<BR>' . str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			return array('error' => $err);

		} else { //no errors
			//get results nodes
            $responses = $responseDoc->getElementsByTagName("{$verb}Response");

            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = date('Y-m-d h:i:s',strtotime($endTimes->item(0)->nodeValue));
				//echo "endTime = $endTime <BR />\n";

				$itemArrays  = $response->getElementsByTagName("ItemArray");
				foreach($itemArrays as $itemArray){
					$items = $itemArray->getElementsByTagName('Item');
					foreach($items as $item){
						$itemID = $item->getElementsByTagName('ListingStatus')->item(0)->nodeValue;
						echo "$itemID<br/>";
						echo $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;

					}
				}

				$pagenumber_element = $response->getElementsByTagName("TotalNumberOfPages");
				$total_pages = $pagenumber_element->item(0)->nodeValue;
				$retArray['total_pages'] = $total_pages;
				//echo "itemID = $itemID <BR />\n";


				$transactions = $response->getElementsByTagName('Transaction');

				foreach($transactions as $tran){

					$itemIDs  = $tran->getElementsByTagName("ItemID");
					$itemID   = $itemIDs->item(0)->nodeValue;

					$orderID = $tran->getElementsByTagName('OrderID')->item(0)->nodeValue;
					$currentPrice = $tran->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
					$bidCount = $tran->getElementsByTagName('BidCount')->item(0)->nodeValue;
					$reserveMet = $tran->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
					$quantityPurchased = $tran->getElementsByTagName('QuantityPurchased')->item(0)->nodeValue;
					$date = $tran->getElementsByTagName('CreatedDate')->item(0)->nodeValue;

					$notes = $tran->getElementsByTagName('BuyerCheckoutMessage')->item(0)->nodeValue;
					$notes = self::strip_cdata( $notes );
					$amountPaid = $tran->getElementsByTagName('AmountPaid')->item(0)->nodeValue;
					$isPaid = (
								'NoPaymentFailure' == $tran->getElementsByTagName('eBayPaymentStatus')->item(0)->nodeValue )
								&& $amountPaid > 0.00
								&& ( 'Complete' == $tran->getElementsByTagName('CompleteStatus')->item(0)->nodeValue );

					if($CFG->in_testing){
						$isPaid = true;
					}

					// unique transactionID
					$transactionID  = $tran->getElementsByTagName('TransactionID')->item(0)->nodeValue;
					$externalTransactionID  = $tran->getElementsByTagName('ExternalTransactionID')->item(0)->nodeValue;

					// eBay User ID
					$ebayUserID  = $tran->getElementsByTagName('UserID')->item(0)->nodeValue;

					$buyer_info = $tran->getElementsByTagName('Buyer')->item(0);
					$buyer_node = $tran->getElementsByTagName('BuyerInfo')->item(0);
					$shipping_info_node = $buyer_node->getElementsByTagName('ShippingAddress')->item(0);

					$shipping_service_node = $tran->getElementsByTagName('ShippingServiceSelected')->item(0);
					$shipping_service = $shipping_service_node->getElementsByTagName('ShippingService')->item(0)->nodeValue;

					$shippingAmount = $tran->getElementsByTagName('ShippingServiceCost')->item(0)->nodeValue;
					
//					$shippingAmount	= $tran->getElementsByTagName('ActualShippingCost')->item(0)->nodeValue;
					
					$shippingAddress['name']     = $shipping_info_node->getElementsByTagName('Name')->item(0)->nodeValue;
					$shippingAddress['email']    = $buyer_info->getElementsByTagName('Email')->item(0)->nodeValue;
					$shippingAddress['phone']    = $shipping_info_node->getElementsByTagName('Phone')->item(0)->nodeValue;
					$shippingAddress['street']   = $shipping_info_node->getElementsByTagName('Street1')->item(0)->nodeValue;
					$shippingAddress['street2']  = $shipping_info_node->getElementsByTagName('Street2')->item(0)->nodeValue;
					$shippingAddress['city']     = $shipping_info_node->getElementsByTagName('CityName')->item(0)->nodeValue;
					$shippingAddress['state']    = $shipping_info_node->getElementsByTagName('StateOrProvince')->item(0)->nodeValue;
					$shippingAddress['zip']      = $shipping_info_node->getElementsByTagName('PostalCode')->item(0)->nodeValue;
					$shippingAddress['country']  = $shipping_info_node->getElementsByTagName('CountryName')->item(0)->nodeValue;					
					
					$shippingDetails	     = $tran->getElementsByTagName("ShippingDetails")->item(0);
					//$shippingDetail		     = $shippingDetails->item(0)->nodeValue;
					
					$salesTax		     = $shippingDetails->getElementsByTagName('SalesTax')->item(0);
					//print_ar( $salesTax );
					$salesTaxPercentage	     = $salesTax->getElementsByTagName('SalesTaxPercent')->item(0)->nodeValue;
					$salesTaxAmount		     = $salesTax->getElementsByTagName('SalesTaxAmount')->item(0)->nodeValue;										

					$retArray[$transactionID] = array(
						'id'		    => $itemID,
						'transaction_id'    => $transactionID,
						'tx_id'		    => $transactionID,
						'end_time'	    => $date,
						'current_price'	    => $currentPrice,
						'bid_count'	    => $bidCount,
						'reserve_met'	    => ($reserveMet == 'true' ? 'Y' : 'N'),
						'quantity_sold'	    => $quantityPurchased,
						'shippingAddress'   => $shippingAddress,
						'is_paid'	    => $isPaid,
						'amount_paid'	    => $amountPaid,
						'order_id'	    => $orderID,
						'ebay_user_id'	    => $ebayUserID,
					        'notes'		    => $notes,
						'tax_rate'	    => $salesTaxPercentage,
						'tax_amount'	    => $salesTaxAmount,
						'shipping_amount'   => $shippingAmount,
						'external_transaction_id' => $externalTransactionID,
						'shipping_service' => $shipping_service
					);

				}

            } // foreach response

		} // if $errors->length > 0


		if($verified){
			return $retArray;
		} else {
			return false;
		}
	}


	static function completeSale( $order_id )
	{
	    global $CFG;

		$verb = 'CompleteSale';

		$verified = false;
		$ret_fees = array();
		$order = Orders::get1( $order_id );
		$store = Store::get1($order['order_tax_option']);
		if($store){
			$CFG->ebay_token = $store['ebay_token'];
			$CFG->ebay_dev_id = $store['ebay_dev_id'];
			$CFG->ebay_app_id = $store['ebay_app_id'];
			$CFG->ebay_cert_id = $store['ebay_cert_id'];
		}

		$perPage = 200;

		///Build the request Xml string
		$requestXmlBody  = '<?xml version="1.0" encoding="utf-8"?>';
		$requestXmlBody .= "<{$verb}Request xmlns=\"urn:ebay:apis:eBLBaseComponents\">";
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$CFG->ebay_token</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		$requestXmlBody .= '<ErrorLanguage>en_US</ErrorLanguage>';
		$requestXmlBody .= "<Version>" . self::COMPAT_LEVEL . "</Version>";

		/*
		 * Options -
		 * --Feedback?
		 *
		 * <FeedbackInfo> FeedbackInfoType
			<CommentText> string </CommentText>
			<CommentType> CommentTypeCodeType </CommentType>
			<TargetUser> UserIDType (string) </TargetUser>
		      </FeedbackInfo>
		 *
		 *
		 */

		$order = Orders::get1( $order_id );
		$shipments = Orders::getTracking(0, $order_id);
		$ebay_order_id_parts = explode("-",$order['ebay_order_id']);
		 
		//This ID is either an Order ID - multiple items, or a transaction ID, single.
		$ebay_order = EbayEventReport::getItems('', '', '', '', $order['ebay_order_id'] );

		$ebay_order_id = true;
		if( !$ebay_order )
		{
			$ebay_order = EbayEventReport::getItems('', '',$ebay_order_id_parts[0] , $ebay_order_id_parts[1], '' );			
		    $ebay_order_id = false;
		}

		if( $ebay_order_id )
		{
		   $requestXmlBody .= <<<XML
		    <OrderID>{$ebay_order[0][order_id]}</OrderID>
XML;
		}
		else {
		$requestXmlBody .= <<<XML
		    <ItemID>{$ebay_order[item_id]}</ItemID>
		    <TransactionID>{$ebay_order[tx_id]}</TransactionID>
XML;
		}

		$requestXmlBody .= <<<XML
		      <Paid>true</Paid>
		      <Shipped>true</Shipped>
XML;

		if( $shipments )
		{
		    $requestXmlBody .= "<Shipment>";
		    foreach( $shipments as $shipment )
		    {
		    $service_name_words = explode(" ",$shipment[service_name]);
			$requestXmlBody .= <<<XML
			<ShipmentTrackingDetails>
			  <ShipmentTrackingNumber>{$shipment[tracking_number]}</ShipmentTrackingNumber>
			  <ShippingCarrierUsed>{$service_name_words[0]}</ShippingCarrierUsed>
			</ShipmentTrackingDetails>
XML;
		    }
		    $requestXmlBody .= "<ShippedTime>" . db_date($shipments[0]['ship_date'], 'c') . "</ShippedTime>";
		    $requestXmlBody .= "</Shipment>";
		}

		$requestXmlBody .= "</{$verb}Request>";

		$responseDoc = self::getResponse($verb,$requestXmlBody,false);

		$errors = $responseDoc->getElementsByTagName('Errors');

		//if there are error nodes
		if($errors->length > 0)
		{
			$err = '<P><B>eBay returned the following error(s):</B>';
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $errors->item(0)->getElementsByTagName('ErrorCode');
			$shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
			$longMsg  = $errors->item(0)->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$err .= '<P>'. $code->item(0)->nodeValue. ' : '. str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it

			if(count($longMsg) > 0)
				$err .= '<BR>' . str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));

			return array('error' => $err, 'xmlBody' => $requestXmlBody);

		} else {
		    //Went through not sure anything else needs to be stored... perhaps mark that it was sent

		    $order_info['ebay_ship_confirm'] = 'Y';
		    Orders::update( $order_id, $order_info );
		    return true;
		}
	}
	
	static function strip_cdata($string)
	{
	    preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $string, $matches);
	    return str_replace($matches[0], $matches[1], $string);
	} 
	
	
	static function testFromDB( $id )
	{
	    global $CFG;
	    $verb = 'GetSellerTransactions';
	    //$responses = $responseDoc->getElementsByTagName("{$verb}Response");

	    $log = EbayEventReport::get1( $id );
	    
	    $responseDoc = new DomDocument();
	    
	    $responseDoc->loadXML($log['response_xml']);

	    
	    ?>

	    <?
	    
	    $responses = $responseDoc->getElementsByTagName("{$verb}Response");
	  //  print_ar( $responses );
            foreach ($responses as $response) {
				$acks = $response->getElementsByTagName("Ack");
				$ack  = $acks->item(0)->nodeValue;

				if($ack == 'Success'){
					$verified = true;
				}

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = date('Y-m-d h:i:s',strtotime($endTimes->item(0)->nodeValue));
				//echo "endTime = $endTime <BR />\n";

				$itemArrays  = $response->getElementsByTagName("ItemArray");
				foreach($itemArrays as $itemArray){
					$items = $itemArray->getElementsByTagName('Item');
					foreach($items as $item){
						$itemID = $item->getElementsByTagName('ListingStatus')->item(0)->nodeValue;
						echo "$itemID<br/>";
						echo $item->getElementsByTagName('QuantitySold')->item(0)->nodeValue;

					}
				}

				$pagenumber_element = $response->getElementsByTagName("TotalNumberOfPages");
				$total_pages = $pagenumber_element->item(0)->nodeValue;
				$retArray['total_pages'] = $total_pages;
				//echo "itemID = $itemID <BR />\n";


				$transactions = $response->getElementsByTagName('Transaction');

				foreach($transactions as $tran){

					$itemIDs  = $tran->getElementsByTagName("ItemID");
					$itemID   = $itemIDs->item(0)->nodeValue;

					$orderID = $tran->getElementsByTagName('OrderID')->item(0)->nodeValue;
					$currentPrice = $tran->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
					$bidCount = $tran->getElementsByTagName('BidCount')->item(0)->nodeValue;
					$reserveMet = $tran->getElementsByTagName('ReserveMet')->item(0)->nodeValue;
					$quantityPurchased = $tran->getElementsByTagName('QuantityPurchased')->item(0)->nodeValue;
					$date = $tran->getElementsByTagName('CreatedDate')->item(0)->nodeValue;

					$notes = $tran->getElementsByTagName('BuyerCheckoutMessage')->item(0)->nodeValue;
					$notes = self::strip_cdata( $notes );
					$amountPaid = $tran->getElementsByTagName('AmountPaid')->item(0)->nodeValue;
					$isPaid = (
								'NoPaymentFailure' == $tran->getElementsByTagName('eBayPaymentStatus')->item(0)->nodeValue )
								&& $amountPaid > 0.00
								&& ( 'Complete' == $tran->getElementsByTagName('CompleteStatus')->item(0)->nodeValue );

					if($CFG->in_testing){
						$isPaid = true;
					}

					// unique transactionID
					$transactionID  = $tran->getElementsByTagName('TransactionID')->item(0)->nodeValue;

					// eBay User ID
					$ebayUserID  = $tran->getElementsByTagName('UserID')->item(0)->nodeValue;

					$buyer_info = $tran->getElementsByTagName('Buyer')->item(0);
					$buyer_node = $tran->getElementsByTagName('BuyerInfo')->item(0);
					$shipping_info_node = $buyer_node->getElementsByTagName('ShippingAddress')->item(0);

					$shipping_service_node = $tran->getElementsByTagName('ShippingServiceSelected')->item(0);
					$shippingAmount = $tran->getElementsByTagName('ShippingServiceCost')->item(0)->nodeValue;
					
					//$shippingAmount	= $tran->getElementsByTagName('ActualShippingCost')->item(0)->nodeValue;
					
					$shippingAddress['name']     = $shipping_info_node->getElementsByTagName('Name')->item(0)->nodeValue;
					$shippingAddress['email']    = $buyer_info->getElementsByTagName('Email')->item(0)->nodeValue;
					$shippingAddress['phone']    = $shipping_info_node->getElementsByTagName('Phone')->item(0)->nodeValue;
					$shippingAddress['street']   = $shipping_info_node->getElementsByTagName('Street1')->item(0)->nodeValue;
					$shippingAddress['street2']  = $shipping_info_node->getElementsByTagName('Street2')->item(0)->nodeValue;
					$shippingAddress['city']     = $shipping_info_node->getElementsByTagName('CityName')->item(0)->nodeValue;
					$shippingAddress['state']    = $shipping_info_node->getElementsByTagName('StateOrProvince')->item(0)->nodeValue;
					$shippingAddress['zip']      = $shipping_info_node->getElementsByTagName('PostalCode')->item(0)->nodeValue;
					
					$shippingDetails	     = $tran->getElementsByTagName("ShippingDetails")->item(0);
					//$shippingDetail		     = $shippingDetails->item(0)->nodeValue;
					
					$salesTax		     = $shippingDetails->getElementsByTagName('SalesTax')->item(0);
					//print_ar( $salesTax );
					$salesTaxPercentage	     = $salesTax->getElementsByTagName('SalesTaxPercent')->item(0)->nodeValue;
					$salesTaxAmount		     = $salesTax->getElementsByTagName('SalesTaxAmount')->item(0)->nodeValue;
					
					

					$retArray[$transactionID] = array(
						'id'		    => $itemID,
						'transaction_id'    => $transactionID,
						'tx_id'		    => $transactionID,
						'end_time'	    => $date,
						'current_price'	    => $currentPrice,
						'bid_count'	    => $bidCount,
						'reserve_met'	    => ($reserveMet == 'true' ? 'Y' : 'N'),
						'quantity_sold'	    => $quantityPurchased,
						'shippingAddress'   => $shippingAddress,
						'is_paid'	    => $isPaid,
						'amount_paid'	    => $amountPaid,
						'order_id'	    => $orderID,
						'ebay_user_id'	    => $ebayUserID,
					        'notes'		    => $notes,
						'tax_rate'	    => $salesTaxPercentage,
						'tax_amount'	    => $salesTaxAmount,
						'shipping_amount'   => $shippingAmount,
					);

				}

            } // foreach response

	    return $retArray;
	}
}