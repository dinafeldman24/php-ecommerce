<?php

class ResourceLoader {

	public $base_dir; // string
	public $files; // array of filepaths
	

	public function __construct($type='css',$base_dir='',$extra_vars=''){
		global $CFG;

		$this->files = array();

		if($base_dir){
			$this->base_dir = $base_dir;
		} else {
			$this->base_dir = $CFG->dirroot . "/";
		}
			if($type == 'css'){
				$this->files[] = $CFG->redesign_dirroot.'/css/all.css';
				$this->files[] = $CFG->redesign_dirroot.'/css/responsive.css';
				//$this->files[] = $CFG->redesign_dirroot.'/css/print-min.css';
				//$this->files[] = $this->base_dir.'css/jquery-ui.smoothness.min.css';
			} else if($type == 'js'){				
				//$this->files[] =$CFG->redesign_dirroot . "/js/prototype.min.js";
				//$this->files[] = $CFG->sharedjs_dir . "/jquery.tools.min.js";
				$this->files[] = $CFG->redesign_dirroot     . "/js/jquery.noconflict.js";	
				//$this->files[] = $CFG->sharedjs_dir . "/scriptaculous.all.min.js";											
				$this->files[] = $CFG->redesign_dirroot.'/js/jquery.main.js';
				$this->files[] = $CFG->redesign_dirroot.'/js/sitefunctions.js';
				//$this->files[] = $CFG->sharedjs_dir . "/jquery-ui.min.js";					
				$this->files[] = $CFG->redesign_dirroot.'/js/tinybox-min.js';
				//$this->files[] = $CFG->redesign_dirroot.'/js/jquery.ddslick-min.js';
				//$this->files[] = $CFG->redesign_dirroot.'/solr/solr_search.min.js';
				//$this->files[] = $CFG->assets_dir .'/js/email_signup.js';
			}
	}

	static function generateURLTag($type="css",$vars='', $cfg_obj=null){

		global $CFG;
		
		$obj = new ResourceLoader($type,'',$vars);

		$max_time = 0;

		// Calculate latest file modify time
		foreach($obj->files as $f){
			$max_time = max($max_time,filemtime($f));
		}
		if ($cfg_obj) $url = isset( $_SERVER['HTTPS']) ?$cfg_obj->sslurl:$cfg_obj->baseurl;
		else $url = isset( $_SERVER['HTTPS']) ?$CFG->sslurl:$CFG->baseurl;
		
		$url .="resource_loader.php?get_resources=1&amp;type=$type&amp;ts=$max_time";

		if(is_array($vars)){
			foreach($vars as $key => $v){
				$url .= "&$key=$v";
			}
		}

		if($type == "css"){
			//$url = "/css" . $url;
		}

		return $url;
	}
	
	static function isIE(){
		list($xxx,$browser,$version) = self::browser_info();

		if($browser == "msie" && $version < 7){
			return true;
		}

		return false;
	}

	static function browser_info($agent=null) {
		
		// Declare known browsers to look for
		$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
		'konqueror', 'gecko');

		// Clean up agent and build regex that matches phrases for known browsers
		// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
		// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
		$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
		$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

		// Find all phrases (or return empty array if none found)
		if (!preg_match_all($pattern, $agent, $matches)) return array();

		// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
		// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
		// in the UA).  That's usually the most correct.
		$i = count($matches['browser'])-1;
		return array($matches['browser'][$i] => $matches['version'][$i]);
	}

    static function generateCDNUrl($type, $cfg_obj = null, $source = ''){
        if(!$cfg_obj){
            global $CFG;
            $cfg_obj = $CFG;
        }
        $file = $cfg_obj->assets_dir . '/combined.' . $type;
        $ts = filemtime($file);
        $url = isset( $_SERVER['HTTPS']) ? $cfg_obj->ssl_assets_cdn: $cfg_obj->assets_cdn;
//$url = $cfg_obj->ssl_assets_cdn;
 //       $url = $cfg_obj->ssl_assets_cdn;
//mail("rachel@tigerchef.com", "url", $url);
        return $url . '/combined.' . $type. '?ts=' . $ts;
    }

} // end class
