<?php

class OrderItemStatuses {
	
	function insert($info)
	{		
		return db_insert('order_item_statuses',$info);
	}
	
	function update($id,$info)
	{
		return db_update('order_item_statuses',$id,$info);
	}
	
	function delete($id)
	{
		return db_delete('order_item_statuses',$id);
	}
	
	function get($id=0)
	{
		$sql = "SELECT order_item_statuses.*
				FROM order_item_statuses
				WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND order_item_statuses.id = $id ";
		}
		
		$sql .= " GROUP BY order_item_statuses.id
				  ORDER BY order_item_statuses.step_no";
		
		return db_query_array($sql);
	}
	
	function get1($id)
	{
		$id = (int) $id;
		
		if (!$id) return false;
		
		$result = OrderItemStatuses::get($id);
		return $result[0];
	}
}

?>