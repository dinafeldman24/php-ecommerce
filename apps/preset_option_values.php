<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   August 11 2015
*
*****************/

class PresetOptionValues{
    
    function PresetOptionValues(){}

    static function updateOrInsert($info, $id=null){
        if(!is_null($id) && !empty($id) && $id){
            if($info['is_active'] == 0){
                //make sure to deactivate any product options 
                //that have this preset value
                $sql = "UPDATE product_options SET is_active = 'N' WHERE  preset_value_ids regexp ':" . $id . "[,}]' > 0";
                db_query($sql);
            }
            db_update('preset_option_values',$id, $info);
            $res = true;
        }
        else{
            $res = db_insert('preset_option_values', $info);
        }
        return $res;
    }
    
    static function get(){
        $sql = "SELECT pov.*, o.name as option_name 
        FROM preset_option_values pov JOIN option_presets op ON pov.preset_id = op.id 
        JOIN options o ON op.option_id = o.id";
        return db_query_array($sql);
    }

    static function delete($id){
        return db_delete('preset_option_values', $id);
    }

    static function updateOrder($preset_id, $order_info){
        $sql = 'INSERT IGNORE INTO preset_option_values(id, display_order) VALUES ';
        foreach($order_info as $key => $val){
            $sql .= "($val, $key), ";
        }
        $sql = rtrim($sql, ', ') . ' ON DUPLICATE KEY UPDATE display_order =
            VALUES(display_order)';
        $res = db_query($sql);
        echo json_encode(array('res' => var_export($res, 1), 'query' => $sql, 'order_info' => var_export($order_info, 1)));
            
    }
    
}

?>
