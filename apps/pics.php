<?php

class Pics {

	static function getProductThumbnailHref($product_row,$local_path=false){
		global $CFG;

		if(!$product_row){
			return '';
		}
		$name = preg_replace('/[^A-z0-9]/s','-',$product_row['name']);

		if($local_path){
			$path = $CFG->dirroot . '/itempics/' . $name . '-' . $product_row['id'] . $CFG->thumbnail_suffix;
		} else {
			$path = $CFG->baseurl . 'itempics/' . $name . '-' . $product_row['id'] . $CFG->thumbnail_suffix;
		}

		return $path;
	}

	static function getProductXLargeHref($product_row,$local_path=false){
		global $CFG;

		if(!$product_row){
			return '';
		}
		$name = preg_replace('/[^A-z0-9]/s','-',$product_row['name']);

		if($local_path){
			$path = $CFG->dirroot . '/itempics/' . $name . '-' . $product_row['id'] . $CFG->xlarge_suffix;
		} else {
			$path = $CFG->baseurl . 'itempics/' . $name . '-' . $product_row['id'] . $CFG->xlarge_suffix;
		}

		return $path;
	}

	static function get1Byurl($url){
  	$sql= "SELECT product_images.img_url FROM product_images WHERE product_images.img_url = '{$url}' ";
	  return db_query_array($sql);					
	} 
	
}