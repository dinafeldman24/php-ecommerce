<?php

class Specs {
	
	static function insertHeader( $info )
	{
		$header = Specs::getHeader( 0, $info['name'] );
		if( $header )
		{
			//echo'<h3> Header by that name already found. Next time Choose the Filter from the Existing List.</h3>';
			return $header[0]['id'];
		}
		
		return db_insert('spec_headers', $info );
	}
	
	static function updateHeader( $id, $name )
	{
		$info = array();
		$info['name'] = $name;
		
		return db_update( 'spec_headers', $id, $info );
	}
	
	static function deleteHeader( $id )
	{
		db_delete( 'spec_headers', $id );
		$sql = " DELETE FROM cat_spec_headers WHERE spec_header_id = $id ";
		db_query( $sql );
		$sql = " DELETE FROM cat_specs WHERE spec_header_id = $id ";
		return db_query( $sql );
	}
	
	static function insertCatHeader( $info )
	{
		$header = Specs::getCatHeaders( $info['cat_id'], $info['spec_header_id'] );
		if( $header )
		{
			echo'<h3> Header by that name already found. Next time Choose the Filter from the Existing List.</h3>';
			return $header[0]['id'];
		}
		
		return db_insert('cat_spec_headers', $info );
	}
	
	static function insertCatSpec( $info )
	{
		return db_insert('cat_specs', $info,'', true);
	}
	
	static function getHeader( $id=0, $name='', $cat_id=0, $order='' )
	{
		$sql = " SELECT * FROM spec_headers WHERE 1 ";
		if( $id )
			$sql .= " AND id = $id ";
		if( $name )
			$sql .= " AND UPPER(name) = UPPER('$name') ";
		if( $cat_id )
		{
			$sql .= " AND cat_id = $cat_id ";
		}
			
		if( $order !== '' )
		{
			$sql .= " ORDER BY $order ";
		}
			
		return db_query_array( $sql );
	}
	
	static function getCatHeaders( $cat_id, $spec_header_id=0, $order='' )
	{
		$sql = " SELECT cat_spec_headers.*, spec_headers.name FROM cat_spec_headers ";
		$sql .= " LEFT JOIN spec_headers on spec_headers.id = cat_spec_headers.spec_header_id ";
		$sql .= " WHERE 1 ";
		
		if( $spec_header_id )
			$sql .= " AND spec_header_id = $spec_header_id ";
		if( $cat_id )
		{
			$sql .= " AND cat_id = $cat_id ";
		}
			
		if( $order !== '' )
		{
			$sql .= " ORDER BY $order ";
		}
		else 
		{
			$sql .= " ORDER BY cat_spec_headers.display_order ";
		}
//		echo $sql;
		return db_query_array( $sql, 'spec_header_id' );
	}
	
	static function getValuesForHeader( $cat_id, $header_id )
	{
		$sql .= " SELECT cat_specs.*, filters.name ";
		$sql .= " FROM cat_specs ";
		$sql .= " LEFT JOIN filters on filters.id = cat_specs.filter_id ";
		$sql .= " WHERE 1 ";
		if( $cat_id )
			$sql .= " AND cat_specs.cat_id = $cat_id ";
		if( $header_id )
			$sql .= " AND cat_specs.spec_header_id = $header_id ";
		
		$sql .= " ORDER BY cat_specs.display_order ";
		//echo $sql;
		return db_query_array( $sql );
	}
	
	static function deleteCatSpecs( $cat_id, $header_id )
	{
		$sql = " DELETE FROM cat_specs WHERE cat_id = $cat_id and spec_header_id = $header_id ";
		return db_query( $sql );
	}
	
	static function deleteCatSpecHeader( $cat_id, $header_id )
	{
		$sql = " DELETE FROM cat_spec_headers WHERE cat_id = $cat_id and spec_header_id = $header_id ";
		return db_query( $sql );
	}
	
	static function deleteCatSpecHeaders( $cat_id )
	{
		$sql = " DELETE FROM cat_spec_headers WHERE cat_id = $cat_id ";
		return db_query( $sql );
	}
	
	static function editSpecSheet( $prod_id, $edit=true )
 	{
 		//Going to do this differently than the filters, bc its easier. 
 		//Children will use parent sheets unless a sheet is made for that child
 		//Could defautly load up the top spread sheet when they go to edit, would appear to inherit that way
 		$cat_list = Products::getProductCats( $prod_id );
 		$product = Products::get1( $prod_id );
 		$prod_cats = array();
 		foreach( $cat_list as $key=>$value )
 		{
 			$prod_cats[] = $key;
 		}
 	//	print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $prod_cats );
 	//	print_ar( $cats );
 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}
 		
 		if( !$headers )
 			return false;
 			
 		//print_ar( $headers );
 		$filters = Filters::getFiltersForCat( $filter_cat );
 		//print_ar( $filters );
 		
 		echo'<table class="spec_sheet">';
 		
 		foreach( $headers as $header )
 		{
 			$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
 			echo'<tr><th colspan="2" class="spec_header">' . $header['name'] .'</th></tr>';
 			
 			if( $specs )
 			foreach( $specs as $spec )
 			{
 			//	echo'<tr><td>' . $filter_cat . ' ' . $spec['filter_id'] . '</td></tr>';
 				echo'<tr><th class="spec_name">' . $spec['name'] . '</th>';
 				if( trim(strtoupper($spec['name'])) == 'WIDTH' || 
							trim(strtoupper($spec['name'])) == 'HEIGHT' || 
							trim(strtoupper($spec['name'])) == 'LENGTH' )
				{
					echo'<td class="spec_value">';
					echo Products::displayDimension( $spec['name'], 'product', $product['id'], true);
					echo'</td>';
				}
				else 
				{
 				$filter = $filters[$spec['filter_id']];
 				if( $filter )
 					$prod_filter = Products::getProductFilter( $prod_id, $filter['filter_id'] );
 				else 
 				{
 					$prod_filter = Products::getProductFilter( $prod_id, $spec['filter_id'] );
 				}
			//print_ar( $prod_filter );
				if( $prod_filter )
					$value = $prod_filter['value'];
				else
					$value = '';
					
 				if( $filter['type'] == 'select options' )
				{
					//Need a select box for these, any other just enter the value
					$options = Filters::getCatFilterOptions( $filter['cat_id'], $filter['filter_id'], true );
					if( !$options )
					{
						if( $edit )
							echo'ADD OPTIONS FOR THIS FILTER 1st';
						else 
							echo'<td class="spec_value"></td>';
					}
					else 
					{
						if( $edit )
							echo'<td>' . Filters::getCatFilterOptionSelect($filter['cat_id'], $filter['filter_id'], $value, 'info[filters][' . $filter['filter_id'] . ']') . '</td>';
						else 	
							echo'<td class="spec_value">' . $value .'</td>';
					}
				}
				else 
				{
					if( $edit )
					{
						echo'<td>';
					//	print_ar( $filter );
						echo'<input type="text" name="info[filters][' . $filter['filter_id'] . '] size="10" value="' . $value . '" />';
						echo'</td>';	
					}
					else 
					{
							echo'<td class="spec_value">' . $value .'</td>';
					}
				}
 				
 				}
 				echo'</tr>';
 			}
 		}
 		echo'</table>';
 	}
	
 	
 	static function getSpecifications( $product_id )
 	{
 		$sql = " SELECT * FROM product_specs WHERE product_id = $product_id ORDER by `order` ";
 		
 		return db_query_array( $sql );
 	}
 	static function insertSpecification( $info )
 	{
 		return db_insert('product_specs', $info );
 	}
 	static function deleteSpecification( $id )
 	{
 		return db_delete('product_specs', $id );
 	}
 	static function updateSpecification( $id, $info )
 	{
 		return db_update('product_specs', $id, $info );
 	}
 	
 	/*
 		Most of this class is being use for the compare function of the website
 		This function is pulling directly from product_specs and just outputing it
 	*/
 	static function displaySpecSheet( $product_id )
 	{
 		global $CFG; 
 		
 		$specs = Specs::getSpecifications( $product_id );
 		
 		$prod_cats = Products::getProductCats( $product_id );
 		if( is_array( $prod_cats ) )
			foreach( $prod_cats as $key => $value )
			{
				$prod_cats[$key] = $value['cat_id'];
			}
 		
		$product = Products::get1( $product_id );
		$product['filters'] = Products::getProductFilterValues( $product_id );
		
		$cats = Cats::getFilterSearchCats( $prod_cats );
 		$filter_cat = $cats[0];

 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}
 		
		
 		$ret = '<table class="product_specifications">';
 		if( $specs )
 		{
 			if( $headers )
	 		{
			
				if( $headers[$CFG->dimensions_header_id] )
		 			$dim_specs = Specs::getValuesForHeader( $headers[$CFG->dimensions_header_id]['cat_id'], $CFG->dimensions_header_id );
		 		else 
		 			$dim_specs = false;
	 			
	 			if( $dim_specs )
	 			{
	 				$ret .= '<tr class="product_specifications_header"><th colspan="2" class="product_specifications_header"> Dimensions </th></tr>';
	 				foreach( $dim_specs as $spec )
	 				{
	 					if( trim(strtoupper($spec['name'])) == 'WIDTH' || 
							trim(strtoupper($spec['name'])) == 'HEIGHT' || 
							trim(strtoupper($spec['name'])) == 'LENGTH' || 
							trim(strtoupper($spec['name'])) == 'WEIGHT' )
						{
							if( trim(strtoupper($spec['name'])) == 'WEIGHT' )
							{
								$quotes = false;
								$extra = " lbs.";
							}
							else 
							{
								$quotes = true;
								$extra = "";
							}
							$ret .= '<tr class="product_specifications_value">
	 						 <td class="product_specifications_title">' . $spec['name'] .':</td>
	 					     <td class="product_specifications_value">' . Products::displayDimension($spec['name'], 'product', $product['id'], $quotes) . $extra . '</td> </tr>';
						}
						else 
						{
							if( $product['filters'][$spec['filter_id']]['value'] == "" )
							{
								continue;
							}
							
							$ret .= '<tr class="product_specifications_value">
	 						 <td class="product_specifications_title">' . $spec['name'] .':</td>
	 					     <td class="product_specifications_value">' . $product['filters'][$spec['filter_id']]['value'] .'</td> </tr>';
						}
	 				}
	 			}
	 			
	 		}
 			
	 		foreach( $specs as $s )
	 		{
	 			if( $s['header'] == 'Y' )
	 			{
	 				if( $s['title'] == 'Dimensions' )
	 					continue;
	 				$ret .= '<tr class="product_specifications_header"><th colspan="2" class="product_specifications_header">' . $s['title'] .'</th></tr>';
	 			}
	 			else 
	 			{
	 				$ret .= '<tr class="product_specifications_value">
	 						 <td class="product_specifications_title">' . $s['title'] .':</td>
	 					     <td class="product_specifications_value">' . $s['value'] .'</td> </tr>';
	 			}
	 		}
 		}
 		$ret .= "</table>";
 		
 		return $ret;
 	}
 	
 	
}