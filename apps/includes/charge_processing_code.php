<?php
$error = false; 
if ($cc_processor == "paypal") {
		$paypalCcObj = new PaypalCc ();
		// $amount = number_format($total,2);
		$amount = $total;
		
		if ("success" != $paypalCcObj->capture ( $tx_id, $amount )) {
			if ("success" != $paypalCcObj->do_reference_transaction ( $tx_id, $amount, "Sale" )) {
				$error = true;
				echo "Paypal Response: " . $paypalCcObj->getL_LONGMESSAGE0 ();
				//return false;
			} else {
				$action = 'sale';
				$orig_id = $paypalCcObj->getTRANSACTIONID ();
				$resp_code = "1";
				$resp_msg = "This transaction has been approved.";
			}
		} else {
			$action = 'capture';
			$orig_id = $paypalCcObj->getTRANSACTIONID ();
			$resp_code = "1";
			$resp_msg = "This transaction has been approved.";
		}
		
		if($transaction['payment_method'] == 'Paypal Checkout'){
			$payment_method = 'paypal';
		}else{
			$payment_method = 'cc';
		}
		
		$charge_info = Array (
				'order_id' => $id,
				'action' => $action,
				'transaction_id' => $orig_id,
				'payment_method' => $payment_method,
				'pid' => $tx_id,
				'amount' => $amount,
				'response_code' => $resp_code,
				'response_msg' => $resp_msg,
				'user_id' => $_SESSION ['id'],
				'order_payment_id' => $transaction['order_payment_id'],
				'card_type' => $transaction['card_type'], 
		);

	} else if ($cc_processor == "amex") {
		$amex_obj = new AmexCC();	
		// $amount = number_format($total,2);
		$amount = $total;
		$result = $amex_obj->capture($transaction['amex_orderid'], substr(md5($tx_id.time()), 0 , 17), $amount);
		//$result = 'temp fail';
		if ($result != 'SUCCESS') {

			//get token
			$token = Orders::getToken($tx_id);
			
			$auth_fields = array('ordernum'			=> $id .'-'. time(),
					'transaction_id'	=> substr(md5($tx_id.time()), 0 , 17));
				
			$charge_info = Array (
							'order_id' => $id,
							'pid' => $tx_id,
							'amount' => $amount,
							'user_id' => $_SESSION ['id'],
							'card_type' => 'amex',
							'order_payment_id' => $transaction['order_payment_id'], 
							'amex_orderid' => $auth_fields['order_num'],
							);
	
			//authorize 
			$authorization_result = $amex_obj->authorize('','','',$amount,$auth_fields,$token);
			if ($authorization_result != 'SUCCESS') {
				$error = true;
				echo $amex_obj->getNiceErrorMessage();
				//return false;
			} else {
				//need to enter authorzation info into db
				
				$charge_info['action'] = 'authorize';
				$charge_info['transaction_id'] = $amex_obj->getTRANSACTIONID ();
				$charge_info['response_code'] = '1';
				$charge_info['response_msg'] = 'This transaction has been approved';
				Orders::insertCharge ( $charge_info );

				$capture_result = $amex_obj->capture ( $auth_fields['ordernum'], substr(md5($tx_id.time().'1'), 0 , 17), $amount);
				if ($capture_result != 'SUCCESS') {
					$error = true;
					echo $amex_obj->getNiceErrorMessage();
					//return false;
				} else {
					$action = 'capture';
					$orig_id = $amex_obj->getTRANSACTIONID ();
					$resp_code = "1";
					$resp_msg = "This transaction has been approved.";
				}
			}
		} else {
			$action = 'capture';
			$orig_id = $amex_obj->getTRANSACTIONID ();
			$resp_code = "1";
			$resp_msg = "This transaction has been approved.";
		}
				
		$charge_info = Array (
				'order_id' => $id,
				'action' => $action, 
				'transaction_id' => $amex_obj->getTRANSACTIONID (),
				'pid' => $tx_id,
				'amount' => $amount,
				'response_code' => $resp_code,
				'response_msg' => $resp_msg,
				'user_id' => $_SESSION ['id'],
				'card_type' => 'amex',
				'order_payment_id' => $transaction['order_payment_id'],
				'amex_orderid' => $amex_obj->getORDERID(),
		);
	} else if ($cc_processor == "authorize.net") {
		// authorize this amount
		// echo $order['transaction_id'] . ' ' . $total;
		if (! ($pf->capture ( $tx_id, $total ))) {
			$resp = $pf->getResponseMessage ();
			$resp_code = $pf->getResponseCode ();
			
			require_once ("RB/authorize.net.cim.php");
			// already did a capture on that transaction. Try to create new one
			// via CIM
			if ($order['order_tax_option'] == $CFG->able_kitchen_store_id)
			{			
				$authNetCim = new AuthNetCim ( $CFG->ablekitchen_authorizenet_user, $CFG->ablekitchen_authorizenet_pass, $CFG->authorizenet_in_testing );
			}
			else $authNetCim = new AuthNetCim ( $CFG->authorizenet_user, $CFG->authorizenet_pass, $CFG->authorizenet_in_testing );
			$authNetCim->setParameter ( 'customerProfileId', $order ['cim_profile_id'] );
			$authNetCim->getCustomerProfileRequest ();
			
			$payment_profile_id = $authNetCim->customerPaymentProfileId;
			$authNetCim->setParameter ( 'customerPaymentProfileId', $payment_profile_id );
			$amount = number_format ( $total, 2 );
			$authNetCim->setParameter ( 'transaction_amount', $amount );
			$authNetCim->setParameter ( 'transactionType', 'profileTransAuthCapture' );
			
			// Use CIM to charge customer again
			$respArray = $authNetCim->createCustomerProfileTransactionRequest ();
			
			if (! $authNetCim->isSuccessful ()) {
				$error = true;
				echo ("AIM Response: $resp ($resp_code)<hr />");
				echo ("CIM Response: " . implode ( ", ", $authNetCim->error_messages ) . "<br />" . $authNetCim->text . "<br />" . $authNetCim->directResponse) . "<hr />";
				//return false;
			} else {
				$action = 'sale';
				$orig_id = $authNetCim->_orig_id;
				$resp_code = $respArray ['responseCode'];
				$resp_msg = $respArray ['responseReasonText'];
			}
		} else {
			$action = 'capture';
			$orig_id = $pf->getTransactionID ();
			$resp_code = $pf->getResponseCode ();
			$resp_msg = $pf->getResponseMessage ();
		}
		
		$charge_info = Array (
				'order_id' => $id,
				'action' => $action,
				'transaction_id' => $orig_id,
				'amount' => $total,
				'response_code' => $resp_code,
				'response_msg' => $resp_msg,
				'user_id' => $_SESSION ['id'] 
		);
	} // end if cc processor is authorize.net
	elseif($cc_processor == 'emerchant' || $cc_processor == 'americard' || $cc_processor == 'fidelity'|| $cc_processor == 'cardknox-usaepay-emulator'){ //GW
        $emerchant_obj = new eMerchantCc($cc_processor);//GW
        $success = $emerchant_obj->capture($tx_id, $total);
        if(!$success){
            echo $cc_processor.' Response: ' . $emerchant_obj->getError();//GW
            $error = true;
            //return false;
        }
        else{
            $charge_info = array(
                'order_id' => $id,
                'pid' => Orders::getPID($tx_id),
                'order_payment_id' => $transaction['order_payment_id'],
                'action' => 'capture',
                'card_type' => $transaction['card_type'],
                'transaction_id' => $emerchant_obj->getRefNum(),
                'amount' => $total,
                'response_code' =>1,
                'user_id' => $_SESSION['id']
                );
        }
    }//end if cc processor is emerchant
    if (!$error)
    {		
		$charge_id = Orders::insertCharge ( $charge_info );
		Orders::insertOrderChargeBreakdown($charge_id, $sales_merchandise, $shipping_income, $sales_tax,$coupons, $sales_discounts);
	
		if (count ( $certificates ) > 0) 	// Moved it down here because I dont want to
	                                 // create the certificate until after the
	                                 // charge.
		{
			foreach ( $certificates as $cert ) {
				Gifts::generateCertificate ( $cert, $id );
			}
		}
	}
?>