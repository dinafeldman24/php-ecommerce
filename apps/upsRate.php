<?php
class upsRate {
    var $AccessLicenseNumber;  
    var $UserId;  
    var $Password;
    var $ShipperNumber; 
    var $credentials;

    function setCredentials($access,$user,$pass,$shipper) {
    	   
		$this->AccessLicenseNumber = $access;
		$this->UserID = $user;
		$this->Password = $pass;	
		$this->ShipperNumber = $shipper;
		$this->credentials = 1;
    }

    // Define the function getRate() - no parameters
    function getRate($PostalCode,$dest_zip,$service,$packages,$requestOption='Rate',$dimensions=null,$origin_state_code='',$dest_state_code='') {
		if ($this->credentials != 1) {
			print 'Please set your credentials with the setCredentials function';
			die();
		}
		//StateProvinceCode
		/*
		<PickupType>
		<Code>01</Code>
		</PickupType>
		<CustomerClassification>
		<Code>00</Code>
		</CustomerClassification>
		*/		
		 
		
		$data ="<?xml version=\"1.0\"?>  
			<AccessRequest xml:lang=\"en-US\">  
			    <AccessLicenseNumber>$this->AccessLicenseNumber</AccessLicenseNumber>  
			    <UserId>$this->UserID</UserId>  
			    <Password>$this->Password</Password>  
			</AccessRequest>  
			<?xml version=\"1.0\"?>  
			<RatingServiceSelectionRequest xml:lang=\"en-US\">  
			    <Request>  
				<TransactionReference>  
				    <CustomerContext>Bare Bones Rate Request</CustomerContext>  
				    <XpciVersion>1.0001</XpciVersion>  
				</TransactionReference>  
				<RequestAction>Rate</RequestAction>  
				<RequestOption>$requestOption</RequestOption>  
			    </Request>  
			<PickupType>
				<Code>01</Code>
			</PickupType>
			<Shipment>  
			    <Shipper>  
					<Address>  
					    <PostalCode>$PostalCode</PostalCode>  
					    <CountryCode>US</CountryCode>
					</Address>  
				    <ShipperNumber>$this->ShipperNumber</ShipperNumber>  
			    </Shipper>  
			    <ShipTo>  
					<Address>  
					    <PostalCode>$dest_zip</PostalCode>  
					    <CountryCode>US</CountryCode>
					    <StateProvinceCode>$dest_state_code</StateProvinceCode>  
						<ResidentialAddressIndicator/>  
					</Address>  
			    </ShipTo>  
			    <ShipFrom>  
					<Address>  
					    <PostalCode>$PostalCode</PostalCode>  
					    <CountryCode>US</CountryCode>
					    <StateProvinceCode>$origin_state_code</StateProvinceCode>  
					</Address>  
			    </ShipFrom>
			    <RateInformation>
					<NegotiatedRatesIndicator/>
				</RateInformation> 
			    "; 
		if($service){
			  $data .= "
			  		<Service>  
						<Code>$service</Code>  
				    </Service>
			  ";
		}

		foreach ($packages as $pkg){
		$data .="
				<Package>  
					<PackagingType>  
					    <Code>02</Code>  
					</PackagingType>
				"; 
		if($pkg['dimensions']){
			  $data .="
			  		<Dimensions>
						<UnitOfMeasurement>  
							<Code>IN</Code>  
					    </UnitOfMeasurement>
					    <Length>".ceil($pkg[dimensions][length])."</Length>
					    <Width>".ceil($pkg[dimensions][width])."</Width>
					    <Height>".ceil($pkg[dimensions][height])."</Height>   
					</Dimensions>
			  ";
		}  
		$data .="
					<PackageWeight>  
					    <UnitOfMeasurement>  
							<Code>LBS</Code>  
					    </UnitOfMeasurement>  
					    <Weight>".$pkg['weight']."</Weight>  
					</PackageWeight>  
			    </Package> ";
		}
		$data .= "
			</Shipment>  
		</RatingServiceSelectionRequest>";  
// 			$ch = curl_init("https://www.ups.com/ups.app/xml/Rate");

			//test server
// 			$ch = curl_init("https://wwwcie.ups.com/ups.app/xml/Rate");
		$timeB4 = microtime (true);
			
		
			//live server
			$ch = curl_init("https://onlinetools.ups.com/ups.app/xml/Rate");
		
			curl_setopt($ch, CURLOPT_HEADER, 1);  
			curl_setopt($ch,CURLOPT_POST,1);  
			curl_setopt($ch,CURLOPT_TIMEOUT, 60);  
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);  
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data);  
			$result=curl_exec ($ch);  
		    //echo '<!-- '. $result. ' -->'; // THIS LINE IS FOR DEBUG PURPOSES ONLY-IT WILL SHOW IN HTML COMMENTS  
			curl_close($ch);

			$timeAfter = microtime(true);
			$dif = $timeAfter - $timeB4;
			
// 			if($_SESSION['cust_acc_id'] == '6056')
// 				mail('tova@tigerchef.com', 'curl ' . $dif ,$timeAfter .'-'. $timeB4);					
			
			$request = $data;
			$data = strstr($result, '<?'); 			
			
			$xml = simplexml_load_string($data);
			$json = json_encode($xml);
			$params = json_decode($json,TRUE);
			//mail('tova@tigerchef.com', 'shippingrate - all', 'request'.var_export($request,true). 'result'.var_export($data,true));
			
			$params['request'] = $request;
			
			return $params;			
			  
	    }  
    }
?>
