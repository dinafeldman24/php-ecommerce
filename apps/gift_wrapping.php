<?php

class GiftWrapping 
{	
	public static function insert($info)
	{		
		return db_insert('gift_wrapping',$info);
	}
	
	public static function update($id,$info)
	{
		return db_update('gift_wrapping',$id,$info);
	}
	
	public static function delete($id)
	{
		return db_delete('gift_wrapping',$id);
	}
		
	public static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='', $name='')
	{
		$sql = "SELECT gift_wrapping.*
				FROM gift_wrapping
				WHERE 1 ";
		
		if ($id > 0) 
		{
			$sql .= " AND gift_wrapping.id = $id ";
		}
		if ($begins_with != '') 
		{
			$sql .= " AND gift_wrapping.name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') 
		{
			$fields = Array('gift_wrapping.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if ($name != '') 
		{
			$sql .= " AND gift_wrapping.name = '$name' ";
		}
			
		$sql .= "ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'gift_wrapping.name';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' DESC ';
		}
		
		return db_query_array($sql);
	}
	
	public static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = self::get($id,'','','','','','','');
		return $result[0];
	}
	
	public static function get1ByName($name)
	{
		$result = self::get(0,'','','','', $name,'','');
		if( count( $result ) )
			return $result[0];
		else 
			return false;
	}
	
	public static function make_image_name($id)
	{
	    global $CFG;
	    
	    if (!$id)
	    {
	        return '';
	    }
	    
	    return sprintf("%sgiftwrappics/%d.jpg", ($_SERVER['HTTPS']?$CFG->sslurl:$CFG->baseurl), $id);
	}
}

?>