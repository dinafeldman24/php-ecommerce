<?php
class Social{
  
 function facebook(){
     $facebook = new Facebook(array(
		'appId'		=> FACEBOOK_APP_ID,
		'secret'	=> FACEBOOK_APP_SECRET,
		));
			//get the user facebook id		
			$user = $facebook->getUser();
			//echo $user;exit;
			if($user){
				try{
					//get the facebook user profile data
					$user_profile = $facebook->api('/me', array('fields' => 'id,email,first_name,last_name,picture'));
					$params = array('next' => BASE_URL.'sociallogin.php?type=facebook');
					//logout url
					$logout =$facebook->getLogoutUrl($params);
					$_SESSION['User']=$user_profile;
					$_SESSION['facebook_logout']=$logout;
				}catch(FacebookApiException $e){
					error_log($e);
					$user = NULL;
				}		
			}
			if(empty($user)){
			  //login url	
			  $loginurl = $facebook->getLoginUrl(array(
							'scope'			=> 'email, publish_actions',
							'redirect_uri'	=> BASE_URL.'sociallogin.php?type=facebook',
							'display'=>'popup'
							));
			     header('Location: '.$loginurl);
			}
  
  }
	
	
	
	
    function google(){
			$client = new Google_Client();
			$client->setApplicationName("Tigerchef Reviews");
			$client->setClientId(GOOGLE_CLIENT_ID);
			$client->setClientSecret(GOOGLE_CLIENT_SECRET);
			$client->setRedirectUri(GOOGLE_REDIRECT_URI);
			$client->setApprovalPrompt(APPROVAL_PROMPT);
			$client->setAccessType(ACCESS_TYPE);
			$oauth2 = new Google_Oauth2Service($client);
			if (isset($_GET['code'])) {
			  $client->authenticate($_GET['code']);
			  $_SESSION['token'] = $client->getAccessToken();
			}
			if (isset($_SESSION['token'])) {
			 $client->setAccessToken($_SESSION['token']);
			}
			if (isset($_REQUEST['error'])) {
			 echo '<script type="text/javascript">window.close();</script>'; exit;
			}
			if ($client->getAccessToken()) {
			  $user = $oauth2->userinfo->get();
			  $_SESSION['User']=$user;
			  $_SESSION['token'] = $client->getAccessToken();
			} else {
			  $authUrl = $client->createAuthUrl();
			  header('Location: '.$authUrl);
			
			}
      }

 function twitter(){
  if(!isset($_GET['oauth_token']))
  {
    $connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
	  $request_token = $connection->getRequestToken(TWITTER_CALLBACK); 

	if(	$request_token)
	{
		$token = $request_token['oauth_token'];
		$_SESSION['request_token'] = $token ;
		$_SESSION['request_token_secret'] = $request_token['oauth_token_secret'];
		
		switch ($connection->http_code) 
		{
			case 200:
				$url = $connection->getAuthorizeURL($token);
		    	header('Location: ' . $url); 
			    break;
			default:
			    echo "Coonection with twitter Failed";
		    	break;
		}
	 }
	} 
  else
  {
	   $connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $_SESSION['request_token'], $_SESSION['request_token_secret']);
	   $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
	   if($access_token)
	   {
			$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
			$params =array();
			$params['include_entities']='false';
			$content = $connection->get('account/verify_credentials',$params);
			$content=get_object_vars($content);
		  $_SESSION['User']=$content;
	   }
  }

 }
 
 
  function linkedin(){
	
  if(!isset($_GET['oauth_token'])){
    $linkedin = new LinkedIn(LINKEDIN_ACCESS, LINKEDIN_SECRET,LINKEDIN_CALLBACK);
    $linkedin->debug = true;

    # Now we retrieve a request token. It will be set as $linkedin->request_token
    $linkedin->getRequestToken();
    $_SESSION['requestToken'] = serialize($linkedin->request_token);
  
    header("Location: " . $linkedin->generateAuthorizeUrl());

	} else {	
    $linkedClass   =   new linkedClass();
    $linkedin = new LinkedIn(LINKEDIN_ACCESS, LINKEDIN_SECRET,LINKEDIN_CALLBACK);
    if (isset($_REQUEST['oauth_verifier'])){
        $_SESSION['oauth_verifier']     = $_REQUEST['oauth_verifier'];

        $linkedin->request_token    =   unserialize($_SESSION['requestToken']);
        $linkedin->oauth_verifier   =   $_SESSION['oauth_verifier'];
        $linkedin->getAccessToken($_REQUEST['oauth_verifier']);
        $_SESSION['oauth_access_token'] = serialize($linkedin->access_token);
   }
   else{
        $linkedin->request_token    =   unserialize($_SESSION['requestToken']);
        $linkedin->oauth_verifier   =   $_SESSION['oauth_verifier'];
        $linkedin->access_token     =   unserialize($_SESSION['oauth_access_token']);
   }
   $content1 = $linkedClass->linkedinGetUserInfo($_SESSION['requestToken'], $_SESSION['oauth_verifier'], $_SESSION['oauth_access_token']);

   
    $xml   = simplexml_load_string($content1);
    $array = self::XML2Array($xml);
    $content = array($xml->getName() => $array);
		mail("dina.websites@gmail.com","content", $content);
	  $_SESSION['User']=$content['person'];

  }		
}
	
	
	 function XML2Array(SimpleXMLElement $parent)
    {
        $array = array();
        foreach ($parent as $name => $element) {
            ($node = & $array[$name])
                && (1 === count($node) ? $node = array($node) : 1)
                && $node = & $node[];
            $node = $element->count() ? XML2Array($element) : trim($element);
        }
        return $array;
    }
		
		
		
		
		function facebook_post(){
    $facebook = new Facebook(array(
		'appId'		=> FACEBOOK_APP_ID,
		'secret'	=> FACEBOOK_APP_SECRET,
		));
			//get the user facebook id		
			$user = $facebook->getUser();
			//echo $user;exit;
			
			if($user){
				try{
					//get the facebook user profile data
					$user_profile = $facebook->api('/me', array('fields' => 'id,email,first_name,last_name,picture'));
					$_SESSION['User']=$user_profile;
					print_r($user_profile);
				}catch(FacebookApiException $e){
					error_log($e);
					$user = NULL;
				}		
			}
			if(empty($user)){
			  //login url	
				echo "No USER";
			  $loginurl = $facebook->getLoginUrl(array(
							'scope'			=> 'email, publish_actions',
							'redirect_uri'	=> BASE_URL.'socialpost.php?type=facebook_post',
							'display'=>'popup'
							));
			     header('Location: '.$loginurl);
			}


 /*   $params = array(
        "access_token" => $facebook->getAccessToken(),
				"message" => "Here is a blog post about auto posting on Facebook using PHP #php #facebook",
				"link" => "http://www.pontikis.net/blog/auto_post_on_facebook_with_php",
				"picture" => "http://i.imgur.com/lHkOsiH.png",
				"name" => "How to Auto Post on Facebook with PHP",
				"caption" => "www.pontikis.net",
				"description" => "Automatically post on Facebook with PHP using Facebook PHP SDK. How to create a Facebook app. Obtain and extend Facebook access tokens. Cron automation."
		 );
   try {
     $ret = $facebook->api(FACEBOOK_APP_ID.'/feed', 'POST', $params);
     echo 'Successfully posted to Facebook';
    } catch(Exception $e) {
     echo $e->getMessage();
	}*/





  }
		
}?>