<? 
class OrderAssignments {
	
	function insert($info)
	{		
		return db_insert('order_assignments',$info);
	}
	
	function update($id,$info)
	{
		return db_update('order_assignments',$id,$info);
	}
	
	function delete($order_id, $note_id, $operator_id)
	{
		return db_delete('order_assignments',array($order_id, $operator_id, $note_id), array('order_id', 'operator_id', 'note_id'));
	}
	
	function get($id=0, $order_id=0, $operator_id=0, $current_only='current')
	{
		$sql = "SELECT order_assignments.*, order_notes.*, admin_users.username
				FROM order_assignments, order_notes, admin_users
				WHERE 1 AND order_assignments.note_id = order_notes.id
				AND order_assignments.operator_id = admin_users.id";
		
		if ($id > 0) {
			$sql .= " AND order_assignments.id = $id ";
		}
		if ($order_id > 0) {
			$sql .= " AND order_assignments.order_id = $order_id ";
		}
		if ($operator_id > 0) {
			$sql .= " AND order_assignments.operator_id = $operator_id ";
		}
		if ($current_only == 'current')
		{
			$sql .= " AND (order_assignments.action_required_date = '0000-00-00' 
							OR order_assignments.action_required_date <= CURDATE())";
		}
		else if ($current_only == 'future')
		{
			$sql .= " AND order_assignments.action_required_date != '0000-00-00' 
					  AND order_assignments.action_required_date > CURDATE()";
		}
		$sql .= " ORDER BY order_notes.date_added DESC";
		return db_query_array($sql);
	}
	
	function get1($id)
	{
		$id = (int) $id;
		
		if (!$id) return false;
		
		$result = OrderItemStatuses::get($id);
		return $result[0];
	}
}
?>