<?php


    class AmazonMWS {

    static function submitInventoryFeed($product_update_type = "", $country='US') {

        global $CFG;

        require_once( $CFG->amazon_dir .'Model/SubmitFeedRequest.php' );
		if ($country == "US") $service_url_to_use = $CFG->amazon_serviceUrl;
        else if ($country == "CA") $service_url_to_use = $CFG->amazon_serviceUrlCanada;
        
    	$config = array (
          'ServiceURL' => $service_url_to_use,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
		
        //Need to bulid a feed file of all the updates
        if ($product_update_type == "") $feed_file = AmazonMWS::generateFeedFile();
        else if ($product_update_type == "MR") $feed_file = AmazonMWS::generateFeedFile('a/u', "MR"); // M. Rothman hourly feed
        else if ($product_update_type == "WH" && $country == "CA") $feed_file = AmazonMWS::generateFeedFile('a/u', "WH", "CA"); // WH phantom skus hourly file
        else if ($product_update_type == "WH") $feed_file = AmazonMWS::generateFeedFile('a/u', "WH"); // WH phantom skus hourly file
        else if ($product_update_type == "SED") $feed_file = AmazonMWS::generateFeedFile('a/u', "SED"); // SED feed
        else if ($product_update_type == "FF") $feed_file = AmazonMWS::generateFeedFile('a/u', "FF"); // FlashFurniture feed
        else if ($product_update_type == "WS") $feed_file = AmazonMWS::generateFeedFile('a/u', "WS"); // Weston feed
        else if ($product_update_type == "JR" && $country == "US") $feed_file = AmazonMWS::generateFeedFile('a/u', "JR"); // Johnson rose feed
        else if ($product_update_type == "JR" && $country == "CA") $feed_file = AmazonMWS::generateFeedFile('a/u', "JR", "CA"); // Johnson Rose for Amazon Canada
        else if ($product_update_type == "CH" && $country == "US") $feed_file = AmazonMWS::generateFeedFile('a/u', "CH"); // Chroma feed
        else if ($product_update_type == "CH" && $country == "CA") $feed_file = AmazonMWS::generateFeedFile('a/u', "CH", "CA"); // Chroma for Amazon Canada

        $content = file_get_contents( $feed_file );

        $feed_file = @fopen( $feed_file, 'r' );

        if ($country == 'US')
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID,
             	 AWS_SECRET_ACCESS_KEY,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID,
           		'Merchant' => MERCHANT_ID,
           		'FeedType' => '_POST_FLAT_FILE_INVLOADER_DATA_',
        		'FeedContent' => $feed_file,
        		'PurgeAndReplace' => false,
        		'ContentMd5' => base64_encode(md5(stream_get_contents($feed_file), true)),
         	);
		}
		else if ($country == "CA")
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID_CANADA,
             	 AWS_SECRET_ACCESS_KEY_CANADA,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID_CANADA,
           		'Merchant' => MERCHANT_ID_CANADA,
           		'FeedType' => '_POST_FLAT_FILE_PRICEANDQUANTITYONLY_UPDATE_DATA_',
        		'FeedContent' => $feed_file,
        		'PurgeAndReplace' => false,
        		'ContentMd5' => base64_encode(md5(stream_get_contents($feed_file), true)),
         	);			
		}
        
        $request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);

        //print_ar( $request );

        $status = AmazonMWS::invokeSubmitFeed($service, $request);

        @fclose($feed_file);

        return $status;
    }

    static function submitFeed( $type='', $file='', $country = 'US')
    {
        global $CFG;

        require_once( $CFG->amazon_dir .'Model/SubmitFeedRequest.php' );

        if ($country == "US") $service_url_to_use = $CFG->amazon_serviceUrl;
        else if ($country == "CA") $service_url_to_use = $CFG->amazon_serviceUrlCanada;
        
        $feed_file = @fopen( $file, 'r' );
        
        $config = array (
          'ServiceURL' => $service_url_to_use,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
		
        if ($country == 'US')
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID,
             	 AWS_SECRET_ACCESS_KEY,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID,
           		'Merchant' => MERCHANT_ID,
           		'FeedType' => $type,
        		'FeedContent' => $feed_file,
        		'PurgeAndReplace' => false,
        		'ContentMd5' => base64_encode(md5(stream_get_contents($feed_file), true)),
         	);
		}
		else if ($country == "CA")
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID_CANADA,
             	 AWS_SECRET_ACCESS_KEY_CANADA,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID_CANADA,
           		'Merchant' => MERCHANT_ID_CANADA,
           		'FeedType' => $type,
        		'FeedContent' => $feed_file,
        		'PurgeAndReplace' => false,
        		'ContentMd5' => base64_encode(md5(stream_get_contents($feed_file), true)),
         	);			
		}
        
        //$content = file_get_contents( $file );		
        $request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);

        //print_ar( $request );

        $status = AmazonMWS::invokeSubmitFeed($service, $request);

        @fclose($feed_file);

        return $status;
    }

    function invokeSubmitFeed(MarketplaceWebService_Interface $service, $request)
      {
          try {
          ob_start();
              $response = $service->submitFeed($request);

            echo ("Service Response\n");
            echo ("=============================================================================\n");

            echo("        SubmitFeedResponse\n");
            if ($response->isSetSubmitFeedResult()) {
                echo("            SubmitFeedResult\n");
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                echo("                FeedSubmissionInfo\n");
                $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
                if ($feedSubmissionInfo->isSetFeedSubmissionId())
                {
                    echo("                    FeedSubmissionId\n");
                    echo("                        " . $feedSubmissionInfo->getFeedSubmissionId() . "\n");
                }
                if ($feedSubmissionInfo->isSetFeedType())
                {
                    echo("                    FeedType\n");
                    echo("                        " . $feedSubmissionInfo->getFeedType() . "\n");
                }
                if ($feedSubmissionInfo->isSetSubmittedDate())
                {
                    echo("                    SubmittedDate\n");
                    echo("                        " . $feedSubmissionInfo->getSubmittedDate()->format(DATE_FORMAT) . "\n");
                }
                if ($feedSubmissionInfo->isSetFeedProcessingStatus())
                {
                    echo(" FeedProcessingStatus\n");
                    echo("                        " . $feedSubmissionInfo->getFeedProcessingStatus() . "\n");
                }
                if ($feedSubmissionInfo->isSetStartedProcessingDate())
                {
                    echo(" StartedProcessingDate\n");
                    echo("                        " . $feedSubmissionInfo->getStartedProcessingDate()->format(DATE_FORMAT) . "\n");
                }
                if ($feedSubmissionInfo->isSetCompletedProcessingDate())
                {
                    echo(" CompletedProcessingDate\n");
                    echo("                        " . $feedSubmissionInfo->getCompletedProcessingDate()->format(DATE_FORMAT) . "\n");
                }
                }
            }
            if ($response->isSetResponseMetadata()) {
                echo("            ResponseMetadata\n");
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId())
                {
                echo("                RequestId\n");
                echo("                    " . $responseMetadata->getRequestId() . "\n");
                }
            }
        $status = ob_get_clean();
// echo "$status";
        return true;
         } catch (MarketplaceWebService_Exception $ex) {
         ?>
        <div style="display:none;" id="amazon_debug"><?
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         ?>
        </div>
            <?
            return false;
         }
     }

     public function generateFeedFile( $type='a/u', $product_update_type = '', $country = "US", $other_info=array() )
     {
        global $CFG;
        
        switch( $type )
        {
        case 'a/u':

            if ($product_update_type == '') $inventory = Inventory::getAmazonUpload();
        else if ($product_update_type == "MR") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_MR", "dist_sku", $CFG->m_rothman_supplier_id);
            }
        	else if ($product_update_type == "SED") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_SED", "dist_sku", $CFG->sed_supplier_id);
            }
            else if ($product_update_type == "FF") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_FF", "dist_sku", $CFG->flash_furniture_supplier_id);
            }
        	else if ($product_update_type == "WS") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_WS", "dist_sku", $CFG->weston_supplier_id);
            }
        	else if ($product_update_type == "JR") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_JR", "dist_sku", $CFG->johnson_rose_supplier_id);
            }
        	else if ($product_update_type == "CH") 
            {
            	$inventory = SupplierFeed::getDataForAmzInvUpdates("product_feed_CH", "dist_sku", $CFG->chroma_supplier_id);
            }
        	else if ($product_update_type == "WH")
            {
            	$inventory = Inventory::get(0,0,0,'','',0,0,0,$CFG->default_inventory_location);
            	$no_wh_skus = ChannelMaxSettings::getByType("noWhSku");
            	$no_wh_array = array();
            	$counter = 0;
            	foreach ($no_wh_skus as $no_wh_sku)
            	{
            		$no_wh_array[] = $no_wh_sku['vendor_sku'];
            	}    
            	$brands_by_case_array = BrandsOrderedByCase::getArrayOfBrandsOrderedByCase();        	             	
            }
            if( !$inventory )
            {
            return false;
            }

            $filename = "Amazon-inventory-". $product_update_type . $country . time() .".txt";
            $dirname = "/feeds";
            $filename = $CFG->dirroot . $dirname . '/' . $filename;
            $data_file = fopen($filename,"w");
            //$data = "sku\tproduct-id\tproduct-id-type\tprice\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfullfillment-center-id\n";
            //$data = "sku\tproduct-id\tproduct-id-type\tprice\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfulfillment-center-id\r\n";
            // RSunness removed price Nov 1 '11 because using channelmax to reprice
            if ($product_update_type == "") $data = "sku\tproduct-id\tproduct-id-type\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfulfillment-center-id\r\n";
            else if ($product_update_type == "WH") $data = "sku\tproduct-id\tproduct-id-type\tprice\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfulfillment-center-id\tleadtime-to-ship\r\n";
            else if ($country == "US" && ($product_update_type == "MR" || $product_update_type == "SED" || $product_update_type == "FF"  || $product_update_type == "WS"  || $product_update_type == "JR" || $product_update_type == "CH")) $data = "sku\tprice\tquantity\tleadtime-to-ship\r\n";
            else if ($country == "CA" && ($product_update_type == "MR" || $product_update_type == "SED" || $product_update_type == "FF"  || $product_update_type == "WS"  || $product_update_type == "JR" || $product_update_type == "CH")) $data = "sku\tprice\tminimum-seller-allowed-price\tmaximum-seller-allowed-price\tquantity\tleadtime-to-ship\tfulfillment-channel\r\n";
            
            fputs($data_file,$data);

            foreach( $inventory as $row )
            {
            $p = Products::get1( $row['product_id'] );
			
            if( $row['product_option_id'] )
            {
                $option = Products::getProductOption($row['product_option_id'] );
                $p['vendor_sku'] = $option['vendor_sku'];
                $p['price'] = $option['additional_price'];
                $p['upc'] = $option['upc'];
            }
            else $row['product_option_id'] = 0;
            
			if ($product_update_type == "WH") 
			{
				$counter++;
				//if ($p['vendor_sku'] && (in_array($p['vendor_sku'], $no_wh_array) || $p['brand_id'] == 83)) $row['qty'] = 0;
				if ($p['vendor_sku'] && (in_array($p['vendor_sku'], $no_wh_array) || in_array($p['brand_id'], $brands_by_case_array)) 
				|| ($p['brand_id'] != '33' && $p['brand_id'] != '123' && $p['brand_id'] != '139' 
				&& $p['brand_id'] != '114'&& $p['brand_id'] != '7' && $p['brand_id'] != '409' 
				&& $p['brand_id'] != '200'&& $p['brand_id'] != '91'&& $p['brand_id'] != '9'
				&& $p['brand_id'] != '15'&& $p['brand_id'] != '1093' && $p['brand_id'] != '99'
				&& $p['brand_id'] != '46'&& $p['brand_id'] != '76' && $p['brand_id'] != '59'
				&& $p['brand_id'] != '676'&& $p['brand_id'] != '129' && $p['brand_id'] != '22'
				&& $p['brand_id'] != '49'&& $p['brand_id'] != '82' && $p['brand_id'] != '485'
				&& $p['brand_id'] != '86'&& $p['brand_id'] != '214' && $p['brand_id'] != '136'
				&& $p['brand_id'] != '83' && $p['brand_id'] != '442'
				&& $p['brand_id'] != '69'&& $p['brand_id'] != '113' && $p['brand_id'] != '137'
				&& $p['brand_id'] != '57'&& $p['brand_id'] != '116' && $p['brand_id'] != '679'
				&& $p['brand_id'] != '98'&& $p['brand_id'] != '156' && $p['brand_id'] != '47'
				&& $p['brand_id'] != '45' && $p['brand_id'] != '30'	&& $p['brand_id'] != '104'
				&& $p['brand_id'] != '90' && $p['brand_id'] != '51'
				) || $p['brand_id'] == 26) $row['qty'] = 0;
				else 
				{
		    		// check if any of that sku is on an open order.  subtract that number from $qty_to_use
	    			$qty_on_open_orders = 0;           
	    			$open_orders_with_this_sku = Orders::getItems(0,0,'','',0,'','',0,0,$row['product_id'],0,0,'','','', $CFG->new_order_status_id, $row['product_option_id']);
	    		
	    			if ($open_orders_with_this_sku)
	    			{
	    				foreach ($open_orders_with_this_sku as $open_order)
	    				{
	    					$qty_on_open_orders += $open_order['qty'];
	    				}
	    			 
	    				$row['qty'] -= $qty_on_open_orders;	    			
	    			}	    		
	    			$row['qty'] = max($row['qty'], 0);				
				}
            	//if ($counter > 50) break;
				$p['vendor_sku'] .= $CFG->warehouse_sku_suffix;
            	$wh_sku = AmazonWarehouseSkus::get1($p['vendor_sku']);
			}
            
			$p_line = $p['vendor_sku'] . "\t";    //SKU
			if ($product_update_type == "" || $product_update_type == "WH")
			{
            
            $p_line .= $p['upc'] . "\t";        //PRODUCT-ID
            $p_line .= "3\t";            //PRODUCT-ID-TYPE 3=UPC
            if ($product_update_type == "WH")
            {
            	if (!$wh_sku)
            	{
            		if($p['price'] > 0) $p_line .= $p['price'] . "\t";        //Price
            		else $p_line .= ($p['vendor_price'] * 1.3) . "\t";
            		$info['warehouse_sku'] = $p['vendor_sku'];
            		$info['product_id'] = $row['product_id'];
            		$info['product_option_id'] = $row['product_option_id'];
            		AmazonWarehouseSkus::insert($info);
            		unset($info);            		             		            			            			
            	}
            	else $p_line .= "\t";
            }
            $p_line .= "11\t";            //Item Condition -- 11 = New
            $p_line .= $row['qty'] . "\t";        //QTY
            $p_line .= "a\t";            //Add-Delete -- a = add/update
            $p_line .= "1\t";            //International shippping 1 = No 2 = Yes
            $p_line .= "\t";            //Expedited-shipping -- need to check to see what they offer
            $p_line .= "\t";            //Item-note
            $p_line .= "\t";            //Fullfillment-center-id
            if ($product_update_type == "WH") $p_line .= "4\t";
 			}
			else if ($product_update_type == "MR" || $product_update_type == "SED" || $product_update_type == "FF" || $product_update_type == "WS" || $product_update_type == "JR" || $product_update_type == "CH") // feed that is generated every hour automatically with M. Rothman items
			{
				// if we have it in the warehouse, use that qty instead of feed qty
				$in_warehouse = Inventory::get(0,0,0,'inventory.qty','',$row['product_id'], 0,0,$CFG->default_inventory_location);
	    		$warehouse_record = $in_warehouse[0];
	    		
	    		if ($warehouse_record['qty'] && $warehouse_record['qty'] > 0)
	    		{
	    			$qty_to_use = $warehouse_record['qty'];
	    			//if ($p['vendor_sku'] == "KT1770") echo "Going to use Warehouse qty of " . $qty_to_use ." for " . $p['vendor_sku'] . "\n";
	    		}
	    		else 
	    		{
	    			$qty_to_use = $row['qty'];
	    			if ($product_update_type == "FF" && $qty_to_use <= 20) $qty_to_use = 0;
	    			else if ($qty_to_use < 5) $qty_to_use = 0;
	    		} 	    			    			
	    		if ($product_update_type == "FF") $lead_time = "4";	    		
	    		else if ($product_update_type == "WS" || ($product_update_type == "JR" && $country == "US") || ($product_update_type == "CH" && $country == "US")) $lead_time = "5";
	    		else if (($product_update_type == "JR" || $product_update_type == "CH")&& $country == "CA") $lead_time = "6";
	    		//else if ($product_update_type == "WS" || ($product_update_type == "JR" && $country == "US")) $lead_time = "10";
	    		//else if ($product_update_type == "JR" && $country == "CA") $lead_time = "10";
	    		else $lead_time = "";	
	    		
	    		// check if any of that sku is on an open order.  subtract that number from $qty_to_use
	    		$qty_on_open_orders = 0;           
	    		$open_orders_with_this_sku = Orders::getItems(0,0,'','',0,'','',0,0,$row['product_id'],0,0,'','','', $CFG->new_order_status_id);
	    		
	    		if ($open_orders_with_this_sku)
	    		{
	    			foreach ($open_orders_with_this_sku as $open_order)
	    			{
	    				$qty_on_open_orders += $open_order['qty'];
	    			}
	    			 //if ($p['vendor_sku'] == "KT1770") echo "Qty on open orders is " . $qty_on_open_orders . " for " . $p['vendor_sku'] . "\n";
	    			$qty_to_use -= $qty_on_open_orders;
	    			
	    		}	    		
	    		$qty_to_use = max($qty_to_use, 0);
	    		
	    		if ($country == "CA") $p_line .= "\t\t";
	    		$p_line .= "\t" . $qty_to_use . "\t";
	    		$p_line .= $lead_time;
	    		if ($country == "CA") $p_line .= "\t";
			}
            $p_line .= "\r\n";
            fputs($data_file,$p_line);

            if (($product_update_type == "")) Inventory::update( $row['id'], array('to_upload'=>'N'));
            }

            fclose( $data_file );

            //return $data_file;
            return $filename;

            break;
        case '_POST_FLAT_FILE_FULFILLMENT_DATA_':

            $order_object->amazon_ship_confirm = 'N';
            
            if($country == "US")
	    	{
	    		$order_object->order_tax_option = $CFG->amazon_store_id;
		    	// also get shipped Checkout by Amazon orders
	    		$order_object2->amazon_ship_confirm = 'N';
	    		$order_object2->order_tax_option = $CFG->website_store_id;
	    		$order_object2->payment_type = "cba";
	    		$order_object2->current_status_id = $CFG->shipped_order_status_id;
	    		$orders2 = Orders::getByO( $order_object2 );
	    	}
	    	else if ($country == "CA")
	    	{
		    	$order_object->order_tax_option = $CFG->amazon_canada_store_id;
		    	$orders2 = array();
	    	}
	    	$order_object->current_status_id = $CFG->shipped_order_status_id;
	       
	    	$orders = Orders::getByO( $order_object );
	    
	    	if (sizeof($orders2) > 0)$orders_merged = array_merge($orders, $orders2);
	    	else $orders_merged = $orders;

            $filename="Amazon-shipconfirm-$country-" . time().".txt";
            $dirname = "/feeds";
            $filename = $CFG->dirroot . $dirname . '/' . $filename;
            $data_file = fopen($filename,"w");
            //$data = "sku\tproduct-id\tproduct-id-type\tprice\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfullfillment-center-id\n";
            $data = "order-id\torder-item-id\tquantity\tship-date\tcarrier-code\tcarrier-name\ttracking-number\tship-method\r\n";
            fputs($data_file,$data);

            foreach( $orders as $row )
            {

            $tracking = Orders::getTracking(0, $row['id'] );
            if( $tracking ) {
                foreach( $tracking as $t )
                {
                	if ($t['tracking_number'])
                	{
                $items = Orders::getItemsForTracking( $t['id'] );
                foreach( $items as $i )
                {
                    $p_line = ($row['amazon_order_id']?$row['amazon_order_id']:$row['3rd_party_order_number'])  . "\t"; //order id
                    if ($i['amazon_order_item_id'])
                    {
                    $p_line .= $i['amazon_order_item_id'] . "\t"; //order item id -- all same shipment? Not needed if so
                    $p_line .= $i['qty'] . "\t"; //quantity
                    }
                    else 
                    {
                    	$p_line .= "\t\t";
                    }
                    $p_line .= db_date( $t['ship_date'], 'Y-m-d') . "\t"; //Ship date - REQUIRED

                    $carrier_name = '';

                    if( stristr( $t['service_name'], 'UPS' ) )
                        $t['service_name'] = 'UPS';

                    switch( $t['service_name'] )
                    {
                    case 'USPS':
                        $carrier_code = 'USPS';
                        break;
                    case 'UPS':
                        $carrier_code = 'UPS';
                        break;
                    case 'FedEx':
                        $carrier_code = 'FedEx';
                        break;
                    default:
                        $carrier_code = 'Other';
                        $carrier_name = $t['service_name'];
                    }

                    $p_line .= $carrier_code . "\t"; //carrier-code
                    $p_line .= $carrier_name . "\t"; //carrier-name
                    $p_line .= $t['tracking_number'] . "\t"; //tracking number
                    $p_line .= "\t"; //ship method


                    $p_line .= "\r\n";
                    fputs($data_file,$p_line);
                }
                	}
                }
            }
            else {
                $p_line = $row['amazon_order_id'] . "\t"; //order id
                $p_line .= "\t"; //order item id -- all same shipment? Not needed if so
                $p_line .= "\t"; //quantity
                $p_line .= date('Y-m-d') . "\t"; //Ship date - REQUIRED

                $p_line .= "\t";  //carrier-code
                $p_line .= "\t";  //carrier-name
                $p_line .= "\t";  //tracking number
                $p_line .= "\t"; //ship method

                $p_line .= "\r\n";
                fputs($data_file,$p_line);
            }
            }
            fclose( $data_file );
            return $filename;
            break;

   case '_POST_FLAT_FILE_ORDER_ACKNOWLEDGEMENT_DATA_':
			if (count($other_info) == 0) return;
			            	       
	    	$order_to_cancel = Orders::get1($other_info['order_id']);
	    	$amazon_order_id = $order_to_cancel['amazon_order_id'];
	    	
            $filename="Amazon-cancellation-$country-" . time().".txt";
            $dirname = "/feeds";
            $filename = $CFG->dirroot . $dirname . '/' . $filename;
            $data_file = fopen($filename,"w");

            $data = "order-id\tmerchant-order-id\tcancellation-reason-code\tamazon-order-item-code\r\n";
            fputs($data_file,$data);
			$data2 = "$amazon_order_id\t\t".$other_info['reason_for_cancellation']."\t".$other_info['amazon_order_item_id']."\r\n";        
            fputs($data_file,$data2);
            fclose( $data_file );
            return $filename;
            break;
        
      case '_POST_FLAT_FILE_PAYMENT_ADJUSTMENT_DATA_':
			if (count($other_info) == 0) return;
			            	       
	    	$order_to_cancel = Orders::get1($other_info['order_id']);	    	
	    	$amazon_order_id = $order_to_cancel['amazon_order_id'];
	    	
            $filename="Amazon-adjustment-$country-" . time().".txt";
            $dirname = "/feeds";
            $filename = $CFG->dirroot . $dirname . '/' . $filename;
            $data_file = fopen($filename,"w");
			$data = "order-id\torder-item-id\tadjustment-reason-code\tcurrency\titem-price-adj\titem-tax-adj\tshipping-price-adj\tshipping-tax-adj\tgift-wrap-price-adj\tgift-wrap-tax-adj\titem-promotion-adj\titem-promotion-id\tship-promotion-adj\tship-promotion-id\tquantity-cancelled\r\n";            
            fputs($data_file,$data);
            foreach($other_info['amz_item_info'] as $amz_item_info_one_line)
            {            	
				$data2 = "$amazon_order_id\t".$amz_item_info_one_line['order_item_id']."\t".$other_info['reason_for_cancellation'].
						 "\tUSD\t".$amz_item_info_one_line['item-price-adj']."\t".$amz_item_info_one_line['item-tax-adj']."\t".
						 $amz_item_info_one_line['shipping-price-adj']."\t".$amz_item_info_one_line['shipping-tax-adj']."\t".
						 $amz_item_info_one_line['gift-wrap-price-adj']."\t".$amz_item_info_one_line['gift-wrap-tax-adj']."\t".
						 ($amz_item_info_one_line['item-promotion-adj'] > 0 ? $amz_item_info_one_line['item-promotion-adj'] : '')
						 ."\t\t".
						 ($amz_item_info_one_line['ship-promotion-adj'] > 0 ? $amz_item_info_one_line['ship-promotion-adj'] : '') 
						."\t\t\r\n";
				fputs($data_file,$data2);
            }                    
            fclose( $data_file );
            return $filename;
            break;
        }
        
     }

     static function getReport( $reportID='', $save_report=true, $country = "US" )
     {
         global $CFG;

        require_once( $CFG->amazon_dir .'Model/GetReportRequest.php' );

        if ($country == "US") $service_url_to_use = $CFG->amazon_serviceUrl;
        else if ($country == "CA") $service_url_to_use = $CFG->amazon_serviceUrlCanada;
        
        $feed_file = @fopen( $file, 'r' );
        
        $config = array (
          'ServiceURL' => $service_url_to_use,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
		
        if ($country == 'US')
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID,
             	 AWS_SECRET_ACCESS_KEY,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID,
           		'Merchant' => MERCHANT_ID,
   				'Report' => @fopen('php://memory', 'rw+'),
           		'ReportId' => $reportID,
         	);
		}
		else if ($country == "CA")
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID_CANADA,
             	 AWS_SECRET_ACCESS_KEY_CANADA,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID_CANADA,
           		'Merchant' => MERCHANT_ID_CANADA,
   				'Report' => @fopen('php://memory', 'rw+'),
           		'ReportId' => $reportID,
         	);			
		}
        
         $request = new MarketplaceWebService_Model_GetReportRequest($parameters);


         try {
          $response = $service->getReport($request);

//            echo ("Service Response\n");
//            echo ("=============================================================================\n");
//
//            echo("        GetReportResponse\n");
//            if ($response->isSetGetReportResult()) {
//              $getReportResult = $response->getGetReportResult();
//
//              echo ("            GetReport");
//
//              if ($getReportResult->isSetContentMd5()) {
//            echo ("                ContentMd5");
//            echo ("                " . $getReportResult->getContentMd5() . "\n");
//              }
//            }
//            if ($response->isSetResponseMetadata()) {
//            echo("            ResponseMetadata\n");
//            $responseMetadata = $response->getResponseMetadata();
//            if ($responseMetadata->isSetRequestId())
//            {
//                echo("                RequestId\n");
//                echo("                    " . $responseMetadata->getRequestId() . "\n");
//            }
//            }
//
//            echo ("        Report Contents\n");
            if( $save_report )
            {
            $filename = $CFG->dirroot . '/edit/amazon_reports/' . $reportID . '.txt';
            $handle = fopen( $CFG->dirroot . '/edit/amazon_reports/' . $reportID . '.txt', w );
            if( $handle && is_file( $CFG->dirroot . '/edit/amazon_reports/' . $reportID . '.txt' ) )
            {
                fwrite( $handle, stream_get_contents( $request->getReport() ) );
            }

            fclose( $handle );

//            echo"File " . $filename . " downloaded \r\n";

            $info['is_downloaded'] = 'Y';
            $info['filename'] =  $reportID . '.txt';
            $o = (object) array("report_id"=>$reportID);
            //$o->report_id = $reportID;
            $report = AmazonReport::get1ByO( $o );
            AmazonReport::update( $report['id'], $info );
            //echo "Updated \r\n";

            } else {
            echo (stream_get_contents($request->getReport()) . "\n");
            }
            //self::acknowledgeReport($reportID);

         } catch (MarketplaceWebService_Exception $ex) {
         echo ("Trying to get report $reportID\n");
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         }
     }

     function invokeGetReport(MarketplaceWebService_Interface $service, $request)
      {
          try {
              $response = $service->getReport($request);

//            echo ("Service Response\n");
//            echo ("=============================================================================\n");
//
//            echo("        GetReportResponse\n");
//            if ($response->isSetGetReportResult()) {
//              $getReportResult = $response->getGetReportResult();
//              echo ("            GetReport");
//
//              if ($getReportResult->isSetContentMd5()) {
//                echo ("                ContentMd5");
//                echo ("                " . $getReportResult->getContentMd5() . "\n");
//              }
//            }
//            if ($response->isSetResponseMetadata()) {
//                echo("            ResponseMetadata\n");
//                $responseMetadata = $response->getResponseMetadata();
//                if ($responseMetadata->isSetRequestId())
//                {
//                echo("                RequestId\n");
//                echo("                    " . $responseMetadata->getRequestId() . "\n");
//                }
//            }

//            echo ("        Report Contents\n");
//            echo (stream_get_contents($request->getReport()) . "\n");

         } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         }
    }

    static function requestReport( $report_type='' , $amazon_country='US')
     {
         global $CFG;

        require_once( $CFG->amazon_dir .'Model/RequestReportRequest.php' );

        if ($amazon_country == "US") $service_url_to_use = $CFG->amazon_serviceUrl;
        else if ($amazon_country == "CA") $service_url_to_use = $CFG->amazon_serviceUrlCanada;
        
        $config = array (
          'ServiceURL' => $service_url_to_use,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
		
        if ($amazon_country == 'US')
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID,
             	 AWS_SECRET_ACCESS_KEY,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID,
           		'Merchant' => MERCHANT_ID,
           		'ReportType' => $report_type,
         	);
		}
		else if ($amazon_country == "CA")
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID_CANADA,
             	 AWS_SECRET_ACCESS_KEY_CANADA,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

          	$parameters = array (
           		'Marketplace' => MARKETPLACE_ID_CANADA,
           		'Merchant' => MERCHANT_ID_CANADA,
           		'ReportType' => $report_type,
         	);			
		}
         if( $report_type == '_GET_FLAT_FILE_ORDERS_DATA_' )
         {
         //instead of doing it for just today, get the date of the last run report of this type, and go from there. That will ensure that if anything broke
         //it will still pick up

         //$o = (object) array('report_type' => $report_type ); // warning: report_type must be one of enum('unshipped_orders', 'order_report', 'amazon_fulfilled_shipments', 'inventory_report' )

         //$reports = AmazonReport::getByO( $o );

//             if( $reports )
//             {
//                 $most_recent = $reports[0];
//
//                 if( $most_recent['start_ts'] && $most_recent['end_ts'] )
//                 {
//                $parameters['EndDate'] = time();
//                $parameters['StartDate'] = $most_recent['end_ts'];
//                 }
//                 else {
//                $parameters['EndDate'] = time();
//                $parameters['StartDate'] = mktime(0, 0, 0, date('m'), date('d'), date('Y') );
//                 }
//             }
//             else {
                /**
                 * Update 6/25/10 -- Bug in above code ($report_type) was always returning nothing
                 * so this code was running. Changing to always grab the last 2 days of data to
                 * work around different timezones.
                 */

                $parameters['EndDate'] = time();
                $parameters['StartDate'] = strtotime("-48 hours");
//             }
         }
          else if( $report_type == '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_' || $report_type == '_GET_MERCHANT_LISTINGS_DATA_LITE_' || $report_type == '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_')
         {
         //instead of doing it for just today, get the date of the last run report of this type, and go from there. That will ensure that if anything broke
         //it will still pick up

         $parameters['EndDate'] = time();
         $parameters['StartDate'] = strtotime("-48 hours");
         }
         else if( $report_type == '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_' )
         {
             //instead of doing it for just today, get the date of the last run report of this type, and go from there. That will ensure that if anything broke
             //it will still pick up

             $parameters['EndDate'] = time();
             $parameters['StartDate'] = strtotime("-168 hours");
             //$parameters['StartDate'] = strtotime("2011-12-10 00:00:00");
             //$parameters['EndDate'] = strtotime("2011-12-22 00:00:00");

         }

         $request = new MarketplaceWebService_Model_RequestReportRequest( $parameters );

         try {
              $response = $service->requestReport($request);
//              $requestReportResult = $response->getRequestReportResult();
//              if ($response->isSetRequestReportResult()) {
//            $reportRequestInfo = $requestReportResult->getReportRequestInfo();
//              }
              //print_ar( $response );
            //echo ("Service Response\n");
            //echo ("=============================================================================\n");

//            echo("        RequestReportResponse\n");
            if ($response->isSetRequestReportResult()) {
    //            echo("            RequestReportResult\n");
                $requestReportResult = $response->getRequestReportResult();
                //print_ar( $requestReportResult );
                if ($requestReportResult->isSetReportRequestInfo()) {

                $reportRequestInfo = $requestReportResult->getReportRequestInfo();
                //print_ar( $reportRequestInfo );
        //          echo("                ReportRequestInfo\n");
                  if ($reportRequestInfo->isSetReportRequestId())
                  {
            //          echo(" ReportRequestId\n");
                //      echo("                        " . $reportRequestInfo->getReportRequestId() . "\n");
                  }
                  if ($reportRequestInfo->isSetReportType())
                  {
//                      echo("                    ReportType\n");
//                      echo("                        " . $reportRequestInfo->getReportType() . "\n");
                  }
                  if ($reportRequestInfo->isSetStartDate())
                  {
//                      echo("                    StartDate\n");
//                      echo("                        " . $reportRequestInfo->getStartDate()->format(DATE_FORMAT) . "\n");
                  }
                  if ($reportRequestInfo->isSetEndDate())
                  {
//                      echo("                    EndDate\n");
//                      echo("                        " . $reportRequestInfo->getEndDate()->format(DATE_FORMAT) . "\n");
                  }
                  if ($reportRequestInfo->isSetSubmittedDate())
                  {
//                      echo("                    SubmittedDate\n");
//                      echo("                        " . $reportRequestInfo->getSubmittedDate()->format(DATE_FORMAT) . "\n");
                  }
                  if ($reportRequestInfo->isSetReportProcessingStatus())
                  {
//                      echo(" ReportProcessingStatus\n");
//                      echo("                        " . $reportRequestInfo->getReportProcessingStatus() . "\n");
                  }
                  }
            }
            if ($response->isSetResponseMetadata()) {
//                echo("            ResponseMetadata\n");
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId())
                {
//                echo("                RequestId\n");
//                echo("                    " . $responseMetadata->getRequestId() . "\n");
                }
            }

         } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         }

         $report_info['request_id'] = $reportRequestInfo->getReportRequestId();
         $report_info['is_downloaded'] = 'N';

         $report_info['start_ts']    = $parameters['StartDate'];
         $report_info['end_ts']    = $parameters['EndDate'];

         switch( $report_type )
         {
         case '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_':
             $report_info['report_type'] = 'unshipped_orders';
             break;
         case '_GET_FLAT_FILE_ORDERS_DATA_':
             $report_info['report_type'] = 'order_report';
             break;
        case '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_':
        case '_GET_MERCHANT_LISTINGS_DATA_LITE_':
             $report_info['report_type'] = 'inventory_report';
             break;
        case '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_':
             $report_info['report_type'] = 'amazon_fulfilled_shipments';
             break;
        case '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_':
            $report_info['report_type'] = 'fba_inventory_report';
            break;
         }

         $report_info['amazon_country'] = $amazon_country;
         AmazonReport::insert( $report_info, 'date_requested' );

         return $reportRequestInfo->getReportRequestId();
     }

     static function getReportList($specific_type = "", $country = "US")
     {
         global $CFG;

        require_once( $CFG->amazon_dir .'Model/GetReportListRequest.php' );
        require_once( $CFG->amazon_dir .'Model/TypeList.php' );

        if ($country == "US") $service_url_to_use = $CFG->amazon_serviceUrl;
        else if ($country == "CA") $service_url_to_use = $CFG->amazon_serviceUrlCanada;
                       
        $config = array (
          'ServiceURL' => $service_url_to_use,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
		
        if ($country == 'US')
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID,
             	 AWS_SECRET_ACCESS_KEY,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

		}
		else if ($country == "CA")
		{
         	$service = new MarketplaceWebService_Client(
            	 AWS_ACCESS_KEY_ID_CANADA,
             	 AWS_SECRET_ACCESS_KEY_CANADA,
             	 $config,
             	 APPLICATION_NAME,
             	 APPLICATION_VERSION
             );

		}       


         $request = new MarketplaceWebService_Model_GetReportListRequest();
         if ($country == "US") $request->setMarketplace(MARKETPLACE_ID);
         else if ($country == "CA") $request->setMarketplace(MARKETPLACE_ID_CANADA);
         if ($country == "US") $request->setMerchant(MERCHANT_ID);
         else if ($country == "CA") $request->setMerchant(MERCHANT_ID_CANADA);
         $request->setAvailableToDate(new DateTime('now'));
         $request->setAvailableFromDate(new DateTime('-3 month'));
         $request->setAcknowledged(false);
         if ($specific_type != "")
         {
             $list = new MarketplaceWebService_Model_TypeList();
             $list->setType($specific_type);
             $request->setReportTypeList($list);
         }
         try {
         //ob_start();
          $response = $service->getReportList($request);

//            echo ("Service Response\n");
//            echo ("=============================================================================\n");

//            echo("        GetReportListResponse\n");
            if ($response->isSetGetReportListResult()) {
//            echo("            GetReportListResult\n");
            $getReportListResult = $response->getGetReportListResult();
            if ($getReportListResult->isSetNextToken())
            {
//                echo("                NextToken\n");
//                echo("                    " . $getReportListResult->getNextToken() . "\n");
            }
            if ($getReportListResult->isSetHasNext())
            {
//                echo("                HasNext\n");
//                echo("                    " . $getReportListResult->getHasNext() . "\n");
            }
            $reportInfoList = $getReportListResult->getReportInfoList();
            foreach ($reportInfoList as $reportInfo) {

                AmazonReport::checkReport( $reportInfo );

//                echo("                ReportInfo\n");
                if ($reportInfo->isSetReportId())
                {
//                echo("                    ReportId\n");
//                echo("                        " . $reportInfo->getReportId() . "\n");
                }
                if ($reportInfo->isSetReportType())
                {
//                echo("                    ReportType\n");
//                echo("                        " . $reportInfo->getReportType() . "\n");
                }
                if ($reportInfo->isSetReportRequestId())
                {
//                echo("                    ReportRequestId\n");
//                echo("                        " . $reportInfo->getReportRequestId() . "\n");
                }
                if ($reportInfo->isSetAvailableDate())
                {
//                echo("                    AvailableDate\n");
//                echo("                        " . $reportInfo->getAvailableDate()->format(DATE_FORMAT) . "\n");
                }
                if ($reportInfo->isSetAcknowledged())
                {
//                echo("                    Acknowledged\n");
//                echo("                        " . $reportInfo->getAcknowledged() . "\n");
                }
                if ($reportInfo->isSetAcknowledgedDate())
                {
//                echo("                    AcknowledgedDate\n");
//                echo("                        " . $reportInfo->getAcknowledgedDate()->format(DATE_FORMAT) . "\n");
                }
            }
            }
            if ($response->isSetResponseMetadata()) {
//            echo("            ResponseMetadata\n");
            $responseMetadata = $response->getResponseMetadata();
            if ($responseMetadata->isSetRequestId())
            {
//                echo("                RequestId\n");
//                echo("                    " . $responseMetadata->getRequestId() . "\n");
            }
            }
          //ob_end_clean();

         } catch (MarketplaceWebService_Exception $ex) {
             echo("Caught Exception: " . $ex->getMessage() . "\n");
             echo("Response Status Code: " . $ex->getStatusCode() . "\n");
             echo("Error Code: " . $ex->getErrorCode() . "\n");
             echo("Error Type: " . $ex->getErrorType() . "\n");
             echo("Request ID: " . $ex->getRequestId() . "\n");
             echo("XML: " . $ex->getXML() . "\n");
         }
     }

     static function acknowledgeReport( $report_id )
     {
         global $CFG;

         require_once( $CFG->amazon_dir .'Model/UpdateReportAcknowledgementsRequest.php' );

        $config = array (
          'ServiceURL' => $CFG->amazon_serviceUrl,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );

         $service = new MarketplaceWebService_Client(
                 AWS_ACCESS_KEY_ID,
                 AWS_SECRET_ACCESS_KEY,
                 $config,
                 APPLICATION_NAME,
                 APPLICATION_VERSION
             );

        $parameters = array (
          'Marketplace' => MARKETPLACE_ID,
          'Merchant' => MERCHANT_ID,
          'ReportIdList' => array ('Id' => array ($report_id)),
          'Acknowledged' => true,
        );

        $request = new MarketplaceWebService_Model_UpdateReportAcknowledgementsRequest($parameters);

        try {
              $response = $service->updateReportAcknowledgements($request);

                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo(" UpdateReportAcknowledgementsResponse\n");
                if ($response->isSetUpdateReportAcknowledgementsResult()) {
                    echo(" UpdateReportAcknowledgementsResult\n");
                    $updateReportAcknowledgementsResult = $response->getUpdateReportAcknowledgementsResult();
                    if ($updateReportAcknowledgementsResult->isSetCount())
                    {
                        echo("                Count\n");
                        echo("                    " . $updateReportAcknowledgementsResult->getCount() . "\n");
                    }
                    $reportInfoList = $updateReportAcknowledgementsResult->getReportInfo();
                    foreach ($reportInfoList as $reportInfo) {
                        echo("                ReportInfo\n");
                        if ($reportInfo->isSetReportId())
                        {
                            echo("                    ReportId\n");
                            echo("                        " . $reportInfo->getReportId() . "\n");
                        }
                        if ($reportInfo->isSetReportType())
                        {
                            echo(" ReportType\n");
                            echo("                        " . $reportInfo->getReportType() . "\n");
                        }
                        if ($reportInfo->isSetReportRequestId())
                        {
                            echo(" ReportRequestId\n");
                            echo("                        " . $reportInfo->getReportRequestId() . "\n");
                        }
                        if ($reportInfo->isSetAvailableDate())
                        {
                            echo(" AvailableDate\n");
                            echo("                        " . $reportInfo->getAvailableDate()->format(DATE_FORMAT) . "\n");
                        }
                        if ($reportInfo->isSetAcknowledged())
                        {
                            echo(" Acknowledged\n");
                            echo("                        " . $reportInfo->getAcknowledged() . "\n");
                        }
                        if ($reportInfo->isSetAcknowledgedDate())
                        {
                            echo(" AcknowledgedDate\n");
                            echo("                        " . $reportInfo->getAcknowledgedDate()->format(DATE_FORMAT) . "\n");
                        }
                    }
                }
                if ($response->isSetResponseMetadata()) {
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId())
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                }

         } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         }
     }



    function getFeedSubmissionResult( $feed_id )
    {
    global $CFG;
    require_once( $CFG->amazon_dir .'Model/GetFeedSubmissionResultRequest.php' );

        $config = array (
          'ServiceURL' => $CFG->amazon_serviceUrl,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );

         $service = new MarketplaceWebService_Client(
                 AWS_ACCESS_KEY_ID,
                 AWS_SECRET_ACCESS_KEY,
                 $config,
                 APPLICATION_NAME,
                 APPLICATION_VERSION
             );

         $parameters = array (
          'Marketplace' => MARKETPLACE_ID,
          'Merchant' => MERCHANT_ID,
          'FeedSubmissionId' => $feed_id,
          'FeedSubmissionResult' => @fopen('php://memory', 'rw+'),
        );


        $request = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
      try {
              $response = $service->getFeedSubmissionResult($request);

                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        GetFeedSubmissionResultResponse\n");
                if ($response->isSetGetFeedSubmissionResultResult()) {
                  $getFeedSubmissionResultResult = $response->getGetFeedSubmissionResultResult();
                  echo ("            GetFeedSubmissionResult");
          print_ar( $getFeedSubmissionResultResult );
                  if ($getFeedSubmissionResultResult->isSetContentMd5()) {
                    echo ("                ContentMd5");
                    echo ("                " . $getFeedSubmissionResultResult->getContentMd5() . "\n");
                  }
                }
                if ($response->isSetResponseMetadata()) {
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId())
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                }

     } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
    }
?>