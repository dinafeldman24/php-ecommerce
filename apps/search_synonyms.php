<?
class SearchSynonyms {
	static function insert($info) {
		return db_insert ( 'search_synonyms', $info);
	}
	static function update($id, $info) {
		return db_update ( 'search_synonyms', $id, $info, 'id');
	}
	static function delete($id) {
		return db_delete ( 'search_synonyms', $id );
	}
	static function get($id = 0, $order_id = 0, $order = '', $order_asc = '', $limit = 0, $start = 0, $get_total = false, $code = '') {
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(search_synonyms.id)) AS total ";
		} else {
			$sql .= " search_synonyms.* ";
		}
		
		$sql .= " FROM search_synonyms ";
		
		$sql .= " WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND search_synonyms.id = $id ";
		}
		
		if ($order_id) {
			$sql .= " AND `search_synonyms`.order_id = $order_id";
		}
		
		if ($code) {
			$sql .= " AND `search_synonyms`.code LIKE '$code'";
		}
		
		if (! $get_total) {
			$sql .= ' GROUP BY search_synonyms.id ';
			
			if ($order) {
				$sql .= " ORDER BY ";
				
				$sql .= addslashes ( $order );
				
				if ($order_asc !== '' && ! $order_asc) {
					$sql .= ' DESC ';
				}
			} else {
				$sql .= " ORDER BY search_synonyms.id DESC";
			}
		}
		
		if ($limit > 0) {
			$sql .= db_limit ( $limit, $start );
		}
		
		if (! $get_total) {
			$ret = db_query_array ( $sql );
		} else {
			$ret = db_query_array ( $sql );
			return $ret [0] ['total'];
		}
		return $ret;
	}
	static function get1($id) {
		$id = ( int ) $id;
		
		if (! $id)
			return false;
		$result = self::get ( $id );
		
		return is_array ( $result ) ? array_shift ( $result ) : false;
	}
}

