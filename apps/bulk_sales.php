<?  
class BulkSales
{
	static function get($id=0, $current_only = false)
	{
		$sql = "SELECT ";
	    $sql .= " bulk_sales.* ";		
		$sql .= " FROM bulk_sales ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND bulk_sales.id = $id ";
		}
				
		if ($current_only) $sql .= " AND NOW() BETWEEN bulk_sales.start_time and bulk_sales.end_time AND bulk_sales.active = 'Y'";
	
//		echo $sql;	
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}
	
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('bulk_sales', $id);		
	}
	
	static function insert($info)
	{
		if(!$info)
			return false;
		db_insert('bulk_sales', $info);
		$id = db_insert_id();
		// if current time is within the period of the sale, activate it immediately
		if ((time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time']))) 
		{
			self::activate_bulk_sale($id);
		}					
 
		return true;		
	}
	
	static function insert_individual_sales($bulk_sale_id)
	{
		if (!$bulk_sale_id) return false;
		$the_bulk_sale = self::get1($bulk_sale_id);
		
		$overlapping_sale_string = "";
		
		$sale_cats_array = explode (",", $the_bulk_sale['cat_id_list']);
	   
		$sale_brands_array = explode (",", $the_bulk_sale['brand_id_list']);

		$all_sale_prods = array();
		if (count($sale_cats_array) && count($sale_brands_array) > 0)
		{
			foreach ($sale_cats_array as $sale_cat)
			{
				foreach ($sale_brands_array as $sale_brand)
				{
					// non-defaults in the get(): $sale_cat, active = 'Y', $sale_brand, $limited (2nd to last) = true
					$these_sale_prods = Products::get(0,'','','','',$sale_cat,'Y','','',0, $sale_brand,
				 						0,0,0,0,0,0,0,'',
				 						false,false,'',false,"",'','',0,0,false,
				 						'N','',true, array(),'','', false, "N", true,'');

				 	if (count($these_sale_prods) > 0) $all_sale_prods = array_merge($all_sale_prods, $these_sale_prods);
				 	$symbolic_cats = Cats::getSymbolicSubs( $sale_cat, 'name' );
				 	if ($symbolic_cats)
				 	{
				 		foreach ($symbolic_cats as $symbolic_sale_cat)
				 		{
							$these_sale_prods = Products::get(0,'','','','',$symbolic_sale_cat['id'],'Y','','',0, $sale_brand,
				 						0,0,0,0,0,0,0,'',
				 						false,false,'',false,"",'','',0,0,false,
				 						'N','',true, array(),'','', false, "N", true,'');

				 			if (count($these_sale_prods) > 0) $all_sale_prods = array_merge($all_sale_prods, $these_sale_prods);
				 			
				 		}
				 	}	   						
				}
			}

		}
		else if (count($sale_cats_array) > 0)
		{
			foreach ($sale_cats_array as $sale_cat)
			{
				// non-defaults in the get(): $sale_cat, active = 'Y', $sale_brand, $limited (2nd to last) = true
				$these_sale_prods = Products::get(0,'','','','',$sale_cat,'Y','','',0, 0,
			 						0,0,0,0,0,0,0,'',
			 						false,false,'',false,"",'','',0,0,false,
			 						'N','',true, array(),'','', false, "N", true,'');
			 	if (count($these_sale_prods) > 0) $all_sale_prods = array_merge($all_sale_prods, $these_sale_prods);						
			
			}
			
		}
		else if (count($sale_brands_array) > 0)
		{
			foreach ($sale_brands_array as $sale_brand)
			{
					// non-defaults in the get(): active = 'Y', $sale_brand, $limited (2nd to last) = true
					$these_sale_prods = Products::get(0,'','','','',0,'Y','','',0, $sale_brand,
				 						0,0,0,0,0,0,0,'',
				 						false,false,'',false,"",'','',0,0,false,
				 						'N','',true, array(),'','', false, "N", true,'');

				 	if (count($these_sale_prods) > 0) $all_sale_prods = array_merge($all_sale_prods, $these_sale_prods);										
			}
			
		}
		if (count($all_sale_prods) > 0)
		{		
			// need to loop through each one
			// also get each one's active, non-deleted options
			// do db_replace for each record into sales
			$info = array();
			$info['start_time'] = $the_bulk_sale['start_time'];
			$info['end_time'] = $the_bulk_sale['end_time'];	
			$info['limit_sale_by_inventory'] = 'N';		
			$info['essensa_only'] = $the_bulk_sale['essensa_only'];
			$info['bulk_sales_id'] = $the_bulk_sale['id'];
			
			foreach ($all_sale_prods as $sale_prod)
			{
				$info['product_id'] = $sale_prod['id'];
				$discount_amount = $the_bulk_sale['discount_amount'];
				$options = Products::getProductOptions(0,$sale_prod['id']);
				if ($options)
				{
					foreach($options as $one_option)
					{
						$info['product_option_id'] = $one_option['product_option_id'];
						$reg_price = $one_option['additional_price'] != '0.00' ? $one_option['additional_price'] : $one_option['base_prod_price'];						
						// dollar--just subtract, but don't put price below $1
						if ($the_bulk_sale['discount_type'] == 'dollar') $info['sale_price'] = max('1.00', $reg_price - $discount_amount);
						// percent
						else $info['sale_price'] = $reg_price - (($discount_amount / 100) * $reg_price);  
						
						// check if is already sale in that time period
						$option_param = ($info['product_option_id'] && $info['product_option_id'] > 0) ? $info['product_option_id']: 'none'; 
						$prev_sales = Sales::get(0, false, $info['product_id'], '', $option_param, false, '', $info['start_time'], $info['end_time'] );
						$new_id = Sales::insert($info);
						if ($prev_sales) 
						{
							$overlapping_sale_string .= "<span style='color: red; font-size:18pt'>";
							foreach ($prev_sales as $prev_sale)
							{
								$overlapping_sale_string .= " Please note: Previously entered sale " . $prev_sale['id'] . " overlaps with newly activated sale id $new_id for product " . $info['product_id'] . ", option " . $option_param .".<br/>\n";
							}
							$overlapping_sale_string .= "</span><br/>";
						}						
					}
				}
				else 
				{					
					$info['product_option_id'] = 0;
					$reg_price = $sale_prod['price'];
					if ($the_bulk_sale['discount_type'] == 'dollar') $info['sale_price'] = max('1.00', $reg_price - $discount_amount);
					// percent
					else $info['sale_price'] = $reg_price - (($discount_amount / 100) * $reg_price);
					
					// check first if is already sale in that time period
					$option_param = ($info['product_option_id'] && $info['product_option_id'] > 0) ? $info['product_option_id']: 'none'; 
					$prev_sales = Sales::get(0, false, $info['product_id'], '', $option_param, false, '', $info['start_time'], $info['end_time'] );

					$new_id = Sales::insert($info);
					if ($prev_sales) 
					{
						$overlapping_sale_string .= "<span style='color: red; font-size:18pt'>";
						foreach ($prev_sales as $prev_sale)
						{
							$overlapping_sale_string .= " Please note: Previously entered sale " . $prev_sale['id'] . " overlaps with newly activated sale id $new_id for product " . $info['product_id'] . ", option " . $option_param .".<br/>\n";
						}
						$overlapping_sale_string .= "</span><br/>";
					}					
				}
			}
		}
		if ($overlapping_sale_string != "") 
		{
			$headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			mail("rachel@tigerchef.com, estee@tigerchef.com", "Overlapping sale information", $overlapping_sale_string, $headers);
		}
	}
	
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
		
		$sale = db_query_array("SELECT * FROM bulk_sales WHERE bulk_sales.id = $id");
		$sale = $sale[0];
		 
		if($sale['active'] == "Y"){			
				self::deactivate_bulk_sale($id);
				$bulk_sale_ind_items = Sales::get(0, false, 0, '', 'none', false, $id);
				if($bulk_sale_ind_items)
				{
					foreach ($bulk_sale_ind_items as $ind_item)
					{
						if ($ind_item['sold_out'] == 'Y') Sales::deactivate_already_sold_out_sale($ind_item['id']); 
						else Sales::deactivate_sale($ind_item['id']);
						Sales::delete($ind_item['id']);
					}
				}
				
				$ret = db_update('bulk_sales',$id,$info);
				// re-activate the sale if it is in the sale time-frame
				if ((time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time']))) 
				{
					self::activate_bulk_sale($id);
				}
		} 
		else if ($sale['active'] == 'N')
		{
				$ret = db_update('bulk_sales',$id,$info);
				if ((time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time']))) 
				{
					self::activate_bulk_sale($id);
				}
		}
		
		return $ret;
	}
	static function getJustExpired()
	{
		$sql = "SELECT ";
	    $sql .= " bulk_sales.* ";		
		$sql .= " FROM bulk_sales ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() > bulk_sales.end_time 
	    		AND bulk_sales.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	static function deactivate_bulk_sale($id)
	{
		$sql = "UPDATE bulk_sales SET bulk_sales.active = 'N' 
				WHERE bulk_sales.id = $id";
		db_query($sql);	
	}
	static function getNeedToBeActivated()
	{
		$sql = "SELECT ";
	    $sql .= " bulk_sales.* ";		
		$sql .= " FROM bulk_sales ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() BETWEEN bulk_sales.start_time AND bulk_sales.end_time
	    			AND bulk_sales.active = 'N'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	static function activate_bulk_sale($id)
	{
		//echo "going to activate";
		self::insert_individual_sales($id);		
		$sql = "UPDATE bulk_sales SET bulk_sales.active = 'Y' WHERE bulk_sales.id = $id";
		db_query($sql);				
	}
	
	static function getCurrentEssensaBulkSales()
	{
		$sql = "SELECT  bulk_sales.* ";		
		$sql .= " FROM bulk_sales ";
		$sql .= " WHERE 1 ";					
		$sql .= " AND NOW() BETWEEN bulk_sales.start_time and bulk_sales.end_time AND bulk_sales.active = 'Y'";
		$sql .= " AND essensa_only = 'Y'";
              
		//echo $sql;	
	    $ret = db_query_array($sql);

		return $ret;
		
	}
}?>