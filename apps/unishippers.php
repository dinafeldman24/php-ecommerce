<?php 
class UniShippers {	
	var $SGICN;
	var $dataToSend;	

	function setCredentials($sgicn) {
		$this->credentials = 1;
						
		//Provided by Story Group Support Technician
		$this ->SGICN = $sgicn;
	}
	
	/**
	 * Method expects:
	* destinationCity = String containing destination's city name
	* destinationState = String containing destinations' state name
	* destinationZip = String containing destination's Postal or ZIP code
	* quantity0 = String containing number of items for this line
	* package0 = String describing package type for this line (see section 8 for
			allowable types)
	* freightClass0 = String containing shipment's overall class
	* weight0 = String declaring weight in pounds
	*/
	// Define the function getRate() - no parameters
	function getRate($originCity, $originState, $originZip, $destinationCity, $destinationState, $destinationZip, $weight0, $freightClass0='92.5', $quantity0=1, $package0='PALLETS') {
		global $CFG;
		
		if ($this->credentials != 1) {
			print 'Please set your credentials with the setCredentials function';
			die();
		}
		
		$this->dataToSend = "destinationCity=" . urlencode($destinationCity) . "&";
		$this->dataToSend .= "destinationState=" . urlencode($destinationState) . "&";
		$this->dataToSend .= "destinationZip=" . urlencode($destinationZip) . "&";
		$this->dataToSend .= "quantity0=" . urlencode($quantity0) . "&";
		$this->dataToSend .= "package0=" . urlencode($package0) . "&";
		$this->dataToSend .= "freightClass0=" . urlencode($freightClass0) . "&";
		$this->dataToSend .= "weight0=" . urlencode($weight0) . "&";
		$this->dataToSend .= "originCity=" . urlencode($originCity) . "&";
		$this->dataToSend .= "originState=" . urlencode($originState) . "&";
		$this->dataToSend .= "originZip=" . urlencode($originZip) . "&";
		$this->dataToSend .= "SGICN=" . urlencode($this->SGICN);
// 		mail('tova@tigerchef.com', 'unishippers data', var_export($this->dataToSend,true));
		
		$ch = curl_init($CFG->unishippersURL);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_TIMEOUT, 60);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
// 		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
// 		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$this->dataToSend);
		$response=curl_exec ($ch);
		
		if (curl_error($ch)){
			$error = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
		}
		
		//echo '<!-- '. $result. ' -->'; // THIS LINE IS FOR DEBUG PURPOSES ONLY-IT WILL SHOW IN HTML COMMENTS
		curl_close($ch);
				
		$decoded_response = $this->decodeResponse($response);
		
		if($decoded_response){
			if($decoded_response['error']){
				$error = $response;
			}
		}
		//mail('tova@tigerchef.com', 'unishippers', var_export($this,true).var_export($response,true).var_export($decoded_response,true));
		$this->debug = array('request'=>$this->dataToSend,'response'=>$response,'decoded_response'=>$decoded_response);
		
		if(!$error){
			//mail('tova@tigerchef.com', 'unishippers-rate', var_export($response['rate'],true).var_export($response['error'],true).var_export($response['msg'],true));
				
			return $decoded_response['rate'];
		}else{
			//alert that something failed
			if(!$CFG->in_testing){
				mail($CFG->error_email,'Freight rating failed',var_export($this,true).var_export($error,true));
			}else{
				mail('tova@tigerchef.com','Freight rating failed',var_export($this,true).var_export($error,true));
			}
			//mail('tova@tigerchef.com', 'unishippers-error', var_export($error,true).var_export($response['error'],true).var_export($response['msg'],true));
			return 0;
		}
		
	}
	
	function decodeResponse($response){
		if (strstr($response, "cURL Error") != FALSE) {
			//error
		}
		// loop through server response and form an associative array
		// name/value pair format
		if (strlen($response) != 0) {
			$pairArray = explode(",", $response);
			foreach ($pairArray as $pair) {
				$param = explode("=", $pair);
				$responseArray[urldecode($param[0])] = urldecode($param[1]);
			}
		}
	
		return $responseArray;
	}
}
?>