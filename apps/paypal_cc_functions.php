<?php
class PaypalCc
{
	// fields are based on what's given as responses from Paypal
	// failure	
	var $L_ERRORCODE0;
	var $L_LONGMESSAGE0;

	// for successful auth
	var $AVSCODE;
	var $CVV2MATCH;
	var $TRANSACTIONID;
	var $L_FMFREPORTNAME0;
	
	// for express chekout
	var $TOKEN;
	var $PAYERID;
	
	function PaypalCc()
	{
        global $CFG;
          
 // set up the environment
        $this->set_up_environment($CFG->paypal_environment);
		
	}
	/*  Methods for Website Payments Pro: DoDirectPayment, DoCapture, DoVoid, DoReferenceTransaction  */

	/**
 	* Send HTTP POST Request
 	*
 	* @param	string	The API method name
 	* @param	string	The POST Message fields in&name=value pair format
 	* @return	array	Parsed HTTP Response body
 	* Configure timeout settings to allow for the fact that the DoDirectPayment API operation might take as long as 60 seconds to complete, even though completion in less than 3 seconds is typical. Consider displaying a ?processing transaction? message to the buyer and disabling the Pay button until the transaction finishes.
 	*/
	static function PPHttpPost($methodName_, $nvpStr_) 
	{
		global $CFG;
        //$CFG->Paypal_API_Endpoint = "https://api-3t.".$CFG->paypal_environment.".paypal.com/nvp";
		//$CFG->Paypal_API_Express_Checkout_Redirect = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout";
		
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $CFG->Paypal_API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=".$CFG->Paypal_version."&PWD=".$CFG->Paypal_API_Password."&USER=".$CFG->Paypal_API_UserName."&SIGNATURE=".$CFG->Paypal_API_Signature."$nvpStr_";
	
		//mail("dina.websites@gmail.com","nvp str", "$nvpreq".var_export($ch,true)); 
		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) 
		{
			mail("rachel@tigerchef.com","failed", "$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i =>   $value) 
		{
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr)>   1) 
			{
				$httpParsedResponseAr[$tmpAr[0]] = urldecode($tmpAr[1]);
			}
		}
//print_r($httpParsedResponseAr);
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) 
		{
			exit("Invalid HTTP Response for POST request($nvpreq) to $CFG->Paypal_API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}
	function authorize($cc_num, $exp_date, $cc_type, $grand_total, $auth_fields)
	{
		$payment_type = urlencode('Authorization');				// or 'Sale'
		$ipAddress = urlencode($_SERVER['REMOTE_ADDR']);
		$currency_id = urlencode('USD');
		$nvpStr = "&PAYMENTACTION=$payment_type".
				"&AMT=$grand_total".
				"&CREDITCARDTYPE=$cc_type".
				"&ACCT=$cc_num".
				"&EXPDATE=$exp_date".
				"&CVV2=".urlencode($auth_fields['cvv2']).
				"&FIRSTNAME=".urlencode($auth_fields['first_name']).
				"&LASTNAME=".urlencode($auth_fields['last_name']).
				"&STREET=".urlencode($auth_fields['streetaddress']).
				"&CITY=".urlencode($auth_fields['city']).
				"&STATE=".urlencode($auth_fields['state']).
				"&ZIP=".urlencode($auth_fields['zipcode']).
				"&COUNTRYCODE=US".
				
				"&SHIPTONAME=".urlencode($auth_fields['shipto_name']).
				"&SHIPTOSTREET=".urlencode($auth_fields['shipto_street']).
				"&SHIPTOSTREET2=".urlencode($auth_fields['shipto_street2']).
				"&SHIPTOCITY=".urlencode($auth_fields['shipto_city']).
				"&SHIPTOSTATE=".urlencode($auth_fields['shipto_state']).
				"&SHIPTOZIP=".urlencode($auth_fields['shipto_zipcode']).
				"&SHIPTOCOUNTRY=US".				
				
				"&CURRENCYCODE=$currency_id".
				"&INVNUM=".$auth_fields['ordernum'].
				"&RETURNFMFDETAILS=1";

		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('DoDirectPayment', $nvpStr);

		$this->setAVSCODE($httpParsedResponseAr['AVSCODE']);
		$this->setCVV2MATCH($httpParsedResponseAr['CVV2MATCH']);
		
		if ("SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			$this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFPENDINGNAME0']);
			$cancel_result = $this->cancel_pending_transaction ($httpParsedResponseAr['TRANSACTIONID']);
			if ($cancel_result == "failure") mail("rachel@tigerchef.com", "cancel pending transaction failed", $httpParsedResponseAr['TRANSACTIONID']);
			
			return "failure";
		}
		else if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"])) 
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);

			return "success";
		} 
		else  
		{		
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			$this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFDENYNAME0']);
			return "failure";
		}		
	}
	function sale($cc_num, $exp_date, $cc_type, $amount, $auth_fields)
	{
		$payment_type = urlencode('Sale');			
		$ipAddress = urlencode($_SERVER['REMOTE_ADDR']);
		$currency_id = urlencode('USD');
		$amount = round($amount, 2);
		$nvpStr = "&PAYMENTACTION=$payment_type".
				"&AMT=$amount".
				"&CREDITCARDTYPE=$cc_type".
				"&ACCT=$cc_num".
				"&EXPDATE=$exp_date".
				"&CVV2=".urlencode($auth_fields['cvv2']).
				"&FIRSTNAME=".urlencode($auth_fields['first_name']).
				"&LASTNAME=".urlencode($auth_fields['last_name']).
				"&STREET=".urlencode($auth_fields['streetaddress']).
				"&CITY=".urlencode($auth_fields['city']).
				"&STATE=".urlencode($auth_fields['state']).
				"&ZIP=".urlencode($auth_fields['zipcode']).
				"&COUNTRYCODE=US".
				
				"&SHIPTONAME=".urlencode($auth_fields['shipto_name']).
				"&SHIPTOSTREET=".urlencode($auth_fields['shipto_street']).
				"&SHIPTOSTREET2=".urlencode($auth_fields['shipto_street2']).
				"&SHIPTOCITY=".urlencode($auth_fields['shipto_city']).
				"&SHIPTOSTATE=".urlencode($auth_fields['shipto_state']).
				"&SHIPTOZIP=".urlencode($auth_fields['shipto_zipcode']).
				"&SHIPTOCOUNTRY=US".
				
				"&CURRENCYCODE=$currency_id".
				"&INVNUM=".$auth_fields['ordernum'].
				"&RETURNFMFDETAILS=1";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('DoDirectPayment', $nvpStr);
	
		$this->setAVSCODE($httpParsedResponseAr['AVSCODE']);
		$this->setCVV2MATCH($httpParsedResponseAr['CVV2MATCH']);
		
		if ("SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			$this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFPENDINGNAME0']);
			$cancel_result = $this->cancel_pending_transaction ($httpParsedResponseAr['TRANSACTIONID']);
			if ($cancel_result == "failure") mail("rachel@tigerchef.com", "cancel pending transaction failed", $httpParsedResponseAr['TRANSACTIONID']);
			
			return "failure";
		}
		else if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"])) 
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			
			return "success";
		}
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			$this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFDENYNAME0']);
						
			return "failure";
		}
	}
	
	function capture($transaction_id, $amt=0, $complete_code_type="NotComplete")
	{	
		// Set request-specific fields.
		$authorizationID = urlencode($transaction_id);
		$amt = urlencode($amt);
		$currency = urlencode('USD');                            // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
		$complete_code_type = urlencode($complete_code_type);                // or 'NotComplete'
		//$note = urlencode('example_note');
	
		// Add request-specific fields to the request string.
		$nvpStr="&AUTHORIZATIONID=$authorizationID&AMT=$amt&COMPLETETYPE=$complete_code_type&CURRENCYCODE=$currency";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('DoCapture', $nvpStr);
		$resp = print_r($httpParsedResponseAr, true);		
		
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
		{
			if (!$httpParsedResponseAr['TRANSACTIONID']) mail("rachel@tigerchef.com", "resp", $resp);
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			return "success";
		} 
		else  
		{		
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return "failure";
		}	
	}
	function void ($transaction_id)
	{
		$authorizationID = urlencode($transaction_id);
        //$note = urlencode('example_note');

        // Add request-specific fields to the request string.
        $nvpStr="&AUTHORIZATIONID=$authorizationID";

        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = PaypalCc::PPHttpPost('DoVoid', $nvpStr);
               
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
        {
        	$this->setTRANSACTIONID($httpParsedResponseAr['AUTHORIZATIONID']);
			return true;
        } else  
        {
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return false;
        }
	}
	
	function credit ($transaction_id, $amount, $note)
	{
		$transaction_id = urlencode($transaction_id);
		$note = urlencode($note);
		$amount = urlencode(round($amount, 2));
		$currency = urlencode("USD");
		$refund_type = urlencode("Partial");		
	
		// Add request-specific fields to the request string.
		$nvpStr = "&TRANSACTIONID=$transaction_id";
		$nvpStr .= "&REFUNDTYPE=$refund_type";
		$nvpStr .= "&AMT=$amount";
		$nvpStr .= "&CURRENCYCODE=$currency";
		$nvpStr .= "&NOTE=$note";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('RefundTransaction', $nvpStr);
		 
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['REFUNDTRANSACTIONID']);
			return "success";
		} 
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return "failure";
		}
	}
	
	function do_reference_transaction ($orig_transaction_id, $amt=0, $type="Authorization")
	{
		// Set request-specific fields.
		$orig_transaction_id = urlencode($orig_transaction_id);
		$amt = urlencode($amt);
		$currency = urlencode('USD');                            // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
		$type = urlencode($type);                // or 'NotComplete'
		//$note = urlencode('example_note');
	
		// Add request-specific fields to the request string.
		$nvpStr="&REFERENCEID=$orig_transaction_id&AMT=$amt&PAYMENTACTION=$type&CURRENCYCODE=$currency&RETURNFMFDETAILS=1";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('DoReferenceTransaction', $nvpStr);
		
		if ("SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			$accept_result = $this->accept_pending_transaction ($httpParsedResponseAr['TRANSACTIONID']);
			if ($accept_result == "failure") mail("rachel@tigerchef.com", "accept pending transaction failed", $httpParsedResponseAr['TRANSACTIONID']);
			return "success";	
		}
		else if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			
			return "success";
		}
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			$this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFDENYNAME0']);
						
			return "failure";
		}
	}
	
	function cancel_pending_transaction ($transaction_id)
	{
		$transaction_id = urlencode($transaction_id);	
	
		// Add request-specific fields to the request string.
		$nvpStr = "&TRANSACTIONID=$transaction_id";
		$nvpStr .= "&ACTION=Deny";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('ManagePendingTransactionStatus', $nvpStr);
		 
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			return "success";
		} 
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return "failure";
		}
	}
	
	function accept_pending_transaction ($transaction_id)
	{
		$transaction_id = urlencode($transaction_id);	
	
		// Add request-specific fields to the request string.
		$nvpStr = "&TRANSACTIONID=$transaction_id";
		$nvpStr .= "&ACTION=Accept";
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('ManagePendingTransactionStatus', $nvpStr);
		 
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			$this->setTRANSACTIONID($httpParsedResponseAr['TRANSACTIONID']);
			return "success";
		} 
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return "failure";
		}
	}
	
    function set_express_checkout($fields,$cancel_url)
    {
        global $CFG;
        global $cart;

        $ipAddress = urlencode($_SERVER['REMOTE_ADDR']);
        $return_url = urlencode($CFG->sslurl . $CFG->Paypal_API_Express_Checkout_Return_Url);
        $callback_url = urlencode($CFG->sslurl . $CFG->Paypal_API_Express_Checkout_Callback_Url);
        $logo_image = urlencode($CFG->sslurl . $CFG->Paypal_API_Express_Checkout_Logo_Img);
        $hdr_logo_image = urlencode($CFG->sslurl . $CFG->Paypal_API_Express_Checkout_Logo_ImgHDR);

        $nvpStr .= "&PAYMENTREQUEST_0_PAYMENTACTION=".urlencode('Authorization').
            "&PAYMENTREQUEST_0_AMT=".urlencode($fields['subtotal'] - $fields['discount'] - $fields['rewards']).
            "&PAYMENTREQUEST_0_CURRENCYCODE=".urlencode('USD').
			"&RETURNURL=$return_url".
            "&CANCELURL=".urlencode($cancel_url).
            "&HDRIMG=$hdr_logo_image".
            "&LOGOIMG=$logo_image".
            "&EMAIL=".urlencode($_SESSION['cust_username']).
            "&CARTBORDERCOLOR=$CFG->Paypal_API_Express_Checkout_Border_Color";

			
       // mail('dina.websites@gmail.com', 'setEC', var_export($nvpStr,true) .'<br>'. var_export($nvpFormatedStr,true) .'<br>'. var_export($httpParsedResponseAr,true));
        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = PaypalCc::PPHttpPost('SetExpressCheckout', $nvpStr);

        $nvpFormatedStr = explode("&", $nvpStr);
       // mail('dina.websites@gmail.com,rachel@tigerchef.com', 'setEC', var_export($nvpStr,true) .'<br>'. var_export($nvpFormatedStr,true) .'<br>'. var_export($httpParsedResponseAr,true));

        //mail('tova@tigerchef.com', 'setEC', var_export($nvpFormatedStr,true) .'<br>'. var_export($httpParsedResponseAr,true));
        //mail('tova@tigerchef.com', 'setEC2', var_export($httpParsedResponseAr,true));

        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]))
        {
            $this->setTOKEN($httpParsedResponseAr['TOKEN'],true);
            //mail("rachel@tigerchef.com", "success", "");
            return "success";
        }
        else
        {
            $this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
            $this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
            //mail("rachel@tigerchef.com", "failure", "");
            return "failure";
        }
    }
	
	function callback_response($shipping_methods)
	{
		global $cart;
		global $CFG;
		 
		$nvpStr = "METHOD=CallbackResponse&CURRENCYCODE=USD";
	
		$shipping_method_count=0;
		foreach ($shipping_methods as $shipping_method){
				
			$default = ($shipping_method['code'] == $cart->data['shipping_method']) ? 'true' : 'false' ;
				
			$nvpStr .= "&L_SHIPPINGOPTIONISDEFAULT".$shipping_method_count."=".urlencode($default).
			"&L_SHIPPINGOPTIONNAME".$shipping_method_count."=".urlencode('').
			"&L_SHIPPINGOPTIONLABEL".$shipping_method_count."=".urlencode($shipping_method['name']).
			"&L_SHIPPINGOPTIONAMOUNT".$shipping_method_count."=".urlencode(round($shipping_method['cost_unformated'], 2)).
			"&L_TAXAMT".$shipping_method_count."=".urlencode($shipping_method['adjusted_sales_tax']);
	
			$shipping_method_count++;
		}
	
		// 		$nvpStr = "METHOD=CallbackResponse&OFFERINSURANCEOPTION=true&L_SHIPPINGOPTIONNAME0=".urlencode($shipping_method['name'])."&" .
		// 				"L_SHIPPINGOPTIONLABEL0=dsfdghfj&L_SHIPPINGOPTIONAMOUNT0=".urlencode(round($shipping_method['cost_unformated'], 2))."&L_TAXAMT0=2.00&" .
		// 				"L_INSURANCEAMOUNT0=2.00&L_SHIPPINGOPTIONISDEFAULT0=true";
	
		// 		mail('tova@tigerchef.com', 'get_shipping_info2', var_export($shipping_methods,true).var_export($nvpStr,true));
	
		echo $nvpStr;
	}
	
	function get_express_checkout_details()
	{
		$nvpStr = "&TOKEN=".$this->getTOKEN();
	
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = PaypalCc::PPHttpPost('GetExpressCheckoutDetails', $nvpStr);
	
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			if($httpParsedResponseAr['PAYMENTREQUEST_0_SHIPTOZIP'] && !$httpParsedResponseAr['PAYMENTREQUEST_0_SHIPTOSTATE']){
                //we need to detect the state, because paypal are having issues
                $debug['ORIGINAL_RETURN'] = $httpParsedResponseAr;
				$city_state = ZipCodesUSA::get_city_state_from_zip(substr($httpParsedResponseAr['PAYMENTREQUEST_0_SHIPTOZIP'],0,5));
                $httpParsedResponseAr['PAYMENTREQUEST_0_SHIPTOSTATE'] = $city_state['StateAbbr'];
                $debug['FIXED_PAYPAL_RETURN'] =  $httpParsedResponseAr;
                $debug['CITY_STATE'] = $city_state;
                mail('rachel@tigerchef.com,tova@tigerchef.com','paypal - state detected based on zip',var_export($debug,true));
            }
			return $httpParsedResponseAr;
		}
		else
		{
			$this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
			$this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
			return "failure";
		}
	}
	
    function do_express_checkout_payment ($fields,$items)
    {
        global $CFG;

        $nvpStr = "&TOKEN=".$this->getTOKEN().
            "&PAYERID=".$this->getPAYERID().
            "&PAYMENTREQUEST_0_PAYMENTACTION=Authorization".

            "&PAYMENTREQUEST_0_AMT=".urlencode($fields['grand_total']).

            "&SHIPTONAME=".$fields['shipping_info']['shipping_fname'].
            //	"&SHIPTONAME=".urlencode($fields['shipping_info']['shipping_fname']).
            "&SHIPTOSTREET=".urlencode($fields['shipping_info']['shipping_address']).
            "&SHIPTOSTREET2=".urlencode($fields['shipping_info']['shipping_address2']).
            "&SHIPTOCITY=".urlencode($fields['shipping_info']['shipping_city']).
            "&SHIPTOSTATE=".urlencode($fields['shipping_info']['shipping_state']).
            "&SHIPTOZIP=".urlencode($fields['shipping_info']['shipping_zip']).
            "&SHIPTOPHONENUM=".urlencode($fields['shipping_info']['shipping_home_phone']).
            "&SHIPTOCOUNTRY=US";

        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = PaypalCc::PPHttpPost('DoExpressCheckoutPayment', $nvpStr);
// 		mail('tova@tigerchef.com', 'doexpress', var_export($httpParsedResponseAr,true).$nvpStr);

        $nvpFormatedStr = explode("&", $nvpStr);
        //mail('tova@tigerchef.com', 'doexpress', var_export($nvpFormatedStr,true) .'<br>'. var_export($httpParsedResponseAr,true));


        if ("SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
        {
            $this->setTRANSACTIONID($httpParsedResponseAr['PAYMENTINFO_0_TRANSACTIONID']);
            $this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
            $this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
            $this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFPENDINGNAME0']);
            $cancel_result = $this->cancel_pending_transaction ($httpParsedResponseAr['PAYMENTINFO_0_TRANSACTIONID']);
            if ($cancel_result == "failure") mail("rachel@tigerchef.com,tova@tigerchef.com", "cancel pending transaction failed", $httpParsedResponseAr['PAYMENTINFO_0_TRANSACTIONID']);

            return "failure";
        }
        else if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]))
        {
            $this->setTRANSACTIONID($httpParsedResponseAr['PAYMENTINFO_0_TRANSACTIONID']);

            return "success";
        }
        else
        {
            $this->setL_ERRORCODE0($httpParsedResponseAr['L_ERRORCODE0']);
            if($this->getL_ERRORCODE0() == "10486"){ //Funding Failure - Redirect back to paypal with original token
                $this->setL_LONGMESSAGE0("There is an issue with the payment option selected in paypal. " .
                    "Please go back to <a href='{$CFG->Paypal_API_Express_Checkout_Redirect}&token={$this->getTOKEN()}'>paypal</a> and fix it.");
            }else{
                $this->setL_LONGMESSAGE0($httpParsedResponseAr['L_LONGMESSAGE0']);
            }

            $this->setL_FMFDENYNAME0($httpParsedResponseAr['L_FMFDENYNAME0']);

            /*
            if($this->getL_ERRORCODE0() == "10486"){ //Funding Failure - Redirect back to paypal with original token
                header('Location:'. $CFG->Paypal_API_Express_Checkout_Redirect . "&token=" .$this->getTOKEN());
                exit();
            }
            */
            return "failure";
        }
    }
	
	function setUpItemsForExpressCheckout($fields,$items){
	//mail('tova@tigerchef.com', 'set up in paypal',var_export($fields,true));
		$temp = "&PAYMENTREQUEST_0_SHIPPINGAMT=".urlencode($fields['default_shipping_cost']);
	
		$nvpStr = "&PAYMENTREQUEST_0_AMT=".urlencode($fields['grand_total']).
		"&PAYMENTREQUEST_0_ITEMAMT=".urlencode($fields['subtotal'] + $fields['min_charge'] + $fields['liftgate_fee'] - $fields['discount'] - $fields['rewards']).
		"&PAYMENTREQUEST_0_TAXAMT=".urlencode($fields['tax']).
			
		"&PAYMENTREQUEST_0_INVNUM =".urlencode($fields['ordernum']).
		"&PAYMENTREQUEST_0_CURRENCYCODE=".urlencode('USD');
	
		$item_number=0;
		foreach ($items as $item){
			if( $item['options'] )
			{
				$item['mfr_part_num'] = $item['options'][0]['mfr_part_num'] . ' - ' . $item['options'][0]['value'];
			}
			$nvpStr .= "&L_PAYMENTREQUEST_0_NAME".$item_number."=".urlencode($item['product_name']).
			"&L_PAYMENTREQUEST_0_AMT".$item_number."=".urlencode($item['cart_price']).
			"&L_PAYMENTREQUEST_0_NUMBER".$item_number."=".urlencode($item['mfr_part_num']).
			"&L_PAYMENTREQUEST_0_QTY".$item_number."=".urlencode($item['qty']);
	
			$item_number++;
		}
		if($fields['min_charge']){
	
			$nvpStr .= "&L_PAYMENTREQUEST_0_NAME".$item_number."=".urlencode('Manufacturer Minimum Charges').
			"&L_PAYMENTREQUEST_0_AMT".$item_number."=".urlencode($fields['min_charge']).
			"&L_PAYMENTREQUEST_0_QTY".$item_number."=1";
			$item_number++;
		}
		if($fields['liftgate_fee']){
			$nvpStr .= "&L_PAYMENTREQUEST_0_NAME".$item_number."=".urlencode('Liftgate Fee').
			"&L_PAYMENTREQUEST_0_AMT".$item_number."=".urlencode($fields['liftgate_fee']).
			"&L_PAYMENTREQUEST_0_QTY".$item_number."=1";
			$item_number++;
		}
		if($fields['discount']){
			$nvpStr .= "&L_PAYMENTREQUEST_0_NAME".$item_number."=".urlencode('Discount').
			"&L_PAYMENTREQUEST_0_AMT".$item_number."=".urlencode('-'.$fields['discount']).
			"&L_PAYMENTREQUEST_0_QTY".$item_number."=1";
			$item_number++;
		}
		if($fields['rewards']){
			$nvpStr .= "&L_PAYMENTREQUEST_0_NAME".$item_number."=".urlencode('Reward Points').
			"&L_PAYMENTREQUEST_0_AMT".$item_number."=".urlencode('-'.$fields['rewards']).
			"&L_PAYMENTREQUEST_0_QTY".$item_number."=1";
			$item_number++;
		}
	
		return $nvpStr;
	}
	
	function getNicelyFormattedFmfError()
	{
		$fmf_error = self::getL_FMFDENYNAME0();
		$avs_resp = self::getAVSCODE();
		
		switch ($fmf_error)
		{
			case "AVS No Match":
				return " AVS mismatch. The address provided does not match billing address of cardholder.";
				break;
			case "AVS Partial Match":
				if ($avs_resp == "Z" || $avs_resp == "W" || $avs_resp == "P") $avs_partial_text = "Zip code matched billing address of cardholder but address did not.";		 
				else if ($avs_resp == "A" || $avs_resp == "B") $avs_partial_text = "Street address matched billing address of cardholder but zip code did not.";
				return " AVS Partial Match.  $avs_partial_text";
				break;
			
			default:
				return $fmf_error;
				break;				
		}
	}
	/**
 	* @return the $L_ERRORCODE0
 	*/
	public function getL_ERRORCODE0() {
		return $this->L_ERRORCODE0;
	}

	/**
 	* @return the $L_LONGMESSAGE0
 	*/
	public function getL_LONGMESSAGE0() {
		return $this->L_LONGMESSAGE0;
	}
	
	/**
 	* @return the $AVSCODE
 	*/
	public function getAVSCODE() {
		return $this->AVSCODE;
	}
	
	/**
 	* @return the $CVV2MATCH
 	*/
	public function getCVV2MATCH() {
		return $this->CVV2MATCH;
	}
	
	/**
 	* @return the $TRANSACTIONID
 	*/
	public function getTRANSACTIONID() {
		return $this->TRANSACTIONID;
	}
	
	/**
 	* @return the $L_FMFREPORTNAME0
 	*/
	public function getL_FMFDENYNAME0() {
		return $this->L_FMFDENYNAME0;
	}
	/**
	 * @return the $TOKEN
	 */
	public function getTOKEN() {
		return $this->TOKEN;
	}
	
	/**
	 * @return the $PAYERID
	 */
	public function getPAYERID() {
		return $this->PAYERID;
	}
	
	/**
	 * @param $token - express checkout token
	 * @return the sessioninfo stored for the token
	 */
	public static function getSessionInfo($token){
        $token = preg_replace('/\'A=0/','',$token);
		$sql = "SELECT * FROM paypal_tokens
		LEFT JOIN sessions ON sessions.session_id = paypal_tokens.session_id
		WHERE token = '$token' ";
		return db_get1(db_query_array($sql));
	}
	
	/**	
 	* @param field_type $L_ERRORCODE0
 	*/
	public function setL_ERRORCODE0($L_ERRORCODE0) {
		$this->L_ERRORCODE0 = $L_ERRORCODE0;
	}
	
	/**
 	* @param field_type $L_LONGMESSAGE0
 	*/
	public function setL_LONGMESSAGE0($L_LONGMESSAGE0) {
		$this->L_LONGMESSAGE0 = $L_LONGMESSAGE0;
	}
	
	/**
 	* @param field_type $AVSCODE
 	*/
	public function setAVSCODE($AVSCODE) {
		$this->AVSCODE = $AVSCODE;
	}
	
	/**
 	* @param field_type $CVV2MATCH
 	*/
	public function setCVV2MATCH($CVV2MATCH) {
		$this->CVV2MATCH = $CVV2MATCH;
	}
	
	/**
 	* @param field_type $TRANSACTIONID
 	*/
	public function setTRANSACTIONID($TRANSACTIONID) {
		$this->TRANSACTIONID = $TRANSACTIONID;
	}
	
	/**	
 	* @param field_type $L_FMFREPORTNAME0
 	*/
	public function setL_FMFDENYNAME0($L_FMFDENYNAME0) {
		$this->L_FMFDENYNAME0 = $L_FMFDENYNAME0;
	}
	/**
	 * @param field_type $TOKEN
	 */
	public function setTOKEN($TOKEN,$save_in_db = false) {
		$this->TOKEN = $TOKEN;
	
		if($save_in_db){
			$info = array('token' => $TOKEN,
					'session_id' => session_id(),
					'customer_id' => $_SESSION['cust_acc_id']
			);
				
			db_insert('paypal_tokens', $info);
		}
	}
	
	/**
	 * @param field_type $PAYERID
	 */
	public function setPAYERID($PAYERID) {
		$this->PAYERID = $PAYERID;
	}
	
	 /**
     * @param $paypal_environment 'sandbox' or 'live'
     */
    public function set_up_environment($paypal_environment)
    {
        global $CFG;

        if ($paypal_environment == "sandbox" || $paypal_environment == "beta-sandbox") {
            $CFG->Paypal_API_UserName = $CFG->paypal_sandbox_api_username;
            $CFG->Paypal_API_Password = $CFG->paypal_sandbox_api_password;
            $CFG->Paypal_API_Signature = $CFG->paypal_sandbox_api_signature;

            $CFG->Paypal_API_Endpoint = $CFG->paypal_sandbox_api_endpoint;
            $CFG->Paypal_API_Express_Checkout_Redirect = $CFG->paypal_sandbox_api_express_checkout_redirect;

        } else {
            $CFG->Paypal_API_UserName = $CFG->paypal_live_api_username;
            $CFG->Paypal_API_Password = $CFG->paypal_live_api_password;
            $CFG->Paypal_API_Signature = $CFG->paypal_live_api_signature;

            $CFG->Paypal_API_Endpoint = $CFG->paypal_live_api_endpoint;
            $CFG->Paypal_API_Express_Checkout_Redirect = $CFG->paypal_live_api_express_checkout_redirect;
        }
    }
	
	
}
?>