<? 
class Sales
{
	static function get($id=0, $current_only = false, $product_id = 0, $sold_out = '', $product_option_id = 'none', $take_essensa_into_account = false, $bulk_sales_id = '', $date_range_start = '', $date_range_end = '', $order_by = '')
	{  
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND sales.id = $id ";
		}
		if ($product_id > 0) {
		    $sql .= " AND sales.product_id = $product_id ";
		}
		
		if ($product_option_id !== 'none') {						
		    $sql .= " AND sales.product_option_id = $product_option_id ";
		}								
		if ($sold_out != "") {
		    $sql .= " AND sales.sold_out = '$sold_out' ";
		}				
		if ($current_only) $sql .= " AND sales.active = 'Y' AND NOW() BETWEEN sales.start_time and sales.end_time";
		if ($date_range_start != '' && $date_range_end != '') 
		{
			$sql .= " AND ('$date_range_start' BETWEEN sales.start_time AND sales.end_time 
					OR '$date_range_end' BETWEEN sales.start_time and sales.end_time 
					OR '$date_range_start' < sales.start_time AND '$date_range_end' > sales.start_time)";
		}
		if ($take_essensa_into_account)
		{
			// if the person isn't logged in or the person isn't an essensa member, 
			// only look for essensa_only = 'Y'
			if (!($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember()))
			{
				$sql .= " AND essensa_only = 'N'";
			}               
		}
		if ($bulk_sales_id !== '')
		{
			$sql .= " AND sales.bulk_sales_id = " . $bulk_sales_id;
		}	
	
		if ($order_by != '') $sql .= " ORDER BY $order_by";
	    $ret = db_query_array($sql);
//mail("rachel@tigerchef.com", "sale sql 2", $sql);
		return $ret;
	}
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}

	static function getCurrentNotSoldOutEssensaSales()
	{
		$sql = "SELECT  sales.* ";		
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";
	    $sql .= " AND sales.sold_out = 'N' ";
						
		$sql .= " AND NOW() BETWEEN sales.start_time and sales.end_time AND sales.active = 'Y'";
		$sql .= " AND essensa_only = 'Y' and bulk_sales_id = 0";
              
//		echo $sql;	
	    $ret = db_query_array($sql);

		return $ret;
		
	}
	
	static function seeIfCurrentSaleForProd ($product_id, $product_option_id=0, $take_essensa_into_account = false)
	{
		$ret = self::get(0, true, $product_id, '', $product_option_id, $take_essensa_into_account);
		if ($ret) return true;
		else return false;
	}
	
	static function seeIfCurrentNotSoldOutSaleForProd ($product_id, $product_option_id=0)
	{ 
		$ret = self::get(0, true, $product_id, 'N', $product_option_id, true);
		if ($ret) return true;
		else return false;
	}
	
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('sales', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
		if (!$info['limit_sale_by_inventory']) $info['limit_sale_by_inventory'] = 'N';	
		db_insert('sales', $info);
		$id = db_insert_id();
		// if current time is within the period of the sale, activate it immediately
		if (time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time']))
		{
			self::activate_sale($id);			
		}
 
		return $id;
		
	}
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
		if (!$info['limit_sale_by_inventory']) $info['limit_sale_by_inventory'] = 'N';
		$sale = db_query_array("SELECT * FROM sales WHERE sales.id = $id");
		$sale = $sale[0];
		 
		if($sale['active'] == "Y"){			
			if($info['product_id'] != $sale['product_id'] || $info['product_option_id'] != $sale['product_option_id'] || $info['essensa_only'] != $sale['essensa_only'] ){
				self::deactivate_sale($id);
				$ret = db_update('sales',$id,$info);
				self::activate_sale($id);
			} else if($info['sale_price'] != $sale['sale_price']){
				//update sales table
				$ret = db_update('sales',$id,$info);
				//update products or product_options table
				if ($info['product_option_id'] == 0 || $info['product_option_id'] == '') 
				{
					db_query("UPDATE products, sales SET products.price = sales.sale_price
						WHERE sales.id = $id AND products.id = sales.product_id");
				}
				else 	
				{
					db_query("UPDATE product_options, sales SET product_options.additional_price = sales.sale_price
						WHERE sales.id = $id AND product_options.id = sales.product_option_id AND 
						product_options.product_id = sales.product_id");
				}									
			}
			if ($info['sale_cost'] == '') $info['sale_cost'] = "0.00";			
			if($info['sale_cost'] != $sale['sale_cost']){
				//update sales table
				$ret = db_update('sales',$id,$info);
				//update products or product_options table
				if ($info['product_option_id'] == 0 || $info['product_option_id'] == '') 
				{
					if (!$info['sale_cost'] && $sale['sale_cost'])
					{
						db_query("UPDATE products, sales SET products.vendor_price = sales.old_cost
						WHERE sales.id = $id AND products.id = sales.product_id");
					}
					
					else 
					{	db_query("UPDATE products, sales SET products.vendor_price = sales.sale_cost
						WHERE sales.id = $id AND products.id = sales.product_id");
					}
				}
				else 	
				{	
					if (!$info['sale_cost'] && $sale['sale_cost'])				
					{
						db_query("UPDATE product_options, sales SET product_options.vendor_price = sales.old_cost
						WHERE sales.id = $id AND product_options.id = sales.product_option_id AND 
						product_options.product_id = sales.product_id");						
					}
					else 
					{
						db_query("UPDATE product_options, sales SET product_options.vendor_price = sales.sale_cost
						WHERE sales.id = $id AND product_options.id = sales.product_option_id AND 
						product_options.product_id = sales.product_id");
					}
				}									
			}
			if ($info['start_time'] != $sale['start_time'] || $info['end_time'] != $sale['end_time'])
			{
				// if the dates/times of the sale changed and now is no longer in the time of the sale
				if (!(time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time'])))
				{	
					$ret = db_update('sales',$id,$info);
					if ($sale['sold_out'] == 'Y') self::deactivate_already_sold_out_sale($id); 
					else self::deactivate_sale($id);
				}
			}
			if (!$ret) $ret = db_update('sales',$id,$info);
		}
		else if ($sale['active'] == 'N' && 
			($info['start_time'] != $sale['start_time'] || $info['end_time'] != $sale['end_time']) && 
			time() >= strtotime($info['start_time']) && time() <= strtotime($info['end_time']))
			{
				$ret = db_update('sales',$id,$info);
				self::activate_sale($id);
			}
		else{
			$ret = db_update('sales',$id,$info);
		}
		return $ret;
	}
	
	static function getJustExpiredAndSoldOut()
	{
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() > sales.end_time 
	    		AND sales.sold_out = 'Y' AND sales.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	static function getJustExpiredAndNotSoldOut()
	{
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() > sales.end_time 
	    		AND sales.sold_out = 'N' AND sales.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function getNeedToBeActivated()
	{
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";
		
	    $sql .= " AND NOW() BETWEEN sales.start_time AND sales.end_time
	    			AND sales.sold_out = 'N' AND sales.active = 'N'";
		
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function getActiveButSoldOut()
	{
		global $CFG;
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales LEFT JOIN inventory ON (sales.product_id = inventory.product_id 
				  AND sales.product_option_id = inventory.product_option_id 
				  AND inventory.store_id = $CFG->default_inventory_location)";
		$sql .= " WHERE 1 ";
		$sql .= " AND limit_sale_by_inventory = 'Y'";
	    $sql .= " AND NOW() BETWEEN start_time AND end_time";
	    $sql .= " AND sales.sold_out = 'N' AND sales.active = 'Y' 
	    		AND (inventory.qty <= 0 OR inventory.qty IS NULL)";
		
	    $ret = db_query_array($sql);
		//echo $sql;
		return $ret;
	}
	static function getActiveButNoLongerSoldOut()
	{
		global $CFG;
		$sql = "SELECT ";
	    $sql .= " sales.* ";		
		$sql .= " FROM sales LEFT JOIN inventory ON (sales.product_id = inventory.product_id 
				  AND sales.product_option_id = inventory.product_option_id 
				  AND inventory.store_id = $CFG->default_inventory_location)";
		$sql .= " WHERE 1 ";
		$sql .= " AND limit_sale_by_inventory = 'Y'";
	    $sql .= " AND NOW() BETWEEN start_time AND end_time";
	    $sql .= " AND sales.sold_out = 'Y' AND sales.active = 'Y' 
	    		AND inventory.qty > 0 AND inventory.qty IS NOT NULL";
		//echo $sql;
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function deactivate_sale($id)
	{
		$sale_info=self::get1($id);
		if ($sale_info['product_option_id'] != 0)
		{
			$sql = "UPDATE product_options, sales, products SET			 
				sales.active = 'N', ";
			
			if ($sale_info['essensa_only'] == 'Y') 
			{
				$sql .= " product_options.essensa_price = IF(products.essensa_price <> sales.old_essensa_price,sales.old_essensa_price,'0.00')";				
			}
			else 
			{
				$sql .= " product_options.additional_price = IF(products.price <> sales.old_price, sales.old_price, '0.00')";				
			}
			
			if ($sale_info['sale_cost'] > 0) $sql .= ", product_options.vendor_price = IF(products.vendor_price <> sales.old_cost,sales.old_cost,'0.00')"; 
			$sql .=	" WHERE sales.id = $id AND product_options.id = sales.product_option_id
				AND sales.product_id = product_options.product_id AND products.id = product_options.product_id";	
					
		}
		else 
		{
			$sql = "UPDATE products, sales SET ";
			if ($sale_info['essensa_only'] == 'Y') 
			{
				$sql .= " products.essensa_price = sales.old_essensa_price";
			}
			else $sql .= " products.price = sales.old_price";
			
			if ($sale_info['sale_cost'] > 0) $sql .= " , products.vendor_price = sales.old_cost ";
			$sql .= ", products.inventory_limit = sales.old_inventory_limit, sales.active = 'N' 
				WHERE sales.id = $id AND products.id = sales.product_id";

		}
		//echo $sql;
		db_query($sql);
		
		// also remove it from the specials category, id 15
		// in case is sale of 2 product options from one product, just remove everything from 
		// Specials category and re-populate with current sales
		$sql2 = "DELETE from product_cats WHERE	cat_id = 15";
		db_query($sql2);
		$sql3 = "REPLACE INTO product_cats (product_id, cat_id) select sales.product_id, '15' FROM 
				sales WHERE active = 'Y'";
		db_query($sql3);
	}
	static function deactivate_already_sold_out_sale($id)
	{
		$sql = "UPDATE sales SET sales.active = 'N' 
				WHERE sales.id = $id";
		db_query($sql);	
	}
		
	static function activate_sale($id)
	{
		$sale_info=self::get1($id);
		if ($sale_info['product_option_id'] != 0)
		{
			$sql = "UPDATE products, product_options, sales 
				SET sales.old_price = if(product_options.additional_price > 0, product_options.additional_price, products.price), ";
			
			if ($sale_info['essensa_only'] == 'Y') 
			{
				$sql .= " sales.old_essensa_price = product_options.essensa_price, 
					product_options.essensa_price = sales.sale_price,";
			}
			else $sql .= " product_options.additional_price = sales.sale_price,";
			
			if ($sale_info['sale_cost'] > 0) $sql .= " sales.old_cost = product_options.vendor_price, product_options.vendor_price = sales.sale_cost, ";	 
			$sql .=	" sales.active = 'Y' 
				WHERE sales.id = $id AND products.id = sales.product_id
				AND product_options.id = sales.product_option_id AND product_options.product_id = sales.product_id";
		}
		else 
		{
			$sql = "UPDATE products, sales SET sales.old_price = products.price, ";
			
			if ($sale_info['essensa_only'] == 'Y') 
			{
				$sql .= " sales.old_essensa_price = products.essensa_price, 
					products.essensa_price = sales.sale_price,";
			}
			else $sql .= " products.price = sales.sale_price,";			
			 
			$sql .=	" sales.old_inventory_limit = products.inventory_limit, products.inventory_limit = sales.limit_sale_by_inventory, ";
				if ($sale_info['sale_cost'] > 0) $sql .= " sales.old_cost = products.vendor_price, products.vendor_price = sales.sale_cost, ";				 
			$sql .= "sales.active = 'Y' 
				WHERE sales.id = $id AND products.id = sales.product_id";
		}
		db_query($sql);		
		
		// also put it in the specials category, id 15
		$sql2 = "REPLACE INTO product_cats (product_id, cat_id) select sales.product_id, '15' FROM 
				sales WHERE id = $id";
		db_query($sql2);
	}
	
	//static function update_active_sale($id, $info){
	//	$sql = "UPDATE products, sales SET products.price = sales.sale_price, 
	//			products.inventory_limit = 'Y', sales.active = 'Y' 
	//			WHERE sales.id = $id AND products.id = sales.product_id";
	//	db_query($sql);	
	//}
	
	static function mark_sale_as_sold_out($id) // this function won't be called for product_options
	{
		$sale_info=self::get1($id);
		
		$sql = "UPDATE products, sales SET products.price = sales.old_price, ";
		if ($sale_info['sale_cost'] > 0) $sql .= "products.vendor_price = sales.old_cost, "; 
		$sql .=	"products.inventory_limit = sales.old_inventory_limit,
				sales.sold_out = 'Y' 
				WHERE sales.id = $id AND products.id = sales.product_id";
		db_query($sql);				
	}
	static function mark_sale_as_no_longer_sold_out($id) // this function won't be called for product_options
	{
		$sale_info=self::get1($id);
		
		$sql = "UPDATE products, sales SET products.price = sales.sale_price, ";
		if ($sale_info['sale_cost'] > 0) $sql .= "products.vendor_price = sales.sale_cost, "; 
		$sql .= "products.inventory_limit = 'Y',
				sales.sold_out = 'N' 
				WHERE sales.id = $id AND products.id = sales.product_id";
		db_query($sql);				
	}
	static function get_min_time_of_sale_expiration()
	{
		$sql = "SELECT min(end_time) as min_exp";	
		$sql .= " FROM sales ";
		$sql .= " WHERE 1 ";
		$sql .= " AND NOW() BETWEEN sales.start_time and sales.end_time AND sales.active = 'Y'";
		
	    $ret = db_query_array($sql);

		return $ret[0]['min_exp'];
	}
	static function see_if_all_options_on_sale($product_id)
	{
		$options = Products::getProductOptions(0, $product_id);

		$min_sale_price = $max_sale_price  = $min_old_price = $max_old_price = 0;
		
		foreach ($options as $cur_option)
		{
			if(!self::seeIfCurrentSaleForProd($product_id, $cur_option['id'], true)) return false;
			$cur_option_sale = self::get(0, true, $product_id, 'N', $cur_option['id']);
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && 
				$cur_option['essensa_price'] != '0.00' && ($cur_option['essensa_price'] < $cur_option['additional_price'] || $cur_option['additional_price'] == '0.00') )
				{
					if ($cur_option['essensa_price'] > $max_sale_price || $max_sale_price == 0) $max_sale_price = $cur_option['essensa_price'];
					if ($cur_option['essensa_price'] < $min_sale_price || $min_sale_price == 0) $min_sale_price = $cur_option['essensa_price'];
				}
				else 
				{
					if ($cur_option['additional_price'] > $max_sale_price || $max_sale_price == 0) $max_sale_price = $cur_option['additional_price'];
					if ($cur_option['additional_price'] < $min_sale_price || $min_sale_price == 0) $min_sale_price = $cur_option['additional_price'];		
				}
			
			if ($cur_option_sale[0]['old_price'] > $max_old_price || $max_old_price == 0) $max_old_price = $cur_option_sale[0]['old_price'];			
			if ($cur_option_sale[0]['old_price'] < $min_old_price || $min_old_price == 0) $min_old_price = $cur_option_sale[0]['old_price']; 						
		}
		$return_val = array("min_sale_price"=>$min_sale_price, "max_sale_price"=>$max_sale_price,
							"min_old_price"=>$min_old_price, "max_old_price"=>$max_old_price);
		return $return_val;
	}
}
?>