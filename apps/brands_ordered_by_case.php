<? class BrandsOrderedByCase
{
	static function getArrayOfBrandsOrderedByCase()
	{
		$brands_by_case_array = array();
		$sql = "SELECT * FROM brands_ordered_by_case";
		$result = db_query_array($sql);
		if ($result){
			
			foreach ($result as $one_brand)
			{
				$brands_by_case_array[] = $one_brand['brand_id'];
			}
		}
		return $brands_by_case_array;
	}
}
?>