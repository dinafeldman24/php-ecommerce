<?php 
class AmazonOrderLines
{
	static function insertAmazonOrderLineAdjustment($amz_item_info_array, $charge_id)	
	{
		foreach($amz_item_info_array as $amz_item_info)
		{
			$sql = "INSERT INTO amazon_order_line_adjustments (order_items_id, `item-price`, `item-tax`, `shipping-price`, `shipping-tax`,
						`gift-wrap-price`, `gift-wrap-tax`, `item-promotion-discount`, `ship-promotion-discount`, charge_id) 
						VALUES ('".	$amz_item_info['order_items_id'] . "','" . $amz_item_info['item-price-adj'] . "','" . 
						$amz_item_info['item-tax-adj'] ."','".
						$amz_item_info['shipping-price-adj'] . "','".$amz_item_info['shipping-tax-adj']."','".
						$amz_item_info['gift-wrap-price-adj']."','".$amz_item_info['gift-wrap-tax-adj'] . "','".						
						$amz_item_info['item-promotion-adj']."','".$amz_item_info['ship-promotion-adj'] . "', $charge_id)";
			db_query($sql);
		}					
	}
	static function insertAmazonOrderLineAmounts($item, $order_items_id)
	{
		$sql = "INSERT INTO amazon_order_line_amounts (order_items_id, `item-price`, `item-tax`, `shipping-price`, `shipping-tax`, `gift-wrap-price`,
							`gift-wrap-tax`, `item-promotion-discount`, `ship-promotion-discount`) VALUES
							(".$order_items_id.",'".$item['line_total']."','".$item['tax']."','".$item['shipping-price']."','".
							$item['shipping-tax']."','".$item['gift-wrap-price']."','".$item['gift-wrap-tax']."','".$item['item-promotion-discount']."','".
							$item['ship-promotion-discount']."')"; 
		db_query($sql);
	}
	static function getMaxAllowedRefund($order_items_id)
	{
		$get_original_amounts = db_query_array("SELECT * FROM amazon_order_line_amounts WHERE order_items_id = $order_items_id");
		$original_amounts_result = $get_original_amounts[0]; 
		$get_adjustment_sums_sql = "SELECT IFNULL(SUM(`item-price`),0) AS item_price_adjustment_sum,
											IFNULL(SUM(`item-tax`),0) AS item_tax_adjustment_sum,
											IFNULL(SUM(`shipping-price`),0) AS shipping_price_adjustment_sum,
											IFNULL(SUM(`shipping-tax`),0) AS shipping_tax_adjustment_sum,
											IFNULL(SUM(`gift-wrap-price`),0) AS gift_wrap_price_adjustment_sum,
											IFNULL(SUM(`gift-wrap-tax`),0) AS gift_wrap_tax_adjustment_sum,
											IFNULL(SUM(`item-promotion-discount`),0) AS item_promotion_adjustment_sum,
											IFNULL(SUM(`ship-promotion-discount`),0) AS ship_promotion_adjustment_sum
									FROM amazon_order_line_adjustments 
									WHERE amazon_order_line_adjustments.order_items_id = $order_items_id";
				
		$get_adjustment_sums = db_query_array($get_adjustment_sums_sql);
		$adjustment_sums_result = $get_adjustment_sums[0];
		
		$max_allowed_array = array();
		$max_allowed_array['item-price'] = $original_amounts_result['item-price'] - $adjustment_sums_result['item_price_adjustment_sum'];
		$max_allowed_array['item-tax'] = $original_amounts_result['item-tax'] - $adjustment_sums_result['item_tax_adjustment_sum'];
		$max_allowed_array['shipping-price'] = $original_amounts_result['shipping-price'] - $adjustment_sums_result['shipping_price_adjustment_sum'];
		$max_allowed_array['shipping-tax'] = $original_amounts_result['shipping-tax'] - $adjustment_sums_result['shipping_tax_adjustment_sum'];
		$max_allowed_array['gift-wrap-price'] = $original_amounts_result['gift-wrap-price'] - $adjustment_sums_result['gift_wrap_price_adjustment_sum'];
		$max_allowed_array['gift-wrap-tax'] = $original_amounts_result['gift-wrap-tax'] - $adjustment_sums_result['gift_wrap_tax_adjustment_sum'];
		$max_allowed_array['item-promotion-discount'] = $original_amounts_result['item-promotion-discount'] - $adjustment_sums_result['item_promotion_adjustment_sum'];
		$max_allowed_array['ship-promotion-discount'] = $original_amounts_result['ship-promotion-discount'] - $adjustment_sums_result['ship_promotion_adjustment_sum'];
		
		return $max_allowed_array;		
	}
	
	static function getColumnsToDisplayForOrder($order_id)
	{
		$order_items = Orders::getItems(0, $order_id);
		if (!$order_items) return;
		$cols_to_display = array(
			"item-price" => false,
			"item-tax" => false,
			"shipping-price" => false,
			"shipping-tax" => false,
			"gift-wrap-price" => false,
			"gift-wrap-tax" => false,
			"item-promotion-discount" => false,
			"ship-promotion-discount" => false 
		);
		foreach($order_items as $order_item)
		{
			if ($order_item['item-price'] > 0) $cols_to_display["item-price"] = true;
			if ($order_item['item-tax'] > 0) $cols_to_display["item-tax"] = true;
			if ($order_item['shipping-price'] > 0) $cols_to_display["shipping-price"] = true;
			if ($order_item['shipping-tax'] > 0) $cols_to_display["shipping-tax"] = true;
			if ($order_item['gift-wrap-price'] > 0) $cols_to_display["gift-wrap-price"] = true;
			if ($order_item['gift-wrap-tax'] > 0) $cols_to_display["gift-wrap-tax"] = true;
			if ($order_item['item-promotion-discount'] > 0) $cols_to_display["item-promotion-discount"] = true;
			if ($order_item['ship-promotion-discount'] > 0) $cols_to_display["ship-promotion-discount"] = true;
		}
		return $cols_to_display;
	}
}
?>