<?
/**
 * Image class for YContent system (database table "image").
 *
 */
class ImageDB{

	static function insert($info){
		return db_insert("image",$info,'date_added');
	}

	static function update($id,$info){
		return db_update("image",$id,$info);
	}

	static function delete($id="",$filekey=""){
		if($filekey){
			$o->file_key = $filekey;
			$img = self::getByO($o);
			$img = $img[0];
			if($img){
				$id = $img['id'];
				@unlink($CFG->image_upload_dir . "/" . $img['file_key'] . $CFG->full_suffix);
				@unlink($CFG->image_upload_dir . "/" . $img['file_key'] . $CFG->preview_suffix);
				@unlink($CFG->image_upload_dir . "/" . $img['file_key'] . $CFG->thumbnail_suffix);
			}
		}
		return db_delete("image",(int)$id);
	}

	static function getByO($o=""){
		global $CFG;

		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT image.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(image.id)) as total ";
		}

		$from_ = " FROM image ";

		$join_ .= " ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND image.id = [id] ";
		}
		if(isset($o->file_key)){
			$where_ .= " AND image.file_key = [file_key] ";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY image.id ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	static function getFilekey($id){
		$rec = self::get1($id);
		return (string) $rec['file_key'];
	}

	static function getID($file_key){
		if(!$file_key){
			return false;
		}

		$o->file_key = $file_key;
		$result = db_get1( ImageDB::getByO($o) );

		return $result['id'];
	}

	static function getTinyImage($filekey,$placeholder=false){
		global $CFG;

		if(is_numeric($filekey)){
			$filekey = self::getFilekey($filekey);
		}

		if($placeholder && !$filekey){
			return $CFG->placeholder_img;
		}
		if($filekey == ""){
			return "";
		}

		return $CFG->image_upload_url . $filekey . $CFG->thumbnail_suffix;
	}

	/* Avatar */
	static function getMediumImage($filekey,$placeholder=false){
		global $CFG;

		if(is_numeric($filekey)){
			$filekey = self::getFilekey($filekey);
		}

		if($placeholder && !$filekey){
			return $CFG->placeholder_img;
		}
		if($filekey == ""){
			return "";
		}

		return $CFG->image_upload_url . $filekey . $CFG->preview_suffix;
	}

	static function getFullImage($filekey){
		global $CFG;

		if(is_numeric($filekey)){
			$filekey = self::getFilekey($filekey);
		}

		if($filekey == ""){
			return "";
		}
		return $CFG->image_upload_url . $filekey . $CFG->full_suffix;
	}




}