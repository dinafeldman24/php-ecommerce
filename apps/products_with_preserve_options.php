<?php

class Products {

    static $current_group;

	static function insert($info,$update_url=true,$has_vendor_sku=false)
	{
		//New change, SKUs can now have Duplicates -- 1/26/2011 - Jimmy

//	    //Check for anything with the same SKU, if it exists fail!
// 
//		$check = Products::get1ByModel( $info['vendor_sku'], false );
//		//print_ar( $check ); 
//		if( $check )
//		    return false;

		if ((!$info['brand_id'] || !$info['mfr_part_num'])&& !$has_vendor_sku) 
		{
			echo "Cannot insert product because brand or mfr part number is not set";
			return false;
		}		
				
		if( $info['mfr_part_num'] )
		    $info['mfr_part_num'] = trim( $info['mfr_part_num'] );
		
		if (!$has_vendor_sku){
		   $brand_id_prefix = str_pad($info['brand_id'], 4, "0", STR_PAD_LEFT);
		   $info['vendor_sku'] = $brand_id_prefix . "-" . $info['mfr_part_num'];
		}

		$product_id = db_insert('products',$info,'date_added');

		$product = self::get1( $product_id ); 

		if($update_url && !$product['url_name'])
			self::setURL( $product, false);
		
		Tracking::trackInsert( 'product', $product_id );
		
		return $product_id;
	}


	static function update($id,$info, $update_url=false, $make_new_url_from_name=false)
	{				
		$update_product = true;
		
		$prev_product_info = Products::get1($id);

		// if setting mfr part number or brand id changed...
	    if (($info['mfr_part_num'] && $prev_product_info['mfr_part_num'] != $info['mfr_part_num'])|| ($info['brand_id'] && $prev_product_info['brand_id'] != $info['brand_id']))
	    {
			// if trying to change mfr part number and no brand set, give error
	    	if((!$info['brand_id'] || $info['brand_id'] == 0) && !$prev_product_info['brand_id']) 	    
			{
				echo "Cannot update product because no brand is set";
				return false;
			}		
				
	    	if ($info['mfr_part_num']) $part_num_to_use = trim( $info['mfr_part_num'] );
	    	else $part_num_to_use = $prev_product_info['mfr_part_num'];
			if ($info['brand_id']) $brand_id_to_use = $info['brand_id'];
			else $brand_id_to_use = $prev_product_info['brand_id'];
			$brand_id_prefix = str_pad($brand_id_to_use, 4, "0", STR_PAD_LEFT);
			$info['vendor_sku'] = $brand_id_prefix . "-" . $part_num_to_use;
	    }


	   //No longer a vendor_sku check... can be duplicates
//	    $check = Products::get1ByModel( $info['vendor_sku'], false );
//	    if( $check && $check['id'] != $id && $check['is_active'] == 'Y' && $check['is_deleted'] == 'N' )
//	    {
//		return false;
//	    }
	    
	    $tracking_id = Tracking::trackUpdate( 0, 'product', $id, 'update' );

		if($info['weight']){
		//	$info['weight'] = preg_replace("/[^0-9.]/",'',$info['weight']);
		}
		
		if(!$update_url){
			$info['url_name'] = $prev_product_info['url_name'];
		}
		
		if($make_new_url_from_name){
			$info['url_name'] = self::makeURL($info['name']);
			$update_url = false;
		}
		
		if ($info['url_name'] && $update_url)
		{
			$info['url_name'] = trim($info['url_name']);

	    	$info['url_name'] = str_replace( "&", "and", $info['url_name'] );
	    	$info['url_name'] = str_replace( "/", "-", $info['url_name'] );

	    	$info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
	    	$info['url_name'] = str_replace(' ','-',$info['url_name']);

	    	$info['url_name'] = rtrim( $info['url_name'], '-' );
	    	$info['url_name'] = strtolower( $info['url_name'] );

	    	//Now need to check for that URL in products and Cats and brands to make sure it isn't there already
	        $p_url = Products::get1ByURLName( $info['url_name'] );
			if ($p_url && $p_url['id'] != $id) 
			{
				echo "<div class='errorBox'>Product " . $p_url['vendor_sku'] . " already exists with URL ".$info['url_name']."!</div>";
				$update_product = false;
			}

	    	$c_url = Cats::get1ByURLName( $info['url_name'] );
	    	if ($c_url && $c_url['id'] != $id) 
	    	{
	    		echo "<div class='errorBox'>Category " . $c_url['name'] . " already exists with that URL!</div>";
	    		$update_product = false;	    
	    	}
			$b_url = Brands::get1ByURLName( $info['url_name'] );
	    	if ($b_url && $b_url['id'] != $id) 
	    	{
	    		echo "<div class='errorBox'>Brand " . $b_url['name'] . " already exists with that URL!</div>";
	    		$update_product = false;	    
	    	}
			// see if url changed.  If it did, put it in the url_redirects table
			$old_prod_info = Products::get1($id);
			if ($old_prod_info['url_name'] && $old_prod_info['url_name'] != $info['url_name']) 
			{
				$redirect_info['from_url'] = "/".$old_prod_info['url_name'].".html";
				$redirect_info['to_url'] = "/".$info['url_name'].".html"; 
				UrlRedirects::insert($redirect_info);
			}
	    
		}
		
		if($update_product)
			$result = db_update('products',$id,$info, 'id', 'date_updated' );
		
		/* if ($update_url)
		{
		  $product = self::get1( $id );
		  self::setURL($product, true);
		}*/
		// if brand id changed, also change option skus
	    if($result)
	    if ($info['brand_id'] && $prev_product_info['brand_id'] != $info['brand_id'])
	    {
	    	$prod_options = Products::getOptionsForProduct($prev_product_info['id']);
	    	if ($prod_options)
	    	{ 
	    		foreach ($prod_options as $one_option)
	    		{
	    		    $info2 = array();
	    		    $info2['mfr_part_num'] = $one_option['mfr_part_num'];
	    			Products::updateProductOption($one_option['id'], $info2);			
	    		}
	    	}
	    }

	    if($result)
	    $tracking = Tracking::trackUpdate( $tracking_id );
		
		return $result;
	}

	static function delete($id)
	{
		if( !$id )
			return false;
		$tracking_id = Tracking::trackUpdate( 0, 'product', $id, 'update' );
		$info['is_deleted'] = 'Y';
		$info['is_active'] = 'N';

		$options = Products::getProductOptions( 0, $id );
		if( $options )
			foreach( $options as $option )
			{
				Products::deleteProductOption( $option['id'] );
			}

		$result = Products::update( $id, $info, false );
		
		$tracking = Tracking::trackUpdate( $tracking_id );
		
		return $result;
	}

	static function search($keywords, $active = true, $brand_id=0)
	{
		$keywords = trim( $keywords );
		$search_string = '%'.str_replace(' ','%',  addslashes($keywords) ).'%';
		
		$brand_id = (int) $brand_id;
        $q_sku = '';
//print_ar( $brand_id );
	    if ($keywords && $keywords != '') {
            $q_sku = "SELECT products.*,
                                brands.name AS brand_name,
                                cats.id AS cat_id, cats.name AS cat_name
                         FROM products
                         LEFT JOIN brands ON brands.id = products.brand_id
                         LEFT JOIN product_cats ON product_cats.product_id = products.id
                         LEFT JOIN cats ON cats.id = product_cats.cat_id
                         LEFT JOIN product_options ON product_options.product_id = products.id
                         WHERE products.is_active = 'Y'
                         AND products.website = 'Y'
			 AND products.is_accessory = 'N'
                         AND ";
            $fields = Array('products.vendor_sku', 'product_options.vendor_sku', 'products.aka_sku', 'products.id');

            $q_sku_keywords = db_split_keywords($keywords, $fields, 'OR', true, true);

            $q_sku .= "(" . $q_sku_keywords . ")";

	    if( $brand_id )
		$q_sku .= " AND products.brand_id = $brand_id ";

       		$q_sku .= " GROUP BY products.id ORDER BY products.name";

		//echo $q_sku;
        }

		$q = " SELECT products.*,
						brands.name AS brand_name,
						cats.id AS cat_id,cats.name AS cat_name
					 FROM products
					 LEFT JOIN brands ON brands.id = products.brand_id
					 LEFT JOIN product_cats ON product_cats.product_id = products.id
					 LEFT JOIN cats ON cats.id = product_cats.cat_id
					 LEFT JOIN product_options ON product_options.product_id = products.id
					 WHERE products.is_active = 'Y'
					 AND products.website = 'Y'
					 AND products.is_accessory = 'N'
					  ";
		
		
		if( $brand_id )
			$q .= " AND products.brand_id = $brand_id ";
		
		$q .= " AND
					 		(	";
					 		
	    if ($keywords && $keywords != '') {


            $fields = Array('products.name',
			    //'products.description',
			    'products.vendor_sku',
			    'products.id',
			    'product_options.vendor_sku',
			    'products.tagline',
			    'cats.name',
			    'cats.cat_keywords',
			    'products.aka_sku'  );

            $q .= db_split_keywords($keywords,$fields,'AND',true);

	    //Adding a hardcoded exception to this rule
//	    if( strtolower($keywords) != 'swarovski' ) {
//
//		if(!(strpos($keywords,' ') > 0) && strlen($keywords) > 5){
//		    $i = 0;
////			while(strlen($keywords)-$i > 4){
////			    $i++;
////				$q .= " OR ".db_split_keywords(substr($keywords,0,($i*-1)),$fields,'OR',true)." \n ";
////			}
//		    $q .= " OR ".db_split_keywords(substr($keywords,0,(3)),$fields,'OR',true)." \n ";
//		}
//	    }
        }
        else{
        	$q .= "1";
        }
        
        /* $q .= " 
   								OR
   									products.vendor_sku LIKE ".db_escape($search_string)."
   								OR
   									products.name LIKE ".db_escape($search_string)."
   								OR
   									products.tagline LIKE ".db_escape($search_string)."
   								OR 
   									product_options.vendor_sku LIKE ".db_escape($search_string). "*/
   									
   		$q .= "				) GROUP BY products.id ORDER BY products.name";	
		
        //echo "<pre>$q</pre>";
        //print_ar($q);
        

        if ($q_sku) {
            $sql = '(' . $q_sku . ') UNION DISTINCT (' . $q . ')';
            //print_ar($sql);
            return db_query_array($sql);
        }
        else {
            return db_query_array($q);
        }
	}


	static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='',$cat_id=0,$active='',$homepage='',$featured='', $occ_id=0, $brand_id=0,
				 $start_price = 0, $end_price = 0, $vendor_id = 0, $width = 0, $color = 0, $height=0, $length=0, $group_by='',
				 $colors=false, $include_subs=false, $filter_query='', $total=false, $sku ="", $website='', $filters='', $limit=0, $start=0, $unbuffered=false,
				 $is_deleted='N',$is_available='', $backend=true, $keyword_search=array(), $upc='',$is_accessory='', $return_cats=false, $cats_is_hidden="N", 
				 $limited = false,$mfr_part_num = '',$url_name='',$vendor_sku_name = ''  )
	{
		global $CFG;	
	
		
		if( $total )
		{
			$sql .= " SELECT count( distinct products.id) as total ";
			if( $cat_id )
			{
				if( is_array( $cat_id ) )
				{
					$cats = $cat_id;
				}
				else
				{
					if( $include_subs )
					{
						$cats = array();
						$cats[] = $cat_id;
						$cats = Cats::getAllSubs( $cat_id, $cats, $backend );
						$sym_cats  = Cats::getSymbolicSubs($cat_id);
						if($sym_cats)
						{
							foreach($sym_cats as $sym_cat_key=>$sym_cat_value)
							{
								$cats[$sym_cat_key] = $sym_cat_key;
							}				
						}
						//print_ar( $cats );
					}
					else
						$cats = $cat_id;
				}

					//Should I include all the sub cats on the filters too?

					if( !is_array( $filters ) )
						$filters = Filters::getFiltersForCat( $cats );
					//print_ar( $filters );
					$num_filters = count( $filters );
			}
		}
		else
		{

		    if( $return_cats )
		    {
			$sql = " SELECT cats.name, count( distinct products.id ) as total, cats.id, cats.pid, cats.url_name ";
		    }
		    else {
		    	//if ($limited) $sql .= "SELECT products.id, products.name, products.vendor_sku,";
		    	if ($limited) $sql .= "SELECT products.id, products.name, products.vendor_sku,
		    		products.brand_id, products.price, products.retail_price, 
		    		products.vendor_price, products.map_price, products.description, products.is_active,
		    		products.promo_message, products.is_deleted, products.upc, products.ships_in, products.priced_per,
		    		products.per_price, products.unit_case, products.url_name, products.promo_msg, products.inventory_limit,
		    		products.is_accessory, products.meta_desc, products.exclude_min, products.exclude_discount,
		    		products.title_tag, products.case_discount, products.num_pcs_per_case,
		    		products.img_thumb_url, products.img_lg_url, products.img_xlg_url, products.seo_text,
		    		products.energy_star, products.mfr_part_num, products.free_shipping, products.essensa_price, ";
		    	else $sql = "SELECT products.*,";
				$sql .=" 
						IF (brands.min_charge > 5 AND exclude_min = 'N', CEIL(brands.min_amount/products.vendor_price),NULL) as min_qty,
						(products.price * (1 - products.case_price_percent)) AS case_price_per_piece, 
						  brands.name AS brand_name, brands.recommended_vendor as is_recommended,
					    cats.id AS cat_id,cats.name AS cat_name,
					    product_options.id AS product_option_id, 
					    product_options.vendor_sku AS product_option_vendor_sku,
					    product_options.mfr_part_num AS product_option_mfr_part_num,
					    product_options.is_active AS product_option_is_active,best_selling_products.num_orders ";
		    }

	/*		if($keywords){
				print_ar($keywords);

			}
				 /*   $fields = (count($keyword_search) > 0) ? $keyword_search : array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		//		    //$fields = $keyword_search;
		//
		//            //$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		//		    $sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);

				    //Need to use the keyword matching and expand the kewywords to include what I need

				    $keywords = KeywordMatch::buildKeywords( $keywords );


				    $fields = implode( ",", $fields );
				    $sql .= ", MATCH( products.name, products.description ) AGAINST ('" . addslashes($keywords) . "') as relevance ";

				    $search_skus = true;
				} */

//			        		occasions.name as occ_name,
//			       			 occasions.id as occ_id,
//							occasions.id as occasion_id ";
			if( $color || $colors )
			{
				$sql .= " , product_options.value ";
			}
			

			//When I have a specific cat_id I need to get all of the cat specific filters for that cat, and they need to be
			//sortable, and filterable
			if( $cat_id )
			{
				if( is_array( $cat_id ) )
				{
					$cats = $cat_id;
				}
				else
				{
					$cat = Cats::get1( $cat_id );
					if( $include_subs && $cat['pid'] )
					{
						$cats = array();
						$cats[] = $cat_id;
						//echo'ran this here <br />';
						$cats = Cats::getAllSubs( $cat_id, $cats, $backend );
						$sym_cats  = Cats::getSymbolicSubs($cat_id);
						if($sym_cats)
						{
							foreach($sym_cats as $sym_cat_key=>$sym_cat_value)
							{
								$cats[$sym_cat_key] = $sym_cat_key;
							}				
						}
						//print_ar( $cats );
					}
					else
						$cats = array($cat_id);

				}

					//Should I include all the sub cats on the filters too?
					if( !is_array( $filters ) )
						$filters = Filters::getFiltersForCat( $cats );
					//print_ar( $filters );
					//print_ar( $filters );
					$num_filters = count( $filters );
			}
		}

		/*
		if (trim($keywords) != ''){
			$sql .= " FROM product_summary_data 
					LEFT JOIN products ON products.id = product_summary_data.product_id "; //for fulltext query
		}else{
			$sql .= "FROM products ";
		}
		*/
		$sql .= "
				FROM products 
				LEFT JOIN brands ON brands.id = products.brand_id
				LEFT JOIN product_cats ON product_cats.product_id = products.id 
				LEFT JOIN best_selling_products ON best_selling_products.product_id = products.id";


		$sql .= "	LEFT JOIN cats ON cats.id = product_cats.cat_id ";
		
//				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
//				LEFT JOIN occasions ON occasions.id = product_occasions.occasion_id ";
 
		if( $num_filters )
		{
			$i =1;
			foreach( $filters as $filter )
			{
				if( $filter['display'] != 'N' )
					$sql .= " LEFT JOIN product_filters as filter$filter[filter_id] ON filter$filter[filter_id].product_id = products.id AND filter$filter[filter_id].filter_id = $filter[filter_id] ";
				++$i;
			}
		}
		
		$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id ";

		
		$sql .= " WHERE 1 ";

		if( is_array( $id ) )
		{
			$id_string = mysql_real_escape_string( implode(",", $id) );
			$sql .= " AND products.id IN ($id_string) ";
		}
		elseif ($id > 0) {
			$sql .= " AND products.id = $id ";
		}

		if ($keywords){
		    
			//$sql .= "AND products.name LIKE '%" . addslashes($keywords) . "%' ";
		
		}
		if( is_array( $brand_id ) )
		{
			$brand_string = mysql_real_escape_string("'" . implode("','", $brand_id) . "'");
			$sql .= " AND products.brand_id IN ($brand_string) ";
		}
		elseif ($brand_id > 0) {			
			$sql .= " AND products.brand_id = ".(int)$brand_id;
		}
		if ($active !== '') {
			$sql .= " AND products.is_active = '".($active ? 'Y' : 'N')."' ";
			$sql .= " AND (product_options.is_active = '".($active ? 'Y' : 'N')."' OR product_options.is_active IS NULL) ";
		}
		if ($homepage !== '') {
			$sql .= " AND products.show_on_homepage = '".($homepage ? 'Y' : 'N')."' ";
		}
		if ($featured !== '') {
			$sql .= " AND products.is_featured = '".($featured ? 'Y' : 'N')."' ";
		}
		if ($website !== '') {
			$sql .= " AND products.website = '".($website ? 'Y' : 'N')."' ";
		}

		if ($start_price > 0 && $end_price > 0) {
			$start_price = mysql_real_escape_string($start_price);
			$end_price = mysql_real_escape_string($end_price);
			$sql .= " AND products.price BETWEEN $start_price AND $end_price ";
		}
		else if ($start_price > 0) {
			$start_price = mysql_real_escape_string($start_price);
			$sql .= " AND products.price >= $start_price ";
		}
		else if ($end_price > 0) {
			$end_price = mysql_real_escape_string($end_price);			
			$sql .= " AND products.price <= $end_price ";
		}

		if ($begins_with != '') {
			$sql .= " AND products.mfr_part_num LIKE '".mysql_real_escape_string(addslashes($begins_with))."%'";
		}
		
// 		if (trim($keywords) != '') {
// 			$keywords = mysql_real_escape_string($keywords);
// 			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
// 			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
// 		}	
		
		if (trim($keywords) != '') {
			$keywords = mysql_real_escape_string(trim($keywords));
			$keywords_array = preg_split('/\s+/', $keywords);
			//$search_str = '+"' . implode('" +"', $keywords_array) . '"';
			
			$search_str = '+' . implode(' +', $keywords_array) . '*';
			//$search_str .= ' +"' . implode('" +"', $keywords_array) . '"';
			//$sql .= " AND ( MATCH (product_summary_data.product_name,product_summary_data.product_vendor_sku,product_summary_data.product_option_vendor_sku,product_summary_data.product_description,product_summary_data.cat_name,product_summary_data.brand_name,product_summary_data.aka_sku) AGAINST ('$search_str' IN BOOLEAN MODE)) ";
			
			//$sql .= " AND ( MATCH (products.name,products.vendor_sku,products.description,products.aka_sku) AGAINST ('$search_str' IN BOOLEAN MODE)) ";
					
		//	foreach ($keywords_array as $keyword){
			//	$sql .= " OR products.id = '$keyword' ";
			//}
			//$sql .= ")";

			//==== 
			
			$fields = Array('products.name','products.description','products.vendor_sku','products.aka_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
			
			//===


// 			$fields = Array('brands.name','cats.name','product_options.vendor_sku');
// 			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
	
		}
		
		/*if (trim($keywords) != '') {

		    $fields = (count($keyword_search) > 0) ? $keyword_search : array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
//		    //$fields = $keyword_search;
//
//            //$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		    if( $order == 'relevance' )
			$sql .= " AND (" . db_split_keywords($keywords,$fields,'OR',true) . ") ";
		    else
			$sql .= " AND (" . db_split_keywords($keywords,$fields,'AND',false) . ") ";
		}*/
		
		
		if( is_array($cat_id) )
		{
			$cat_string = mysql_real_escape_string("'" . implode("','", $cat_id) . "'");
			$sql .= " AND product_cats.cat_id IN ($cat_string) ";
                        
                                

		}
		elseif ($cat_id > 0) {

			if( $include_subs  )
			{
				if( !$cats )
				{
					//echo'ran this here <br />';
					$cats = array();
					$cats[] = $cat_id;
					$cats = Cats::getAllSubs( $cat_id, $cats, $backend );
					$sym_cats  = Cats::getSymbolicSubs($cat_id);
					if($sym_cats)
					{
						foreach($sym_cats as $sym_cat_key=>$sym_cat_value)
						{
							$cats[$sym_cat_key] = $sym_cat_key;
						}				
					}
				
				}
				//print_ar( $cats );
				$sql .= " AND product_cats.cat_id IN ('" .  implode("','", $cats) . "') ";

			}
			else
			{
				$cat_id = mysql_real_escape_string($cat_id);
				$sql .= " AND product_cats.cat_id = $cat_id ";
                                
                                
			}
		}

		if ($occ_id > 0) {
			$occ_id = mysql_real_escape_string($occ_id);
			$sql .= " AND product_occasions.occasion_id = $occ_id ";
		}

		if ($vendor_id > 0) {
			$vendor_id = mysql_real_escape_string($vendor_id);			
			$sql .= db_restrict('products.vendor_id', $vendor_id);
		}

		if( $width )
		{
			$width = mysql_real_escape_string($width);			
			$sql .= " AND products.width = $width ";
		}
		if( $length )
		{
			$length = mysql_real_escape_string($length);			
			$sql .= " AND products.length = $length ";
		}
		if( $height )
		{
			$height = mysql_real_escape_string($height);			
			$sql .= " AND products.height = $height ";
		}
		if( $color )
		{
			$color = mysql_real_escape_string($color);			
			$sql .= " AND product_options.value like '$color%' ";
		}

		if( $sku )
		{
			$sku = mysql_real_escape_string($sku);			
			$sql .= " AND ( products.vendor_sku = UPPER('$sku') OR products.aka_sku = UPPER('$sku')) ";
		}
		if( $mfr_part_num )
		{
			$mfr_part_num = mysql_real_escape_string($mfr_part_num);			
			$sql .= " AND ( products.mfr_part_num = UPPER('$mfr_part_num')) ";
		}
		if( $url_name )
		{
			$url_name = addslashes($url_name);
			$sql .= " AND ( products.url_name = '$url_name' ) ";
		}
		if( $vendor_sku_name )
		{
			$vendor_sku_name = addslashes($vendor_sku_name);
			$sql .= " AND ( products.vendor_sku_name = '$vendor_sku_name' ) ";
		}
		if( $is_deleted )
		{
			$is_deleted = mysql_real_escape_string($is_deleted);		
			$sql .= " AND products.is_deleted = '$is_deleted' ";
		}
		if( $is_available )
		{
			$is_available = mysql_real_escape_string($is_available);
			$sql .= " AND products.is_available = '".addslashes($is_available)."' ";
		}
		if( $upc )
		{
			$upc = mysql_real_escape_string($upc);			
		    $sql .= " AND products.upc = '" . addslashes( $upc ) . "' ";
		}
		if($is_accessory){
			$is_accessory = mysql_real_escape_string($is_accessory);			
			$sql .= " AND products.is_accessory = '".db_esc($is_accessory)."' ";
		}

                if($cats_is_hidden == 'N')
                {
                    $sql .= " AND cats.is_hidden = 'N'";
                }

		$relavance_search = false;
		if( $filter_query )
		{
			//echo "filter_query is $filter_query";
			//$filter_query = mysql_real_escape_string($filter_query);
			$sql .= $filter_query;
		}
		if( !$total )
		{
		     if( $return_cats )
			{
	//		 if( $order == 'relevance' )
//				$relavance_search = true;

			    $group_by = "cats.id";
			    $order = "cats.name";
			    $order_asc = true;
			}

			if( $group_by )
			{
				$group_by = mysql_real_escape_string($group_by);				
				$sql .= " GROUP BY $group_by ";
			}
			else
			{
				$sql .= " GROUP BY products.id
						   ";
			}

		//	if( ($order == 'relevance' || $relavance_search) && $keywords )
		//	{
		//	    $sql .= " HAVING relevance > 0 ";
	//		}

			$sql .= " ORDER BY ";

			if ($order == '') {
				$sql .= 'products.price,brands.name,products.name';
			}
			else {
				$order = mysql_real_escape_string($order);
				$sql .= addslashes($order);
			}

			if ($order_asc !== '' && !$order_asc) {
				$sql .= ' DESC ';
			}

//echo $sql;
//die(); 
 //mail('rachel@tigerchef.com', 'sql', $sql);

			if( $limit )
			{
				$start = mysql_real_escape_string($start);
				$limit = mysql_real_escape_string($limit);				
				$start = (int)$start;
				
				$limit = (int)$limit;
				$sql .=" limit $start, $limit ";
			}


		}
//		if( $keywords )
//		if( $_REQUEST['debug'] == 3 )
// 		echo $sql . '<br /><br />';
		//print_ar( $cats );

                //print_ar($sql);
//                 echo "\n\n$sql\n\n";
		
		$xStart = microtime(true);
		if( $return_cats )
		    $result = db_query_array($sql,'id');
		else
		    $result = db_query_array($sql);
		$xDur = (microtime(true) - $xStart)*1000;
		
		//echo $sql;
		
		if($_GET['debug'] == 1){
			echo "<!-- product::get $xDur -->";
		}

		return $result;
	}

	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false; 

		$o = (object) array();
		$o->id = $id;

		return self::get1ByO( $o );
	}

	static function get1ByUPC( $upc )
	{
	    if( !$upc )
		return false;

	    $product = db_get1( self::get( 0,'','','','',0,'','','', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', false, false, '', false, "", '', '', 0, 0, false, 'N','', true, array(), $upc, '', false, ''  ));

	    if( !$product )
	    {
		$product = db_get1( self::getProductOptions( 0,0,false,true, '', true, 0, 0, 'Y', 'N', $upc ) );
		if( $product )
		{
		    $product['upc_type'] = 'option';
		}
	    }
	    else {
		$product['upc_type'] = 'product';
	    }

	    if( $product )
	        return $product;

	    return false;
	}

	static function get1ByModel( $model='', $only_active_prods = true )
	{
		//On their import sheet they said that I is replaced with 1, want me to understand that
		if( !$model )
			return false;
		$model = addslashes( $model );

		if($only_active_prods){
			$is_active = 'Y';
		}
		else{
			$is_active = '';
		}
		$result = Products::get( 0,'','','','',0,$is_active,'','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, $model,'','',0,0,false,'N','',true,array(),'','',false, '');

		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(vendor_sku) = UPPER('$model') AND is_active = 'Y' and is_deleted='N'  ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }
				$result[0]['is_option'] = true;
				return $result[0];
			}
		}
		//Replace I with 1 and check again, the reason I did this is due to the off chance that 1 of both exsists, I only
		//want to find 1
		$model = strtoupper( $model );
		$model = str_replace( 'I', '1', $model );

		$result = Products::get( 0,'','','','',0,'Y','','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, $model);
		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(vendor_sku) = UPPER('$model') ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }

				$result[0]['is_option'] = true;
				return $result[0];
			}

			return false;
		}
	}

	static function get1ByMfrPartNum( $model='', $only_active_prods = true )
	{
		//On their import sheet they said that I is replaced with 1, want me to understand that
		if( !$model )
			return false;
		$model = addslashes( $model );

		if($only_active_prods){
			$is_active = 'Y';
		}
		else{
			$is_active = '';
		}
		$result = Products::get( 0,'','','','',0,$is_active,'','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, '','','',0,0,false,'N','',true,array(),'','',false, '', false, $model);

		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(mfr_part_num) = UPPER('$model') AND is_active = 'Y' and is_deleted='N'  ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }
				$result[0]['is_option'] = true;
				return $result[0];
			}
		}
		//Replace I with 1 and check again, the reason I did this is due to the off chance that 1 of both exsists, I only
		//want to find 1
		$model = strtoupper( $model );
		$model = str_replace( 'I', '1', $model );

		$result = Products::get( 0,'','','','',0,$is_active,'','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, '','','',0,0,false,'N','',true,array(),'','',false, '', false, $model);
		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(mfr_part_num) = UPPER('$model') ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }

				$result[0]['is_option'] = true;
				return $result[0];
			}

			return false;
		}
	}
	// TO BE USED FOR ABLE KITCHEN.  need to look at inactive, too
	static function getAllByMfrPartNum( $model='', $only_active_prods = false )
	{
		//On their import sheet they said that I is replaced with 1, want me to understand that
		if( !$model )
			return false;
		$model = addslashes( $model );

		if($only_active_prods){
			$is_active = 'Y';
		}
		else{
			$is_active = '';
		}

		//check product options for the model
		$sql = " SELECT brands.name as brand_name, products.priced_per, product_options.*, product_options.id as product_option_id 
				FROM product_options, products, brands 
				WHERE brands.id = products.brand_id and products.id = product_options.product_id AND 
				UPPER(product_options.mfr_part_num) = UPPER('$model') ";
		if ($is_active == 'Y') $sql .= " AND product_options.is_active = 'Y'";
		$sql .= " AND product_options.is_deleted='N'  ";
		
		$result = db_query_array( $sql );

		if(!$result)
		{
			$result = Products::get( 0,'','','','',0,$is_active,'','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, '','','',0,0,false,'N','',true,array(),'','',false, '', true, $model);
		}

		return $result;
	}
	
	static function getModelList( $only_products = false, $starts_with='' )
	{
		//Get an array of all of the models in 1 query, then I can find the price imports off this
		$sql = " select products.id as product_id, UPPER(products.vendor_sku) as vendor_sku, products.name, 'product' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM products ";
		$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$sql .= " WHERE 1 AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y' ";
		if( $starts_with )
			$sql .= " AND products.vendor_sku like '$starts_with%' ";
		$sql .= " UNION select products.id as product_id, UPPER(products.aka_sku) as vendor_sku, products.name, 'product' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM products ";
		$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$sql .= " where products.aka_sku <> '' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y'";
		if( $starts_with )
			$sql .= " AND products.vendor_sku like '$starts_with%' ";
		if( !$only_products )
		{
			$sql .= " UNION select product_options.id as product_id, UPPER(product_options.vendor_sku) as vendor_sku, product_options.value as name, 'option' as type, brands.name as brand_name, products.name as description, products.id as parent_id from product_options ";
			$sql .= " LEFT JOIN products on products.id = product_options.product_id ";
			$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
			$sql .= " WHERE 1 AND product_options.is_active = 'Y' and product_options.is_deleted = 'N' and product_options.website = 'Y' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y' ";
			if( $starts_with )
				$sql .= " AND products.vendor_sku like '$starts_with%' ";

			$sql .= " UNION select product_options.id as product_id, UPPER(product_options.aka_sku) as vendor_sku, product_options.value as name, 'option' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM product_options ";
			$sql .= " LEFT JOIN products on products.id = product_options.product_id ";
			$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";

			$sql .= " where product_options.aka_sku <> '' AND product_options.is_active = 'Y' and product_options.is_deleted = 'N' and product_options.website = 'Y' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y'";
			if( $starts_with )
				$sql .= " AND product_options.vendor_sku like '$starts_with%' ";
		}

		$sql .= " ORDER BY brand_name, TRIM(vendor_sku) ";

		return db_query_array( $sql, 'vendor_sku');
	}

	static function getProductWidth( $id=0, $cat_id=0, $total=false, $start_price=0, $end_price=0, $color=0, $brand=0 )
	{
		$sql = " SELECT products.width ";
		if( $total )
		{
			$sql .= ", count(products.id) as total ";
		}
		$sql .= " FROM products ";
		$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
							LEFT JOIN cats ON cats.id = product_cats.cat_id ";
		if( $color )
		{
			$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id
					  LEFT JOIN options on options.id = product_options.option_id ";
		}

		$sql .= " WHERE 1 ";

		if( $id )
		{
			$sql .= " AND id = $id ";
		}
		if ($cat_id > 0) {
			$sql .= " AND product_cats.cat_id = $cat_id ";
		}
		if( $start_price )
		{
			$sql .= " AND products.price >= $start_price ";
		}
		if( $end_price )
		{
			$sql .= " AND products.price <= $end_price ";
		}
		if( $color )
		{
			$sql .= " AND product_options.value = '$color' ";
		}
		if( $brand )
		{
			$sql .= " AND products.brand_id = $brand ";
		}
		if( $total )
		{
			$sql .= " GROUP BY products.width ";
		}

		return db_query_array( $sql );
	}

	static function getProductsByColor( $id=0, $cat_id=0, $total=true, $start_price=0, $end_price=0, $width=0, $brand=0 )
	{
		$sql = " SELECT product_options.value ";
		if( $total )
		{
			$sql .= ", count(products.id) as total ";
		}
		$sql .= " FROM products ";
		$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
							LEFT JOIN cats ON cats.id = product_cats.cat_id
							LEFT JOIN product_options ON product_options.product_id = products.id
							LEFT JOIN options on options.id = product_options.option_id ";

		$sql .= " WHERE 1 ";

		if( $id )
		{
			$sql .= " AND id = $id ";
		}
		if ($cat_id > 0) {
			$sql .= " AND product_cats.cat_id = $cat_id ";
		}
		if( $start_price )
		{
			$sql .= " AND products.price >= $start_price ";
		}
		if( $end_price )
		{
			$sql .= " AND products.price <= $end_price ";
		}
		if( $width )
		{
			$sql .= " AND products.width = $width ";
		}
		if( $brand )
		{
			$sql .= " AND products.brand_id = $brand ";
		}

		$sql .= " AND product_options.value IS NOT NULL ";

		if( $total )
		{
			$sql .= " GROUP BY product_options.value ";
		}


		//echo $sql;
		return db_query_array( $sql );
	}

	static function getProductsByRanges( $ranges, $cat_id = 0, $width=0, $color=0, $brand=0 )
	{
		if( !is_array( $ranges ) )
			return false;
		$ret = array();
		foreach( $ranges as $range )
		{
			$sql = " SELECT count(*) as total FROM products ";
			if( $cat_id )
			{
				$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
									LEFT JOIN cats ON cats.id = product_cats.cat_id ";
			}
			if( $color )
			{
				$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id
						  LEFT JOIN options on options.id = product_options.option_id ";
			}
			$sql .= " where price >= '$range[start]' AND price <= '$range[end]' ";
			if( $width )
			{
				$sql .= " AND products.width = $width ";
			}
			if( $cat_id )
				$sql .= " AND product_cats.cat_id = $cat_id ";
			if( $color )
			{
				$sql .= " AND product_options.value = '$color' ";
			}
			if( $brand )
			{
				$sql .= " AND products.brand_id = $brand ";
			}
			$total = db_get1(db_query_array($sql));
			$ret[] = array('start'=>$range['start'], 'end'=>$range['end'], 'total'=>$total['total'] );
		}

		return $ret;
	}

	static function getHomePageProducts()
	{
		return Products::get(0,'','','products.homepage_order','',0,'Y','Y');
	}

	static function getFeaturedProducts($cat_id = 0)
	{
		return Products::get(0,'','','','',$cat_id,'Y','','Y');
	}

	static function getProductsForCat($cat_id,$occ_id=0,$start_price=0,$end_price=0, $width=0, $color=0, $brand=0, $order='products.tagline, products.name', $order_asc='', $backend=true, $total = false, $limit=0, $start=0, $is_accessory = '',$keywords=null, $limited = false, $filter_query='', $cat_filters='', $length = 0, $height=0 )
	{
		return Products::get(0,'',$keywords,$order,$order_asc,$cat_id,'Y','','',$occ_id,$brand,$start_price,$end_price,0,$width,$color,$height,$length,'',false,true, $filter_query, $total, '', '', $cat_filters, $limit, $start, false, 'N', '', $backend,'','',$is_accessory, false, 'N', $limited);
	}
	static function getNumActiveProductsInCat($cat_id, $mfr_id = 0)
	{
		$sql = "SELECT SQL_CACHE count(DISTINCT(products.id)) AS num FROM products, product_cats WHERE products.id = product_cats.product_id 
			AND is_active = 'Y' and is_deleted = 'N' and ";
			
		if(is_array($cat_id)){
			$sql .= " product_cats.cat_id IN ( ". implode(',',$cat_id) ." )";
		}else{
			$sql .= " product_cats.cat_id = $cat_id";
		}
		
		if ($mfr_id > 0) $sql .= " AND products.brand_id = " . $mfr_id;
		//echo $sql;
		$result = db_query_array($sql);
		
		if ($result) return $result[0]['num'];
		else return 0;
	}

	static function getProductsForOcc($occ_id)
	{

		return Products::get(0,'','','products.sort','',0,'Y', '', '', $occ_id);
	}

	static function associate2Cats($product_id,$cat_ids,$delete_old_ones='Y')
	{
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";

		if (trim($cat_ids_str) == '') {
			db_delete('product_cats',$product_id,'product_id');
			return true;
		}

		// delete old ones

                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM product_cats WHERE product_id = $product_id AND cat_id NOT IN ($cat_ids_str)");
                }

		

		// insert new ones

		$info = Array('product_id'=>$product_id);

		foreach ($cat_ids as $cat_id) {

		    if( $cat_id <= 0 )
			continue;

			$info['cat_id'] = $cat_id;
			db_insert('product_cats',$info,'',true);
		}

		return true;
	}

	static function associate2Occasions($product_id,$occasion_ids)
	{
		if (!is_array($occasion_ids)) {
			$occasion_ids = Array($occasion_ids);
		}

		$occasion_ids_str = implode(',',$occasion_ids);

		if (trim($occasion_ids_str) == '') {
			db_delete('product_occasions',$product_id,'product_id');
			return true;
		}

		// delete old ones

		$sql = "DELETE FROM product_occasions WHERE product_id = $product_id AND occasion_id NOT IN ($occasion_ids_str)";

		db_query($sql);

		// insert new ones

		$info = Array('product_id'=>$product_id);

		foreach ($occasion_ids as $occasion_id) {
			$info['occasion_id'] = $occasion_id;
			db_insert('product_occasions',$info,'',true);
		}

		return true;
	}

	static function getProductCats($product_id)
	{
		if( !$product_id )
			return false;
		return db_query_array("SELECT cat_id FROM product_cats WHERE product_id = $product_id",'cat_id');
	}

	static function getProductOccasions($product_id)
	{
		return db_query_array("SELECT product_occasions.*, occasions.name as occasion_name, occasions.name_english AS occasion_name_english FROM product_occasions LEFT JOIN occasions ON (occasions.id = product_occasions.occasion_id) WHERE product_id = $product_id",'occasion_id');
	}

	static function insertLinkType($info)
	{
		return db_insert('link_types',$info);
	}

	static function updateLinkType($id,$info)
	{
		return db_update('link_types',$id,$info);
	}

	static function deleteLinkType($id)
	{
		return db_delete('link_types',$id);
	}

	static function getLinkTypes($id=0)
	{
		$sql = "SELECT link_types.*
				FROM link_types
				WHERE 1 ";

		if ($id > 0)
			$sql .= " AND link_types.id = $id ";

		$sql .= " ORDER BY link_types.name ";

		return db_query_array($sql);
	}

	static function getLinkType($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Products::getLinkTypes($id);
		return $result[0];
	}

	static function insertProductLink($product_id1,$product_id2,$link_type_id,$option_id=0)
	{
		$info = Array(
			'product_id1'  => $product_id1,
			'product_id2'  => $product_id2,
			'link_type_id' => $link_type_id,
			'option_id'    => $option_id
		);

		return db_insert('product_links',$info,'',true,false,false);
	}

	static function updateProductLink($orig_product_id1,$orig_product_id2,$product_id1,$product_id2,$link_type_id,$option_id=0)
	{
		db_query("UPDATE product_links
				  SET product_id1=$product_id1, product_id2=$product_id2, link_type_id=$link_type_id, option_id=$option_id
				  WHERE product_id1=$orig_product_id1 AND product_id2=$orig_product_id2");
		return db_affected_rows();
	}

	static function deleteProductLink($id)
	{
		db_query("DELETE FROM product_links WHERE id=$id ");
		return db_affected_rows();
	}

	static function getLinksForProduct($product_id,$group_by_type=false,$type_id=0, $order_by ='')
	{
		if( !$product_id )
			return false;

		$sql = "SELECT products.*,
					link_types.name AS link_type_name,
					product_links.link_type_id,
					product_links.option_id,
					product_links.id as link_id,
					brands.name AS brand_name,
					cats.id AS cat_id,cats.name AS cat_name
				FROM product_links
				LEFT JOIN products ON products.id = product_links.product_id2
				LEFT JOIN link_types ON link_types.id = product_links.link_type_id
				LEFT JOIN brands ON brands.id = products.brand_id
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				LEFT JOIN cats ON cats.id = product_cats.cat_id ";

		$sql .= " WHERE product_id1 = $product_id AND products.is_active = 'Y' ";

		if( $type_id )
		{
			$sql .= " AND product_links.link_type_id = $type_id ";
		}

		$sql .= "	GROUP BY product_links.id
				ORDER BY
				";

		if( $order_by )
		{
			$sql .= $order_by;
		}
		else
		{
			$sql .= " link_types.name,products.price ";
		}

		$result = db_query_array($sql);

		if (!$group_by_type) {
			return $result;
		}
		else {
			if ($result) {
				foreach ($result as $row) {
					$links[$row['link_type_name']][] = $row;
				}
				return $links;
			}
			else {
				return false;
			}
		}
	}

	static function getProductLink($id)
	{
		$sql = "SELECT *
				FROM product_links
				WHERE id = $id ";

		return db_query_array($sql,'',true);
	}

	static function insertOption($info)
	{
		return db_insert('options',$info);
	}

	static function updateOption($id,$info)
	{
		return db_update('options',$id,$info);
	}

	static function deleteOption($id)
	{
		return db_delete('options',$id);
	}

	static function getOptions($id = 0, $is_free = '', $name = '')
	{
		$sql = "SELECT options.*
				FROM options
				WHERE 1 ";

		if ($id > 0)
			$sql .= " AND options.id = $id ";

		if($is_free != '')
			$sql .= " AND options.is_free = '$is_free' ";

		if ($name != '')
			$sql .= " AND options.name = '$name' ";
			
		$sql .= " ORDER BY options.order_fld,options.name ";

		return db_query_array($sql);
	}

	static function getOption($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		
		$result = Products::getOptions($id);

		return $result[0];
	}

	static function insertProductOption($info, $prices = '')
	{
		$prod_info = Products::get1($info['product_id']);
			
		if (!$prod_info['brand_id'] || !$info['mfr_part_num']) 
		{
			echo "Cannot insert product option because product brand or option mfr part number is not set";
			return false;
		}	
				
		if( $info['mfr_part_num'] )
		    $info['mfr_part_num'] = trim( $info['mfr_part_num'] );
		
		$brand_id_prefix = str_pad($prod_info['brand_id'], 4, "0", STR_PAD_LEFT);
		
		$info['vendor_sku'] = $brand_id_prefix . "-" . $info['mfr_part_num'];		
        if(isset($info['product_option_values_set'])){
            $opts = explode(',', rtrim(ltrim($info['product_option_values_set'], ','),','));
            sort($opts);
            $info['product_option_values_set'] = ',' . implode(',', $opts) . ',';
        }
		$id = db_insert('product_options', $info);

		if(is_array($prices)){
			foreach($prices as $qty => $price){
				$sql = "INSERT INTO product_option_prices SET product_option_id = '$id', quantity = '$qty', price = '$price' ";
				db_query($sql);
			}
		}

		return $id;
	}

	static function insertProductFilter( $info )
	{
		return db_insert( 'product_filters', $info );
	}

	 static function insertProductPDF( $info )
	 {
	 	return db_insert( 'product_pdfs', $info );
	 }

	static function updateProductPDF($id,$info)
	{
		return db_update('product_pdfs',$id,$info);
	}

	static function deleteProductPDF($id)
	{
		//db_delete('product_option_prices',$id,'product_option_id');

		return db_delete('product_pdfs',$id);
	}

    static function generateProductOptionsForAvailableOptions($product_id){
      $finalarray=array();
        $sql="SELECT product_available_options. * , option_values. *, product_available_options.id as val_id, product_available_options.id as avail_id, options.name as option_name
        FROM product_available_options
        LEFT JOIN option_values ON product_available_options.options_values_id = option_values.id
        LEFT JOIN options on options.id=option_values.options_id
        WHERE product_available_options.product_id ={$product_id}
        AND product_available_options.optional = 0";
      $options_for_product=db_query_array($sql);
      if ($options_for_product){
          foreach ($options_for_product as $option){
              if(!array_key_exists($option['option_name'], $finalarray))
               $finalarray[$option['option_name']]=array();
                 $finalarray[$option['option_name']][]=$option['val_id'];
        }  
        $combine=array_cartesian_product($finalarray);
        foreach ($combine as $c){
            sort($c);
          $info=array();
          $info['product_id']=$product_id;
          $product=Products::get1($product_id);
          $info['additional_price']=$product['price'];
          $info['vendor_sku']=$product['vendor_sku'];
          $info['mfr_part_num'] = substr($product['vendor_sku'],
                strpos($product['vendor_sku'], '-') +1);
          $info['product_option_values_set']=',' . implode(",",$c) . ','; 
 //         echo "<br/>THis is going to be the product option:   <br>" . print_r ($info, 1)	;
//         echo "<br>+++++++++++++++++++++++++++++++++++++++<br><br>";
          $res = db_insert('product_options', $info);
          //echo 'the result of insert: ' . $res . '<br/>';
        }
      }
}
    
function array_cartesian_product($arrays)
{
$result = array();
$arrays = array_values($arrays);
}

	static function updateProductOption($id,$info,$prices = '')
	{		
		$prev_option_info = Products::getProductOptions($id, 0, false, true, '', true, 0, 0, '');
		$prod_info = Products::get1($prev_option_info[$id]['product_id']);
		
// 		print_ar($prev_option_info);
// 		var_dump($id);
// 		print_ar($prod_info);
		
		// if trying to change mfr part number and no brand set, give error
	    if ($info['mfr_part_num'])
	    {
	    	if(!$prod_info['brand_id'] || $prod_info['brand_id'] == 0) 	    
			{
				echo "Cannot update option because no product brand is set";
				return false;
			}		
				
	    	$info['mfr_part_num'] = trim( $info['mfr_part_num'] );
		
			$brand_id_prefix = str_pad($prod_info['brand_id'], 4, "0", STR_PAD_LEFT);
			
			$info['vendor_sku'] = $brand_id_prefix . "-" . $info['mfr_part_num'];
	    }
        if(isset($info['product_option_values_set'])){
            $values_set = explode(",",rtrim(ltrim($info['product_option_values_set'],","),","));
            sort($values_set);
            $info['product_option_values_set'] = ',' .implode(',', $values_set) . ',';
        }
		$result = db_update('product_options',$id,$info, 'id', 'date_updated');

		return $result;
	}

	function deleteProductOption($id)
	{
		if( !$id )
			return false;
		$info['is_active'] = "N";
		$info['is_deleted'] = "Y";

		return Products::updateProductOption( $id, $info );
		//db_delete('product_option_prices',$id,'product_option_id');

		//return db_delete('product_options',$id);
	}


	static function getProductOptions($product_option_id=0,$product_id=0,$for_package=false,
					  $with_prices = true, $order='', $associate=true, $start_price=0, $end_price=0,
				          $is_active='Y', $is_deleted='N', $upc='' )
	{
		$sql = "SELECT product_options.*,
					options.name AS option_name,
					options.display_name_override,
					options.is_upgrade,
					options.is_free,
					sub_options.name AS suboption_name,
					products.name AS product_name,
					brands.name as brand_name, products.price as base_prod_price,
					product_options.essensa_price as option_essensa_price,
					products.essensa_price as product_essensa_price,
					products.case_price_percent as product_case_price_percent,
					product_options.mfr_part_num as option_mfr_part_num,
					product_options.id as product_option_id
				FROM product_options
				LEFT JOIN options ON options.id = product_options.option_id
				LEFT JOIN options AS sub_options ON sub_options.id = product_options.suboption_id
				LEFT JOIN products ON products.id = product_options.product_id
				LEFT JOIN brands ON brands.id = products.brand_id
				WHERE 1";

		if ($product_option_id > 0)
			$sql .= " AND product_options.id = $product_option_id ";
		if ($product_id > 0)
			$sql .= " AND product_options.product_id = $product_id ";
		if ($for_package)
			$sql .= " AND product_options.include_in_packages = 'Y' ";

		if ($start_price > 0 && $end_price > 0) {
			$sql .= " AND product_options.additional_price BETWEEN $start_price AND $end_price ";
		}
		else if ($start_price > 0) {
			$sql .= " AND product_options.additional_price >= $start_price ";
		}
		else if ($end_price > 0) {
			$sql .= " AND product_options.additional_price <= $end_price ";
		}

		if( $is_active )
			$sql .= " AND product_options.is_active = '$is_active' ";
		if( $is_deleted )
			$sql .= " AND product_options.is_deleted = '$is_deleted' ";

		if( $upc )
		    $sql .= " AND product_options.upc = '" . addslashes( $upc ) . "' ";

		if( $order )
		{
			$sql .= " ORDER BY $order ";
		}
		else
		{
			$sql .= " ORDER BY product_options.additional_price ASC ";
		}
		if( $associate )
			$options = db_query_array($sql,'id');
		else
			$options = db_query_array($sql );
		return $options;
	}


	static function getProductOptionsStruct($product_id)
	{
		$result = array();
		$options = self::getProductOptions(0, $product_id);

		if(is_array($options))
		foreach($options as $option){
			$result[$option['option_id']][] = $option;
		}

		return $result;
	}

	static function getOptionsForProduct($product_id, $backend=false)
	{
	    if( $backend )
	    {
		$active = '';
		$deleted = '';
	    } else {
		$active = 'Y';
		$deleted = 'N';
	    }
	    
		return Products::getProductOptions(0,$product_id,false,true,'order_fld ASC',true,0,0,$active,$deleted);
	}


	static function getProductOptionPrices($product_option_id)
	{
		$sql = "SELECT * FROM product_option_prices WHERE product_option_id = '$product_option_id' ";
		return db_query_array($sql,'quantity',false,false,'price');
	}


	static function getProductOption($product_option_id, $backend = false)
	{
		$product_option_id = (int) $product_option_id;
		if (!$product_option_id) return false;

		if( $backend )
		{
		    $active = '';
		    $deleted = '';
		} else {
		    $active = 'Y';
		    $deleted = 'N';
		}
		
		$result = Products::getProductOptions($product_option_id,0,false,true,'',true,0,0,$active, $deleted);
		return $result[$product_option_id];
	}

	static function changeDefault($product_option_id,$info)
	{
		$product_id = $info[product_id];
		//$option_id = $info[option_id];
		if(!$product_id) return false;
		$sql = "UPDATE product_options
					SET is_default = 'N'
						WHERE product_id = $product_id
							AND is_default = 'Y'
							AND option_id = '$info[option_id]'
							AND id <> $product_option_id";
		//echo $sql;
		db_query($sql);
	}

	static function getBizrateCategories() {
		$sql = "SELECT id, category
				FROM bizrate_categories
				ORDER BY category ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

	static function getShoppingComCategories() {
		$sql = "SELECT id, name
				FROM shopping_com_categories
				ORDER BY name ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

	static function getPriceGrabberCategories() {
		$sql = "SELECT id, name
				FROM price_grabber_cats
				ORDER BY name ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

   static function getByOccasionIDVendorID($occ_id=0,$vendor_id=0,$product_id=0)
	{
		$sql = "SELECT products.*, product_cats.cat_id
				FROM products
				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
				LEFT JOIN product_cats ON products.id=product_cats.product_id
				WHERE products.id <> $product_id AND products.is_active = 'Y'";
		if (is_array($vendor_id)) {
			$sql .= " AND products.vendor_id IN (". implode(",", $vendor_id) .")";
		}
		if (is_array($occ_id)) {
			$sql .= " AND product_occasions.occasion_id IN (". implode(",", $occ_id) .")";
		}

		$sql .= " GROUP BY products.id ";

		return db_query_array($sql,"id");
	}

	static function getByOccasionID($occ_id=0, $product_id=0)
	{
		$sql = "SELECT products.*, product_occasions.occasion_id AS occ_id
				FROM products
				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
				WHERE products.is_active = 'Y' AND products.id <> $product_id";

		if ($occ_id <> 0) {
			$sql .= " AND product_occasions.occasion_id IN (". implode(",", $occ_id) .")";
		}

		$sql .= " GROUP BY products.id ";

		return db_query_array($sql,"id");
	}

	static function getByCatID($cat_id=0, $product_id=0, $order='', $order_asc=false )
	{
		$sql = "SELECT products.*, (products.price * (1 - products.case_price_percent)) AS case_price_per_piece, product_cats.cat_id
				FROM products
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				WHERE products.is_active = 'Y' AND products.id <> $product_id";

		if (is_array($cat_id)) {
			$sql .= " AND product_cats.cat_id IN (". implode(",", $cat_id) .")";
		} elseif($cat_id) {
			$sql .= " AND product_cats.cat_id = '$cat_id'";
		}

		$sql .= " GROUP BY products.id ";

		$sql .= " ORDER BY ";

		if ($order == '') {
			$sql .= 'products.price,products.name';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}


		return db_query_array($sql,"id");
	}


	static function getPopularProducts($cat_id = 0, $occ_id = 0, $limit = 12)
	{

		$sql = "SELECT products.*, product_cats.cat_id, COUNT(order_items.id) as num_sold
   				FROM products
   				LEFT JOIN order_items ON order_items.product_id = products.id
   				LEFT JOIN orders ON orders.id = order_items.order_id
   				LEFT JOIN product_cats ON products.id=product_cats.product_id
   				LEFT JOIN product_occasions ON products.id=product_occasions.product_id
   				WHERE products.is_active='Y' AND orders.date > DATE_SUB(CURDATE(),INTERVAL 90 DAY)";

		if($cat_id > 0){
			$sql .= " AND product_cats.cat_id ='$cat_id' ";
		}

		if($occ_id > 0){
			$sql .= " AND product_occasions.occasion_id ='$occ_id' ";
		}

   		$sql .= " GROUP BY products.id ORDER BY num_sold DESC, products.name LIMIT $limit";

		$prods = db_query_array($sql);

		return $prods;
	}


	static function getNewestProducts($cat_id = 0, $occ_id = 0, $limit = 12)
	{

		$sql = "SELECT products.*, IF(products.show_updated_new = 'Y',date_updated,date_added) AS date_added, product_cats.cat_id
   				FROM products
   				LEFT JOIN product_cats ON products.id=product_cats.product_id
   				LEFT JOIN product_occasions ON products.id=product_occasions.product_id
   				WHERE products.is_active='Y' ";

		if($cat_id > 0){
			$sql .= " AND product_cats.cat_id ='$cat_id' ";
		}

		if($occ_id > 0){
			$sql .= " AND product_occasions.occasion_id ='$occ_id' ";
		}

   		$sql .= " GROUP BY products.id ORDER BY date_added DESC, products.name LIMIT $limit";

		$prods = db_query_array($sql);

		return $prods;
	}



   //get suggested prodects
   static function getSuggestedProducts($product_id, $recommendations = false, $customers_who_bought = false){
	  if(!$customers_who_bought){
	   	  global $CFG;

		  $link_id = $recommendations ? $CFG->recommended_link_id : $CFG->related_link_id;

	   	  $result = array();

	      $sql = "SELECT products.id, products.name, products.vendor_sku, price, product_cats.cat_id, link_types.name AS link_type_name, brands.name as brand_name
	   				FROM products
	   				LEFT JOIN brands on brands.id = products.brand_id
	   				LEFT JOIN product_cats ON products.id = product_cats.product_id
	   				LEFT JOIN product_links ON (
	   					product_links.product_id1 = products.id OR
	   					product_links.product_id2 = products.id)
	   				LEFT JOIN link_types ON link_types.id = product_links.link_type_id
	   				WHERE
	   					products.is_active='Y' AND
	   					products.is_available='Y' AND
	   					products.id != '$product_id' AND
	   					( product_links.product_id1 = '$product_id' OR product_links.product_id2 = '$product_id' ) AND product_links.link_type_id = $link_id
	   				GROUP BY products.id
	   				ORDER BY products.name";

	      $data = db_query_array($sql);
	      if(is_array($data) && count($data))
	      foreach($data as $prod){
	      	  $result[$prod['link_type_name']][] = $prod;
	      }

		  return $result;
     }
      else{
	      $sql = "SELECT products.id, products.name, products.vendor_sku, products.price, product_cats.cat_id, COUNT(shared_items.id) AS order_count, SUM(shared_items.qty) AS order_qty, brands.name as brand_name
	   				FROM order_items

	   				LEFT JOIN order_items AS shared_items ON shared_items.order_id = order_items.order_id
	   				LEFT JOIN products ON products.id = shared_items.product_id
	   				LEFT JOIN product_cats ON products.id = product_cats.product_id
	   				LEFT JOIN brands on brands.id = products.brand_id
	   				WHERE
	   					order_items.product_id = '$product_id' AND
	   					shared_items.product_id != '$product_id' AND
	   					products.is_active='Y'
	   				GROUP BY products.id
	   				ORDER BY order_count DESC, order_qty DESC LIMIT 5";

	      $data = db_query_array($sql);
	      if(is_array($data) && count($data))
	      foreach($data as $prod){
	      	  $result['Customers who bought this also bought...'][] = $prod;
	      }
      }

      return $result;
   }

   static function associate2Filters( $product_id, $filters, $delete=true )
   {
		if (!is_array($filters)) {
			$filters = Array($filters);
		}
		//print_ar( $filters );
		$filter_ids_str = '';
		foreach( $filters as $key => $filter )
		{
			if( $filter_ids_str != '' )
			{
				$filter_ids_str .= ', ' . $key;
			}
			else
			{
				$filter_ids_str .= $key;
			}
		}
		//print_ar( $filters );
		if (@trim($filter_ids_str) == '') {
			db_delete('product_filters',$product_id,'product_id');
			return true;
		}

		// delete old ones
		if( $delete )
		{
		//	db_query("DELETE FROM product_filters WHERE product_id = $product_id AND filter_id NOT IN ($filter_ids_str)");
			db_query("DELETE FROM product_filters WHERE product_id = $product_id");
		}
		// insert new ones

		$info = Array('product_id'=>$product_id);
		$currentsql = " SELECT * FROM product_filters where product_id = $product_id ";
		$cur_filters = db_query_array( $currentsql, 'filter_id' );
		//print_ar( $filters );
		foreach ($filters as $key => $filter) {
			if ($filter != "0" && $filter != "")
			{ // don't enter 0 or blank values!!!
				$filter = ucfirst(trim($filter));
				$info['filter_id'] = $key;
				$info['value'] = $filter;
				//print_ar( $info );
				if( $cur_filters[ $key ] )
					db_query( 'UPDATE product_filters set value = "' . addslashes($filter) . '" where product_id = ' . $product_id . ' and filter_id = ' . $key );
				else
					db_insert('product_filters',$info,'',true);
			}
		}

		return true;
   }

   static function getProductFilter( $product_id, $filter_id )
   {
   	if( !$product_id || !$filter_id )
   		return false;
   	$sql .= "SELECT * FROM product_filters where product_id = $product_id and filter_id = $filter_id ";
 		$result = db_query_array( $sql );

 		if( $result )
 			return $result[0];
 		else return false;
   }

   static function getProductCatTree( $product_id )
   {
   	 //Get the all the cats for the product
   	 //return a tree of the cats
   	 $cats = Products::getProductCats( $product_id );
   	 $cat_list = array();
   	 if( $cats )
	   	 foreach( $cats as $cat )
	   	 {
	   	 	 $cat_list[] = $cat['cat_id'] ;
	   	 }
	   else
	   	$cats = array();
   	 //Need to parse the cat_list and return a tree
   	 $cats = Cats::buildCatsTree( $cat_list );
   	 //print_ar( $cats );

   	 return $cats;
   }

   static function getProductFilterValues( $product_id, $cat_id=0 )
   {

 		$sql = " SELECT filters.name, product_filters.value, product_filters.filter_id FROM product_filters ";
   		$sql .= " LEFT JOIN filters on filters.id = product_filters.filter_id ";
   		$sql .= " WHERE product_filters.product_id = $product_id ";

   	if( $cat_id )
   	{
   		$specific_filters = array();
   		$filters = Filters::getFiltersForCat( $cat_id );

   		foreach( $filters as $f )
   		{
   			if( $f['display'] == 'Y' )
   				$specific_filters[] = $f['filter_id'];
   		}
   		$sql .= " and product_filters.filter_id IN ('" . implode( "','", $specific_filters ) . "') ";
   	}

	//echo $sql;

   	return db_query_array( $sql, 'filter_id' );
   }

   static function getProductPdfs( $product_id=0, $limit=0, $start=0, $last_updated=0 )
   {
	   	$sql = "SELECT * FROM product_pdfs WHERE 1 ";

	   	if( $product_id )
	   		$sql .= " AND product_id = $product_id ";


	   	//make sure the db is at least 1 week older than now
		if( $last_updated )
		{
			$sql .= " AND DATEDIFF( curdate(), last_checked ) > 7 ";
		}

		if( $limit )
		{
			$sql .=" limit $start, $limit ";
		}

	//	echo $sql;

	   	return db_query_array( $sql );
   }

   static function get1PDF( $id )
   {
   	$id = (int) $id;
		if (!$id) return false;

		$sql = " SELECT * FROM product_pdfs WHERE id = $id ";
		$result = db_query_array( $sql );

		return $result[0];
   }

   static function updateParentPrices( )
   {

   	//Need to get all of the parent products that have a price of 0.00. Then if they have options, asign the
   	//lowest option price to the parent
   	//This is only for sorting purposes, the parent product can't be purchased
   	//1/4/08 - I changed this to get all parents and update their prices to the lowest option.
   	//This should always be updated in case all options go up, parent shouldnt' display low as (old option)
   	$start = 0;

   	while($products = Products::get( 0,'','','','',0,'','','',0,0,0,0,0,0,0,0,0,'',false,false,'',false,'','','',1000,$start ) )
   	{
   		//print_ar( $products );
   		foreach( $products as $product )
   		{
   				$options = Products::getProductOptions( 0, $product['id'], false, true, '', false );
   				//first one is going to be the lowest price by default
   				if( $options )
   				{

   					//echo' Updating ' . $product['vendor_sku'] . ' = ' . $options[0]['additional_price'] .  ' <br />';
   					$prod_info = array();
   					$prod_info['price'] = $options[0]['additional_price'];
   					$prod_info['retail_price'] = $options[0]['additional_price'];

   					Products::update( $product['id'], $prod_info );
   				}
   				//if no options then this product has no children and not in the price sheet

   		}
   		$start += 1000;
   	}
   }

   static function getProductMarkup( $prod, $back = false )
   {
   	//I need to modify this funciton, now it is going to RETURN the AMOUNT that should be ADDED to the PRODUCT PRICE
   	//This is becasue of the fact that they want to also add a dollar value to certain ranges
   	//     < 100 --- add 25$
   	// 100 - 200 == 20$
   	// 200 - 300 -- $15
   	// 300 - 400 -- $10
  	//print_ar( $prod );

  		$product = Products::get1( $prod['product_id'] );

   		if( $prod['product_id'] ){
   			$product_id = $prod['product_id'];
   			$is_option = true;
   		}
   		else{
   			$product_id = $prod['id'];
   			$product = Products::get1( $product_id );
   			$is_option = false;
   		}

  		$is_unavailable = ($prod["is_available"] == "N" || $product["is_available"] == "N") ? true : false;

   		if( $prod['additional_price'] > 0 )
   		{
   			$price = $prod['additional_price'];
   			//echo $price . ' from add price ';
   			$product = Products::get1( $prod['product_id'] );
   			$prod['brand_id'] = $product['brand_id'];
   		}
   		else{
   			$price = $prod['price'];
   		}

   		if($price == 0.00 && $prod['final_price'] == 0.00){
   			$updateProduct = true;
   			if((int) $product_id){
   				$options = Products::getProductOptions( 0, $product_id, false, true, 'additional_price','','','','Y','N'); //get all options
   				if($options){
   					foreach($options as $option){
   						if($option['additional_price'] > 0 || $option['retail_price'] > 0 || $option['final_price'] > 0){
   							$updateProduct = false; //if the option is active and costs $$$, this is not a 0.00 product!
   							break;
   						}
   					}
   				}
   				if($updateProduct){
   					if($product["is_available"] == "Y"){
   						Products::update($product_id,array("is_available"=>"N"));
   					}
   				}
   				else{
   					if($product["is_available"] == "N"){
   						//Products::update($product_id,array("is_available"=>"Y"));
   					}
   				}
   			}

   			if($updateProduct || $is_option){
   				return 0.00;
   			}
   		}

   		if( $prod['final_price'] > 0 ){
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $prod['final_price'];
   		}


   		if( !$back ){

	   		switch( $price )
	 			{
	 				case $price == 0 :
	 					$extra_markup = 0;
	 					break;
	 				case $price < 100 :
	 					$extra_markup = 25;
	 					break;
	 				case $price >= 100 && $price < 200:
	 					$extra_markup = 20;
	 					break;
	 				case $price >= 200 && $price < 300:
	 					$extra_markup = 15;
	 					break;
	 				case $price >= 300 && $price < 400:
	 					$extra_markup = 10;
	 					break;
	 				default:
	 					$extra_markup = 0;
	 			}
   		}
 			else
 				$extra_markup = 0;

   	//Set back to true to return backend price
   		global $CFG;

   		//First check if the product has an override, then brand then cat, then default
   		//EDITED - cat moved to higher priority than brand - AF 11/19

   		//print_ar( $cats );
   		if( $back )
   			$type = 'backend';
   		else
   			$type = 'frontend';

   		if( $prod[$type .'_mark'] )
   		{
   			$markup = $prod[$type .'_mark'];

   			$price = $price * ($markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		$cat_mark = 0;

   		$cats = Products::getProductCatTree( $product_id ); //GET CAT PRICE

   		$orig_price = $price;
   		foreach( $cats as $cat )
   		{
   			if( $cat[$type . '_mark'] )
   			{
	   			$markup = $cat[$type . '_mark'];

	   			$price = $orig_price * ($markup/100 + 1) + $extra_markup;

	   			$price = self::roundup( $price, -1 );
   				$price = $price - 1; //Set it to the upper 9


	   			$cat_mark = 1;
   			}
   		}
   		unset( $cats );
   		if( $cat_mark ){
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		$brand = Brands::get1( $prod['brand_id'] );

   		if( $brand[$type .'_mark'] ) //GET BRAND PRICE
   		{
   			$markup = $brand[$type .'_mark'];

   			$price = $price * ($markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
   			unset( $brand );
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		if( $back )
   		{
   			if((($price + $extra_markup) >= $CFG->backend_markup_large_price) && $CFG->backend_markup_large_price > 0){
   				$price_markup = $CFG->backend_markup_large_mark;
   			}
   			else{
   				$price_markup = $CFG->backend_mark;
   			}

   			$price = $price * ($price_markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}
   		else
   		{
   			if((($price + $extra_markup) >= $CFG->frontend_markup_large_price) && $CFG->frontend_markup_large_price > 0){
   				$price_markup = $CFG->frontend_markup_large_mark;
   			}
   			else{
   				$price_markup = $CFG->frontend_mark;
   			}

   			$price = $price * ($price_markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );

   			$price = $price - 1; //Set it to the upper 9

   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}
   }

   static function isAvailableForPurchase($prod_id=0){
   		$prod_id = (int) $prod_id;
   		if(!$prod_id){
   			return false;
   		}
   		else{
   			$product = Products::get1($prod_id);
   			if($product["is_available"] == "N"){
   				return false;
   			}
   			else{
   				return true;
   			}
   		}
   }

   static function roundup ($value, $dp)
	 {
	    // Offset to add to $value to cause round() to round up to nearest significant digit for '$dp' decimal places
	    $offset = pow (10, -($dp + 1)) * 5;
	    return round ($value + $offset, $dp);
	 }

   static function displayDimension( $which, $type, $id, $quotes = false )
   {
   		$which = strtolower( $which );
   	//just need a funciton to take a floating point number and display a fraction
   		if( $type == 'option' )
   		{
   			$option = Products::getProductOption( $id );
   			$product = Products::get1( $option['product_id'] );

   			if( $option[ 'display_' . $which ] )
   			{
			    if( $option['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $option[ 'display_' . $which ] . "&quot;";
   				else
   					return $option[ 'display_' . $which ];
   			}

   			if( $product[ 'display_' . $which ] )
   			{
			    if( $product['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $product[ 'display_' . $which ] . "&quot;";
   				else
   					return $product[ 'display_' . $which ];
   			}

   			if( $option[$which] > 0 )
   				$number = $option[$which];
   			else
   				$number = $product[$which];
   		}
   		elseif( $type == 'product' )
   		{
   			$product = Products::get1( $id );
   			if( $product[ 'display_' . $which ] )
   			{
			    if( $product['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $product[ 'display_' . $which ] . "&quot;";
   				else
   					return $product[ 'display_' . $which ];
   			}

   			$number = $product[$which];
   		}

   		$number_parts = explode('.', $number );
   		$ret = '';
   		if( count( $number_parts ) > 1 && $number_parts[1] > 0 )
   		{
   			if( $number_parts[1] == 1111 )
   			{
   				$ret = $number_parts[0] . ' 1/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 2222 )
   			{
   				$ret = $number_parts[0] . ' 2/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 3333 )
   			{
   				$ret = $number_parts[0] . ' 1/3';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 4444 )
   			{
   				$ret = $number_parts[0] . ' 4/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 5555 )
   			{
   				$ret = $number_parts[0] . ' 5/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 6666 )
   			{
   				$ret = $number_parts[0] . ' 2/3';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 7777 )
   			{
   				$ret = $number_parts[0] . ' 7/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 8888 )
   			{
   				$ret = $number_parts[0] . ' 8/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}



   			$gcd = Products::gcd( trim($number_parts[1]), 10000 );
   			//echo 'gcd = ' . $gcd;
   			$numerator = trim($number_parts[1]) / $gcd;
   			//echo ' <br />10000 / ' . $gcd;
   			$denominator = 10000 / $gcd;
   			//print_ar( $number_parts );
   			$ret = $number_parts[0] . ' ' . $numerator . '/' . $denominator;
   		}
   		else
   		{
		    if( $number == 0 )
			return "N/A";
   			$ret = number_format($number, 0,'.','');
   		}

   		if( $quotes )
   			$ret .= "&quot;";

   		return $ret;
   }

   static function gcd( $x, $y)
   {
   	//echo $x . ' ' . $y;
			while ($y != 0) {
				$w = $x % $y;
				$x = $y;
				$y = $w;
		}
			return $x;
	}

	static function getSpecificMarkup( $prod, $mark )
	{
		  if( $prod['additional_price'] )
   			$price = $prod['additional_price'];
   		else
   			$price = $prod['price'];


 			$price = $price * ($mark/100 + 1);
 			//echo'price to return = ' . $price;
 			return $price;
	}
 	static function shouldTheItemsHaveCompare($items)
 	{
 		if (is_array($items)) 
 		{
 			$items_string = implode(",", $items);
 			$min_num_rows = 2;
 		}
 		else 
 		{
 			$items_string = $items;
 			$min_num_rows = 1;
 		}
 		$query = "SELECT distinct products.id
			FROM `products` 
			LEFT JOIN product_filters ON products.id = product_filters.product_id
			LEFT JOIN filters ON product_filters.filter_id = filters.id
			WHERE (
			height >0
			OR length >0
			OR width >0
			OR product_filters.value IS NOT NULL
			)
			AND products.id IN (" . $items_string . ") ";
 	
 		$result = db_query_array($query);
 		
 		if (count($result) >= $min_num_rows) return true; // show
 		else return false; // if on single item, are no filters or on group is only 1 with filters
 	}
	static function compare( $items, $cat_id , $specs)
	{
		global $CFG;
		$numb_products = count($items);
		?>
		    <div class="featured_items category_landing">
		<?
		echo'<table class="compare_items">';
		$products = array();
		$filter_names = array();
		$i = 0;
		$have_height = $have_width = $have_length = false;
		if( is_array( $items ) )
			foreach( $items as $item )
			{
				$products[$i] = Products::get1( $item );
				$products[$i]['filters'] = Products::getProductFilterValues( $item );
				foreach ($products[$i]['filters'] as $the_filter)
				{
					$filter_id = $the_filter['filter_id'];
					if (!array_key_exists($filter_id, $filter_names)) $filter_names[$filter_id]['name'] = $the_filter['name'];
					
				}
				
				if ($products[$i]['height'] > 0) $have_height = true;
				if ($products[$i]['width'] > 0) $have_width = true;
				if ($products[$i]['length'] > 0) $have_length = true;
				
				uasort($filter_names, 'sort_array_by_name');				
					
				
				//echo $i;
				//print_ar($products[$i]['filters'] );
				$i++;
			}
		if ($have_length)$filter_names[]['name'] = "Length";
		if ($have_width)$filter_names[]['name'] = "Width";
		if ($have_height) $filter_names[]['name'] = "Height";			
		//print_r( $products );
		//print_r($filter_names);
/* 		$prod_cats[] = $cat_id;
 	//	print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $prod_cats );
 	//	print_ar( $cats );
 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}

 		if( !$headers )
 			return false;

 		//print_ar( $headers );
 		$filters = Filters::getFiltersForCat( $filter_cat );
 		//print_ar( $filters );
*/
		echo'<tr class="comp_product_row">';
		echo'<td  class="comp_product_header_first" align="center"></td>';
		
		foreach( $products as $p )
		{
		    $name = $p['name'];
		    $name = preg_replace('/[^A-z0-9]/s','-',$name);

		    $path = $CFG->baseurl . 'itempics/' . $p['img_thumb_url'];
			$path_on_server = $CFG->dirroot . '/itempics/' . $p['img_thumb_url'];
       		if (!file_exists($path_on_server) || !$p['img_thumb_url']) $path =$CFG->baseurl . 'images/imagecomingsoon_t.jpg';		    
		    ?>
			<td align="center">
		    <div class="compare_items_box">
			    <div class="iner">
				<a href="<?=Catalog::makeProductLink_( $p )?>">
					<img src="<?=$path?>" alt="<?=$p['name']?>" />
				    </a>
					 <br />
					 <!--<span class="compare_brand">by <?=$p['brand_name']?></span> <br />
					<span class="compare_model">Model #: <?=$p['vendor_sku']?></span><br />
				   <span class="compare_sold">Sold As: <?= Products::getSoldAsText( $p['id'] ) ?></span><br />
				-->
				<!-- <span class="compare_price"><a href="<?=Catalog::makeProductLink_( $p )?>">
				    Our Price: $<?=number_format( $p['price'], 2 )?></a></span>-->
				</div>
			</div>
		    </div>
		    </td>
		    <?


//			$prod_link = Catalog::makeProductLink_($prod, true, false);
//			echo'<td class="comp_product_header" align="center" valign="top" ><div id="prod_title"><a href="'. $prod_link .'">' . $prod['name'] . '</a></div>';
//			if (file_exists(Catalog::makeProductImageLink($prod['id'], true, true))) {
//			    echo '<div id="img_container"><a href="'. $prod_link.'"><img id="prod_thumb" src="'. Catalog::makeProductImageLink($prod['id'], true ) .'" /></a></div>';
//			}
//			else
//			{
//
//				echo '<div id="img_container"><img id="prod_thumb" src="/pics/spacer.gif" /></div>';
//			}
//			$markup = Products::getProductMarkUp( $prod );
//			echo'<div class="prod_price">As Low As $' . number_format($markup, 2) . '</div>';
		}
		$row_length = $numb_products + 1;
		echo'</td>';
		echo'</tr>';
		$blank_row = '<tr class="comp_spec_row_even"><td class="comp_spec_name">&nbsp;</td>';
		for( $i = 0; $i < $numb_products; $i++ )
		{
				//print_ar( $products );
				$blank_row .= '<td class="comp_spec_value">&nbsp;</td>';		
		}	
		$blank_row .= "</tr>";
		$class = "comp_spec_row";
		
	//	foreach( $headers as $header )
		// First show product name and price
		echo'<tr class="' . $class . '">';
		echo'<td class="comp_spec_name"><nobr>' . "Product Name" . '</nobr></td>';
		
		for( $i = 0; $i < $numb_products; $i++ )
		{
			//print_ar( $products );
			echo'<td class="comp_spec_value">';
			echo $products[$i]['name'];
			echo'</td>';
		}
		echo "</tr>";
		echo $blank_row;
		
		echo'<tr class="' . $class . '">';
		echo'<td class="comp_spec_name"><nobr>' . Price . '</nobr></td>';
		
		for( $i = 0; $i < $numb_products; $i++ )
		{
			//print_ar( $products );
			echo'<td class="comp_spec_value">$';
			echo $products[$i]['price'];
			echo'</td>';
		}
		echo "</tr>";
		echo $blank_row;
		
		// Now show filters
		$counter = 0;
		foreach ($filter_names as $filter_id=>$spec)
		{
			$counter++;
		//	$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
			/*echo'<tr class="comp_header_row">';
			echo'<th class="comp_header_name">' . $spec['name'] . '</th>';
			echo'<td colspan="' . ($row_length-1) .'" class="comp_header_holder">&nbsp;</td></tr>';
				*/	
			//$class = Products::compareSpecs( $products, $spec );
				if ($counter > 1) echo $blank_row;				
						 
				echo'<tr class="' . $class . '">';
				echo'<td class="comp_spec_name"><nobr>' . $spec['name'] . '</nobr></td>';

				if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
				    trim(strtoupper($spec['name'])) == 'HEIGHT' ||
				    trim(strtoupper($spec['name'])) == 'LENGTH'  )
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
						//print_ar( $products );
						echo'<td class="comp_spec_value">';
						echo Products::displayDimension( $spec['name'], 'product', $products[$i]['id'], true);
						echo'</td>';
					}
				}
				else
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
					    if( $products[$i]['filters'][$spec['filter_id']]['value'] === 0 )
					    {
						$products[$i]['filters'][$spec['filter_id']]['value'] = 'N/A';
					    }
						echo'<td class="comp_spec_value">' . $products[$i]['filters'][$filter_id]['value'] . '</td>';
					}
				}
				echo'</tr>';
			
		}
		echo'</table>';
		?>
		    </div>
		<?
	}

	private function compareSpecs( $products, $spec )
	{
		//print_ar( $products );
		//print_ar( $spec );
		$same_class = "comp_spec_row";
		$different_class = "comp_spec_row_diff";

		$numb_products = count( $products );
		$different = false;
		if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
			trim(strtoupper($spec['name'])) == 'HEIGHT' ||
			trim(strtoupper($spec['name'])) == 'LENGTH' )
		{
			for( $i = 0; $i < $numb_products; $i++ )
			{
				$x = $i + 1;
				if( $x >= $numb_products )
					break;
				if( $products[$i][trim(strtolower($spec['name']))] != $products[$x][trim(strtolower($spec['name']))] )
				{
					$different = true;
					break;
				}
			}
		}
		else
		{
			for( $i = 0; $i < $numb_products; $i++ )
			{
				$x = $i + 1;
				if( $x >= $numb_products )
					break;
				if( $products[$i]['filters'][$spec['filter_id']]['value'] != $products[$x]['filters'][$spec['filter_id']]['value'] )
				{
					$different = true;
					break;
				}
			}
		}

		if( $different )
			return $different_class;
		else
			return $same_class;
	}

	static function getAllProductValues( $prod_id )
	{
		$prod_cats = Products::getProductCats( $prod_id );
//		$prod_cats = array();
		if( $prod_cats )
		    foreach( $prod_cats as $key => $val )
		    {
			    $prod_cats[] = $key;
		    }
		else
		    return false;
		//Quick fix here, it should only use compare if it didn't find a filter value, so putting type zspec will make that type show last.
		$sql = " SELECT distinct(cat_filters.filter_id),cat_filters.cat_id, cat_filters.display,  cat_filters.type, filters.name FROM cat_filters, filters WHERE cat_filters.cat_id in ('". implode("','", $prod_cats ) . "') AND filters.id=cat_filters.filter_id ";
		//$sql .= " UNION SELECT distinct(cat_specs.filter_id),cat_specs.cat_id, 'zspec' as 'type', filters.name FROM cat_specs, filters WHERE cat_specs.cat_id in ('". implode("','", $prod_cats ) . "') AND filters.id=cat_specs.filter_id order by name, cat_id DESC, type ASC";
		$sql .= "  order by name, cat_id DESC, type ASC";
		//echo $sql;
		$results = db_query_array( $sql );
		//print_ar( $results );

		$values = array();
		$last_value = 0;
		if( is_array( $results ) )
			foreach( $results as $r )
			{
				if( $r['filter_id'] == $last_value )
					continue;
				$i = $r['name'];
				$last_value = $r['filter_id'];
				$values[$i]['filter_id'] = $r['filter_id'];
				$values[$i]['cat_id'] = $r['cat_id'];
				$values[$i]['name'] = $r['name'];
				$values[$i]['display'] = $r['display'];
				if( $r['type'] == "range" || $r['type'] == 'select'  )
				{
					$values[$i]['type'] = 'text';
					$values[$i]['which'] = 'filter';
				}
				elseif( $r['type'] == "zspec" )
				{
					$values[$i]['type'] = 'text';
					$values[$i]['which'] = 'spec';
				}
				else
				{
					$values[$i]['type'] = $r['type'];
					$values[$i]['which'] = 'filter';
				}
			}
		//print_ar( $values );

		return $values;
	}

	static function editValues( $prod_id )
	{

		if( !$prod_id )
			return false;
		$values1 = Products::getAllProductValues( $prod_id );
		$values2 = Products::getProductFilterValues($prod_id); // replaced above line w/ this 2/13
		$values3 = array();
		foreach ($values2 as $v2)
		{ 
			$values3[$v2['name']] = $v2;
			
		}
		//$values = array_merge($values1, $values3);
		if ($values1 !== false && count($values1) > 0 && count($values3) > 0)	$values = $values1 + $values3; // union
		else if (count($values1) > 0) $values = $values1;
		else $values = $values3;
		
	//print_r( $values );
		$product = Products::get1( $prod_id );
		//print_ar( $product );
		echo'<table>';

		$prod_cats[] = Products::getProductCats( $prod_id );
		$prod_cats = array_shift( $prod_cats );

		$cat_list = array();
		foreach( $prod_cats as $pc )
		{
			$cat_list[] = $pc['cat_id'];
		}
 		//print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $cat_list );
// 		print_ar( $cats );

 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		$headers = array();
 		foreach( $cats as $cat )
 		{
 			$cur_headers = Specs::getCatHeaders( $cat );
// 			print_ar( $cur_headers );
 			if( $cur_headers )
 				$headers = array_merge( $headers, $cur_headers);
 		}
 		/*
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}*/
//print_ar( $headers );
//if( !$headers )
	if( true )
 		{
 		if( $values )
			foreach( $values as $v )
			{
				echo'<tr><th>';
				if( $v['display'] == "Y" )
					echo '<span style="font-size: 150%">*</span>';
				 echo $v['name'];

				echo '</th>';

				if( trim(strtoupper($v['name'])) == 'WIDTH' ||
                                    trim(strtoupper($v['name'])) == 'HEIGHT' ||
                                    trim(strtoupper($v['name'])) == 'LENGTH' )
				{
					echo'<td>';
						echo Products::displayDimension( $v['name'], 'product', $product['id'], true );

					echo'</td>';

				}
				else if( trim(strtoupper($v['name'])) == 'PRICE' )
				{
				    echo'<td>';
					?>
					$<?=$product['price']?>
					<?
				    echo'</td>';
				}
				else
				{
					$prod_filter = Products::getProductFilter( $prod_id, $v['filter_id'] );
					if( $prod_filter )
						$value = $prod_filter['value'];
					else
						$value = '';
					//if( $v['type'] == "text" )
					//{
						echo'<td><input type="text" name="info[filters][' . $v['filter_id'] . ']" size="25" value="' . htmlentities($value) . '" />';
						?><a href='#' onClick='window.open("view_filter_values.php?filter_id=<?=$v['filter_id']?>", "filters", "height=400,width=400");return false;'>See Current Values</a>
						<? echo'</td>';
					//}
					/*else
					{
						$options = Filters::getCatFilterOptions( $v['cat_id'], $v['filter_id'], true );
						if( !$options )
						{
							echo'<td><a href="category_options.php?cat_id=' . $v['cat_id'] . '&action=edit" target="_blank">
										ADD OPTIONS FOR THIS FILTER FIRST</a></td>';
						}
						else
						{
							echo'<td>' . Filters::getCatFilterOptionSelect($v['cat_id'], $v['filter_id'], $value, 'info[filters][' . $v['filter_id'] . ']') . '</td>';
						}
					}*/
				}
				echo'</tr>';
			}
 		}
 		else
 		{
 			foreach( $headers as $header )
 			{
 				echo'<tr><th colspan=2 style="font-size: 125%">' . $header['name'] . '</th></tr>';
 				$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
 				//print_ar( $specs );
 				//print_ar( $values );
 				foreach( $specs as $s )
 				{
 					if( $values[$s['name']] )
 						$v = $values[$s['name']];
 					else
 						continue;
 					//print_ar( $v );
	 				echo'<tr><th>';
						if( $v['display'] == "Y" )
							echo '<span style="font-size: 150%">*</span>';
						 echo $v['name'];
					echo '</th>';

					if( trim(strtoupper($v['name'])) == 'WIDTH' ||
									trim(strtoupper($v['name'])) == 'HEIGHT' ||
									trim(strtoupper($v['name'])) == 'LENGTH' )
					{
						echo'<td>';
							echo Products::displayDimension($v['name'], 'product', $product['id'], true );

						echo'</td>';

					}
					else
					{

						$prod_filter = Products::getProductFilter( $prod_id, $v['filter_id'] );
						if( $prod_filter )
							$value = $prod_filter['value'];
						else
							$value = '';
						if( $v['type'] == "text" )
						{
							echo'<td><input type="text" name="info[filters][' . $v['filter_id'] . ']" size="25" value="' . htmlentities($value) . '" /></td>';
						}
						else
						{
							$options = Filters::getCatFilterOptions( $v['cat_id'], $v['filter_id'], true );
							if( !$options )
							{
								echo'<td><a href="category_options.php?cat_id=' . $v['cat_id'] . '&action=edit" target="_blank">
											ADD OPTIONS FOR THIS FILTER FIRST</a></td>';
							}
							else
							{
								echo'<td>' . Filters::getCatFilterOptionSelect($v['cat_id'], $v['filter_id'], $value, 'info[filters][' . $v['filter_id'] . ']') . '</td>';
							}
						}
					}
					echo'</tr>';
 				}
 			}
 		}
		echo'</table>';
	}

	static function getAccessories( $id )
	{
		$link_types = Products::getLinkTypes();
		$acc_id = 0;
		foreach( $link_types as $link )
		{
			if( ($link['name']) == "Product Accessories" )
			{
				$acc_id = $link['id'];
				break;
			}
		}
		if( !$acc_id )
			return 0;

		$accessories = Products::getLinksForProduct( $id, false, $acc_id, 'product_links.option_id DESC, products.name' );

		return $accessories;
	}

	static function getPriceSelect( $product, $name='price_select', $id='price_select',
			$price_options=array(5,10,15,20), $sel_price=0, $msg='Select Price', $onchange='' )
	{
		$ret = "";
		$selected = false;
		$ret .= "<select name='$name' id='$id' onchange='$onchange' >";
		//$ret .="<option value=''>" . $msg . "</option>";
		$web_price = Products::getProductMarkup( $product );
		$ret .= "<option value='" .number_format($web_price,2) . "' ";

			if( number_format($sel_price,2) == number_format($web_price,2) )
			{
				$ret .= "selected";
				$selected = true;
			}
		$ret .= ">Web - " . number_format($web_price,2) . "</option>";

		foreach( $price_options as $opt )
		{
			$prod_price = Products::getSpecificMarkup( $product, $opt );
			$ret .= "<option value='" .number_format($prod_price,2) . "' ";
			if( number_format($sel_price,2) == number_format($prod_price,2) )
			{
				$ret .= "selected";
				$selected = true;
			}
			$ret .= ">$opt% - " . number_format($prod_price,2) . "</option>";
		}
		$ret .= "<option value='' ";
		if( !$selected )
			$ret .=" selected ";
		$ret .= ">Custom</option>";
		$ret .="</select>";

		return $ret;
	}


	static function outputInfoSidebar( $product, $cat_id )
	{
		global $CFG;

		$products = array();
		$products[] = $product;
		$prod_cats = array();
		if( !$cat_id )
		{
			$prod_cats = Products::getProductCats( $product['id'] );
			if( is_array( $prod_cats ) )
				foreach( $prod_cats as $key => $value )
				{
					$prod_cats[$key] = $value['cat_id'];
				}
		}
		else
			$prod_cats[] = $cat_id;

		$i = 0;
		$numb_products = count($products);
		if( is_array( $products ) )
			foreach( $products as $item )
			{
				$products[$i] = Products::get1( $item['id'] );
				$products[$i]['filters'] = Products::getProductFilterValues( $item['id'] );
				//echo $i;
				//print_ar($products[$i]['filters'] );
				$i++;
			}


 	//	print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $prod_cats );
 		//print_ar( $cats );
 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}

 		if( !$headers )
 		{
 			return false;
 		}

 		//print_ar( $headers );
 		$filters = Filters::getFiltersForCat( $filter_cat );
 		//print_ar( $filters );
 		?>
		<div class="main">
      	<?
      	if($headers){
		foreach( $headers as $header )
		{
			$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
			echo'<div class="sidebox_top_grey">';
			echo $header['name'] . '</div><div class="main1">';

			if( $specs ){
			echo '<table>';
			foreach( $specs as $spec )
			{
				$class = Products::compareSpecs( $products, $spec );


				if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
							trim(strtoupper($spec['name'])) == 'HEIGHT' ||
							trim(strtoupper($spec['name'])) == 'LENGTH' )
				{
					echo'<tr><td class="title">' . $spec['name'] . ':</td>';
					for( $i = 0; $i < $numb_products; $i++ )
					{
						//print_ar( $products );
						echo '<td class="value">'. Products::displayDimension($spec['name'], 'product', $products[$i]['id'], true) . '</td></tr>';
						echo'</tr>';
					}
				}
				else
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
						if( $products[$i]['filters'][$spec['filter_id']]['value'] == '' )
							continue;

						echo'<tr><td class="title">' . $spec['name'] . ':</td>';
						echo'<td class="value">' . $products[$i]['filters'][$spec['filter_id']]['value'] . '</td></tr>';

					}
				}

			}
			echo '</table></div>';
			}else{
				if(count($headers) == 1){
					echo "</div>";
				}
			}
		}}else{
			echo "</div>";
		}
		echo "</div>";
	}

	static function isMap($vendor_sku=""){
		$parent_id = ProductOptions::getParentID($vendor_sku);

		if($parent_id){
			$product = Products::get1($parent_id);
		}
		else{
			$product = Products::get1ByModel($vendor_sku);
		}

		if($product["is_map"] == "Y"){
			return true;
		}
		else{
			return false;
		}
	}

	static function missingImage( $filename )
	{
		$info['filename'] = $filename;

		return db_insert( 'missing_images', $info );
	}

	static function get1ByURLName( $url_name )
	{
		$product = db_get1(self::get(0,'','','','',0,'Y','','','',0,0,0,0,0,0,0,0,'',false,false,'',false,'','','',0,0,false,'N','',false,'','','',false,'N',false,'',$url_name));
		return $product;
		
   	    //$sql = " SELECT * FROM products WHERE 1 ";
	    ////$sql .= " AND UPPER(products.url_name) = UPPER('" . addslashes( $url_name ). "') AND products.is_active = 'Y' AND products.is_deleted = 'N' ";
	    //$sql .= " AND products.url_name = '" . addslashes( $url_name ). "' AND products.is_active = 'Y' AND products.is_deleted = 'N' ";

	    //echo $sql . '<br />';

	    //return db_get1( db_query_array( $sql ) );
	}
	static function get1ByVendorSkuName ($vendor_sku_name)
	{
		$product = db_get1(self::get(0,'','','','',0,'Y','','','',0,0,0,0,0,0,0,0,'',false,false,'',false,'','','',0,0,false,'N','',false,'','','',false,'N',false,'','',$vendor_sku_name));
		return $product;
	}
	static function setURL( $product, $update=false )
	{

	    /*if( $product['is_active'] == 'N' )
		return;*/

	    if( $update )
	    {
		//Clear the URL first bc it could be the same;
		$info['url_name'] = '';
		self::update( $product['id'], $info, false);
	    }
	    
	    $info['url_name'] = self::makeURL(trim($product['name']));

	    Products::update( $product['id'], $info, true);

	}
	
	static function makeURL( $name )
	{
		$url_name = trim($name);
	
		//Need to determine where to cut the string... 25 char limit....
		$parts = explode( ' ', $url_name );
	
		$count = 0;
		$short_name='';
		foreach( $parts as $part )
		{
			if( strlen( $part ) + $count <= 255 )
			{
				//	echo 'Count = ' . $count . '<br />';
				$short_name .= $part . ' ';
				$count = $count + strlen($part) + 1;
			}
			else
				break;
		}
		$url_name = trim($short_name);
	
		$url_name = str_replace( "&", "and", $url_name );
		$url_name = str_replace( "/", "-", $url_name );
	
		$url_name = preg_replace('/[^a-zA-Z0-9\- ]/','',$url_name);
		$url_name = str_replace(' ','-',$url_name);
	
	
		$url_name = rtrim( $url_name, '-' );
	
		//Now need to check for that URL in products and Cats to make sure it isn't there already
		$counter = 1;
		$orig_url = $url_name;
	
		$p_url = Products::get1ByURLName( $url_name );
	
		while ( $p_url = Products::get1ByURLName( $url_name ) )
		{
			$url_name = $orig_url . '-' . $counter;
	
			$counter++;
			//echo'trying... ' . $product['id'];
			//print_ar( $info['url_name'] );
		}
	
		while ( $c_url = Cats::get1ByURLName( $url_name ) )
		{
			$url_name = $orig_url . '-' . $counter;
	
			$counter++;
			while ( $p_url = Products::get1ByURLName( $url_name ) )
			{
				$url_name = $orig_url . '-' . $counter;
	
				$counter++;
// 				echo'trying... ' . $product['id'];
// 				print_ar( $url_name );
			}
		}
		while ( $b_url = Brands::get1ByURLName( $url_name ) )
		{
			$url_name = $orig_url . '-' . $counter;
	
			$counter++;
			while ( $p_url = Products::get1ByURLName( $url_name ) )
			{
				$url_name = $orig_url . '-' . $counter;
	
				$counter++;
// 				echo'trying... ' . $product['id'];
// 				print_ar( $url_name );
			}
		}
	
		$url_name = strtolower( $url_name );
	
		return $url_name;
	
	}

	static function checkPDFExists($original_url=""){
		if(!$original_url){
			return false;
		}

		$sql = "SELECT * FROM product_pdfs WHERE original_url = '".addslashes($original_url)."';";

		$results = db_query_array($sql);

		if($results){?>
				1----<?=$results[0][url]?>----<?=$results[0][original_url]?>
		<?}else{?>
				0----
			<?
		}
	}


	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT products.*, products.vendor_sku AS product_vendor_sku ";



		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(products.id)) as total ";
		}
		else{
			$select_ .= ", 
						IF (brands.min_charge > 5 AND exclude_min = 'N', CEIL(brands.min_amount/products.vendor_price),NULL) as min_qty,
						(products.price * (1 - products.case_price_percent)) AS case_price_per_piece,brands.name AS brand_name, 
						brands.freight_override as brand_freight_override ,
						brands.free_shipping AS brand_free_shipping,
						brands.recommended_vendor as is_recommended,
						brands.shipping_origin_zipcodes as brand_origin_zipcodes,
						brands.handling_fee as mfr_handling_fee ";
			$join_ = "LEFT JOIN brands on products.brand_id=brands.id ";
		}

		$from_ = " FROM products ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND products.id = [id] ";
		}

		if($o->brand_id >  0 ){
			$where_ .= " AND `products`.brand_id = [brand_id]";
		}
		if($o->vendor_id >  0 ){
			$where_ .= " AND `products`.vendor_id = [vendor_id]";
		}
		if($o->name != ''){
			$where_ .= " AND `products`.name = [name]";
		}
		if($o->tagline != ''){
			$where_ .= " AND `products`.tagline = [tagline]";
		}
		if($o->price >  0 ){
			$where_ .= " AND `products`.price = [price]";
		}
		if($o->retail_price >  0 ){
			$where_ .= " AND `products`.retail_price = [retail_price]";
		}
		if($o->vendor_price >  0 ){
			$where_ .= " AND `products`.vendor_price = [vendor_price]";
		}
		if($o->description != ''){
			$where_ .= " AND `products`.description = [description]";
		}
		if($o->notes != ''){
			$where_ .= " AND `products`.notes = [notes]";
		}
		if($o->vendor_sku != ''){
			$where_ .= " AND `products`.vendor_sku = [vendor_sku]";
		}
		if($o->vendor_sku_name != ''){
			$where_ .= " AND `products`.vendor_sku_name = [vendor_sku_name]";
		}
		if($o->is_active != ''){
			$where_ .= " AND `products`.is_active = '$o->is_active'";
		}
		if($o->is_available != ''){
			$where_ .= " AND `products`.is_available = '$o->is_available'";
		}
		if($o->is_not_available_description != ''){
			$where_ .= " AND `products`.is_not_available_description = [is_not_available_description]";
		}
		if($o->stock_level >  0 ){
			$where_ .= " AND `products`.stock_level = [stock_level]";
		}
		if($o->show_on_homepage != ''){
			$where_ .= " AND `products`.show_on_homepage = [show_on_homepage]";
		}
		if($o->homepage_order >  0 ){
			$where_ .= " AND `products`.homepage_order = [homepage_order]";
		}
		if($o->is_featured != ''){
			$where_ .= " AND `products`.is_featured = [is_featured]";
		}
		if($o->bizrate_id >  0 ){
			$where_ .= " AND `products`.bizrate_id = [bizrate_id]";
		}
		if($o->shopping_com_id >  0 ){
			$where_ .= " AND `products`.shopping_com_id = [shopping_com_id]";
		}
		if($o->price_grabber_id >  0 ){
			$where_ .= " AND `products`.price_grabber_id = [price_grabber_id]";
		}
		if($o->show_updated_new != ''){
			$where_ .= " AND `products`.show_updated_new = [show_updated_new]";
		}

		if($o->exclude_min != ''){
			$where_ .= " AND `products`.exclude_min = [exclude_min]";
		}

		if($o->exclude_discount != ''){
			$where_ .= " AND `products`.exclude_discount = [exclude_discount]";
		}

		if(is_array($o->date_updated)){
			if($o->date_updated['start']){
				$o->date_updated_start = $o->date_updated['start'];
				$where_ .= " AND `products`.date_updated >= [date_updated_start]";
			}
			if($o->date_updated['end']){
				$o->date_updated_end = $o->date_updated['end'];
				$where_ .= " AND `products`.date_updated <= [date_updated_end]";
			}
		} else 		if($o->date_updated != ''){
			$where_ .= " AND `products`.date_updated = [date_updated]";
		}
		if(is_array($o->date_added)){
			if($o->date_added['start']){
				$o->date_added_start = $o->date_added['start'];
				$where_ .= " AND `products`.date_added >= [date_added_start]";
			}
			if($o->date_added['end']){
				$o->date_added_end = $o->date_added['end'];
				$where_ .= " AND `products`.date_added <= [date_added_end]";
			}
		} else 		if($o->date_added != ''){
			$where_ .= " AND `products`.date_added = [date_added]";
		}
		if($o->sort >  0 ){
			$where_ .= " AND `products`.sort = [sort]";
		}
		if($o->show_in_yahoo_feed != ''){
			$where_ .= " AND `products`.show_in_yahoo_feed = [show_in_yahoo_feed]";
		}
		if($o->width >  0 ){
			$where_ .= " AND `products`.width = [width]";
		}
		if($o->height >  0 ){
			$where_ .= " AND `products`.height = [height]";
		}
		if($o->length >  0 ){
			$where_ .= " AND `products`.length = [length]";
		}
		if($o->weight >  0 ){
			$where_ .= " AND `products`.weight = [weight]";
		}
		if($o->display_height != ''){
			$where_ .= " AND `products`.display_height = [display_height]";
		}
		if($o->display_width != ''){
			$where_ .= " AND `products`.display_width = [display_width]";
		}
		if($o->display_length != ''){
			$where_ .= " AND `products`.display_length = [display_length]";
		}
		if($o->features != ''){
			$where_ .= " AND `products`.features = [features]";
		}
		if($o->specs != ''){
			$where_ .= " AND `products`.specs = [specs]";
		}
		if($o->shipping_type != ''){
			$where_ .= " AND `products`.shipping_type = [shipping_type]";
		}
		if($o->taxable != ''){
			$where_ .= " AND `products`.taxable = [taxable]";
		}
		if($o->website != ''){
			$where_ .= " AND `products`.website = [website]";
		}
		if($o->avail >  0 ){
			$where_ .= " AND `products`.avail = [avail]";
		}
		if($o->on_order >  0 ){
			$where_ .= " AND `products`.on_order = [on_order]";
		}
		if($o->include_singlefeed != ''){
			$where_ .= " AND `products`.include_singlefeed = [include_singlefeed]";
		}
		if($o->include_nextag != ''){
			$where_ .= " AND `products`.include_nextag = [include_nextag]";
		}
		if($o->include_pricegrabber != ''){
			$where_ .= " AND `products`.include_pricegrabber = [include_pricegrabber]";
		}
		if($o->include_shopping_com != ''){
			$where_ .= " AND `products`.include_shopping.com = [include_shopping.com]";
		}
		if($o->include_shopzilla != ''){
			$where_ .= " AND `products`.include_shopzilla = [include_shopzilla]";
		}
		if($o->include_become != ''){
			$where_ .= " AND `products`.include_become = [include_become]";
		}
		if($o->include_smarter != ''){
			$where_ .= " AND `products`.include_smarter = [include_smarter]";
		}
		if($o->promo_message != ''){
			$where_ .= " AND `products`.promo_message = [promo_message]";
		}
		if($o->keywords != ''){
			$where_ .= " AND `products`.keywords = [keywords]";
		}
		if($o->skip_update != ''){
			$where_ .= " AND `products`.skip_update = [skip_update]";
		}
		if($o->add1_caption != ''){
			$where_ .= " AND `products`.add1_caption = [add1_caption]";
		}
		if($o->add2_caption != ''){
			$where_ .= " AND `products`.add2_caption = [add2_caption]";
		}
		if($o->add3_caption != ''){
			$where_ .= " AND `products`.add3_caption = [add3_caption]";
		}
		if($o->add4_caption != ''){
			$where_ .= " AND `products`.add4_caption = [add4_caption]";
		}
		if($o->frontend_mark >  0 ){
			$where_ .= " AND `products`.frontend_mark = [frontend_mark]";
		}
		if($o->backend_mark >  0 ){
			$where_ .= " AND `products`.backend_mark = [backend_mark]";
		}
		if($o->final_price >  0 ){
			$where_ .= " AND `products`.final_price = [final_price]";
		}
		if($o->replace_with != ''){
			$where_ .= " AND `products`.replace_with = [replace_with]";
		}
		if($o->free_shipping != ''){
			$where_ .= " AND `products`.free_shipping = [free_shipping]";
		}
		if($o->aka_sku != ''){
			$where_ .= " AND `products`.aka_sku = [aka_sku]";
		}
		if($o->is_promo_allowed != ''){
			$where_ .= " AND `products`.is_promo_allowed = [is_promo_allowed]";
		}
		if($o->is_deleted != ''){
			$where_ .= " AND `products`.is_deleted = [is_deleted]";
		}
		if($o->is_map != ''){
			$where_ .= " AND `products`.is_map = [is_map]";
		}
		if($o->local_warehouse != ''){
			$where_ .= " AND `products`.local_warehouse = [local_warehouse]";
		}
		if($o->upc != ''){
			$where_ .= " AND `products`.upc = [upc]";
		}
		if($o->unadvertised_price_flag != ''){
			$where_ .= " AND `products`.unadvertised_price_flag = [unadvertised_price_flag]";
		}
		if($o->h2 != ''){
			$where_ .= " AND `products`.h2 = [h2]";
		}
		if($o->special_description != ''){
			$where_ .= " AND `products`.special_description = [special_description]";
		}
		if($o->added_by >  0 ){
			$where_ .= " AND `products`.added_by = [added_by]";
		}
		if($o->added_by_description != ''){
			$where_ .= " AND `products`.added_by_description = [added_by_description]";
		}
		if($o->spec_sheet_url != ''){
			$where_ .= " AND `products`.spec_sheet_url = [spec_sheet_url]";
		}
		if($o->ships_in != ''){
			$where_ .= " AND `products`.ships_in = [ships_in]";
		}
		if($o->is_finance != ''){
			$where_ .= " AND `products`.is_finance = [is_finance]";
		}
		if($o->priced_per != ''){
			$where_ .= " AND `products`.priced_per = [priced_per]";
		}
		if($o->per_price >  0 ){
			$where_ .= " AND `products`.per_price = [per_price]";
		}
		if($o->unit_case != ''){
			$where_ .= " AND `products`.unit_case = [unit_case]";
		}
		if($o->per_order != ''){
			$where_ .= " AND `products`.per_order = [per_order]";
		}
		if($o->line != ''){
			$where_ .= " AND `products`.line = [line]";
		}
		if($o->gift_wrapping != ''){
			$where_ .= " AND `products`.gift_wrapping = [gift_wrapping]";
		}
		if($o->url_name != ''){
			$where_ .= " AND `products`.url_name = [url_name]";
		}
		if($o->promo_msg != ''){
			$where_ .= " AND `products`.promo_msg = [promo_msg]";
		}
		if($o->include_google != ''){
			$where_ .= " AND `products`.include_google = [include_google]";
		}
		if($o->is_accessory != ''){
			$where_ .= " AND `products`.is_accessory = [is_accessory]";
		}

		if( $o->name_like != '' ) {
			$where_ .= " AND `products`.name LIKE '%" . addslashes( $o->name_like ) . "%' ";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY products.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}
		if ($o->search_keywords != '') {
			$join_ .= "LEFT JOIN product_cats ON products.id=product_cats.product_id ";
			$join_ .= "LEFT JOIN cats ON product_cats.cat_id=cats.id ";
			$join_ .= "LEFT JOIN product_options ON products.id=product_options.product_id ";
			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$where_ .= " AND " . db_split_keywords($o->search_keywords,$fields,'AND',true);
		}	
		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function getCatsForSearch( $keyword, $brand_id, $order='', $keyword_search=array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id') )
	{
	    global $CFG;

	     $result = Products::get(0,'',$keyword,$order,'',0,'Y','','','', $brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'','','',0, 0,false,'N','',true, $keyword_search,'','N',true);

	     return $result;
	}
	
	
	static function makeProductBoxes(){
		
	}

	static function chargeShipping( $product_id, $product=false)
	{
	    global $CFG;
//print_ar($product_id );
	    
	    if(!$product){
	    	$product = Products::get1( $product_id );
	    }
	    //$brand	= Brands::get1( $product['brand_id'] );
	    $cats	= Cats::getCatsForProduct( $product['id'] );
//print_ar( $product );
//print_ar( $cats );
	    if( $product['free_shipping'] == 'Y' )
		return false;

	    if( $product['brand_free_shipping'] == 'Y' )
		return false;

	    if( $cats )
		foreach( $cats as $c )
		{
		    if( $c['free_shipping'] == 'Y' )
			return false;
		}

	    return true;
	}
	static function doOptionsVaryInPrice($product_options)
	{
		$prices_array = array();
		foreach($product_options as $one_option)
		{
          //  echo '<br/><b>New option:</b> ' . print_r($one_option, 1);
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($one_option['option_essensa_price'] && $one_option['option_essensa_price'] != '0.00' 
					&& (!($one_option['additional_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['additional_price']) 
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['base_prod_price'])
					&& !($one_option['product_essensa_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['product_essensa_price'])))
					{
		//				echo 'the essensa option price:' . $one_option['option_essensa_price'] . '<br/>';
                        $prices_array[] = $one_option['option_essensa_price'];
					}
				else if ($one_option['product_essensa_price'] && $one_option['product_essensa_price'] != '0.00' && 
					!($one_option['additional_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['additional_price'])  
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['base_prod_price'])) 
					{
      //                  echo 'the essensa product price:' . $one_option['product_essensa_price'];
						$prices_array[] = $one_option['product_essensa_price'] ;
					}
				else if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00'){
    //                echo 'the optoin price: ' . $one_option['additional_price'] .'<br/>';
                     $prices_array[] = $one_option['additional_price'];
                }
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00'){
  //                  echo 'using the base prod price: ' . $one_option['base_prod_price']. '<br/>';
                     $prices_array[] = $one_option['base_prod_price'];	
			}
            /*echo 'the current price: ' . $prices_array[count($prices_array) - 1]
             . ' and the session: ' . $_SESSION['product_options'][$one_option['id']]['essensa_price'] . '<br/>';*/
            }

			else 
			{
				if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00') $prices_array[] = $one_option['additional_price'];
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00') $prices_array[] = $one_option['base_prod_price'];
//                echo '<br/><b>Non Essensa</b>the current prices: ' . $prices_array[count($prices_array) -1] . ' and the session: ' . $_SESSION['product_options'][$one_option['id']]['price'] . '<br/>';
			}
		}
		$prices_array_unique = array_unique($prices_array);
		if (count($prices_array_unique) > 1) return true;
		else return false;
	}
	static function getOptionsMinPrice($product_options)
	{
		$prices_array = array();		
		foreach($product_options as $one_option)
		{
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($one_option['option_essensa_price'] && $one_option['option_essensa_price'] != '0.00' 
					&& (!($one_option['additional_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['additional_price']) 
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['base_prod_price'])
					&& !($one_option['product_essensa_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['product_essensa_price'])))
					{
						$prices_array[] = $one_option['option_essensa_price'];
					}
				else if ($one_option['product_essensa_price'] && $one_option['product_essensa_price'] != '0.00' && 
					!($one_option['additional_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['additional_price'])  
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['base_prod_price'])) 
					{
						$prices_array[] = $one_option['product_essensa_price'];
					}
				else if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00') $prices_array[] = $one_option['additional_price'];
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00') $prices_array[] = $one_option['base_prod_price'];				
			}
			else 
			{
				if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00') $prices_array[] = $one_option['additional_price'];
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00') $prices_array[] = $one_option['base_prod_price'];
			}
		}
		$prices_array_unique = array_unique($prices_array);
		return min($prices_array_unique);		
	}
	static function getOptionsMaxPrice($product_options)
	{
		$prices_array = array();		
		foreach($product_options as $one_option)
		{
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($one_option['option_essensa_price'] && $one_option['option_essensa_price'] != '0.00' 
					&& (!($one_option['additional_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['additional_price']) 
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['base_prod_price'])
					&& !($one_option['product_essensa_price'] != '0.00' && $one_option['option_essensa_price'] > $one_option['product_essensa_price'])))
					{
						$prices_array[] = $one_option['option_essensa_price'];
					}
				else if ($one_option['product_essensa_price'] && $one_option['product_essensa_price'] != '0.00' && 
					!($one_option['additional_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['additional_price'])  
					&& !($one_option['base_prod_price'] != '0.00' && $one_option['product_essensa_price'] > $one_option['base_prod_price'])) 
					{
						$prices_array[] = $one_option['product_essensa_price'];
					}
				else if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00') $prices_array[] = $one_option['additional_price'];
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00') $prices_array[] = $one_option['base_prod_price'];	
			}
			else 
			{
				if ($one_option['additional_price'] && $one_option['additional_price'] != '0.00') $prices_array[] = $one_option['additional_price'];
				else if ($one_option['base_prod_price'] && $one_option['base_prod_price'] != '0.00') $prices_array[] = $one_option['base_prod_price'];
			}
		}
		$prices_array_unique = array_unique($prices_array);
		return max($prices_array_unique);		
	}
	static function displayColorChoices( $product_id , $page_id=null, $grid_view = false)
	{
	    global $CFG;
	    
	    //Need to check if there are color values for all the options. 
	    if (!$CFG->on_mobile) $new_display = true;
	    else $new_display = false;
	    
	    $product_options = Products::getProductOptions( 0, $product_id );
	    $options_vary_in_price = Products::doOptionsVaryInPrice($product_options);
		if ($options_vary_in_price)
		{
			$min_option_price = Products::getOptionsMinPrice($product_options);	
			$max_option_price = Products::getOptionsMaxPrice($product_options);
		}	    
	    foreach( $product_options as $key => $opt )
	    {
		$color_q = new OptionColorMapQuery();
		
		if($opt['color_id']){
			$color_q->id = $opt['color_id'];
		} else {
			$color_q->name = $opt['value'];
		}		
		//$color_q->name = $opt['value'];
		
		$color_map = OptionColorMap::get1ByO( $color_q );
		
		if( !$color_map )
		{
			$new_display = false;
		}
		else
		    $product_options[$key]['color_value'] = $color_map['color_value'];
			$product_options[$key]['option_type'] = $color_map['option_type'];		    
	    }
	    
	    if( $new_display ) {
			?>
				<div class="<?=($page_id)?'listing_available_colors_v2':($grid_view ? 'listing_available_colors_grid': 'listing_available_colors')?>">
				<input type="hidden" name="option[<?=$product_id?>]" value="" id="option_<?=$product_id?>" />
				<div class="listing_available_prods">
				<?php
					if (!$page_id){
						$p=Products::get1($product_id);
					//echo $p['free_shipping'];
					//if (($CFG->global_free_shipping_switch)&&($p['free_shipping']=='Y'))
					//	echo "<img style=\"float:left;padding:0;margin:0;vertical-align:top;\" src=\"images/free_shipping.jpg\" />";
					}
					?>
					Choose a Color:
				</div>
				<ul <?=(($page_id) && $page_id == "products" && $options_vary_in_price)? "style=width:280px;":"" ?>>
				<?
					$counter = 0;
					
					foreach( $product_options as $opt )
					{
						if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
						{
							if ($opt['option_essensa_price'] && $opt['option_essensa_price'] != '0.00' 
								&& (!($opt['additional_price'] != '0.00' && $opt['option_essensa_price'] > $opt['additional_price']) 
								&& !($opt['base_prod_price'] != '0.00' && $opt['option_essensa_price'] > $opt['base_prod_price'])
								&& !($opt['product_essensa_price'] != '0.00' && $opt['option_essensa_price'] > $opt['product_essensa_price'])))
								{
									$this_option_price = $opt['option_essensa_price'];
								}
							else if ($opt['product_essensa_price'] && $opt['product_essensa_price'] != '0.00' && 
								!($opt['additional_price'] != '0.00' && $opt['product_essensa_price'] > $opt['additional_price'])  
								&& !($opt['base_prod_price'] != '0.00' && $opt['product_essensa_price'] > $opt['base_prod_price'])) 
								{
									$this_option_price = $opt['product_essensa_price'];
								}
							else if ($opt['additional_price'] && $opt['additional_price'] != '0.00') $this_option_price = $opt['additional_price'];
							else if ($opt['base_prod_price'] && $opt['base_prod_price'] != '0.00') $this_option_price = $opt['base_prod_price'];	
						}
					  	else $this_option_price = ($opt['additional_price'] > 0) ? $opt['additional_price'] : $opt['base_prod_price'];
					  	$class_string = '';
						if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id, $opt['id'])) 
						{
							$this_option_price_string = "Sale: $" . $this_option_price;
							$the_sale_data = Sales::get(0, true, $product_id, 'N', $opt['id'], true, '', '', '', 'sale_price ASC');											
							if ($the_sale_data)
							{
								$this_sale_data = $the_sale_data[0];								
								$class_string = "This item is on sale!<br><div class=\'sale_price\'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
								$this_option_price_string = $class_string . $this_option_price_string;
							}
						}
						else $this_option_price_string = " $" . $this_option_price;						
						
				  		$option_prices_string = "&nbsp;" . $opt['value'] .  ": $". $this_option_price;
					  					  
						$counter++;
						if ($counter > 5 && (!$page_id || $page_id != "products")) 
						{
							echo "&nbsp;<a href='".Catalog::makeProductLink_($p)."'>More...</a>";
							break;
						}
						if ($opt['option_type'] == "color")
						{ 
						if (($page_id) && $page_id == "products" && $options_vary_in_price)
						{?>
						<li id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" style="border-bottom: none; border-top: none;padding-top:1px;padding-bottom:1px;float:left;">						
						<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>" style="padding:0;background: <?=$opt['color_value'] ?>;">&nbsp;</a>
						<? 						
	
						}
						else
						{
						?>
						<li id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );"style="padding:0;background: <?=$opt['color_value'] ?> ">
						<? /*<input style="width:18px;padding:0;background: <?=$opt['color_value'] ?>" disabled="disabled" type="text" size="1" value=" " alt="color"/> */?>
						<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>" style="padding:0;background: <?=$opt['color_value'] ?>;">&nbsp;</a>
						<? }?>
					</li>
					<? if (($page_id) && $page_id == "products" && $options_vary_in_price)
					{?>						
						<div style="display:inline;vertical-align:middle;color:black;font-size:11px;float:left;padding-left:2px;width:110px;line-height:2em;padding-bottom:2px;"><?=$option_prices_string?></div>						
					<? }
					}
					else if ($opt['option_type'] == "image")
					{
						if (($page_id) && $page_id == "products" && $options_vary_in_price)
						{?>												
						<li class="<?=$product_id?>_options" style="border-bottom: none; border-top: none;padding-top:1px;padding-bottom:1px;float:left;">
						<div style="display:none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
					<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>">					
					<img class="no_reg" id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" border="0" src="/optionpics/<?=$opt['color_value'] ?>" style="padding-top: 0px" onmouseover="mp_showtrail('/optionpics/<?=str_replace('.jpg', '_larger.jpg', $opt['color_value'])?>','',70,70)" onmouseout="mp_hidetrail()" style="border-bottom: none; border-top: none;padding-top:1px;padding-bottom:1px;float:left;">
					</a>
						<? 						
							
						}
						else
						{						
							?>
						<li class="<?=$product_id?>_options">
						<div style="display:none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
					<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>">					
					<img class="no_reg" id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" border="0" src="/optionpics/<?=$opt['color_value'] ?>" style="padding-top: 0px; " onmouseover="mp_showtrail('/optionpics/<?=str_replace('.jpg', '_larger.jpg', $opt['color_value'])?>','',70,70)" onmouseout="mp_hidetrail()">
					</a>	
					<? }?>
					</li>
					<? if (($page_id) && $page_id == "products" && $options_vary_in_price)
					{?>						
						<div style="display:inline;vertical-align:middle;color:black;font-size:11px;float:left;padding-left:2px;width:110px;line-height:2em;padding-bottom:2px;"><?=$option_prices_string?></div>						
					<? }					
					 }
				  }
					?>
				</ul>
				<?php

				?>
			</div>															
	    <?
		} else {
		$colors_avail = 0;
		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
		}
		
		
		if (!$CFG->on_mobile)
		{
		?>
		<div class="<?= $grid_view ? "options_grid_search" : "listing_available_colors_v1"?>">
		<select class="opts" id="option_<?=$product_id?>" name="option[<?=$product_id?>]" onChange="updateOptionValueDropdown('<?=$product_id?>', '<?=$options_vary_in_price ? $min_option_price . " - $" . $max_option_price : $option['base_prod_price']?>');">
		    <option value="">Options (Select <?php echo $opt_name;?>)</option>		 
		    <?
		} 
		else 
		{
			echo $opt_name . " Options: ";
		}

		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($option['option_essensa_price'] && $option['option_essensa_price'] != '0.00' 
					&& (!($option['additional_price'] != '0.00' && $option['option_essensa_price'] > $option['additional_price']) 
					&& !($option['base_prod_price'] != '0.00' && $option['option_essensa_price'] > $option['base_prod_price'])
					&& !($option['product_essensa_price'] != '0.00' && $option['option_essensa_price'] > $option['product_essensa_price'])))
				{
						$this_option_price = $option['option_essensa_price'];
				}
				else if ($option['product_essensa_price'] && $option['product_essensa_price'] != '0.00' && 
					!($option['additional_price'] != '0.00' && $option['product_essensa_price'] > $option['additional_price'])  
					&& !($option['base_prod_price'] != '0.00' && $option['product_essensa_price'] > $option['base_prod_price'])) 
				{
						$this_option_price = $option['product_essensa_price'];
				}
				else if ($option['additional_price'] && $option['additional_price'] != '0.00') $this_option_price = $option['additional_price'];
				else if ($option['base_prod_price'] && $option['base_prod_price'] != '0.00') $this_option_price = $option['base_prod_price'];	
			}
			else $this_option_price = ($option['additional_price'] > 0) ? $option['additional_price'] : $option['base_prod_price'];
			$class_string = '';
			if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id, $option['id'])) 
			{
				$this_option_price_string = "Sale: $" . $this_option_price;
				$the_sale_data = Sales::get(0, true, $product_id, 'N', $option['id'], true, '', '', '', 'sale_price ASC');											if ($the_sale_data)
					{
							$this_sale_data = $the_sale_data[0];
							$class_string = "This item is on sale!<br><div class='sale_price'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
					}
			}
			else $this_option_price_string = " $" . $this_option_price; 
			$option_prices_string = $option['value'] .  ": $". $this_option_price;
			    if( $p['inventory_limit'] == 'Y') {
				    //Check each option for inventory
				    $inv_o = (object) array();
				    $inv_o->product_id = $p['id'];

				    $inv_o->store_id = $CFG->default_inventory_location;
				    $inv_o->product_option_id = $option['id'];
				    $inv = Inventory::get1ByO( $inv_o );

				    if( $inv ){
					if( $inv['qty'] <= 0 ) {
					    continue;
					}
				    } else {
					    continue;
				    }
			    }
			    ++$colors_avail;
			    
			if (!$CFG->on_mobile)
		    {
			?>												    
			<option id="<?=$this_option_price?>" class="<?=$class_string?>" value="<?=$option['id']?>" <?=$option['is_default'] == 'Y' ? 'selected="selected"' : ''?>><?=$option['value'] . " - ". $this_option_price_string?></option>
			<?
		    }
		    else 
		    {
				if ($colors_avail > 1) echo ", ";
				echo $option['value'];  
		    }
		    
		}
		    
		    if (!$CFG->on_mobile)
		    {
		    ?>
		</select>
		</div>
		<?}
	    }
	}
	
	static function getSoldAsText( $product_id, $case_price_text = false )	
	{
		$sql = "SELECT priced_per, unit_case
			 FROM products WHERE id = $product_id";

		$result = db_query_array($sql);
		$my_result = $result[0];
		
		$priced_per = $my_result['priced_per'];
		$unit_case = $my_result['unit_case'];
		
		$text_to_return = "";
		
		if (($unit_case == "" || $unit_case == "0") && ($priced_per == "" || $priced_per == "0"))
		{
			if ($case_price_text) $text_to_return = "ea";
			else $text_to_return = "1 ea";
		}
		else if ($unit_case == "" || $unit_case == "0")
		{
			// if is some numeral in priced_per, just used priced_per
			// otherwise, use "1" as the unit case number
			$contains = preg_match('~[\d]~i', $priced_per);
			
			if ($contains) $text_to_return = $priced_per;
			else $text_to_return = "1 ". $priced_per; 	
		}
		else if ($priced_per == "" || $priced_per == "0") 
		{
			$text_to_return = $unit_case;
		}	
		else 
		{
			$text_to_return = $unit_case . " " . $priced_per;
		}
		return $text_to_return;
	}

	static function getSoldAsCaseText( $product_id)	
	{
		$sql = "SELECT priced_per, unit_case, num_pcs_per_case
			 FROM products WHERE id = $product_id";

		$result = db_query_array($sql);
		$my_result = $result[0];
		
		$priced_per = $my_result['priced_per'];
		$unit_case = $my_result['unit_case'];
		$num_pcs_per_case = $my_result['num_pcs_per_case'];
		
		$text_to_return = "";
		
		if (($unit_case == "" || $unit_case == "0") && ($priced_per == "" || $priced_per == "0"))
		{
			$text_to_return = $num_pcs_per_case;
		}
		else if ($unit_case == "" || $unit_case == "0")
		{
			// if is some numeral in priced_per, just used priced_per
			// otherwise, use "1" as the unit case number
			$matches = array();
			$contains = preg_match('~[\d]~i', $priced_per, $matches);
			
			if ($contains) 
			{
				$digit_part = $matches[0];
				$text_to_return = str_replace($digit_part, ($digit_part * $num_pcs_per_case), $priced_per);
			}
			else $text_to_return = $num_pcs_per_case. " ". $priced_per;			
		}
		else if ($priced_per == "" || $priced_per == "0") 
		{
			$text_to_return = ($unit_case * $num_pcs_per_case);
		}	
		else 
		{
			$text_to_return = ($unit_case * $num_pcs_per_case) . " " . $priced_per;
		}
		$text_to_return = str_ireplace("pack", "packs", $text_to_return);
		$text_to_return = str_ireplace("Box", "Boxes", $text_to_return);
		return $text_to_return;
	}
	
	static function get_cross_sells_for_product($product_id, $category_id)
	{

		//print_r($matches[0]);
		//echo $category_id." is the category id";
		$cross_sells = array();
		$already_done = array();
		// first get any hard-coded values for this product
		// then, if not yet 4, get name and divide it into separate words
		// put all "numeral words" in array, then loop through the array 
		// and get other prods in same cat that match
		// if still not 4, look in db for name/description best matches in that category
		
		$cross_sell_overrides = CrossSellOverrides::get(0,$product_id,'','order_fld');
		if ($cross_sell_overrides)
		{
			foreach ($cross_sell_overrides as $cross_sell_override)
			{
				if (!in_array($cross_sell_override['cross_sell_prod_id'],$already_done))
				{
					$res_array = array();
					$res_array['product_id'] = $cross_sell_override['cross_sell_prod_id'];
					$full_cross_sell = Products::get1($cross_sell_override['cross_sell_prod_id']);
					$res_array['name'] = $full_cross_sell['name'];
					$cross_sells[] = $res_array;
					$already_done[] = $res_array['product_id'];
				}
			}
		}
		if (count($cross_sells) < 16)
		{
			// get product name
			$this_prod = Products::get1($product_id);
			preg_match_all('!\d+!', $this_prod['name'], $matches);
			$this_prod['name'] = mysql_real_escape_string($this_prod['name']);
			if ($matches[0] && $category_id)
			{
				foreach ($matches[0] as $one_numeral)
				{
					$sql = "SELECT product_id,name from products, product_cats
						WHERE products.id = product_cats.product_id AND products.id<> $product_id 
						AND cat_id = $category_id
						AND products.name like '%$one_numeral%'
						AND is_active = 'Y' and is_deleted = 'N' AND is_accessory = 'N'";
				
					$result = db_query_array($sql);
					if ($result)
					{
						foreach ($result as $this_result)
						{
							if (!in_array($this_result['product_id'],$already_done))
							{
								$cross_sells[] = $this_result;
								$already_done[] = $this_result['product_id'];
							}
						}
					}
				}
			}
		}
		if (count($cross_sells) < 16)
		{
			$sql2 = "SELECT product_id, name FROM products,product_cats 
			WHERE MATCH (name, description) AGAINST ('".mysql_real_escape_string($this_prod['name'])."')
			AND products.id = product_cats.product_id AND products.id<> $product_id 
			AND cat_id = '$category_id' AND is_active = 'Y' and is_deleted = 'N'";
			
			$result2 = db_query_array($sql2);
				if ($result2)
				{					
					foreach ($result2 as $this_result2)
					{
						if (!in_array($this_result2['product_id'],$already_done))
						{ 
							$cross_sells[] = $this_result2;
							$already_done[] = $this_result2['product_id'];
						}
					}
				}
		}
		//print_r($cross_sells);

		return $cross_sells;
	}
	static function see_if_fba_exists($vendor_sku)
	{
		if (substr($vendor_sku, -3, 3) == "FBA") return true;
		else
		{
			$sql = "SELECT * FROM products where vendor_sku = concat('$vendor_sku', 'FBA') AND is_deleted = 'N'";
			$result = db_query_array($sql);
			if ($result) return true;
			else return false;
			
		}
	}
	static function see_if_prod_is_eligible_for_funding($product_id)
	{
		if (!is_numeric($product_id)) return false;
		else
		{
			$sql = "SELECT * FROM product_cats, cats WHERE product_cats.product_id = $product_id AND product_cats.cat_id = cats.id AND cats.eligible_for_funding = 'Y'";
			$result = db_query_array($sql);
			if ($result) return true;
			else return false;
			
		}
	}
	
	static function getProductOptionValue($product_id,$product_option_id){
		$sql = "SELECT  `value` FROM  `product_options`
		WHERE  `id` = " . $product_option_id . " AND  `product_id` = ". $product_id . " ";
		return db_query_array($sql);
	}
	
	static function getProductInventory ( $product_id = '', $option_id = '', $product_list = ''){

		global $CFG;
		
		$sql = "CREATE TEMPORARY TABLE IF NOT EXISTS extracted_products(
			`product_id` INT( 10 ) NOT NULL ,
			`product_option_id` INT( 10 ) ,
			`vendor_sku` VARCHAR( 255 ) ,
			`product_option_vendor_sku` VARCHAR( 255 )  ,
			`original_sku` VARCHAR( 255 ) ,
			`product_option_original_sku` VARCHAR( 255 ) ,
            `original_mfr_part_num` VARCHAR( 255 ) ,
			`product_option_original_mfr_part_num` VARCHAR( 255 ) ,
			`pk`VARCHAR( 6 ) ,
			`stock_qty` INT(11) NOT NULL ,
			   INDEX (  `product_id` ) ,
			   INDEX (  `product_option_id` ) ,
			   INDEX (  `original_sku` ) ,
			   INDEX (  `product_option_original_sku` ),
               INDEX (  `original_mfr_part_num` ) ,
			   INDEX (  `product_option_original_mfr_part_num` ),
			   INDEX (  `pk` )
		)AS(
			SELECT products.id AS product_id, 
				product_options.id AS product_option_id, 
				products.vendor_sku AS vendor_sku, 
				product_options.vendor_sku AS product_option_vendor_sku, 
				SUBSTRING_INDEX( SUBSTRING_INDEX( products.vendor_sku, 'FBA', 1 ), '@', 1 ) AS original_sku, 
				SUBSTRING_INDEX( SUBSTRING_INDEX( product_options.vendor_sku, 'FBA', 1 ), '@', 1 ) AS product_option_original_sku, 
                SUBSTRING_INDEX( SUBSTRING_INDEX( products.mfr_part_num, 'FBA', 1 ), '@', 1 ) AS original_mfr_part_num, 
				SUBSTRING_INDEX( SUBSTRING_INDEX( product_options.mfr_part_num, 'FBA', 1 ), '@', 1 ) AS product_option_original_mfr_part_num, 
				IF( 
					products.vendor_sku REGEXP '@[[:digit:]]+PK', 
					IF( 
						SUBSTRING_INDEX( products.vendor_sku, '@', -1 ) REGEXP '[[:digit:]]+PK', 
						SUBSTRING_INDEX( SUBSTRING_INDEX( products.vendor_sku, 'FBA', 1 ) , '@', -1) , 
						SUBSTRING( products.vendor_sku, LOCATE( '@', products.vendor_sku ) +1, LOCATE( 'PK', products.vendor_sku ) +1 - LOCATE( '@', products.vendor_sku ) ) 
						), 
					NULL 
					) AS pk,
            SUM( inventory.qty ) AS stock_qty 
			FROM products 
				LEFT JOIN product_options ON product_options.product_id = products.id 
                LEFT JOIN inventory ON products.id = inventory.product_id
				AND (
				product_options.id IS NULL 
				OR product_options.id = inventory.product_option_id
				)
			WHERE ( 
					products.vendor_sku LIKE CONCAT( products.vendor_sku, '@%' ) 
					OR 
					products.vendor_sku = products.vendor_sku 
				) ";
// 		if($product_sku_list){
// 			$sql .= " AND (SUBSTRING_INDEX( products.vendor_sku,  '@', 1 ) IN ($product_sku_list) ) " ;
// 		}
		if($product_list){
			$sql .= " AND ( products.id IN ($product_list) ) " ;
		}
		
		if($product_id)
			$sql .= " AND products.id = $product_id";
		if($option_id){
			$sql .= " AND product_options.id = $option_id";
		}
		$sql .= " AND ( inventory.store_id = " . $CFG->default_inventory_location . " OR inventory.store_id IS NULL ) ";
		$sql .= " GROUP BY original_sku, product_option_original_sku, pk);";
		
		db_query_array($sql);
		
		 
		//echo $sql . 'SELECT * FROM extracted_products';
// 		echo "<br>";
		
		$sql = "SELECT product_id,
						product_option_id,
						extracted_products.vendor_sku,
						product_option_vendor_sku,
						extracted_products.original_sku,
						product_option_original_sku,
						extracted_products.pk,
						stock_qty,
   						fba_qty	
   				FROM extracted_products				
   				LEFT JOIN (   					
		   				SELECT sku AS vendor_sku,
						SUBSTRING_INDEX( SUBSTRING_INDEX(sku,'FBA',1), '@', 1 ) AS original_sku,
						IF(
							sku REGEXP '@[[:digit:]]+PK',
							IF(
								SUBSTRING_INDEX( SUBSTRING_INDEX(sku,'FBA',1), '@', -1 ) REGEXP '[[:digit:]]+PK',
								SUBSTRING_INDEX( SUBSTRING_INDEX( sku,'FBA',1 ) , '@', -1) ,
								SUBSTRING( SUBSTRING_INDEX(sku,'FBA',1), LOCATE( '@', SUBSTRING_INDEX(sku,'FBA',1) ) +1, LOCATE( 'PK', SUBSTRING_INDEX(sku,'FBA',1) ) +1 - LOCATE( '@', SUBSTRING_INDEX(sku,'FBA',1) ) )
								),
							NULL
							) AS pk,
			             SUM( afn_fulfillable_quantity ) AS fba_qty
						 FROM amazon_fba_inventory
						 WHERE 1 ";
				//if I want to use this I need to add the product option skus
				//if($product_sku_list){
				//	$sql .= " AND (SUBSTRING_INDEX( SUBSTRING_INDEX(sku,'FBA',1),  '@', 1 ) IN ($product_sku_list) ) " ;
				//}
				$sql .= " GROUP BY original_sku, pk) AS fba_stock";
			$sql .= " ON (
				 		((fba_stock.original_sku = extracted_products.original_sku OR fba_stock.original_sku = extracted_products.product_option_original_sku)
							OR
						(fba_stock.original_sku = extracted_products.original_mfr_part_num OR fba_stock.original_sku = extracted_products.product_option_original_mfr_part_num))
				 		 AND ( fba_stock.pk = extracted_products.pk OR (fba_stock.pk IS NULL AND extracted_products.pk IS NULL) )
					)";
		
// 		db_query_array($sql);
// 		echo $sql;
// 		$sql = "DROP  TABLE IF EXISTS extracted_products";
		
		//$sql = "SELECT * FROM extracted_products";
		return db_query_array($sql);

	}
	  
	function updateColorValue($color_id){
		$option_color_map = OptionColorMap::get1($color_id);
		$sql = "UPDATE product_options SET value = '" . $option_color_map['name'] . "'
			WHERE color_id = $color_id ";
		return db_query_array($sql);
	}

	static function getExportData($field_list_string, $keywords = '', $cat_id = 0, $sub_cat = 0, $is_active = '', $brand_id = '', $group_by = 'products.id')
	{
		$select_clause = "SELECT " . $field_list_string . " FROM ";
		$from_clause = "products
				LEFT JOIN brands ON products.brand_id = brands.id
				LEFT JOIN ships_in ON products.ships_in = ships_in.id
				LEFT JOIN product_options ON product_options.product_id = products.id
				LEFT JOIN product_cats pc1 ON products.id = pc1.product_id 
				LEFT JOIN cats c1 ON (pc1.cat_id = c1.id AND c1.pid = 0)";
		$where_clause = " WHERE 1 AND products.is_deleted = 'N'";
		if (trim($keywords) != '') 
		{
			$keywords = urldecode($keywords);
			$keywords = mysql_real_escape_string($keywords);
			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','c1.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$where_clause .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		
		if ($sub_cat != 0) $cat_id = $sub_cat;

		if ($cat_id != 0)
		{
			$from_clause .= " LEFT JOIN product_cats pc2 ON 
				(pc2.product_id = products.id AND pc2.cat_id = $cat_id)";
			$where_clause .= " AND pc2.cat_id IS NOT NULL";
		}
		
		if ($is_active != '') $where_clause .= " AND products.is_active = '" . $is_active . "'";
		if ($brand_id != 0) $where_clause .= " AND products.brand_id = '" . $brand_id . "'";

		$group_by_clause = " GROUP BY $group_by";
		
		$query = $select_clause . $from_clause . $where_clause . $group_by_clause;
		//mail("rachel@tigerchef.com", "query", $query);
		$result =  db_query_array($query);
		//$result_output = print_r($result, true);
		//mail("rachel@tigerchef.com", "array", $result_output);
		return $result;
	}
	
	static function getPriceToDisplayForProduct($product_id)
	{
		$return_array = array();
		
		$product = Products::get1( $product_id); //Get the detailed info.
		if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && 
			$product['essensa_price'] != '0.00' && $product['essensa_price'] < $product['price']) 
		{
				$product['price'] = $product['essensa_price'];
				$is_essensa_pricing = true;
		}
		$product_options = Products::getProductOptions( 0, $product_id );
		if ($product_options)
		{
			$options_vary_in_price = Products::doOptionsVaryInPrice($product_options);
			if ($options_vary_in_price)
			{				
				$min_option_price = Products::getOptionsMinPrice($product_options);	
				$max_option_price = Products::getOptionsMaxPrice($product_options);
				$option_prices = array('min' => $min_option_price, 'max' => $max_option_price);
			}		
            else{
                //we don't care which option if they share a price, but we can't do product_options[0] because the array keys are ids
                $first_po = array_shift($product_options);
                if($is_essensa_pricing && $first_po['essensa_price'] > 0){
                    $product['price'] = $first_po['essensa_price'];
                }
                elseif($first_po['additional_price'] > 0){
                    $product['price'] = $first_po['additional_price'];
                }
            }

		}
		else $options_vary_in_price = false;
	    	
		if ($options_vary_in_price) $reg_price_to_print = number_format( $min_option_price, 2, ".", "" ) . ' - $' .number_format( $max_option_price, 2, ".", "" ); 
		else $reg_price_to_print = number_format( $product['price'], 2, ".", "" );
		
		if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product['id'], 'none'))
		{	
			$the_sale_data = Sales::get(0, true, $product['id'], 'N', 'none', true, '', '', '', 'sale_price ASC');
				
			if ($product_options)
			{
				$sale_for_options_data = Sales::see_if_all_options_on_sale($product['id']);
				
				if ($sale_for_options_data)
				{
					if ($sale_for_options_data['min_old_price'] == $sale_for_options_data['max_old_price']) $old_price_to_print = number_format( $sale_for_options_data['min_old_price'], 2, ".", "" );
					else $old_price_to_print = number_format( $sale_for_options_data['max_old_price'], 2, ".", "" );

					if ($sale_for_options_data['min_sale_price'] == $sale_for_options_data['max_sale_price']) $sale_price_to_print = number_format( $sale_for_options_data['min_sale_price'], 2, ".", "" );
					else{
						$sale_price_to_print = number_format( $sale_for_options_data['max_sale_price'], 2, ".", "" ) . ' - $' . number_format( $sale_for_options_data['max_sale_price'], 2, ".", "" );
						$option_prices = array('min' => $sale_for_options_data['min_sale_price'], 'max' => $sale_for_options_data['max_sale_price']);
					}
				}else{
					
				}
			}					
			else 
			{
				$old_price_to_print =$the_sale_data[0]['old_price'] ;																										
				$sale_price_to_print =$the_sale_data[0]['sale_price'] ;
			}								  	
		}
		
		if ($product['case_discount'] == 'Y')
		{
			if($is_essensa_pricing || $product_options){
				$product['case_price_per_piece'] = $product['price'] * (1 - $product['case_price_percent']);
			}
			if($option_prices){
				$case_price_per_piece['min'] = $option_prices['min'] * (1 - $product['case_price_percent']);
				$case_price_per_piece['max'] = $option_prices['max'] * (1 - $product['case_price_percent']);
				
				$return_array['case_price'] = number_format($case_price_per_piece['min'] * $product['num_pcs_per_case'], 2);
				$return_array['case_price_per_piece'] = number_format($case_price_per_piece['min'], 2);
			}else{
				$return_array['case_price'] = $product['case_price_per_piece'] * $product['num_pcs_per_case'];
				$return_array['case_price_per_piece'] = number_format($product['case_price_per_piece'], 2); 
			}
		}
		
		$return_array['reg_price_to_print'] = $reg_price_to_print;
		if ($sale_price_to_print){ $return_array['sale_price_to_print'] = $sale_price_to_print; }
		if ($old_price_to_print){ $return_array['old_price_to_print'] = $old_price_to_print; }
		if ($option_prices){ $return_array['option_prices'] = $option_prices; }
		return $return_array;
	}
	
	static function getCanBuyInfo($product, $cfg_obj)
	{					
		$can_buy = true;
    	$can_buy_limit = 0;
    		
    	if( $product['inventory_limit'] == 'Y' )
		{
		    //Check for inventory
		    $inv_o = (object) array();
		    $inv_o->product_id = $product['id'];
			$inv_o->store_id = $cfg_obj->default_inventory_location;

	    	$inv = Inventory::getByO( $inv_o );

	    	$can_buy = false;

	    	if( $inv )
	    	{
				foreach( $inv as $r )
				{
		    		if( $r['qty'] > 0 )
		    		{
						$can_buy = true;
						$can_buy_limit += $r['qty'];
	
						$can_buy_limit -= $r['on_order'];
		    		}
				}		
	    	}

	    	// also see if this is an M. Rothman or Flash Furniture item.  
	    	// If it is, we'll want to limit to current Flash Furniture stock.		
			$suppliers = ProductSuppliers::get($product['id']); 

			foreach($suppliers as $the_supplier)
			{ 
				
				if ($the_supplier['brand_id'] == $cfg_obj->m_rothman_supplier_id)
				{
					// get current M. Rothman inventory
					$cur_mr_stock = SupplierFeed::getCurrentStock($cfg_obj->m_rothman_table, "qty_avail", "dist_sku", $the_supplier['dist_sku']);
					$can_buy_limit += $cur_mr_stock;
				}
				if ($the_supplier['brand_id'] == $cfg_obj->flash_furniture_supplier_id)
				{
					// get current Flash Furninture inventory
					$cur_ff_stock = SupplierFeed::getCurrentStock($cfg_obj->flash_furniture_table, "qty_avail", "dist_sku", $the_supplier['dist_sku']);
					$can_buy_limit += $cur_ff_stock;				
				}
				if ($the_supplier['brand_id'] == $cfg_obj->weston_supplier_id)
				{
					// get current Weston inventory
					$cur_ws_stock = SupplierFeed::getCurrentStock($cfg_obj->weston_table, "qty_avail", "dist_sku", $the_supplier['dist_sku']);
					$can_buy_limit += $cur_ws_stock;				
				}				
				if ($the_supplier['brand_id'] == $cfg_obj->johnson_rose_supplier_id)
				{
					// get current Weston inventory
					$cur_jr_stock = SupplierFeed::getCurrentStock($cfg_obj->johnson_rose_table, "qty_avail", "dist_sku", $the_supplier['dist_sku']);
					$can_buy_limit += $cur_jr_stock;				
				}
			}
			if( !$can_buy_limit ) $can_buy = false;
			else $can_buy = true;		    	
		}
    	$return_array = array();
    	$return_array['can_buy_limit'] = $can_buy_limit;
    	$return_array['can_buy'] = $can_buy;
    	return $return_array;		
	}
	static function displayColorChoicesRedesign( $product_id , $page_id=null, $grid_view = false)
	{
	    global $CFG;
	    
	    //Need to check if there are color values for all the options. 
	    $new_display = true;	    
	    
	    $product_options = Products::getProductOptions( 0, $product_id );
	    $options_vary_in_price = Products::doOptionsVaryInPrice($product_options);
		if ($options_vary_in_price)
		{
			$min_option_price = Products::getOptionsMinPrice($product_options);	
			$max_option_price = Products::getOptionsMaxPrice($product_options);
		}	    
	    foreach( $product_options as $key => $opt )
	    {
			$color_q = new OptionColorMapQuery();
		
			if($opt['color_id']){
				$color_q->id = $opt['color_id'];
			} else {
				$color_q->name = $opt['value'];
			}		
		
			$color_map = OptionColorMap::get1ByO( $color_q );
		
			if( !$color_map )
			{
				$new_display = false;
			}
			else
			{
			    $product_options[$key]['color_value'] = $color_map['color_value'];
				$product_options[$key]['option_type'] = $color_map['option_type'];
			}		    
	    }
	    
	    if( $new_display ) {
			?>
				<div class="inner-box color">
				<input type="hidden" name="option[<?=$product_id?>]" value="" id="option_<?=$product_id?>" />
				<?php
					if (!$page_id){
						$p=Products::get1($product_id);
					}
					?>
					<span>Choose a Color:</span>				
				<ul class="color-list">
				<?
					$counter = 0;
					
					foreach( $product_options as $opt )
					{
						if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
						{
							if ($opt['option_essensa_price'] && $opt['option_essensa_price'] != '0.00' 
								&& (!($opt['additional_price'] != '0.00' && $opt['option_essensa_price'] > $opt['additional_price']) 
								&& !($opt['base_prod_price'] != '0.00' && $opt['option_essensa_price'] > $opt['base_prod_price'])
								&& !($opt['product_essensa_price'] != '0.00' && $opt['option_essensa_price'] > $opt['product_essensa_price'])))
								{
									$this_option_price = $opt['option_essensa_price'];
								}
							else if ($opt['product_essensa_price'] && $opt['product_essensa_price'] != '0.00' && 
								!($opt['additional_price'] != '0.00' && $opt['product_essensa_price'] > $opt['additional_price'])  
								&& !($opt['base_prod_price'] != '0.00' && $opt['product_essensa_price'] > $opt['base_prod_price'])) 
								{
									$this_option_price = $opt['product_essensa_price'];
								}
							else if ($opt['additional_price'] && $opt['additional_price'] != '0.00') $this_option_price = $opt['additional_price'];
							else if ($opt['base_prod_price'] && $opt['base_prod_price'] != '0.00') $this_option_price = $opt['base_prod_price'];	
						}
					  	else $this_option_price = ($opt['additional_price'] > 0) ? $opt['additional_price'] : $opt['base_prod_price'];
					  	$class_string = '';
					  	$this_sale_data = '';
						if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id, $opt['id'])) 
						{
							$the_sale_data = Sales::get(0, true, $product_id, 'N', $opt['id'], true, '', '', '', 'sale_price ASC');											
							if ($the_sale_data)
							{
								$this_sale_data = $the_sale_data[0];
								$this_option_price = $this_sale_data['sale_price'];
								$this_option_price_string = "Sale: $" . $this_sale_data['sale_price'];
								$class_string = "This item is on sale!<br><div class=\'sale_price\'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
								$this_option_price_string = $class_string . $this_option_price_string;
							}
						}
						else $this_option_price_string = " $" . $this_option_price;						
						
				  		$option_prices_string = "&nbsp;" . $opt['value'] .  ": $". $this_option_price;
				  				  
						$counter++;
						if ($counter > 5 && (!$page_id || $page_id != "products")) 
						{
							echo "</span>";
							echo "&nbsp;<a class='more-link-2' href='".Catalog::makeProductLink_($p)."'>More...</a>";
							break;
						}
						if ($counter == 4 && (!$page_id || $page_id != "products"))
						{
							echo "<span class='more-link'>&nbsp;<a href='".Catalog::makeProductLink_($p)."'>More...</a></span>";
							echo "<span class='some-more-colors'>";
						}
						if ($opt['option_type'] == "color")
						{  
							if (($page_id) && $page_id == "products" && $options_vary_in_price)
							{?>
							<li id="<?=$opt['id']?>" class="<?=$product_id?>_options"  style="background:<?=$opt['color_value']?>;border:<?=$opt['color_value']?>;" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );">						
							<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>" >&nbsp;</a>
							<? 						
		
							}
							else
							{
							?>
							<li id="<?=$opt['id']?>" class="<?=$product_id?>_options" style="background:<?=$opt['color_value']?>;border:<?=$opt['color_value']?>;" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" style="background: <?=$opt['color_value'] ?> ">
							<? /*<input style="width:18px;padding:0;background: <?=$opt['color_value'] ?>" disabled="disabled" type="text" size="1" value=" " alt="color"/> */?>
							<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>">&nbsp;</a>
							<? }?>
							
							<? 
							if ($this_sale_data && !Sales::see_if_all_options_on_sale($product_id))
							{?>
								<span class="option_sale">SALE</span>		
							<? }
							if (($page_id) && $page_id == "products" && $options_vary_in_price)
							{?>						
								<span class="option_price"><?=$option_prices_string?></span>		
							<? }
							?>
							</li>
							<?php 
						}
						else if ($opt['option_type'] == "image")
						{
							if (($page_id) && $page_id == "products" && $options_vary_in_price)
							{?>												
							<li class="<?=$product_id?>_options">
							<div style="display:none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
						<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>">					
						<img class="no_reg" id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" border="0" src="/optionpics/<?=$opt['color_value'] ?>" >
								<!--  onmouseover="mp_showtrail('/optionpics/<?=str_replace('.jpg', '_larger.jpg', $opt['color_value'])?>','',70,70)" onmouseout="mp_hidetrail()" style="border-bottom: none; border-top: none;padding-top:1px;padding-bottom:1px;float:left;" -->
						</a>
							<? 						
								
							}
							else
							{						
								?>
							<li class="<?=$product_id?>_options">
							<div style="display:none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
						<a class="color_block" href="javascript:void(0);" title="<?=$opt['value']?>">					
						<img class="no_reg" id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?>, '<?=$this_option_price_string?>',<?=$opt['color_id']?> );" border="0" src="/optionpics/<?=$opt['color_value'] ?>" style="padding-top: 0px; " onmouseover="mp_showtrail('/optionpics/<?=str_replace('.jpg', '_larger.jpg', $opt['color_value'])?>','',70,70)" onmouseout="mp_hidetrail()">
						</a>	
						<? }?>
						<? 
						if ($this_sale_data && !Sales::see_if_all_options_on_sale($product_id))
						{?>
							<span class="option_sale">SALE</span>		
						<? }
						if (($page_id) && $page_id == "products" && $options_vary_in_price)
							{?>						
							<div class="option_price"><?=$option_prices_string?></div>						
							<?}	
							?>
						</li>
						<?php 				
						}						
					 }
	    			 if (($counter == 4 || $counter == 5) && (!$page_id || $page_id != "products"))
					 {
					 	echo "</span>";
					 }
					?>
				</ul>
				<?php

				?>
			</div>															
	    <?
		} else {
		$colors_avail = 0;
		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
		}
		$class = $grid_view ? "options_grid_search" : "listing_available_colors_v1";
		?>
		<div class="<?= $class ?> inner-box color">
		<span>Select <?php echo $opt_name;?>:</span><select class="opts" id="option_<?=$product_id?>" name="option[<?=$product_id?>]" onChange="updateOptionValueDropdown('<?=$product_id?>', '<?=$options_vary_in_price ? $min_option_price . " - $" . $max_option_price : $option['base_prod_price']?>');">
		    <option value="">Options</option>		 
		    <?

		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($option['option_essensa_price'] && $option['option_essensa_price'] != '0.00' 
					&& (!($option['additional_price'] != '0.00' && $option['option_essensa_price'] > $option['additional_price']) 
					&& !($option['base_prod_price'] != '0.00' && $option['option_essensa_price'] > $option['base_prod_price'])
					&& !($option['product_essensa_price'] != '0.00' && $option['option_essensa_price'] > $option['product_essensa_price'])))
				{
						$this_option_price = $option['option_essensa_price'];
				}
				else if ($option['product_essensa_price'] && $option['product_essensa_price'] != '0.00' && 
					!($option['additional_price'] != '0.00' && $option['product_essensa_price'] > $option['additional_price'])  
					&& !($option['base_prod_price'] != '0.00' && $option['product_essensa_price'] > $option['base_prod_price'])) 
				{
						$this_option_price = $option['product_essensa_price'];
				}
				else if ($option['additional_price'] && $option['additional_price'] != '0.00') $this_option_price = $option['additional_price'];
				else if ($option['base_prod_price'] && $option['base_prod_price'] != '0.00') $this_option_price = $option['base_prod_price'];	
			}
			else $this_option_price = ($option['additional_price'] > 0) ? $option['additional_price'] : $option['base_prod_price'];
			$class_string = '';
			if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id, $option['id'])) 
			{
				$the_sale_data = Sales::get(0, true, $product_id, 'N', $option['id'], true, '', '', '', 'sale_price ASC');											if ($the_sale_data)
				if ($the_sale_data)
				{
					$this_sale_data = $the_sale_data[0];
					$this_option_price = $this_sale_data['sale_price'];
					$this_option_price_string = "Sale: $" . $this_sale_data['sale_price'];
					$class_string = "This item is on sale!<br><div class='sale_price'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
					$this_option_price_string = $class_string . $this_option_price_string;
				}
			}
			else $this_option_price_string = " $" . $this_option_price; 
			$option_prices_string = $option['value'] .  ": $". $this_option_price;
			    if( $p['inventory_limit'] == 'Y') {
				    //Check each option for inventory
				    $inv_o = (object) array();
				    $inv_o->product_id = $p['id'];

				    $inv_o->store_id = $CFG->default_inventory_location;
				    $inv_o->product_option_id = $option['id'];
				    $inv = Inventory::get1ByO( $inv_o );

				    if( $inv ){
					if( $inv['qty'] <= 0 ) {
					    continue;
					}
				    } else {
					    continue;
				    }
			    }
			    ++$colors_avail;		    
			?>												    
			<option id="<?=$this_option_price?>" class="<?=$class_string?>" value="<?=$option['id']?>" <?=$option['is_default'] == 'Y' ? 'selected="selected"' : ''?>><?=$option['value'] . " - ". $this_option_price_string?></option>
			<?
		    
		    
		}		    
		    ?>
		</select>
		</div>
		<?
	    }
	}
	
    static function getRequired($option){
        if($option['optional'] == 0 ){
            echo ' required="1" ';
        }
        else{
            echo ' required="0" ';
        }
        if($option['required_if_visible']){
            echo ' required_if_visible="1" ';
        }
    }

    static function getOptionContainer($option, $product_id){
        global $all_option_script;
        $current_group = self::$current_group;
        if($current_group != $option['group_name']){
            $class = $show = $btn_class = '';
            if($current_group != null){
                ?><!-- End container for option group <?php echo $current_group?> --></div><?php
            }
            $current_group = $option['group_name'];
            self::$current_group = $current_group;
            if($option['hide_group'] == '1'){
                $class = 'hidden_group';
                $show = '<span>Show</span>';
            }
            if($option['disable_group'] == '1'){
                $btn_class = 'disabled';
                $all_option_script .= " hidden_groups['" . $option['group_id'] . "'] = true;";
            }
            echo "<!-- Start container for option group: " . $current_group . "-->
            <span class='option_group_description'>" . ProductAvailableOptionGroups::getDescription($option['group_id']) . "</span>
            <button id='option_group_button_" . $option['group_id'] . "' class='btn btn-primary option_group_title $btn_class'>Add $current_group </button>
            <div class='option_group_container $class'  id='container_group_" . $option['group_id']. "'>";
        }
        $option_id = $option['option_label_id'];
        $parent_info = $option['parent_id'] == '' ? '' : 'data-child-row="' . $option['parent_id'] . '"';
        ?><div class='listing_available_colors_v1 option-selector-holder file-upload' id='option_select_<?php echo $option_id?>' <?php echo $parent_info?> data-label='<?php echo $option['option_name']?>'>
<?php
    }

    static function getOptionJS($option, $product_id, $show_tags = true){
        global $all_option_script;
        $info = Products::getOptionInfoForJs($option, $product_id);
        $all_option_script .= "option_info_obj[" . $option['id'] . "] = getOptionInfo('" . $info . "');" . PHP_EOL;
    }

    static function getOptionPrice($option){
        $add_price = 0;
        $price_label = '';
        if($option['additional_price'] > 0){
            if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember() && $option['essensa_additional_price']){
                $price_label = 'add $' . $option['essensa_additional_price'];
                $add_price = $option['essensa_additional_price'];
            }
            else{
                $price_label = 'add $' . $option['additional_price'];
                $add_price = $option['additional_price'];
            }
        }
        return array($add_price,  $price_label);
    }

    static function getOptionInfoForJs($option, $product_id){
        list($add_price, $label) = Products::getOptionPrice($option);
        $info = 'product_id=' . $product_id . '&option_value_id=' .$option['id'] . '&option_id=' . $option['option_label_id'] . '&optional_additional_price=' . $add_price . '&color_id=' . $option['color_id'] . '&option_name=' . $option['option_name']. '&text=' . $option['label'];
        return $info;
    }

    static function displayFileUploadOption($label, $option, $product_id){
        Products::getOptionContainer($option, $product_id);
        $value_id = $option['id'];
        $file_upload_id = 'file_upload_option_' . $value_id;
        ob_start();
        Products::getRequired($option);
        $required = ob_get_clean();
        ?>
        <input type='hidden' name='option[<?php echo $value_id?>]' id='option_select_<?php echo $value_id?>' <?php echo Products::getRequired($option) ?> data-label='<?php echo $option['label']?>' data-input-type="file"/> 
        <input type='hidden' id='option_customization_<?php echo $value_id?>' name='option_customization_<?php echo $value_id?>' data-<?php echo trim($required) ?> data-option-value-id='<?php echo $value_id?>' data-label='<?php echo $label?>' data-customization-type='file'/>
        <label for='option_customization_<?php echo $value_id?>' class="option_select_label"><?php echo $label ?></label>
<span class="btn btn-success fileinput-button" id='file_input_span_<?php echo $value_id?>'>
        <i class="glyphicon glyphicon-plus"></i>
        <span>Upload</span>
        <!-- The file input field used as target for the file upload widget -->
        <input  type="file" name="files" class='file_upload' 
        id='<?php echo $file_upload_id?>' data-url='ajax/ajax.product_info.php'
        data-form-data='{"option_value_id" :<?php echo $value_id?>, "action":"upload_files"}'
        data-label='<?php echo $label?>'/>
            </span>  
        </div>
         <?php  Products::getOptionJS($option, $product_id);
    }

    static function displayTextOption($label, $option, $product_id){
        global $all_option_script;
        Products::getOptionContainer($option, $product_id);
        $option_value_id = $option['id'];
        $additional_prices =Products::getOptionPrice($option); 
        if(strpos($_SESSION['preserve_options'][$product_id]['required'], $option['id']) > -1 || strpos($_SESSION['preserve_options'][$product_id]['optional'], $option['id']) > -1){ 
            $all_option_script .= 'populate_preserved[' . $option['option_label_id'] . '] = {option_type: "text", value:"' . $option['id'] . '", customize:"' . $_SESSION['preserve_options'][$product_id]['customizations'][$option['id']][1] . '"};';
    }
        ?><input type='hidden' name='option[<?php echo $option_value_id?>]' id='option_select_<?php echo $option_value_id?>' <?php Products::getRequired($option)?> data-label='<?php echo $label?>' data-input-type="text"/>
        <label for='option_customization_<?php echo $option_value_id?>' class="option_select_label text_option"><?php echo $label ?></label> <span class='text-option-price'>
        <?php
        if($option['additional_price'] > 0){
            echo $additional_prices[1];
        }
        else{
            echo '&nbsp;';
        }
        ?>    </span><?php
        $info = <<<EOS
        id='option_customization_$option_value_id' 
            name='option_customization_$option_value_id' 
            data-required='$required' 
            data-option-value-id='$option_value_id' 
            data-customization-type='text'
EOS;
        if($option['input_type'] == 'text'){ ?>
            <input type='text' <?php echo $info?>/>
           <?php 
        }
        elseif($option['input_type'] == 'textarea'){ ?>
            <textarea <?php echo $info?>></textarea>
            <?php
        } ?>
            </div><div class="clear_option_selects">&nbsp;</div> 
        <?php
        Products::getOptionJS($option, $product_id);
    }
    
    static function displayRadioOption($label, $options, $product_id){
        global $all_option_script;
        Products::getOptionContainer($options[0], $product_id);
 ?>     
        <label for='option_select_<?php echo $option['id']?>' class="option_select_label"><?php echo $label ?></label>
        <div class='radio-container'>
        <?php 
        $i = 0;
        foreach($options as $option){ 
            $option_value_id = $option['id'];
    ?>
        <input type='radio' value='<?php echo $option_value_id?>'  name='option_select_<?php echo $option['option_label_id']?>'
                id='option_select_<?php echo $option_value_id?>' 
                data-label='<?php echo $option['label']?>' <?php Products::getRequired($option)?> 
                <?php if ($i++ == 0){ echo 'checked';}?> data-input-type="radio"/>
            <label for='option_customization_<?php echo $option['id']?>'>
                <?php echo $option['label'] ?>
            </label>
            <?php if($option['additional_price'] > 0){ 
                $additional_prices = Products::getOptionPrice($option);
                echo "<span class='radio-option-price'>" .$additional_prices[1] . '</span>';
            }?>
<?php         Products::getOptionJS($option, $product_id);  
        if(strpos($_SESSION['preserve_options'][$product_id]['required'], $option['id']) > -1 || strpos($_SESSION['preserve_options'][$product_id]['optional'], $option['id']) > -1){ 
            $all_option_script .= 'populate_preserved[' . $option['option_label_id'] . '] = {option_type: "radio", value:"' . $option['id'] . '"};';
    }
        } ?> 
        </div></div><div class="clear_option_selects">&nbsp;</div>
    <?php   
    }

    static function displayCheckboxOption($label, $option, $product_id){
        global $all_option_script;
        Products::getOptionContainer($option, $product_id);
        ?>
        <label for='option_select_<?php echo $option['id']?>' class="option_select_label"><?php echo $label ?></label>
        <input type='checkbox' name='option[<?php echo $option['id']?>]' id='option_select_<?php echo $option['id']?>' value='<?php echo $option['id']?>' <?php Products::getRequired($option)?> data-label='<?php echo $label?>' data-input-type="checkbox"/>
        <?php if($option['additional_price'] > 0){ 
            echo "<span class='text-option-price'>add $" . $option['additional_price'] . '</span>';
        }?>
        </div><div class="clear_option_selects">&nbsp;</div> 
<?php           Products::getOptionJS($option, $product_id); 
        if(strpos($_SESSION['preserve_options'][$product_id]['required'], $option['id']) > -1 || strpos($_SESSION['preserve_options'][$product_id]['optional'], $option['id']) > -1){ 
            $all_option_script .= 'populate_preserved[' . $option['option_label_id'] . '] = {option_type: "checkbox", option_value:"' . $option['id'] . '"};';
    }
    }

    static function displayOptionSelects($label, $current_options, $product_id, $display_final_prices = false){
            global $all_option_script;
            $cur_oname = $name = $label;
            $all_option_script .= "option_id_to_names[" . $current_options[0]['option_label_id'] . "] = '" . $name . "';";
            $count = 0;
            $selected_index = $selected_value = 0;
            foreach($current_options as $option){
                if($count++ == 0){
                    $current_oid = $option['option_label_id'];
                    Products::getOptionContainer($option, $product_id);
                    ?>
                    <input type="hidden" class="hidden-option-input" 
                    name="option[<?=$current_oid?>]" value="" 
                    id="option_<?=$current_oid?>" <?php Products::getRequired($option); ?>
                    data-option-type="<?=$name?>" data-input-type="select"
                    data-label='<?php echo $label?>' />
                   <label for="option_select_<?php echo $current_oid?>" class='option_select_label'><?php echo $name?></label>
                    <select id="option_select_<?=$current_oid?>" 
                    class="opts" onChange="updateAvailableOption('<?=$product_id?>', '<?php echo $current_oid?>')" >
                    <option value="product_id=<?php echo $product_id?>&option_id=<?php echo $current_oid?>&option_value_id=unselected">Please Select</option>
                        <?php
                }
                $info = Products::getOptionInfoForJs($option, $product_id);
                Products::getOptionJS($option, $product_id, false); 
                list($add_price, $price_label) = Products::getOptionPrice($option);
                if($display_final_prices){
                    $price_row = self:: getPriceAndIdForOptions($product_id, ',' . $option['id'] . ',');
                    if(isset($price_row['price']) && (float)$price_row['price'] > 0.00){
                        $price_label .= '$' . $price_row['price'];
                    }
                }
                ?>	<option id="<?=$add_price?>" 
                    class="" value="<?=$info?>"
                    data-description="<?=$price_label?>" 
                    data-option_value_id="<?php echo $option['id']?>"
                    <?php echo ProductOptionCache::getColorCodeForDropDown($option['color_id']);?>
                    <?=$option['is_default'] == 'Y' ? 'selected="selected"' : ''?> >
                    <?=$option['label']?></option>
               <?php
                if(strpos($_SESSION['preserve_options'][$product_id]['required'], $option['id']) > -1 || strpos($_SESSION['preserve_options'][$product_id]['optional'], $option['id']) > -1){ 
                    $selected_index = ($count +1);
                    $selected_value = $option['id'];
                }
            }
            ?></select><br style='clear:both;'/></div><div class="clear_option_selects">&nbsp;</div>
            <?php
            if($selected_index > 0){
            $script .= ' populate_preserved[' . $current_oid . '] = {option_type: "select", value: "' . $selected_value . '", index:"' . $selected_index . '"}; ';
            }
            return $script;
    }

    static function optionPricesVary($product_id){
        ProductOptionCache::setProductOptionCache($product_id);
        return $_SESSION['prices_vary'][$product_id];
    }

	static function displayDynamicOptionDropdown($product_id){
        global $CFG, $all_option_script;
        self::$current_group = null;
        $product_options = ProductAvailableOptions::getForProductPage($product_id);
		$class = $grid_view ? "options_grid_search" : "listing_available_colors_v1";
        $current_oname =  '';
        $all_option_script = "<script type='text/javascript'>
        var option_id_to_names = {}; 
        var this_page_product_id = $product_id;
        var option_info_obj = {};
        var populate_preserved = {}; 
        var hidden_groups = {};
        var show_groups_on_load = {};
        var show_file_buttons_on_load = {};
        var show_file_preview_on_load = {};
";
        $is_file = false;
        $display_final_prices = false;
        if(Products::OptionPricesVary($product_id) && sizeof($product_options == 1)){
            $display_final_prices = true;
        }
	    foreach($product_options as $name => $current_options){
            switch($current_options[0]['input_type']){
                case  'checkbox':
                    Products::displayCheckboxOption($name, $current_options[0], $product_id);
                    break;
                case 'file':
                    $is_file = true;
                    Products::displayFileUploadOption($name, $current_options[0], $product_id);
                    break;
                case 'text':
                case 'textarea':
                    Products::displayTextOption($name, $current_options[0], $product_id);
                    break;
                case 'select':
                    $all_option_script .= Products::displayOptionSelects($name, $current_options, $product_id, $all_option_script, $display_final_prices);
                    break;
                case 'radio':
                    Products::displayRadioOption($name, $current_options, $product_id);
                    break;
            }
        }

        if(!is_null(self::$current_group)){
            echo '<!-- End container for ' . self::$current_group . '--></div><div class="clear_option_selects">&nbsp;</div>';
        }
        if($is_file){
            ?>
            <div id='upload_container'>
            <div id='files'></div>
            <div id='progress' class='progress'>
                <div class='progress-bar progress-bar-success'></div>
            </div>
            </div>
            <?php
            $all_option_script .= "</script>
        <script type='text/javascript' src='js/load-image.all.min.js'>        </script>
        <script type='text/javascript' src='js/canvas-to-blob.min.js'>        </script>
        <script type='text/javascript' src='js/jquery.iframe-transport.js'>        </script>
        <script type='text/javascript' src='js/jquery.fileupload.js'>        </script>
        <script type='text/javascript' src='js/jquery.fileupload-process.js'>        </script>
        <script type='text/javascript' src='js/jquery.fileupload-image.js'>        </script>
        <script type='text/javascript' src='js/jquery.fileupload-validate.js'>        </script>
        <link rel='stylesheet' href='css/jquery.fileupload.css'>
        <script type='text/javascript'>
        \$j(document).ready(function () {
            initFileUploads();
    }); ";
        }
        
        $preserves_to_display = array('open_groups' => 'show_groups_on_load', 'open_file_buttons' => 'show_file_buttons_on_load', 'file_previews' => 'show_file_preview_on_load');
        foreach($preserves_to_display as $ses_var =>$js_var){
 //           echo 'going to check: ' . $ses_var . PHP_EOL;
            if(isset($_SESSION['preserve_options'][$product_id][$ses_var])){
   //         echo $ses_var . ' is set. ' . PHP_EOL;
            foreach($_SESSION['preserve_options'][$product_id][$ses_var] as $curr => $val){
            $all_option_script .=  "{$js_var}['{$curr}'] = \"$val\"; ";
        
            }
        }
        }
        if(isset($_SESSION['preserve_options'][$product_id]['current_product_option'])){
            $all_option_script .= 'current_product_id = ' . $_SESSION['preserve_options'][$product_id]['current_product_option'] . '; ';
        }
        $all_option_script .= "
        \$j(document).ready(function () {
            \$j('.price-info-holder .cart-box .option_group_title').click(function(){
                toggleOptionGroup(\$j(this));
            });
    \$j('input[type=\"text\"][id^=\"option_customization\"], input[type=\"radio\"][id^=\"option_customization\"], textarea[id^=\"option_customization\"]').on('change',
        function(){ 
            var target = \$j('#option_select_' + \$j(this).attr('id').replace(/option_customization_/, ''));
            var val = \$j(this).data('option-value-id');
            if(\$j(this).val() != ''){
                target.val(val);
            }
            else{
                target.val('');
            }
            target.change();
            toggleDependentChildren(val);
        });
         //selects are dealt with differently, only set up inputs
         \$j('input[id^=option_select_]').change(function(){processOptionChange(this.id);});
        populatePreservedOptions();
    }); </script>";
}

	static function displayOptionDropdown( $product_id , $page_id=null, $grid_view = false, $print_page = false)
	{
		global $CFG;
		 
		if (!$page_id){
			$p=Products::get1($product_id);
		}
		$product_options = Products::getProductOptions( 0, $product_id );
		$options_vary_in_price = Products::doOptionsVaryInPrice($product_options);
		if ($options_vary_in_price)
		{
			$min_option_price = Products::getOptionsMinPrice($product_options);
			$max_option_price = Products::getOptionsMaxPrice($product_options);
		}
		foreach( $product_options as $key => $opt )
		{
			$color_q = new OptionColorMapQuery();
	
			if($opt['color_id']){
				$color_q->id = $opt['color_id'];
			} else {
				$color_q->name = $opt['value'];
			}
	
			$color_map = OptionColorMap::get1ByO( $color_q );
	
			if( $color_map )
			{
				$product_options[$key]['color_value'] = $color_map['color_value'];
				$product_options[$key]['option_type'] = $color_map['option_type'];
			}
		}
		
		$colors_avail = 0;
		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
		}
		$class = $grid_view ? "options_grid_search" : "listing_available_colors_v1";
		
		if($print_page){
			?>
			<ul class="print-option-list <?=strtolower($opt_name)?>">
		    <?
		}else{
			?>
			<div class="<?= $class ?> option-selector-holder">
			<input type="hidden" class="hidden-option-input" name="option[<?=$product_id?>]" value="" id="option_<?=$product_id?>" data-option-type="<?=$opt_name?>"/>
			<select id="option_select_<?=$product_id?>" class="opts" onChange="updateOptionValueDropdown('<?=$product_id?>', '<?=$options_vary_in_price ? $min_option_price . " - $" . $max_option_price : $option['base_prod_price']?>');">
		   <!--  <option value="<?=$product_id?>">Select <?php echo $opt_name;?>:</option>		 --> 
			<?php 
		}

		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
			if ($_SESSION['cust_acc_id'] && Customers::seeIfEssensaMember())
			{
				if ($option['option_essensa_price'] && $option['option_essensa_price'] != '0.00' 
					&& (!($option['additional_price'] != '0.00' && $option['option_essensa_price'] > $option['additional_price']) 
					&& !($option['base_prod_price'] != '0.00' && $option['option_essensa_price'] > $option['base_prod_price'])
					&& !($option['product_essensa_price'] != '0.00' && $option['option_essensa_price'] > $option['product_essensa_price'])))
				{
						$this_option_price = $option['option_essensa_price'];
				}
				else if ($option['product_essensa_price'] && $option['product_essensa_price'] != '0.00' && 
					!($option['additional_price'] != '0.00' && $option['product_essensa_price'] > $option['additional_price'])  
					&& !($option['base_prod_price'] != '0.00' && $option['product_essensa_price'] > $option['base_prod_price'])) 
				{
						$this_option_price = $option['product_essensa_price'];
				}
				else if ($option['additional_price'] && $option['additional_price'] != '0.00') $this_option_price = $option['additional_price'];
				else if ($option['base_prod_price'] && $option['base_prod_price'] != '0.00') $this_option_price = $option['base_prod_price'];	
			}
			else $this_option_price = ($option['additional_price'] > 0) ? $option['additional_price'] : $option['base_prod_price'];
			$class_string = '';
			
			if (Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id, $option['id'])) 
			{
				
				$this_option_price_string = "Sale: $" . $this_option_price;
// 				list($dollar_part,$cent_part) = explode('.', $this_option_price);
// 				$this_option_price_string = "<div class=\'sale_price_label\'>Sale</div> <span itemprop=\'price\'>$".$dollar_part.".<sup>".$cent_part."</sup></span>";
				
				$the_sale_data = Sales::get(0, true, $product_id, 'N', $opt['id'], true, '', '', '', 'sale_price ASC');
				if ($the_sale_data)
				{
					$this_sale_data = $the_sale_data[0];
// 					$class_string = "<span class=\'sale-item-img\'></span>";
					//$class_string = "This item is on sale!<br><div class=\'sale_price\'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
					$this_option_price_string = $class_string . $this_option_price_string;
				}
				
// 				$the_sale_data = Sales::get(0, true, $product_id, 'N', $option['id'], true, '', '', '', 'sale_price ASC');											if ($the_sale_data)
// 				if ($the_sale_data)
// 				{
// 					$this_sale_data = $the_sale_data[0];
// 					$this_option_price = $this_sale_data['sale_price'];
// 					$this_option_price_string = "Sale: $" . $this_sale_data['sale_price'];
// 					$class_string = "This item is on sale!<br><div class='sale_price'>Our Price: $" . $this_sale_data['old_price'] . "</div>";
// 					//$this_option_price_string = $class_string . $this_option_price_string;
// 				}
			}
			else $this_option_price_string = " $" . $this_option_price; 
			$option_prices_string = $option['value'] .  ": $". $this_option_price;
		    if( $p['inventory_limit'] == 'Y') {
			    //Check each option for inventory
			    $inv_o = (object) array();
			    $inv_o->product_id = $p['id'];

			    $inv_o->store_id = $CFG->default_inventory_location;
			    $inv_o->product_option_id = $option['id'];
			    $inv = Inventory::get1ByO( $inv_o );
			    if( $inv ){
				if( $inv['qty'] <= 0 ) {
				    continue;
				}
			    } else {
				    continue;
			    }
		    }
		    ++$colors_avail;

		    $bgcolor = $img_src = '';
		    if($color_map){
			    if ($option['option_type'] == "color")
			    {
			    	$bgcolor = $option['color_value'];  
			    }else{
			    	$img_src = "/optionpics/" . $option['color_value']; 
				}		
			}
		    $info = 'product_id='.$product_id.'&option_id='.$option['id'].'&color_id='.$option['color_id'].'&option_price='.$class_string . $this_option_price_string;
			if($option['product_case_price_percent']>0){
				$info .= '&option_case_price=$'. number_format($this_option_price * (1-$option['product_case_price_percent']),2);			
			}
			
			if($print_page){
				?>
				<li class="option-item">
					<?php 
					if($img_src || $bgcolor){
						?>
						<img class="dd-option-image" src="<?=$img_src?>" style="<?= 'background:'.$bgcolor?>">
						<?php 
					}
					?>
					<label class="dd-option-text" style="cursor:pointer;"><?=$option['value']?></label>
					<small class="dd-option-description dd-desc"><?= $class_string . $this_option_price_string ?></small>
				</li>
				<?php 
// 				$option_count++;
// 				if($option_count % 3 == 0){
// 					echo "</ul><ul class='print-option-list'>";
// 				}  
			}else{
			    ?>	
				<option id="<?=$this_option_price?>" class="<?=$class_string?>" value="<?=$info?>"  data-imagesrc="<?=$img_src?>" data-bgcolor="<?=$bgcolor?>" data-description="<?=$this_option_price_string?>" <?=$option['is_default'] == 'Y' ? 'selected="selected"' : ''?>><?=$option['value']?></option>
				<?
			}
		}
		if($print_page){		    
			?>
			</ul>
			<?php 
		}else{
			?>
			</select>
			</div>	
			<?php 
		}
	}
	


    static function getPriceAndIdForOptions($product_id, $selected_values, $is_essensa = false){
        if(!isset($_SESSION['product_cache'][$product_id])){
            ProductOptionCache::setProductOptionCache($product_id);
        }
        $row = $_SESSION['product_options_by_set'][$selected_values];
        $price_field = $is_essensa ? 'essensa_price' : 'price';
        return array('id' => $row['option_id'], 
            'case_price' => $row['case_' . $price_field],
            'case_price_per_piece' => $row['case_' . $price_field . '_per_piece'],
            'price' => $row[$price_field]);
    }

    static function getPriceForOptionalOptions($selected_values, $is_essensa, $product_id = 0){
        if($product_id){
            ProductOptionCache::setProductOptionCache($product_id);
        }
        $total = 0;
        $all_values = explode(",", $selected_values);
        $price_field = $is_essensa ? 'essensa_price' : 'price';
        foreach($all_values as $curr){
            if(!is_numeric($curr)|| isset($_SESSION['related_products'][$curr])){
                continue;
            }
            $total += $_SESSION['optional_options'][$curr][$price_field];
        }
        return $total;
    }
    
	static function compareRedesign($items, $cat_id)
	{
		global $CFG;
		$numb_products = count($items);
		$products = array();
		$filter_names = array();
		$i = 0;
		$have_height = $have_width = $have_length = false;
		if( is_array( $items ) )
		{
			foreach( $items as $item )
			{
				$products[$i] = Products::get1( $item );
				$products[$i]['filters'] = Products::getProductFilterValues( $item );
				foreach ($products[$i]['filters'] as $the_filter)
				{
					$filter_id = $the_filter['filter_id'];
					if (!array_key_exists($filter_id, $filter_names)) $filter_names[$filter_id]['name'] = $the_filter['name'];
					
				}
				
				if ($products[$i]['height'] > 0) $have_height = true;
				if ($products[$i]['width'] > 0) $have_width = true;
				if ($products[$i]['length'] > 0) $have_length = true;
				
				uasort($filter_names, 'sort_array_by_name');				

				$i++;
			}
		}
		if ($have_length)$filter_names[]['name'] = "Length";
		if ($have_width)$filter_names[]['name'] = "Width";
		if ($have_height) $filter_names[]['name'] = "Height";
		?>
		<div class="wrapper twocolumns-page">
		<div id="content">
		<form action="#" class="compare-form">
			<fieldset>
				<h1>Product Comparison</h1>
				<div class="table-box">
					<table class="compare-table">										
					<tr>
					<th>Product</th>					
					<? for( $i = 0; $i < $numb_products; $i++ )
					{?>
						<th><a class='prod-name-link' href="<?=Catalog::makeProductLink_( $products[$i])?>"><?=$products[$i]['name']?></a></th>
				 <? } ?>
					</tr>
					<tr>
					<td>Image</td>
					<? 		
					foreach( $products as $p )
					{
						$path = Catalog::makeProductImageLink( $p['id']);		    
		    		?>
					<td>
						<div class="img-box">
						<a href="<?=Catalog::makeProductLink_( $p )?>">
						<img src="<?=$path?>" alt="<?=$p['name']?>" />
						</a>
						</div>
					</td>								
		    	<?	} ?>
					</tr>	
					
					<tr class="even">
					<td>Price</td>
					<? 
					for( $i = 0; $i < $numb_products; $i++ )
					{
						$prices_to_print_array = Products::getPriceToDisplayForProduct($products[$i]['id']);
						?>
						<td><?=$prices_to_print_array['reg_price_to_print']?></td>
				 <? } ?>
					</tr>
					
					<tr>
					<td>Model</td>
					<? 
					for( $i = 0; $i < $numb_products; $i++ )
					{?>
						<td><?=$products[$i]['mfr_part_num']?></td>
				 <? } ?>
					</tr>
					<tr class="even">
					<td>Brand</td>
					<? 
					for( $i = 0; $i < $numb_products; $i++ )
					{?>
						<td><?=$products[$i]['brand_name']?></td>
				 <? } ?>
					</tr>
					<? 
					 $counter = 0;
                     $at_least_one_has_rating = false;
                     for( $i = 0; $i < $numb_products; $i++ )
					 {   
						$rating = Product_Ratings::get_rating_for_product($products[$i]['id']);
						if ($rating) $at_least_one_has_rating = true;
					 }
					 if ($at_least_one_has_rating)
					 {  
					 	$counter++;
						if ($counter %2 == 0 ) $tr_class_text = ' class="even"';
						else $tr_class_text = "";
					 ?>
					<tr <?=$tr_class_text ?>>
                       <td>Rating</td>
                    <?    
					for( $i = 0; $i < $numb_products; $i++ )
					{?>
						<td>                       
					<?     
						$rating = Product_Ratings::get_rating_for_product($products[$i]['id']);
    					$number_of_stars = 5;

                		if ($rating)
                		{
                		?>              
                 			<ul class="rating">
                 			<?
                 			for ($j=1;$j <= 5; $j++)
                 			{                 				
                 			?>
              			<li <?=ceil($rating['percentage']/100*$number_of_stars) == $j ? ("class='active' style='width:". $rating['percentage']*$number_of_stars/100 * 18 . "px;'") : ''?> >
              			</li>
              			<? } ?>
              			</ul>
                 		<?
                		}?>
                		</td>
                	<? } ?>
                	</tr>
                	<? } 
                	$counter++;
					if ($counter %2 == 0 ) $tr_class_text = ' class="even"';
					else $tr_class_text = "";
					?>					
					<tr <?= $tr_class_text ?>>
					<td>Availability</td>
					<? 
					for( $i = 0; $i < $numb_products; $i++ )
					{
						$can_buy_info = Products::getCanBuyInfo($products[$i], $CFG);
    					$can_buy = $can_buy_info['can_buy'];
					?>
						<td><?=$can_buy ? "In Stock" : "Out of Stock"?></td>
				 <? } ?>
					</tr>							
				<? 												
					// Now show filters					
					foreach ($filter_names as $filter_id=>$spec)
					{
						$counter++;
						if ($counter %2 == 0 ) $tr_class_text = ' class="even"';
						else $tr_class_text = "";				
					?>		 
						<tr <?=$tr_class_text ?>>
						<td><nobr><?= $spec['name']?></nobr></td>
					<? 
						if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
				    		trim(strtoupper($spec['name'])) == 'HEIGHT' ||
				    		trim(strtoupper($spec['name'])) == 'LENGTH'  )
						{
							for( $i = 0; $i < $numb_products; $i++ )
							{
								echo'<td>';
								echo Products::displayDimension( $spec['name'], 'product', $products[$i]['id'], true);
								echo'</td>';
							}
						}
						else
						{
							for( $i = 0; $i < $numb_products; $i++ )
							{
							    if( $products[$i]['filters'][$spec['filter_id']]['value'] === 0)
							    {
									$products[$i]['filters'][$spec['filter_id']]['value'] = 'N/A';
							    }
								echo'<td>' . $products[$i]['filters'][$filter_id]['value'] . '</td>';
							}
						}
						echo'</tr>';			
					}					
					$counter++;
					if ($counter %2 == 0 ) $tr_class_text = ' class="even"';
					else $tr_class_text = "";
					?>
					<tr <?=$tr_class_text ?>>
					<td>Weight</td>
					<? 
						for( $i = 0; $i < $numb_products; $i++ )
						{?>
							<td><?=weight::display($products[$i]['weight'])?></td>
				 	 <? } ?>				
                    </tr>
                    <tr>
					<td></td>
					<? 
					for( $i = 0; $i < $numb_products; $i++ )
					{
						$can_buy_info = Products::getCanBuyInfo($products[$i], $CFG);
    					$can_buy = $can_buy_info['can_buy'];
    					$can_buy_limit = $can_buy_info['can_buy_limit'];										
				  	?>
				  	<td>
				  	<? 	
    					if ($can_buy) 
                    	{
                         	$param_sku = addcslashes($products[$i]['vendor_sku'], '"');
                         	$param_price = number_format( $products[$i]['price'], 2 );
                         ?>                                                                
                           	<input class="quantity_new" name="quantity[<?=$products[$i]['id']?>]" type="text" size="3" value="1" maxlength="3" onfocus="if(this.value == '0'){this.value='';}"/>
    						<a href='#' class="btn-add" onclick='return cat_add_one_to_cart( "<?= $products[$i]['id'] ?>", "<?= $param_sku ?>", "<?= $param_price ?>", "<?= $path ?>", "<?= $can_buy_limit ?>" )';return false;>
    						<span>Add to Cart</span>
							</a>    													                                 
					<? }
					else 
					{?>
						<span>Currently out of stock.</span>
						<span class="compare-os"><a class="stock_ava" href='#' onClick='javascript:window.open("/request_stock_notification.php?product_id=<?=$p['id']?>","RequestStockNotification","resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=400,height=280,status");return false;'>Notify me when available</a></span>
					<? } ?>
					</td>
					<? } ?>
					</tr>
					<? 
					if ($numb_products > 2)
					{ ?>
					<tr>
					<td></td>
					<? 
						for( $i = 0; $i < $numb_products; $i++ )
						{?>
							<td><span class="compare-os"><a href='#' onClick="remove_from_compare('<?=$products[$i]['id']?>');return false;">Remove</a> from compare</span></td>
				 	 <? } ?>
					</tr>
				 <? } ?>                            
			</table>
			</div>
			<div class="btn-row">
					<a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn-back">Back to Category</a>
				</div>
			</fieldset>
		</form>
	</div>
</div>
		<?	
	}
	
	function calculate_financing($price){
		global $CFG;
		if($price > 5000){
			return ceil($price * $CFG->financing_factor_over_5k);
		}else{
			return ceil($price * $CFG->financing_factor_under_5k);
		}
	}
	
	function getCatSymbolicSubCats($cat_id){
		
		$sql = "	SELECT symbolic_cats.cat_id 
					FROM  `symbolic_cats`
					LEFT JOIN cats ON symbolic_cats.cat_id = cats.id
					LEFT JOIN (
						SELECT cat_id, COUNT( product_id ) AS cat_product_count
						FROM product_cats
						GROUP BY cat_id
					) AS cat_product_counts ON cat_product_counts.cat_id = symbolic_cats.cat_id
					WHERE symbolic_cats.pid >0 
				";
		if(is_array($cat_id)){
			$sql .= " AND cats.id IN (" . implode(',', $cat_id) .")";
		}else{
			$sql .= " AND cats.id = " . $cat_id;
		}
		return db_query_array($sql);
	}
	
	function validateProduct( $id )
	{
		/*
		 * need to validate 2 things
		* 1) make sure that all categories that are selected are to the lowest level
		* 2) make sure all required filters are filled.
		*/
		$errors = array();
	
		$product = Products::get1( $id );
	
		$prod_cats[] = Products::getProductCats( $id );
		$prod_cats = array_shift( $prod_cats );
		
		foreach ($prod_cats as $cat){
			$prod_cats_list[] = $cat['cat_id'];
		}
		//var_dump($prod_cats_list);
		//echo implode(',', $prod_cats_list);
		
		if($prod_cats){
			$product_symbolic_cats = Products::getCatSymbolicSubCats($prod_cats_list);
		}

		if(!is_array($prod_cats)){$prod_cats = array();} 
		if(!is_array($prod_cats)){$product_symbolic_cats = array();}
		
		$prod_cats = array_merge($prod_cats,$product_symbolic_cats);
		//$prod_cats = array_unique_multidimensional($prod_cats);
	
		if($product['is_active']=='Y' && count($prod_cats)==0){
			$errors[] = 'This product is active and no categories are selected. Please select categories.';
		}
		//print_ar( $prod_cats );
	
		$cat_list = array();
		if( $prod_cats ){
			foreach( $prod_cats as $cat )
			{
				$cat_list[] = $cat['cat_id'];
				$which_cat = Cats::get1( $cat['cat_id'] );
			}
		}
		//print_ar($cat_list);
		if( $cat_list ){
			$cat_id_list = implode(',', $cat_list);
			$subs = Cats::get(0, '','','','',$cat_id_list );
	
			if( $subs )
			{
				//print_ar($subs);
				$found = false;
				foreach( $subs as $sub_cat )
				{
					if( in_array( $sub_cat['id'], $cat_list ) ) //only 1 of the subs need be there
					{
						$found[] = $sub_cat['pid'];
					}
				}
				//print_ar($found);
				if($found){
					//check which have subs (lowest level cats wont have subs)
					foreach($cat_list as $cat)
					{
						if(in_array( $cat, $found ) === false){
							$not_found[] = $cat;
						}
					}
				}else{
					$not_found = $cat_list;
				}
				//print_ar($not_found);
				//we have cats that don't have subcats
				if(is_array($not_found)){
					$not_found_ids = implode(',', $not_found);
					$subs = Cats::get(0, '','','','',$not_found_ids );
	
					//get pid of subcats whose parent doesn't have a subcat chosen
					foreach ($subs as $sub_cat){
						$error_cats[] = $sub_cat['pid'];
					}
	
					$error_cats = array_unique($error_cats);
					foreach($error_cats as $cat)
					{
						$which_cat = Cats::get1( $cat );
						$errors[] = "Please make sure the lowest level is selected for each category, " . $which_cat['name'] . " - needs lower category.";
					}
				}
			}
	
		}
	
		$req_filters = array();
	
		$values = Products::getAllProductValues( $id );
		$first = true;
		//Filters are no longer requiered at all.
		//	if( $values )
			//		foreach( $values as $v )
				//		{
				//			if( $v['display'] == 'Y' )
					//			{
					//				if( trim(strtoupper($v['name'])) == 'WIDTH' ||
					//					trim(strtoupper($v['name'])) == 'HEIGHT' ||
					//					trim(strtoupper($v['name'])) == 'LENGTH' )
						//					{
						//						continue;
						//					}
						//				$prod_filter = Products::getProductFilter( $id, $v['filter_id'] );
						//				if( !$prod_filter['value'] )
							//				{
							//					if( $first )
								//					{
								//						$errors[] = "You must fill out all filter and compare values with an asterisk (*): ";
								//						$first = false;
								//					}
								//					$errors[] = "Please fill in a value for " . $v['name'];
								//				}
								//			}
								//
								//		}
		if( is_array( $errors ) )
			return $errors;
		else
			return true;
	}
	
	function get_product_description_text($description){
		$description = str_replace ( "<ul>", "", $description );
		$description = str_replace ( "</ul>", "", $description );
		$description = str_replace ( "<li>", " ", $description );
		$description = str_replace ( "</li>", ",", $description );
		$description = trim ( preg_replace ( '/\s\s+/', ' ', $description ) );
		if (substr ( $description, - 1 ) == ',') {
			$description = substr ( $description, 0, - 1 );
		}
		$description = StdLib::strip_html_tags ( $description );
		return $description;
	}
	
	function setMinQty(&$product){

		//if the brand has a fee greater than $5 
		if($product['brand_min_fee'] > 5 && $product['exclude_min'] == 'N'){
			$product['min_qty'] = ceil($product['brand_min_amount'] / $product['vendor_price']);
		}
		
		if($product['min_qty']){
			$product['name'] .= ' [Minimum Order: '.$product['min_qty'].']';
		}
		return $product;
	}
	
	function recently_viewed_products( $product_id, $limit = 16 ) {
	
		global $CFG;
	
		$cookie_name = $CFG->site_name . '_recently_viewed';
			
		// Get recently viewed product cookies data
		$viewed_products_ids = !empty( $_COOKIE[$cookie_name] ) ? (array) explode( '|', $_COOKIE[$cookie_name] ) : null;
		
		if(is_array($viewed_products_ids)){
			//get prodcuts to show - exclude current prodcut
			$product_ids = $viewed_products_ids;
			$current_product_id = array_search($product_id, $product_ids);
			unset($product_ids[$current_product_id]);
			if( count($product_ids) > 0 ){
				$viewed_products = products::get($product_ids,'','','','',0,'Y','','',0,0,0,0,0,0,0,0,0,'',false,false,'',false,'','','',$limit);
			}			
		}
		
		// Add current product to the cookie
		if(!is_array($viewed_products_ids) || in_array($product_id,$viewed_products_ids) === false ){
			$viewed_products_ids[] = $product_id;
		}		
		
		$viewed_products_ids = array_unique($viewed_products_ids);
		
		// If we have more that $limit products - remove the oldest
		if(count($viewed_products_ids) > $limit){
			array_shift($viewed_products_ids);
		}
				
// 		setcookie( $cookie_name, implode('|', $viewed_products_ids), time()+60*60*24*30, '/', "dev1.lionsdeal.tigerchef.com");
		list($first,$url) = explode('://',$CFG->baseurl);
		setcookie( $cookie_name, implode('|', $viewed_products_ids), time()+60*60*24*30, '/', trim($url,'/') );
		
		return $viewed_products;
	}
	
	function getBundleComponents($product_id,$component_product_id='',$component_product_option_id=''){
		$sql = "SELECT product_bundle_components.*, products.name , brands.name as brand_name,
                    IF( product_options.vendor_sku IS NULL, products.vendor_sku, product_options.vendor_sku) AS vendor_sku
                FROM product_bundle_components
				LEFT JOIN products ON products.id = component_product_id
				LEFT JOIN product_options ON product_bundle_components.component_product_option_id = product_options.id
				LEFT JOIN options ON options.id = product_options.option_id
				LEFT JOIN brands ON brands.id = products.brand_id"; 
		 
		$sql .= " WHERE bundle_product_id = {$product_id} ";
		if($component_product_id){
			$sql .= " AND component_product_id = {$component_product_id} ";
		}
		if($component_product_option_id){
			$sql .= " AND component_product_option_id = {$component_product_option_id} ";
		}
		return db_query_array($sql);
	}
	
	function get1BundleComponent($product_id,$component_product_id,$component_product_option_id=''){

		$result = Products::getBundleComponents($product_id,$component_product_id,$component_product_option_id);
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	
	function insertBundleComponent($info){
		return db_insert_or_update('product_bundle_components',$info,'','',true); 
	}
	function updateBundleComponent($info){
		return db_insert_or_update('product_bundle_components',$info,'','',true);
	}
	function deleteBundleComponent($info){
		db_query("DELETE FROM product_bundle_components 
					WHERE bundle_product_id = {$info['bundle_product_id']} AND component_product_id = {$info['component_product_id']} AND component_product_option_id = {$info['component_product_option_id']} ");
		return db_affected_rows();
	}
	
	
}
function sort_array_by_name($a, $b)
{
   	return strcmp($a['name'], $b['name']);
}

function array_cartesian_product($arrays)
{
    $result = array();
    $arrays = array_values($arrays);
    $sizeIn = sizeof($arrays);
    $size = $sizeIn > 0 ? 1 : 0;
    foreach ($arrays as $array)
        $size = $size * sizeof($array);
    for ($i = 0; $i < $size; $i ++)
    {
        $result[$i] = array();
        for ($j = 0; $j < $sizeIn; $j ++)
            array_push($result[$i], current($arrays[$j]));
        for ($j = ($sizeIn -1); $j >= 0; $j --)
        {
            if (next($arrays[$j]))
                break;
            elseif (isset ($arrays[$j]))
                reset($arrays[$j]);
        }
    }
    return $result;
}

?>
