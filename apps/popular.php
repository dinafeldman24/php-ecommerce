<?php
//include '../application.php';
//include($CFG->dirroot . "/apps/cats.php");
/**
 * Class for building objects for the Popular Categories in our shop.
 *
 */
class Popular  {

	public $limit;
	public $id;
	public $name;
	public $desc;
	public $image;
	public $products;
	public $isPopular;
	public $resource;

	/**
	 *
	 */
	function __construct($cats, $limit=8) 
	{
		//Need to design logic for creating popular categories.  
		//Right now this will be done using what categories have the most
		//child categories.
		$this->limit = $limit;
		//Query the database to find parent cats.
		//$ids = Cats::getParentCats(0);
		
		if(!$cats)
		{
			$this->isPopular = false;
			
		} else {
			$this->resource = $cats;
			$this->isPopular = true;
		}
		
	}
	
	protected function makePopular($limit)
	{
		//SQL statement for determining which 8 categories have the most products in their category.		
		$sql = sprintf("SELECT count(p.product_id), p.cat_id, c.id, c.name, c.cat_desc, c.url_name
			FROM product_cats p, cats c
			WHERE p.cat_id = c.id 
			GROUP BY p.cat_id
			ORDER BY count(p.product_id) 
			DESC LIMIT %u", $limit);
		
		$query = db_query($sql);	
		
		$result = db_combine_results($query);
		
		$this->resource = $result;
	    
	}
	
	protected function getPopular()
	{
	    //Changed this, since you have already passed the cats to the class on creation, no reason to grab them again, just need some counts.

		foreach( $this->resource as $key=>$value )
		{
		    $sub_prods = Products::getProductsForCat( $value['id'], 0, 0, 0, 0, 0, 0, 'products.tagline, products.name', '', false,true,'','','N' );
                    $this->resource[$key]['count'] = $sub_prods[0]['total']; // count($sub_prods);// $sub_prods[0]['total'];
		}

		//Get id's for popular cats
//		$popSql = sprintf("SELECT c.id, c.name, c.cat_desc, c.url_name, count(p.product_id)
//			FROM cats c, product_cats p
//			WHERE c.is_popular = 'Y' AND c.id = p.cat_id
//			GROUP BY c.id
//			ORDER BY count(p.product_id)
//			DESC LIMIT %u", $limit);
//
//		$popQuery = db_query($popSql);
//
//		$popResult = db_combine_results($popQuery);
//
//		$this->resource = $popResult;
	}
	
	private function buildPopular()
	{
		echo "<h2><a style='color: #e33e25;' href='/restaurant-equipment.html'>Popular Restaurant Equipment Categories</a></h2>";

		foreach( $this->resource as $popCat )
		{
			$popCat['pid'] = $popCat['id'];
		    ?>
			<div class="pop_featured_items_box">
				<div class="pop_iner" onmouseover="borderButton(<?=$popCat['id']?> );" onmouseout="clearButton(<?=$popCat['id']?>);">
					<a href="<?=Catalog::makeCatLink($popCat)?>">
					    <img src="<?=Catalog::makeCategoryImageLink( $popCat['id'], false, true )?>" alt="<?=$popCat['name']?>" />
					</a>
				    <label styel="top: 102px;">
						<a href="<?=Catalog::makeCatLink($popCat)?>">
						   <b style="color: #000;"> <? echo $popCat['name'] . " (" . $popCat['count'] . ")";?></b>
						</a>
				    </label>
				    <span class="pop_description">
				    	<a href="<?=Catalog::makeCatLink( $popCat )?>" title="<?=$popCat['name']?>">
							<?
								echo StdLib::addEllipsis($popCat['cat_desc'],300,false,false);
							?>
						</a>
					</span>
				</div>
			</div>
			<?
		}
	}
private function buildPopular2()
	{

		foreach( $this->resource as $popCat )
		{
			$popCat['pid'] = $popCat['id'];
		    ?>
		    <li>
			<div class="pop_featured_items_box">
				<div class="pop_iner" onmouseover="borderButton(<?=$popCat['id']?> );" onmouseout="clearButton(<?=$popCat['id']?>);">
					<a href="<?=Catalog::makeCatLink($popCat)?>">
					    <img style="width:155px;" src="<?=Catalog::makeCategoryImageLink( $popCat['id'], false, true )?>" alt="<?=$popCat['name']?>" />
					</a>
				    <label styel="top: 102px;">
						<a href="<?=Catalog::makeCatLink($popCat)?>">
						   <b style="color: #000;"> <? echo $popCat['name'] . " (" . $popCat['count'] . ")";?></b>
						</a>
				    </label>
				    <span class="pop_description">
				    	<a href="<?=Catalog::makeCatLink( $popCat )?>" title="<?=$popCat['name']?>">
							<?
								echo StdLib::addEllipsis($popCat['cat_desc'],102,false,false);
							?>
						</a>
					</span>
				</div>
			</div>
			</li>
			<?
		}
	}
	
	public function displayPopular($limit)
	{
		if($this->isPopular)
			self::getPopular($this->limit);
		else 
			self::makePopular($this->limit);		
		
		
		self::buildPopular($this->resource);
		
		
	}
	public function displayPopular2($limit)
	{
		if($this->isPopular)
			self::getPopular($this->limit);
		else 
			self::makePopular($this->limit);		
		
		
		self::buildPopular2($this->resource);
		
		
	}
}


?>