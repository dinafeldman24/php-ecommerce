<?
class Youtube {
	static function insert($info, $date = '') {
		return db_insert ( 'youtube_ids', $info, $date );
	}
	static function update($id, $info, $date = '') {
		return db_update ( 'youtube_ids', $id, $info, 'id', $date );
	}
	static function delete($id) {
		return db_delete ( 'youtube_ids', $id );
	}
	static function get($id = 0, $order = '', $order_asc = '', $limit = 0, $start = 0, $get_total = false, $video_type = '', $video_type_id = '', $is_active = '',$look_at_children = false, $group_by_url=false) {
		
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(youtube_ids.id)) AS total ";
		} else {
			$sql .= " youtube_ids.* ";
		}
		
		$sql .= " FROM youtube_ids ";
		
		$sql .= " WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND youtube_ids.id = $id ";
		}
		
		if ($video_type != '') {
			$video_type = mysql_real_escape_string ( $video_type );
			$sql .= " AND `youtube_ids`.video_type = '$video_type'";
		}
		
		if ($video_type_id != '') {
			$video_type_id = mysql_real_escape_string ( $video_type_id );
			$sql .= " AND (`youtube_ids`.video_type_id = '$video_type_id'";
			if ($video_type == "category" && $look_at_children)
			{
				$subcats_array = Cats::getSubCats($video_type_id);				
				if ($subcats_array) 
				{//print_r($subcats_array);
					foreach ($subcats_array as $element)
					{
						$subcats_array_simplified[] = $element['id'];
					}
					$subcats_string = implode(",", array_filter($subcats_array_simplified));
					$sql .= " OR `youtube_ids`.video_type_id IN ($subcats_string)";			 
				}
			}
			$sql .= ")";
		}
		
		if ($is_active) {
			$sql .= " AND youtube_ids.is_active = '" . addslashes ( $is_active ) . "' ";
		}
		
		if (! $get_total) {
			
			if($group_by_url){
				$sql .= ' GROUP BY youtube_ids.url ';
			}else{
				$sql .= ' GROUP BY youtube_ids.id ';
			}
			
			if ($order) {
				$sql .= " ORDER BY ";
				
				$sql .= addslashes ( $order );
				
				if ($order_asc !== '' && ! $order_asc) {
					$sql .= ' DESC ';
				}
			} else {
				$sql .= " ORDER BY youtube_ids.display_order";
			}
		}
		
		if ($limit > 0) {
			$sql .= db_limit ( $limit, $start );
		}
		
		if (! $get_total) {
			$ret = db_query_array ( $sql );
		} else {
			$ret = db_query_array ( $sql );
			return $ret [0] ['total'];
		}
// 		 echo $sql;
		// mail('tova@tigerchef.com', 'sql', $sql);
		return $ret;
	}
	static function get1($id) {
		$id = ( int ) $id;
		
		if (! $id)
			return false;
		$result = self::get ( $id );
		
		return is_array ( $result ) ? array_shift ( $result ) : false;
	}
	static function getYoutubeID($video_type, $video_type_id) {
		$res = self::get ( 0, '', '', 0, 0, false, $video_type, $video_type_id );
		if ($res)
			return $res [0] ['url'];
		else
			return false;
	}
	static function makeVideoSlider($video_type, $video_type_id) {

		$videos = Youtube::get ( 0, '', '', 0, 0, false, $video_type, $video_type_id, 'Y' );
		$count_videos = count ( $videos );
		
		if($count_videos){
			
			?>
			<div id='slideshow'>
			<div class='slides'>
			<?
			$x=array('one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen','twenty');
			$counter = 0;
			$first = true;	
			?>
			<ul>
			<?
			foreach ( $videos as $video ) {
				if ($first) {
					$style = "";
					$first = false;
				} else
					$style = "display: none;";
				?>
				<li id="slide-<?=$x[$counter]?>">
				<?
				
				$embed_url = Catalog::makeVideoEmbedLink($video['id']);
				if($embed_url){
					?>
					<a href="#" onclick='video_box("#video_box","<?=$embed_url?>"); return false;'></a>
					<img style="width:150px;" src="<?=Catalog::getVideoThumbnail($video['id'],'','',0)?>?<?=time()?>" alt="<?=$video['caption']?>">
					
					<?
					}
					?>
			    </li> 
			
				<?
				$counter ++;
				?>
		<? } ?>
		</ul>
		</div>
		<ul class="slides-nav">
		<?
		$cnt = 0;
		foreach ( $videos as $video ) {
			
			if ($first) {
				?><li class="on"><?
				$first = false;
			} else
		?>
			
			<li>
			    <a href="#slide-<?=$x[$cnt]?>"></a>
		    </li>
		<?  $cnt++; } ?>
	    </ul>
	</div>
	<?
		}
	}
	
	static function makeVideoCarousel($video_type, $video_type_id) {
		
		$id = 'mycarouselvideos_' . $video_type;
		$class =  $video_type . '_videos';

		?>
		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
			
		<script type="text/javascript">
		jQuery(document).ready(function() {
		jQuery('#<?=$id?>').jcarousel({
			visible:1,
			scroll: 1
		});
		});
		</script>
		<?php 

		$videos = Youtube::get ( 0, '', '', 0, 0, false, $video_type, $video_type_id, 'Y' );
		if(is_array($videos)){
			echo "<div class='".$class."'><ul id='".$id."' class='jcarousel-skin-videos'>";
			
			foreach ($videos as $video)
			{
				echo "<li style='text-align:center'>";
				$embed_url = Catalog::makeVideoEmbedLink($video['id']);
				if($embed_url){
					?>
				<div class="yt_video">	<a href="#" onclick='video_box("#video_box","<?=$embed_url?>"); return false;'>
					<img  style="width:150px;" src="<?=Catalog::getVideoThumbnail($video['id'],'','',0)?>?<?=time()?>" alt="<?=$video['caption']?>">
					<span></span></a></div>
					<?
				}
				echo "</li>";	
			}
			echo "</ul></div>";
		}
	}
	
	static function getProductVideos($video_type_id) {

		$videos = Youtube::get ( 0, '', '', 0, 0, false, 'product', $video_type_id, 'Y' );		
		
		if(is_array($videos)){
			if(count($videos)==1){
				?>
				<div class="one-video-holder" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
					<iframe class="one-video" width="560" height="315" src="<?=Catalog::makeVideoEmbedLink($videos[0]['id'])?>" frameborder="0"></iframe>
				</div>
				<p itemprop="name"><?=$videos[0]['caption']?></p>
				<span itemprop="description"><?
					//echo $videos[0]['description'];
				?></span>
				<meta itemprop="thumbnailUrl" content="<?=Catalog::getVideoThumbnail($videos[0]['id'],'','',0)?>"/>
				<?php 
			} else {
				?>
				<ul class="product-video-list">
				<?
				for ($i = 0; $i< count($videos); $i++) {
					?>
					<li itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
						<div class="video-holder">
							<iframe width="560" height="315" src="<?=Catalog::makeVideoEmbedLink($videos[$i]['id'])?>" frameborder="0"></iframe>
						</div>
						<p itemprop="name"><?=$videos[$i]['caption']?></p>
						<span itemprop="description"><?=$videos[$i]['description']?></span>
						<meta itemprop="thumbnailUrl" content="<?=Catalog::getVideoThumbnail($videos[$i]['id'],'','',0)?>"/>
					</li> 
					<?
					if (($i + 1) % 3 == 0) {
						?>
						</ul>
						<ul class="product-video-list">
						<?
					}
				}
			}
			?>
			</ul>
			<?php 		
		}
	}
}

