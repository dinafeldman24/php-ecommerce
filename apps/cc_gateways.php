<?php

/*****************
*   Class for credit card Gateways changes
*
*****************/

class CcGateways{
    
    function get($id=0, $date_time='',$cc_type='', $start_time='', $end_time='', $gateway=''){
		global $debug;
		
		$sql = "SELECT *";
		$sql .= " FROM cc_gateway_changes ";
		$sql .= " WHERE 1 ";
 
		if ($id > 0) {
		    $sql .= " AND cc_gateway_changes.id = $id ";
		}
		if($date_time != ''){
			$date_time=stripslashes($date_time);
			$sql .= " AND `cc_gateway_changes`.start_time < '$date_time'";
			$sql .= " AND `cc_gateway_changes`.end_time > '$date_time'";
		}
		if($start_time!= ''){
			$sql .= " AND `cc_gateway_changes`.start_time < '$start_time'";
		}
		if($end_time!= ''){
			$sql .= " AND `cc_gateway_changes`.end_time < '$end_time'";
		}
		if($cc_type!= ''){
			$is_active = mysql_real_escape_string($cc_type);
			$sql .= " AND `cc_gateway_changes`.cc_type LIKE '%$cc_type%'";
		}
		
		$sql .= ' GROUP BY cc_gateway_changes.id ';
		$ret = db_query_array($sql);
		return $ret;
    }
	
	function update($id, $info, $table='cc_gateway_changes'){
		return db_update($table,$id,$info);
    }
	function insert($info, $table='cc_gateway_changes'){
		 return db_insert($table ,$info);
    }
	function delete($id=0, $table='cc_gateway_changes'){
		return db_delete($table ,$id);
    }	
	
}




?>
