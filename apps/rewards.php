<?php

class Rewards {
    
    function insertReward($info)
	{
		return db_insert('rewards_history', $info);
	}
	
	function updateReward($id, $info)
	{
		return db_update('rewards_history', $id, $info);
	}
	
	function deleteReward($id)
	{
		return db_delete('rewards_history', $id);
	}
	
	function getRewards($id = 0, $customer_id = 0, $customer_query ='', $order_id=0, $start_date='',$end_date='',$order='',$order_asc='', $total='',$action='',$action_type='',$review_id = '')
	{
                $customer_query = trim($customer_query);
	                        
		$q = " SELECT rewards_history.*,
                                customers.first_name as customer_fname,
                                customers.last_name as customer_lname
                        FROM rewards_history ";
                        
                if( $total ){
		    $q = " SELECT COUNT(DISTINCT rewards_history.id) as total
                            FROM rewards_history ";
		}
                
                $q .= " LEFT JOIN customers ON customers.id = rewards_history.customer_id ";
                
                $q .= " WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('rewards_history.id', $id);
		}
                if ($customer_id) {
			$q .= db_restrict('customer_id', $customer_id);
		}
                if ($customer_query && $customer_query != ''){
                        $q .= " AND ";
                        $fields = Array('customers.first_name',
			    'customers.last_name',
			    'customers.id',
			    'customers.email'  );

                    $q .= db_split_keywords($customer_query,$fields,'OR',true);
                }
                if ($order_id) {
			$q .= db_restrict('order_id', $order_id);
		}
                if(isset($start_date) || isset($end_date)){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$q .= db_queryrange('rewards_history.date',$start_date,$end_date);
		}
                if($action){
                    $q .= db_restrict('action', $action);
                }
								
								if($review_id){
                    $q .= db_restrict('review_id', $review_id);
                }
                if($action_type){
                    $q .= db_restrict('action_type', $action_type);
                }
                
                //echo $q;
                
                if( $total ){
                    $arr = db_query_array($q);
		    return $arr[0]['total'];
                }
                
                if($order){
                        $q .= " ORDER BY " . db_escape_order_by($order);
                        if($order_asc){
                                $q .= " ASC ";
                        } else {
                                $q .= " DESC ";
                        }
                }
                else {
                        $q .= " ORDER BY rewards_history.id DESC ";
                }

                //$q .= " GROUP BY rewards_history.id ";
               		
		return db_query_array($q);
	}
        
        static function get1($id)
	{
		$id = (int) $id;

		if (!$id) 
		{
			return false;
		}
                
		$result = Rewards::getRewards($id);
		return $result[0];
	}
        
        /**
	 * Get By Object Parameters
	 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
	 */
	static function getRewardsByO($o=""){	
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
	
		$select_ = " SELECT rewards_history.* ,
                                customers.first_name as customer_fname,
                                customers.last_name as customer_lname ";

		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(rewards_history.id)) as total ";
		}
	
		$join_ .= " LEFT JOIN customers ON customers.id = rewards_history.customer_id";
				
		$from_ = " FROM rewards_history ";
	
		$where_ = " WHERE 1 ";
	
		if(isset($o->id)){
			$where_ .= " AND rewards_history.id = [id] ";
		}
                
		if( isset( $o->customer_id )  ){
			$where_ .= " AND `rewards_history`.customer_id = [customer_id]";
		}
                                
                if( isset( $o->points_qty )  ){
			$where_ .= " AND `rewards_history`.points_qty = [points_qty]";
		}
                
                if( isset( $o->action_type )  ){
			$where_ .= " AND `rewards_history`.action_type = [action_type]";
		}
                
                if( isset( $o->action )  ){
			$where_ .= " AND `rewards_history`.action = [action]";
		}
		
		if( isset( $o->order_id )  ){
			$where_ .= " AND `rewards_history`.order_id = [order_id]";
		}

		if(isset($o->start_date) || isset($o->end_date)){
			if($o->start_date){
				$o->start_date .= " 00:00:00";
			}
			if($o->end_date){
				$o->end_date .= " 23:59:59";
			}
			$where_ .= db_queryrange('rewards_history.date',$o->start_date,$o->end_date);
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY rewards_history.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}

			$groupby_ = " GROUP BY rewards_history.id ";
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);
		
		if($o->total){
			return (int)$result[0]['total'];
		}
	
		return $result;
	}

        function getAvailableRewards($customer_id, $only_active = true){
            $q = " SELECT SUM(points_qty) AS available_rewards FROM rewards_history
                WHERE customer_id = '$customer_id'
                AND canceled = 'N' ";
            if($only_active){
                $q .= " AND active = 'Y' ";
            }
            $q .= " GROUP BY customer_id ";
                //echo $q;
            $ret = db_query_array($q);
            return ($ret[0]['available_rewards'] > 0 ? $ret[0]['available_rewards'] : 0 );
        }
        
        function getPendingRewards($customer_id){
            $q = " SELECT SUM(points_qty) AS pending_rewards FROM rewards_history
                WHERE customer_id = '$customer_id'
                AND active = 'N'
                AND canceled = 'N'
                GROUP BY customer_id ";
                
            $ret = db_query_array($q);
            return $ret[0]['pending_rewards'];
        }
        
        function getPendingRewardsWithAvailablity($customer_id){
            $q = " SELECT points_qty, date, action FROM rewards_history
                WHERE customer_id = '$customer_id'
                AND active = 'N'
                AND canceled = 'N' ";
                
            $points = db_query_array($q);
            
            $return = array('review' => null,'order' => null);
            
            foreach($points as $item){
                
                if($item['action'] == 'review' || $item['action'] == 'comment'){
                    $return['review'] +=  $item['points_qty'];
                } else if($item['action'] == 'order'){
                    $ret['available'] = date('F d, Y', strtotime($item['date']. ' + 45 days'));
                    $ret['points_qty'] = $item['points_qty'];
                    $return['order'][] = $ret;
                }
            }
            
            return $return;
        }
        
        function activateRewards(){
        	global $CFG;
            $q = " SELECT id FROM rewards_history";
            if ($CFG->site_name == "lionsdeal") $q .=  " WHERE date < DATE_SUB(NOW(),INTERVAL 30 DAY )";
            else $q .=  " WHERE date < DATE_SUB(NOW(),INTERVAL 45 DAY )";
            $q .= " AND canceled = 'N'
                   AND active = 'N'
                   AND action_type = 'earned' ";
            
            $result = db_query_array($q);

            $ids = '';
            foreach($result as $record){
                $ids .= $record['id'] . ",";
            }
            $ids = trim ($ids ,',');

            if($ids){
                $q = " UPDATE rewards_history
                        SET active = 'Y'
                        WHERE id IN ($ids) ";
                db_query_array($q);
                
                $q = " SELECT customer_id, SUM(points_qty) as new_rewards FROM rewards_history
                       WHERE id IN ($ids)
                       GROUP BY customer_id ";
                $result = db_query_array($q);
            }
            
            return $result;
        }
                
        function activate1($review_id, $active = 'Y', $comment_id='',$action=""){
        	
        	if(!$review_id){
        		return false;
        	}
        	
            $q = " UPDATE rewards_history SET ";
            
            if($active == 'Y'){
            	$q .= " active = 'Y'";
            	$q .= ", canceled = 'N'";
            }elseif($active == 'P'){
            	$q .= " active = 'N'";
            	$q .= ", canceled = 'N'";
            }else{
            	$q .= " canceled = 'Y'";
            }
            
            $q .= "  WHERE 1 ";
            
            $q .= " AND review_id = '$review_id' ";
            
            if($comment_id){
            	$q .= " AND comment_id = '$comment_id' ";
            }
						
						if ($action){
						  $q .= " AND action = '$action' ";
						}
					
            //echo $q;
            db_query_array($q);
            
            if($active == 'Y'){
                $q = " SELECT * FROM rewards_history
                        WHERE review_id = '$review_id' ";
	            if($comment_id){
	            	$q .= " AND comment_id = '$comment_id' ";
	            }
							$q .= " AND active = 'Y' ";
							$q .= " AND canceled = 'N' ";
							if (($action=='image')||($action=='video')){
                	$q .= " AND action = '$action' ";
							}		
										
							 $rewards = db_query_array($q);
							 
						  $info = array(
                    'customer_id' => $rewards[0]['customer_id'],
                    'new_rewards' => 0
               );
							 $info['review_id']=$review_id;
							 $info['extra_info']='Thank you for submitting a review! The following has been approved:<br/>';
							 foreach ($rewards as $reward){
                 $info['new_rewards'] =$info['new_rewards']+$reward['points_qty'];
							   if ($reward['action']=='review') {$info['extra_info'] .= 'Your review.<br>';}
							   if ($reward['action']=='image') {$info['extra_info'] .= 'Your image.<br>';}
 							   if ($reward['action']=='video') {$info['extra_info'] .= 'Your video.<br>';}
							 }
                $info['extra_info'] .='<br/>';
							 //mail('tova@tigerchef.com', 'email', print_r($rewards,true));
             
                if($comment_id){
                	$info['extra_info'] = 'Your comment has been approved!<br>';
									Rewards::sendRewardsNotification($info);
                }
								if ($action=='review')
                   Rewards::sendReviewConfirmation($info);
            }
						return ($info);
        }
        
        function sendRewardsNotification($info){
            global $CFG;
            
            $customer = Customers::get1($info['customer_id']);
            
            if($customer['email_pending_rewards'] == 'Y'){

                $vars['full_name'] = $customer['first_name'] . " " . $customer['last_name'];
                $vars['new_rewards'] = $info['new_rewards'];
                $vars['new_rewards_dollar'] = number_format($info['new_rewards']*$CFG->reward_pts_dollar_conversion/100,2);
                $vars['total_available_rewards'] = Rewards::getAvailableRewards($customer['id']);
                $vars['total_available_dollar'] = number_format($vars['total_available_rewards']*$CFG->reward_pts_dollar_conversion /100,2) ;
                $vars['extra_info'] = $info['extra_info'];
                //mail('tova@tigerchef.com','debug: rewards notification',print_r($vars,true));
                TigerEmail::sendOne($vars['full_name'],$customer['email'],'pending_rewards_notification',$vars);
                //TigerEmail::sendOne($vars['full_name'],'tova@tigerchef.com','pending_rewards_notification',$vars);
            }
        }
        
				static function sendReviewConfirmation($info){
				global $CFG;
        ///////////////////Email to customer///////////////////////
				$cust = Customers::get1($info['customer_id']);				
			  $vars['new_rewards'] = $info['new_rewards'];
        $vars['new_rewards_dollar'] = number_format($info['new_rewards']*$CFG->reward_pts_dollar_conversion/100,2);
        $vars['total_available_rewards'] = Rewards::getAvailableRewards($cust['id']);
        $vars['total_available_dollar'] = number_format($vars['total_available_rewards']*$CFG->reward_pts_dollar_conversion /100,2) ;
        $vars['extra_info'] = $info['extra_info'];
				$review=Product_Reviews::get1($info['review_id']);	
	      $item = Products::get1($review['product_id']);
        $itemName = $item['name'];
        $itemUrl = $item['url_name'];
   	    $custFullName = $cust['first_name'] . " " . $cust['last_name'];
			  $custEmail = $cust['email'];
			  $reviewTitle=$review['title'];
			  $reviewComment=$review['comment'];
			  if ($review['img_url'])
			  { 
			    $reviewImage="<img src='".$CFG->baseurl."upload/".$review['img_url']."' width='200'>";
			    $imageCaption=$review['img_caption'];
			  }
			
			  if ($review['video_url'])
			  { 
			    $reviewVideo=$review['video_url'];
			    $videoCaption=$review['video_caption'];
			  }
				$vars['full_name']=$custFullName;						
				$vars['plink']=$CFG->baseurl.$itemUrl.".html";
				$vars['product']=$itemName;
	      $imageurl=Catalog::makeProductImageLink($review['product_id'], false);
        $imageurl=str_replace("https", "http", $imageurl);
				$vars['image_link']=$imageurl;
				$vars['title']=stripslashes($reviewTitle);
				$reviewComment=preg_replace('#(\\\r|\\\r\\n|\\\n)#', '<br/>',$reviewComment);
        $reviewComment=str_replace("<br/><br/>","<br/>",$reviewComment);
				$vars['comment']=stripslashes($reviewComment)."<br/>";
				$vars['points']=$info['extra_info'];
			  $rating=Product_Ratings::get('',$review['product_id'],$review['customer_id']);
				$star_rating=$rating[0]['rating'];
				if ($star_rating)
							$vars['comment'] .= "You rated this product as: $star_rating out of 5 <br />";
				$re=Product_Recommends::get('',$review['product_id'],$review['customer_id']);			
				
				if ($re[0]['recommend']=='Y'){$vars['comment'] .= "<strong>Would You Recommend this Product?</strong> Yes<br/>";}
				if ($re[0]['recommend']=='N'){$vars['comment'] .= "<strong>Would You Recommend this Product?</strong> No<br/>";}
				
				$terms1=Review_Terms::get('',1,'','',$review['id']);
				$terms2=Review_Terms::get('',2,'','',$review['id']);
				$terms3=Review_Terms::get('',3,'','',$review['id']);
						
							if ($terms1)
							{
							  $vars['comment'].= "<b>Pros: </b>";
								foreach ($terms1 as $pro)
								 {
								   $vars['comment'] .= $pro['description'].", ";
								 }
                $vars['comment']=substr($vars['comment'], 0, -2);
								$vars['comment'] .="<br/>";	
							}
							
							if ($terms2)
							{
							  $vars['comment'] .= "<b>Cons: </b>";
								foreach ($terms2 as $con)
								 {
								   $vars['comment'] .= $con['description'].", ";
								 }
                $vars['comment']=substr($vars['comment'], 0, -2);
								$vars['comment'] .="<br/>";	
							}

							
							if ($terms3)
							{
							  $vars['comment'] .= "<b>Best Uses: </b>";
								foreach ($terms3 as $best)
								 {
								   $vars['comment'] .= $best['description'].", ";
								 }
								$vars['comment']=substr($vars['comment'], 0, -2); 
								$vars['comment'] .="<br/>";	
							}

							
							if ($reviewImage){
							$vars['comment'].="<br/>You added this image to your review:<br/><b>$imageCaption</b><br/>$reviewImage</br>";
							}
							
					
							if ($reviewVideo){
							$vars['comment'].="<br/>Here's the link to the youtube video you added:<br/><b>$videoCaption</b><br/><a href='http://www.youtube.com/watch?v=".$reviewVideo."'>http://www.youtube.com/watch?v=".$reviewVideo."</a></br>";
							}
					
							$to_email = $custEmail;
							$E = TigerEmail::sendOne($CFG->company_name,$to_email,"New Review", $vars); 
				
			  }
				
				
        function cancelRewards($order_id='',$cancel,$action_type='',$review_id='',$reward_id='',$action){
           
            $q = " UPDATE rewards_history
                    SET canceled = '$cancel'
                    WHERE 1 ";
            if($reward_id){
                $q .= " AND id = '$reward_id' ";
            }
            if($order_id){
                $q .= " AND order_id = '$order_id' ";
            }
            if($review_id){
                $q .= " AND review_id = '$review_id' ";
            }
            if($action_type){
                $q .= " AND action_type = '$action_type' ";
            }
						 
            if($action){
                $q .= " AND action = '$action' ";
            }

            //echo $q;
            db_query_array($q);
        }
        
        //THIS FUNCTION IS NO LONGER USED - TE
        function calculateEarnedRewardsForProduct($product_id,$item_price){
           
           global $CFG;
           
            //Check if product should earn rewards
	    $earned_rewards = 0;
	    $found = false;
	    $product_cats = Cats::getCatsForProduct($product_id);
	    $cats_to_exclude = $CFG->rewards_exclude_categories . ',';
	    foreach($product_cats as $cat){
		if(!(strpos($cats_to_exclude,$cat['id'].',')===false)){
		    $found = true;
		}
	    }
	    if(!$found){
		$earned_rewards += round($item_price * ($CFG->reward_pts_dollar_conversion)) ;
	    }
            return $earned_rewards;
        }
    
		function numRewardsPerPrice($item_price){
        	global $CFG;
        	//var_dump($item_price);
			return max(round($item_price * ($CFG->rewards_pts_awarded_per_dollar)),1) ;
        }   
    
}