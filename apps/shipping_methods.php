<?php

class ShippingMethods {

	static function insert($info)
	{
		return db_insert('shipping_methods',$info);
	}

	static function update($id,$info)
	{
		return db_update('shipping_methods',$id,$info);
	}

	static function delete($id)
	{
		return db_delete('shipping_methods',$id);
	}

	static function get($id=0, $order='', $order_asc='',$code='',$api_code='',$selectable='',$desc='',$iglobal_shipping_level='',$tracking_import_name='',$display_in_manual_add_tracking='')
	{
		$sql = "SELECT shipping_methods.*
				FROM shipping_methods
				WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND shipping_methods.id = $id ";
		}
		if($code){
			$code = db_esc($code);

			$sql .= " AND shipping_methods.code = '$code' ";
		}
		if($api_code){
			$api_code = db_esc($api_code);

			$sql .= " AND shipping_methods.api_code = '$api_code' ";
		}
		if($selectable){
			$selectable = db_esc($selectable);
			$sql .= " AND shipping_methods.selectable = '$selectable' ";
		}
		if($desc){
			$name = db_esc($desc);
		
			$sql .= " AND shipping_methods.descr = '$desc' ";
		}
        if($iglobal_shipping_level){
            $iglobal_shipping_level = db_esc($iglobal_shipping_level);

            $sql .= " AND shipping_methods.iglobal_shipping_level = '$iglobal_shipping_level' ";
        }
    	if($tracking_import_name){
            $tracking_import_name = db_esc($tracking_import_name);

            $sql .= " AND shipping_methods.tracking_import_name LIKE '%$tracking_import_name%' ";
        }
        
    	if($display_in_manual_add_tracking){
            $display_in_manual_add_tracking = db_esc($display_in_manual_add_tracking);

            $sql .= " AND shipping_methods.display_in_manual_add_tracking = '$display_in_manual_add_tracking' ";
        }
        
        $sql .= " GROUP BY shipping_methods.id
				  ORDER BY ";

		if ($order == '') {
			$sql .= ' selectable ASC, shipping_methods.days DESC ';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}
	
		return db_query_array($sql);
	}
	
	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = ShippingMethods::get($id);
		return $result[0];
	}

	static function haveLabel($order_tracking_id_or_row){
		global $CFG;

		if(is_array($order_tracking_id_or_row)){
			$row = $order_tracking_id_or_row;
		} else {
			$id = (int)$order_tracking_id_or_row;
			if(!$id){
				return false;
			}
			$row = db_get1( Orders::getTracking($id) );
		}

		if(!$row){
			return false;
		}

		if(stripos($row['service_name'],'ups') === false){
			return false;
		}

		return is_file($CFG->dirroot . "/edit/labels/".$row['tracking_number'] . ".pdf") ||
			   is_file($CFG->dirroot . "/edit/labels/".$row['tracking_number'] . ".gif");
	}

	static function haveLabelGif($tracking_number){
		global $CFG;

		return
			   is_file($CFG->dirroot . "/edit/labels/".$tracking_number . ".gif");
	}


}

?>