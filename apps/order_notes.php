<?php

class OrderNotes
{
	public static function insert($info)
	{
	    $info['date_added'] = strftime('%Y-%m-%d %H-%M-%S', time());
		return db_insert('order_notes', $info);
	}
	
	public static function update($id,$info)
	{
		return db_update('order_notes', $id, $info);
	}
	
	public static function delete($id)
	{
		return db_delete('order_notes', $id);
	}
		
	public static function get($id=0, $order_id=0, $user_id=0, $type='', $start_date=0, $end_date=0, $order='', $order_asc='')
	{
		$sql = "SELECT order_notes.*
				FROM order_notes
				WHERE 1 ";
		
		if ($id > 0) 
		{
			$sql .= " AND order_notes.id = $id ";
		}

        if ($order_id > 0)
        {
            $sql .= " AND order_notes.order_id = $order_id ";    
        }
        
        if ($user_id > 0)
        {
            $sql .= " AND order_notes.user_id = $user_id ";    
        }
        
        if ($type != '')
        {
            $sql .= " AND order_notes.type = $type ";    
        }
			
		$sql .= " ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'order_notes.id';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' ASC ';
		}
		
		return db_query_array($sql);
	}
	
	public static function get1ByOrderID($id)
	{
		$id = (int) $id;
		
		if (!$id) 
		{
		    return false;
		}
		
		$result = self::get(0, $id);
		
		return $result[0];
	}
}

?>