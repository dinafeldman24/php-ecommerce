<?php

class Product_Recommends
{
    public static function get($id = 0, $product_id = 0, $customer_id = 0, $recommend)
    {
        $sql = "SELECT product_reviews.*
                FROM product_reviews
                WHERE 1 ";

        if ($id > 0)
        {
//            $sql .= " AND product_recommend.id = $id ";
        }

        if ($product_id > 0)
        {
            $sql .= " AND product_reviews.product_id = $product_id ";
        }

        if ($customer_id > 0)
        {
            $sql .= " AND product_reviews.customer_id = $customer_id ";
        }

        if ($recommend)
        {
            $sql .= " AND product_reviews.recommend = $recommend ";
        }

        return db_query_array($sql);
    }

   public static function get_recommend_for_product($product_id)
    {
        /* make sure we are dealing with good data */
       if (!$product_id && !is_int($product_id))
        {
            return null;
        }

        $sql = "SELECT count( * ) AS recommends_total
FROM product_reviews
WHERE product_id = {$product_id} AND recommend='Y'
GROUP BY product_id ";


        $result = db_query_array($sql);
        if ($result)
        {
            return $result[0];
        }
        else
        {
            return false;
        }
    }

    public static function get_recommend_by_product_and_customer($product_id, $customer_id)
    {
        return self::get(0, $product_id, $customer_id);
    }

    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info, $product_id, $customer_id)
    {
        $info['product_id']     = $product_id;
        $info['customer_id']    = $customer_id;

        return db_insert('product_reviews', $info);
    }

    public static function delete($id)
    {
    //    return db_delete('product_recommend', $id);
    }

    public static function update($id, $info)
    {
//        return db_update('product_reviews', $id, $info );
    }
}

?>