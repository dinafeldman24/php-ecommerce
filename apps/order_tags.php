<? 
class OrderTags
{
	static function get($id=0, $tag_name='', $order_by = '')
	{  
		$sql = "SELECT ";
	    $sql .= " order_tags.* ";		
		$sql .= " FROM order_tags ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND order_tags.id = $id ";
		}
		if ($tag_name !='') {
		    $sql .= " AND order_tags.tag_name = '$tag_name'";
		}												
	
		if ($order_by != '') $sql .= " ORDER BY $order_by";
	    $ret = db_query_array($sql);
//mail("rachel@tigerchef.com", "order tags sql", $sql);
		return $ret;
	}
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}

	static function delete($id = 0)
	{
		if(!$id) return false;

		// using default params for everything except last one, $id
		$see_if_orders_with_this_tag = Orders::get(0,0,'','',0,'','','','','',0,'',0,'','','','','',0,'','',0,'','','','','',0, '','','','','','','','','',0,0,0,"",'','','',false,0,0,'','','','',false,'','','',false,'', 0,'any',$id);	
		if ($see_if_orders_with_this_tag)
		{
			echo "There are orders in the system that use this tag; therefore, this tag may not be deleted.";
			return false;
		}
		else return db_delete('order_tags', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
			
		db_insert('order_tags', $info);				
	}
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
		

			$ret = db_update('order_tags',$id,$info);

		return $ret;
	}
}
?>