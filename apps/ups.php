<?php

include_once 'RB/stdlib.php';
include_once 'Net/Curl.php';	// needed by _request()

class UPS {

	const UPS_HOST_PROD = 'https://www.ups.com/';
	const UPS_HOST_DEV = 'https://wwwcie.ups.com/';
	const UPS_SHIP_CONFIRM_URL = 'ups.app/xml/ShipConfirm';
	const UPS_SHIP_ACCEPT_URL = 'ups.app/xml/ShipAccept';
	const UPS_REQUEST_VOID_URL = 'ups.app/xml/Void';
        const UPS_REQUEST_RATE = 'ups.app/xml/Rate';

	const UPS_TRACKING_URL = 'ups.app/xml/Track';

	private $_debug;
	private $_in_testing;
	private $_access_xml;
	private $_action_xml;

	private $num_packages = 1;

	// rustybrick access key FBE225BDC951EF61
	// pass ask ronnie
	/**
	 * inits the ups object with the access xml
	 *
	 * @param string $AccessLicenseNumber
	 * @param string $UserId
	 * @param string $Password
	 * @param boolean $in_testing to show integration
	 * @param boolean $debug
	 * @return void
	 */
	public function __construct($AccessLicenseNumber,$UserId,$Password,$in_testing=false,$debug=false)
	{
		$this->_debug = $debug;
		$this->_in_testing = $in_testing;

		$this->_access_xml = '<?xml version="1.0"?'.'>
			<AccessRequest xml:lang="en-US">
				<AccessLicenseNumber>'.$AccessLicenseNumber.'</AccessLicenseNumber>
				<UserId>'.$UserId.'</UserId>
				<Password>'.$Password.'</Password>
			</AccessRequest>';
		$this->_action_xml = '';
	}


        // ------------------------------------------------------------------------------------
	// request a rate quote
	//  return the amount, tracking number and digest on success
	//	return false on failure
	// ------------------------------------------------------------------------------------
	public function ShipRates($info)
	{
		$this->setShipRates($info);
		$data = $this->_request(self::UPS_REQUEST_RATE);
                
                $options = Array();
                foreach ($data->RatedShipment as $rs ){
                    $options[] = Array(
                            'amount' => (string) $rs->TotalCharges->MonetaryValue,
                            'id'	 => (string) $rs->Service->Code
                            );
                }
		return $options;
	}



	// ------------------------------------------------------------------------------------
	// request a shipment
	//  return the amount, tracking number and digest on success
	//	return false on failure
	// ------------------------------------------------------------------------------------
	public function ShipConfirm($info)
	{
		$this->setShipConfirm($info);
		$data = $this->_request(self::UPS_SHIP_CONFIRM_URL);
		
		$negotiated_rate = (string) $data->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue;		
		//mail("rachel@tigerchef.com", "negotiated rate from ShipConfirm", $negotiated_rate);
		/*return Array(
			'amount' => (string) $data->ShipmentCharges->TotalCharges->MonetaryValue,
			'id'	 => (string) $data->ShipmentIdentificationNumber,
			'digest' => (string) $data->ShipmentDigest
		);*/
		return Array(
			'amount' => (string) $negotiated_rate,
			'id'	 => (string) $data->ShipmentIdentificationNumber,
			'digest' => (string) $data->ShipmentDigest
		);
	}


	// ------------------------------------------------------------------------------------
	// accept a shipment
	//  return the amount, tracking number and label on success
	//	return false on failure
	// ------------------------------------------------------------------------------------
	public function ShipAccept($digest)
	{
		$this->setShipAccept($digest);
		$data = $this->_request(self::UPS_SHIP_ACCEPT_URL);

		/**
		 * Single Label & Package
		 */
		$negotiated_rate = (string) $data->ShipmentResults->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue;		
		//mail("rachel@tigerchef.com", "negotiated rate from ShipAccept", $negotiated_rate);
		$retval = Array(
			//'amount' => (string) $data->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue,
			'amount' => (string) $negotiated_rate,
			'tracking_number'	 => (string) $data->ShipmentResults->PackageResults->TrackingNumber,

			//EPL2,ZPL, SPL Image
			'label' => base64_decode((string) $data->ShipmentResults->PackageResults->LabelImage->GraphicImage),

			// GIF Image
			'html_label' => base64_decode((string) $data->ShipmentResults->PackageResults->LabelImage->HTMLImage),
		);

		if(count($data->ShipmentResults->PackageResults) > 1){
			/**
			 * Multiple labels & tracking numbers returned
			 */

			$retval['tracking_number'] = array();
			$retval['label'] = array();
			$retval['html_label'] = array();
			foreach($data->ShipmentResults->PackageResults as $pkg){
				$retval['tracking_number'][] = $pkg->TrackingNumber;
				$retval['label'][] = base64_decode((string) $pkg->LabelImage->GraphicImage);
				$retval['html_label'][] = base64_decode((string) $pkg->LabelImage->HTMLImage);
			}
		}

		// Check for High Value Report
		if($data->ShipmentResults->ControlLogReceipt){
			$retval['high_value_report'] = base64_decode(
				(string) $data->ShipmentResults->ControlLogReceipt->GraphicImage
			);

		}

		return $retval;
	}


	// ------------------------------------------------------------------------------------
	// void a shipment
	//  return true on success
	//	return false on failure
	// ------------------------------------------------------------------------------------
	public function ShipVoid($tracking_number)
	{
		$this->setShipVoid($tracking_number);
		$data = $this->_request(self::UPS_REQUEST_VOID_URL);

		return true;
	}

	// ------------------------------------------------------------------------------------
	// track a shipment
	//  return info on success
	//	return false on failure
	// ------------------------------------------------------------------------------------
	public function ShipTrack($tracking_number )
	{
		$this->setTracking($tracking_number, $option);

		$data = $this->_request(self::UPS_TRACKING_URL);



		$package 	= $data->Shipment->Package;

		$ret 		= array();



		foreach( $package as $key => $p )

		{

			$i = (string) $p->TrackingNumber;

			//print_ar( $p );

			$ret[$i]['tracking_number']  	= (string) $p->TrackingNumber;

			$x = 0;

			foreach( $p->Activity as $activity )

			{

				$ret[$i]['activity'][$x]['status_code']	= (string) $activity->Status->StatusType->Code;

				$ret[$i]['activity'][$x]['status_desc']	= (string) $activity->Status->StatusType->Description;

				$ret[$i]['activity'][$x]['date']		= (string) $activity->Date;

				$ret[$i]['activity'][$x]['time']		= (string) $activity->Time;

				$x++;

			}

		}



		return $ret;
	}

        // ------------------------------------------------------------------------------------
	// produces xml for ship confirm action
	// ------------------------------------------------------------------------------------
	private function setShipRates($info)
	{

		// Shipping Multiple Packages in 1 Shipment - 5/27/2010
		if(isset($info['MultiPackage'])){

			$multipackage = $info['MultiPackage'];
			$this->num_packages = count($multipackage);

			foreach($multipackage as $k => $v){
				if(!is_array($v)){
					$multipackage[$k] = xmlentities(trim($v));
				}
			}
		}

		foreach ($info as $k => $v) {
			if(!is_array($v)){
				$info[$k] = xmlentities(trim($v));
			}
		}
                //print_ar($info);
		$defaults = Array(
			'LabelPrintMethod' => 'EPL',
			'LabelStockSizeWidth' => '6',
			'LabelStockSizeHeight' => '4',
			'ShipperName' => '',
			'ShipperPhoneNumber' => '',
			'ShipperNumber' => '',
			'ShipperAddressLine1' => '',
			'ShipperAddressLine2' => '',
			'ShipperAddressLine3' => '',
			'ShipperCity' => '',
			'ShipperState' => '',
			'ShipperZip' => '',
			'ShipperCountryCode' => 'US',

			'CompanyName' => '',
			'AttentionName' => '',
			'PhoneNumber' => '',
			'AddressLine1' => '',
			'AddressLine2' => '',
			'AddressLine3' => '',
			'City' => '',
			'State' => '',
			'Zip' => '',
			'CountryCode' => 'US',
			'Residential' => false,
			'ServiceCode' => '',
			'OrderNumber' => '',
			'Packaging' => '',
			'Weight' => '',
			'Length' => '',
			'Width' => '',
			'Height' => '',

			'ReferenceNumber2Code' => '',
			'ReferenceNumber2'	   => '',
                        'CurrencyCode'         => 'USD'
		);

		foreach ($defaults as $k => $v)
			if(!isset($info[$k])) $info[$k] = $v;
		if ($info['ShipmentDescription'] != '')
			$shipment_description = "<Description>$info[ShipmentDescription]</Description>";
		if ($info['InvoiceLineTotal'] > 0){
			$invoice_line_total = '<InvoiceLineTotal>
										<CurrencyCode>USD</CurrencyCode>
										<MonetaryValue>'.$info['InvoiceLineTotal'].'</MonetaryValue>
									</InvoiceLineTotal>';
		}

		$service_options = '';

		if($info['SaturdayDelivery']){
			$service_options .= "<SaturdayDelivery/>";
		}

		if($service_options){
			$service_options = "<ShipmentServiceOptions>$service_options</ShipmentServiceOptions>";
		}

		$this->_action_xml = '<?xml version="1.0"?'.'>
			<RatingServiceSelectionRequest xml:lang="en-US">
				<Request>
					<TransactionReference>
						<CustomerContext>ShipConfirmUS</CustomerContext>
						<XpciVersion>1.0001</XpciVersion>
					</TransactionReference>
					<RequestAction>Rate</RequestAction>
					<RequestOption>Shop</RequestOption>
				</Request>
				<LabelSpecification>
					<LabelPrintMethod>
						<Code>'.$info['LabelPrintMethod'].'</Code>
					</LabelPrintMethod>
					<LabelStockSize>
						<Height>'.$info['LabelStockSizeHeight'].'</Height>
						<Width>'.$info['LabelStockSizeWidth'].'</Width>
					</LabelStockSize>
					<LabelImageFormat>
						<Code>'.$info['LabelPrintMethod'].'</Code>
					</LabelImageFormat>
				</LabelSpecification>
				<Shipment>'.
					$service_options
					.
					($shipment_description != '' ? $shipment_description : '')
					.($invoice_line_total != '' ? $invoice_line_total : '').
					'<Shipper>
						<Name>'.$info['ShipperName'].'</Name>
						<AttentionName>'.$info['ShipperName'].'</AttentionName>
						<PhoneNumber>'.$info['ShipperPhoneNumber'].'</PhoneNumber>
						<ShipperNumber>'.$info['ShipperNumber'].'</ShipperNumber>
						<Address>
							<AddressLine1>'.$info['ShipperAddressLine1'].'</AddressLine1>
							<AddressLine2>'.$info['ShipperAddressLine2'].'</AddressLine2>
							<AddressLine3>'.$info['ShipperAddressLine3'].'</AddressLine3>
							<City>'.$info['ShipperCity'].'</City>
							<StateProvinceCode>'.$info['ShipperState'].'</StateProvinceCode>
							<PostalCode>'.$info['ShipperZip'].'</PostalCode>
							<CountryCode>'.$info['ShipperCountryCode'].'</CountryCode>
						</Address>
					</Shipper>
					<ShipTo>
						<CompanyName>'.$info['CompanyName'].'</CompanyName>
						<AttentionName>'.$info['AttentionName'].'</AttentionName>
						<PhoneNumber>'.$info['PhoneNumber'].'</PhoneNumber>
						<Address>
							<AddressLine1>'.$info['AddressLine1'].'</AddressLine1>
							<AddressLine2>'.$info['AddressLine2'].'</AddressLine2>
							<AddressLine3>'.$info['AddressLine3'].'</AddressLine3>
							<City>'.$info['City'].'</City>
							<StateProvinceCode>'.$info['State'].'</StateProvinceCode>
							<PostalCode>'.$info['Zip'].'</PostalCode>
							<CountryCode>'.$info['CountryCode'].'</CountryCode>
							'.(($info['Residential']) ? "<ResidentialAddress />" : "").'
						</Address>
					</ShipTo> '
                                        . ( is_array($info[ShipFrom]) ? '
                                        <ShipFrom>
                                                <CompanyName>'.$info[ShipFrom][CompanyName].'</CompanyName>
                                                <AttentionName>'.$info[ShipFrom][AttentionName].'</AttentionName>
                                                <PhoneNumber>'.$info[ShipFrom][PhoneNumber].'</PhoneNumber>
                                                <Address>
                                                        <AddressLine1>'.$info[ShipFrom][Address1].'</AddressLine1>
                                                        <AddressLine2>'.$info[ShipFrom][Address2].'</AddressLine2>
                                                        <AddressLine3>'.$info[ShipFrom][Address3].'</AddressLine3>
                                                        <City>'.$info[ShipFrom][City].'</City>
                                                        <StateProvinceCode>'.$info[ShipFrom][State].'</StateProvinceCode>
                                                        <PostalCode>'.$info[ShipFrom][Zip].'</PostalCode>
                                                        <CountryCode>'.$info[ShipFrom][CountryCode].'</CountryCode>
                                                </Address>
                                        </ShipFrom>'
                                        : '' ) . '
					<PaymentInformation>
						<Prepaid>
							<BillShipper>
								<AccountNumber>'.$info['ShipperNumber'].'</AccountNumber>
							</BillShipper>
						</Prepaid>
					</PaymentInformation>
					<Service>
						<Code>'.$info['ServiceCode'].'</Code>
					</Service>';

				if(is_array($multipackage)){
					foreach($multipackage as $package){
						/**
						 * MULTIPACKAGE SHIPMENT
						 */
						$this->_action_xml .= '<Package>';

					if ($info['CountryCode'] == 'US') {

						$this->_action_xml .=
							'<ReferenceNumber>
								<Code>PO</Code>
								<Value>'.$info['OrderNumber'].'</Value>
							</ReferenceNumber>';


						if( $info['ReferenceNumber2'] != '' && $info['ReferenceNumber2Code'] != '' )
						{
							$this->_action_xml .=
								'<ReferenceNumber>
									<Code>' . $info['ReferenceNumber2Code'] .'</Code>
									<Value>'.$info['ReferenceNumber2'].'</Value>
								</ReferenceNumber>';
						}
					}

					$this->_action_xml .= '
						<PackagingType>
							<Code>'.$info['Packaging'].'</Code>
						</PackagingType>';

						if ($package['Length'] || $package['Width'] || $package['Height']) {
							$this->_action_xml .= '<Dimensions>
							                <UnitOfMeasurement>
							                    <Code>IN</Code>
							                </UnitOfMeasurement>
							                <Length>'.$package['Length'].'</Length>
							                <Width>'.$package['Width'].'</Width>
							                <Height>'.$package['Height'].'</Height>
							            </Dimensions>';
						}

						$this->_action_xml .= '<PackageWeight>
							<UnitOfMeasurement>
								<Code>LBS</Code>
							</UnitOfMeasurement>
							<Weight>'.$package['Weight'].'</Weight>
						</PackageWeight>

						<PackageServiceOptions>';

						if ($info['CountryCode'] == 'US' && $info['DCISType'] ) {
							$this->_action_xml .= '<DeliveryConfirmation>';

							$this->_action_xml .= '<DCISType>' . $info['DCISType'] . '</DCISType>';

							$this->_action_xml .= '</DeliveryConfirmation>';
						}

                        if ($package['InsuredValue'] && $package['InsuredValue'] > 0){

                            $this->_action_xml .= '
                                            <InsuredValue>
                                                <CurrencyCode>'.$info['CurrencyCode'].'</CurrencyCode>
                                                <MonetaryValue>'.$package['InsuredValue'].'</MonetaryValue>
                                            </InsuredValue>';
                        }

                     $this->_action_xml .= '</PackageServiceOptions>

					</Package>';
                     /**
                      * End Each Multi Package
                      */
					}

				} else {
					/**
					 * SINGLE PACKAGE SHIPMENT
					 */
					$this->_action_xml .= '<Package>';

					if ($info['CountryCode'] == 'US') {

						$this->_action_xml .=
							'<ReferenceNumber>
								<Code>PO</Code>
								<Value>'.$info['OrderNumber'].'</Value>
							</ReferenceNumber>';


						if( $info['ReferenceNumber2'] != '' && $info['ReferenceNumber2Code'] != '' )

						{

							$this->_action_xml .=
								'<ReferenceNumber>

									<Code>' . $info['ReferenceNumber2Code'] .'</Code>

									<Value>'.$info['ReferenceNumber2'].'</Value>

								</ReferenceNumber>';

						}
					}

					$this->_action_xml .= '
						<PackagingType>
							<Code>'.$info['Packaging'].'</Code>
						</PackagingType>';

						if ($info['Length'] || $info['Width'] || $info['Height']) {
							$this->_action_xml .= '<Dimensions>
							                <UnitOfMeasurement>
							                    <Code>IN</Code>
							                </UnitOfMeasurement>
							                <Length>'.$info['Length'].'</Length>
							                <Width>'.$info['Width'].'</Width>
							                <Height>'.$info['Height'].'</Height>
							            </Dimensions>';
						}

						$this->_action_xml .= '<PackageWeight>
							<UnitOfMeasurement>
								<Code>LBS</Code>
							</UnitOfMeasurement>
							<Weight>'.$info['Weight'].'</Weight>
						</PackageWeight>

						<PackageServiceOptions>';

						if ($info['CountryCode'] == 'US' && $info['DCISType'] ) {
							$this->_action_xml .= '<DeliveryConfirmation>';

							$this->_action_xml .= '<DCISType>' . $info['DCISType'] . '</DCISType>';

							$this->_action_xml .= '</DeliveryConfirmation>';
						}

                        if ($info['InsuredValue'] && $info['InsuredValue'] > 0){

                            $this->_action_xml .= '
                                            <InsuredValue>
                                                <CurrencyCode>'.$info['CurrencyCode'].'</CurrencyCode>
                                                <MonetaryValue>'.$info['InsuredValue'].'</MonetaryValue>
                                            </InsuredValue>';
                        }

                     $this->_action_xml .= '</PackageServiceOptions>

					</Package>';
                     /**
                      * End Single Package
                      */
				}
				$this->_action_xml .= '</Shipment></RatingServiceSelectionRequest>';

			//echo $this->_action_xml;
	}


	// ------------------------------------------------------------------------------------
	// produces xml for ship confirm action
	// ------------------------------------------------------------------------------------
	private function setShipConfirm($info)
	{

		// Shipping Multiple Packages in 1 Shipment - 5/27/2010
		if(isset($info['MultiPackage'])){

			$multipackage = $info['MultiPackage'];
			$this->num_packages = count($multipackage);

			foreach($multipackage as $k => $v){
				if(!is_array($v)){
					$multipackage[$k] = xmlentities(trim($v));
				}
			}
		}                

		foreach ($info as $k => $v) {
			if(!is_array($v)){
				$info[$k] = xmlentities(trim($v));
			}
		}
	//print_ar($info);
		$defaults = Array(
			'LabelPrintMethod' => 'EPL',
			'LabelStockSizeWidth' => '6',
			'LabelStockSizeHeight' => '4',
			'ShipperName' => '',
			'ShipperPhoneNumber' => '',
			'ShipperNumber' => '',
			'ShipperAddressLine1' => '',
			'ShipperAddressLine2' => '',
			'ShipperAddressLine3' => '',
			'ShipperCity' => '',
			'ShipperState' => '',
			'ShipperZip' => '',
			'ShipperCountryCode' => 'US',

			'CompanyName' => '',
			'AttentionName' => '',
			'PhoneNumber' => '',
			'AddressLine1' => '',
			'AddressLine2' => '',
			'AddressLine3' => '',
			'City' => '',
			'State' => '',
			'Zip' => '',
			'CountryCode' => 'US',
			'Residential' => false,
			'ServiceCode' => '',
			'OrderNumber' => '',
			'Packaging' => '',
			'Weight' => '',
			'Length' => '',
			'Width' => '',
			'Height' => '',

			'ReferenceNumber2Code' => '',
			'ReferenceNumber2'	   => '',
            'CurrencyCode'         => 'USD'
		);

		foreach ($defaults as $k => $v)
			if(!isset($info[$k])) $info[$k] = $v;
		if ($info['ShipmentDescription'] != '')
			$shipment_description = "<Description>$info[ShipmentDescription]</Description>";
		if ($info['InvoiceLineTotal'] > 0){
			$invoice_line_total = '<InvoiceLineTotal>
										<CurrencyCode>USD</CurrencyCode>
										<MonetaryValue>'.$info['InvoiceLineTotal'].'</MonetaryValue>
									</InvoiceLineTotal>';
		}

		$service_options = '';

		if($info['SaturdayDelivery']){
			$service_options .= "<SaturdayDelivery/>";
		}

		if($service_options){
			$service_options = "<ShipmentServiceOptions>$service_options</ShipmentServiceOptions>";
		}

		$this->_action_xml = '<?xml version="1.0"?'.'>
			<ShipmentConfirmRequest xml:lang="en-US">
				<Request>
					<TransactionReference>
						<CustomerContext>ShipConfirmUS</CustomerContext>
						<XpciVersion>1.0001</XpciVersion>
					</TransactionReference>
					<RequestAction>ShipConfirm</RequestAction>
					<RequestOption>nonvalidate</RequestOption>
				</Request>
				<LabelSpecification>
					<LabelPrintMethod>
						<Code>'.$info['LabelPrintMethod'].'</Code>
					</LabelPrintMethod>
					<LabelStockSize>
						<Height>'.$info['LabelStockSizeHeight'].'</Height>
						<Width>'.$info['LabelStockSizeWidth'].'</Width>
					</LabelStockSize>
					<LabelImageFormat>
						<Code>'.$info['LabelPrintMethod'].'</Code>
					</LabelImageFormat>
				</LabelSpecification>
				<Shipment>'.
					$service_options
					.
					($shipment_description != '' ? $shipment_description : '')
					.($invoice_line_total != '' ? $invoice_line_total : '').
					'<Shipper>
						<Name>'.$info['ShipperName'].'</Name>
						<AttentionName>'.$info['ShipperName'].'</AttentionName>
						<PhoneNumber>'.$info['ShipperPhoneNumber'].'</PhoneNumber>
						<ShipperNumber>'.$info['ShipperNumber'].'</ShipperNumber>
						<Address>
							<AddressLine1>'.$info['ShipperAddressLine1'].'</AddressLine1>
							<AddressLine2>'.$info['ShipperAddressLine2'].'</AddressLine2>
							<AddressLine3>'.$info['ShipperAddressLine3'].'</AddressLine3>
							<City>'.$info['ShipperCity'].'</City>
							<StateProvinceCode>'.$info['ShipperState'].'</StateProvinceCode>
							<PostalCode>'.$info['ShipperZip'].'</PostalCode>
							<CountryCode>'.$info['ShipperCountryCode'].'</CountryCode>
						</Address>
					</Shipper>                                        
					<ShipTo>
						<CompanyName>'.$info['CompanyName'].'</CompanyName>
						<AttentionName>'.$info['AttentionName'].'</AttentionName>
						<PhoneNumber>'.$info['PhoneNumber'].'</PhoneNumber>
						<Address>
							<AddressLine1>'.$info['AddressLine1'].'</AddressLine1>
							<AddressLine2>'.$info['AddressLine2'].'</AddressLine2>
							<AddressLine3>'.$info['AddressLine3'].'</AddressLine3>
							<City>'.$info['City'].'</City>
							<StateProvinceCode>'.$info['State'].'</StateProvinceCode>
							<PostalCode>'.$info['Zip'].'</PostalCode>
							<CountryCode>'.$info['CountryCode'].'</CountryCode>
							'.(($info['Residential']) ? "<ResidentialAddress />" : "").'
						</Address>
					</ShipTo> '
					. '<RateInformation>
					<NegotiatedRatesIndicator/>
					</RateInformation>'
                                        . ( is_array($info[ShipFrom]) ? '
                                        <ShipFrom>
                                                <CompanyName>'.$info[ShipFrom][CompanyName].'</CompanyName>
                                                <AttentionName>'.$info[ShipFrom][AttentionName].'</AttentionName>
                                                <PhoneNumber>'.$info[ShipFrom][PhoneNumber].'</PhoneNumber>
                                                <Address>
                                                        <AddressLine1>'.$info[ShipFrom][Address1].'</AddressLine1>
                                                        <AddressLine2>'.$info[ShipFrom][Address2].'</AddressLine2>
                                                        <AddressLine3>'.$info[ShipFrom][Address3].'</AddressLine3>
                                                        <City>'.$info[ShipFrom][City].'</City>
                                                        <StateProvinceCode>'.$info[ShipFrom][State].'</StateProvinceCode>
                                                        <PostalCode>'.$info[ShipFrom][Zip].'</PostalCode>
                                                        <CountryCode>'.$info[ShipFrom][CountryCode].'</CountryCode>
                                                </Address>
                                        </ShipFrom>' 
                                        : '' ) . '
					<PaymentInformation>
						<Prepaid>
							<BillShipper>
								<AccountNumber>'.$info['ShipperNumber'].'</AccountNumber>
							</BillShipper>
						</Prepaid>
					</PaymentInformation>
					<Service>
						<Code>'.$info['ServiceCode'].'</Code>
					</Service>';

				if(is_array($multipackage)){
					foreach($multipackage as $package){
						/**
						 * MULTIPACKAGE SHIPMENT
						 */
						$this->_action_xml .= '<Package>';

					if ($info['CountryCode'] == 'US') {

						$this->_action_xml .=
							'<ReferenceNumber>
								<Code>PO</Code>
								<Value>'.$info['OrderNumber'].'</Value>
							</ReferenceNumber>';


						if( $info['ReferenceNumber2'] != '' && $info['ReferenceNumber2Code'] != '' )
						{
							$this->_action_xml .=
								'<ReferenceNumber>
									<Code>' . $info['ReferenceNumber2Code'] .'</Code>
									<Value>'.$info['ReferenceNumber2'].'</Value>
								</ReferenceNumber>';
						}
					}

					$this->_action_xml .= '
						<PackagingType>
							<Code>'.$info['Packaging'].'</Code>
						</PackagingType>';

						if ($package['Length'] || $package['Width'] || $package['Height']) {
							$this->_action_xml .= '<Dimensions>
							                <UnitOfMeasurement>
							                    <Code>IN</Code>
							                </UnitOfMeasurement>
							                <Length>'.$package['Length'].'</Length>
							                <Width>'.$package['Width'].'</Width>
							                <Height>'.$package['Height'].'</Height>
							            </Dimensions>';
						}

						$this->_action_xml .= '<PackageWeight>
							<UnitOfMeasurement>
								<Code>LBS</Code>
							</UnitOfMeasurement>
							<Weight>'.$package['Weight'].'</Weight>
						</PackageWeight>

						<PackageServiceOptions>';

						if ($info['CountryCode'] == 'US' && $info['DCISType'] ) {
							$this->_action_xml .= '<DeliveryConfirmation>';

							$this->_action_xml .= '<DCISType>' . $info['DCISType'] . '</DCISType>';

							$this->_action_xml .= '</DeliveryConfirmation>';
						}

                        if ($package['InsuredValue'] && $package['InsuredValue'] > 0){

                            $this->_action_xml .= '
                                            <InsuredValue>
                                                <CurrencyCode>'.$info['CurrencyCode'].'</CurrencyCode>
                                                <MonetaryValue>'.$package['InsuredValue'].'</MonetaryValue>
                                            </InsuredValue>';
                        }

                     $this->_action_xml .= '</PackageServiceOptions>

					</Package>';
                     /**
                      * End Each Multi Package
                      */
					}

				} else {
					/**
					 * SINGLE PACKAGE SHIPMENT
					 */
					$this->_action_xml .= '<Package>';

					if ($info['CountryCode'] == 'US') {

						$this->_action_xml .=
							'<ReferenceNumber>
								<Code>PO</Code>
								<Value>'.$info['OrderNumber'].'</Value>
							</ReferenceNumber>';


						if( $info['ReferenceNumber2'] != '' && $info['ReferenceNumber2Code'] != '' )

						{

							$this->_action_xml .=
								'<ReferenceNumber>

									<Code>' . $info['ReferenceNumber2Code'] .'</Code>

									<Value>'.$info['ReferenceNumber2'].'</Value>

								</ReferenceNumber>';

						}
					}

					$this->_action_xml .= '
						<PackagingType>
							<Code>'.$info['Packaging'].'</Code>
						</PackagingType>';

						if ($info['Length'] || $info['Width'] || $info['Height']) {
							$this->_action_xml .= '<Dimensions>
							                <UnitOfMeasurement>
							                    <Code>IN</Code>
							                </UnitOfMeasurement>
							                <Length>'.$info['Length'].'</Length>
							                <Width>'.$info['Width'].'</Width>
							                <Height>'.$info['Height'].'</Height>
							            </Dimensions>';
						}

						$this->_action_xml .= '<PackageWeight>
							<UnitOfMeasurement>
								<Code>LBS</Code>
							</UnitOfMeasurement>
							<Weight>'.$info['Weight'].'</Weight>
						</PackageWeight>

						<PackageServiceOptions>';

						if ($info['CountryCode'] == 'US' && $info['DCISType'] ) {
							$this->_action_xml .= '<DeliveryConfirmation>';

							$this->_action_xml .= '<DCISType>' . $info['DCISType'] . '</DCISType>';

							$this->_action_xml .= '</DeliveryConfirmation>';
						}

                        if ($info['InsuredValue'] && $info['InsuredValue'] > 0){

                            $this->_action_xml .= '
                                            <InsuredValue>
                                                <CurrencyCode>'.$info['CurrencyCode'].'</CurrencyCode>
                                                <MonetaryValue>'.$info['InsuredValue'].'</MonetaryValue>
                                            </InsuredValue>';
                        }

                     $this->_action_xml .= '</PackageServiceOptions>

					</Package>';
                     /**
                      * End Single Package
                      */
				}
				$this->_action_xml .= '</Shipment></ShipmentConfirmRequest>';

			//echo $this->_action_xml;
	}


	// ------------------------------------------------------------------------------------
	// produces xml for ship accept action
	// ------------------------------------------------------------------------------------
	public function setShipAccept(&$digest)
	{
		$this->_action_xml = '<?xml version="1.0"?'.'>
			<ShipmentAcceptRequest>
				<Request>
					<TransactionReference>
						<CustomerContext>TR01</CustomerContext>
						<XpciVersion>1.0001</XpciVersion>
					</TransactionReference>
					<RequestAction>ShipAccept</RequestAction>
					<RequestOption>01</RequestOption>
				</Request>
				<ShipmentDigest>'.$digest.'</ShipmentDigest>
			</ShipmentAcceptRequest>';
	}


	// ------------------------------------------------------------------------------------
	// produces xml for ship void action
	// ------------------------------------------------------------------------------------
	public function setShipVoid($tracking_number)
	{
		$this->_action_xml = '<?xml version="1.0"?'.'>
			<VoidShipmentRequest>
				<Request>
					<TransactionReference>
						<CustomerContext>Void</CustomerContext>
						<XpciVersion>1.0001</XpciVersion>
					</TransactionReference>
					<RequestAction>Void</RequestAction>
					<RequestOption>1</RequestOption>
				</Request>';

		if(is_array($tracking_number)){
			$this->_action_xml .= '<ExpandedVoidShipment>';
			$this->_action_xml .= '<ShipmentIdentificationNumber>'.$tracking_number[0].'</ShipmentIdentificationNumber>';
			foreach($tracking_number as $num){
				$this->_action_xml .= '<TrackingNumber>'.$num.'</TrackingNumber>';
			}
			$this->_action_xml .= '</ExpandedVoidShipment>';
		} else {
			$this->_action_xml .= '<ShipmentIdentificationNumber>'.$tracking_number.'</ShipmentIdentificationNumber>';
		}

		$this->_action_xml .= '</VoidShipmentRequest>';
	}

	// ------------------------------------------------------------------------------------
	// produces xml for ship void action
	// ------------------------------------------------------------------------------------
	public function setTracking($tracking_number)
	{
		$this->_action_xml = '<?xml version="1.0"?'.'>
			<TrackRequest>
				<Request>
					<TransactionReference>
						<CustomerContext>Example 1</CustomerContext>
						<XpciVersion>1.0001</XpciVersion>
					</TransactionReference>
					<RequestAction>Track</RequestAction>
					<RequestOption>activity</RequestOption>
				</Request>
				<TrackingNumber>'.$tracking_number.'</TrackingNumber>
			</TrackRequest>';
	}

	// ------------------------------------------------------------------------------------
	// generic function to send request to ups
	//	return data from ups
	//
	// *** REQUIRES libcurl and PEAR::Net_Curl ***
	// ------------------------------------------------------------------------------------
	private function _request($url)
	{
		global $CFG;

		if (!$this->_in_testing) {
			$url = self::UPS_HOST_PROD . $url;
		}
		else {
			$url = self::UPS_HOST_DEV . $url;
		}

		if ($this->_action_xml == '') {
			die('Error: No request set');
		}

		$conn = new Net_Curl($url);
		// if (Net_Curl::isError($conn)) {
		//	die(sprintf('Curl Error [%d]: %s',
		//			$conn->getCode(), $conn->getMessage()));
		//}

		$conn->type='post';
		$conn->fields = $this->_access_xml.$this->_action_xml;
		$conn->timeout = 120;

		if($this->_debug){
			// print "XML: ".htmlspecialchars($action_xml) . " *** ";
		}

		$data = $conn->execute();
		/*
		if (Net_Curl::isError($data)) {
			die(sprintf('Curl Error [%d]: %s',
					$data->getCode(), $data->getMessage()));
		}*/

		$conn->close();

		if ($this->_debug) {
			echo "<pre style=\"font-family: monaco; font-size: 9pt; color:black\">";
			echo "<b>POST TO:</b> <i>$url</i><br>";
			echo "<p><b>REQUEST:</b><br>";
			echo $this->_colorize($this->_access_xml);
			echo "<br>";
			echo $this->_colorize($this->_action_xml);
			echo "<p><b>RESPONSE:</b><br>";
			echo $this->_colorize($data);
			echo "</pre>";
		}
		$oldData = $data;
		$data = simplexml_load_string($data);
//		mail("rachel@tigerchef.com", "data from line 999 of apps/ups.php", $oldData. "<br/>and request was<br/>".$this->_access_xml.$this->_action_xml);
		if ($data->Response->ResponseStatusCode != 1) {
			//mail("rachel@tigerchef.com", "data from line 999 of apps/ups.php", $oldData);
			
			$err = $data->Response->Error;
			$err_code = (string) $err->ErrorCode;
			$err_desc = (string) $err->ErrorDescription;
			$err_serverity = (string) $err->ErrorSeverity;
			$err_loc = (string) $err->ErrorLocation->ErrorLocationElementName;

			throw new Exception(sprintf('UPS Error [%d]: %s',
					$err_code, "$err_desc in $err_loc ($err_serverity)"));
		}
		else {
			return $data;
		}
	}


	// ------------------------------------------------------------------------------------
	// colorizes xml in html format (for debug)
	// ------------------------------------------------------------------------------------
	private function _colorize($t)
	{
   		$t = htmlentities($t);
   		return ereg_replace("&gt;([^&]*)&lt;","&gt;<font color=red>\\1</font>&lt;",$t);
    }


    public function getLastXmlRequest(){
    	return $this->_action_xml;
    }
}

?>