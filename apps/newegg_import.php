<? 
	// Created by RSunness Apr '12
	class NeweggImport
	{
		static function import_new_orders_files_via_ftp($ftp_server, $ftp_user_name, $ftp_password, $remote_dir, $local_dir = "/tmp")
		{
			global $CFG;
			$newegg_store_data = Store::get1($CFG->newegg_store_id);
			$last_imported_time = strtotime($newegg_store_data['last_import_time']);
 			$last_imported_time = $last_imported_time - (60*60*8); // because timestamp is for 7-8 hrs earlier than actual file creation time in our timezone
			// set up basic connection
			$conn_id = ftp_connect($ftp_server);

			// login with username and password
			$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_password);

			if ($login_result)
			{
				if (ftp_chdir($conn_id, $remote_dir)) 
				{			
					// get files created since the Newegg script last ran
					$contents = ftp_nlist($conn_id, ".");
					foreach ($contents as $one_file)
					{
						$file_mod_time = ftp_mdtm($conn_id, $one_file);	
//						echo date('Y-m-d H:i:s', $file_mod_time)." for file $one_file with file mod time $file_mod_time and last import $last_imported_time\n";
						if ($file_mod_time > $last_imported_time)
						{
							$local_file = $local_dir. "/".$one_file;
							
							if (ftp_get($conn_id, $local_file, $one_file, FTP_BINARY)) 
							{
   								//echo "Successfully written to $local_file\n";
   								NeweggImport::parse_orders_file($local_file);
							} 
							else 
							{
   								echo "There was a problem--was trying to write $one_file to $local_file\n";
							}
						}
					}
				} 
				else 
				{ 
   					echo "Couldn't change directory\n";
				} 				
	
				// close the connection
				ftp_close($conn_id);
				unset($info);
				$info['last_import_time'] = date("Y-m-d H:i:s");
				Store::update($CFG->newegg_store_id, $info);
				
			}	
			else echo "Couldn't even log into server";
		}
		
		static function parse_orders_file($full_file_name)
		{			
			global $CFG;
			
			$z = new XMLReader;					
			$z->open($full_file_name);			

			// move to the first <Order /> node
			while ($z->read() && $z->name !== 'Order');

			// now that we're at the right depth, hop to the next <Order/> until the end of the tree
			while ($z->name === 'Order')
			{
				$doc = new DOMDocument;				
				/* old method, replaced by using simplexml_load_string so that we can extract the CDATA easily
    			$node = simplexml_import_dom($doc->importNode($z->expand(), true)); */
				 
    			$node1 = $doc->importNode($z->expand(), true);
    			$doc->appendChild($node1);
	 			$node = simplexml_load_string($doc->saveXML(), "SimpleXMLElement", LIBXML_NOCDATA);
	 			
    			unset($order_info);

    			$existing_order = Orders::getOrderIdFromGoogleCheckoutOrderNumber($node->OrderNumber);
    			if ($existing_order) echo "\nOrder ".$node->OrderNumber." already is in the system\n";
				else 
				{
					//print_r($node);
					
					switch((string)$node->ShipTo->ShippingMethod)
					{
						case "One-Day Shipping":
							$shipping_id = 4;
							break;			
						case "Two-Day Shipping":
							$shipping_id = 3;
							break;
						case "Expedited Shipping (3-5 business days)":
							$shipping_id = 2;
							break;																				
						case "Standard Shipping (5-7 business days)":
						default:	
							$shipping_id = 1;
							break;											
					}
					
					$order_total = (float) $node->OrderTotal;
					$order_shipping = (float) $node->ShipTotal;
					$order_subtotal = $order_total - $order_shipping;
					$order_info = array(
			    		'date' => date('Y-m-d H:i:s', (strtotime((string)$node->OrderDateTime)+ 10800)), // add 10800 (3 hrs) to convert from Pacific time to Eastern
			    		'customer_id' => 0,
						'3rd_party_order_number' => (string) $node->OrderNumber,
			    		'billing_first_name' => (string) $node->ShipTo->FirstName,
			    		'billing_last_name' => (string) $node->ShipTo->LastName,
			    		'billing_address1' => (string) $node->ShipTo->Address1,
			    		'billing_address2' => (string) $node->ShipTo->Address2,
			    		'billing_city' => (string) $node->ShipTo->City,
			    		'billing_state' => (string) $node->ShipTo->StateCode,
			    		'billing_zip' => (string) $node->ShipTo->Zip,
			    		'billing_phone' => (string) $node->ShipTo->Phone,
			    		'billing_company' => (string) $node->ShipTo->Company,
			    		'shipping_first_name' => (string) $node->ShipTo->FirstName,
			    		'shipping_last_name' => (string) $node->ShipTo->LastName,
			    		'shipping_address1' => (string) $node->ShipTo->Address1,
			    		'shipping_address2' => (string) $node->ShipTo->Address2,
			    		'shipping_city' => (string) $node->ShipTo->City,
			    		'shipping_state' => (string) $node->ShipTo->StateCode,
			    		'shipping_zip' => (string) $node->ShipTo->Zip,
			    		'shipping_phone' => (string) $node->ShipTo->Phone,
						'shipping_company' => (string) $node->ShipTo->Company,			    		

			    		'is_repeat_order' => 'N',

			    		'shipping_charged' => 'Y',
			    		'current_status_id' => $CFG->new_order_status_id,
			    		'order_tax_option' => $CFG->newegg_store_id,
			    		'email' => (string) $node->ShipTo->Email,

			    		'imported_from' => 'Newegg - ' . $full_file_name,
			   		 	'date_added' => date('Y-m-d H:i:s'),
			    		'shipping_id' => $shipping_id, 
			
			    	    'subtotal'	    => $order_subtotal,
			    		'shipping'	    => $order_shipping,
		    		);

		    		$order_id = Orders::insert($order_info);
		    		//echo "\n<p> Inserted Order #$order_id\n";
		    		//print_r($order_info);
		    		foreach ($node->ItemList->Item as $cur_item)
		    		{
		    			$cur_sku = $cur_item->SellerPartNumber;
		    			$cur_qty = $cur_item->Quantity;
		    			$cur_unit_price = $cur_item->UnitPrice;
		    			//echo "\nHere with qty $cur_qty of item $cur_sku with unit price $cur_unit_price";
		    			unset($product_id);
		    			unset($product_option_id);
		    			// first see if the sku matches an option that is not deleted and is active
		    			$product_option = ProductOptions::getByVendorSku($cur_sku, true, true);
			    		// 	if there was no such product option, see if it matches one that's not deleted but is active
			    		if (!$product_option) $product_option = ProductOptions::getByVendorSku($cur_sku, true);
			    		// otherwise, just match it to a sku
		    			if ($product_option) 
		    			{
		    				$product_id = $product_option[0]['product_id'];
		    				$product_option_id = $product_option[0]['product_option_id'];
		    				// get the prod, too
		    				$the_prod = Products::get1($product_id);
		    			}
		    			else 
		    			{
		    				$the_prod = Products::get1ByModel($cur_sku, true);
		    				if (!$the_prod) $the_prod = Products::get1ByModel($cur_sku, false);
		    				if (!$the_prod) 
		    				{
		    					mail ("rachel@tigerchef.com", "Newegg SKU $cur_sku on order $order_id does not exist in our system", "");
		    					continue;		    				 
		    				}
		    			}
		    			//echo "\ngot the prod with id " . $the_prod['id'];

		    			$order_item_info = array(
				    	'order_id' => $order_id,
				    	'product_id' => $the_prod['id'],
				    	'price' => $cur_unit_price,
				    	'qty' => $cur_qty,
				    	'order_item_status_id' => 1,
			    	);

			    	$oItem = Orders::insertItem($order_item_info);

			    	if($product_option)
			    	{
						$option = Products::getProductOption($product_option_id);

						$option_info = array(
				    		'order_item_id' => $oItem,
				    		'product_option_id' => $product_option_id,
				    		'value' => $option['value'],
				    		'additional_price' => $option['additional_price']
						);

						Orders::insertItemOption( $option_info );
			    	}	
					
		    		} // end foreach item		
		    			    	$chargeInfo = array(
			    		'pid'      => 0,
			    		'date'     => date('Y-m-d H:i:s'),
			    		'order_id' => $order_id,
			    		'action'   => 'sale',
			    		'transaction_id' => $oItem,
			    		'amount'   => ($order_subtotal + $order_shipping),
			    		'response_code' => 1,
			    		'response_msg' => 'Payment on Newegg',
			    		'notes'    => 'Payment on Newegg'
		    );
		    $charge_id = Orders::insertCharge($chargeInfo);
		    Orders::insertOrderChargeBreakdown($charge_id, $order_subtotal , $order_shipping);	    				    		
				} // end else that order is not already in the system				    								
			    // go to next <Order /> in the file
    			$z->next('Order');
			} // end while there are more orders in the file			
		} // end function parse_orders_file 			
	}
?>