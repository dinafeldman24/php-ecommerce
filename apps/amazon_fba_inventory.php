<?
class AmazonFbaInventory
{ 
	static function insert($info)
	{
		return db_insert('amazon_fba_inventory',$info);
	}
	
	static function empty_table()
	{
		mysql_query("TRUNCATE TABLE amazon_fba_inventory") or die(mysql_error());
	}
	
	static function delete($id)
	{
		return db_delete('amazon_fba_inventory',$id);
	}
	
	static function get($sku="",$has_afn_fulfillable_quantity="", $order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
			$sql .= " COUNT(DISTINCT(amazon_fba_inventory.id)) AS total ";
		} else {
			$sql .= " amazon_fba_inventory.* ";
		}
	
		$sql .= " FROM amazon_fba_inventory ";
	
		$sql .= " WHERE 1 ";
	
		if ($sku != "") {
			$sql .= " AND amazon_fba_inventory.sku = '$sku' ";
		}
		if ($has_afn_fulfillable_quantity != "" && $has_afn_fulfillable_quantity === true)
		{
			$sql .= " AND amazon_fba_inventory.afn_fulfillable_quantity > 0 ";			
		}
	
		if (!$get_total){
			$sql .= ' GROUP BY amazon_fba_inventory.sku ';
	
			if($order){
				$sql .= " ORDER BY ";
	
				$sql .= addslashes($order);
	
				if ($order_asc !== '' && !$order_asc) {
					$sql .= ' DESC ';
				}
			}
		}
	
		if ($limit > 0) {
			$sql .= db_limit($limit,$start);
		}
	
		if (!$get_total) {
			$ret = db_query_array($sql);
		} else {
			$ret = db_query_array($sql,'',true);
		}
	
		return $ret;
	}
	
	static function get1($sku)
	{
		if (!$sku) return false;
		$result = self::get($sku);
	
		return $result[0];
	}
	
	static function update_prods_with_asins()
	{ 
		$sql = "UPDATE `amazon_fba_inventory`, `products` set products.asin = amazon_fba_inventory.asin WHERE amazon_fba_inventory.sku = products.vendor_sku";
		mysql_query($sql) or die(mysql_error());
		$sql2 = "UPDATE `amazon_fba_inventory`, `product_options` set product_options.asin = amazon_fba_inventory.asin WHERE amazon_fba_inventory.sku = product_options.vendor_sku";
		mysql_query($sql2) or die(mysql_error());						
		
		$sql = "UPDATE `amazon_fba_inventory`, `products` set products.asin = amazon_fba_inventory.asin WHERE amazon_fba_inventory.sku = products.mfr_part_num";
		mysql_query($sql) or die(mysql_error());
		$sql2 = "UPDATE `amazon_fba_inventory`, `product_options` set product_options.asin = amazon_fba_inventory.asin WHERE amazon_fba_inventory.sku = product_options.mfr_part_num";
		mysql_query($sql2) or die(mysql_error());
	}
	
	function get_fba_including_alternate_listings($vendor_sku,$mfr_part_num,$option_vendor_sku = 0,$option_mfr_part_num=''){
		
		$vendor_sku_arr = explode('@',$vendor_sku);
		$vendor_sku = rtrim($vendor_sku_arr[0],'FBA');
		
		$mfr_part_num_arr = explode('@',$mfr_part_num);
		$mfr_part_num = rtrim($mfr_part_num_arr[0],'FBA');
		
		$pk = '';
		foreach ($vendor_sku_arr as $sku){
			if(preg_match ( '/(\d+)PK/' , $sku, $matches )){
				$pk = '@' . $sku;
				break;
			}
		}
		
		if($option_vendor_sku) 
		{
			$option_vendor_sku = explode('@',$option_vendor_sku);
			$option_vendor_sku = rtrim($option_vendor_sku[0],'FBA');
		}
		if($option_mfr_part_num){
			$option_mfr_part_num = explode('@',$option_mfr_part_num);
			$option_mfr_part_num = rtrim($option_mfr_part_num[0],'FBA');
		}
		
// 		$sql = " SELECT SUM(afn_fulfillable_quantity) AS fba_qty
// 		FROM amazon_fba_inventory
// 		WHERE (
// 		( amazon_fba_inventory.sku LIKE '$vendor_sku"."%"."' OR amazon_fba_inventory.sku LIKE '$mfr_part_num"."%"."' )
// 		";
// 		if($option_vendor_sku){
// 		$sql .= "  OR
// 		( amazon_fba_inventory.sku LIKE '$option_vendor_sku"."%"."' OR amazon_fba_inventory.sku LIKE '$option_mfr_part_num"."%"."' )";
// 		}
// 		$sql .= " )";
		
		  
		$sql = " SELECT SUM(afn_fulfillable_quantity) AS fba_qty
			FROM amazon_fba_inventory
			WHERE 1 ";
		if($option_vendor_sku){
			$sql .= " AND ( '$option_vendor_sku' = SUBSTRING_INDEX( SUBSTRING_INDEX(amazon_fba_inventory.sku,'@',1),  'FBA', 1 ) OR '$option_mfr_part_num' = SUBSTRING_INDEX( SUBSTRING_INDEX(amazon_fba_inventory.sku,'@',1),  'FBA', 1 )) ";
		}else{
			$sql .= " AND ( '$vendor_sku' = SUBSTRING_INDEX( SUBSTRING_INDEX(amazon_fba_inventory.sku,'@',1),  'FBA', 1 ) OR '$mfr_part_num' = SUBSTRING_INDEX( SUBSTRING_INDEX(amazon_fba_inventory.sku,'@',1),  'FBA', 1 ))";
		}
		
		if($pk) {
			$sql .= " AND amazon_fba_inventory.sku REGEXP '$pk'";
		} else {
			$sql .= " AND amazon_fba_inventory.sku REGEXP '@[[:digit:]]+PK' = 0 ";
		}

		//print_ar($sql);
		$fba_qty = db_query_array($sql);
		//print_ar($fba_qty);
		//print_ar($fba_qty[0]);
		return $fba_qty[0]['fba_qty'];
		
	}
		
}
?>