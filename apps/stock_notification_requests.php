<?
class StockNotificationRequest
{
	public static function insert($info)
	{
	    return db_insert('stock_notification_requests', $info);
	}
	
	public static function getRequestsByProd($product_id)
	{
		$sql = "SELECT DISTINCT email 
				FROM `stock_notification_requests` 
				WHERE prod_id = $product_id
				AND notification_sent = 'N'";
		
		return db_query_array($sql);	
	}
	public static function sendStockNotificationEmail($product_id)
	{
		$email_address_list = StockNotificationRequest::getRequestsByProd($product_id);
		if ($email_address_list)
		{
			$the_prod = Products::get1($product_id);
			$product_name = $the_prod['name'];
			$the_url = Catalog::makeProductLink_($the_prod);
			$datum['body'] = "You recently submitted a stock request on Tigerchef.com for $product_name. This is to inform you that the item is currently back in stock! <br><br>
			Quantities are limited; please come view and purchase the product on 
			our website at <a href='$the_url'>$the_url</a>, or call us at 1-877-928-4437! <br><br>Please note: This email does not guarantee stock on the item. Item may sell out quickly.<br><br> 
			We look forward to serving you!<br><br>";
			
			foreach ($email_address_list as $email_address)
			{
				$the_address = $email_address['email'];
				TigerEmail::sendOne("Valued Customer",$the_address,'blank',$datum,'Per Your Request: TigerChef Stock Notification');
				
				mysql_query("UPDATE `stock_notification_requests`
				SET notification_sent = 'Y' 
				WHERE 
				prod_id = $product_id
				AND email = '$the_address'
				AND notification_sent = 'N'") or die(mysql_error());								
			}
		}
	}
	public static function notifyTigerchef($info)
	{
		$email_to = "estee@tigerchef.com";
		
		$the_prod = Products::get1($info['prod_id']);
		$product_name = $the_prod['name'];
		$the_url = Catalog::makeProductLink_($the_prod);
		$email_body = "FYI, a stock notification request for $product_name ($the_url) was just submitted by ". $info['email'];
		mail($email_to, "Stock Notification Request Submitted", $email_body, "From: custserv@tigerchef.com\r\n");
	}

} 
?>