<?php

class Invoices
{
	public static function insert($info)
	{
		return db_insert('invoices',$info);
	}

	public static function update($id,$info)
	{
		return db_update('invoices',$id,$info);
	}

	public static function delete($id)
	{
		return db_delete('invoices',$id);
	}

	public static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='', $name='',$type='invoice')
	{
		$sql = "SELECT invoices.*, store.id as store_id
				FROM invoices
				LEFT JOIN store ON store.invoice_id = invoices.id
				WHERE 1 ";

		if ($id > 0)
		{
			$sql .= " AND invoices.id = $id ";
		}
		if ($begins_with != '')
		{
			$sql .= " AND invoices.name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '')
		{
			$fields = Array('invoices.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if ($name != '')
		{
			$sql .= " AND invoices.name = '$name' ";
		}
		if($type){
			$sql .= " AND invoices.type = '".db_esc($type)."' ";
		}

		$sql .= " GROUP BY invoices.id ";

		$sql .= "ORDER BY ";

		if ($order == '')
		{
			$sql .= 'invoices.name';
		}
		else
		{
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc)
		{
			$sql .= ' DESC ';
		}

		return db_query_array($sql);
	}

	public static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = self::get($id,'','','','','','','');
		return $result[0];
	}

	public static function get1ByName($name)
	{
		$result = self::get(0,'','','','', $name,'','');
		if( count( $result ) )
			return $result[0];
		else
			return false;
	}

	public static function get_invoice_template($name,$type="normal")
	{
	    global $CFG;

//	    return file_get_contents(sprintf("%s/invoices/purchase_order_pdf.html", $CFG->dirroot));

		if($name == ''){
			$name = "justcharger"; // default the store name to use TigerChef related templates.
		}

	    if($type=='po'){
	    	$retval = file_get_contents(sprintf("%s/invoices/purchase_order.html", $CFG->dirroot));
	    	if(!$retval){
	    		TigerEmail::sendAdminEmail("Couldn't open invoice template: ". sprintf("%s/invoices/purchase_order.html", $CFG->dirroot), 'Error with Invoice or Packing Slip');
	    	}
	    	return $retval;
	    } else if($type == "po_pdf"){
	    	$retval = file_get_contents(sprintf("%s/invoices/purchase_order_pdf.html", $CFG->dirroot));
	    	if(!$retval){
	    		TigerEmail::sendAdminEmail("Couldn't open invoice template: ". sprintf("%s/invoices/purchase_order_pdf.html", $CFG->dirroot), 'Error with Invoice or Packing Slip');
	    	}
	    	return $retval;
	    } else if($type == 'packing_slip'){
	    	$retval = file_get_contents(sprintf("%s/invoices/%s-ups-label.html", $CFG->dirroot, $name));
	    	if(!$retval){
	    		TigerEmail::sendAdminEmail("Couldn't open invoice template: ". sprintf("%s/invoices/%s-ups-label.html", $CFG->dirroot, $name), 'Error with Invoice or Packing Slip');
	    	}
	    	return $retval;
	    }

	    if (!$name)
	    {
	        return '';
	    }

	    $retval = file_get_contents(sprintf("%s/invoices/%s-invoice.html", $CFG->dirroot, $name));
	    if(!$retval){
    		TigerEmail::sendAdminEmail("Couldn't open invoice template: ". sprintf("%s/invoices/%s-invoice.html", $CFG->dirroot, $name), 'Error with Invoice or Packing Slip');
    	}
    	return $retval;
	}
}

?>