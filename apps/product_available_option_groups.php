<?php

/*****************
*   Class for interface
*   with dynamic product 
*   option groups
*   Hadassa Golovenshitz
*   June 16 2015
*
*****************/

class ProductAvailableOptionGroups {
    
    var $id = null;
    var $product_id;
    var $product_name;
    var $title;
    var $description;
    var $parent;
    var $hide_children;

    function ProductAvailableOptionGroups($id = null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    function populate($id){
        $sql = 'SELECT * FROM product_available_option_groups WHERE
            id = ' . (int)$id;
            $info = db_query_array($sql);
            if(!is_array($info)){
                return;
            }
            $info = $info[0];
            $this->id = $id;
            $this->product_id = $info['product_id'];
            $this->title = $info['title'];
            $this->description = $info['description'];
            $this->parent = $info['parent_id'];
            $this->hide_children = $info['hide_children'];
    }

    function toJson(){
        $info = array(
            'hide_children' => $this->hide_children,
            'parent' => $this->parent,
            'title' => $this->title,
            'description' => $this->description,
            'id' => $this->id,
            'product_id' => $this->product_id,
        );
        return json_encode($info);
    }

    static function updateOrInsert($info, $id = null){
        if(!is_null($id) && !empty($id) && $id){
            $res = db_update('product_available_option_groups',$id, $info);
        }
        else{
            $res = db_insert('product_available_option_groups', $info);
        }
        return $res;
    }

    static function delete($id){
        $res =  db_delete('product_available_option_groups', $id);
        return $res;
    }

    
    static function getDescription($id){
        $sql = "SELECT description FROM product_available_option_groups WHERE id = " . (int)$id;
        $info = db_query_array($sql);
        return ($info) ? $info[0]['description'] : '';
    }
}
?>
