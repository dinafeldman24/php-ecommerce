<?php

class ProductSuppliers 
{
	
	public static function insert($info)
	{		
		return db_insert('product_suppliers', $info,'',true);
	}
	
	public static function update($product_id, $brand_id, $info)
	{
	    return db_update('product_suppliers', array($product_id, $brand_id), $info, array('product_id', 'brand_id'));
	    
		//return db_update('product_suppliers', $id, $info);
	}
	
	public static function update2($product_id, $info)
	{
	    return db_update('product_suppliers', $product_id, $info, 'product_id');
	}
	
	public static function delete($product_id, $brand_id)
	{
	    return db_delete('product_suppliers', array($product_id, $brand_id), array('product_id', 'brand_id'));
		//return db_delete('product_suppliers', $id);
	}

	public static function get($product_id=0, $brand_id=0, $order='', $order_asc='', $delivery_method='', $primary_supplier='', $brand_type = '', $vendor_sku = '', $mfr_part_num = '', $supplier_dist_sku = '')
	{
		$sql = "SELECT product_suppliers.*, brands.name as brand_name, products.name as product_name,
		               products.vendor_sku as product_vendor_sku, products.price, products.vendor_price, products.id as the_product_id
		               
				FROM product_suppliers 
				LEFT JOIN brands ON brands.id=product_suppliers.brand_id
				LEFT JOIN products ON products.id=product_suppliers.product_id
				WHERE 1 
				";
		
		if ($product_id > 0) 
		{
			$sql .= " AND product_suppliers.product_id = $product_id ";
		}
		if ($brand_id > 0)
		{
		    $sql .= " AND product_suppliers.brand_id = $brand_id ";    
		}
		
		if ($brand_type != '')
		{
		    $sql .= " AND brands.type = $brand_type ";    
		}
		
		if ($vendor_sku != '')
		{
			$sql .= " AND products.vendor_sku = '$vendor_sku' ";
		}
		if ($mfr_part_num != '')
		{
			$sql .= " AND products.mfr_part_num = '$mfr_part_num' ";
		}
		if ($supplier_dist_sku != '')
		{
		    $sql .= " AND product_suppliers.dist_sku = '$supplier_dist_sku' ";    
		}		
		if ($delivery_method != '')
		{
		    $sql .= " AND product_suppliers.delivery_method = $delivery_method ";    
		}
		if ($primary_supplier != '')
		{
		    $sql .= " AND product_suppliers.primary_supplier = $primary_supplier ";    
		}
					
		$sql .= " ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'product_suppliers.product_id';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' DESC ';
		}		
    	
		return db_query_array($sql);
	}
	
	public static function get_by_product_ids($prod_ids = array())
	{
	    $sql = "SELECT product_supplier.*
				FROM product_suppliers 
				WHERE 1 
				";
	   
	    if ($prod_ids && is_array($prod_ids))
	    {
	        $sql .= " AND product_supplier.product_id IN (" . implode(',', $prod_ids) . ") ";
	    }
	    else 
	    {
	        return false;
	    }
	    		
		return db_query_array($sql);    
	}
	
	public static function get1($product_id, $brand_id)
	{
		$product_id = (int) $product_id;
		if (!$product_id) return false;
		$result = self::get($product_id, $brand_id);
		return $result[0];
	}
	static function seeIfFeedTable($brand_id)
	{
		$sql = "SELECT * FROM supplier_to_feed_table_mapping WHERE brand_id = $brand_id";
		$result = db_query_array($sql);
		return $result;
	}
}

?>