<?php


include_once(dirname(__FILE__).'/pdf.php');

class PDFCustoms extends PDF {

	private $data;

	public function __construct()
	{
	}

	public function setFieldValue($field,$value)
	{
		$this->data[$field] = ($value);
	}

	protected function compile(&$pdflib)
	{
		global $CFG;
		
		$pdflib->set_parameter("FontOutline", "Times New Roman={$CFG->font_dir}times.ttf");
		$pdflib->set_parameter("FontOutline", "Times New Roman-Bold={$CFG->font_dir}timesbd.ttf");

		$letterhead = $pdflib->open_pdi($CFG->pdf_templates_dir."/customs.pdf", "", 0);
		$letterhead_page = $pdflib->open_pdi_page($letterhead, 1, "");
		$pdflib->begin_page_ext(20, 20, "");
		$pdflib->fit_pdi_page($letterhead_page, 0, 0, "adjustpage");
		
		foreach ($this->data as $key => $val) {
			if (!$pdflib->fill_textblock(1, $key, $val, "embedding encoding=winansi")) {
	                    		throw new PDFFormException($pdflib->get_errmsg());
	                	}
		}
		
		$pdflib->close_pdi_page($letterhead_page);
		$pdflib->close_pdi($letterhead);
		$pdflib->end_page_ext("");
	}

}

?>