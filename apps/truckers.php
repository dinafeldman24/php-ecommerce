<?php

	class Truckers {

		static function insert($info)
		{
			return db_insert('truckers',$info);
		}

		static function update($id,$info)
		{
			return db_update('truckers',$id,$info);
		}

		static function delete($id)
		{
			return db_delete('truckers',$id);
		}

		static function get( $id=0, $order="", $order_asc="", $active="", $name="", $type="", $code="", $template="" )
		{
			$sql = " SELECT truckers.*, email_templates.name as template_name FROM truckers ";
			$sql .= " LEFT JOIN email_templates on email_templates.id = truckers.tracking_template_id ";
			$sql .=" WHERE 1 ";

			if( $id )
			{
				$sql .= " AND truckers.id = $id ";
			}

			if( $active )
				$sql .= " and truckers.active = '$active' ";

			if( $name )
				$sql .= " AND truckers.name = '$name' ";

			if( $type )
				$sql .= " AND truckers.type = '$type' ";

			if( $code )
				$sql .= " AND truckers.trucker_code = '$code' ";

			if( $template )
				$sql .= " AND truckers.tracking_template_id = '$template' ";

			$sql .= " ORDER BY ";
			if ($order == '') {
				$sql .= 'truckers.name';
			}
			else {
				$sql .= addslashes($order);
			}

			if ($order_asc !== '' && !$order_asc) {
				$sql .= ' DESC ';
			}

			//echo $sql;

			return db_query_array( $sql );
		}

		static function get1($id)
		{
			$id = (int) $id;
			if (!$id) return false;
			$result = Truckers::get($id);
			return $result[0];
		}

		static function getTruckerSelect( $name, $id="", $cur_trucker=0, $message="Choose a Trucker", $include_inactive=false, $type='trucker' )
		{
			$ret ="";
			if( $include_inactive )
				$active = '';
			else
				$active = "Y";
			$ret .= '<select name="' . $name . '" id="' . $id . '" size="1" onchange="'. $on_change . '" >';
			$truckers = Truckers::get( 0, "", "", $active, '', $type );
			$ret .= '<option value="0">' . $message .'</option>';
		if( $truckers )
			foreach( $truckers as $trucker )
			{
				$ret .= '<option value="' . $trucker['id'] . '"';
				if( $cur_trucker == $trucker['id'] )
					$ret .= ' selected ';
				$ret .= '>' . $trucker['name'];

				$ret .= '</option>';

			}
			$ret .= "</select>";

			return $ret;
		}

		static function importTrucker($zipcode='',$trucker_id=''){
			$zipcode = (int) $zipcode;
			$trucker_id = (int) $trucker_id;
			if(!$zipcode && is_numeric($trucker_id))
				return false;
			else
				return db_replace("trucker_lookup",array("zipcode"=>$zipcode,"trucker_id"=>$trucker_id));
		}

		static function countTruckerLookup(){
			//return count
			$sql = "SELECT count(DISTINCT zipcode) AS count FROM trucker_lookup";
			$arr = db_query_array($sql);
			return $arr[0]['count'];
		}

		static function getTruckerByZipcode($zipcode=''){
			if(!$zipcode)
				return false;

			$sql = "SELECT trucker_id AS id FROM trucker_lookup WHERE zipcode = '".addslashes($zipcode)."' LIMIT 1";
			$arr = db_query_array($sql);

			return $arr[0]['id'];

		}

		static function getTruckerNameByCode($trucker_code=''){
			if(!$trucker_code)
				return false;

			$sql = "SELECT name FROM truckers WHERE trucker_code = '".$trucker_code."' LIMIT 1";
			$arr = db_query_array($sql);

			return $arr[0]['name'];
		}
	}
?>