<?php class Site_Reviews
{
    public static function get($id = 0, $customer_id = 0, $title = '', $order_by = '', $order_asc = '', $approved='')
    {
        $sql = "SELECT site_reviews.*
                FROM site_reviews
                WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND site_reviews.id = $id ";
        }

        if ($customer_id > 0)
        { 
            $sql .= " AND sote_reviews.customer_id = $customer_id ";
        }

        if ($title != '')  
        {
            $sql .= " AND site_reviews.title = $title ";
        }
				if ($approved != '')
        {
            $sql .= " AND site_reviews.approved = $approved ";
        }
        
        
        if ($order_by == '')
        {
			// Default Sort Here
            $sql .= " ORDER BY site_reviews.id DESC ";
        } 
		else 
		{
			$order_by = db_esc($order_by);
			
			$sql .= " ORDER BY $order_by ";

			if($order_asc == "DESC" || $order_asc == "" || $order_asc == false){
				$order_asc = false;
			} else {
				$order_asc = true;
			}

			if ($order_asc)
			{
				$sql .= " ASC ";
			}
			else
			{
				$sql .= ' DESC ';
			}
		}
        
				return db_query_array($sql);
    }
		
	

    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info, $product_id, $customer_id)
    {
        $info['customer_id']    = $customer_id;
			  $safetitle=mysql_real_escape_string($info['title'] );
				$safecomment=mysql_real_escape_string($info['description'] );
				$info['title']=$safetitle;
				$info['comment']=$safecomment;	
						
				if (!$info['approved']){
				$info['approved']='P';
        }
        return db_insert('site_reviews', $info);
    }

    public static function delete($id)
    {
        /*delete terms relationship to review*/
				return db_delete('site_reviews', $id);
    }

    public static function update($id, $info)
		
    {    if ($info['title']){
    		$safetitle=mysql_real_escape_string($info['title'] );
				$info['title']=$safetitle;
				}
				if ($info['description']){
				$safecomment=mysql_real_escape_string($info['description'] );
				$info['description']=$safecomment;
				}				
        return db_update('site_reviews', $id, $info );
    }

}
?>