<?php
/*****************
*   Class for caching
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   March 15 2015
*
*****************/

class ProductOptionCache{

    public static $color_map = array();
    public static $font_map = array();
    public static $labels = array();
    public static $one_per_cart_products = array();
    public static $related_products = array();
    public static $product_cache = array();
    public static $product_sales = array();
    public static $product_all_options_on_sale = array();
    public static $product_options = array();
    public static $product_options_by_set = array();
    public static $option_names = array();
    public static $options_set_for_product_option = array();
    public static $prices_vary = array();
    public static $essensa_prices_vary = array();
    public static $min_price = array();
    public static $max_price = array();
    public static $min_essensa_price = array();
    public static $max_essensa_price = array();
    public static $options_set = array();
    public static $optional_options = array();
    public static $preset_options = array();
    public static $presets_for_po = array();
    public static $required_preset_ids = array();
    public static $required_option_sql =
        "select  po.product_option_values_set as pov_set, 
        po.id as po_id, @reg_price := if(po.additional_price > 0.00, po.additional_price, p.price) as price,
      if(po.vendor_price > 0.00, po.vendor_price, p.vendor_price) as base_cost,
         @essensa_price := if(po.essensa_price < po.additional_price AND po.essensa_price > 0.00 and (if(p.essensa_price > 0 and po.essensa_price < p.essensa_price, 0, 1)), po.essensa_price, if(p.essensa_price > 0.00 and p.essensa_price < po.additional_price and p.essensa_price < p.price, p.essensa_price, if( po.additional_price > 0.00, po.additional_price, p.price))) as essensa_price, po.vendor_sku, 
         sum(pa.additional_cost) as add_on_cost,
         sum(pa.additional_price) as add_on_price, sum(if(not(isnull(pa.essensa_additional_price)) and pa.essensa_additional_price > 0, pa.essensa_additional_price, pa.additional_price)) as essensa_add_on_price,     
        @case_price_per_piece := if(p.case_price_percent > 0, ROUND(@reg_price *(1 - p.case_price_percent), 2),@reg_price) as case_price_per_piece,
        @case_price_per_piece * p.num_pcs_per_case as case_price,
        @case_essensa_price_per_piece := if(p.case_price_percent > 0, ROUND(@essensa_price * (1 - p.case_price_percent), 2), @essensa_price) as case_essensa_price_per_piece,
        @case_essensa_price_per_piece * p.num_pcs_per_case as case_essensa_price,
         ltrim(group_concat(concat( ' ',trim(pl.label), ': ', option_value(o.id, ov.value)))) as combined, 
         ltrim(group_concat( if(o.input_type <> 'radio', concat(' ', trim(pl.label), ': ', option_value(o.id, ov.value)), ''))) as combined_no_radio,
         group_concat(o.name) as option_name,label, 
         group_concat(option_value(o.id, ov.value)) as option_value, preset_value_ids, 
         o.id as option_id, pa.id as pa_id, pa.product_available_option_labels_id as label_id, pl.show_in_cart
         FROM  
             product_options po JOIN  products p ON po.product_id = p.id
         LEFT JOIN product_available_options pa 
            ON locate(concat(',', pa.id,',') , product_option_values_set  ) > 0
         LEFT JOIN  option_values ov ON pa.options_values_id = ov.id
         LEFT JOIN  product_available_option_labels pl  
            ON pa.product_available_option_labels_id = pl.id
        LEFT JOIN options o ON ov.options_id = o.id ";


    function ProductOptionCache(){}

    static function setFontFamilies($ids = null){
        if(sizeof(self::$font_map) > 0){
            return;
        }
        if(!is_null($ids)){
           $info = ProductOptionValues::getFontsAndColors($ids);
        }
        else{
            $info = OptionFontMap::getAllFonts();
        }
        foreach($info as $curr_font){
            self::$font_map[$curr_font['ov_id']] = $curr_font['value'];
        }
    }

    static function getFonts($ids = null){
        self::setFontFamilies($ids);
        return self::$font_map;
    }

    static function setColorCodesAndImages(){
        if(sizeof(self::$color_map) > 0){
            return;
        }
        self::$color_map = array();
        $all_colors = OptionColorMap::get();
        foreach($all_colors as $curr){
            $option_type = ($curr['option_type'] == 'color') ?
                'bgcolor' : 'imagesrc';
            $value = $curr['color_value'];
            if($option_type == 'imagesrc'){
                $value = '/optionpics/' . $value;
            }
            self::$color_map[$curr['id']] = 'data-' . $option_type . '="' . $value . '"';
        }
    }

    static function getColors(){
        self::setColorCodesAndImages();
        return self::$color_map;
    }

    static function getColorCodeForDropDown($color_id){
        if(!sizeof(self::$color_map)){
            self::setColorCodesAndImages();
        }
        return(isset( self::$color_map[$color_id]))? 
            self::$color_map[$color_id] : '';
    }

    static function getMessageBelowLabel($product_id, $label_id){
        if(isset(self::$labels[$product_id][$label_id]) &&
            !empty(self::$labels[$product_id][$label_id]['message_below'])){
           return self::$labels[$product_id][$label_id]['message_below'];
    }
        return '';
    }

    static function getMessageAboveLabel($product_id, $label_id){
        if(isset(self::$labels[$product_id][$label_id]) &&
            !empty(self::$labels[$product_id][$label_id]['message_above'])){
           return self::$labels[$product_id][$label_id]['message_above'];
    }
        return '';
    }

    static function getLabelName($product_id, $label_id){
            return self::$labels[$product_id][$label_id]['label'];
    }

    static function setLabels($product_id){
        if(isset(self::$labels[$product_id])){
            return;
        }
        self::$labels[$product_id] = array();
        $labels = ProductAvailableOptionLabels::getForProduct($product_id);
        foreach($labels as $label){
            self::$labels[$product_id][$label['id']] = $label;
        }
    }

    static function setRelatedProduct($product_id, $option){
        $related_prod = $option['related_product'];
        if((int)$related_prod == 0){
            return;
        }
        if(!array_key_exists($related_prod, self::$one_per_cart_products)){
            self::$one_per_cart_products[$related_prod] = array();
        }
        self::$related_products[$option['pa_id']] =
            array('product_id' => $related_prod, 
                    'option_id' => $option['related_product_option'],
                    );
        self::$one_per_cart_products[$related_prod][] = $option['pa_id'];
    }

    static function getPresetIdsForAvailableOptionsSet($options_set, $presets){
        $ret = array();
        $options = explode(',', $options_set);
        foreach($options as $curr){
            if(array_key_exists($curr, $presets)){
                $ret[] = $presets[$curr];
            }
        }
        return $ret;
    }
    static function getPresetIdsFromJson($preset_string){
        $ret = array();
        if(empty($preset_string)){
            return $ret;
        }
        try {
            $preset_array = json_decode($preset_string);
            foreach($preset_array as $pa_id => $preset_id){
                $ret[] = $preset_id;
            }
        }
        catch(Exception $e){
            //if the string wasn't well formed json, or it wasn't a string...
        }
        return $ret;
    }

    static function setCacheForPresets($preset_value_ids){
        $sql = "SELECT pov.id as preset_val_id, @val := option_value(p.option_id, ov.value) as option_value,
            @option_name := o.name as option_name, @preset_name := p.name, CONCAT(@option_name, ': ', @val) AS combined 
            FROM preset_options p JOIN preset_option_values pov ON p.id = pov.preset_id
            JOIN options o ON p.option_id = o.id JOIN option_values ov ON pov.options_values_id = ov.id
            WHERE pov.id IN (" . implode(',', $preset_value_ids) . ") ";
        $preset_info = db_query_array($sql);
        foreach($preset_info as $curr){
            self::$preset_options[$curr['preset_val_id']] = array(
                        'option_text' => self::orderText($curr['combined']),
                        'option_names' => $curr['option_name'],
                        'option_values' => $curr['option_value'],
                        );
        }
    }

    static function getOptionInfoByOptionId($option_id, $product_id){
        self::setProductOptionCache($product_id);
        return self::$product_options[$option_id];
    }

    static function getSaleOptions($product_id){
        return self::$product_sales[$product_id];
    }

    static function someOptionsOnSale($product_id){
        return (sizeof(self::$product_sales[$product_id]) > 0 && !self::$product_all_options_on_sale[$product_id]);
    }

    static function setProductOptionCache($product_id, $force = false){
        if(empty($product_id) || !$product_id){
            return;
        }
        if(isset(self::$product_cache[$product_id]) && !$force){
            return;
        }
        self::setLabels($product_id);
        self::$product_sales[$product_id] = array();
        self::$product_all_options_on_sale[$product_id] = true;
        $sales = Sales::get(0,true,$product_id);
        $product_sales = array();
        foreach($sales as $curr){
            $product_sales[$curr['product_option_id']] = array('essensa' => $curr['essensa_only'], 'sale_price' => $curr['sale_price']);
        }
        $all_set_options = array();
        $option_prices = array();
        $option_essensa_prices = array();
        $preset_ids = array();
        $sql = self::$required_option_sql . " WHERE 
            po.product_id = $product_id 
            AND po.is_active = 'Y'
        group by po.id order by pa.display_order";
        $options = db_query_array($sql);
        if($options){
           foreach($options as $curr){
                self::setRelatedProduct($product_id, $curr);
               $preset_ids = array_merge($preset_ids, self::getPresetIdsFromJson($curr['preset_value_ids']));
               $essensa_price = $curr['essensa_price']; 
               $price = $curr['price']; 
               $is_sale = false;
               if(isset($product_sales[$curr['po_id']])){
                   $is_sale = true;
                    if($product_sales[$curr['po_id']]['essensa_only'] == 'Y'){ 
                        $essensa_price = $product_sales[$curr['po_id']]['sale_price'];
                   }
                    else{
                        $price = $product_sales[$curr['po_id']]['sale_price'];
                        if($price < $essensa_price){
                            $essensa_price = $price;
                        }
                    }
               }
               else{
                    self::$product_all_options_on_sale[$product_id] = false;
               }
                $option_prices[] = $price;
                $option_essensa_prices[] = $essensa_price;
				if ($curr['show_in_cart']==1 || (strpos($_SERVER['REQUEST_URI'] ,'edit') !== false))
					   $text_for_cart=$curr['combined'];
				else
					   $text_for_cart=$curr['combined_no_radio'];
                $this_option = array(
                    'price' => $price,
                    'essensa_price' => $essensa_price, 
                    'option_text' => self::orderText($text_for_cart),
                    'option_names' => $curr['label'],
                    'option_values' => $curr['option_value'],
                    'options_set' => $curr['pov_set'],
                    'label_id' => $curr['label_id'],
                    'option_id' => $curr['po_id'],
                    'option_sku' => $curr['vendor_sku'],
                    'case_price' => $curr['case_price'],
                    'case_essensa_price' => $curr['case_essensa_price'],
                    'case_price_per_piece' => $curr['case_price_per_piece'],
                    'case_essensa_price_per_piece' => $curr['case_price_per_piece'],
                    'is_sale' => $is_sale,
                    'additional_price' => $curr['add_on_price'],
                    'additional_cost' => $curr['add_on_cost'],
                    'additional_essense_price' => $curr['essensa_add_on_price'],
                    'total_price' => $price + $curr['add_on_price'],
                    'total_essensa_price' => $essensa_price + $curr['essensa_add_on_price'],
                      'cost' => $curr['base_cost'],  
                );	

                self::$product_options[$curr['po_id']] = $this_option;
                self::$product_options_by_set[$curr['pov_set'] . str_replace('"', '', $curr['preset_value_ids'])] = $this_option;
                self::$option_names[$curr['option_id']] = $curr['option_name'];
                self::$options_set_for_product_option[$curr['po_id']] = $curr['pov_set'];
                self::$presets_for_po[$curr['po_id']] = $curr['preset_value_ids'];
                self::$required_preset_ids = array_merge(self::$required_preset_ids, self::getPresetIdsFromJson($curr['preset_value_ids']));
                if($is_sale){
                    self::$product_sales[$product_id][] = array(
                    'description' => $curr['combined'],
                    'id' => $curr['po_id'],
                    'price' => $price,
                    'essensa_price' => $essensa_price,
                    );
                
                }
                $these_options_set = explode(',', $curr['pov_set']);
                foreach($these_options_set as $curr){
                    $all_set_options[] = $curr;
                }
           }
            $option_prices = array_unique($option_prices);
            $option_essensa_prices = array_unique($option_essensa_prices);
            if(sizeof($option_prices) > 1){
                self::$prices_vary[$product_id] = true;
                self::$min_price[$product_id] = min($option_prices);
                self::$max_price[$product_id] = max($option_prices);
            }
            else{
                self::$prices_vary[$product_id] = false;
            }
            if(sizeof($option_essensa_prices) > 1){
                self::$essensa_prices_vary[$product_id] = true;
                self::$min_essensa_price[$product_id] = min($option_essensa_prices);
                self::$max_essensa_price[$product_id] = max($option_essensa_prices);
            }
            else{
                self::$essensa_prices_vary[$product_id] = false;
            }
            if(sizeof($preset_ids)){
                self::setCacheForPresets($preset_ids);
            }
        }
        //optional options
        $sql = "SELECT pa.id, pa.id as pa_id, ifnull(pres_vals.additional_price, pa.additional_price) as price, 
            ifnull(pres_vals.essensa_additional_price, pa.essensa_additional_price) 
            as essensa_price, 
            ifnull(pres_vals.is_default, pa.is_default) as is_default,
            @this_option_value := option_value(ov.options_id, ov.value),
            @this_preset_value := option_value(pres.option_id, ov2.value),
            @this_value := ifnull(@this_option_value, @this_preset_value) as option_value, 
            if(@this_value <> '', concat( ' ',trim(pl.label), ': ', @this_value), pl.label) as combined, 
            @input_type := ifnull(o.input_type, o2.input_type) as input_type,
            if(@this_value <> '' and @input_type <> 'radio', concat( ' ',trim(pl.label), ': ', @this_value), '') as combined_no_radio, 
            ifnull(pres_vals.id, 0) as preset_value_id, 
            pl.label as option_name, related_product, related_product_option, pl.show_in_cart
            FROM  product_available_options pa
            LEFT JOIN option_values ov ON pa.options_values_id = ov.id 
            LEFT JOIN  options o ON ov.options_id = o.id 
           LEFT JOIN product_available_option_labels pl ON pa.product_available_option_labels_id = pl.id
            LEFT JOIN  preset_options pres ON pa.preset_id = pres.id 
            LEFT JOIN preset_option_values pres_vals ON pres.id = pres_vals.preset_id
            LEFT JOIN option_values ov2 ON pres_vals.options_values_id = ov2.id 
            LEFT JOIN options o2 ON ov2.options_id = o2.id
         WHERE 
            pa.product_id = $product_id
            and pl.optional = 1 ORDER BY pa.display_order, pres_vals.display_order";
        $options = db_query_array($sql);
        if($options){
        foreach($options as $curr){
				if ($curr['show_in_cart']==1 || (strpos($_SERVER['REQUEST_URI'] ,'edit') !== false))
					   $text_for_cart=$curr['combined'];
				else
					   $text_for_cart=$curr['combined_no_radio'];
               $this_option = array(
                    'price' => $curr['price'],
                    'essensa_price' => $curr['essensa_price'],
                    'option_text' => self::orderText($text_for_cart),
                    'option_names' => $curr['option_name'],
                    'option_values' => $curr['option_value'],
                    'option_id' => '',
                    'options_set' => '',
                    'option_sku' => '',
                    );	
				self::$optional_options[$curr['id']] = $this_option;
                if($curr['preset_value_id']){
                    self::$preset_options[$curr['preset_value_id']] = array(    
                        'essensa_price' => $curr['essensa_price'],
                        'price' => $curr['price'],
                        'option_text' => self::orderText($text_for_cart),
                        'option_names' => $curr['option_name'],
                        'option_values' => $curr['option_value'],
                        );
                }

                $all_set_options[] = $curr['pa_id'];
                self::setRelatedProduct($product_id, $curr);
            }
        }
        self::$options_set[$product_id] = array_unique($all_set_options);
        self::$product_cache[$product_id] = true;
    }

    function orderText($text){
        $vals = explode(',', $text);
        sort($vals);
        $text = implode(', ', $vals);
        return $text;
    }

    static function getProductOptions($po_id, $pid, $field = null){
        self::setProductOptionCache($pid);
        if($field){
            return self::$product_options[$po_id][$field];
        }
        return self::$product_options[$po_id];
    }

    static function getDescriptionForSingleOption($option_id, $preset_id = 0, $no_radio = false){
        $field = 'option_text';		
        if($preset_id){
            return  self::$preset_options[$preset_id][$field] . ' ';
        }
        if(isset(self::$product_options[$option_id])){
           return  self::$product_options[$option_id][$field];
        }
        elseif(isset(self::$optional_options[$option_id])){
          return self::$optional_options[$option_id][$field];
        }
        elseif(isset(self::$optional_options[$option_id])){
          return  self::$optional_options[$option_id][$field];
        }
    }
    static function getDescriptionForUserInput($option_id, $input_info, $is_order){
        global $CFG;
        if(strpos($input_info[0], 'text') !== false){
            return self::$optional_options[$option_id]['option_names'] . ': ' . $input_info[1];
        }
        elseif($input_info[0] == 'file'){
            $path = $CFG->baseurl . ($is_order ? $CFG->edit_display_path :$CFG->site_display_path );  
            return '<div class="user_file_input_display">' .self::$optional_options[$option_id]['option_names'] . ': <img src="' . $path . '/' . $input_info[1] . '"/></div>';
        }
    }

    static function getDescriptionForOptions($options, $product_id = '', $no_radio = false){
        self::setProductOptionCache($product_id);
        $description = '';
        foreach($options as $option){   
            $presets = json_decode($option['preset_value_ids'], true);
            $description .= self::getDescriptionForSingleOption($option['product_option_id'], 0, $no_radio) . ', ';
            $description = rtrim($description, ', ');
            $preset_values_for_option = self::getPresetIdsForAvailableOptionsSet(self::getOptionsSetForProductOption($option['product_option_id']), $presets);
            foreach($preset_values_for_option as $curr){
                $description .= self::getDescriptionForSingleOption(-1, $curr, $no_radio) . ', ';
            }
            if(isset($option['available_options_id']) && !empty($option['available_options_id'])){
                $optionals = explode(',', $option['available_options_id']);
            }
            elseif(isset($option['optional_options']) && !empty($option['optional_options'])){
                $optionals=  explode(',', $option['optional_options']);
            }
            if(isset($option['order_item_id'])){
                $user_input = json_decode($option['value'], true);
                $is_order = true;
            }
            else{
                $user_input = json_decode($option['user_input']);
                $is_order = false;
            }
            foreach($optionals as $option_id){
                $preset_id = !is_null($presets) && array_key_exists($option_id, $presets) ? $presets[$option_id] : 0;
                if(!array_key_exists($option_id, $user_input)){
                $this_description = self::getDescriptionForSingleOption((int)$option_id, $preset_id, $no_radio);
                if(!empty($this_description)){
                    $description .= $this_description . ', ';
                }
                }
            }
            $description = rtrim($description, ', ');
            foreach($user_input as $option_id => $input){
                $description .= self::getDescriptionForUserInput($option_id, $input, $is_order);
            }
        }
        return trim($description, ', ');
    }

    static function getSKUForOptions($options, $pid){
        self::setProductOptionCache($pid);
//        echo 'the options: ' . var_export($options, 1) . '<br/>and the product option cache: ' . var_export(ProductOptionCache::$product_options, 1);
        foreach($options as $option){
     //       echo 'looking for: ' . $option['product_option_id'] ;
            if(isset(  ProductOptionCache::$product_options[$option['product_option_id']])){
                return ProductOptionCache::$product_options[$option['product_option_id']]['option_sku'];
            }
        }
        return '';
    }

    static function getOptionsSetForProductOption($option_id, $product_id = null){
        self::setProductOptionCache($product_id);
        return self::$options_set_for_product_option[$option_id];
    }

    static function getRelatedProductsForOptions($option_ids, $product_id = null){
        self::setProductOptionCache($product_id);
        $related_prods = array();
        foreach($option_ids as $id){
            if(isset(self::$related_products[$id])){
                $related_prods[$id] = self::$related_products[$id];
            }
        }
        return array_unique($related_prods);
    }
    
    static function pricesVary($product_id, $is_essensa = false){
        self::setProductOptionCache($product_id);
        if($is_essensa){
            return self::$essensa_prices_vary[$product_id];
        }
        return self::$prices_vary[$product_id];
    }

    static function getMinPrice($product_id, $is_essensa = false){
        self::setProductOptionCache($product_id);
        if($is_essensa){
            return self::$min_essensa_price[$product_id];
        }
        return self::$min_price[$product_id];
    }

    static function getMaxPrice($product_id, $is_essensa = false){
        self::setProductOptionCache($product_id);
        if($is_essensa){
            return self::$max_essensa_price[$product_id];
        }
        return self::$max_price[$product_id];
    }

    static function getPresetText($presets, $product_id = null){
        self::setProductOptionCache($product_id);
        $text = '';
        try{
            $these_presets = json_decode($presets);
        }
        catch(Exception $e){
            $these_presets = $presets;
        }
        foreach($these_presets as $pa_id => $preset_val_id){
           if(!isset(self::$preset_options[$preset_val_id])){
              self::setCacheForPresets(array($preset_val_id));   
           }
            $text .= self::$preset_options[$preset_val_id]['option_text'] . ', ';
        }
        return rtrim($text, ', ');
    }

    static function getTextFromSet($options_set, $product_id = null, $presets = null){
        self::setProductOptionCache($product_id);
        $text = self::$product_options_by_set[$options_set . str_replace('"', '', $presets)]['option_text'];  
        if($presets){
            $text .= ', ' . self::getPresetText($presets, $product_id);
        }
        return trim($text, ', ');
    }

    static function getOptionBySet($options_set, $product_id = null, $presets = ''){
        self::setProductOptionCache($product_id);
        if($presets == '{}' && isset(self::$product_options_by_set[$options_set])){
            return self::$product_options_by_set[$options_set];
        }
        $presets = str_replace('"', '', $presets);
        return self::$product_options_by_set[$options_set . $presets];
    }
    
    static function getCostForPO($product_id, $product_option_id){
        self::setProductOptionCache($product_id);
        if(isset(self::$product_options[$product_option_id])){
            return self::$product_options[$product_option_id]['additional_cost'];
        }
        return 0;
    }

}
?>
