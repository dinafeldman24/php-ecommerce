<?php

class Brands {

	static function insert($info)
	{
		$can_update = true;
		// Generate the url_name, unless it was provided on the cat insert form
		if (!(isset($info['url_name']) && $info['url_name'] != "")) {
			$info['url_name'] = trim(strtolower($info['name']));
		}
		
		// Handle special characters and whitespace in the url name
		$info['url_name'] = str_replace( "&", "and", $info['url_name'] );
		$info['url_name'] = str_replace( "/", "-", $info['url_name'] );
		
		$info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
		$info['url_name'] = str_replace(' ','-',$info['url_name']);
		
		//Now need to check for that URL in products and Cats and brands to make sure it isn't there already
	    $p_url = Products::get1ByURLName( $info['url_name'] );
		if ($p_url && $p_url['id'] != $id) 
		{
			echo "<div class='errorBox'>Product " . $p_url['vendor_sku'] . " already exists with URL ".$info['url_name']."  Can't perform insert/update!</div>";
			$can_update = false;
		}

	    $c_url = Cats::get1ByURLName( $info['url_name'] );
	    if ($c_url && $c_url['id'] != $id) 
	    {
	    	echo "<div class='errorBox'>Category " . $c_url['name'] . " already exists with URL ".$info['url_name']." Can't perform insert/update.</div>";
	    	$can_update = false;	    
	    }
		$b_url = Brands::get1ByURLName( $info['url_name'] );
	    if ($b_url && $b_url['id'] != $id) 
	    {
	    	echo "<div class='errorBox'>Brand " . $b_url['name'] . " already exists with URL ".$info['url_name']."  Can't perform insert/update.</div>";
	    	$can_update = false;	    
	    }		
		
		if ($can_update) return db_insert('brands',$info);
	}

	static function update($id,$info)
	{
		$can_update = true;
		// see if url changed.  If it did, put it in the url_redirects table
		if ($info['url_name'])
		{
			//Now need to check for that URL in products and Cats and brands to make sure it isn't there already
	    	$p_url = Products::get1ByURLName( $info['url_name'] );
			if ($p_url && $p_url['id'] != $id) 
			{
				echo "<div class='errorBox'>Product " . $p_url['vendor_sku'] . " already exists with URL ".$info['url_name']."  Can't perform insert/update!</div>";
				$can_update = false;
			}

	    	$c_url = Cats::get1ByURLName( $info['url_name'] );
	    	if ($c_url && $c_url['id'] != $id) 
	    	{
	    		echo "<div class='errorBox'>Category " . $c_url['name'] . " already exists with URL ".$info['url_name']." Can't perform insert/update.</div>";
	    		$can_update = false;	    
	    	}
			$b_url = Brands::get1ByURLName( $info['url_name'] );
	    	if ($b_url && $b_url['id'] != $id) 
	    	{
	    		echo "<div class='errorBox'>Brand " . $b_url['name'] . " already exists with URL ".$info['url_name']."  Can't perform insert/update.</div>";
	    		$can_update = false;	    
	    	}
		
			if ($can_update)
			{
				$old_brand_info = Brands::get1($id);
				if ($old_brand_info['url_name'] != $info['url_name']) 
				{
					$redirect_info['from_url'] = "/".$old_brand_info['url_name'].".html";
					$redirect_info['to_url'] = "/".$info['url_name'].".html"; 
					UrlRedirects::insert($redirect_info);
				}
			}
		}
		if ($can_update) return db_update('brands',$id,$info);
		else return false;
	}

	static function delete($id)
	{
		return db_delete('brands',$id);
	}

static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='', $name='', $is_active='', $type='brand',	$display_in_brands_drop_down='', $name_with_no_spaces = '', $url_name = '', $nickname = '', $is_recommended='',$use_instr=false)
	{
		$sql = "SELECT brands.*
				FROM brands
				WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND brands.id = $id ";
		}
		if ($begins_with != '') {
			$sql .= " AND brands.name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') {
			$fields = Array('brands.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if( $name != '' ){
			if($use_instr){
				$sql .= " AND INSTR( '".addslashes($name)."' , brands.name ) ";
				//echo $sql;
			}else{
				$sql .= " AND brands.name = '".addslashes($name)."' ";
			}
		}		
		if( $url_name !='' )
		{
			$sql .= " AND brands.url_name = '".addslashes($url_name)."'";
		}
		if( $nickname != '' )
			$sql .= " AND brands.nickname = '".addslashes($nickname)."' ";
		
		if( $name_with_no_spaces != '' )
			$sql .= " AND LOWER(REPLACE(REPLACE( brands.name,  ' ',  '' ), '-', '')) = '".addslashes($name_with_no_spaces)."' ";			

		if( $is_active != '' )
			$sql .= " AND brands.is_active = '$is_active' ";


		if ($type != '')
		{
		    $sql .= " AND brands.type = '$type' ";
		}

		if ($display_in_brands_drop_down != '')
		{
			$sql .= " AND brands.display_in_brands_drop_down = '$display_in_brands_drop_down'";
		}
		if( $is_recommended != '' )																				
			$sql .= " AND brands.recommended_vendor = '$is_recommended' ";	
			
		$sql .= " GROUP BY brands.id
				  ORDER BY ";

		if ($order == '') {
			$sql .= 'brands.name';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

//		echo $sql;

		return db_query_array($sql);
	}

	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Brands::get($id,'','','','','','','');
		return $result[0];
	}

	static function get1ByName( $name )
	{
		$result = Brands::get(0,'','','','', $name,'','');
		if( count( $result ) )
			return $result[0];
		else
			return false;
	}

	static function _brands2pulldown(&$brands, $selected_brand, $level=0)
	{

		// if it goes any further, i guess it's gonna have to be black.
		$levels = array('#000000', '#7B0202', '#B70404', '#BF0202', '#EB0606');

		$ret = '';

		if (is_array($brands)) {
			foreach ($brands as $brand) {

	     $style = "style='color:$levels[$level]'";

				$ret .= "<option value='$brand[id]' $style name='$name' ";

				if ($selected_brand == $brand['id'])
				  $ret .= ' selected ';

				$ret .= ">";
				$ret .= str_repeat('&nbsp;&nbsp;',$level);
				$ret .= $brand['name'];

				$ret .= Brands::_brands2pulldown($brand['children'],$selected_brand,$level+1, $name);

				}
		}

		return $ret;
	}

	static function brands2pulldown(&$brands, $selected_brand, $level=0, $name = 'filterbrand', $first_blank = true, $width = '', $onchange = '', $msg ='')
	{
		if($width) $style = " style='width:{$width}px'";
		if($onchange) $onchange = " onchange='$onchange'";
		$ret = "<select name='$name' $style $onchange>";

		if ($first_blank)
		  $ret .= "<option value=0>$msg";

		$ret .= Brands::_brands2pulldown($brands, $selected_brand, $level=0);
		$ret .= "</select>";
		return $ret;
	}

	static function getBrandByName( $brand_name )
	{
		$sql = " SELECT * FROM brands WHERE UPPER(name) = UPPER('$brand_name') ";
		$results = db_query_array( $sql );
		return $results[0];
	}

	static function getBrandsForCat( $cat_id )
	{
		if( !is_array( $cat_id ) )
			$cat_id = array( $cat_id );
		$sql = "SELECT distinct products.brand_id
				FROM products
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				WHERE products.is_active = 'Y' AND products.id <> 0";

		if (is_array($cat_id)) {
			$sql .= " AND product_cats.cat_id IN ('". implode("','", $cat_id) ."')";
		}

		$result = db_query_array($sql);

		$brand_ids = array();
		$sql = " SELECT * from brands WHERE ";
		if( $result )
			foreach( $result as $r )
			{
				$brand_ids[] = $r['brand_id'];
			}
			$sql .= " id IN ('". implode("','", $brand_ids) ."') AND brands.is_active ='Y' ORDER BY brands.name ASC";

		return db_query_array( $sql );
	}
	static function getBrandsWith15OrMoreActiveProds()
	{
		$sql = "SELECT distinct brands.*, products.brand_id, count(*) as num
				FROM products, brands
				WHERE products.brand_id = brands.id AND products.is_active = 'Y' 
				AND products.id <> 0
				AND products.is_deleted = 'N' 
				GROUP BY brand_id HAVING num >= 20
				ORDER BY brands.name ASC";

		return db_query_array( $sql );
	}
	
	static function getBrandBestSellers($brand_id, $num_to_return = 8)
 	{
 		$num_to_return = (int) $num_to_return;
 		if ($num_to_return <= 0) $num_to_return = 1;
 		$sql = "SELECT * FROM brand_best_sellers, products WHERE brand_best_sellers.brand_id = " . $brand_id . " AND products.id = brand_best_sellers.product_id 
 				AND products.is_active = 'Y' and products.is_deleted = 'N' LIMIT $num_to_return";
 		
 		$result = db_query_array($sql);
 		return $result;
 	}
	static function getSomeRandomBrandProducts($brand_id, $num_to_return = 8)
	{
		$sql = "SELECT id AS product_id FROM products WHERE brand_id = $brand_id AND 
				products.is_active = 'Y' AND products.is_deleted = 'N'
				ORDER BY RAND() LIMIT $num_to_return";
		return db_query_array($sql);
	}
	static function getCatsToDisplayOnBrandPage($brand_id, $num_to_return = 18)
	{
		$sql = "SELECT product_cats.cat_id, cats.name, COUNT(*) AS num
				FROM products, product_cats, cats
				WHERE products.brand_id = $brand_id AND product_cats.cat_id = cats.id 
				AND products.id = product_cats.product_id
				AND products.is_active = 'Y' 
				AND products.id <> 0
				AND products.is_deleted = 'N'
				AND cats.list_on_brand_page = 'Y'
				GROUP BY product_cats.cat_id 
				ORDER BY num DESC
				LIMIT $num_to_return";

		return db_query_array( $sql );
	}
	static function getRecentlyAddedProducts($brand_id, $num_to_return = 10)
	{
		$sql = "SELECT id FROM products WHERE brand_id = $brand_id AND 
				products.is_active = 'Y' AND products.is_deleted = 'N'
				AND date_added > DATE_SUB(NOW(), INTERVAL 1 YEAR)
				ORDER BY date_added DESC LIMIT $num_to_return";
		return db_query_array($sql);
	}
	static function getRecentlyAddedReviews($brand_id, $num_to_return = 10)
	{
		$sql = "SELECT product_reviews.id, product_ratings.rating,
				CAST(product_ratings.rating * 20 AS UNSIGNED) AS percentage
				FROM (product_reviews, products)
				LEFT JOIN product_ratings 
				ON (product_reviews.customer_id = product_ratings.customer_id
				AND product_reviews.product_id = product_ratings.product_id)
				WHERE brand_id = $brand_id AND 
				products.is_active = 'Y' AND products.is_deleted = 'N'
				AND product_reviews.product_id = products.id
				AND product_reviews.approved = 'Y'
				ORDER BY product_reviews.approved_date DESC LIMIT $num_to_return";
		return db_query_array($sql);
	}
	static function updateURLName( $id )
	{
		$brand = Brands::get1( $id );
		//echo'Old Url Name = ' . $cat['url_name'];
		$info['url_name'] = trim($brand['name']);

		$info['url_name'] = str_replace( "&", "and", $info['url_name'] );
		$info['url_name'] = str_replace( "/", "-", $info['url_name'] );

		$info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
		$info['url_name'] = str_replace(' ','-',$info['url_name']);
		//echo ', new url_name = ' . $info['url_name'] . '<br />';
		return db_update( 'brands', $id, $info );
	}
	function get1ByURLName( $url_name )
	{

		$result = Brands::get(0,'','','is_active','DESC','','', 'brand','','', $url_name);
		return $result[0];
	}
	static function getBestSellingBrands($num_to_return = 8)
 	{
 		$num_to_return = (int) $num_to_return;
 		if ($num_to_return <= 0) $num_to_return = 1;
 		$sql = "SELECT * FROM best_selling_brands, brands WHERE brands.id = best_selling_brands.brand_id
 		 ORDER BY num_orders DESC LIMIT $num_to_return";
 		
 		$result = db_query_array($sql);
 		return $result;
 	}
}

?>