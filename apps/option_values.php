<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   Feb 15 2015
*
*****************/

class ProductOptionValues {
    
    const COLOR_OPTION_ID=1;
    var $id = null;
    var $option_id;
    var $option_name;
    var $value;
	var $image_file;
    var $delete_error;

    function ProductOptionValues($id=null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    function populate($id){
        $this->id = $id;
        $value = self::get1($id);
        if(is_array($value)){
            $this->value = $value['value'];
			$this->image_file = $value['image_file'];
            $this->option_name = $value['option_name'];
            $this->option_id = $value['option_id'];
            $this->formula = $value['formula'];
            $this->font_family = $value['font_value'];
            $this->font_type = $value['font_option_type'];
        }
    }

    /****************
    DB Interactions
    ***************/

    function updateOrInsert(){
        $value = $this->value;
		$image_file = $this->image_file;
        if(!is_null($this->id)){
            $sql = "UPDATE option_values 
			 SET value = '$value',
              image_file = '$image_file' 
			  WHERE id = " . $this->id;
            $res = db_query($sql);
            if(!$res){
                return false;
            }
            return $res;
        }   
        else{
            $info = array('value' => $value,
            'options_id' => $this->option_id,
			'image_file' => $this->image_file);
            $res = db_insert('option_values', $info);
            if(!$res){
                return false;
            }
            $this->id = $res;
            return $res;
        }
    }

    static function get($id = 0, $ids = null, $option_id = null){
        $sql = "SELECT v.id as value_id, @option_name := option_value(v.options_id, v.value) as value,
             @option_name as name,
             v.id as id,
		     v.image_file as image_file,
             o.id as option_id, o.name as option_name, o.formula,
             of.value as font_value, of.option_type as font_option_type
             FROM option_values v JOIN options o ON 
             (o.id = v.options_id) 
             LEFT JOIN option_font_map of ON 
             (of.option_value_id = v.id)
             WHERE 1";
         if($id){
             $sql .= " AND v.id = $id";
         }
         if($ids){
             $sql .= " AND v.id IN ($ids)";
         }
         if($option_id){
            $sql .= ' AND options_id = ' . $option_id;
         }
         return db_query_array($sql);   
    }

    static function get1($id){
        $info = self::get($id);
        try{
            return $info[0];
        }
        catch(Exception $e){
            return null;
        }
    }

    function delete(){
        return db_delete('option_values', $this->id);
    }

    /***
    Getters and Setters
    ****/

    function setValue($value){
        $this->value = $value;
    }

    function getValue(){
        return $this->value;
    }
	
	 function setImage_file($image_file=null){
        $this->image_file = $image_file;
    }

    function getImage_file(){
        return $this->image_file;
    }

    function setOptionId($option_id){
        $this->option_id = $option_id;
    }

    function getOptionId(){
        return $this->option_id;
    }

    static function getForOption($option_id){
        return self::get(0, null, $option_id);
    }

    static function getValueForColor($color_id){
        $sql = "SELECT ov.id as value_id, name, color_value, option_type FROM option_values ov JOIN option_color_map ocm ON ov.value = ocm.id WHERE ocm.id = " . (int)$color_id . ' AND options_id = 1';
        $arr = db_query_array($sql);
        if(!sizeof($arr)){
            return 0;
        }
        return $arr[0];
    }

    static function getFontsAndColors($font_ids, $color_ids = ''){
        $sql = '';
        if(!empty($font_ids)){
            $sql .= "(SELECT f.value as value, ov.id as ov_id, option_type, 'font' as option_name
            FROM option_values ov 
            LEFT JOIN option_font_map f ON f.option_value_id = ov.id
            WHERE ov.id IN ($font_ids))";
        }
        if(!empty($font_ids) && !empty($color_ids)){
            $sql .= " UNION ";
        }
        if(!empty($color_ids)){
            $sql .= "   (SELECT color_value as value, ov.id as ov_id, option_type, 'color' as option_name
            FROM option_values ov
            LEFT JOIN option_color_map c ON c.id = ov.value
            WHERE ov.id IN($color_ids)) ";
        }
        $sql .= " ORDER BY option_name, value";
        return db_query_array($sql);
    }


    /************
    Output Functions
    *************/

}
?>
