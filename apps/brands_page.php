<?php

class Brands_Page
{
	
	public static function insert($info)
	{		
		//($table,$info,$date='',$ignore=false,$silent=false,$echo_sql=false,$return_bool=false,$delayed=false,$time_function="NOW()")
		return db_insert('brands_page', $info) ;//, '', 0, 0, 1);
	}
	
	public static function update($brands_id, $info)
	{
		return db_update('brands_page', $brands_id, $info, "brand_id");
	}
	
	public static function delete($brands_id)
	{
		return db_delete('brands_page', $brands_id, "brand_id");
	}
	
	public static function get($brand_id=0)
	{
		$sql = "SELECT brands_page.*, brands.name
				FROM brands_page, brands
				WHERE brands_page.brand_id=brands.id ";
		
		if ($brand_id > 0)
		{
		  $sql .= " AND brands_page.brand_id = $brand_id ";
		}
		
		$sql.="order by brands.name";
		
		//echo "sql:  $sql";
				
		return db_query_array($sql);
	}
	
	public static function get1($brand_id)
	{
		$brand_id = (int) $brand_id;
		if (!$brand_id) return false;
		$result = self::get($brand_id);
		return $result[0];
	}
	static function getImages( $brand_id = 0, $id=0, $is_active='' )
	{
		$sql = " SELECT * FROM brands_page_image ";
		$sql .= " WHERE 1 ";
		
		if($brand_id)
			$sql .= " AND brands_page_image.brand_id = " . (int) $brand_id;
		if( $id )
			$sql .= " AND brands_page_image.id = " . (int) $id;
		if( $is_active )
			$sql .= " AND brands_page_image.is_active = '" . addslashes( $is_active ) . "' ";
			
		$sql .= " ORDER BY brands_page_image.order_fld ";	
			
		return db_query_array( $sql );
	}
		
	static function get1Image( $id )
	{
		return db_get1( self::getImages('', $id ) );
	}
	
	static function updateImage( $image_id, $info )
	{
		return db_update( 'brands_page_image', $image_id, $info );
	}
	
	static function insertImage( $info )
	{
		return db_insert( 'brands_page_image', $info );
	}
	
	static function deleteImage( $id )
	{
		return db_delete( 'brands_page_image', $id );
	}
	
	public static function getSubcats($brand_id, $limit =10){
		
		//Get categories with the most active products
		$sql = "select pc.cat_id, sum(is_active) as total_active, c.name
			FROM products p, product_cats pc, cats c
			WHERE p.id=pc.product_id
			AND pc.cat_id=c.id
			and p.brand_id='$brand_id'
			and p.is_active='Y' and c.pid > 0
			group by pc.cat_id
			order by total_active DESC 
			LIMIT $limit";
		//echo "sql is $sql";
		return db_query_array($sql);
	}
}

?>
