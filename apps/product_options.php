<?php

class ProductOptions {
	
	
	function outputOptions($options)
	{
		if(!is_array($options) || count($options) == 0) return false;
		
		foreach($options as $option_id => $option_set){
			switch($option_id){
				case 1:
					self::outputSize($option_set);
					break;
			}
		}
		
		return true;
	}
	
	
	
	
	function outputSize($options)
	{
		if(!is_array($options) || count($options) == 0) return false;

		$selectMenu = '';
		$priceTables = array();
		$defaultOption = 0;
		
		$selectMenu = "<b>Please select a size:</b><br><select id='product_options' name='options[]' onchange='updatePriceChart()'>";
		
		foreach($options as $option){
			
			if(!is_array($option['prices']) || count($option['prices']) == 0) continue;
			
			$index = $option['id'];
			$defaultOption = $option['is_default'] == 'Y' ? $index : $defaultOption;
			
			$selectMenu .= "<option value='".$index."'".($option['is_default'] == 'Y' ? ' selected' : '').">".$option['value']."</option>\n";
			
			$priceTables[$index] = '
				<table id="prices_'.$index.'" class="prodQuantity" width="" border="0" cellpadding="0" cellspacing="0" style="display:'.($option['is_default'] == 'Y' ? '' : 'none').'">
				<tr>
					<th style="width:80px">Quantity</th>
					<th style="width:70px">Price Each</th>
					<th>Total</th>
				</tr>';
			
			foreach($option['prices'] as $qty => $price){
				$priceTables[$index] .= '
					<tr>
						<td style="text-align: left"><input type="radio" name="qty" value="'.$qty.'"> '.$qty.'</td>
						<td>$'.number_format($price,2,'.',',').'</td>
						<td>$'.number_format($qty * $price,2,'.',',').'</td>
					</tr>';
			}
			
			$priceTables[$index] .= '</table>';
			
		}
		
		$selectMenu .= "</select><br><br><br>";
		
		if(count($priceTables) == 0) return false;
		
		
		echo($selectMenu);
		
		foreach($priceTables as $table){
			echo($table);
		}

		echo('<script language="JavaScript"> updatePriceChart(); </script>');
		
		return true;
	}
	
	function getByVendorSku($vendor_sku, $not_deleted = false, $only_active = false){
		if(!trim($vendor_sku)){
			return false;
		}

		$vendor_sku = addslashes($vendor_sku);

		$sql = "SELECT product_options.*, products.*, product_options.id as product_option_id FROM product_options 
				LEFT JOIN products ON product_options.product_id = products.id
				WHERE product_options.vendor_sku = '$vendor_sku'";
		if ($not_deleted) $sql .= " AND products.is_deleted = 'N' AND product_options.is_deleted = 'N'";
		if ($only_active) $sql .= " AND products.is_active = 'Y' AND product_options.is_active = 'Y'";
		
		return db_query_array($sql);
	}
	function getByIds($option_id,$product_id){
		if(!trim($option_id) || !trim($product_id)){
			return false;
		}

		$option_id = addslashes($option_id);
		$product_id = addslashes($product_id);

		$sql = "SELECT product_options.*, products.*,
				product_options.id AS product_option_id,
				product_options.vendor_sku AS product_option_vendor_sku,
				product_options.essensa_price AS product_option_essensa_price
				 FROM product_options 
				LEFT JOIN products ON product_options.product_id = products.id
				WHERE product_options.id = '$option_id' AND product_options.product_id = '$product_id'";
		return db_query_array($sql);
	}
	
function getParentID($child_vendor_sku="", $own_product_id = ""){		
		$sql = 'SELECT products.id FROM product_options 
				LEFT JOIN products ON products.id = product_options.product_id 
				WHERE product_options.vendor_sku = "'.addslashes($child_vendor_sku).'"';
		if ($own_product_id <> '') $sql .= " AND product_options.product_id <> " . $own_product_id ;
		
		$result = db_query_array($sql);
		
		if($result){
			foreach($result as $prod){
				if($prod['id'] > 0){
					return (int) $prod['id'];
				}
			}
		}
		else{
			return false;
		}
	}
	
    static function duplicateOptions($old_pid, $new_pid, $sku_suffix = ''){

        $options = db_query_array("SELECT * FROM product_options WHERE product_id = $old_pid");
        if(sizeof($options) == 0){
            return;
        }

        //duplicate product available options
        $insert_available_options = "INSERT INTO product_available_options (product_id, options_values_id, additional_price, optional, essensa_additional_price, preset_id, display_order) 
            SELECT $new_pid, options_values_id, additional_price, optional, essensa_additional_price, preset_id, display_order 
            FROM product_available_options WHERE product_id = $old_pid";
        $res = db_query($insert_available_options);
        if(!$res){
            die( ' <h1>Error inserting product options</h1>');       
        }
        //get a map of old option to new option, and parent_ids
        $get_available_options = "SELECT pa1.id as old_id, pa1.parent_id as old_parent_id, pa2.id as new_id,
            pg.id as group_id,  pg.parent_id as group_parent, pl.id as label_id
            FROM product_available_options pa1 JOIN product_available_options pa2 ON pa1.options_values_id = pa2.options_values_id 
            LEFT JOIN product_available_option_groups pg ON pa1.product_available_option_groups_id = pg.id 
            LEFT JOIN product_available_option_labels pl on pa1.product_available_option_labels_id = pl.id
            WHERE pa1.product_id = $old_pid AND pa2.product_id = $new_pid 
            AND pa1.display_order = pa2.display_order  "; 
        $available_options = db_query_array($get_available_options);
        $available_options_map = array();
        $ids_to_parents = array();
        $groups = array();
        $group_parents = array();
        $labels = array();
        $avail_opt_groups_insert = "INSERT INTO product_available_options (id, product_available_option_groups_id) VALUES ";
        $avail_opt_labels_insert = "INSERT INTO product_available_options (id, product_available_option_labels_id) VALUES ";
        foreach($available_options as $curr){
            $available_options_map[$curr['old_id']] = $curr['new_id'];
            //check if we receive null or 'null'
            if(!is_null($curr['parent_id'])){
                $ids_to_parents[$curr['old_id']] = $curr['parent_id'];
            }
            //if there is a group id, create a new group if it hasn't been created and write the sql to update the new row
            //with the new group id
            if(!is_null($curr['group_id'])){
                if( !isset($groups[$curr['group_id']])){
                    $sql = "INSERT INTO product_available_option_groups (product_id, title, description, hide_until_required_set)
                        SELECT $new_pid, title, description, hide_until_required_set FROM product_available_option_groups WHERE
                       id = {$curr['group_id']}";
                    $ret =db_query($sql);
                    if(!$ret){
                        die('Error inserting option groups');
                    }
                    $groups[$curr['group_id']] = db_insert_id();
                }
                if(!is_null($curr['group_parent'])){
                    $group_parents[$curr['group_id']] = $curr['group_parent'];
                }
                $avail_opt_groups_insert .= "(" . $curr['new_id'] . ", " . $groups[$curr['group_id']] . "), ";
            }
            //create a new label if it hasn't been created and write the sql to update the new row
            //with the new label id
            if(!is_null($curr['label_id'])){
                if(!isset($labels[$curr['label_id']])){
                    $sql = "INSERT INTO product_available_option_labels (product_id, option_id, label, message_above, message_below, optional, required_if_visible, is_preview) 
                    SELECT $new_pid, option_id, label, message_above, message_below, optional, required_if_visible, is_preview FROM  product_available_option_labels 
                    WHERE id = {$curr['label_id']}";
                    $ret = db_query($sql);
                    echo 'the ret for insert label: ' . $ret;
                    if(!$ret){
                        die('Error creating option labels');
                    }
                    $labels[$curr['label_id']] = db_insert_id();
                    echo 'the db_insert_id: ' . $labels[$curr['label_id']];
                }
                $avail_opt_labels_insert .= "(" . $curr['new_id'] . ", " . $labels[$curr['label_id']] . "), ";
            }
        }

        //if there were groups, update them now
        if(sizeof($groups)){
            $avail_opt_groups_insert = substr($avail_opt_groups_insert, 0, -2) . ' ON DUPLICATE KEY UPDATE product_available_option_groups_id = VALUES(product_available_option_groups_id)';
            $ret = db_query($avail_opt_groups_insert);
            if(!$ret){
                die('Error update option groups');
            }
            //if any of the groups had parents, update those
            if(sizeof($group_parents)){
                $sql = "INSERT INTO product_available_option_groups (id, parent_id) VALUES ";
                foreach($group_parents as $old_child => $old_parent){
                   $sql .= "(" . $groups[$old_child] . ", " . $groups[$old_parent] . '), ';
                }
                $sql = substr($sql, 0, -2) . " ON DUPLICATE KEY UPDATE parent_id = VALUES(parent_id)";
                $ret = db_query($parent_id);
                if(!$ret){
                    die('Error creating option groups');
                }
            }
        }
        if(sizeof($labels)){
            $avail_opt_labels_insert = substr($avail_opt_labels_insert, 0, -2) . ' ON DUPLICATE KEY UPDATE product_available_option_labels_id = VALUES(product_available_option_labels_id)';
            $ret = db_query($avail_opt_labels_insert);
            if(!$ret){
                die('Error update option labels');
            }
            echo 'the labels update query: ' . $avail_opt_labels_insert;
        }


        foreach( $options as $option )
        {
            unset($option['id']);
            $option['product_id'] = $new_pid;
            $option['mfr_part_num'] = $option['mfr_part_num'] . $sku_suffix; 
            $old_available_options = explode(',', trim($option['product_option_values_set'], ','));
            $these_options = ',';
            foreach($old_available_options as $curr){
                $these_options .= $available_options_map[$curr] . ',';
            }
            $option['product_option_values_set'] = $these_options;
            Products::insertProductOption($option);
        }
    }

    static function getRelatedProductForAvailableOptions($option_id, $available_options_ids, $product_id){
        $option_available_options = explode(',',ProductOptionCache::getOptionsSetForProductOption($option_id, $product_id));
        $available_options = array_unique(array_merge($option_available_options, explode(',', $available_options_ids)));
        return ProductOptionCache::getRelatedProductsForOptions($available_options);
    }
   static function getOptionText($option_id, $optionals, $product_id = 0){ 
        if(!isset($_SESSION['product_options'][$option_id])){
            if(!$product_id){
                $sql = "SELECT product_id FROM product_options
                    WHERE id = $option_id";
                $res = db_query_array($sql);
                $product_id = $res[0]['id'];
            }   
            ProductOptionCache::setProductOptionCache($product_id);
        }   
        $return = array($_SESSION['product_options'][$option_id]); 
        foreach(explode(",", $optionals) as $curr){
            $return[] = $_SESSION['optional_options'][$curr];
        }   
        return $return;
    }   

    static function getOptionInfoByOptionId($option_id, $product_id){
        return ProductOptionCache::getOptionInfoByOptionId($option_id, $product_id);
    }   

    static function getOptionsSetForDisplay($option_id, $optionals){
        $options = ProductOptions::getOptionText($option_id, $optionals);
        $string = ''; 
        foreach($options as $curr){
            $string .= $curr['option_text'] . ', ';
        }   
        return substr($string, 0, -2);
    }   
    
    static function displayOptionTextFromSet($options_set, $prod_id =0, $presets){
        return ProductOptionCache::getTextFromSet($options_set, $prod_id, $presets);
        }   
    static function getFromOptionsSet($prod_id, $options_set){
        $sql = 'SELECT * FROM product_options WHERE product_id ='
        .  $prod_id . " AND product_option_values_set = '"
        . $selected_options . "'";
        return db_query_array($sql);
    }   


}

?>
