<?php 
class Product_Questions
{
    public static function get($id = 0, $product_id = 0, $customer_id = 0, $customer_query ='', $text = '', $order_by = '', $order_asc = '', $approved='', $get_answers=false, $total='', $start_date='',$end_date='',$keywords='')
    {
    	$customer_query = trim($customer_query);
    	
        $sql = " SELECT product_questions.*,
	        		products.name as product_name,
	        		customers.first_name as customer_fname,
	        		customers.last_name as customer_lname, 
	        		customers.img_url as customer_img_url
                FROM product_questions ";
        
        if( $total ){
        	$sql = " SELECT COUNT(DISTINCT product_questions.id) as total
                            FROM product_questions ";
        }
        
        if($get_answers){
        	$sql .= " LEFT JOIN product_answers ON product_questions.id = product_answers.question_id ";
        }
        
        $sql .= " LEFT JOIN products ON product_questions.product_id = products.id
        		LEFT JOIN customers ON (product_questions.customer_id = customers.id ";
        
        if($get_answers){
        	$sql .= " OR product_answers.customer_id = customers.id ";
        }     
        
        $sql .= ") WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND product_questions.id = $id ";
        }

        if ($product_id > 0)
        {
            $sql .= " AND product_questions.product_id = $product_id ";
        }

        if ($customer_id > 0)
        {
            $sql .= " AND product_questions.customer_id = $customer_id ";
        }
        
        if ($customer_query && $customer_query != ''){
        	$sql .= " AND ";
        	$fields = Array('customers.first_name',
        			'customers.last_name',
        			'customers.id',
        			'customers.email'  );
        
        	$sql .= db_split_keywords($customer_query,$fields,'OR',true);
        }
        
        if(isset($start_date) || isset($end_date)){
        	if($start_date){
        		$start_date .= " 00:00:00";
        	}
        	if($end_date){
        		$end_date .= " 23:59:59";
        	}
        	$q .= db_queryrange('product_questions.date',$start_date,$end_date);
        }

        if ($text != '')
        {
            $sql .= " AND product_questions.text = '$text' ";
        }
		if ($approved != '')
        {
            $sql .= " AND product_questions.approved = '$approved' ";
        }
        
        if (trim($keywords) != '') {
        	$keywords = mysql_real_escape_string($keywords);
        	$fields = Array('products.name','products.description','products.vendor_sku', 'products.id', 'products.aka_sku');
        	$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
        }
        
        if(!$total){
	    	if ($order_by == '')
	    	{
	    		// Default Sort Here
	    		$sql .= " ORDER BY product_questions.id DESC ";
	    	}
	    	else
	    	{
	    		$order_by = db_esc($order_by);
	    			
	    		$sql .= " ORDER BY $order_by ";
	    
	    		if($order_asc == "DESC" || $order_asc == "" || $order_asc == false){
	    			$order_asc = false;
	    		} else {
	    			$order_asc = true;
	    		}
	    
	    		if ($order_asc)
	    		{
	    			$sql .= " ASC ";
	    		}
	    		else
	    		{
	    			$sql .= ' DESC ';
	    		}
	    	}
    	}
    	
    	//echo $sql;    	 
    	$result = db_query_array($sql);
    	
    	if($total){
    		return (int)$result[0]['total'];
    	}
    	
    	return $result;
    }
    
    public static function get_answers($id = 0, $question_id = 0, $customer_id = 0, $customer_query ='', $text = '', $order_by = '', $order_asc = '', $approved='', $total='', $start_date='',$end_date='')
    {
    	$customer_query = trim($customer_query);
    	 
    	$sql = " SELECT product_answers.*,
	        		customers.first_name as customer_fname,
	        		customers.last_name as customer_lname,
	        		customers.img_url as customer_img_url,
    				admin_users.first_name as admin_name
                FROM product_answers ";
    
    	if( $total ){
    		$sql = " SELECT COUNT(DISTINCT product_answers.id) as total
                            FROM product_answers ";
    	}
    
    	$sql .= " LEFT JOIN customers ON product_answers.customer_id = customers.id
    			  LEFT JOIN admin_users ON product_answers.admin_id = admin_users.id ";

    	$sql .= " WHERE 1 ";
    
    	if ($id > 0)
    	{
    		$sql .= " AND product_answers.id = $id ";
    	}
    
    	if ($question_id > 0)
    	{
    		$sql .= " AND product_answers.question_id = $question_id ";
    	}
    
    	if ($customer_id > 0)
    	{
    		$sql .= " AND product_answers.customer_id = $customer_id ";
    	}
    
    	if ($customer_query && $customer_query != ''){
    		$sql .= " AND ";
    		$fields = Array('customers.first_name',
    				'customers.last_name',
    				'customers.id',
    				'customers.email'  );
    
    		$sql .= db_split_keywords($customer_query,$fields,'OR',true);
    	}
    
    	if(isset($start_date) || isset($end_date)){
    		if($start_date){
    			$start_date .= " 00:00:00";
    		}
    		if($end_date){
    			$end_date .= " 23:59:59";
    		}
    		$q .= db_queryrange('product_answers.date',$start_date,$end_date);
    	}
    
    	if ($text != '')
    	{
    		$sql .= " AND product_answers.text = '$text' ";
    	}
    	if ($approved != '')
    	{
    		$sql .= " AND product_answers.approved = '$approved' ";
    	}
    
    	if(!$total){
	    	if ($order_by == '')
	    	{
	    		// Default Sort Here
	    		$sql .= " ORDER BY product_answers.id DESC ";
	    	}
	    	else
	    	{
	    		$order_by = db_esc($order_by);
	    			
	    		$sql .= " ORDER BY $order_by ";
	    
	    		if($order_asc == "DESC" || $order_asc == "" || $order_asc == false){
	    			$order_asc = false;
	    		} else {
	    			$order_asc = true;
	    		}
	    
	    		if ($order_asc)
	    		{
	    			$sql .= " ASC ";
	    		}
	    		else
	    		{
	    			$sql .= ' DESC ';
	    		}
	    	}
    	}
    	
    	//echo $sql;
    	$result = db_query_array($sql);
    	
    	if($total){
    		return (int)$result[0]['total'];
    	}
    	
    	return $result;
    	
    }
		
    public static function get_product_questions_with_answers($product_id, $approved_questions = 'Y', $approved_answers = 'Y')
    {
        /* make sure we are dealing with good data */
        if (!$product_id && !is_int($product_id))
        {
            return null;
        }
        
        $questions = Product_Questions::get(0,$product_id,0,'','','','',$approved_questions);

        foreach ($questions as $key => $current_question){
        	$questions[$key]['answers'] = Product_Questions::get_answers(0,$current_question['id'],0,'','','','',$approved_answers);
        }
        return $questions;
    }

    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }
    
    public static function get1Answer($id)
    {
    	$id = (int) $id;
    
    	if (!$id)
    	{
    		return false;
    	}
    
    	$result = self::get_answers($id);
    	return $result[0];
    }

    public static function insert_question($info)
    {
		$info['text'] = mysql_real_escape_string($info['text'] );
		
		if (!$info['approved']){
			$info['approved']='P';
        } 
        
        return db_insert('product_questions', $info);
    }
    
    public static function insert_answer($info)
    {
    	$info['text'] = mysql_real_escape_string($info['text'] );

    	if (!$info['approved']){
    		$info['approved']='P';
    	}
    	
    	$return = db_insert('product_answers', $info);
    	
    	if($return && $info['approved'] == 'Y'){
    		Product_Questions::send_question_answered_alert($return);
    	}
    	
    	return $return;
    }

    public static function delete_question($id)
    {
        //delete answers to this question
		db_delete('product_answers', $id, 'question_id');
		
		//delete question
		return db_delete('product_questions', $id);
    }
    
    public static function delete_answer($id)
    {
    	return db_delete('product_answers', $id);
    }

    public static function update_question($id, $info){  
    foreach ($info as $key => $value) {
    		$info[$key] = mysql_real_escape_string(stripslashes($value));
    	}
 
		return db_update('product_questions', $id, $info);
    }
    
    public static function update_answer($id, $info){
    	foreach ($info as $key => $value) {
    		$info[$key] = mysql_real_escape_string(stripslashes($value));
    	}
    	
    	$original_answer = self::get1Answer($id);
    	$result = db_update('product_answers', $id, $info);
 
    	// Notify customer his question was answered
    	if($result){    		
    		if(($info['approved']=='Y')&&($original_answer['approved']!='Y')){   		
    			Product_Questions::send_question_answered_alert($id);
    		}
    	}
    	
     	return $result;
    }
    
    static function display_product_questions($product_id,$questions){
		
    	$count_answers = 0;
    	
    	
    	
		if ($questions)
		{
			foreach ($questions as $current_question){
				$count_answers += count($current_question['answers']); 
			}
			
// 			$pending_customer_question = Product_Questions::get(0,$product_id,$_SESSION['cust_acc_id'],'','','','','P');
// 			if ($pending_customer_question) {
// 				$text_to_write = 'Edit your question';
// 				$question_onclick = "onclick=\"TINY.box.show({url:'ajax/ajax.product_questions.php',post:'product_id=" . $product_id."',fixed:false});return false;\"";
// 			} else {
// 					$text_to_write = 'Ask a question';
// 				if($_SESSION['cust_acc_id']){
// 					$question_onclick = "onclick=\"TINY.box.show({url:'ajax/ajax.product_questions.php',post:'product_id=" . $product_id."',fixed:false});return false;\"";
// 				} else {
// 					$question_onclick = "onclick=\"TINY.box.show({url:'popuplogin.php',post:'link=".$CFG->baseurl . "product_questions.php?product_id=" . $product_id."'});return false;\"";
// 				}
// 			}

			$text_to_write = 'Ask a question';
			?>
			<div class="questions-full-comments-top" id="customer_questions">
	
				<h3>Customer Questions
					<?php 
					if($text_to_write){
						?>
						<span class="product-actions">
							<a class="question_overlay_link" href="<?=$CFG->baseurl . "product_questions.php?product_id=" . $product_id?>" <?= $question_onclick?>> <?=$text_to_write?></a>
						</span>
						<?php 
					}
					?>
				</h3>
				<?php 
				echo "<span class='count-questions'>" ;
				echo (count($questions)==1) ? '1 Question' : count($questions).' Questions' ;
				echo "</span>";
				if ($count_answers){
					echo "<span class='count-answers'>" ;
					echo ($count_answers==1) ? "1 answer" : $count_answers." answers";
					echo "</span>";
				}
				?>
			</div>   

			<div id="question-section-wrap">
				<?	Product_Questions::listQuestions($product_id,$questions) ?>			 
			</div>
			
			<?php 
		}else{
			if ($_SESSION['cust_acc_id']) {
				$onclick = "onclick=\"TINY.box.show({url:'ajax/ajax.product_questions.php',post:'product_id=" . $product_id."',fixed:false});return false;\"";
			} else {
				$onclick = "onclick=\"TINY.box.show({url:'popuplogin.php',post:'link=".$CFG->baseurl . "product_questions.php?product_id=" . $product_id."'});return false;\"";
			}
			?>
			<h3><label>No Questions Yet</label></h3> 
			<p>Be the first to <a href="#" <?=$onclick?>>ask</a> a question!</p>
			<?
		}
	}
	
	function listQuestionsRedesigned($product_id, $questions, $product_name){
		?>
		<div class="ask-question-button">
			<a class="question_overlay_link" onClick="sendToDataLayer('Ask a Question - Product', 'Q&A Section---product:<?=htmlspecialchars(str_replace("'","",$product_name), ENT_QUOTES)?>');" href="<?=$CFG->baseurl . "product_questions.php?product_id=" . $product_id?>" <?= $question_onclick?>>Ask a Question</a>
		</div> 
		<div>
			<?php 
			$question_num = 0;
			foreach ($questions as $question){
				$question_num++;
				$customer = Customers::get1($question['customer_id']);
				$name_str = $question['nickname'] ? $question['nickname'] : strtoupper(substr($customer['first_name'],0,1)) . ". " . $customer['last_name'] ;
				?>
				<div class="comments-details">
					<div class="full-comment-content" id="question<?=$question_num?>">	
						<div class="questions-left">
							<p class="question-text">
								<?php 
								$question['text'] = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $question['text'] );
								$question['text'] = str_replace ( "<br/><br/>", "<br/>", $question['text'] );
								echo stripslashes($question['text']);
								?>
							</p>
							<div class="hidden-question-section-<?=$question_num ?>">
								<div class="question-heading">
									<div class="author-details">By <span class="author"><?=$name_str?></span>
										<?
										if (($customer['role_caption']) && ($customer['role_id'] != 11)) {
											echo " - ";
											if (($customer['institution_type_caption']) && ($customer['institution_type_id'] != 19))
												echo $customer['institution_type_caption'] . "&nbsp;" . $customer['role_caption'];
										}
										?>
										<span class="question-date"><?=date("F j, Y", strtotime($question['date']));?></span>
									</div>
								</div>
								<?
								$answer_count = count($question['answers']);
								$answer_num = 0;
								if ($answer_count > 0) {
									?>
									<div class="answers-section">
									<?php 
										foreach ( $question['answers'] as $answer ) {
											$answer_num++;  
			
											$answer['text'] = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $answer['text'] );
											$answer['text'] = str_replace ( "<br/><br/>", "<br/>", $answer['text'] );
											
											if($answer['customer_id']){
												$author = strtoupper(substr($answer['customer_fname'],0,1)) . ". " . $answer['customer_lname'] ;
											}else{
												$author = ucfirst($answer['admin_name']) . " - TigerChef Staff";
											}
											
											$last_comment = ($answer_num == $answer_count) ? 'last_comment' : '';											
											?>
							 			   	<div class="rest-comment <?=$last_comment?>">
							 			   		<p><?=stripslashes($answer['text'])?></p>
						 			   			<div class="rest-comment-head">
													<span>
														<span class="author">by&nbsp;<?=$author?></span>
														<span class="date"><?=date("F j, Y", strtotime($answer['date']))?></span>
													</span> 
												</div>
											</div>
											<?
										}
										?>
									</div>
									<?php 
								}
								?>
							</div>
						</div>
						<div class="question-right">
							<? 
							if ($_SESSION['cust_acc_id']) {
								$onclick = "TINY.box.show({url:'ajax/ajax.product_questions.php',post:'action=show_answer_form&question_id=".$question['id']."',fixed:false});return false;";
							}else{ 
								$onclick = "TINY.box.show({url:'popuplogin.php',post:'type=answer_question&question_id=".$question['id']."'});return false;";
							}
							?>
							<div class="answer-info">
								<div class="answer-count"><?=$answer_count?> answers</div>
								<?php
								if($answer_count > 0){ ?>
									<div class="last-answer">Last answer: <?=date("F j, Y", strtotime($question['answers'][0]['date']))?></div>
									<?php 
								} ?>
							</div>
							<div class="hidden-question-section-<?=$question_num ?> ">
								<div class="answer-question-button">
									<a class="comment-link" href="#" onclick="<?=$onclick?>">Answer this Question</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php 
			}	
			?>
		</div>
		<?php 
	}
	
	function listQuestions($product_id, $questions){
		?>
		<div>
			<?php 
			$question_num = 0;
			foreach ($questions as $question){
				$question_num++;
				$customer = Customers::get1($question['customer_id']);
				$name_str = $question['nickname'] ? $question['nickname'] : strtoupper(substr($customer['first_name'],0,1)) . ". " . $customer['last_name'] ;
				?>
				<div class="comments-details">
					<div class="full-comment-content" id="question<?=$question_num?>">	
						<div class="question-heading">
							<div class="author-details">By <span class="author"><?=$name_str?></span>
								<? 
								if (($customer['role_caption']) && ($customer['role_id'] != 11)) {
									echo " - ";
									if (($customer['institution_type_caption']) && ($customer['institution_type_id'] != 19))
										echo $customer['institution_type_caption'] . "&nbsp;" . $customer['role_caption'];
								}
								?>
							</div>
							<span class="question-date"><?=date("F j, Y", strtotime($question['date']));?></span>
							<?php 
							$question['text'] = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $question['text'] );
							$question['text'] = str_replace ( "<br/><br/>", "<br/>", $question['text'] );
							?> 
						</div>	
						<p>
							<?= stripslashes($question['text'])?>
						</p>
						
						<div class="question-bottom">
							<? 
							if ($_SESSION['cust_acc_id']) {
								$onclick = "TINY.box.show({url:'ajax/ajax.product_questions.php',post:'action=show_answer_form&question_id=".$question['id']."',fixed:false});return false;";
							}else{ 
								$onclick = "TINY.box.show({url:'popuplogin.php',post:'link=".Catalog::makeProductLink_(Products::get1($product_id))."#question".$question_num."'});return false;";
							}
							?>
							<div>
								<span>Know the answer to this question?</span>
								<a class="comment-link" href="#" onclick="<?=$onclick?>">Answer Question</a>
							</div>
						</div>
						<?
						if ($question['answers']) {
							echo "<h3>Answers</h3>";
							foreach ( $question['answers'] as $answer ) {

								$answer['text'] = preg_replace ( '#(\\\r|\\\r\\n|\\\n)#', '<br/>', $answer['text'] );
								$answer['text'] = str_replace ( "<br/><br/>", "<br/>", $answer['text'] );
								
								if($answer['customer_id']){
									$author = strtoupper(substr($answer['customer_fname'],0,1)) . ". " . $answer['customer_lname'] ;
								}else{
									$author = ucfirst($answer['admin_name']) . " - TigerChef Staff";
								}
								?>
				 			   		<div class="rest-comment">
				 			   			<div class="rest-comment-head">
											<span>
												<span class="author"><?=$author?></span>&nbsp;answered: 
											</span> 
										<em class="date"><?=date("F j, Y", strtotime($answer['date']))?></em>
									</div>
									<p><?=stripslashes($answer['text'])?></p>
								</div>
								<?
							}
						}
						?>
					</div>
				</div>
				<?php 
			}	
			?>
		</div>
		<?php 
	}
	
	function send_question_answered_alert($answer_id){
		
		$answer = Product_Questions::get1Answer($answer_id);
		$question = Product_Questions::get1($answer['question_id']);

		if ($question['notify_answers']=='Y'){
			
			$product = Products::get1($question['product_id']);
			$imageurl = Catalog::makeProductImageLink($question['product_id'], false);
			$imageurl = str_replace("https", "http", $imageurl);
			
			$vars['full_name'] = $question['nickname'] ? $question['nickname'] : $question['customer_fname'].' '.$question['customer_lname'];
			$vars['product_link'] = Catalog::makeProductLink_($product);
			$vars['product_image'] = $imageurl;
			$vars['product_name'] = $product['name'];
			$vars['date'] = date("F j, Y", strtotime($answer['date']));
			if($answer['customer_id']){
				$vars['author'] = strtoupper(substr($answer['customer_fname'],0,1)) . ". " . $answer['customer_lname'] ;
			}else{
				$vars['author'] = "<b>" . ucfirst($answer['admin_name']) . " - TigerChef Staff</b>";
			}
			
			$vars['content'] = preg_replace('#(\\\r|\\\r\\n|\\\n)#', '<br/>',$answer['text']);
			$vars['content'] = str_replace("<br/><br/>","<br/>",$vars['content']);
			$vars['content'] = stripslashes($vars['content'])."<br/>";
			
			$cust = Customers::get1($question['customer_id']);
			$to_email = $question['email']? $question['email'] : $cust['email'];
						
			if (!$question['customer_id'] || ($question['customer_id'] >0 && $question['customer_id'] != $answer['customer_id'])){
				$E = TigerEmail::sendOne($CFG->company_name,$to_email,"question_answered_alert", $vars);
			}
		}
	}
	
	function processAnswer($info,$type='') {
	
		$robotest = $_GET['robotest'];
	
		if (!$_SESSION['cust_acc_id']) 	// User is not loged in. He should login first.
		{
			$return['error'] = "You must login to answer. <a href='/account.php'>Click here to login now >></a>";
		}
	
		if (($info['text'] == ' ') || (! $info['text'])) 	// comment is blank
		{
			$return['error'] =  "Please type your answer!";
		}
	
		if (($info['text'] != ' ') && ($info['text']) && ($robotest=="") && ($_SESSION['cust_acc_id'])) {
			$info ['customer_id'] = $_SESSION['cust_acc_id'];
	
			$result = Product_Questions::insert_answer($info);
	
			$product = Products::get1 ( db_get1(Product_Questions::get('',$info['question_id'])));
			$pname = $product ['name'];
	
			$msg = "A answer for question ";
			$msg .= $info['question_id']. " for $pname";
			$msg .= "was added <br/>";
			$msg .= "Here is a copy of the new answer:<br />";
			$msg .= "Answer: " .$info['text'] ."<br/>";
	
// 			$to_email = ($CFG->in_testing) ? $CFG->testing_email : "estee@tigerchef.com,david@tigerchef.com";
	
			//$to_email = 'tova@tigerchef.com';
			
			$vars ['body'] = $msg;
	
			if($CFG->in_testing){
				$E = TigerEmail::sendOne ( $CFG->company_name, $CFG->testing_email, "blank", $vars, "Answer for question # ".$info['question_id']." was submitted for " . $pname, true, $CFG->company_name, $CFG->company_email );
			}else{
				$E = TigerEmail::sendOne ( $CFG->company_name, 'estee@tigerchef.com', "blank", $vars, "Answer for question # ".$info['question_id']." was submitted for " . $pname, true, $CFG->company_name, $CFG->company_email );
				$E = TigerEmail::sendOne ( $CFG->company_name, 'chaya@suburbanrestequipment.com', "blank", $vars, "Answer for question # ".$info['question_id']." was submitted for " . $pname, true, $CFG->company_name, $CFG->company_email );
				$E = TigerEmail::sendOne ( $CFG->company_name, 'etty@suburbanrestequipment.com', "blank", $vars, "Answer for question # ".$info['question_id']." was submitted for " . $pname, true, $CFG->company_name, $CFG->company_email );
				
			}
			
// 			$subject = "Answer for question # ".$info['question_id']." was submitted for " . $pname;
// 			mail($to_email,$subject,$msg);
			//$E = TigerEmail::sendOne ( $CFG->company_name, $to_email, "blank", $vars, "Answer for question # ".$info['question_id']." was submitted for " . $pname, true, $CFG->company_name, $CFG->company_email );
		}
	
		if ($result) {
			$return['success'] = "<div><span class='thanks-for-commenting'>Thank you for your answer!</span><span class='comment-pending-approval'>Your answer is pending approval.</span></div>";
		}
	
// 		if($type === 'popup'){
			echo json_encode($return);
// 		}else{
// 			return $return['error'];
// 		}
	}
	
	function processQuestion($info,$type='') {
// 	print_ar($info);
		$info['customer_id'] = $_SESSION['cust_acc_id'];
	
// 		if (! $info['customer_id']) 	// User is not loged in. He should login first.
// 		{
// 			$return['error'] = "You must login to ask a question. <a href='/account.php'>Click here to login now >></a>";
// 		}
	
		// Validate reqired fields
		foreach ( $info as $key => $value ) {
			if ( !$value && $key!=='id' && $key!=='customer_id') {
				$return['error'][$key] = 'This is a required field';
				$error = 1;
			}
		}
		
		if($info['email']){
			$valid = validate_email($info['email'],true);
			if(!$valid){
				$return['error']['email'] = "Please enter a valid email.";
			}
		}
		//print_ar($return);
		if (!$return['error'] && (! $info['robotest'])) {
	
			if(!$info['approved']){ $info ['approved'] = 'P'; }
			$info['text'] = preg_replace ( '#(\r\n)#', '\\n', $info ['text'] );
	
			if ($info ['notify_answers']) {
				$info ['notify_answers'] = 'Y';
			} else {
				$info ['notify_answers'] = 'N';
			}
	
			if($info['id']){
				//if there is an id - update the question
				$result = Product_Questions::update_question($info['id'],$info);
			} else {
				//this is a new question
				$result = Product_Questions::insert_question($info);
				$new_question = true;
			}
			if ($result) {
	
				if($new_question){
					//only send an email for a new question - since we are only letting them edit it while it is pending
					$item = Products::get1 ( $info['product_id'] );
					$itemName = $item ['name'];
					$itemUrl = $item ['url_name'];
	
					$cust = Customers::get1 ( $info['customer_id'] );
					$custFullName = $cust ['first_name'] . " " . $cust ['last_name'];
					$custEmail = $cust ['email'];
	
					$msg = "A question was asked by ";
					$msg .= "<a href='mailto:$custEmail'>$custFullName</a> ";
					$msg .= "about <a href='$CFG->baseurl$itemUrl.html'>$itemName</a><br/>";
	
	
					$msg .= "Here is a copy of the new question:<br />";
					$msg .= $info['text'] ."<br/>";
	
// 					$to_email = ($CFG->in_testing) ? $CFG->testing_email : "estee@tigerchef.com,david@tigerchef.com";
	
					// 				$to_email = 'tova@tigerchef.com';
	
					$vars ['body'] = $msg; 
									
					if($CFG->in_testing){
						$E = TigerEmail::sendOne ( $CFG->company_name, $CFG->testing_email, "blank", $vars, "A Question Was Submitted for " . $item['name'], true, $CFG->company_name, $CFG->company_email );
					}else{
						$E = TigerEmail::sendOne ( $CFG->company_name, 'estee@tigerchef.com', "blank", $vars, "A Question Was Submitted for " . $item['name'], true, $CFG->company_name, $CFG->company_email );
						$E = TigerEmail::sendOne ( $CFG->company_name, 'chaya@suburbanrestequipment.com', "blank", $vars, "A Question Was Submitted for " . $item['name'], true, $CFG->company_name, $CFG->company_email );
						$E = TigerEmail::sendOne ( $CFG->company_name, 'etty@suburbanrestequipment.com', "blank", $vars, "A Question Was Submitted for " . $item['name'], true, $CFG->company_name, $CFG->company_email );
						
					}
						
					
// 					$E = TigerEmail::sendOne ( $CFG->company_name, $to_email, "blank", $vars, "A Question Was Submitted for " . $item['name'], true, $CFG->company_name, $CFG->company_email );
	
				}
					
				$return['success'] = "<div><span class='thanks-for-commenting'>Thank you for submitting a question!</span><span class='comment-pending-approval'>Your question is pending approval.</span></div>";
			}
		}
// 		if($type === 'popup'){
// 			echo json_encode($return);
// 		}else{
			return $return;
// 		}
	
	}
	
	function deleteQuestion($question_id,$type='') {
		$question = Product_Questions::get1($question_id);
		if($question['customer_id'] == $_SESSION['cust_acc_id']){
			if (Product_Questions::delete_question($question_id) === false) {
				$return['message'] = "Error deleting question.";
			} else {
				$return['message'] = "Your question was successfully deleted.";
				$return['success'] = "success";
			}
		} else {
			$return['message'] = "Error deleting question.";
		}
	
// 		if($type === 'popup'){
// 			echo json_encode($return);
// 		}else{
			return $return;
// 		}
	}
	

}
?>