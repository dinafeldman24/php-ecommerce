<?

class BlogLinkCat{

	static function insert($info){
		return db_insert("blog_link_cats",$info);
	}

	static function update($id,$info){
		return db_update("blog_link_cats",$id,$info);
	}

	static function delete($id){
		return db_delete("blog_link_cats",$id);
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT blog_link_cats.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(blog_link_cats.id)) as total ";
		}

		$from_ = " FROM blog_link_cats ";

		$where_ = " WHERE 1 ";

		if($o->id > 0){
			$where_ .= " AND blog_link_cats.id = [id] ";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY blog_link_cats.id ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}


}