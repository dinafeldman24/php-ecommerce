<?php

class Session
{
    /* Open session */
    function _open($path, $name) 
    {
        return TRUE;
    }

    /* Close session */
    function _close() 
    {
        /* This is used for a manual call of the
           session gc function */
        $this->_gc(0);
        return TRUE;
    }

    /* Read session data from database */
    function _read($ses_id) 
    {
    	$result = db_query_array("SELECT session_value FROM sessions WHERE session_id = '$ses_id'",'',true);
    	
        if (!$result) {
            return '';
        }
		else {
			return $result['session_value'];
		}
    }

    /* Write new data to database */
    function _write($ses_id, $data) 
    {    	
    	
    	$ua =  $_SERVER['HTTP_USER_AGENT'] ;
    	if (strpos($ua, '+http') == true || strpos($ua, 'http:') == true ||
    	strpos($ua, 'www.') == true || strpos($ua, '@') == true)
    	{
    		//mail('tova@tigerchef.com',session_id().'spider! '. $ua ,'');
    		// i'm spider
    		return false;
    	}else{
    		if($ua){
				//mail('tova@tigerchef.com,rachel@tigerchef.com',session_id(),var_export(array($ua,$_SERVER),true));  
			}
    	 
	   		$session_res = db_query("UPDATE sessions  
					   				  SET session_time=NOW(), 
					   					  session_value='".addslashes($data)."'
					   		          WHERE session_id='$ses_id'");
	   		
	        if (!$session_res) {
	            return FALSE;
	        }
	        else if (db_affected_rows()) {
	            return TRUE;
	        }
			else {
				
				if (isset($_SERVER['HTTP_REFERER'])) $referrer = $_SERVER['HTTP_REFERER'];
				else $referrer = '';
								
				if(strpos($referrer,'://64.65.60.176/') || strpos($referrer,'www.justchargerplates.com'))
					$referrer = '';
					
		        $session_sql = "INSERT INTO sessions (session_id, session_time, session_start, session_value, 
		        						ip_address, user_agent, referrer) 
		        				VALUES ('$ses_id', NOW(), NOW(), '".addslashes($data)."',
		        					'".$_SERVER['REMOTE_ADDR']."','".addslashes($_SERVER['HTTP_USER_AGENT'])."','".addslashes($referrer)."')";
		        $session_res = db_query($session_sql,false,false,true);
		        
		        if (!$session_res) {    
		            return FALSE;
		        }
		        else {
		            return TRUE;
		        }
			}
    	}
    }
    
    /* Destroy session record in database */
    function _destroy($ses_id) 
    {
        $session_res = db_query("DELETE FROM sessions WHERE session_id = '$ses_id'");
        
        if (!$session_res) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    /* Garbage collection, deletes old sessions */
    function _gc($life) 
    {
    	return FALSE; // for now don't delete anything
        $ses_life = strtotime("-5 minutes");	// override life and delete things after x

        $session_res = db_query("DELETE FROM sessions WHERE session_time < $ses_life");

        if (!$session_res) 
        {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function getReferrer($ses_id){
    	$return = db_query_array("SELECT referrer FROM sessions WHERE session_id = '$ses_id'");
    	return $return[0]['referrer'];
    }
    
    function updateCustomerInfo($ses_id,$email='',$customer_name='',$country=''){
        //need at least one field to update
        if(!$email && !$customer_name && !$country){
            return ;
        }

    	$sql = "UPDATE sessions
    			SET ";

        if($email !== ''){
            $_set_fields[] = "email='".mysql_real_escape_string($email)."'";
        }
        if($customer_name !== ''){
            $_set_fields[] = "customer_name='".mysql_real_escape_string($customer_name)."'";
        }

        if($country !== ''){
            if(preg_match('/[a-zA-Z]{2}/',$country)!== false){
                $_set_fields[] = "country_code='".$country."'";
            }
        }

        $sql .= implode(', ',$_set_fields);

        $sql .= " WHERE session_id='$ses_id'";
    	return db_query($sql);
    }
}

?>