<?php

class Review_Terms
{

    public static function insert($info)
    {
          return db_insert('review_terms', $info);
		 }

 public static function update($id, $info)
		
    {
    		$safedescription=mysql_real_escape_string($info['description'] );
				$info['description']=$safedescription;
							
        return db_update('review_terms', $id, $info );
    }		 

  public static function associate2Cats($term_id,$cat_ids,$delete_old_ones='Y')
	{
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";

		if (trim($cat_ids_str) == '') {
			db_delete('review_terms_cat',$term_id,'term_id');
			return true;
		}

		// delete old ones

                //if($delete_old_ones == "Y")
                //{
                    db_query("DELETE FROM review_terms_cat WHERE term_id = $term_id AND category_id NOT IN ($cat_ids_str)");
                //}

		

		// insert new ones

		$info = Array('term_id'=>$term_id);

		foreach ($cat_ids as $cat_id) {

		    if( $cat_id <= 0 )
			continue;

			$info['category_id'] = $cat_id;
			db_insert('review_terms_cat',$info,'',true);
		}

		return true;
	}
	
	 public static function associate2Review($term_id,$review_ids,$delete_old_ones='Y')
	{
		if (!is_array($review_ids)) {
			$review_ids = Array($review_ids);
		}

		$review_ids_str = "'" . implode("','",$review_ids) . "'";

		if (trim($review_ids_str) == '') {
			db_delete('review_terms_used',$term_id,'term_id');
			return true;
		}

		// delete old ones

                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM review_terms_used WHERE term_id = $term_id AND review_id NOT IN ($review_ids_str)");
                }

		// insert new ones

		foreach ($review_ids as $re) {
	    if( $re <= 0 )
			     continue;
			$info = Array('term_id'=>$term_id,'review_id'=>$re);
			db_insert('review_terms_used',$info,'',true);
		}

		return true;
	}
	
	
	public static function get($term_id,$type,$approved,$cat_id,$review_id)
	{
	
							
	    $sql = "SELECT DISTINCT review_terms.id * AS terms, review_terms_cat.category_id AS category_id, review_terms_cat.term_id AS term
              FROM review_terms, review_terms_cat
              WHERE review_terms.id = review_terms_cat.term_id
							
       ";

        if ($term_id > 0)
        {
            $sql .= " AND review_terms.id = $term_id";
        }

        if ($type > 0)
        {
            $sql .= " AND review_terms.type = $type";
        }

        if ($approved > 0)
        {
            $sql .= " AND review_terms.approved = $approved";
        }
				 if ($cat_id > 0)
        {
            $sql .= " AND review_terms_cat.category_id = $cat_id";
        }

        $sql.= " GROUP BY review_terms.id";
				
       return db_query_array($sql);
	}
	
	static function getTermsCats($term_id)
	{
		if( !term_id )
			return false;
		return db_query_array("SELECT category_id FROM review_terms_cat WHERE term_id = $term_id",'category_id');
	}	
	
		static function getTermsUsed($term_id)
	{
		if( !term_id )
			return false;
		return db_query_array("SELECT review_id FROM review_terms_used WHERE term_id = $term_id",'review_id');
	}	

	    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }
		
		public static function getProductTerms ($product_id,$type)
		{
		$cats=Cats::getCatsForProduct($product_id);
		$i=0;
		foreach ($cats as $cat){
		   $term=self::get("","$type",'Y',$cat[id],"");
			 if ($term){$result[$i]=$term;}
		   $i++;
		   }
		return $result;	 
		}

}
?>