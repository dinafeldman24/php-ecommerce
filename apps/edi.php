<?php 
class EDIOperations
{
	static function connectToSharedMysqlServer($db_data)
	{
		$db_handle = mysql_connect($db_data['hostname'],$db_data['username'], $db_data['password']) OR die (mysql_error());
		mysql_select_db($db_data['dbname']);

//		$q = mysql_query('DESC	 detl850');
		//echo "<br/></br>";
		//while($row = mysql_fetch_array($q)) {
//    		echo "{$row['Field']} - {$row['Type']}\n";
		//}
	}
	static function insert850HeaderData($header_info)
	{
		db_insert('head850', $header_info);
	}
	static function insert850LinesData($line_info)
	{
		foreach($line_info as $one_line)
		{
			db_insert('detl850', $one_line);
		}
	}
	static function getIDForHeaderUniqueKey($po_id)
	{
		global $CFG;
		db_insert('purchase_order_edi_sendings', array('po_id'=>$po_id));
		
		$unique_key_header = $po_id . "-" . db_insert_id() . "-" . $CFG->site_abbrev;
		return $unique_key_header;
	}
	static function getIDForItemUniqueKey($item_id)
	{
		global $CFG;
		db_insert('purchase_order_item_edi_sendings', array('item_id'=>$item_id));
		$unique_key_item = $item_id . "-" . db_insert_id(). "-" . $CFG->site_abbrev;
		return $unique_key_item;
	}
	static function generateDataForWinco850($po_id, $po_type='N')
	{
		global $CFG;
		$invoices = Invoices::get('','','','','','','po');
		$invoice        = $invoices[0];
		$po = PurchaseOrder::get1($po_id);
		
		if (!$po) 
		{
			echo "Can't get PO data";
			return; 
		}		
		//echo "<pre>";
		//print_r($po);
		//echo "</pre>";
		$unique_key_header = EDIOperations::getIDForHeaderUniqueKey($po_id);
		//echo $unique_key_header;
		$orderitems     = Orders::getItems('','','','','','','','','','','','',$po_id);
		
		if (!$orderitems) 
		{
			echo "Can't get PO items";
			return; 
		}	
		
		if($po['cust_bill_box']) $bill_to_address_whole = $po['cust_bill_box']; 
		else $bill_to_address_whole = $invoice['bill_to_address'];
		
//		echo "<pre>";
//		print_r($orderitems);
//		echo "</pre>";
		
	    if ($orderitems)
	    {
	        $order_items_850_array = array();

	    	foreach ($orderitems as $x => $oi)
	    	{
	    		$product              = Products::get1($oi['product_id']);
	    	    $product_sku          = $oi['product_vendor_sku'] ? $oi['product_vendor_sku'] : $product['vendor_sku'];

	    		// Combine Items
				if(isset($orderitems2[$product_sku])){
					$orderitems2[$product_sku]['item_qty'] += $oi['item_qty'];
				} else {
					$orderitems2[$product_sku] = $oi;					
				}
	    	}

	    	$freight_items = false;

	    	$orderitems = $orderitems2;
	    	$orderitems3 = $orderitems2;
	    	
	    	$counter = 0;
	    	foreach ($orderitems as $sku => $oi)
	    	{
				$counter++;
                $additional_cost      = 0;
	    		$item_id              = $oi['id'];
	    	    $product              = Products::get1($oi['product_id']);
                $product_sku          = PurchaseOrder::getSKU($oi);
	    	    $product_name         = $product['name'];
               if(isset($oi['options'])){
                   $product_name .= ' ' .ProductOptionCache::getDescriptionForOptions($oi['options'], $oi['product_id']);
                   $additional_cost = ProductAvailableOptions::getAdditionalCost($oi['options'][0]['optional_options']);
                    $additional_cost += ProductOptionCache::getCostForPO($oi['product_id'], $oi['options'][0]['product_option_id']);
                }    
 
                $product_unit_cost    = ( ( $oi['cust_each'] >= 0.0  && $oi['cust_each'] != '-1' ) ? $oi['cust_each'] : (($oi['option_vendor_price'] && $oi['option_vendor_price'] > 0 && $oi['option_vendor_price'] != "0.00") ? $oi['option_vendor_price'] : $oi['vendor_price']));
               $product_unit_cost    += $additional_cost;
	    	    $quantity             = $oi['item_qty'];
	    	    $price                = $product_unit_cost;
	    	    $item_total           = $price * $quantity;
				$grand_total         += ($quantity * $product_unit_cost);

				$order_ids[$oi['order_id']] = $oi['order_id'];
				$this_order_id = $oi['order_id'];
				
				$unique_key_item = EDIOperations::getIDForItemUniqueKey($item_id);
				
				$order_items_850_array[] = array(
					'UNIQUEKEY'=>$unique_key_header,
					'UNIQUEITEM'=>$unique_key_item,
					'POLINE'=>$counter,
					'VENDORNUM'=>$product_sku,
					'QUANTITY'=>$quantity,
					'UNITPRICE'=>number_format($product_unit_cost,2, '.',''),
					'UNITMEAS'=>"EA",
					'ITEMDESC'=>substr($product_name, 0, 80),
					'PROCESSED'=>'S',									
					'RECVID'=>$CFG->edi_RECVID,
					'SENDID'=>$CFG->edi_SENDID,
					'PARTNER'=>$CFG->edi_PARTNER
																
				);
					
//							<td class='line_total price'>$<input type='text' value='".number_format($item_total, 2, '.','')."' name='line_total[$item_id]' readonly='readonly' /></td>
				
				$ship_addr = $oi['shipping_address1'] . " " .
							 $oi['shipping_address2'] . " " .
							 $oi['shipping_city'] . " " .
							 $oi['shipping_state'] . " " .
							 $oi['shipping_zip'];
				if(trim($ship_addr) == ""){
					$ship_addr = $invoice['bill_to_address'];
				}
				$ship_address_combo[$ship_addr] = $ship_addr;
	    	}
	    	
	    	//check if items on this PO require freight shipping
	    	if($oi['shipping_type'] == 'Freight'){
	    		$freight_items = true;
	    	}
	    }

	    if(count($order_ids) > 1){
			$multi_orders = true;
	    }
		if( $order_ids ){
	    	$order_id1 = array_pop($order_ids);
	    	$order1 = Orders::get1($order_id1);
		}
			    
	    if($po['cust_ship_via'] && strtolower($po['cust_ship_via']) == "pickup"){			
			$routing_code = "PU"; // pickup
			$transport = 'H';

		} else if($multi_orders){

	    	$routing_code = "PU"; // pickup
	    	$transport = 'H';

	    } else if( $order1 ){
			if (strpos($order1['shipping_method'], '3 Day Select')!==false) $routing_code = "UPS3-3";    	
			else if (strpos($order1['shipping_method'], '2nd Day Air')!==false) $routing_code = "UPS3-2";
			else if (strpos($order1['shipping_method'], 'Next Day Air')!==false) $routing_code = "UPS3-1";
	    	else if($freight_items || strpos($order1['shipping_method'], 'Freight')!==false || strpos($order1['shipping_method_2'], 'Freight')!==false)
	    	{ 
	    		$routing_code = "Ups3-G";
	    		$transport = 'M';
	    	} 
	    	elseif (strpos($order1['shipping_method'], "Ground") !== false || $order1['shipping_method_code'] == "GND")
	    	{ 
	    		$routing_code = "Ups3-G";
	    		$transport = 'M';
	    	}
	    	else // default send 3rd party ups ground
	    	{ 
	    		$routing_code = "Ups3-G";
	    		$transport = 'M';
	    	}
		} else {
			$routing_code = "BW";
			$transport = 'H';
		}

		/* Shipping */
		$to_us = false;
		if($po['cust_ship_box'] && (trim($po['cust_ship_box']) == $invoice['bill_to_address'] || $po['cust_ship_box'] == "pick up")) $to_us = true;
		else if(count($ship_address_combo) > 1){
			// Different Ship addresses detected
			$to_us = true;
		} else  if (!$this_order_id) {
			$to_us = true;
		}
		else {
			$to_us = false;
			// Only 1 shipping address
			
			foreach($orderitems3 as $first_item){ break; }
			$ship_to_name = $first_item['shipping_first_name'] . " " . $first_item['shipping_last_name'];
			$ship_to_company = ( $first_item['shipping_company'] ? ($first_item['shipping_company'] . "\r\n") : '');
			$ship_to_address1 = $first_item['shipping_address1'];
			$ship_to_address2 = ( $first_item['shipping_address2'] ? ($first_item['shipping_address2'] . "\r\n") : '');
			$ship_to_city =  $first_item['shipping_city'];
			$ship_to_state = $first_item['shipping_state'];
			$ship_to_zip = $first_item['shipping_zip'];
			$ship_to_phone = $first_item['shipping_phone'] ? $first_item['shipping_phone'] : '' ;
		}
				
		if ($to_us)
		{
			$ship_to_name = $CFG->purchasing_company_name;
			$ship_to_company = '';
			$ship_to_address1 = $CFG->purchasing_company_address1;
			$ship_to_address2 = '';
			$ship_to_city = $CFG->purchasing_company_city;
			$ship_to_state = $CFG->purchasing_company_state;
			$ship_to_zip = $CFG->purchasing_company_zip;
			$ship_to_phone = $CFG->company_phone;
		}
		$header_data_850 = array(
			'UNIQUEKEY'=>$unique_key_header,	
			'PARTNER'=>$CFG->edi_PARTNER,
			'VENDID'=>$CFG->edi_VENDID,
			'VENDORNAME'=>$CFG->edi_VENDORNAME,		
			'PONUMBER'=>$po['code'] ? $po['code'] : $po['id'],
			'PODATE'=>date('Ymd'),
			'ARRIVDATE'=>date('Ymd'),
			'CUSTORDER'=>$po['code'] ? $po['code'] : $po['id'],
			'ROUTING'=>$routing_code,
			'HEADNOTES'=>$po['notes'],
			'TOTAMOUNT'=>number_format($grand_total,2, ".", ""),
			'BYNAME'=>$CFG->purchasing_company_name,
			'BYADDR'=>$CFG->purchasing_company_address1,
			'BYCITY'=>$CFG->purchasing_company_city,
			'BYSTATE'=>$CFG->purchasing_company_state,
			'BYZIP'=>$CFG->purchasing_company_zip,
			'STCONTACT'=>substr($ship_to_name, 0, 60),			
			'STNAME'=>substr($ship_to_name, 0, 60),
			'STNAME1'=>substr($ship_to_company, 0, 60),
			'STADDR'=>substr($ship_to_address1, 0, 55),
			'STADDR1'=>substr($ship_to_address2, 0, 55),
			'STCITY'=>substr($ship_to_city, 0, 30),
			'STSTATE'=>$ship_to_state,
			'STZIP'=>substr($ship_to_zip, 0, 5),			
			'STPHONE'=>substr($ship_to_phone, 0, 20),
			'HPROCESSED'=>'S',
			'HEADNOTES'=>$po['notes'],
			'POTYPE'=>'N', //Should be N or R or F. N being Orginal, R being Change Order, and F being cancel
			'PAYMETHOD'=> 'TP',// Can be PP (Pre Paid), CC( Collect), PU(Customer Pick Up), or TP (Third Party Pay)
			'TRANSPORT'=>$transport	//Transportation Method Code � Can be M (Motor) or H (Customer Pick up)						
		);

//		echo "This is header.";
//		print_r($header_data_850);
//		echo "These are lines:";
//		print_r($order_items_850_array);
		EDIOperations::connectToSharedMysqlServer($CFG->edi_options_shared_mysql_server_data);
		EDIOperations::insert850HeaderData($header_data_850);
		EDIOperations::insert850LinesData($order_items_850_array);
		
//		return "success";
	}
	static function importNewWinco810Invoices()
	{
		global $CFG;
				
		$inv_due_in_future = EDIOperations::get810InvoicesDueInFuture();
		
		if ($inv_due_in_future)
		{
			foreach ($inv_due_in_future as $one_inv)
			{				
				// first see if that invoice was already in the system
				$can_import_this_invoice = true;
				
				$invoice_data = SupplierInvoices::get(0,'','',$CFG->winco_supplier_id, $one_inv['INVOICENO']);
				if (!$invoice_data)
				{
					echo "Need to import invoice " . $one_inv['INVOICENO'] . "<br/>";

					$invoice_header_data = array(
						'supplier_id'=>$CFG->winco_supplier_id,
						'supplier_inv_num'=>$one_inv['INVOICENO'],
						'due'=>$one_inv['TOTAMOUNT'],						
						'invoice_date'=>$one_inv['INVDATE'],
						'freight'=>$one_inv['TOTFREIGHT'],
						'invoice_notes'=>$one_inv['HEADNOTES'],
						'date_entered'=>date('Y-m-d'),
						//'additional_fees'
						//'discounts'						
					);
					
					$the_po = PurchaseOrder::getByCode($one_inv['PONUMBER']);
					if (!$the_po) echo "Can not find PO corresponding to " . $one_inv['PONUMBER'];
					else 
					{
						$po_id = $the_po['id'];
						
						$invoice_lines = EDIOperations::getLinesByUniqueKey($one_inv['UNIQUEKEY']);
						if ($invoice_lines)
						{
							$invoice_lines_to_insert = array();
							$invoice_total_calculated = 0;
							foreach ($invoice_lines as $one_line)
							{
								$product = Products::get1ByMfrPartNum($one_line['VENDORNUM']);
								if (!$product)
								{
									echo "Can not find product with model number " . $one_line['VENDORNUM'];
									$can_import_this_invoice = false;
								}
								else 
								{
									// look for this product on this PO
									if ($product['is_option']) $po_lines = Orders::getItems(0,0,'','',0,'','',0,0,$product['product_id'],0,0,$the_po['id'], '', '', 0, $product['id']);
									else $po_lines = Orders::getItems(0,0,'','',0,'','',0,0,$product['id'],0,0,$the_po['id']);
									if (count($po_lines) > 1) 
									{
										echo "More than one record for item " . $one_line['VENDORNUM'] . " found<br/>";
										$can_import_this_invoice = false;
									}
									else if ($po_lines) 
									{
										echo "One record for item " . $one_line['VENDORNUM'] . " found<br/>";
										// check qty
										$qty = $po_lines[0]['qty'];
										$invoice_qty = $one_line['QUANTITY'];
										$invoice_total_calculated += ($invoice_qty * $one_line['UNITPRICE']);
										if ($qty != $invoice_qty)
										{
											echo "Quantities do not match<br/>";
											$can_import_this_invoice = false;
										}
										else 
										{
											if ($one_line['UNITPRICE'] != $po_lines[0]['cost_per_piece']) echo "Price discrepancy: Expected price per piece was " . $po_lines[0]['cost_per_piece'] . " and " . $one_line['UNITPRICE'] . " was charged per piece instead<br/>";											 									
											$invoice_lines_to_insert[]['po_item_id'] = $po_lines[0]['the_order_item_id'];
											$invoice_lines_to_insert[]['invoice_price_per_piece'] = $one_line['UNITPRICE'];
											
										}
									}
									else 
									{										
										echo "No records for item " . $one_line['VENDORNUM'] . " found<br/>";
										$can_import_this_invoice = false;
									}
									  	
								}
								
							}
							// do a check on the totals			
							if ($invoice_total_calculated != $one_inv['TOTAMOUNT'])
							{
								echo "Totals do not match.  Winco total: $" . $one_inv['TOTAMOUNT'] . " and calculated total is $" .$invoice_total_calculated;  
								$can_import_this_invoice = false;
							}				
							if($can_import_this_invoice)
							{ 
								//$new_invoice_id = SupplierInvoices::insert($invoice_header_data);
								/*for ($i = 0; $i < count($invoice_lines_to_insert); $i++)
								{
									$invoice_lines_to_insert[$i]['supplier_invoice_id'] = $new_invoice_id;
									db_insert('supplier_invoice_lines',$invoice_lines_to_insert[$i]);
								}*/
								
								echo "Would import this invoice.<br/>";
							}
						}
						else echo "No invoice lines found for invoice " . $one_inv['INVOICENO'] . "<br/>";
					}
				}
				else echo "Invoice " . $one_inv['INVOICENO'] . " was already in the system.<br/>";
				// OTHERWISE, JUST GO ON TO THE NEXT INVOICE :-)
				 
			}
		}
		else echo "no invoices due in future";
		
	}	
	static function get810InvoicesDueInFuture()
	{		
		global $CFG;
		EDIOperations::connectToSharedMysqlServer($CFG->edi_options_shared_mysql_server_data);
		$query = "SELECT * FROM head810 WHERE `DUEDATE` > CURDATE() ORDER BY `DUEDATE` ASC";
		$result = db_query_array($query);
		echo "This is the invoice header:<br/>";
		db_connect($CFG->dbhost, $CFG->dbname, $CFG->dbuser, $CFG->dbpass);
		echo "<pre>";
		print_r($result);
		echo "</pre>";
		return $result;
	}	
	static function getLinesByUniqueKey($unique_key)
	{
		global $CFG;
		EDIOperations::connectToSharedMysqlServer($CFG->edi_options_shared_mysql_server_data);
		$query = "SELECT * FROM detl810 WHERE `UNIQUEKEY` = '" . $unique_key . "'";
		echo "These are the invoice lines:<br/>";
		$result = db_query_array($query);
		echo "<pre>";
		print_r($result);
		echo "</pre>";
		db_connect($CFG->dbhost, $CFG->dbname, $CFG->dbuser, $CFG->dbpass);
		return $result;
	} 
}

?>
