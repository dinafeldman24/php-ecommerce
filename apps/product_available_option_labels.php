<?php

/*****************
*   Class for interface
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   July 22 2015
*
*****************/

class ProductAvailableOptionLabels {
    
    var $id = null;
    var $option_id;
    var $product_id;
    var $label;
    var $error_label;
    var $message_above;
    var $message_below;
    var $is_preview;

    function ProductAvailableOptionLabels($id=null){
        if(!is_null($id)){
            $this->populate($id);
        }
    }

    /*********************
    *
    *
    * DB Functions
    *
    *
    ********************/


    function populate($id){
        $this->id = $id;
        $sql = 'SELECT * FROM product_available_option_labels WHERE id = ' . (int)$id;
        $info = db_query_array($sql);
        if(!is_array($info)){
            return;
        }
        $info = $info[0];
        $this->option_id = $info['option_id'];
        $this->label = $info['label'];
        $this->error_label = $info['error_label'];
        $this->message_above = $info['message_above'];
        $this->message_below = $info['message_below'];
        $this->product_id = $info['product_id'];
        $this->is_preview = $info['is_preview'];
    }

    static function labelExists($label, $product_id){
        $sql = "SELECT COUNT(1) as num_rows  FROM product_available_option_labels 
            WHERE label = '" . mysql_real_escape_string($label) . "' 
            AND product_id = " . (int)$product_id;
        $res = db_query_array($sql);
        $first = $res[0];
        return ($first['num_rows'] > 0);
    }

    static function updateOrInsert($info, $id=null){
        if(!is_null($id) && !empty($id) && $id){
            $orig_label = self::get1($id);
            if($orig_label['optional'] != $info['optional']){
                $sql = "UPDATE product_available_options SET optional = " . (int)$info['optional'] . " WHERE  product_available_option_labels_id = " . (int)$id;
                db_query($sql);
            }
            $res = db_update('product_available_option_labels',$id, $info);

        }
        else{
            if(ProductAvailableOptionLabels::labelExists($info['label'], $info['product_id'])){
               $res = 'That label exists, please choose a different name.';   
            }
            else{
                $res = db_insert('product_available_option_labels', $info);
            }
        }
        return $res;
    }
    
    static function delete($id){
        $row = db_get1(db_query_array("SELECT COUNT(1) as num FROM product_available_options
            WHERE product_available_option_labels_id = $id"));
        if($row['num'] > 0){
            return false;
        }
        return db_delete('product_available_option_labels', $id);
    }
    
    static function getValuesForLabel($id){
        $sql = "SELECT options_values_id, preset_id FROM product_available_options WHERE product_available_option_labels_id = $id";
        $res = db_query_array($sql);
        $values = array();$presets = array();
        foreach($res as $curr){
            if(!empty($curr['options_values_id']) && $curr['options_values_id'] > 0){
                $values[] = $curr['options_values_id'];
            }
            if(!empty($curr['preset_id']) && $curr['preset_id'] > 0){
                $presets[] = $curr['preset_id'];
            }
        }
        return array('values' => $values, 'presets' => $presets);
    }
    static function get($id = 0, $product_id = 0){
 /*       $sql = "select pl.*, @value_name := IFNULL(CONCAT(o.name,':', option_value(ov.options_id, ov.value)), ''), 
        @preset_name := IFNULL(CONCAT(o2.name, ':', presets.name), ''),
        if(@preset_name <> '', @preset_name, @value_name) AS parent_name,
            is_preview, error_label
        FROM product_available_option_labels pl 
            LEFT JOIN product_available_options pao ON pl.parent_id = pao.id 
            LEFT JOIN option_values ov ON pao.options_values_id = ov.id 
            LEFT JOIN options o ON ov.options_id = o.id 
            LEFT JOIN preset_options presets on pao.preset_id = presets.id
            LEFT JOIN options o2 ON presets.option_id = o2.id
*/
        $sql = "select pl.*, @value_name := IFNULL(CONCAT(pl2.label,': ', option_value(ov.options_id, ov.value)), ''),
        @preset_name := IFNULL(CONCAT(pl2.label, ':', presets.name), ''),
        if(@preset_name <> '', @preset_name, @value_name) AS parent_name,
            pl.is_preview, pl.error_label
        FROM product_available_option_labels pl
            LEFT JOIN product_available_options pao ON pl.parent_id = pao.id
            LEFT JOIN product_available_option_labels pl2 on pao.product_available_option_labels_id = pl2.id
            LEFT JOIN option_values ov ON pao.options_values_id = ov.id
            LEFT JOIN options o ON ov.options_id = o.id
            LEFT JOIN preset_options presets on pao.preset_id = presets.id
            LEFT JOIN options o2 ON presets.option_id = o2.id
        WHERE 1 ";
    
     if($id){
            $sql .= " AND pl.id = " . (int)$id ;
        }
        if($product_id){
            $sql .=" AND pl.product_id = " . (int)$product_id;
        }
        return db_query_array($sql);
    }
    
    static function get1($label_id){
        $rows = self::get($label_id);
        if(sizeof($rows)){
            return $rows[0];
        }
        return array();
    }
    
    static function getForOptionForm($product_id){
        return self::getForProduct($product_id);
    }

    static function getForProduct($product_id){
        return self::get(0, $product_id);
    }

    static function getOptionIdForLabel($label_id){
        $ret = null;
        $sql = "SELECT option_id FROM product_available_option_labels WHERE
            id = " . (int)$label_id;
        $rows = db_query_array($sql);
        if(sizeof($rows) == 1){
            $row = $rows[0];
            $ret = $row['option_id'];
        }
        return $ret;
    }

   /************************
   *
   *
   *    Getters and Setters
   *
   *
   ************************/

    function setProductId($product_id){
        $this->product_id = $product_id;
    }

    function getProductId(){
        return $this->product_id;
    }

   function getOptionId(){
        return $this->option_id;
   }

   function setOptionId($option_id){
       $this->option_id = $option_id;
   }



    /************************
    *
    * Display Functions
    *
    ***********************/

    static function getDropdownsForEditForm($product_id){
            $option_labels = self::getForOptionForm($product_id);
            $script = array();
            $opt_str = '';
            foreach($option_labels as $label){
                $opt_str .= '<option value=' . $label['id'] . '>' . $label['label'] . '</option>';
                $script[$label['id']] = $label['option_id'] ;
            }
            return array('options' => $opt_str, 'labels_to_options' => $script, 'options_full_info' => $option_labels);
    }
}
