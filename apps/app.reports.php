<?php

class Reports{


	static function getBestSellingProducts($start_date='',$end_date=''){

		$sql = " SELECT SUM(oi.qty) as num_sold,
				 oi.product_id,
				 SUM(oi.price * oi.qty) as revenue,
				 SUM(products.vendor_price * oi.qty) as total_vendor_price,
				 SUM(oi.price * oi.qty - products.vendor_price * oi.qty) as total_profit,
				 products.name as product_name ";
		$sql .= " FROM order_items oi ";
		$sql .= " LEFT JOIN products on products.id = oi.product_id ";
		$sql .= " LEFT JOIN orders on orders.id = oi.order_id ";
		$sql .= " WHERE 1 ";

		if($start_date){
			$start_date = db_esc($start_date);
			$sql .= " AND orders.date >= '$start_date' ";
		}
		if($end_date){
			$end_date = db_esc($end_date);
			$sql .= " AND orders.date <= '$end_date' ";
		}

		$sql .= " GROUP BY oi.product_id ";
		$sql .= " ORDER BY num_sold DESC ";

		$sql .= " LIMIT 10 ";

		return db_query_array($sql);
	}

	static function getTotalProfits($start_date='',$end_date='',$deliniation_str='%Y-%m',$limit=10,$o = null){

		$use_deliniation = !!($deliniation_str);
		if(!$deliniation_str){
			$deliniation_str = '%Y-%m';
		}
		$period_sql = "DATE_FORMAT(orders.date,'$deliniation_str')";

		$state_sql = " IF(shipping_state = '' OR shipping_state IS NULL, billing_state, shipping_state) ";
		$sql = "
			SELECT
			$period_sql AS period,
			SUM(oi.price * oi.qty) as revenue,
			SUM(products.vendor_price * oi.qty) as cost,
			SUM(oi.price * oi.qty - products.vendor_price * oi.qty) as profits,
			SUM(oi.price * oi.qty * orders.tax_rate/100) as sales_tax,
			$state_sql as state
			FROM order_items oi
			LEFT JOIN orders ON oi.order_id = orders.id
			LEFT JOIN products ON products.id = oi.product_id
			WHERE 1 ";

		if($o->state){
			if($o->state == "NY"){
				$sql .= " AND ( $state_sql LIKE 'NY' OR $state_sql LIKE 'New York' )";
			} else {
				$sql .= " AND $state_sql = '". db_esc($o->state) . "' ";
			}
		}

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);
		}

		if($use_deliniation){
			$sql .= " GROUP BY $period_sql ";
		}

		$sql .= " ORDER BY period DESC ";

		if($limit){
			$sql .= " LIMIT $limit ";
		}

//		echo "*** $sql *** " ;

		return db_query_array($sql);
	}

	static function getProfitReport($start_date='',$end_date=''){

		$sql = "
			SELECT order_id,
			SUM(oi.price * oi.qty) as amount,
			SUM(products.vendor_price * oi.qty) as vendor_cost,
			0.00 as ebay_fee,
			0.00 as amazon_fee,
			0.00 as credit_fee,
			SUM(oi.price * oi.qty - products.vendor_price * oi.qty - 0.00 - 0.00 - 0.00) as total_profit,
			orders.date as posted_date,
			oi.id as item_number,
			oi.qty as quantity
			FROM order_items oi
			LEFT JOIN products on products.id = oi.product_id
			LEFT JOIN orders on orders.id = oi.order_id

			WHERE 1

			ORDER BY total_profit DESC

			";

		return db_query_array($sql);
	}

	static function getOrderTotals($start_date='',$end_date='',$deliniation_str='%Y-%m',$limit=10){
		$use_deliniation = !!($deliniation_str);
		if(!$deliniation_str){
			$deliniation_str = '%Y-%m';
		}
		$period_sql = "DATE_FORMAT(orders.date,'$deliniation_str')";
		$sql = "
			SELECT
			$period_sql AS period,

			COUNT(DISTINCT IF(order_tax_option = 2, amazon_order_id,NULL)) as amazon_count,
			  SUM(IF(order_tax_option = 2, oi.price * oi.qty, 0)) as amazon_total,

			COUNT(DISTINCT IF(order_tax_option = 3, ebay_order_id,NULL)) as ebay_count,
			  SUM(IF(order_tax_option = 3, oi.price * oi.qty, 0)) as ebay_total,

			COUNT(DISTINCT IF(order_tax_option = 1, orders.id,NULL)) as website_count,
			  SUM(IF(order_tax_option = 1, oi.price * oi.qty, 0)) as website_total

			FROM order_items oi
			LEFT JOIN orders ON oi.order_id = orders.id
			LEFT JOIN products ON products.id = oi.product_id
			WHERE 1 ";

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);
		}

		if($use_deliniation){
			$sql .= " GROUP BY $period_sql ";
		}
		$sql .= " ORDER BY period DESC ";

		if($limit){
			$sql .= " LIMIT $limit ";
		}

//		echo "<p>".$sql;

		return db_query_array($sql);
	}

		static function getUpsBillReport($invoice_num='', $start_date='',$end_date='')
		{
			$sql = "SELECT * FROM ups_bill_info WHERE 1";
			if ($invoice_num != '')$sql .= " AND invoice_number = '$invoice_num'";
			if ($start_date != '' && $end_date != '') $sql .= " AND pickup_date BETWEEN '$start_date' and '$end_date'";
			$sql .= " ORDER BY pickup_date ASC, tracking_num ASC, invoice_section DESC";
      		return db_query_array($sql);
		}
		
		static function getFbaInventoryValueData(){

		$sql = " SELECT products.id as product_id,
				sku, products.name as product_name, afi.asin, afn_fulfillable_quantity, 
				(products.vendor_price + products.fba_order_handling_fee + products.fba_pick_and_pack_fee + 
				products.fba_weight_fee + products.fba_storage_fee + products.fba_inbound_shipping_cost) as fba_cost_per_piece,
				((products.vendor_price + products.fba_order_handling_fee + products.fba_pick_and_pack_fee + 
				products.fba_weight_fee + products.fba_storage_fee + 
				products.fba_inbound_shipping_cost) *  afn_fulfillable_quantity) as total_value ";
		$sql .= " FROM amazon_fba_inventory afi";
		$sql .= " LEFT JOIN products on products.vendor_sku = afi.sku ";
		$sql .= " WHERE 1 ";

		$sql .= " AND afn_fulfillable_quantity > 0 AND products.is_deleted = 'N'";

		return db_query_array($sql);
	}
	
	static function getOrdersForMailchimp($id=0,$store="ALL", $sku = "ALL", $admin_id = "ALL", $brand_id = "ALL", $customer_name='', $customer_id=0,  $shipping='ALL'){
		global $CFG;
		
		if ($admin_id == "9999999") $admin_id = 0;
		
		$sql = "
			SELECT
			DISTINCT orders.id, orders.date, orders.customer_id, orders.billing_first_name, orders.billing_last_name, orders.shipping_id, orders.shipping_id_2, store.name as store_name, customers.first_name, customers.last_name,
			IF(
					   (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - (orders.reward_points_used) + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
					   >= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee + 
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - (orders.reward_points_used) + orders.min_charges + orders.shipping - orders.shipping_discount))),2) , 0
				   )

			AS order_total, orders.subtotal, orders.shipping";
			
		$from_clause = " FROM (orders, store";
				
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			
			AND subtotal > 0
			AND orders.current_status_id <> " . $CFG->canceled_order_status .
			" AND orders.current_status_id <> " . $CFG->quote_status .
			" AND orders.current_status_id <> " . $CFG->test_order_status_id .
			" AND orders.current_status_id <> " . $CFG->government_quote_status_id .
			" AND orders.current_status_id <> " . $CFG->awaiting_fraud_screening_status_id;

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$where_clause .= db_queryrange('orders.date',$start_date,$end_date);
		}
		
		if ($id) $where_clause .= " AND orders.id = " . $id;
		    	
		
		if ($store != "ALL") $where_clause .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($admin_id !== "ALL") $where_clause .= " AND orders.admin_id = '".mysql_real_escape_string($admin_id)."'";
        if ($shipping !== "ALL") $where_clause .= " AND (orders.shipping_id = '".mysql_real_escape_string($shipping)."' OR orders.shipping_id_2  = '".mysql_real_escape_string($shipping)."')";		
		
		if ($brand_id != "ALL")
		{
			$from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND products.brand_id = '".mysql_real_escape_string($brand_id)."'";
		} 
		if ($sku != "ALL") 
		{
			if ($brand_id == "ALL") $from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND products.vendor_sku LIKE '".mysql_real_escape_string($sku)."%'";	
		}
		
        if ($customer_name){
			$name_array = explode(" ",$customer_name);
			if (!$name_array[1]) {
				$name_array[1]=$name_array[0];
                $exp=' OR ';
			}
			else $exp=' AND ';
			$where_clause .=" AND ((orders.billing_first_name LIKE '%".$name_array[0]."%'"
							  .$exp."orders.billing_last_name LIKE '%"  .$name_array[1]."%')	
							  OR (orders.billing_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.billing_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "orders.shipping_last_name LIKE '%".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (customers.first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "customers.last_name LIKE '%".$name_array[1]."%'))";
							  
							  
		}		
		if ($customer_id){
			$where_clause .=" AND orders.customer_id=".$customer_id;
			
		}
		/*if ($name_keywords != "ALL") 
		{
			if ($sku == "ALL") 
			{
				$from_clause .= ", order_items, products";
				$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id";
			}
			$sql .= ", products.name ";
			$where_clause .= " AND products.name LIKE '%".mysql_real_escape_string($name_keywords)."%'";	
		}*/
		
		$from_clause .= ") LEFT JOIN customers on orders.customer_id = customers.id";
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
				
		$order_by = " ORDER BY orders.id ASC ";
		$sql = $sql . $from_clause . $where_clause . $order_by;
		//echo $sql;
		//flush(); 
		return db_query_array($sql);
	}
	
	
	static function getOrdersForTimePeriod($start_date='',$end_date='',$store="ALL", $sku = "ALL", $admin_id = "ALL", $brand_id = "ALL", $customer_name='', $customer_id=0,  $shipping='ALL'){
		global $CFG;
		
		if ($admin_id == "9999999") $admin_id = 0;
		
		$sql = "
			SELECT
			DISTINCT orders.id, orders.date, orders.customer_id, orders.billing_first_name, orders.billing_last_name, orders.shipping_id, orders.shipping_id_2, store.name as store_name, customers.first_name, customers.last_name,
			IF (customers.allow_ship_before_payment = 'Y','Y','N') as terms_cust,
			IF(
					   (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - (orders.reward_points_used) + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
					   >= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee + 
					   (0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - (orders.reward_points_used) + orders.min_charges + orders.shipping - orders.shipping_discount))),2) , 0
				   )

			AS order_total, orders.subtotal, orders.shipping";
			
		$from_clause = " FROM (orders, store";
				
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			
			AND subtotal > 0
			AND orders.current_status_id <> " . $CFG->canceled_order_status .
			" AND orders.current_status_id <> " . $CFG->quote_status .
			" AND orders.current_status_id <> " . $CFG->test_order_status_id .
			" AND orders.current_status_id <> " . $CFG->government_quote_status_id .
			" AND orders.current_status_id <> " . $CFG->awaiting_fraud_screening_status_id;

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$where_clause .= db_queryrange('orders.date',$start_date,$end_date);
		}
		
		if ($store != "ALL") $where_clause .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($admin_id !== "ALL") $where_clause .= " AND orders.admin_id = '".mysql_real_escape_string($admin_id)."'";
        if ($shipping !== "ALL") $where_clause .= " AND (orders.shipping_id = '".mysql_real_escape_string($shipping)."' OR orders.shipping_id_2  = '".mysql_real_escape_string($shipping)."')";		
		
		if ($brand_id != "ALL")
		{
			$from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND products.brand_id = '".mysql_real_escape_string($brand_id)."'";
		} 
		if ($sku != "ALL") 
		{
			if ($brand_id == "ALL") $from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND products.vendor_sku LIKE '".mysql_real_escape_string($sku)."%'";	
		}
		
        if ($customer_name){
			$name_array = explode(" ",$customer_name);
			if (!$name_array[1]) {
				$name_array[1]=$name_array[0];
                $exp=' OR ';
			}
			else $exp=' AND ';
			$where_clause .=" AND ((orders.billing_first_name LIKE '%".$name_array[0]."%'"
							  .$exp."orders.billing_last_name LIKE '%"  .$name_array[1]."%')	
							  OR (orders.billing_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.billing_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "orders.shipping_last_name LIKE '%".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (customers.first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "customers.last_name LIKE '%".$name_array[1]."%'))";
							  
							  
		}		
		if ($customer_id){
			$where_clause .=" AND orders.customer_id=".$customer_id;
			
		}
		/*if ($name_keywords != "ALL") 
		{
			if ($sku == "ALL") 
			{
				$from_clause .= ", order_items, products";
				$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id";
			}
			$sql .= ", products.name ";
			$where_clause .= " AND products.name LIKE '%".mysql_real_escape_string($name_keywords)."%'";	
		}*/
		
		$from_clause .= ") LEFT JOIN customers on orders.customer_id = customers.id";
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
				
		$order_by = " ORDER BY orders.id ASC ";
		$sql = $sql . $from_clause . $where_clause . $order_by;
		//echo $sql;
		//flush(); 
		return db_query_array($sql);
	}
	
	static function getOrdersShippedButNoTracking ($start_date='',$end_date='',$store="ALL", $statuses="ALL" ) {
		global $CFG;
		$sql = "
			SELECT o.id, o.date, s.name as store_name, sm.name AS shipper, os.name as status
FROM orders o
LEFT JOIN order_tracking ot ON o.id = ot.order_id, store s, shipping_methods sm, order_statuses os
WHERE 1 
AND ot.id IS NULL
AND o.current_status_id = os.id
AND o.order_tax_option = s.id
AND o.order_tax_option != ".$CFG->amazon_fba_store_id. /* exclude FBA Amazon */
" AND  o.shipping_id=sm.id
			";

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('o.date',$start_date,$end_date);
		}
		
		if ($store != "ALL") $sql .= " AND o.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($statuses != "ALL") $sql .= " AND o.current_status_id IN (".mysql_real_escape_string($statuses).") ";
		if ($CFG->site_name == "lionsdeal") $sql .= " AND o.id NOT BETWEEN 180244 AND 180442";				
		$sql .= " ORDER BY o.id DESC ";
		
		//echo "sql = $sql";

		return db_query_array($sql);
	}
	
	static function getSkusOrderedInTimePeriod($start_date='',$end_date='',$store="ALL",$brand="ALL", $name_keywords="ALL",$sortby='',$sku="ALL",$group_skus=false){
		//$group_skus = true;
		global $CFG;
		
		$sql = "
			SELECT
			products.vendor_sku AS the_sku, product_options.vendor_sku AS option_vendor_sku, products.name AS product_name, 
			sum(qty) AS total_sold, 
			count(distinct orders.id) as num_orders,
			sum(qty * order_items.price) AS custs_paid, 
			sum(qty * products.vendor_price) AS we_paid
												
			FROM (orders, order_items, products, store)
			LEFT JOIN order_item_options on order_items.id = order_item_options.order_item_id
			LEFT JOIN product_options on order_item_options.product_option_id = product_options.id
			WHERE 1 
			AND orders.order_tax_option = store.id
			AND order_items.order_id = orders.id
			AND order_items.product_id = products.id
			AND subtotal > 0
			AND orders.current_status_id NOT IN $CFG->status_to_exclude_from_sales_reports_string";

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);
		}
		
		if ($store != "ALL") $sql .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($brand != "ALL") $sql .= " AND products.brand_id = '".mysql_real_escape_string($brand)."'";
		if ($sku != "ALL") $sql .= " AND products.vendor_sku = '".mysql_real_escape_string($sku)."'";
		if ($name_keywords != "ALL") $sql .= " AND products.name LIKE '%".mysql_real_escape_string($name_keywords)."%'";

		if ($sortby == "") $sortby = "products.vendor_sku";
		$sql .= " GROUP by order_items.product_id, order_item_options.product_option_id ORDER BY $sortby ASC";
		
		//echo $sql;
		
		if($group_skus){
			$sql = "
					SELECT IF(pk IS NOT NULL, concat(original_sku,'@',pk), original_sku) as the_sku,option_vendor_sku,product_name,total_sold,num_orders,custs_paid,we_paid
					FROM (
					SELECT 
							report_products.the_sku,
							SUBSTRING_INDEX( SUBSTRING_INDEX( report_products.the_sku, 'FBA', 1 ), '@', 1 ) AS original_sku, 
							option_vendor_sku,
							SUBSTRING_INDEX( SUBSTRING_INDEX( option_vendor_sku, 'FBA', 1 ), '@', 1 ) AS original_product_option_sku,
							product_name,
							SUM( total_sold ) AS total_sold,
							SUM( num_orders ) AS num_orders,
							SUM( custs_paid ) AS custs_paid,
							SUM( we_paid ) AS we_paid,
							IF( 
								report_products.the_sku REGEXP '@[[:digit:]]+PK', 
								IF( 
									SUBSTRING_INDEX( report_products.the_sku, '@', -1 ) REGEXP '[[:digit:]]+PK', 
									SUBSTRING_INDEX( SUBSTRING_INDEX( report_products.the_sku, 'FBA', 1 ) , '@', -1) , 
									SUBSTRING( report_products.the_sku, LOCATE( '@', report_products.the_sku ) +1, LOCATE( 'PK', report_products.the_sku ) +1 - LOCATE( '@', report_products.the_sku ) ) 
									), 
								NULL 
								) AS pk	
							FROM ($sql) as report_products
							GROUP BY original_sku , original_product_option_sku, pk
			) as inner_q";
			
		}
		
		//echo "<br>".$sql;
		
		return db_query_array($sql);
	}
	
	static function getOrdersNotCharged($start_date='',$end_date='', $status = '')
	{

		$sql = "
			SELECT a.date, a.order_id, order_statuses.name,order_statuses.id as order_status_id
			FROM (
			`order_charges` a, orders, order_statuses
			)
			WHERE a.order_id = orders.id
			AND order_statuses.id = orders.current_status_id			
			AND a.action = 'authorize'			
			AND a.order_id NOT
			IN (

				SELECT order_id
				FROM order_charges b
				WHERE b.action = 'capture'
				AND b.transaction_id <> ''
			)
			AND a.order_id NOT
			IN (

				SELECT order_id
				FROM order_charges b
				WHERE b.action = 'void'
			)
			AND a.order_id NOT
			IN (

				SELECT order_id
				FROM order_charges b
				WHERE b.action = 'sale'
			)";

			if($start_date || $end_date){
				if($start_date){
					$start_date .= " 00:00:00";
				}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			
			$sql .= db_queryrange('orders.date',$start_date,$end_date);											
		}
		if ($status != '') $sql .= " AND order_statuses.id = " . $status;
		$sql .= " ORDER BY a.order_id DESC";
		return db_query_array($sql);
	}

	static function getGoogleCheckoutTransactions($start_date='',$end_date='')
	{
			$sql = "SELECT * FROM order_charges WHERE notes = 'Google Checkout Payment' ";
			if($start_date || $end_date){
				if($start_date){
					$start_date .= " 00:00:00";
				}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			
			$sql .= db_queryrange('order_charges.date',$start_date,$end_date);
		
			}
			return db_query_array($sql);
	}
	
	static function getOrderTotalsForMonthlyComparisonReport ($year, $store, $sku, $brand)
	{
		global $CFG;
		
		$sql = "SELECT DATE_FORMAT(orders.date,'%M') AS	 Month, 
					COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total "; 
				
		$from_clause = " FROM (orders, store)";
		
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			AND subtotal > 0
			AND orders.current_status_id <> " . $CFG->canceled_order_status .
			" AND orders.current_status_id <> " . $CFG->quote_status .
			" AND orders.date LIKE '$year%'";
		if ($store != "ALL") $where_clause .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($sku != "ALL") 
		{
			$from_clause .= " LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products ON order_items.product_id = products.id";
			$where_clause .= " AND products.vendor_sku = '".mysql_real_escape_string($sku)."'";	
		}
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		if ($brand != "ALL") 
		{
			if ($sku == "ALL")
			{	
				 $from_clause .= " LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products ON order_items.product_id = products.id";
			} 
			$where_clause .= " AND products.brand_id = '".mysql_real_escape_string($brand)."'";			
		}
		$group_and_order_by_clause = "GROUP BY DATE_FORMAT(orders.date,'%M') ORDER BY DATE_FORMAT(orders.date,'%m') ASC";
		//echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);
	}
	static function getOrderTotalsForComparisonReportByDay ($start_date, $end_date, $statuses_to_exclude)
	{
		global $CFG;
		
		$sql = "SELECT DATE_FORMAT(orders.date,'%Y-%m-%d') AS Date, 
					COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total "; 
				
		$from_clause = " FROM orders, store";
		
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'";
		
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY DATE_FORMAT(orders.date,'%Y-%m-%d') ORDER BY DATE_FORMAT(orders.date,'%Y-%m-%d') ASC";
	//	echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);
	}
	static function getOrderTotalsForComparisonReportByYear ($start_date, $end_date, $statuses_to_exclude)
	{
		global $CFG;
		
		$sql = "SELECT DATE_FORMAT(orders.date,'%Y') AS Date, 
					COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total "; 
				
		$from_clause = " FROM orders, store";
		
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'";
		
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY DATE_FORMAT(orders.date,'%Y') ORDER BY DATE_FORMAT(orders.date,'%Y') ASC";
		echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);
	}
	static function getOrderTotalsForComparisonReportByMonth ($start_date, $end_date, $statuses_to_exclude)
	{
		global $CFG;
		
		$sql = "SELECT DATE_FORMAT(orders.date,'%M %Y') AS Date, 
					COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total "; 
				
		$from_clause = " FROM orders, store";
		
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'";
		
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY DATE_FORMAT(orders.date,'%Y-%m') ORDER BY DATE_FORMAT(orders.date,'%Y-%m') ASC";
//		echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);
	}
	static function getOrderTotalsForComparisonReportByWeek ($start_date, $end_date, $statuses_to_exclude)
	{
		global $CFG;
		
		$sql = "SELECT str_to_date(concat(yearweek(orders.date), ' sunday'), '%X%V %W') AS Date, 
					COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total "; 
				
		$from_clause = " FROM orders, store";
		
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id			
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'";
		
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY WEEK(orders.date) ORDER BY WEEK(orders.date) ASC";
//		echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);
	}
	static function getDataForProductMarkupManagement($sku = 'ALL', $brand = 'ALL', $order_by = '', $category = 'ALL', $name_keywords = 'ALL', $limit = '', $start=0)
	{
		$sql = "SELECT products.id, products.vendor_sku, products.name AS product_name, brands.name AS brand_name, 
			vendor_price AS cost, price AS our_sell_price, retail_price AS list_price, 
			cats.name AS category, 
			CONCAT( ROUND( ( ( price / vendor_price) -1 ) *100, 2 ) , '%') AS markup, 
			ROUND( ( ( price / vendor_price) -1 ) *100, 2 ) AS markup_num,
			CONCAT( ROUND( ( ( price - vendor_price) / price ) *100 ) , '%') AS net_profit,
			ROUND( ( ( price - vendor_price) / price ) *100 ) AS net_profit_num
			FROM products, brands, cats, product_cats
			WHERE products.brand_id = brands.id
			AND product_cats.product_id = products.id
			AND product_cats.cat_id = cats.id
			AND cats.pid =0
			AND products.is_active = 'Y'";
			//AND cats.is_hidden = 'N'";
		if ($sku != 'ALL') $sql .= " AND products.vendor_sku LIKE '%$sku%'";
		if ($brand != 'ALL') $sql .= " AND products.brand_id = $brand";
		if ($category != 'ALL') $sql .= " AND cats.id = $category";		
		if ($name_keywords != "ALL") $sql .= " AND products.name LIKE '%".mysql_real_escape_string($name_keywords)."%'";		
		
		if ($order_by != '') $sql .= " ORDER BY $order_by";
		else $sql .= " ORDER BY products.id ASC";

		if ($limit) $sql .= " LIMIT $start, $limit";
		//echo $sql;
		$result = db_query_array($sql);
		return $result;
	}
	
	static function getOrderTotalsBySalesperson ($start_date, $end_date, $statuses_to_exclude)
	{
		global $CFG;
		
		$sql = "SELECT COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total, concat(admin_users.first_name, ' ', admin_users.last_name) AS salesperson, orders.admin_id"; 
				
		$from_clause = " FROM orders, admin_users";
		
		$where_clause = " WHERE 1 		
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'
			AND admin_users.id = orders.admin_id
			AND orders.order_tax_option = " . $CFG->website_store_id . " AND orders.admin_id > 0";
		
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY admin_id ORDER BY admin_users.last_name ASC";
//		echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);	
	
	}
	static function getRepeatCustomerData ($start_date, $end_date, $statuses_to_exclude, $placed_more_than='', $spent_more_than = '')
	{
		global $CFG;
		
		$sql = "SELECT COUNT(*) AS num_orders, 
					SUM(IF(
   						(orders.subtotal + orders.min_charges - orders.promo_discount - orders.reward_points_used - orders.shipping_discount
						+ orders.shipping + orders.liftgate_fee +
   						(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - orders.reward_points_used + orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) )
   						>= 0, round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee + 
   						(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - orders.shipping_discount
							+ orders.shipping + orders.liftgate_fee))),2) , 0
				   		)
					)
				AS order_total, if(orders.billing_company <> '', CONCAT(orders.billing_first_name, ' ', orders.billing_last_name, '-', orders.billing_company), CONCAT(orders.billing_first_name, ' ', orders.billing_last_name)) AS customer_name, 
				MAX(orders.id) AS most_recent_order, MAX(orders.date) AS most_recent_order_date"; 
				
		$from_clause = " FROM orders";
		
		$where_clause = " WHERE 1 		
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude
			AND orders.date BETWEEN '$start_date' AND '$end_date'
			AND orders.order_tax_option = " . $CFG->website_store_id;
			
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		
		$group_and_order_by_clause = "GROUP BY orders.email"; 
		if ($placed_more_than != '') $group_and_order_by_clause .= " HAVING num_orders > $placed_more_than";
		if ($spent_more_than != '') 
		{
			if ($placed_more_than != '') $group_and_order_by_clause .= " AND";
			else $group_and_order_by_clause .= " HAVING";
			$group_and_order_by_clause .= " order_total > $spent_more_than";	
		}
		$group_and_order_by_clause .= " ORDER BY orders.billing_last_name ASC";
//		echo $sql . $from_clause . $where_clause .$group_and_order_by_clause;
		return db_query_array($sql . $from_clause . $where_clause .$group_and_order_by_clause);	
	
	}
	static function getSupplierSummaryDataForTimePeriod($start_date='',$end_date='',$statuses_to_exclude, $store="ALL",$brand="ALL"){

		$sql = "
			SELECT
			brands.name as brand_name, 
			sum(qty) AS total_sold, 
			count(distinct orders.id) as num_orders,
			sum(qty * order_items.price) AS sales, 
			sum(qty * products.vendor_price) AS cost
												
			FROM (orders, order_items, products, store, brands)
			WHERE 1 
			AND orders.order_tax_option = store.id
			AND order_items.order_id = orders.id
			AND order_items.product_id = products.id
			AND products.brand_id = brands.id
			AND subtotal > 0
			AND orders.current_status_id NOT IN $statuses_to_exclude";

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);
		}
		
		if ($store != "ALL") $sql .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($brand != "ALL") $sql .= " AND products.brand_id = '".mysql_real_escape_string($brand)."'";
		
		if ($sortby == "") $sortby = "brands.name";
		//$sql .= " GROUP by order_items.product_id, order_item_options.product_option_id ORDER BY $sortby ASC";
		$sql .= " ORDER BY $sortby ASC";
		//echo $sql;
		return db_query_array($sql);
	}
	static function getOrdersWithBalanceDue($start_date='', $end_date='',$statuses_to_exclude='', $type="positive",$order_tag='')
	{
		global $CFG;
		// prev had IF(action = 'credit', -1 * order_charges.amount,
		$sql = "SELECT orders.id as the_order_id, DATE_FORMAT(orders.date,'%Y-%m-%d') AS Date, concat(orders.billing_last_name, ' ', orders.billing_first_name) as bill_to_name,billing_company,
				IF( (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - 
				orders.shipping_discount + orders.shipping + orders.liftgate_fee + 
				(0.01 * orders.tax_rate * (orders.subtotal - orders.promo_discount - (orders.reward_points_used) 
				+ orders.min_charges + orders.shipping + orders.liftgate_fee - orders.shipping_discount) ) ) >= 0, 
				round((orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used) - 
				orders.shipping_discount + orders.shipping + orders.liftgate_fee + 
				(0.01 * orders.tax_rate * (orders.subtotal + orders.min_charges - orders.promo_discount - (orders.reward_points_used)
				 - orders.shipping_discount + orders.shipping + orders.liftgate_fee))),2) , 0 ) AS order_total, 
				 SUM(IF(action = 'capture', order_charges.amount, IF(action = 'sale', order_charges.amount,0))) AS charged_sum,
				 IF (date_sub(NOW(), INTERVAL 30 DAY) < orders.date, '1 - 30', IF(date_sub(NOW(), INTERVAL 60 DAY) < orders.date, '31 - 60',
				 IF(date_sub(NOW(), INTERVAL 90 DAY) < orders.date, '61 - 90', 'Over 90'))) AS days_overdue,
				 admin_users.username AS salesperson,order_statuses.name as order_status_name
				 FROM orders 
				 LEFT JOIN order_charges ON orders.id = order_charges.order_id 
				 LEFT JOIN admin_users ON orders.admin_id = admin_users.id 
				 LEFT JOIN order_statuses ON orders.current_status_id = order_statuses.id
				 WHERE orders.current_status_id NOT IN $statuses_to_exclude AND orders.current_status_id <> ".$CFG->returned_order_status. " AND orders.date > '2014-11-01 00:00:00'
				 AND orders.admin_id > 0";
			if($start_date || $end_date){
				if($start_date){
					$start_date .= " 00:00:00";
				}
				if($end_date){
					$end_date .= " 23:59:59";
				}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);
			} 
			if($order_tag != '')$sql .= " AND orders.order_tags LIKE '%,"  . $order_tag . ",%'";
        
			$sql .=	" AND orders.order_tax_option = " . $CFG->website_store_id . " GROUP BY orders.id  ";
			if ($type == "positive") $sql .= " HAVING (order_total > charged_sum) ORDER BY billing_last_name";
			else $sql .= " HAVING (order_total < charged_sum) ORDER BY billing_last_name";
			
			return db_query_array($sql);
	}
	
	static function getShippedFromWhsInfo($start_date='', $end_date='', $store="ALL")
	{
		global $CFG;
		$sql = "SELECT orders.id as the_order_id, DATE_FORMAT(orders.date,'%Y-%m-%d') AS order_date, order_tracking.ship_date 
				 FROM orders, order_tracking 				  
				 WHERE orders.id = order_tracking.order_id
				 AND ((order_tracking.shipper_id = 5 AND order_tracking.ship_from_id = 1) OR (order_tracking.shipper_id = 1 and order_tracking.ship_from_id = 6 AND order_tracking.tracking_number like '1ZY0970W%'))";
				 
			if($start_date || $end_date){
				if($start_date){
					$start_date .= " 00:00:00";
				}
				if($end_date){
					$end_date .= " 23:59:59";
				}
			$sql .= db_queryrange('orders.date',$start_date,$end_date);			
			} 
			
			if ($store != "ALL") $sql .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";  			
//			echo $sql;
			return db_query_array($sql);
	}
static function getTaxReport($start_date='',$end_date='',$total=false,$date_type='payment_date',$details_or_summary="summary"){

	global $CFG;
	if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
		}
	if ($details_or_summary == "details")
	{
		if ($date_type == "payment_date")
		{
			$sql1 = "CREATE TEMPORARY TABLE orders_for_tax_stats SELECT oi.id, oi.billing_company, oi.billing_first_name, oi.billing_last_name, oi.shipping_company, oi.shipping_first_name, oi.shipping_last_name, shipping_state AS state, oi.tax_rate as tax_rate, LEFT(shipping_zip, 5) AS zip,
 store.name as store_name FROM (orders oi, order_charges oc) LEFT JOIN store on oi.order_tax_option = store.id WHERE oc.order_id = oi.id"; 
 //if ($start_date >= $CFG->amazon_collects_all_ny_tax_start )$sql1 .= " AND oi.order_tax_option <> " .$CFG->amazon_fba_store_id ." AND oi.order_tax_option <> " .$CFG->amazon_store_id;
		
		
			$sql1 .= db_queryrange('oc.date',$start_date,$end_date);
			$sql1 .= " GROUP BY oi.id";
			
			db_query($sql1);
 
 			$sql2 = "ALTER TABLE `orders_for_tax_stats` ADD `county` VARCHAR(50) NOT NULL AFTER `store_name`";
 			db_query($sql2);


			$sql3 = "UPDATE orders_for_tax_stats, zip_codes z SET orders_for_tax_stats.county = z.county WHERE orders_for_tax_stats.zip = z.zip AND z.cntydef = 'Y'";
			db_query($sql3);

		$sql = "SELECT orders_for_tax_stats.*, max(oc.date) AS last_payment_date,
			SUM(CASE 
			WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='sale' OR oc.action='capture') 
				THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts) 
			WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.sales_merchandise > 0) 
				THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
			WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.returns_and_allowances > 0)
				THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts) 
			ELSE 0 END ) as not_taxable, 
			SUM(CASE 
				WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture')
					THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts) 
				WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.sales_merchandise > 0) 
					THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
				WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.returns_and_allowances >0) 
					THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts) 
				ELSE 0 END ) as taxable,
			SUM(CASE 
				WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture') 
					THEN ocb.sales_tax 
				WHEN ocb.sales_tax >0 AND (oc.action='credit') 
					THEN -ocb.sales_tax ELSE 0 END ) as tax_collected
			FROM order_charges oc LEFT JOIN orders_for_tax_stats ON oc.order_id = orders_for_tax_stats.id
			LEFT JOIN order_charges_breakdown ocb ON oc.id = ocb.order_charges_id
			WHERE oc.action IN ('credit', 'sale', 'capture') 
			AND oc.payment_method <> 'manual_adjustment' and orders_for_tax_stats.id is not null";
		} // not having else cuz not doing by order_date anymore	
	}
	else {
$sql="(SELECT DISTINCT shipping_state AS state, oi.tax_rate as tax_rate,
           LEFT(shipping_zip, 5) AS zip, z.county as county,
               SUM(CASE WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='sale' OR oc.action='capture') THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                          WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.sales_merchandise > 0) THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts) 
                          WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.returns_and_allowances > 0) THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts)  ELSE 0 END ) as not_taxable,
                 SUM(CASE WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture') THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                          WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.sales_merchandise > 0) THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                                WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.returns_and_allowances >0) THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts)  ELSE 0 END ) as taxable,
                 SUM(CASE WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture') THEN ocb.sales_tax
                          WHEN ocb.sales_tax >0 AND (oc.action='credit') THEN -ocb.sales_tax  ELSE 0 END ) as tax_collected
                 FROM orders oi LEFT JOIN order_charges oc ON oi.id = oc.order_id LEFT JOIN order_charges_breakdown ocb ON oc.id = ocb.order_charges_id
                 LEFT JOIN zip_codes z ON (LEFT (oi.shipping_zip,5)=z.zip AND z.cntydef = 'Y') WHERE 1 AND oc.action IN ('credit', 'sale', 'capture')
                 AND oc.payment_method <> 'manual_adjustment' AND shipping_state='NY'
                 ";
	}
		if($start_date || $end_date){
		
			if ($date_type == "order_date") 
			{
				$sql .= db_queryrange('oi.date',$start_date,$end_date);
			}
			else if ($date_type == "payment_date") 
			{				
				$sql .= db_queryrange('oc.date',$start_date,$end_date);
			}
		}
			 // EXCLUDE FBA ORDERS--because amazon collects and remits tax for them
			 // should not exclude
	// $sql .= " AND oi.order_tax_option <> " .$CFG->amazon_fba_store_id;

if ($details_or_summary == "details") $sql .= " GROUP BY oc.order_id"; 	 
else if (!$total){		
		$sql .= " GROUP BY z.county"; 
}
if ($details_or_summary == "summary")
{
$sql .= ") UNION
                 (SELECT 'Non-NY' AS state, '--' as tax_rate,
           '--' as zip, '--' as county,
               SUM(CASE WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='sale' OR oc.action='capture') THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                          WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.sales_merchandise > 0) THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts) 
                          WHEN (ocb.sales_tax =0 OR ocb.sales_tax IS NULL) AND (oc.action='credit') AND (ocb.returns_and_allowances > 0) THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts)  ELSE 0 END ) as not_taxable,
                 SUM(CASE WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture') THEN (ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                          WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.sales_merchandise > 0) THEN -(ocb.sales_merchandise+ocb.shipping_income-ocb.sales_discounts)
                                WHEN ocb.sales_tax >0 AND (oc.action='credit')AND (ocb.returns_and_allowances >0) THEN -(ocb.returns_and_allowances+ocb.shipping_income-ocb.sales_discounts)  ELSE 0 END ) as taxable,
                 SUM(CASE WHEN ocb.sales_tax >0 AND (oc.action='sale' OR oc.action='capture') THEN ocb.sales_tax
                          WHEN ocb.sales_tax >0 AND (oc.action='credit') THEN -ocb.sales_tax  ELSE 0 END ) as tax_collected
                 FROM orders oi LEFT JOIN order_charges oc ON oi.id = oc.order_id LEFT JOIN order_charges_breakdown ocb ON oc.id = ocb.order_charges_id
                 WHERE 1 AND oc.action IN ('credit', 'sale', 'capture')
                 AND oc.payment_method <> 'manual_adjustment' AND shipping_state<>'NY'";
	if($start_date || $end_date){
		
			if ($date_type == "order_date") 
			{
				$sql .= db_queryrange('oi.date',$start_date,$end_date);
			}
			else if ($date_type == "payment_date") 
			{				
				$sql .= db_queryrange('oc.date',$start_date,$end_date);
			}
		}
         $sql .= "GROUP BY county)";
}
	//	echo "*** $sql *** " ;

		return db_query_array($sql);
	}	
		static function getOrderRefundsForTimePeriod($start_date='',$end_date='',$store="ALL", $sku = "ALL", $admin_id = "ALL", $brand_id = "ALL", $customer_name='', $customer_id=0, $shipping='ALL',$order_tag=array(),$name_keywords='ALL',$discounted_shipping='',$match_order_tags_exact='N',$payment_type=''){
		global $CFG;
		
		if ($admin_id == "9999999") $admin_id = 0;
		
		$sql = "
			SELECT DISTINCT
			orders.id, order_charges.date, orders.customer_id, orders.billing_first_name, orders.billing_last_name, store.name as store_name, customers.first_name, customers.last_name,
			order_charges.amount, order_charges_breakdown.* 
			";
			
		$from_clause = " FROM (orders, store,order_charges, order_charges_breakdown";
				
		$where_clause = " WHERE 1 
			AND orders.order_tax_option = store.id
			AND order_charges.order_id = orders.id
			AND order_charges.id = order_charges_breakdown.order_charges_id			
			AND order_charges.action = 'credit'
			AND subtotal > 0			
			AND orders.current_status_id <> " . $CFG->quote_status .
			" AND orders.current_status_id <> " . $CFG->test_order_status_id .
			" AND orders.current_status_id <> " . $CFG->government_quote_status_id .
			" AND orders.current_status_id <> " . $CFG->awaiting_fraud_screening_status_id;

		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$where_clause .= db_queryrange('order_charges.date',$start_date,$end_date);
		}
		
		if ($store != "ALL") $where_clause .= " AND orders.order_tax_option = '".mysql_real_escape_string($store)."'";
		if ($admin_id !== "ALL") $where_clause .= " AND orders.admin_id = '".mysql_real_escape_string($admin_id)."'";
		if ($shipping !== "ALL") $where_clause .= " AND (orders.shipping_id = '".mysql_real_escape_string($shipping)."' OR orders.shipping_id_2  = '".mysql_real_escape_string($shipping)."')";
        if ($discounted_shipping=='Y') $where_clause .= " AND (orders.original_shipping > orders.shipping)";
		if ($discounted_shipping=='N') $where_clause .= " AND (orders.original_shipping <= orders.shipping)";
		
		if ($brand_id != "ALL")
		{
			$from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND products.brand_id = '".mysql_real_escape_string($brand_id)."'";
		} 
		if ($sku != "ALL") 
		{
			if ($brand_id == "ALL") $from_clause .= ", order_items, products";
			$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id 
							   AND (products.vendor_sku LIKE '".mysql_real_escape_string($sku)."%' OR product_options.vendor_sku LIKE '".mysql_real_escape_string($sku)."%')";	
		}
		
        if ($customer_name){
			$name_array = explode(" ",$customer_name);
			if (!$name_array[1]) {
				$name_array[1]=$name_array[0];
                $exp=' OR ';
			}
			else $exp=' AND ';
			$where_clause .=" AND ((orders.billing_first_name LIKE '%".$name_array[0]."%'"
							  .$exp."orders.billing_last_name LIKE '%"  .$name_array[1]."%')	
							  OR (orders.billing_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.billing_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "orders.shipping_last_name LIKE '%".$name_array[1]."%')
							  OR (orders.shipping_first_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (orders.shipping_last_name LIKE '%".$name_array[0]." ".$name_array[1]."%')
							  OR (customers.first_name LIKE '%".$name_array[0]."%'"
							  .$exp. "customers.last_name LIKE '%".$name_array[1]."%'))";
							  
							  
		}		
		if ($customer_id){
			$where_clause .=" AND orders.customer_id=".$customer_id;
			
		}
        if ($payment_type){
            $where_clause .=" AND orders.payment_type='".$payment_type ."'";
        }
		if ($name_keywords != "ALL") 
		{
			if ($sku == "ALL" && $brand_id == "ALL") 
			{
				$from_clause .= ", order_items, products";
				$where_clause .= " AND order_items.order_id = orders.id 
							   AND order_items.product_id = products.id";
			}
			//$sql .= ", products.name ";
			$where_clause .= " AND products.name LIKE '%".mysql_real_escape_string($name_keywords)."%'";	
		}
		
		$from_clause .= ") LEFT JOIN customers on orders.customer_id = customers.id";
		if ($sku != "ALL") $from_clause .= " LEFT JOIN product_options on products.id = product_options.product_id";
		$where_clause .= " AND shipping_address1 NOT LIKE '27 Chestnut S%'";
		if(count($order_tag) > 0)
		{
			if ($match_order_tags_exact == 'Y')
			{
				if(sizeof($order_tag) == 1 && $order_tag[0] == "no_tags")
				{
					$where_clause .= " AND orders.order_tags = ''";
				}
				else 
				{
					$order_tags_string = ",";
					foreach ($order_tag as $one_tag)
					{
						$order_tags_string .= $one_tag.",";
					}	
					$where_clause .= " AND orders.order_tags = '"  . $order_tags_string . "'";
				}
			}
			else
			{
				if(sizeof($order_tag) == 1 && $order_tag[0] == "no_tags")
				{
					$where_clause .= " AND orders.order_tags = ''";
				}
				else 
				{
					foreach($order_tag as $one_tag)
					{
						$where_clause .= " AND orders.order_tags LIKE '%,"  . $one_tag . ",%'";	
					}
				}
			}	
		}
		$order_by = " ORDER BY orders.id ASC ";
		$sql = $sql . $from_clause . $where_clause . $order_by;
		echo $sql;
		//flush(); 
		return db_query_array($sql);
	}
static function getRMARequestReport($start_date='',$end_date=''){

	global $CFG;
	
   $sql="SELECT purchase_order_notes.date_added, brands.name AS brand_name, purchase_order.code, purchase_order_id FROM `purchase_order_notes`, purchase_order, brands WHERE purchase_order.supplier_id = brands.id AND purchase_order_id = purchase_order.id AND purchase_order_notes.notes LIKE '%RMA Request%'";
		if($start_date || $end_date){
			if($start_date){
				$start_date .= " 00:00:00";
			}
			if($end_date){
				$end_date .= " 23:59:59";
			}
			$sql .= db_queryrange('purchase_order_notes.date_added',$start_date,$end_date);
		}

		//echo "*** $sql *** " ;
 $sql .= " order by brand_name ASC, date_added ASC";
		return db_query_array($sql);
	}	
}
