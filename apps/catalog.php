<?php

class Catalog {


	static function makeProductLink_($product, $cats = false, $occs = false, $color='', $nossl=false)
	{
		global $CFG;

		$link = $CFG->baseurl;
         
		 if ($nossl)
			$link = $CFG->nosslurl;	

			//Going to always have a category involved. So if no cat, just grab main cat for a product
			//$cat = Cats::get1($product['cat_id']);
			//$link .= Catalog::makeCatLink( $cat, true, '', 0, true );


		$link .= Catalog::_makeProductLink(trim($product['url_name']),false);

		return $link;
	}

	function _makeProductLink($sku,$is_dir=false)
	{
	    //Changing from SKU to name for the URL


		return rawurlEncode($sku.($is_dir ? '/' : '.html'));
	}

	function makePackageLink($package='')
	{

		global $CFG;

		$link = $CFG->baseurl . 'Packages/';

		if ($package) {
			$link .= Catalog::_makeLink($package['id'],$package['name'],false);
		}

		return $link;
	}

	// the last parameter is a bit of a hack, but we need it for a nifty feature
	// on the backend for url previews.  better to hack that than the real functionality.
	function makeCatLink($cat, $no_base = false, $override_last_cat_name = '',$brand_id=0, $dir=false, $nossl = false)
	{

		global $CFG;
		//$CFG->baseurl="http://www.tigerchef.com/";
		if( $brand_id )
		{
			$brand = Brands::get1( $brand_id );
			return self::makeBrandLink( $brand, $cat, false );
		}

		if (!$no_base)
			$link = $CFG->baseurl;

		if (!$no_base && $nossl)
			$link = $CFG->nosslurl;	
		
		$pid = $cat['pid'];

			//pre rewrites
			//return $link . 'cat_info.php?cat_id=' . $cat['id'];


			return $link . Catalog::_makeLink($cat['id'],$cat['url_name'],$dir);
	}

	function makeOccasionLink($occasion,$cat='', $no_base = false)
	{
		global $CFG;

		if (is_array($cat)) {
			$link = Catalog::makeCatLink($cat, $no_base);
		}
		else {
			if (!$no_base)
				$link = $CFG->baseurl;
		}

		return $link . Catalog::_makeLink('O'.$occasion['id'],$occasion['name'],true);
	}

	function makeBrandLink($brand,$cat='', $no_base = false)
	{
		global $CFG;

		if (is_array($cat)) {
				$link = Catalog::makeCatLink($cat, $no_base);
		}
		else {
			if (!$no_base)
				$link = $CFG->baseurl;
		}

		return $link . Catalog::_makeLink('B'.$brand['id'],$brand['name'],true);
	}
	
	function makeBrandLogoLink($brand_id, $logo_url = '', $is_home = false){
		global $CFG;
	$message = 'brand logo link: params: ' . $brand_id . ', ' . $logo_url . ',';
    $message .= $is_home ? ' true ': ' false ';
        if(empty($logo_url)){
$message .= 'no brand ulr, getting' . PHP_EOL;
            $brand = Brands::get1( $brand_id );
            $logo_url = $brand['logo_url'];
        }
       if($is_home){
            $home_logo_url = 'home-' . $logo_url;
$message .= ' the home logo url: ' . $home_logo_url . PHP_EOL;
            $path_on_server = $CFG->dirroot . '/brand_logos/' . $home_logo_url;
$message .= 'the path on server: ' . $path_on_server . PHP_EOL;
            if(!file_exists($path_on_server)){
$message .= ' could not find, setting url to: ' . $logo_url . PHP_EOL;
                $path_on_server = $CFG->dirroot . '/brand_logos/' . $logo_url;
            }
            else{
$message .= ' found ,setting url to :' . $home_logo_url . PHP_EOL;
                $logo_url = $home_logo_url;
            }
//mail('hgolov@gmail.com', 'Brand Logos', $message);
        }
        else{
            $path_on_server = $CFG->dirroot . '/brand_logos/' . $logo_url;
        }
   
		if ($CFG->site_name == "lionsdeal") {
			$path = $CFG->baseurl . "brand_logos/" . $logo_url;
		}
		else if (!file_exists($path_on_server)) $path =$CFG->brandlogos_server.'/comingSoon.jpg';
		else $path = $CFG->brandlogos_server . "/" . $logo_url;
	
		return $path;
	}
	
	static function makeBrandLinkNew($brand_id)
	{

		global $CFG;
		//$CFG->baseurl="http://www.tigerchef.com/";
		if( $brand_id )
		{
			$brand = Brands::get1( $brand_id );
		}
		else return;
		
		$link = $CFG->baseurl;		
				
		return $link . Catalog::_makeLink($brand['id'],strtolower($brand['url_name']));
	}
	
	/** NOT USED
	function makePackageLink($package='')
	{

		global $CFG;

		$link = $CFG->baseurl . 'Packages/';

		if ($package) {
			$link .= Catalog::_makeLink($package['id'],$package['name'],false);
		}

		return $link;
	}
	**/

	function makePriceFilter($start_price,$end_price)
	{

		$link = preg_replace('/P-[0-9]+-[0-9]+\//s','',$_SERVER['REQUEST_URI']);
		$link = preg_replace('/Page-[0-9]+\//s','',$link);

		return $link."P-$start_price-$end_price/";
	}

	function _makeLink($id,$name,$is_dir=false, $color='')
	{
		//$name = ereg_replace('[^a-zA-Z0-9 ]','',$name);

		if( $color != '' )
			$color = '-' . str_replace(' ','-',$color);
		return rawurlEncode(str_replace(' ','-',$name)).$color .($is_dir ? '/' : '.html');
	}

	function showError($error)
	{
		?>
		<div class=error><?= htmlentities($error) ?></div><br>
		<?
	}

	function showListing($products,$type='product_in_cat',$width='',$show_date = false, $amt_per_row = 3, $amt_per_page = 12, $info='', $show_brand=true, $show_price=true, $show_color=true, $cat_filters='' )
	{
		global $CFG;
		if ($type == 'product_in_cat' || $type == 'product_in_occ') {
			$img_id_field = 'id';
			$link_func = 'makeProductLink_';
		}
		else if ($type == 'package') {
			$img_id_field = 'main_product_id';
			$link_func = 'makePackageLink';
		}

		if (!is_array($products)) {
			echo('<br>');
			echo'<div id="content">';
			Catalog::showError('Sorry, there are no products in this section yet.');
			echo'</div>';
			echo'</div>';
			return;
		}

		$count = 0;
		$product_ct = count($products);

		if ($product_ct > $amt_per_page) {
			$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
			$amt_pages = ceil($product_ct / $amt_per_page);

			if ($current_page > $amt_pages && $current_page > 1) {
				$new_url = str_replace("/Page-$current_page",'',$_SERVER['REQUEST_URI']);
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: $new_url");
				exit;
			}

			$start_at = ($current_page - 1) * $amt_per_page;
			$full_products = $products;
			$products = array_slice($products,$start_at,$amt_per_page);

			$page_link = preg_replace('/Page-[0-9]*\//i','',$_SERVER['REQUEST_URI']);
			$page_link = preg_replace('/&page=[0-9]*/i','',$page_link);



			$page_var = 'Page-';
			$page_var_post = '/';

			//var_dump($page_link);

			if (strstr($page_link,'?') !== false) {
				$page_var = '&page=';
				$page_var_post = '';
			}

			$pagination  = array();


			for ($x=1;$x<=$amt_pages;$x++) {
				if ($current_page != $x) {

					if ($x > 1)
						$pagination[$x] = "<a href=\"{$page_link}{$page_var}".$x."$page_var_post\"> ";
					else
						$pagination[$x] = "<a href=\"{$page_link}\"> ";

				} else
					$pagination[$x] = " <b>$x</b> ";
			}

			//echo $pagination;

		}


		if( $info['subcats'] )
			$main_cat = $info['subcats'];
		else
			$main_cat = $info['cat_id'];
		?>


			<!-- sort start -->
			<div class="clearfix" id="sort_order">

				<?if( isset( $info['color'] ) )
					{
					?><input type="hidden" name="color" value="<?=$info['color']?>" /><?
					}
					if( isset( $info['range_start'] ) )
					{
					?><input type="hidden" name="range_start" value="<?=$info['range_start']?>" /><?
					}
					if( isset( $info['range_end'] ) )
					{
					?><input type="hidden" name="range_end" value="<?=$info['range_end']?>" /><?
					}
					if( isset( $info['width'] ) )
					{
					?><input type="hidden" name="width" value="<?=$info['width']?>" /><?
					}
					?>

				<? if( $show_brand )
				{
					if( $main_cat )
						$brands = Brands::getBrandsForCat( $main_cat );
					else
						$brands = Brands::get( 0, '', '', '', '', '', 'Y');
					?>
				<label class="floatleft">List By Brand<select name="brand_id" id="brand" onchange="updateResults(this);">
					<option value="0">All Brands</option>
					<?

					//$brands = Brands::get(0,'','','','');
					foreach( $brands as $brand ) {
						?><option value="<?=$brand['id']?>" <?=($_REQUEST['brand_id'] == $brand['id'])?'selected':''?>><?=$brand['name']?></option><?
					}
					?>
				</select></label>
				<? } if( $show_price ) { ?>
				<label class="floatleft">List By Price<select name="sortby" onchange="updateResults(this);">
					<option value="0">Sort by</option>
					<option value="1" <?=($info['sortby'] == 1)?'selected':''?>>Price: Low to High</option>
					<option value="2" <?=($info['sortby'] == 2)?'selected':''?>>Price: High to Low</option>
					<option value="3" <?=($info['sortby'] == 3)?'selected':''?>>Product Name</option>
				</select></label>
				<? } if( $show_color ) { ?>
				<label class="floatleft">List By Color<select name="color" onchange="updateResults(this);">
					<option value="0">Choose Color</option>
					<option value="white" <?=($info['color'] == 'white')?'selected':''?>>White</option>
					<option value="bisque" <?=($info['color'] == 'bisque')?'selected':''?>>Bisque</option>
					<option value="black" <?=($info['color'] == 'black')?'selected':''?>>Black</option>
					<option value="stainless" <?=($info['color'] == 'stainless')?'selected':''?>>Stainless Steel</option>
					<option value="titanium" <?=($info['color'] == 'titanium')?'selected':''?>>Titanium</option>
					<option value="satina" <?=($info['color'] == 'satina')?'selected':''?>>Satina</option>
					<option value="other" <?=($info['color'] == 'other')?'selected':''?>>Other</option>
				</select></label>
			</form>
				<? } ?>
			</div>
			<script type="text/javascript">
				var pr_style_sheet="http://cdn.powerreviews.com/aux/11224/5458/css/powerreviews_express.css";
			</script>
			<script type="text/javascript" src="http://cdn.powerreviews.com/repos/11224/pr/pwr/engine/js/full.js"></script>
				<?/* if ($pagination) { ?>
				<div class="belowpag" style="clear:both;">Page <?= $pagination ?></p></div>
				<? }*/ ?>


			<!-- sort end -->

			<!-- results start -->
			<div id="sort_results">
				<!-- result start -->
				<?

		$num_products = count($products);

		foreach ($products as $product) {

			Catalog::outputProductBlock($product, $img_id_field, $link_func, $show_date, $cat_filters );

			$count++;
		}


		?>

				<!-- result end -->
			</div>
			<!-- results end -->

			<!-- sort start -->
			<? if ($pagination) { ?>
			<div class="clearfix" id="sort_order_bottom">
				<p class="floatright">Page
				<?
					if( $current_page > 1 )
					{
						echo $pagination[$current_page - 1]. ' < </a>';
					}
					for( $i = $current_page; $i < $current_page+10; $i++ )
					{
						if( !$pagination[$i] )
							break;

						if( $i == $current_page )
							echo $pagination[$i];
						else
						{
							echo $pagination[$i] . $i .'</a>';
						}
						if( $i == $current_page + 9 && $pagination[$i+1])
							echo $pagination[$i+1] . ' > </a>';
					}

				?>
				</p>
			</div>
			<? } ?>
			<!-- sort end -->
		</div>
		<!-- content end -->
		<?
	}



	function outputProductBlock($product, $img_id_field = 'id', $link_func = 'makeProductLink_', $show_date = false, $cat_filters='' )
	{
		global $CFG;

		if(!is_array($product)) return;

		$cat_id = $_REQUEST['cat_id'];
		$occ_id = $_REQUEST['occ_id'];

		if (!$cat_id && !$occ_id) {
			$cat_id = $product['cat_id'];
		}

		/*
		if (file_exists('itempics/'.$product[$img_id_field].$CFG->thumbnail_suffix)) {
			$filename = '/itempics/'.$product[$img_id_field].$CFG->thumbnail_suffix;
		}else {
			$filename = '/pics/spacer.gif';
		}*/
		$replace_msg = '';
		if( $product['replace_with'] != '' )
		{
			//They dont want to replace it here, still let them go to it, show it on the product page
			//$replace_msg = $product['vendor_sku'] . ' has been replaced by -- ';
			//$product = Products::get1ByModel( $product['replace_with'] );
		}
		$prod_link = Catalog::$link_func($product, $cat_id, $occ_id);
		if (file_exists('itempics/'.$product['img_thumb_url'])) {
			$filename = '/itempics/'.$product['img_thumb_url'];
		}else {
			$filename = '/pics/spacer.gif';
		}
		$short_name = StdLib::addEllipsis( strip_tags($product['name']) );
		$brand = Brands::get1( $product['brand_id'] );
		?>

		<div class="clearfix sort_result" onmouseover="this.style.backgroundColor='#fbf9ed';" onmouseout="this.style.backgroundColor='#ffffff';">
			<!-- left side start -->
			<div class="sres_img">
			<a style="vertical-align: middle" href="<?= $prod_link ?>"><img class="floatleft" src="<?= $filename ?>" width=<?= $CFG->thumbnail_width ?> height=<?= $CFG->thumbnail_height ?> alt="<?= str_ireplace('"',"''",$short_name) ?>" border=0/></a>
			</div>
			<div class="sres_data">
				<div class="sres_topper clearfix">
					<div class="topper_product_link">
					<h2 class="product_link"><a href="<?= $prod_link ?>"><?=$replace_msg?> <?= strip_tags($brand['name']) ?> - <?=$product['vendor_sku']?></a></h2>
					</div>
					<?/*
					<div class="sres_topper_left">
											</div>
					<div class="sres_topper_right">
					</div>*/?>
					<div class="topper_info_links">
						<div class="info_link1">
						<input type="checkbox" name="compare[]" value="<?=$product['id']?>" id="compare_<?=$product['id']?>" style="margin-bottom:3px;">
						<a href="javascript:compareProducts();">Compare</a>
						</div>
						<div class="info_link2">
						<?
						if(Rebates::getRebatesForProduct($product['id'])){
							echo '<a href="'.$prod_link.'"><img src="'.$CFG->protocol_relative_url.'images/rebates-avail.gif" alt="REBATES AVAILABLE" /></a>';
						}?>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="sres_left">
					<p><a class="description_link" href="<?= $prod_link ?>"><?= $short_name ?></a></p>

					<ul>
						<li><strong>Width:</strong> <?=Products::displayDimension( 'width', 'product', $product['id'], true )?></li>
						<li><strong>Height:</strong> <?=Products::displayDimension( 'height', 'product', $product['id'], true )?></li>
						<li><strong>Length:</strong> <?=Products::displayDimension( 'length', 'product', $product['id'], true )?></li>
					</ul>
					<div style="float:left;width:130px" class="pr_snippet_category">
						<script type="text/javascript">
						var pr_snippet_min_reviews=1;
						POWERREVIEWS.display.snippet(document, { pr_page_id : "<?=$product['id']?>" });
						</script>
					<f/div>
				</div>
				<!-- left side end -->

				<!-- right side start -->
				<?
					$options = Products::getProductOptions( 0, $product['id'], false, true, 'additional_price' );
					//print_ar( $options );

				//	print_ar( $markup );
				$min_height = 100;
				$opt_height = count($options) * 31;
				?>
				<div class="sres_right" style="height: <?=(($opt_height > $min_height)?$opt_height:$min_height)?>px;">

				<?
					if($product['brand_name'] === "Jenn-Air" && $CFG->disable_jennair){
						$jennair = true;
					}
					else{
						$jennair = false;
					}

					if( is_array( $options ) )
					{
						foreach( $options as $option )
						{
							$markup = Products::getProductMarkup( $option );
							//additional price is the base price, need to add front end mark up
							$price = $markup ? number_format($markup, 2) : false;
							$is_map = Products::isMap($product['vendor_sku']);

							if($option['vendor_sku']){
								$sku = $option['vendor_sku'];
								$prod_opt_link = $CFG->baseurl.Catalog::_makeProductLink(trim($sku),false);
							}
							else{
								$sku = $product['vendor_sku'];
								$prod_opt_link = $prod_link;
							}

							?><div class="sres_sku"><a href="<?= $prod_opt_link ?>"><?= $sku ?>  <span style="font-weight:normal">(<?=StdLib::addEllipsis($option['value'],30)?>)</span></a></div><div class="sres_price"><a href="<?= $prod_opt_link ?>">
							<?=( $price ) ? ($is_map || $jennair ? 'View Here' : '$'.$price) : '<span class="unavailableprice">Unavailable</span>'?>
							</a></div><?
						}
					}
					else
					{
						$markup = Products::getProductMarkup( $product );
						$is_map = Products::isMap($product['vendor_sku']);
						//Need to add front end mark up

						$price = $markup ? number_format($markup, 2) : false;
						?><div class="sres_sku"><a href="<?= $prod_link ?>"><?= $sku ?><?= $product['vendor_sku'] ?></a></div><div class="sres_price"><a href="<?= $prod_link ?>">
						<?=( $price ) ? ($is_map || $jennair ? 'View Here' : '$'.$price) : '<span class="unavailableprice">Unavailable</span>'?>
						</a></div><?
					}
				?>
				<?/* MOVED UP TO NEW "THICK BLUE BAR"
				<div class="listing_info_links">
						<?
							//<a style="display:none" class="viewbut" href="$prod_link">More Info</a>
							if(Rebates::getRebatesForProduct($product['id'])){
								echo '<a class="viewbut" href="'.$prod_link.'"><img src="'.$CFG->baseurl.'images/rebates-avail.gif" alt="REBATES AVAILABLE" /></a>';
							}
						?>

						<div class="compare_link">
							<input type="checkbox" name="compare[]" value="<?=$product['id']?>" id="compare_<?=$product['id']?>" style="margin-bottom:3px;">
							<a href="javascript:compareProducts();">Compare</a>
						</div>
					</div>
				*/?>
				</div>

			<!-- right side end -->
			</div>
		</div>
		<?

	}

function getBreadcrumbBar($vars)
	{
	    global $CFG;

	    /* this is coming from direct_me.php */
	    global $cat;
	    global $product;
		global $filter;
	    global $is_category;
	    global $is_product;
	    global $is_filter;
	    
	    //print_ar($vars);
// 	    var_dump($product);
// 	    var_dump($is_category);
// 	    var_dump($is_product);

	    $bullet        = "&gt;";
	    $bullet_for_condensed_bar = "&lt;";
	    $home_element  = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="breadcrumb-element"><a href="'.$CFG->baseurl.'" itemprop="item"  onclick="sendToDataLayer(\'Breadcrumbs\', \'Breadcrumbs\');" ><span itemprop="name">JustCharger</span></a></span>';
	    $bar           = $home_element;
	    $tmp_bar       = $bar;
	    $url_string    = $_REQUEST['url_string'];
	 
	 if ($CFG->site_name == "lionsdeal") {
	    	$bullet 	= "&gt;";
			$bullet_for_condensed_bar = "";
	    	$home_element = '';
	    	$bar		= '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'.$CFG->baseurl.'"  onclick="sendToDataLayer(\'Breadcrumbs\', \'Breadcrumbs\');"  itemprop="item"><span itemprop="name">Home</span></a></span>';
	    	$tmp_bar	= $bar;
	    	$url_string	= $_REQUEST['url_string'];
	    }

		if ($CFG->site_name == "justchargerplates") {
	    	$bullet 	= "&gt;";
			$bullet_for_condensed_bar = "";
	    	$home_element = '';
	    	$bar		= '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'.$CFG->baseurl.'"  onclick="sendToDataLayer(\'Breadcrumbs\', \'Breadcrumbs\');"  itemprop="item"><span itemprop="name">Home</span></a></span>';
	    	$tmp_bar	= $bar;
	    	$url_string	= $_REQUEST['url_string'];
	    }
        if ($is_filter){
        	
        	$bar .= '&nbsp;'.$bullet.'&nbsp;' ;
        	$bar .= $filter['name'].'-'.$_REQUEST['value'];
        }
	    /* just make sure we have something to work with */
	    else if ($is_product)
	    {
		if( $_SESSION['parent_cat_id'] && false ) { // this section not used
		    $cat_tree = Cats::getCatsTree($_SESSION['parent_cat_id']);

		    foreach ($cat_tree as $cat_info)
		    {
			$bar .= ' $bullet <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href=\"'.Catalog::makeCatLink($cat_info).'\"   onclick="sendToDataLayer(\'Breadcrumbs\', \'Breadcrumbs\');"    itemprop="item"><span itemprop="name">';
			$bar .= $cat_info[name];
			$bar .= "</span></a></span>";
			$last_cat = Catalog::makeCatLink($cat_info) . '?1';
		    }

		    /* append product at end of breadcrumb */
		    $bar .= " $bullet " . $product['name'];
		} else { // this is used
		    $cats = Cats::getCatsForProduct($product['id']);
			//$cats = Cats::getCatsForProduct($product['id']);
					    
		    // browse by store cats
		    $browse_by_store_cats_multi = Cats::getAllSubsStructure('1342', $browse_by_store_cats_multi);
		    if ($browse_by_store_cats_multi)
		    {
		    	$browse_by_store_cats = array_values_recursive($browse_by_store_cats_multi);
		    	$browse_by_store_keys = array_keys_multi($browse_by_store_cats_multi);
		    }
		    else 
		    {
		    	$browse_by_store_cats = array();
		    	$browse_by_store_keys = array();
		    }
		    
		    
		    // holiday shop cats
		    $holiday_shop_cats_multi = Cats::getAllSubsStructure('1808', $holiday_shop_cats_multi);
		    if ($holiday_shop_cats_multi)
		    {
		    	$holiday_shop_cats = array_values_recursive($holiday_shop_cats_multi);
				$holiday_shop_keys = array_keys_multi($holiday_shop_cats_multi);
		    }
		    else
		    {
		    	$holiday_shop_cats = array();
		    	$holiday_shop_keys = array();
		    }
			
			// shop Thanksgiving cats
			$thanksgiving_cats_multi = Cats::getAllSubsStructure('1398', $thanksgiving_cats_multi);
			if ($thanksgiving_cats_multi)
			{
				$thanksgiving_cats = array_values_recursive($thanksgiving_cats_multi);
				$thanksgiving_keys = array_keys_multi($thanksgiving_cats_multi);
			}
			else 
			{
				$thanksgiving_cats = array();
				$thanksgiving_keys = array();
			}
			
			// Winter 2012 catalog
			$catalog_cats_multi = Cats::getAllSubsStructure('15', $catalog_cats_multi);
			if ($catalog_cats_multi)
			{
				
				$catalog_cats = array_values_recursive($catalog_cats_multi);
				$catalog_keys = array_keys_multi($catalog_cats_multi);
			}
			else 
			{
				$catalog_cats = array();
				$catalog_keys = array();
			}
		    
			
			$bad_cats = array_merge($catalog_cats, $catalog_keys, $browse_by_store_cats, $holiday_shop_cats, $thanksgiving_cats, $browse_by_store_keys, $holiday_shop_keys, $thanksgiving_keys);
			
		    $old_cats = $cats;
		    foreach ($cats as $the_cat)
		    {
		    	if (!in_array($the_cat['id'], $bad_cats) && $the_cat['is_hidden'] == 'N') 
		    	{
		    		$new_cats[] = $the_cat;
		       	}
		    	//else echo $the_cat['id']. " is in bad cats<br>";
		    }
		    //print_r($new_cats);
		    if (count($new_cats) == 0) $cats = $old_cats;
		    else $cats = $new_cats;
			// RS added code 1/13 to get the deepest category (or one of the deepest) to which the  prod belongs
			// for the breadcrumb
		    $max_depth = 0;
		    $max_id = $cats[0]['id'];
		    foreach ($cats as $the_cat)
		    {
		    	if ($the_cat['is_hidden'] == 'Y') continue;
		    	$this_depth = Cats::getCatDepth($the_cat['id']);
		    	if ($this_depth > $max_depth)
		    	{
		    		$max_depth = $this_depth;
		    		$max_id = $the_cat['id'];
		    	}
		    }
		    $tree = Cats::getCatsTree( $max_id);
		    
		    // end code to get deepest category
		    //$tree = Cats::getCatsTree( $cats[count($cats)-1]['id']); /* old code */
		    
			$count = 0;
		    $cats_num = count($tree);
		    foreach ($tree as $cat_info)
		    {
		    	$count++;
		    	
		    	if($count == $cats_num){
			    	$last_cat_class = "last-breadcrumb-cat";
			    	$short_bullet_span = "<span class='breadcrumb-bullet short-bullet'>{$bullet_for_condensed_bar}</span>";
		    	}else{
		    		$last_cat_class = $short_bullet_span = '';
		    	}
		    	
		    	if ($cat_info[name] == "Browse by Store Type")
				{
			    	$bar .= "<span class='breadcrumb-element'> {$bullet} {$cat_info[name]}</span>";
				}
				else 
				{
					$bar .= "<span class='breadcrumb-element {$last_cat_class}'> <span class='breadcrumb-bullet'>{$bullet}</span>{$short_bullet_span} <span itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\" ><a href=\"".Catalog::makeCatLink($cat_info)."\"   onclick=\"sendToDataLayer('Breadcrumbs', 'Breadcrumbs');\"   itemprop=\"item\"><span itemprop=\"name\">";
					$bar .= $cat_info[name];
					$bar .= "</span></a></span></span>";
				}
				$last_cat = Catalog::makeCatLink($cat_info) . '?1';
				
		    }

		    /* append product at end of breadcrumb */
		    $bar .= "<span class='breadcrumb-element'> $bullet " . $product['name'].'</span>';
		    //print_ar( $cats );
		}
	    }
	    else if ($is_category)
	    {
		if ($cat['id'] > 0)
		{
			global $cat_tree;
		    $cat_tree = Cats::getCatsTree($cat['id']);
		    //print_ar($cat_tree);  

		    $cat_count = count($cat_tree);

		    foreach ($cat_tree as $cat_info)
			{
				$last_cat_class = $short_bullet_span = '';

				//$bar .= "<span class='breadcrumb-element'id='{$last_cat_class}'>";
				if ($cat_count < 2 || $cat_info[name] == "Browse by Store Type")
				{
					if($cat_info[name] == "Browse by Store Type" && $cat_count == 2){
						$bar .= "<span class='shortened-breadcrumbs'>{$home_element}</span>";
					}
					
					$bar .= "<span class='breadcrumb-element'> <span class='breadcrumb-bullet'>{$bullet}</span> ";
					if ($_REQUEST['manufacturer'] && $cat_info[name] != "Browse by Store Type") 
				    {
				    	$brand_info = Brands::get1ByName($_REQUEST['manufacturer']);
				    	if ($brand_info && $brand_info['name'] != $cat_info['name']) $bar .= "<span class='breadcrumb-element'>{$brand_info['name']} </span>";
				    }
					
				    $bar .= "{$cat_info[name]}";
				}
				else
				{
					if ($cat_count < 3){
						$last_cat_class = "last-breadcrumb-cat";
						$short_bullet_span = "<span class='breadcrumb-bullet short-bullet'>{$bullet_for_condensed_bar}</span>";
					}

					$bar .= "<span class='breadcrumb-element'id='{$last_cat_class}'> <span class='breadcrumb-bullet'>{$bullet}</span>{$short_bullet_span} <span itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\"><a href=\"".Catalog::makeCatLink($cat_info)."\"  onclick=\"sendToDataLayer('Breadcrumbs', 'Breadcrumbs');\"   itemprop=\"item\"><span itemprop=\"name\">";			    
					$bar .= $cat_info[name];
					$bar .= "</span></a></span>";
				}
				$bar .= "</span>";
	
				$last_cat = Catalog::makeCatLink($cat_info) . '?1';
				--$cat_count;
		    }
		}
	    }else{
			$bar .= "<span class='shortened-breadcrumbs'>{$home_element}</span>";
		}

		/* trail for the cart */
		if (preg_match('/\/cart\d?\.php/', $_SERVER['SCRIPT_NAME']))
		{
		    $bar .= " $bullet Shopping Cart";
		}

		if (strstr($_SERVER['SCRIPT_NAME'], '/account.php'))
		{
		    $my_acc_link = '<span itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\"><a href="/account.php"   onclick="sendToDataLayer(\'Breadcrumbs\', \'Breadcrumbs\');"  itemprop=\"item\"><span itemprop=\"name\">My Account</span></a></span>';
		    switch($vars['action']){
			    case 'track_packages':
				    $bar .= " $bullet $my_acc_link $bullet Track Packages";
				    break;
			    case 'change_billing':
				    $bar .= " $bullet $my_acc_link $bullet Change Billing Address";
				    break;
				    case 'change_shipping':
				    $bar .= " $bullet $my_acc_link $bullet Change Shipping Address";
				    break;
			    case 'update_shipping':
				    $bar .= " $bullet $my_acc_link $bullet Update Shipping Address";
				    break;

				    case 'change_password':
				    case 'change_email':
				    $bar .= " $bullet $my_acc_link $bullet Change Email/Password";
				    break;
			    case 'print_invoice':
				    $bar .= " $bullet $my_acc_link $bullet Invoice {$CFG->order_prefix}".(int)$_REQUEST['order_id'];
				    break;
			    case 'open_orders':
				    $bar .= " $bullet $my_acc_link $bullet Open Orders";
				    break;

			    default:
				    $bar .= " $bullet My Account";
				    break;
		    }

		}

	    if ($_SERVER['SCRIPT_NAME'] == '/invoice.php')
	    {
		$bar .= " $bullet Invoice";
	    }

	    if ($_SERVER['SCRIPT_NAME'] == '/product_review.php')
	    {
		$bar .= " $bullet Product Review";
	    }

	    if ($_SERVER['SCRIPT_NAME'] == '/search.php')
	    {
		$bar .= " $bullet Search Results";
	    }

		/* rebates */
		if ($_SERVER['SCRIPT_NAME'] == '/rebates.php')
		{
		    $bar .= " $bullet Rebates";
		}

		/* comparison */
		if ($_SERVER['SCRIPT_NAME'] == '/compare.php')
		{
		   $bar .= " $bullet Product Comparison";
		}
		
		/* Breadcrumbs for blog */
		if (strpos($_SERVER['SCRIPT_URL'], "/blog/") !== false && function_exists('wp_title'))
		{
			$bar .= " $bullet <span itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\"><a href=\""."/blog/"."\"   itemprop=\"item\"><span itemprop=\"name\">Chef Blog</span></a></span>";			    
			$bar .= " ".$bullet . str_replace("TigerChef Blog", "", wp_title("", false));
		}

		/* use the title for products or cats that don't have a name or vendor sku */
		if ($tmp_bar == $bar && $_SERVER['SCRIPT_NAME'] != '/index.php' && $GLOBALS['title'])
		{
		    $bar .= " $bullet ".htmlentities($GLOBALS['title']);
		}

		return $bar;
	}	
	


	  function header301($url)
	  {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $url");
			exit();
	  }

		function assertURL($current_url, $proper_url)
		{
			$c = Catalog::_removeLeadingAndTrailingSlashes($current_url);
			$p = Catalog::_removeLeadingAndTrailingSlashes($proper_url);

			if ($c != $p) {
				Catalog::header301($proper_url);
			}
		}

	static function outputAccessory($product, $img_id_field = 'id', $link_func = 'makeProductLink_', $show_date = false, $cat_filters='', $listing='' )
	{
		global $CFG;

			$options = Products::getProductOptions( 0, $product['id'], false, true, 'additional_price' );
			//$markup = Products::getProductMarkup( $product );
			if (!is_array( $options ) )
			{
				$product['additional_price'] = $product['price'];
				$options = array($product);
			}

			$first = true;

			foreach ($options as $option) {
				$markup = Products::getProductMarkup( $option );
				if( $first )
					$first = false;
				$check_value = "product_id={$product['id']}";
				if ($option['value'])
				{
					$check_value .= "&options={$option['id']}";
				}

				?>
				<li class="<?=$listing?>">
				<label for="accessory_<?=$option['id']?>">
				<span class="prod_price">
						<span class="product_price">$<?= number_format($markup, 2);?></span>
					</span>
				<input type="checkbox" name="accessories[]" value="<?=$check_value?>" id="accessory_<?=$option['id']?>" />


					<span class="accessory_info">
						<?
							echo $option['mfr_part_num'];
							if( $option['product_name'] )
								echo ' - ' . $option['product_name'];
							else
								echo ' - ' . $option['name'];

							if( $option['tagline'] )
								echo ' ' . $option['tagline'];
						?>
					</span>

				</label>

				</li>
				<?
				if( $listing == 'listinga' )
					$listing = 'listingb';
				else
					$listing = 'listinga';
			}
			return $listing;
	}

	static function outputWarranties( $type='' )
	{
		$warranties = Products::get(0,'','','','', 114 );
		if( $type == 'cart' )
		{
			?>
			<table class="optionborders" width="100%" border="0" cellspacing="0" cellpadding="0">

			<?
				foreach( $warranties as $w )
				{
					?>
					<tr class="warrantyoptions">
	          <td>
	            <input type="radio" name="warranties[]" value="<?=$w['id']?>">
	          </td>
	          <td><?=$w['name']?></td>
	          <td>$<?=$w['price']?></td>
	        </tr>
					<?
				}
			?>
      </table>
      <?
		}
		else
		{
			?>

			<?
			foreach( $warranties as $w )
			{
				?>
				<li class="warranty" id="warranty_list">
					<input type="radio" name="warranties[]" value="<?=$w['id']?>" id="warranty_<?=$w['id']?>" />
					<label for="warranty_<?=$w['id']?>"><a href="javascript:void(0)" onclick="javascript:popUp('get_warranty_info.php?id=<?=$w[id]?>')" ><?=$w['name']?></a> - $<?=$w['price']?></label>
				</li>
				<?
				$last = $w['id'];
			}
			echo'<input type="hidden" name="last_warranty" value="' . $last . '" id="last_warranty" />';
		}
		?>
		<script type="text/javascript">
		function popUp(URL) {
			day = new Date();
			id = day.getTime();
			eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=450,height=500,left = 465,top = 262');");
		}
		</script>
		<?
	}

	static function makeBrandLinkByName( $name )
	{
		$brand = Brands::get1ByName( $name );

		return Catalog::makeBrandLink( $brand );
	}

	static function makeContentLink( $page, $no_base = false, $nossl=false )
	{

		global $CFG;

		if($page == 'contact'){
			return self::contactUs();
		}
		if($page == 'blog'){
			return self::blogLink(0);
		}

		if( !is_array( $page ) )
			$page = Content::getByPageId( $page );
		if (!$no_base)
			$link = $CFG->baseurl;
			
		//$link .= 'Content/' . self::_makeLink( $page['id'], $page['name'], true );
        
		if ($nossl && !$no_base)
			$link = $CFG->nosslurl;	
		
		//$link .= 'content.php?id=' . $page['id'];
		$link .= strtolower($page['page_id']) . '.html';

		return $link;
	}
	
	static function makeFilterLink( $filter, $no_base = false, $nossl=false )
	{
	
		global $CFG;

		if( !is_array( $filter ) )
			
			$filter = Filters::get1( $filter );
			
			if (!$no_base)
				$link = $CFG->baseurl;
					
				//$link .= 'Content/' . self::_makeLink( $page['id'], $page['name'], true );
	
				if ($nossl && !$no_base)
					$link = $CFG->nosslurl;
	
					//$link .= 'content.php?id=' . $page['id'];
					$link .= strtolower($filter['url_name']) . '.html';
	
					return $link;
	}
	

	static function contactUs(){
		global $CFG;

		return $CFG->baseurl . "contact_us.php";
	}
	
	static function makeVideoEmbedLink($id='',$video_type='',$video_type_id=''){
		$link = 'https://www.youtube.com/embed/';
				
		$video = db_get1(Youtube::get($id,'','',1,0,false,$video_type,$video_type_id));
		
		if(is_array($video)){
			$link .= $video['url'] . '?rel=0&modestbranding=1';
			return $link;
		}
		
		return false;
	}
	
	static function getVideoThumbnail($id='',$video_type='',$video_type_id='',$thumb_number = '2'){
		$link = 'https://img.youtube.com/vi/';
	
		$video = db_get1(Youtube::get($id,'','',1,0,false,$video_type,$video_type_id));
	
		if(is_array($video)){
			$link .= $video['url'] .'/'. $thumb_number . '.jpg';
			return $link;
		}
	
		return false;
	}
	
	static function getAdditionalvideos($video_type,$video_type_id){
		global $CFG;
		echo "<ul class='additional_videos_ul'>";
		
		$videos = Youtube::get(0,'','',0,0,false,$video_type,$video_type_id,'Y');
		foreach ($videos as $video){			
			$the_link = Catalog::makeVideoEmbedLink($video['id']);
			echo "<li class='additional_videos_li'><a href='#' ><img class='additional_videos_thumb' src='".Catalog::getVideoThumbnail($video['id'])."' alt='' onclick='change_video(this,\"$the_link\"); return false;'/></a></li>";
		}
		echo "</ul>";
		
	}
	
	static function makeProductImageLink( $id, $thumb=true, $filename=false, $ebay=false, $xlarge=false)
	{
		global $CFG;
		$product = Products::get1( $id );
		$name = $product['name'];
		$name = preg_replace('/[^A-z0-9]/s','-',$name);

		if( !$filename ) {

		    /*if( $ebay )
				$ssl = $CFG->baseurl;
		    else
				$ssl = $CFG->sslurl;
		    $server_to_use = (isset( $_SERVER['HTTPS'] )?$ssl."/itempics/":"http://itempics.tigerchef.com/");*/
		    if( $ebay ) $server_to_use = $CFG->sslurl . "itempics/";
			
		    else $server_to_use =  $CFG->itempics_server . "/";
			
			if( $ebay && $product['img_ebay_url'])
			{
				$path = $server_to_use . $product['img_ebay_url'];
			}
		    else if( $thumb && $product['img_thumb_url'])
			    //$path = ($_SERVER['HTTPS']?$ssl:$CFG->baseurl) . 'itempics/' . $product['img_thumb_url'];
			    $path = $server_to_use . $product['img_thumb_url'];
		    else if (!$thumb && !$xlarge && $product['img_lg_url'])
			    $path = $server_to_use . $product['img_lg_url'];
		    else if (!$thumb && $product['img_xlg_url'])
			    $path = $server_to_use . $product['img_xlg_url'];
			    		    
		}
		else {
			if( $ebay && $product['img_ebay_url'])
			{
				$path = $server_to_use . $product['img_ebay_url'];
			}
			else if( $thumb && $product['img_thumb_url'])
				//$path = $CFG->dirroot . '/itempics/' . $product['img_thumb_url'];
				 
				$path = $server_to_use . $product['img_thumb_url'];
			else if (!$thumb && $product['img_xlg_url'])
				$path = $server_to_use . $product['img_xlg_url'];
		}
		
		if ($path)
		{
			/*$path_parts = explode("/", $path, 4);
			if ($ebay)$path_on_server = $CFG->dirroot . $path_parts[3];		
			else $path_on_server = $CFG->dirroot . '/itempics/' . $path_parts[3];*/
			$path_parts = explode("/", $path, 5);
			if ($ebay || $path_parts[4])$path_on_server = $CFG->dirroot . '/itempics/' . $path_parts[4];		
			else $path_on_server = $CFG->dirroot . '/itempics/' . $path_parts[3];
		}
       //if (!$path || (!file_exists($path_on_server) && strpos($path, "itempics.tigerchef.com")===false))
       //mail("rachel@tigerchef.com", $path, $path_on_server);
        
       if (!$path || (!file_exists($path_on_server)))
       {
       	//mail("rachel@tigerchef.com", " in the block" . $path, $path_on_server);
       	if ($thumb) $path =$CFG->images_server . '/' . $CFG->no_image_thumbnail_name;
       	else $path =$CFG->images_server . '/' . $CFG->no_image_name;
       }
		return $path;
	}
			


	static function makeCategoryImageLink($id, $thumb=true, $homepage=false )
	{
	    global $CFG;

	    $path = "";
		$cat = Cats::get1($id);
	    if ($thumb)
	    {
	       $file_path = $CFG->dirroot . '/catpics/' . $cat['catpic_url'];

	       if (file_exists($file_path))
	       {
	       	   $ssl = $CFG->sslurl;
			   $path = $CFG->catpics_server . "/" . $cat['catpic_url'];
	       }
	       else
	       {
	           $path = $CFG->catpics_server . "/" . 'default.jpg';
	       }
	    } else {
	    /**
	     * There was no option to follow if thumb was passed as false.
	     */
		//2 Image versions currently, thumb and featured

		if( $homepage )
		{
			$field_name = 'catpic_hp_url';
		}
		else {
			$field_name = 'catpic_featured_url';
		}

		$file_path = $CFG->dirroot . '/catpics/' . $cat[$field_name];
			
		if(file_exists($file_path))
		{
		    $ssl = $CFG->sslurl;
			$path = $CFG->catpics_server . "/" . $cat[$field_name];
		}
		else
	       	{
	           $path = $CFG->catpics_server . "/" . 'default.jpg';
	       	}
	    }

	    return $path;
	}

	static function makeCategoryLargeImageLink($id, $thumb=true, $homepage=false )
	{
	    global $CFG;

	    $path = "";

	    $cat = Cats::get1($id);
	    
	    if ($thumb)
	    {
	       $file_path = $CFG->dirroot . '/catpics/' . $cat['catpic_large_url'];

	       if (file_exists($file_path))
	       {
	           //$path = 'http://catpics.tigerchef.com/' . $id . $CFG->catpic_large_suffix;
	           $path = $CFG->catpics_server . "/" . $cat['catpic_large_url'];
	       }
	       else
	       {
	              $path = $CFG->catpics_server . "/" . 'default.jpg';
	       }
	    } else {
	    /**
	     * There was no option to follow if thumb was passed as false.
	     */
		//2 Image versions currently, thumb and featured

		if( $homepage )
		{
		    $field_name = 'catpic_hp_url';
		}
		else {
		    $field_name = 'catpic_large_url';
		}

		$file_path = $CFG->dirroot . '/catpics/' . $cat[$field_name];
			
		if(file_exists($file_path))
		{
		    $ssl = $CFG->sslurl;
			$path = $CFG->catpics_server . "/" . $cat[$field_name];
		}
		else
	       	{
	           $path = $CFG->catpics_server . "/" .'default.jpg';
	       	}
	    }

	    return $path;
	}
	
	static function makeCategoryMediumImageLink($id, $thumb=true, $homepage=false )
    {
        global $CFG;

        $path = "";
        $cat = Cats::get1($id);

        if ($thumb)
        {
           $file_path = $CFG->dirroot . '/catpics/' . $cat['catpic_medium_url'];

           if (file_exists($file_path) && $cat['catpic_medium_url'] != '')
           {
               //$path = 'http://catpics.tigerchef.com/' . $id . $CFG->catpic_medium_suffix;
               $path = $CFG->catpics_server . "/" . $cat['catpic_medium_url'];
           }
           else
           {
               //$path = $CFG->protocol_relative_url . 'catpics/default.jpg';
               $path = $CFG->protocol_relative_url . "images/".$CFG->no_image_thumbnail_name;
           }
        } else { 
	    /**
	     * There was no option to follow if thumb was passed as false.
	     */
		//2 Image versions currently, thumb and featured

		if( $homepage )
		{
		    $field_name = 'catpic_hp_url';
		}
		else {
		    $field_name = 'catpic_medium_url';
		}

		$file_path = $CFG->dirroot . '/catpics/' . $cat[$field_name];
			
		if(file_exists($file_path))
		{
		    $ssl = $CFG->sslurl;
			$path = $CFG->catpics_server . "/" . $cat[$field_name];
		}
		else
	       	{
	             $path = $CFG->catpics_server . "/" . 'default.jpg';
	       	}
	    }

	    return $path;
	}
	
	static function makeCategoryIconImageLink($id)
	{
	    global $CFG;

	    $path = "";
	    
	    $cat = Cats::get1($id);
		
		$file_path = $CFG->dirroot . '/catpics/' . $cat['catpic_icon_url'];
			
		if(file_exists($file_path))
		{
			$ssl = $CFG->sslurl;
			$path = $CFG->catpics_server . "/" . $cat['catpic_icon_url'];		    
		}
		else
	    {
	           $path = '';
	    }
	    
	    
	    return $path;
	}
	static function makeCategoryBusinessTopImageLink($id)
	{
	    global $CFG;

	    $path = "";
	    
		$cat = Cats::get1($id);
		
		$file_path = $CFG->dirroot . '/catpics/' . $cat['catpic_business_top_url'];
			
		if(file_exists($file_path))
		{
			$ssl = $CFG->sslurl;
			$path = $CFG->catpics_server . "/" . $cat['catpic_business_top_url'];
		}
		else
	    {
	           $path = '';
	    }
	    
	    
	    return $path;
	}
	
	static function makeHomepageTabImageLink($id)
	{
	    global $CFG;

	    $path = "";
	    
		$suffix = $CFG->tab_icon_suffix;
		
		$file_path = $CFG->dirroot . '/homepage_images/' . $id . $suffix;
			
		if(file_exists($file_path))
		{
		    //$path = $CFG->baseurl . 'homepage_images/' . $id . $suffix;
		    $path = $CFG->homepageimages_server.'/' . $id . $suffix;
		}
		else
	    {
	           $path = '';
	    }
	    
	    return $path;
	}
		
	function getAdditionalPics($product_id){
		global $CFG;
		
		$product_info = Products::get1($product_id);
	    
		echo "<ul class='additional_img_ul'>";
		
		$additional_img_list = glob($CFG->dirroot . "/additional_pics/" . $product_id . '_*' . $CFG->thumbnail_suffix);
		//print_ar($additional_img_list);
		
		if($additional_img_list){
			//main image thumbnail
			echo "<li class='additional_img_li' ><a href='#'><img class='additional_img_thumb main_image' src='" .Catalog::makeProductImageLink( $product_id, true) ."' alt='' onclick='change_image(this,\"" . Catalog::makeProductImageLink( $product_id, false)."\"); return false;' /></a></li>";
		}
		
		//additional images
		for($i=1;$i<=9;$i++){
			$thumb_file_path = $CFG->dirroot . "/additional_pics/" . $product_id . "_" . $i . $CFG->thumbnail_suffix;
			$thumb_file_name = $CFG->additionalpics_server . "/" . $product_id . "_" . $i . $CFG->thumbnail_suffix;
			
			$xlarge_file_path = $CFG->dirroot . "/additional_pics/" . $product_id . "_" . $i . $CFG->xlarge_suffix;
			$xlarge_file_name = $CFG->additionalpics_server . "/" . $product_id . "_" . $i . $CFG->xlarge_suffix;
	
			$img_to_use = file_exists($xlarge_file_path) ? $xlarge_file_name : $thumb_file_name ;
			
			if($product_info['add'.$i.'_option_color']){
				$option_color = 'option_color_' . $product_info['add'.$i.'_option_color'];
			}
			
			if(file_exists($thumb_file_path)){
				echo "<li class='additional_img_li'><a href='#' ><img class='additional_img_thumb " . $option_color . "' src='$thumb_file_name' alt='' onclick='change_image(this,\"$img_to_use\"); return false;'/></a></li>";
			}
		}
		echo "</ul>";
	}
	
	function getAdditionalPicsList($product_id){
		global $CFG;
		return Products::getAdditionalImagesInfo($product_id);
	}
	
	function getAdditionalPicsRedesign($product_id){
		global $CFG;
	
		$product_info = Products::get1($product_id);
		
		//$additional_img_list = glob($CFG->dirroot . "/additional_pics/" . $product_id . '_*' . $CFG->thumbnail_suffix);
		$additional_img_list = Catalog::getAdditionalPicsList($product_id);
		
		if($additional_img_list){
			echo "<div id='zoom-gallery'>";
			
				//main image thumbnail
				$image_link = Catalog::makeProductImageLink( $product_id, false );
				$xlarge_image_link = Catalog::makeProductImageLink( $product_id, false, false, false, true);
				echo "<a class='active main_image' href='#' data-image='$xlarge_image_link' data-zoom-image='$xlarge_image_link' onclick='return change_image(this)'>
						<img class='img_zoom' itemprop='image' src='" .Catalog::makeProductImageLink( $product_id, true) ."' alt='' />
					</a>";
				
				foreach($additional_img_list as $image_info){

					$thumb_file_name = $CFG->additionalpics_server ."/". $image_info['image_url'] . $CFG->thumbnail_suffix;

					$large_file_path = $CFG->dirroot . "/additional_pics/" . $image_info['image_url'] . $CFG->large_suffix;
					$large_file_name = $CFG->additionalpics_server ."/". $image_info['image_url'] . $CFG->large_suffix;
					
					$xlarge_file_path = $CFG->dirroot . "/additional_pics/" . $image_info['image_url'] . $CFG->xlarge_suffix;
					$xlarge_file_name = $CFG->additionalpics_server ."/". $image_info['image_url'] . $CFG->xlarge_suffix;
					
					if($image_info['option_color']){
						$option_color = 'option_color_' . $image_info['option_color'];
					}else{
						$option_color = '';
					}
					
					echo "<a href='#' data-image='$large_file_name' data-zoom-image='$xlarge_file_name' onclick='return change_image(this)'>
					<img class='img_zoom' itemprop='image' id='$option_color' src='$thumb_file_name' alt=''/>
					</a>";

				}
			echo "</div>";
		} else {
			$xlarge_file_name = Catalog::makeProductImageLink( $product_id, false, false, false, true);
			echo "<div id='zoom-gallery'><a style='display:none;' href='#' data-image='$xlarge_file_name' data-zoom-image='$xlarge_file_name'><img class='img_zoom'></img></a></div>";
		}
	}
	function getMobilePics($product_id){
		global $CFG;
	
		$product_info = Products::get1($product_id);
		$image_link = Catalog::makeProductImageLink($product_id,false,false,false,true);
	
		$additional_img_list = glob($CFG->dirroot . "/additional_pics/" . $product_id . '_*' . $CFG->xlarge_suffix);
		?>
		<!-- <a class="fancybox" rel="mobile-gallery" href="<?=$image_link?>" title=""> -->
			<img src="<?=$image_link?>" alt="<?=$product_info['name']?>" />
		<!-- </a> -->
		<?php 
	
		if($additional_img_list){
			foreach ($additional_img_list as $xlarge_file_path){

				preg_match("/\\d+_(\\d+)" . $CFG->xlarge_suffix . "/", $xlarge_file_path, $index_array);
				if($index_array){
					$index = $index_array[1];
			
					$xlarge_file_name = $CFG->additionalpics_server . "/" . $product_id . "_" . $index . $CFG->xlarge_suffix;
						
					if($product_info['add'.$index.'_option_color']){
						$option_color = 'option_color_' . $product_info['add'.$index.'_option_color'];
					}
													
					/*								
					?>
 					<a class="fancybox" rel="mobile-gallery" href="<?=$xlarge_file_name?>" title=""></a>
 					<?php 
					*/  	
				}
			}
		}
	}
	
	static function blogLink($blog_row_or_id=0){
		global $CFG;

		if(is_numeric($blog_row_or_id)){
			$blog = Blog::get1($blog_row_or_id);
		} else if(is_array($blog_row_or_id)){
			$blog = $blog_row_or_id;
		}

		if(!$blog['url']){
			return $CFG->baseurl . "blog/";
		}

		return $CFG->baseurl . "blog/" . getRewriteUrl($blog['url']) . "/" . $blog['id'];
//		return $CFG->baseurl . "blog.php?id=" . $blog['id'];
	}

	static function blogCategoryLink($row_or_id){
		global $CFG;

		if(is_numeric($row_or_id)){
			$cat = BlogCat::get1($row_or_id);
		} else if(is_array($row_or_id)){
			$cat = $row_or_id;
		}

		if(!$cat['id']){
			return "#";
		}

		return $CFG->baseurl . "blog/category/" . getRewriteUrl($cat['name']) . "/" . $cat['id'];
		// return $CFG->baseurl . "blog.php?cat_id=" . $cat['id'];
	}

	static function blogAuthorLink($row_or_id){
		global $CFG;

		if(is_numeric($row_or_id)){
			$author = db_get1( Blog::getAuthors($row_or_id) );
		} else if(is_array($row_or_id)){
			$author = $row_or_id;
		}

		if(!$author['author_id']){
			return "#";
		}

		return $CFG->baseurl . "blog/author/" . getRewriteUrl($author['author_name']) . "/" . $author['author_id'];
		// return $CFG->baseurl . "blog.php?author_id=" . $author['author_id'];
	}

	static function blogTagLink($tag){
		global $CFG;

		return $CFG->baseurl . "blog/tag/" . getRewriteUrl($tag) . "/";
	}

	static function fixJSstring($str){
		$str = str_replace(array("'",'"'),'',$str);

		return $str;
	}

	static function myAccount(){
		global $CFG;

		return $CFG->baseurl . "account.php";
	}

	static function createAccount(){
		global $CFG;

		return $CFG->baseurl . "account.php?action=new_user";
	}
}
function array_values_recursive($ary)
{
	$lst = array();
	foreach( array_keys($ary) as $k ){
		$v = $ary[$k];
		if (is_scalar($v)) {
			$lst[] = $v;
		} elseif (is_array($v)) {
			$lst = array_merge( $lst,
					array_values_recursive($v)
			);
		}
	}
	return $lst;
}
function array_keys_multi(array $array)
{
	$keys = array();

	foreach ($array as $key => $value) {
		$keys[] = $key;

		if (is_array($array[$key])) {
			$keys = array_merge($keys, array_keys_multi($array[$key]));
		}
	}

	return $keys;
}
?>