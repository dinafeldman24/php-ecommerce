<?php

class Cats {

	static function insert($info)
	{
		// Generate the url_name, unless it was provided on the cat insert form
		if (!(isset($info['url_name']) && $info['url_name'] != "")) {
			$info['url_name'] = trim(strtolower($info['name']));
		}
		
		// Handle special characters and whitespace in the url name
		$info['url_name'] = str_replace( "&", "and", $info['url_name'] );
		$info['url_name'] = str_replace( "/", "-", $info['url_name'] );
		
		$info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
		$info['url_name'] = str_replace(' ','-',$info['url_name']);
		
		return db_insert('cats',$info,'date_added');
	}

	static function update($id,$info)
	{
		return db_update('cats',$id,$info);
	}

	static function delete($id)
	{
		//Need to also delete any thing that has this cat as a parent

		db_delete( 'cats', $id, 'pid' );

		return db_delete('cats',$id);
	}

	//This function will get all the sub cats... as well as all of the symbolic cats
	static function getSubCats( $cat_id )
	{
	  //  print_ar( $cat_id );
	    $subcats = Cats::get(0,'','','order_fld ASC, name','',$cat_id,'','','N','','','id');
//print_ar( $subcats );
	    $symbolic_cats = Cats::getSymbolicSubs( $cat_id, 'name' );

	    if( !$subcats )
		$subcats = array();

	    if( !$symbolic_cats )
		$symbolic_cats = array();

	  //  print_ar( $symbolic_cats );
	    $cats = array_merge($subcats, $symbolic_cats );
	    usort($cats, array("Cats","cmp"));
	    //print_ar( $cats );

	    return $cats;
	}
	static function cmp($a, $b)
	{
    	return strcmp($a['name'], $b['name']);
	}
	
	static function get($id=0, $begins_with='',$keywords='',$order='',$order_asc='',
	$parent_id='',$prod_ids='', $link_to_cat = '', $is_hidden = '', $name='', $url_name='', $key='')
	{
		if (is_array($prod_ids)) {
			$prod_join = ',product_cats';
		}
		else {
			$prod_join = '';
		}

			$sql = "SELECT cats.*, parent_cats.name AS parent_name
					FROM cats
						LEFT JOIN cats AS parent_cats ON (parent_cats.id = cats.pid)
					$prod_join
					WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND cats.id = $id ";
		}
		if ($begins_with != '') {
			$sql .= " AND TRIM(cats.name) LIKE '".addslashes($begins_with)."%'";
		}

		if ($name != '') {
			$sql .= " AND cats.name LIKE '".addslashes($name)."'";
		}
		if( $url_name !='' )
		{
			$sql .= " AND cats.url_name LIKE '".addslashes($url_name)."'";
		}

		if ($keywords != '') {
			$fields = Array('cats.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}

		if (is_numeric($parent_id)) {
			$sql .= " AND cats.pid = $parent_id ";
		}

		if ($is_hidden != '') {
			$sql .= " AND cats.is_hidden = '$is_hidden' ";
		}

		/*
		else
		{
		    $sql .= " AND cats.is_hidden = 'N' ";
		}
		*/

		if (is_array($prod_ids)) {
			$sql .= " AND product_cats.cat_id = cats.id AND product_cats.product_id IN (".implode(',',$prod_ids).") ";
		}

		if ($link_to_cat) {
			$sql .= db_restrict('link_to_cat', $link_to_cat);
		}

		$sql .= " GROUP BY cats.id
				  ORDER BY ";

		if ($order == '') {
			$sql .= 'cats.order_fld';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

		$sql .= ', cats.name';
		//echo $sql . '<br />';
//		print_ar( $key );
		return db_query_array($sql,$key);
	}

	static function cats2Structure($cats,$pid=0)
	{
		if (is_array($cats)) {

			$retCats = Array();
			$x=0;

			foreach ($cats as $key => $cat) {
				if ($cat['pid'] == $pid) {

					$retCats[$x] = $cat;
					$retCats[$x]['children'] = Cats::cats2Structure($cats,$cat['id']);

					++$x;
				}
			}


			return $retCats;
		}
	}

	/**
	 * Trim category names (remove spaces)
	 *
	 */
	static function fixCatNames(){
		db_query("UPDATE cats SET name = TRIM(name) WHERE 1");
		db_query("UPDATE cats SET h1 = TRIM(h1) WHERE 1");
	}

	// sometimes we'll get a subcategory in the tree, but not the parent (the whole tree).
	// take care of it ...
	static function fixCats($flat_cats)
	{
		for(;;) {

			$perfect_tree = true;

			if ($flat_cats) foreach ($flat_cats as $fc) {
				if ($fc['pid'] && !$flat_cats[$fc['pid']]) {
				  $perfect_tree = false;
				  $flat_cats[$fc['pid']] = Cats::get1($fc['pid']);
				}
			}

			//$flat_cats = array_merge($flat_cats, $fc_add);

			//print_ar($fc_add);
			//print_ar($flat_cats);

			if ($perfect_tree)
			  return $flat_cats;

		}
	}
	static function search($keywords)
	{
		$fields = Array('cats.name', 'cats.cat_keywords');
		$weights = array ('cats.name' => 1000);

		$sql = "SELECT cats.*
				FROM cats
				WHERE 1 ";

		$obi = null;

                $sql .= " AND cats.is_hidden = 'N' ";

		$sql .= " AND " . Search::buildFullText($keywords, $fields, true, $obi, $weights);

                

		if ($obi) {
			$sql .= " ORDER BY $obi DESC";
		}

	//	echo $sql;

		return db_query_array($sql, 'id');

	}


	static function getAllCatsAsStructure()
	{
		$cats = Cats::get();

		return Cats::cats2Structure($cats);
	}

	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Cats::get($id);
		return $result[0];
	}

	static function getCatsTree($pid, $cat_id_key = false)
	{
		do {
			$cat_info = Cats::get1($pid);
			$cat_info['level'] = Cats::getCatDepth( $cat_info['id'] );
			if( $cat_id_key )
			    $ret[$cat_info['id']] = $cat_info;
			else
			    $ret[] = $cat_info;
			$pid = $cat_info['pid'];
		}
		while ($pid > 0);
		if( $cat_id_key )
		    return array_reverse($ret, true);
		else
		    return array_reverse($ret);
	}

	static function getCatsTreeLevels($pid)
	{
		$level = 1;
		do {
			$cat_info = Cats::get1($pid);

			$ret[$level]['value'] = $cat_info['id'];
			$ret[$level]['level'] = $level;
			$ret[$level]['name'] = $cat_info['name'];
			$ret[$level]['backend_mark'] = $cat_info['backend_mark'];
			$ret[$level]['frontend_mark'] = $cat_info['frontend_mark'];
			$pid = $cat_info['pid'];
			$level++;
		}
		while ($pid > 0);

		return array_reverse($ret);
	}

	static function getCatsTreeIDs($pid)
	{
		do {
			$cat_info = Cats::get1($pid);
			$ret[$cat_info['name']] = $cat_info['id'];
			$pid = $cat_info['pid'];
			$level++;
		}
		while ($pid > 0);

		return array_reverse($ret);
	}

static function _cats2pulldown(&$cats, $selected_cat, $level=0)
{

	// if it goes any further, i guess it's gonna have to be black.
	$levels = array('#000000', '#7B0202', '#B70404', '#BF0202', '#EB0606');

	$ret = '';

	if (is_array($cats)) {
		foreach ($cats as $cat) {

     $style = "style='color:$levels[$level]'";

			$ret .= "<option value='$cat[id]' $style name='$name' ";

			if ($selected_cat == $cat['id'])
			  $ret .= ' selected ';

			$ret .= ">";
			$ret .= str_repeat('&nbsp;&nbsp;',$level);
			$ret .= $cat['name'];

			$ret .= Cats::_cats2pulldown($cat['children'],$selected_cat,$level+1, $name);

			}
	}

	return $ret;
}

static function cats2pulldown(&$cats, $selected_cat, $level=0, $name = 'filtercat', $first_blank = true, $width = '', $onchange = '', $msg ='')
{
	if($width) $style = " style='width:{$width}px'";
	if($onchange) $onchange = " onchange='$onchange'";
	$ret = "<select name='$name' $style $onchange>";

	if ($first_blank)
	  $ret .= "<option value=0>$msg";

	$ret .= Cats::_cats2pulldown($cats, $selected_cat, $level=0);
	$ret .= "</select>";
	return $ret;
}

// definition: a cat is a child of itself by definition -- js
static function isCatChildOfCat($cat_id, $parent_cat_id)
{
  if ($cat_id == $parent_cat_id) {
    return true;
  } else if ($cat_id == 0) {
  	return false;
  } else {
  	// would be better to cache the list, but not worth it.
  	$info = Cats::get1($cat_id);
  	return Cats::isCatChildOfCat($info['pid'], $parent_cat_id);
  }
}

// this could be written faster. if it's too slow, let's rewrite it.
static function getCatDepth($cat_id)
{
	$info = Cats::get1($cat_id);
	  if ($info['pid'] == 0) {
		return 0;
	  } else {
		return 1 + Cats::getCatDepth($info['pid']);
	  }
}

//Need a recurisve static function to print out the dynamic category landing page
static function outputUL( $cat_id, $level=1, $new_list=true )
{
	$cat = self::get1($cat_id);
	//echo("<li><strong><a href=\"".Catalog::makeCatLink($cat['id'])."\">".$cat['name']."</a></strong></li>\n");
	$subs = self::get(0, '', '', 'name', '', $cat_id );
	if( is_array( $subs ) && count($subs) > 0 )
	{
		foreach( $subs as $sub )
		{
			echo'<li>';
			if($level==1)
			{
				echo'<strong>';
			}
			//echo $sub['id'];
			echo'<a href="'.Catalog::makeCatLink($sub).'">' . $sub['name'] . '</a>';
			if( $level==1 )
			{
				echo'</strong>';
			}
			$next = self::get(0, '', '', 'name', '', $sub['id'] );
			if( is_array($next) && count($next) > 0)
			{
				if( $new_list )
				{
					echo'<ul>';
				}
						self::outputUL($sub['id'], $level + 1, $new_list);
				if( $new_list )
				{
					echo'</ul>';
				}
			}
			echo'</li>';
		}
	}
}

static function getAllSubs( $cat_id, $cat_array='', $backend = false)
{
    global $CFG;

//	if( $backend )
//	{
//		$handle = fopen( "../apps/includes/cat_array.php", "r" );
//		$content = fread( $handle, filesize("../apps/includes/cat_array.php"));
//		fclose( $handle );
//	}
//	else
//	{
		$handle = fopen( $CFG->dirroot . "/apps/includes/cat_array.php", "r" );
		$content = fread( $handle, filesize($CFG->dirroot ."/apps/includes/cat_array.php"));
		fclose( $handle );
//	}

	$cat_array = unserialize( $content );
	$cat_array = array_shift( $cat_array );
	//print_ar( $cat_array );
	$tree = Cats::getCatsTreeIDs( $cat_id );
	//print_ar( $tree );

	$sub_array = $cat_array;

	if( !$cat_id )
	{
		return $sub_array;
	}

	foreach( $tree as $t )
	{
		$sub_array = $sub_array[$t];
		//print_ar( $sub_array );
	}
	if( !is_array( $sub_array ) )
		$sub_array = array( $sub_array );

	$sub_array[$cat_id] = $cat_id;
	//print_ar( $sub_array );

	return $sub_array;
}

static function getAllSubsStructure( $cat_id, $cat_array )
{
	$cats = Cats::get(0,'','','','',$cat_id);

	if( is_array( $cats ) )
	{
		foreach( $cats as $cat )
		{
			//echo' checking ' . $cat['id'] . '<br />';
			$next = Cats::get(0,'','','','',$cat['id']);

				if( is_array( $next ) )
				{
					$cat_array[$cat_id] = self::getAllSubsStructure( $cat['id'], $cat_array[$cat_id] );
				}
				else
				{
					$cat_array[$cat_id][$cat['id']] = $cat['id'];
				}
		}
	}

	//print_ar( $cat_array );
	return $cat_array;
}

	static function mergeCatTrees( $merged, $tree )
	{
		if( empty( $merged ) )
			return $tree;

		$level=array();
		$name=array();
		$merge_level=array();
		$merge_name=array();
		$backend = array();
		$frontend = array();

		foreach( $merged as $row )
		{
			$merge_level[$row['value']] = $row['level'];
			$merge_name[$row['value']] = $row['name'];

		}
		foreach( $tree as $row )
		{
			$level[$row['value']] = $row['level'];
			$name[$row['value']] = $row['name'];
			$backend[$row['value']] = $row['backend_mark'];
			$frontend[$row['value']] = $row['frontend_mark'];
		}

		foreach( $level as $key => $value )
		{
			$merge_level[$key] = ($merge_level[$key] < $level[$key] )?$level[$key]:$merge_level[$key];
			$merge_name[$key] = $name[$key];
		}
		$ret = array();
		foreach( $merge_level as $key => $value )
		{
			$ret[] = array( 'value' => $key, 'level' => $value, 'name' => $merge_name[$key], 'backend_mark' => $backend[$key], 'frontend_mark' => $frontend[$key] );
		}

		return $ret;
	}

	static function getFilterSearchCats( $cats )
	{

			$main_tree = array();
			if( is_array( $cats ) )
				foreach( $cats as $id )
				{
					$tree = Cats::getCatsTreeLevels( $id );
					$main_tree = Cats::mergeCatTrees( $main_tree, $tree );
				}

			$tree = $main_tree;
			//print_ar( $tree );
			$search_cats = array();

			foreach( $tree as $row )
			{
				if( $row['level'] == 1 )
					$search_cats[] = $row['value'];
			}
			return $search_cats;
	}

	static function getCatByName( $name, $exact = false )
	{
		if( $exact )
			$cats = Cats::get( 0, '','','','','','','','', $name );
		else
			$cats = Cats::get( 0, $name );
		if( count( $cats ) > 1 )
			echo'error, more than 1 match ' . $name;

		$ret = $cats[0];

		return $ret;
	}

	static function buildCatsTree( $cats )
	{
			$main_tree = array();
			foreach( $cats as $id )
			{
				$tree = Cats::getCatsTreeLevels( $id );
				$main_tree = Cats::mergeCatTrees( $main_tree, $tree );
			}

			$tree = $main_tree;
			unset( $main_tree );
			return $tree;
	}

	static function getCatsForBrand( $brand_id )
	{
		$sql  = " SELECT distinct product_cats.cat_id FROM products ";
		$sql .= " LEFT JOIN product_cats on product_cats.product_id = products.id ";
		$sql .= " WHERE products.brand_id = $brand_id ";

		$result = db_query_array( $sql );

		$cat_ids = array();
		$sql = " SELECT * from cats WHERE ";
		if( $result )
			foreach( $result as $r )
			{
				$cat_ids[] = $r['cat_id'];
			}
			$cat_list = implode("','", $cat_ids);
			$cat_list = rtrim( $cat_list, ",'");
			$sql .= " id IN ('". $cat_list ."')";

		return $cats = db_query_array( $sql );
	}

	static function getBrandsForCat( $cat_id )
	{
		$sql  = " SELECT distinct products.brand_id, brands.name FROM products ";
		$sql .= " LEFT JOIN product_cats on product_cats.product_id = products.id ";
		$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$sql .= " WHERE product_cats.cat_id = $cat_id ";

		return db_query_array( $sql );
	}

	static function getParentCatForProduct($product_id)
	{
	    $product_id = (int) $product_id;
	    if (!$product_id)
	    {
	       return false;
	    }

	    $sql = "SELECT product_cats.cat_id
	            FROM product_cats
	            WHERE product_cats.product_id = $product_id";

	    $results = db_query_array($sql);
	    return $results[0];
	}

	static function getCatsForProduct($product_id, $key=''){
		$product_id = (int) $product_id;
		if(!$product_id){
			return false;
		}
		$sql  = "SELECT cats.* FROM product_cats
				LEFT JOIN cats ON product_cats.cat_id = cats.id
				WHERE product_cats.product_id = $product_id AND cats.name IS NOT NULL";
		//echo $sql;
		return db_query_array($sql, $key);
	}

	static function hasCategory($product_id){
		$product_id = (int) $product_id;
		if(!$product_id){
			return false;
		}
		$sql  = "SELECT product_cats.* FROM product_cats
				WHERE product_cats.product_id = $product_id";

		$arr = db_query_array($sql);
		if(!$arr || empty($arr)){
			return false;
		}
		else{
			return true;
		}
	}

	function get1ByName( $name, $pid='' )
	{
		$result = Cats::get(0, '', '', '', '', $pid, '', '', '', $name );
		return $result[0];
	}

	function get1ByURLName( $name )
	{
		$result = Cats::get(0, '', '', '', '', $pid, '', '', '', '', $name );
		return $result[0];
	}

	static function updateURLName( $id )
	{
		$cat = Cats::get1( $id );
		//echo'Old Url Name = ' . $cat['url_name'];
		$info['url_name'] = trim($cat['name']);

		$info['url_name'] = str_replace( "&", "and", $info['url_name'] );
		$info['url_name'] = str_replace( "/", "-", $info['url_name'] );

		$info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
		$info['url_name'] = str_replace(' ','-',$info['url_name']);
		//echo ', new url_name = ' . $info['url_name'] . '<br />';
		return db_update( 'cats', $id, $info );
	}

	static function getSymbolicCats( $cat_id, $key='id' )
	{
	    global $CFG;


	    $cat_id = (int) $cat_id;
	    if(!$cat_id){
		    return false;
	    }
	    $sql  = "SELECT cats.* FROM symbolic_cats
		     LEFT JOIN cats ON symbolic_cats.pid = cats.id WHERE 1 ";

	    $sql .= " AND symbolic_cats.cat_id = $cat_id and cats.id is not null";
	    //echo $sql;
	    return db_query_array($sql,$key);
	}

	static function getSymbolicSubs( $pid, $key='id' )
	{
	    global $CFG;

	    $pid = (int) $pid;
	    if(!$pid){
		    return false;
	    }
	    $sql  = "SELECT cats.* FROM symbolic_cats
		     LEFT JOIN cats ON symbolic_cats.cat_id = cats.id WHERE 1 ";

	    $sql .= " AND symbolic_cats.pid = $pid";
	    //echo $sql;
	    return db_query_array($sql,$key);
	}

	static function associateSymbolic( $cat_id, $cat_ids )
	{
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";

		if (trim($cat_ids_str) == '') {
			db_delete('symbolic_cats',$cat_id,'cat_id');
			return true;
		}

		// delete old ones

		db_query("DELETE FROM symbolic_cats WHERE cat_id = $cat_id AND pid NOT IN ($cat_ids_str)");

		// insert new ones

		$info = Array('cat_id'=>$cat_id);

		foreach ($cat_ids as $pid) {
			$info['pid'] = $pid;
			db_insert('symbolic_cats',$info,'',true);
		}

		return true;
	}

	static function findDuplicateURLs(){
		$sql = "SELECT * FROM (
				SELECT COUNT( * ) AS  `rows` ,  `url_name` , GROUP_CONCAT(DISTINCT cats.id SEPARATOR ',') as ids
				FROM  `cats`
				GROUP BY  `url_name`
				ORDER BY  `Rows` DESC
				) inner1 WHERE rows > 1 ";

		return db_query_array($sql);
	}
	
	static function getPopular($order='id', $sort='DESC', $limit='8')
	{
		global $CFG;

		$o = (object) array();

		$o->order = "id";
		if( $sort == 'ASC' )
		    $o->order_asc = true;
		$o->limit = 8;
		$o->is_popular = 'Y';

		return Cats::getByO($o);
	}
	
	public static function getParentCats($id)
	{
		$catId = $id;
		
		$sql = sprintf(
			"SELECT id 
			FROM cats 
			WHERE pid = '%u' 
			ORDER BY id ASC",
			$catId);
			
		$query = db_query($sql);
		
		$parents = array();
		
		while($result = db_fetch_array($query, MYSQL_ASSOC))
			$parents = array(
				'id'	=> $result['id'],
				'name'	=> $result['name'],
			);
		
		return $parents;
		
		
	}

	/**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT cats.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(cats.id)) as total ";
		}

		$from_ = " FROM cats ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND cats.id = '$o->id' ";
		}

		if( isset( $o->name )  ){
			$where_ .= " AND `cats`.name = [name]";
		}
		if( isset( $o->url_name )  ){
			$where_ .= " AND `cats`.url_name = [url_name]";
		}
		if( isset( $o->pid )  ){
			$where_ .= " AND `cats`.pid = '$o->pid'";
		}
		if(is_array($o->date_added)){
			if($o->date_added['start']){
				$o->date_added_start = $o->date_added['start'];
				$where_ .= " AND `cats`.date_added >= [date_added_start]";
			}
			if($o->date_added['end']){
				$o->date_added_end = $o->date_added['end'];
				$where_ .= " AND `cats`.date_added <= [date_added_end]";
			}
		} else 		if( isset( $o->date_added )  ){
			$where_ .= " AND `cats`.date_added = [date_added]";
		}
		if( isset( $o->order_fld )  ){
			$where_ .= " AND `cats`.order_fld = [order_fld]";
		}
		if( isset( $o->show_size_chart )  ){
			$where_ .= " AND `cats`.show_size_chart = [show_size_chart]";
		}
		if( isset( $o->cat_desc )  ){
			$where_ .= " AND `cats`.cat_desc = [cat_desc]";
		}
		if( isset( $o->cat_keywords )  ){
			$where_ .= " AND `cats`.cat_keywords = [cat_keywords]";
		}
		if( isset( $o->disclaimer )  ){
			$where_ .= " AND `cats`.disclaimer = [disclaimer]";
		}
		if( isset( $o->link_to_cat )  ){
			$where_ .= " AND `cats`.link_to_cat = [link_to_cat]";
		}
		if( isset( $o->override_link )  ){
			$where_ .= " AND `cats`.override_link = [override_link]";
		}
		if( isset( $o->is_popular )  ){
			$where_ .= " AND `cats`.is_popular = [is_popular]";
		}
		if( isset( $o->is_hidden )  ){
			$where_ .= " AND `cats`.is_hidden = '$o->is_hidden'";
		}
		if( isset( $o->nextag_category )  ){
			$where_ .= " AND `cats`.nextag_category = [nextag_category]";
		}
		if( isset( $o->pricegrabber_category )  ){
			$where_ .= " AND `cats`.pricegrabber_category = [pricegrabber_category]";
		}
		if( isset( $o->shopping_com_category )  ){
			$where_ .= " AND `cats`.shopping.com_category = [shopping.com_category]";
		}
		if( isset( $o->shopzilla_category )  ){
			$where_ .= " AND `cats`.shopzilla_category = [shopzilla_category]";
		}
		if( isset( $o->become_category )  ){
			$where_ .= " AND `cats`.become_category = [become_category]";
		}
		if( isset( $o->smarter_category )  ){
			$where_ .= " AND `cats`.smarter_category = [smarter_category]";
		}
		if( isset( $o->frontend_mark )  ){
			$where_ .= " AND `cats`.frontend_mark = [frontend_mark]";
		}
		if( isset( $o->backend_mark )  ){
			$where_ .= " AND `cats`.backend_mark = [backend_mark]";
		}
		if( isset( $o->h1 )  ){
			$where_ .= " AND `cats`.h1 = [h1]";
		}
		if( isset( $o->ebay_cat_id )  ){
			$where_ .= " AND `cats`.ebay_cat_id = [ebay_cat_id]";
		}
		if( isset( $o->meta_desc )  ){
			$where_ .= " AND `cats`.meta_desc = [meta_desc]";
		}
		if( isset( $o->ebay_cat_id2 )  ){
			$where_ .= " AND `cats`.ebay_cat_id2 = [ebay_cat_id2]";
		}
		if( isset( $o->ebay_store_cat_id )  ){
			$where_ .= " AND `cats`.ebay_store_cat_id = [ebay_store_cat_id]";
		}
		if( isset( $o->ebay_store_cat_id2 )  ){
			$where_ .= " AND `cats`.ebay_store_cat_id2 = [ebay_store_cat_id2]";
		}
		if( isset( $o->template )  ){
			$where_ .= " AND `cats`.template = '$o->template'";
		}
		if( isset( $o->sub_featured )  ){
			$where_ .= " AND `cats`.sub_featured = '$o->sub_featured'";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY cats.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;
//echo $sql;
		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}
}


?>