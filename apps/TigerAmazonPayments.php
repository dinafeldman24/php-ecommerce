<?php
require_once('RB/amazon-payments/AmazonPayments.php');

class TigerAmazonPayments extends AmazonPayments {

    function __construct() {
        global $CFG;
        
        parent::__construct($CFG->awsAccessKey, $CFG->awsSecretAccessKey, $CFG->merchantId);
    }

    public function renderButton($cart, $tax_rate) {
        global $CFG;
        
        // Can't determine shipping costs and taxes without a destination zip code.
        // If the user has not entered a zip code, create a fake Amazon button that will prompt the user to enter a zip code, when clicked.

        $destinationZipCode = $cart->data['zipcode'];

        if ( ! $destinationZipCode ) {
            $this->renderPseudoButton();
            return;
        }

        // Get cart item data.

        $amCartItems = array();

        $totalWeight = 0.00;

        $cartItems = $cart->get();

	$min_charge = $cart->calcMinCharges();

        $discount = $cart->discountCalc(0.0,'',true);
        //print_ar($discount);
        //$discount = $discount['overall_discount'];

        if ($cartItems) {
            foreach ($cartItems as $cartItem) {
				
            	if ($cartItem['product_option_id']) 
            	{
            		$option_info = ProductOptions::getByIds($cartItem['product_option_id'],$cartItem['product_id']);
            		$product_option_vendor_sku = $option_info[0]['product_option_vendor_sku'];
            	}
            	else $product_option_vendor_sku = "";
            	
                $product_info = Products::get1($cartItem['product_id']);

                $itemWeight = (double) $product_info['weight'];
                if ($itemWeight == 0.00) {
                    $itemWeight = 1.00;
                }

                $quantity = (integer) $cartItem['qty'];

                $totalWeight += ($quantity * $itemWeight);

                //check for a discount for this line, cant send discount, so have to send NEW PRICE PER ITEM factoring in
                //the discount
                if(is_array($discount['aps']))
                {
                    foreach($discount['aps'] as $discount_line)
                    {
                        if($discount_line['product_id'] == $cartItem['product_id'])
                        {
                            $product_info['price'] = PromoCodes::getDiscountedPricePerItem($cartItem,$discount_line,false);
                        }
                    }
                }

                if ($product_option_vendor_sku != "") $sku_to_use = $product_option_vendor_sku;
                else $sku_to_use = $product_info['vendor_sku'];
                
                $amCartItems[] = array( 'sku' => $sku_to_use ,
					'title' => $product_info['name'], 'amount' => $product_info['price'],
					'quantity' => $quantity, 'weight' => $itemWeight);
            }

	    if( $min_charge )
	    {
		$amCartItems[] = array( 'sku' => 'MINCHARGE' ,
					'title' => 'Manufacturer Minimum Charges', 'amount' => $min_charge,
					'quantity' => 1, 'weight' => 0);
	    }
            
        }

        // Determine shipping method parameters.

        $shippingMethods = ShippingMethods::get();

        $shipper = UPSShipper::get1($CFG->default_from_shipper);
        $originZipCode = $shipper['zip'];

        $amShippingMethods = array();
	$orig_method = $cart->data['shipping_method'];
        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethodCode = $shippingMethod['code'];

            if ($shippingMethodCode == '') {
                continue;
            }
//            else if ($shippingMethodCode == 'GND') {
//                $amServiceLevel = 'Standard';
//
//                if( $cart->getCartTotal() > $CFG->free_shipping_limit ) {
//                    $amount = '0.00';
//                }
//                else {
//                    $amount = $CFG->flat_shipping_rate;
//                }
//            }
            else {

		if( $shippingMethodCode == 'GND' )
		{
		    $amServiceLevel = 'Standard';
		} else if ($shippingMethodCode == '3DS') {
                    $amServiceLevel = 'Expedited';
                }
                else if ($shippingMethodCode == '2DA') {
                    $amServiceLevel = 'TwoDay';
                }
                else { // $shippingMethodCode == '1DA'
                    $amServiceLevel = 'OneDay';
                }

//                $rate = new UpsQuote();
//                $rate->upsProduct($shippingMethodCode);
//                $rate->origin($originZipCode, "US"); // Use ISO country codes!
//                $rate->dest($destinationZipCode, "US"); // Use ISO country codes!
//                $rate->rate("RDP");
//                $rate->container("CP");
//                $rate->weight($totalWeight);
//                $rate->rescom($cart2['shipping_location'] == 'residential' ? "RES" : "COM");
//                $quote = $rate->getQuote();

                $amount = $quote;
		
		$cart->data['shipping_method'] = $shippingMethod['code'];
		
		$amount = Shipping::calcShipping(0, 'cart', '', '', 0, true, '', true);
            }

            $amShippingMethod = array();

            $amShippingMethod["ShippingMethodId"] = $shippingMethod['id'];
            $amShippingMethod["ServiceLevel"] = $amServiceLevel;
            $amShippingMethod["DisplayableShippingLabel"] = $shippingMethod['descr'];

            $amShippingMethod["IncludedRegions"] = array();
            $amShippingMethod["IncludedRegions"][] = array( 'RegionDefinition' => 'PredefinedRegion', 'value' => 'USContinental48States' );

            $amShippingMethod["Rate"] = array( "ShippingRate" => "ShipmentBased",
                                                                    "Price" => array("Amount" => $amount, "CurrencyCode" => "USD"));

            $amShippingMethods[] = $amShippingMethod;
        }
	$cart->data['shipping_method'] = $orig_method;
	//print_ar( $amShippingMethods );
        //print_ar($amCartItems);
        // Set sales tax rules, assumed to vary by State.

        $perStateTaxRules = array();
        $perStateTaxRules[] = array( 'state' => 'NY', 'rate' => ($tax_rate * .01));

        // Create Amazon Checkout button.
        $this->renderFormButton($amCartItems, $amShippingMethods, $perStateTaxRules);
    }
}
?>
