<?php
/*****************
*   Class for caching
*   with dynamic product 
*   options  
*   Hadassa Golovenshitz
*   March 15 2015
*
*****************/

class ProductOptionCache{

    function ProductOptionCache(){}

    static function setColorCodesAndImages(){
        if(isset($_SESSION['color_map'])){
            return;
        }
        $_SESSION['color_map'] = array();
        $all_colors = OptionColorMap::get();
        foreach($all_colors as $curr){
            $option_type = ($curr['option_type'] == 'color') ?
                'bgcolor' : 'imagesrc';
            $value = $curr['color_value'];
            if($option_type == 'imagesrc'){
                $value = '/optionpics/' . $value;
            }
            $_SESSION['color_map'][$curr['id']] = 'data-' . $option_type . '="' . $value . '"';
        }
    }

    static function getColorCodeForDropDown($color_id){
        if(!isset($_SESSION['color_map'])){
            ProductOptionCache::setColorCodesAndImages();
        }
        return(isset( $_SESSION['color_map'][$color_id]))? 
            $_SESSION['color_map'][$color_id] : '';
    }

    static function getMessageBelowLabel($product_id, $label_id){
        if(isset($_SESSION['labels'][$product_id][$label_id]) &&
            !empty($_SESSION['labels'][$product_id][$label_id]['message_below'])){
           return $_SESSION['labels'][$product_id][$label_id]['message_below'];
    }
        return '';
    }

    static function getMessageAboveLabel($product_id, $label_id){
        if(isset($_SESSION['labels'][$product_id][$label_id]) &&
            !empty($_SESSION['labels'][$product_id][$label_id]['message_above'])){
           return $_SESSION['labels'][$product_id][$label_id]['message_above'];
    }
        return '';
    }

    static function getLabelName($product_id, $label_id){
            return $_SESSION['labels'][$product_id][$label_id]['label'];
    }

    static function setLabels($product_id){
        if(isset($_SESSION['labels'][$product_id])){
            return;
        }
        $_SESSION['labels'][$product_id] = array();
        $labels = ProductAvailableOptionLabels::getForProduct($product_id);
        foreach($labels as $label){
            $_SESSION['labels'][$product_id][$label['id']] = $label;
        }
    }

    static function setRelatedProduct($product_id, $option){
        $related_prod = $option['related_product'];
        if((int)$related_prod == 0){
            return;
        }
        if(!isset($_SESSION['one_per_cart_products'])){
            $_SESSION['one_per_cart_products'] = array();
        }
        if(!array_key_exists($related_prod, $_SESSION['one_per_cart_products'])){
            $_SESSION['one_per_cart_products'][$related_prod] = array();
        }
        if(!isset($_SESSION['related_products'])){
            $_SESSION['related_products'] = array();
        }
        $_SESSION['related_products'][$option['pa_id']] =
            array('product_id' => $related_prod, 
                    'option_id' => $option['related_product_option'],
                    );
        $_SESSION['one_per_cart_products'][$related_prod][] = $option['pa_id'];
    }


    static function setProductOptionCache($product_id, $force = false){
        if(empty($product_id)){
            return;
        }
        if(isset($_SESSION['product_cache'][$product_id]) && !$force){
            return;
        }
        self::setLabels($product_id);
        $_SESSION['product_sales'][$product_id] = array();
        $_SESSION['product_all_options_on_sale'][$product_id] = true;
        $sales = Sales::get(0,true,$product_id);
        $product_sales = array();
        foreach($sales as $curr){
            $product_sales[$curr['product_option_id']] = array('essensa' => $curr['essensa_only'], 'sale_price' => $curr['sale_price']);
        }
        $all_set_options = array();
        $option_prices = array();
        $option_essensa_prices = array();
        $sql = "select  po.product_option_values_set as pov_set, 
        po.id as po_id, @reg_price := if(po.additional_price > 0.00, po.additional_price, p.price) as price,
         @essensa_price := if(po.essensa_price < po.additional_price AND po.essensa_price > 0.00 and (if(p.essensa_price > 0 and po.essensa_price < p.essensa_price, 0, 1)), po.essensa_price, if(p.essensa_price > 0.00 and p.essensa_price < po.additional_price and p.essensa_price < p.price, p.essensa_price, if( po.additional_price > 0.00, po.additional_price, p.price))) as essensa_price, po.vendor_sku, 
        @case_price_per_piece := if(p.case_price_percent > 0, ROUND(@reg_price *(1 - p.case_price_percent), 2),@reg_price) as case_price_per_piece,
        @case_price_per_piece * p.num_pcs_per_case as case_price,
        @case_essensa_price_per_piece := if(p.case_price_percent > 0, ROUND(@essensa_price * (1 - p.case_price_percent), 2), @essensa_price) as case_essensa_price_per_piece,
        @case_essensa_price_per_piece * p.num_pcs_per_case as case_essensa_price,
         ltrim(group_concat(concat( ' ',trim(pl.label), ': ', option_value(o.id, ov.value)))) as combined, 
         group_concat(o.name) as option_name,label, 
         group_concat(option_value(o.id, ov.value)) as option_value,
         o.id as option_id, pa.id as pa_id, pa.product_available_option_labels_id as label_id
         FROM  option_values ov, product_available_options pa, 
            options o, product_options po, products p, product_available_option_labels pl  
         WHERE ov.options_id = o.id AND 
            pa.options_values_id = ov.id AND
            locate(concat(',', pa.id,',') , product_option_values_set  ) > 0
            AND po.product_id = $product_id 
            AND po.is_active = 'Y'
            AND pa.product_available_option_labels_id = pl.id
            AND p.id = $product_id
        group by po.id order by pa.display_order";
        $options = db_query_array($sql);
        if($options){
           foreach($options as $curr){
                self::setRelatedProduct($product_id, $curr);
               $essensa_price = $curr['essensa_price']; 
               $price = $curr['price']; 
               $is_sale = false;
               if(isset($product_sales[$curr['po_id']])){
                   $is_sale = true;
                    if($product_sales[$curr['po_id']]['essensa_only'] == 'Y'){ 
                        $essensa_price = $product_sales[$curr['po_id']]['sale_price'];
                   }
                    else{
                        $price = $product_sales[$curr['po_id']]['sale_price'];
                        if($price < $essensa_price){
                            $essensa_price = $price;
                        }
                    }
               }
               else{
                    $_SESSION['product_all_options_on_sale'][$product_id] = false;
               }
                $option_prices[] = $price;
                $option_essensa_prices[] = $essensa_price;
                $this_option = array(
                    'price' => $price,
                    'essensa_price' => $essensa_price, 
                    'option_text' => self::orderText($curr['combined']),
                    'option_names' => $curr['label'],
                    'option_values' => $curr['option_value'],
                    'options_set' => $curr['pov_set'],
                    'label_id' => $curr['label_id'],
                    'option_id' => $curr['po_id'],
                    'option_sku' => $curr['vendor_sku'],
                    'case_price' => $curr['case_price'],
                    'case_essensa_price' => $curr['case_essensa_price'],
                    'case_price_per_piece' => $curr['case_price_per_piece'],
                    'case_essensa_price_per_piece' => $curr['case_price_per_piece'],
                    'is_sale' => $is_sale,
                );
                $_SESSION['product_options'][$curr['po_id']] = $this_option;
                $_SESSION['product_options_by_set'][$curr['pov_set']] = $this_option;
                $_SESSION['option_names'][$curr['option_id']] = $curr['option_name'];
                $_SESSION['options_set_for_product_option'][$curr['po_id']] = $curr['pov_set'];
                if($is_sale){
                    $_SESSION['product_sales'][$product_id][] = array(
                    'description' => $curr['combined'],
                    'id' => $curr['po_id'],
                    'price' => $price,
                    'essensa_price' => $essensa_price,
                    );
                
                }
                $these_options_set = explode(',', $curr['pov_set']);
                foreach($these_options_set as $curr){
                    $all_set_options[] = $curr;
                }
           }
            $option_prices = array_unique($option_prices);
            $option_essensa_prices = array_unique($option_essensa_prices);
            if(sizeof($option_prices) > 1){
                $_SESSION['prices_vary'][$product_id] = true;
                $_SESSION['min_price'][$product_id] = min($option_prices);
                $_SESSION['max_price'][$product_id] = max($option_prices);
                $_SESSION['min_essensa_price'][$product_id] = min($option_essensa_prices);
                $_SESSION['max_essensa_price'][$product_id] = max($option_essensa_prices);
            }
            else{
                $_SESSION['prices_vary'][$product_id] = false;
            }
        }
        //optional options
        $sql = "SELECT pa.id, pa.id as pa_id, additional_price as price, essensa_additional_price
            as essensa_price, @this_value := option_value(o.id, ov.value) as option_value, 
            if(@this_value <> '', concat( ' ',trim(pl.label), ': ', @this_value), o.name) as combined, 
            o.name as option_name, related_product, related_product_option
         FROM  option_values ov, product_available_options pa, options o, product_available_option_labels pl
         WHERE ov.options_id = o.id AND 
            pa.options_values_id = ov.id AND
            pa.product_available_option_labels_id = pl.id AND
            pa.product_id = $product_id
            and pl.optional = 1 ORDER BY pa.display_order";
        $options = db_query_array($sql);
        if($options){
            foreach($options as $curr){
               $this_option = array(
                    'price' => $curr['price'],
                    'essensa_price' => $curr['essensa_price'],
                    'option_text' => self::orderText($curr['combined']),
                    'option_names' => $curr['option_name'],
                    'option_values' => $curr['option_value'],
                    'option_id' => '',
                    'options_set' => '',
                    'option_sku' => '',
                    );
                $_SESSION['optional_options'][$curr['id']] = $this_option;
                $all_set_options[] = $curr['pa_id'];
                self::setRelatedProduct($product_id, $curr);
            }
        }
        $_SESSION['options_set'][$product_id] = array_unique($all_set_options);
        $_SESSION['product_cache'][$product_id] = true;
    }

    function orderText($text){
        $vals = explode(',', $text);
        sort($vals);
        $text = implode(', ', $vals);
        return $text;
    }

}
?>
