<?php

/*****************
*   Class for Product Variation Groups
*
*****************/

class ProductVariations{
    
    function get($id=0, $name='', $is_active=''){
		$sql = "SELECT *";
		$sql .= " FROM product_group_header ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND product_group_header.id = $id ";
		}
		if($name != ''){
			$name = mysql_real_escape_string($name);
			$sql .= " AND `product_group_header`.group_name = '$name'";
		}
		if($is_active != ''){
			$is_active = mysql_real_escape_string($is_active);
			$sql .= " AND `product_group_header`.is_active = '$is_active'";
		}
		
		$sql .= ' GROUP BY product_group_header.id ';
	    $ret = db_query_array($sql);
		return $ret;
    }
	function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = ProductVariations::get($id);
    	return $result[0];
	}
	function update($id, $info, $table='product_group_header'){
		return db_update($table,$id,$info);
    }
	function insert($info, $table='product_group_header'){
		 return db_insert($table ,$info);
    }
	function delete($id=0, $table='product_group_header'){
		return db_delete($table ,$id);
    }
	
	function get_products_group($id){
		$sql = "SELECT product_group_lines.*, products.name, product_group_header.is_active ";
		$sql .= " FROM product_group_lines ";
		$sql .= " LEFT JOIN products on product_group_lines.product_id = products.id ";
		$sql .= " LEFT JOIN product_group_header on product_group_lines.group_id = product_group_header.id ";
		$sql .= " WHERE 1 ";
	    $sql .= " AND product_group_lines.group_id = $id ";
		$ret = db_query_array($sql);
		return $ret;
    }
	function add_product_to_group($id, $product_id){
		
    }
	function remove_product_from_group($id, $product_id){
    
    }
	function get_variations(){
		
			$sql = "SELECT * ";
			$sql .= " FROM product_group_groupings ";
			$sql .= " WHERE 1 ";
			$sql .= " ORDER BY grouping_type ";
			
		$ret = db_query_array($sql);
		return $ret;
    } 
	function get_variation_values($id='', $values=''){		
			$sql = "SELECT product_group_grouping_values.*, product_group_groupings.grouping_type ";
			$sql .= " FROM product_group_grouping_values ";
			$sql .= " LEFT JOIN product_group_groupings ON product_group_grouping_values.grouping_id = product_group_groupings.id ";
			$sql .= " WHERE 1 ";
			if($id) $sql .= " AND  grouping_id IN ($id) ";
			if($values) $sql .= " AND product_group_grouping_values.id IN ($values) ";
			$sql .= " ORDER BY grouping_id, order_id , value";
			
		$ret = db_query_array($sql);
		return $ret;
    } 

}




?>
