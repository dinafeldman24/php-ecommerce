<?php

/*****************
*   Class for interfacing
*   with RMAs
*   Hadassa Golovenshitz
*   March 1 2015
*
*****************/

class OrderRMA{

    var $id;
    var $order_id;
    var $rma_code;
    var $status_id;
    var $status_text;
    var $credit_shipping;
    var $credit_tax;
    var $total_credit;
    var $order_items;
    static $statuses;

    function OrderRMA($id = null, $oid = null){
        if(!is_null($id)){
            $this->populate($id);
        }
        elseif(!is_null($oid)){
            $this->setOrderItems($oid);    
        }
    }   

    /******************
    *   
    *   DB Functions
    *
    ******************/

    function populate($id){
        $this->id = $id;
        $sql = "SELECT * FROM order_rma r JOIN rma_status s ON
             r.status_id = s.id WHERE r.id = " . $id;
        $info = db_get1($sql);
        $this->order_id = $info['order_id'];
        $this->rma_code = $info['rma_code'];
        $this->credit_shipping = $info['credit_shipping'];
        $this->total_credit = $info['total_credit'];
        $this->status_id = $info['rma_status_id'];
        $this->status_name = $info['status_name'];
        $this->setOrderItems();
    }

    function insert($info){
        $this->id = db_insert('order_rma', $info);
        echo 'the id: ' . $this->id;
    }

    /*********************
    *   
    *   Getters & Setters
    *
    *********************/

    function setOrderId($oid){
        $this->order_id = $oid;
    }

    function getOrderId(){
        return $this->order_id;
    }

    function setRMACode($code){
        $this->rma_code = $code;
    }

    function getRMACode(){
        return $this->rma_code;
    }

    function setStatusId($sid){
        $this->status_id = $sid;
    }

    function getStatusId(){
        return $this->status_id;
    }

    function setCreditShipping($is_credit){
        $this->credit_shipping = ($is_credit) ? 1 : 0;
    }

    function getCreditShipping(){
        return $this->credit_shipping;
    }

    function setCreditTax($is_credit){
        $this->credit_tax = ($is_credit) ? 1 : 0;
    }
 
    function getCreditTax(){
        return $this->credit_tax;
    }

    function setTotalCredit($total){
        $this->total_credit = $total;
    }

    function getTotalCredit(){
        return $this->total_credit;
    }

    function setOrderItems($oid = null){
        if(is_array($this->order_items)){
            return $this->order_items;
        }
        if(is_null($this->order_id) && !is_null($oid)){
            $this->setOrderId($oid);
        }
        if(is_null($this->order_id)){
            return array();
        }
        $this->order_items = Orders::getItems(0, $this->order_id);
    }

    function getOrderItems($oid){
        if(!$this->order_items){
            $this->setOrderItems($oid);
        }
        return $this->order_items;
    }
    
    static function setStatuses(){
        $sql = "SELECT * FROM rma_status ORDER BY id";
        $info = db_query_array($sql);
        $statuses = array();
        foreach($info as $curr){
            $statuses[$curr['id']] = $curr['status'];
        }
        self::$statuses = $statuses;
    }

    /******************
    *   
    *   Display Functions
    *
    ******************/


    static function getStatusDropdown(){
        if(!self::$statuses){
            self::setStatuses();
        }
        $str = '';
        foreach(self::$statuses as $id => $name){
            $str .= "<option value='$id'>$name</option>";
        }
        return $str;
    }
        
}
