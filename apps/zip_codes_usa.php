<?php
class ZipCodesUSA
{

	static function get_city_state_from_zip($zip)
    {
		        
            	$sql = "SELECT CityName, StateAbbr, StateName, ZipCode
						  FROM `zip_codes_usa`
						  WHERE CityType = 'D'
						  AND ";
				if(is_array($zip)){
					$sql .= "ZIPCode IN (".db_esc(implode(',',$zip)).")";
				}else{
					$sql .= "ZIPCode = '".db_esc($zip)."'";
				}		  
				
				$result = db_query_array( $sql );
				
				if(is_array($zip)){
					return $result;
				}else{
            		return $result[0];
				}
    }
	static function is_zip_code_valid($zip)
	{
		$sql = "SELECT * 
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."'";
		
		$result = mysql_query( $sql );
		
		if (mysql_num_rows($result) > 0) return true;
		else return false;
	}
	static function is_zip_code_alaska($zip)
	{
		$sql = "SELECT * 
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."' AND StateAbbr IN ('AK')";
		
		$result = mysql_query( $sql );
		//mail("rachel@tigerchef.com", "sql", $sql);
		if (mysql_num_rows($result) > 0) return true;
		else return false;
	}
		static function is_zip_code_hawaii($zip)
	{
		$sql = "SELECT * 
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."' AND StateAbbr IN ('HI')";
		
		$result = mysql_query( $sql );
		
		if (mysql_num_rows($result) > 0) return true;
		else return false;
	}
	static function is_valid_destination($zip='',$zip_info='')
	{
		if(!$zip_info){
			$sql = "SELECT CityName, StateAbbr, StateName, ZipCode
			FROM `zip_codes_usa`
			WHERE ZIPCode = '".db_esc($zip)."'";
			$zip_info = mysql_query( $sql );
		}
		
		$no_ship_destinations = array('PR', 'VI', 'AS', 'GU', 'PW', 'FM', 'MP', 'MH', 'AK', 'HI', 'AE', 'AA', 'AP');
		if(in_array($zip_info['StateAbbr'] , $no_ship_destinations)){
			return false;
		}else{
			return true;
		}
	}
	static function get_state_code_for_zip($zip){
		$sql = "SELECT StateAbbr
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."' ";
		$result = db_get1(db_query_array($sql));
		if($result){ 
			return $result['StateAbbr'];
		}else{
			return false;
		}
	}
	static function see_if_city_state_valid_for_zip($city, $state, $zip)
	{
		$sql = "SELECT *
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."'
		AND CityType <> 'N'
		AND CityName = '".db_esc(str_replace("St.", "Saint", str_replace("st.","Saint", $city)))."'
		AND StateAbbr = '".db_esc($state)."'";
	
		$result = mysql_query( $sql );
	    mail("dina.websites@gmail.com","sql", $sql);
		if (mysql_num_rows($result) == 0) return false;
		else return true;
	}
	
	static function get_local_time_zone($zip){
		$sql = "SELECT distinct(TimeZone),UTC
		FROM `zip_codes_usa`
		WHERE ZIPCode = '".db_esc($zip)."'";
	
		$result = db_query_array( $sql );
	
		if ($result) return $result[0];
		else return false;
	}	
}
?>