<?php

class ShipsIn {
	
	function insert($info)
	{		
		return db_insert('ships_in',$info);
	}
	
	function update($id,$info)
	{
		return db_update('ships_in',$id,$info);
	}
	
	function delete($id)
	{
		return db_delete('ships_in',$id);
	}
	
	//$id can be a comma separated string or a single id
	function get($id=0)
	{
		$sql = "SELECT ships_in.*
				FROM ships_in
				WHERE 1 ";
		
		if ($id > 0) {
			$sql .= " AND ships_in.id IN ($id) ";
		}
                
                $sql .= " ORDER BY time_type , min_time, max_time ";
                
		//echo $sql;
		return db_query_array($sql);
	}
	
	function get1($id)
	{
		$id = (int) $id;
		
		if (!$id) return false;
		
		$result = ShipsIn::get($id);
		return $result[0];
	}
	
	function getIdFromName($name){
		
		if(!$name)
			return '';
        
		//strip whitespace
		//$str = preg_replace('/\s+/', '', $name);
		//$str = strtolower ( $str );
		//$str = preg_replace('/(\d+\-\d+)([a-z]+)/','$1 $2', $str);
		$str = $name;

		$query = "SELECT id , name FROM ships_in
			    WHERE name = '$str' ";

		$ships_in = db_query_array($query);
		
		if(isset($ships_in)){
			$ships_in = $ships_in[0];
			return $ships_in['id'];
		} else {
			return 'error';
		}
	}	
	function longProcessingTime($id, $time = '4'){
		$ships_in = self::get1($id);
		$max_days = $ships_in['max_time'];
		if($ships_in['time_type'] == 'weeks'){
			$max_days *= 7;
		}
		if($ships_in['time_type'] == 'month'){
			$max_days *= 30;
		}
		if ($max_days > $time){
			return $ships_in['name'];
		} else {
			return false;
		}
	}
}

?>