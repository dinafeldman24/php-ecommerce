<?php

  function getGoogleSearchResults($url) {
  
        // Pass URL via CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  //10 sec timeout
        curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 100);
        curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 10);

        $return = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);

        if ($errno) {return $errno;}
        return $return;
  
  
  }



  function getGoogleAuth($url) {

        // Retrieve the Google authorization code
        
        // Pass URL via CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  //10 sec timeout
        curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 100);
        curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 10);

        $return = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);

        if ($errno ) {
             return ("Error: $errno<br>\n");
        }

        $auth = '';
        
        if ($return) {

          $pieces = explode("Auth=", $return);
          // find the Auth= value
          $auth = $pieces[1];
        
      
        }

        return($auth);
  }

        
   function updateGoogleRows($url, $xml_entry, $auth){
   
        $ch = curl_init();
        
        $putString = stripslashes($xml_entry);
        // Put string into a temporary file
        $putData = tmpfile();
        // Write the string to the temporary file
        fwrite($putData, $putString);
        // Move back to the beginning of the file
        fseek($putData, 0);

        curl_setopt($ch, CURLOPT_URL, $url);

        // Set HTTP headers
        $header = array();

        $header[] = "Content-Type: application/atom+xml";
        $header[] = "Authorization: GoogleLogin auth=".trim($auth);
        $header[] = "GData-Version: 1";
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Using a PUT method 
        curl_setopt($ch, CURLOPT_PUT, 1);
        // Pass the temporary file
        curl_setopt($ch, CURLOPT_INFILE, $putData);
        // Pass the length of the temporary file
        curl_setopt($ch, CURLOPT_INFILESIZE, strlen($putString));

 
        $return = curl_exec($ch);
        $errno = curl_errno($ch);

        fclose($putData);

        curl_close($ch);
   
        if ($errno ) {
             return ("Error: $errno<br>\n");
        }
   
        return($return);
   
   
   }

   function insertGoogleRows($url, $xml_entry, $auth)      {
   
   
   
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        // Set HTTP headers
        $header = array();

        $header[] = "Content-Type: application/atom+xml";
        $header[] = "Authorization: GoogleLogin auth=".trim($auth);
        $header[] = "GData-Version: 1";
   
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,$xml_entry);
 
        $return = curl_exec($ch);
        $errno = curl_errno($ch);

        curl_close($ch);
  
        if ($errno ) {
             return ("Error: $errno<br>\n");
        }
   
        return($return);
   
   
   
   }


   function deleteGoogleRows($url,$auth){
   
   
      $ch = curl_init();


      curl_setopt($ch, CURLOPT_URL, $url);

      // Set HTTP headers
      $header = array();

      $header[] = "Content-Type: application/atom+xml";
      $header[] = "Authorization: GoogleLogin auth=".trim($auth);
      $header[] = "GData-Version: 1";

      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

      $return = curl_exec($ch);
      $errno = curl_errno($ch);

      curl_close($ch);
  
  
      if ($errno ) {
             print "Error: $errno<br>\n";
      }
   
      return($return);
    
   
   }
?>
