<?php

	class SearchBanners {
		
		static function insert( $info )
		{
			//save the search term string
			if(isset($info['search_term'])){
				$search_terms = $info['search_term'];
				unset($info['search_term']);
			}
			
			//insert the banner into the db
			$id = db_insert( 'search_banners', $info );
			
			//use the returned id to insert the terms into the search term table
			if(isset($search_terms)){
				SearchBanners::insert_terms($id,$search_terms);
			}
			return $id;
		}
		
		static function update( $id, $info )
		{
			if(isset($info['search_term'])){
				//first remove the old search terms
				SearchBanners::delete_search_term( 0, $id );
				//now insert the new ones
				SearchBanners::insert_terms($id,$info['search_term']);
				unset($info['search_term']);
			}
			return db_update( 'search_banners', $id, $info );
		}
		
		function insert_terms($id,$search_terms){
			//var_dump($id);
			//print_ar($search_terms);
			foreach($search_terms as $term){
				if($id && $term['term'] && $term['query_component_id']){
					SearchBanners::insert_search_term( array('search_banner_id'=>$id, 'search_term'=>trim($term['term']), 'query_component_id'=>$term['query_component_id']) );
				}
			}
			return true;
		}
		
		static function delete( $id )
		{
			SearchBanners::delete_search_term( 0, $id );
			return db_delete( 'search_banners', $id );
		}
		
		static function get( $id=0, $is_active='', $search_term ='', $group_search_term = false)
		{
			$sql = " SELECT DISTINCT(search_banners.id), search_banners.* ";
			
			if($group_search_term){
				$sql .= ",  GROUP_CONCAT(search_term) as search_term ";
			}
						
			$sql .= "  FROM search_banners ";
			
			$sql .= " LEFT JOIN search_banner_search_terms ON search_banners.id = search_banner_search_terms.search_banner_id ";
			$sql .= " LEFT JOIN query_builder qb ON query_component_id = qb.id ";
			$sql .= " WHERE 1 ";
			
			if( $id )
				$sql .= " AND search_banners.id = " . (int) $id;
			if( $is_active ){
				$sql .= " AND search_banners.is_active = '" . addslashes( $is_active ) . "' ";
				$sql .= " AND ( (NOW() BETWEEN search_banners.start_date AND search_banners.end_date )
						OR (NOW()> search_banners.start_date AND search_banners.end_date = '0000-00-00 00:00:00')) ";
			}
			if($search_term != ''){
				/*
				$term_fields = Array('search_term');
				//$sql .= " AND " . db_split_keywords($search_term,$term_fields,'OR',true,true);
				$keyword_arr = explode( "\\\"", $keywords);
				foreach($keyword_arr as $keyword){
					$sql .= " AND REPLACE(sql_component, '_', search_term) ";
				}
				*/
				$sql .= " AND search_term LIKE '".$search_term."' ";

			}
				
			//$sql .= " ORDER BY search_banners.order_fld ";
			if($group_search_term){
				$sql .= " GROUP BY search_banners.id ";
			}
			
			//echo $sql;
			//die;
				
			$search_banners = db_query_array( $sql );
			
			foreach($search_banners as $key=>$banner){
				$search_banners[$key]['term_query'] = self::get_search_terms(0, $banner['id']);
			}
			
			return $search_banners;
		}
		
		static function get1( $id )
		{
			return db_get1( self::get( $id ) );
		}
		
		/**
		 * Get By Object Parameters
		 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
		 */
		static function getByO( $o=NULL){
			$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
		
			$select_ = "SELECT search_banners.* ";

			if($o->total){
				$select_ = "SELECT COUNT(DISTINCT(search_banners.id)) as total ";
			}

			$from_ = " FROM search_banners ";
		
			if(isset($o->group_search_term)){
				$select_ .= " , GROUP_CONCAT(search_term) as search_term ";
				$from_ .= " LEFT JOIN search_banner_search_terms ON search_banners.id = search_banner_search_terms.search_banner_id ";
				$groupby_ = " GROUP BY search_banners.id ";
			}
			
			$where_ = " WHERE 1 ";
		
			if(isset($o->id)){
				$where_ .= " AND search_banners.id = [id] ";
			}
		
			if( isset( $o->url )  ){
				$where_ .= " AND `search_banners`.url = [url]";
			}
			if( isset( $o->alt )  ){
				$where_ .= " AND `search_banners`.alt = [alt]";
			}
			if( isset( $o->is_active )  ){
				$where_ .= " AND `search_banners`.is_active = [is_active]";
			}

			if( isset( $o->query )  ){
				$where_ .= " AND ( `search_banners`.url LIKE '%".$o->query."%'
								OR `search_banners`.alt LIKE '%".$o->query."%'
								OR  `search_banners`.id LIKE '%".$o->query."%' )";
			}		
		
			if(!$o->total){
				if($o->order){
					$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
					if($o->order_asc){
						$orderby_ .= " ASC ";
					} else {
						$orderby_ .= " DESC ";
					}
				}
				else {
					$orderby_ = " ORDER BY search_banners.id DESC ";
				}
				if($o->limit){
					$limit_ .= db_limit($o->limit,$o->start);
				}
			}
		
			$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;
		
			$result = db_template_query($sql,$o);
		
			if($o->total){
				return (int)$result[0]['total'];
			}
		
			return $result;
		}
		
		static function get1ByO( $o )
		{
			$result = self::getByO( $o );
		
			if( $result )
				return $result[0];
		
			return false;
		}
		
		static function insert_search_term( $info )
		{
			return db_insert( 'search_banner_search_terms', $info );
		}
		
		static function delete_search_term( $id, $banner_id=0 )
		{
			if($banner_id >0){
				return db_delete( 'search_banner_search_terms', $banner_id, 'search_banner_id' );
			}else{
				return db_delete( 'search_banner_search_terms', $id );
			}
		}
		
		static function get_search_terms($id=0, $banner_id=0){
			$sql = " SELECT search_banner_search_terms.*, description, REPLACE(sql_component, '_', search_term) AS sql_part, regex_negate, REPLACE(regex_part, '_', search_term) AS regex_part
						FROM search_banner_search_terms 
						LEFT JOIN query_builder qb ON query_component_id = qb.id
						WHERE 1 ";
			
			if($id){
				$sql .= " AND search_banner_search_terms.id ={$id} ";
			}
			if($banner_id){
				$sql .= " AND search_banner_search_terms.search_banner_id ={$banner_id} ";
			}
			
			return db_query_array( $sql );
		}
		
		static function get1_search_term($id){
			return db_get1( self::get_search_terms( $id ) );
		}
		
		//will check if this search query matches the requirments for this banner
		static function show_this_banner($banner,$search_query){
			
			// if we don't have any search term requirments - show the banner
			if(!is_array($banner['term_query'])){ return true; }
			
			foreach($banner['term_query'] as $query_term){				
				$q = ($query_term['regex_negate']=='Y')?'!':'';
				$q .= "preg_match('" . $query_term['regex_part'] . "','".$search_query."')";
				$matching_parts[] = $q;
			}
			
			if($matching_parts){
				$matching_to_evaluate = implode(" {$banner['and_or']} ",$matching_parts);
				//var_dump ($matching_to_evaluate);
				$does_it_match  = eval("return $matching_to_evaluate;") ;
				return $does_it_match;
			}
			return false;
		}
	}
	

?>