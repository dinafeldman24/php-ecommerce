<?php

class Product_Subscribers
{
    public static function get($id = 0, $user_id = 0, $product_id = 0)
    {
            
        $sql = "SELECT product_subscribers.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name,customers.email as email
                FROM product_subscribers
							  LEFT JOIN products ON product_subscribers.product_id =products.id
                LEFT JOIN customers ON product_subscribers.user_id = customers.id
                WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND product_subscribers.id = $id ";
        }

        if ($user_id > 0)
        {
            $sql .= " AND product_subscribers.user_id = $user_id ";
        }

        if ($product_id > 0)
        {
            $sql .= " AND product_subscribers.product_id = $product_id ";
        }

        return db_query_array($sql);
    }
		
	public static function isSub($userid, $productid){
	   $subs=self::get("",$userid,$productid);
 		 foreach ($subs as $sub){
		    if ($sub['user_id']==$userid)
			     return true;
		 }		 
		return false;
	
	}


    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($user_id, $product_id)
    {
        $info['user_id']  = $user_id;
        $info['product_id']  = $product_id;
        return db_insert('product_subscribers', $info,'',true);
    }

    public static function delete($id)
    {
        return db_delete('product_subscribers', $id);
    }


}
?>