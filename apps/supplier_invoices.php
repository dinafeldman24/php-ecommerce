<?php 
class SupplierInvoices
{
	static function insert($info,$date = '')
	{
	    return db_insert('supplier_invoices',$info,$date);
	}

	static function update($id,$info,$date = '')
	{
	    return db_update('supplier_invoices',$id,$info,'id',$date);
	}

	static function delete($id)
	{
	    return db_delete('supplier_invoices',$id);
	}

	static function get($id=0,$order='',$order_asc='', $supplier_id=0, $supplier_inv_num='', $invoice_date='', $date_entered='', $po_number = '', $start = 0, $limit = 0, $qb_batch_id = 'any', $date_entered_through = '')
	{
	$sql = "SELECT DISTINCT supplier_invoices.*, brands.name AS brand_name,
										due AS invoice_total, GROUP_CONCAT(DISTINCT purchase_order.code SEPARATOR ', ') AS pos
										FROM (supplier_invoices, brands)
		 								LEFT JOIN supplier_invoice_lines ON supplier_invoices.id = supplier_invoice_lines.supplier_invoice_id 
										LEFT JOIN purchase_order_items ON supplier_invoice_lines.po_item_id = purchase_order_items.item_id
										LEFT JOIN purchase_order ON purchase_order_items.po_id = purchase_order.id
		 								WHERE supplier_invoices.supplier_id = brands.id";

     if ($supplier_id != 0) $sql .= " AND supplier_invoices.supplier_id = " . $supplier_id;
		 if ($supplier_inv_num != '') $sql .= " AND supplier_invoices.supplier_inv_num = '".$supplier_inv_num."'";
 		 if ($invoice_date != '') $sql .= " AND supplier_invoices.invoice_date = '".$invoice_date."'";
		 if ($date_entered != '') $sql .= " AND LEFT(supplier_invoices.date_entered, 10) = '".$date_entered."'"; 
		 if ($date_entered_through != '') $sql .= " AND supplier_invoices.date_entered <= '".$date_entered_through . " 23:59:59'";
		// if ($po_number != '') $sql .= " AND purchase_order.code = '" . $po_number ."'";		 																													
		 if ($qb_batch_id != 'any' || $qb_batch_id === 0) $sql .= " AND supplier_invoices.qb_batch_id = " . $qb_batch_id ;
		 $sql .= " GROUP BY supplier_invoices.id";
		  if ($po_number != '') $sql .= " HAVING pos like '%" . $po_number ."%'";
		 
		// else mail("rachel@tigerchef.com", "qb_batch_id", $qb_batch_id); 
		 if ($order) 
		 {
		 		$sql .= " ORDER BY $order";
				if ($order_asc != "" && !$order_asc) $sql .= " DESC";
				else $sql .= " ASC";
		 }
		 else $sql .= " ORDER BY invoice_date DESC";
		 if ($limit > 0) 	$sql .= db_limit($limit,$start);
	//	mail("rachel@tigerchef.com", "supplier sql", $sql);
		
		
		return db_query_array($sql);		 
	}
	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);

        return $result[0];
    }
static function insertLines($invoice_id, $invoice_items,$actual_prices_array)
	{		
		if(!invoice_items) return;
		$info = array();
		$info['supplier_invoice_id'] = $invoice_id;
		
		foreach($invoice_items as $one_line)
		{
			$item_id_parts = explode("|", $one_line);
			$item_id = $item_id_parts[1];
			// new
			$item_details_result = Orders::getItems($item_id);
			$item_details = $item_details_result[0];
			$expected_price = $item_details['cust_each'] == '-1' ? ($item_details['option_vendor_price'] && $item_details['option_vendor_price'] > 0 && $item_details['option_vendor_price'] != '0.00'? $item_details['option_vendor_price'] : $item_details['vendor_price'] ): $item_details['cust_each'];
			// end new
			$info['po_item_id'] = $item_id_parts[1];
			$info['invoice_price_per_piece'] = $actual_prices_array[$item_id];
			
			db_insert('supplier_invoice_lines',$info);
			
			if ($info['invoice_price_per_piece'] && $info['invoice_price_per_piece'] != $expected_price) 
			{
				// update the purchase_order_items table with the new cust_each
				db_update("purchase_order_items", $item_id, array("cust_each"=>$info['invoice_price_per_piece']), "item_id");
				// record the old and new expected cost in a new table, along with the supplier invoice id and po_item_id
				$cost_diff_on_invoice_info = array();
				$cost_diff_on_invoice_info['supplier_invoice_id'] = $invoice_id;
				$cost_diff_on_invoice_info['po_item_id'] = $info['po_item_id'];
				$cost_diff_on_invoice_info['expected_price'] = $expected_price;
				$cost_diff_on_invoice_info['actual_price'] = $info['invoice_price_per_piece'];
				$cost_diff_on_invoice_info['date'] = date('Y-m-d H:i:s');
				db_insert("po_cost_changes", $cost_diff_on_invoice_info);
				
				if (PurchaseOrder::wasCostOfGoodsSoldAlreadyWritten($item_details['po_id']) === true)
				{
					$cogs_adjust_amount = $item_details['item_qty'] * ($info['invoice_price_per_piece'] - $expected_price);
					$cogs_info = array("po_id"=>$item_details['po_id'], "amount"=> $cogs_adjust_amount);
					PurchaseOrder::insertCostOfGoodsSold($cogs_info);
				}		
			}
		}
	}
	static function getInvoiceLines($invoice_id)
	{
		$sql = "SELECT brands.quickbooks_name,  supplier_invoices.invoice_date,supplier_invoices.supplier_inv_num,
				 supplier_invoices.due, supplier_invoices.product_subtotal, supplier_invoices.freight, 
				 supplier_invoices.additional_fees,SUM(supplier_invoice_lines.invoice_price_per_piece * purchase_order_items.item_qty) as ext_price, 
				 supplier_invoices.discounts, supplier_invoices.invoice_notes,purchase_order.code AS po_number_code, YEAR(purchase_order.po_sent_date) as po_sent_year 
				FROM supplier_invoice_lines, supplier_invoices, brands, purchase_order_items, purchase_order
				WHERE supplier_invoice_lines.supplier_invoice_id = supplier_invoices.id
				AND supplier_invoices.id = $invoice_id
				AND supplier_invoices.supplier_id = brands.id
				AND supplier_invoice_lines.po_item_id = purchase_order_items.item_id 
				AND purchase_order_items.po_id = purchase_order.id
				GROUP BY purchase_order.id";
		
		$result = db_query_array($sql);
		return $result;
	}
}
?>