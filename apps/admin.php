<?php

include_once($CFG->libdir . '/jsmenu.php');

class Admin {

	function Admin() {}

	function validate($user,$pass)
	{
		$admin = self::get('','','','','','',$user);
		$admin = array_shift($admin);
		return db_query_array("SELECT *
					FROM admin_users
					WHERE id='".$admin['id']."' AND password='".addslashes(md5(md5($admin['id']) . $pass . $admin['id']))."'",'',true);
	}

	function insert($info,$table='admin_users')
	{
		$id = db_insert($table,$info);
		
		//encrypt password and update
		if(isset($info['password'])){
			//$info2['password'] = md5($info['password']);
			$info2['password'] = md5(md5($id) . $info['password'] . $id);
			self::update($id,$info2,'admin_users',false);
		}
		
		return $id;
	}

	function update($id,$info,$table='admin_users',$encrypt=true)
	{
		//encrypt password
		if(isset($info['password']) && $encrypt){
			$info['password'] = md5(md5($id) . $info['password'] . $id);
		}
		return db_update($table,$id,$info);
	}

	function delete($id,$table='admin_users')
	{
		return db_delete($table,$id);
	}

	function get($letter='',$id=0,$group_id=0,$keywords='',$is_active='',$order='',$username='',$display_in_assignments_drop_down = '')
	{

		$sql = 'SELECT admin_users.*,admin_groups.name AS "group"
				FROM admin_users
				LEFT JOIN admin_groups ON admin_users.group_id = admin_groups.id
				WHERE 1 ';
		if ($id > 0)
			$sql .= "AND admin_users.id = $id ";
		if ($letter != '')
			$sql .= "AND admin_users.last_name LIKE '$letter%' ";
		if ($keywords != '') {
			$fields = Array('admin_users.last_name','admin_users.first_name','admin_users.email','admin_groups.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if ($username != '')
			$sql .= "AND admin_users.username = '".addslashes($username)."' ";
		//add ability to get users from multiple group ids
		if(is_array($group_id)){
			$group_id = implode(', ',$group_id);
			$sql .= " AND admin_users.group_id IN ($group_id)";
		}else{
			if ($group_id > 0)
			$sql .= "AND admin_users.group_id = $group_id ";
		}

		if ($is_active != '')
			$sql .= "AND admin_users.is_active = '$is_active' ";

		if ($display_in_assignments_drop_down != '')
		{
			$sql .= "AND admin_users.in_order_assignments_drop_down = '$display_in_assignments_drop_down' ";
		}
		$sql .= ' GROUP BY admin_users.id ';

		if($order != '')
			$sql .= ' ORDER BY '.$order;
		else
			$sql .= ' ORDER BY admin_users.last_name DESC';

		if ($id == 0)
			return db_query_array($sql,'id');
		else
			return db_query_array($sql);
	}

	function get1($id, $col = null)
	{
		$id = (int) $id;

		if (!$id) return false;

		$result = Admin::get('',$id);

		if ($col)
		  return $result[0][$col];
		else
		  return $result[0];
	}

	function getMailing($count=false)
	{
		if (!$count)
			return db_query_array("SELECT first_name,last_name,email FROM admin_users",'email');
		else {
			$result = db_query_array("SELECT COUNT(*) AS ct FROM admin_users");
			return $result[0]['ct'];
		}
	}

	function getGroups()
	{
		return db_query_array("SELECT id,name,description FROM admin_groups ORDER BY name");
	}

	function getGroup($id)
	{
		if ($result = db_query_array("SELECT * FROM admin_groups WHERE id = $id"))
			return $result[0];
		else
			return false;
	}

	// didn't want to touch anything in here so i added a new function instead ...
	// i dont wanna hardcode in values for group indices in apps.
	function getGroupIDByName($name)
	{
		$name = addslashes($name);
		$tmp = db_query_array("SELECT * FROM admin_groups WHERE name='$name'");
		return $tmp[0]['id'];
	}

	function insertGroup($info)
	{
		return Admin::insert($info,'admin_groups');
	}

	function updateGroup($id,$info)
	{
		return Admin::update($id,$info,'admin_groups');
	}

	function deleteGroup($id)
	{
		return Admin::delete($id,'admin_groups');
	}

	function getGroupRights($id)
	{
		$result = db_query("SELECT page_id FROM admin_security WHERE group_id = $id");

		while ($row=db_fetch_array($result))
			$rights[$row['page_id']] = $row['page_id'];

		return $rights;
	}
	
	function getStoreID($id)
	{
		if ($result = db_query_array("SELECT store_id FROM admin_users WHERE id = $id"))
			return $result[0]['store_id'];
		else
			return false;
	}


	function getGroupRightsAdvanced($id)
	{
		$result = db_query("SELECT page_id, _select, _insert, _update, _delete, owner_fullcontrol FROM admin_security WHERE group_id = $id");

		while ($row=db_fetch_array($result))
			$rights[$row['page_id']] = array( 'page_id' => $row['page_id'],
			                                  '_select' => $row['_select'],
			                                  '_insert' => $row['_insert'],
			                                  '_update' => $row['_update'],
			                                  '_delete' => $row['_delete'],
			                                  'owner_fullcontrol' => $row['owner_fullcontrol']
			                                );
		return $rights;
	}

	function setGroupRights($id,$rights)
	{
		if (!is_array($rights)) {
			db_query("DELETE FROM admin_security WHERE group_id = $id");
			return true;
		}

		$rights_str = implode(',',$rights);
		db_query("DELETE FROM admin_security WHERE group_id = $id AND page_id NOT IN ($rights_str)");

		foreach ($rights as $page_id)
			db_query("INSERT IGNORE INTO admin_security (group_id,page_id) VALUES ($id,$page_id)");

		return true;
	}

	function setGroupRightsAdvanced($id,$rights,$rightsAdvanced)
	{
		if (!is_array($rights)) {
			db_query("DELETE FROM admin_security WHERE group_id = $id");
			return true;
		}

		$rights_str = implode(',',$rights);
		db_query("DELETE FROM admin_security WHERE group_id = $id AND page_id NOT IN ($rights_str)");

		foreach ($rights as $page_id) {
			$q = "INSERT IGNORE INTO admin_security (group_id,page_id) VALUES ($id,$page_id)";
			//echo $q;
			db_query($q);
		}

		//print_r($rightsAdvanced);
		if ($rightsAdvanced)
		foreach ($rightsAdvanced as $ra => $val) {
			$select = $val[0] == 's' ? 'Y' : 'N';
			$insert = $val[1] == 'i' ? 'Y' : 'N';
			$update = $val[2] == 'u' ? 'Y' : 'N';
			$delete = $val[3] == 'd' ? 'Y' : 'N';
			$q = "UPDATE admin_security SET _select = '$select', _insert = '$insert', _update = '$update', _delete = '$delete', owner_fullcontrol = 'Y' WHERE page_id = $ra AND group_id = $id";
			//echo $q . "<br>";
			db_query($q);
		}

		return true;
	}

	function getModulesPages($restrict=false, $outer_order = '', $inner_order = '', $name_field = 'name')
	{

		if (!$outer_order)
		  $outer_order = 'admin_modules.disp_order';
		if (!$inner_order)
		  $inner_order = 'name';

		if ($restrict) {
			$where = "  LEFT JOIN admin_security ON admin_security.page_id = admin_pages.id
						LEFT JOIN admin_users ON admin_users.group_id = admin_security.group_id
						WHERE admin_users.id = ".$_SESSION['id'];
		}

		/*$result = db_query("SELECT admin_pages.id,admin_pages.filename,admin_pages.name,admin_modules.name AS module
							FROM admin_pages
							LEFT JOIN admin_modules ON admin_modules.id = admin_pages.module_id
							$where
							ORDER BY admin_modules.disp_order,name");*/

		$result = db_query("SELECT admin_pages.*, admin_modules.{$name_field} AS module
							FROM admin_pages
							LEFT JOIN admin_modules ON admin_modules.id = admin_pages.module_id
							$where
							ORDER BY $outer_order,$inner_order");

		while ($row = db_fetch_array($result,MYSQL_ASSOC)) {
			$pages[$row['module']][] = $row;
		}

		return $pages;
	}

	function showMenu($show_print = false, $outer_order = '', $inner_order = '', $show_rb = true, $name_field = 'name')
	{

		$pages = Admin::getModulesPages(true, $outer_order, $inner_order, $name_field);

		//print_ar($pages);

		$menu = new JSMenu($show_print,$show_rb);

		foreach ($pages as $module => $module_pages) {
			unset($sub_menu);
			$sub_menu = &$menu->addSection($module);

			foreach ($module_pages as $page)
			{
				if (@$page['visibility'] == 'visible' || !@$page['visibility']) {

					$page_name = $page[$name_field];

					if (!$page_name) $page_name = $page['name'];

					$sub_menu->addItem($page['filename'],$page_name,($page['name'][0] == '/') ? '_new' : '');
				}
			}
		}

		$menu->display();
	}

	function isUserInGroup($user_id, $group_id)
	{
	  $user_info = Admin::get1($user_id);
	  return $user_info['group_id'] == $group_id;
	}

	function checkRights()
	{
	}
	
    static function getAdminSelect( $name, $id="", $selected=0, $message="Choose an Admin.", $include_inactive=false, $on_change='', $group_id = 0 )
	{
		$ret ="";
		
		if( $include_inactive )
			$active = '';
		else 
			$active = "Y";
			
		$ret .= '<select name="' . $name . '" id="' . $id . '" size="1" onchange="'. $on_change . '" >';
		
		$ss = Admin::get('',0,$group_id,'',$active);
		
		$ret .= '<option value="0">' . $message .'</option>';
		
		foreach( $ss as $s )
		{
			$ret .= '<option value="' . $s['id'] . '"';
			if( $selected == $s['id'] )
				$ret .= ' selected ';
			$ret .= '>' . $s['first_name'] . ' ' . $s['last_name'];

			$ret .= '</option>';
			
		}
		$ret .= "</select>";
		
		return $ret;
	}	
	
}

?>