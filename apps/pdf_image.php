<?php

include_once(dirname(__FILE__).'/pdf.php');

class PDFImage extends PDF {

	private $image_file;
	private $pdf_file;
	private $width;
	private $height;
	private $x = 0.0;
	private $y = 0.0;
	private $rotate = 0;

	public function __construct($imagefile, $pdffile = '')
	{
		if (file_exists($imagefile)) {
			$this->image_file = $imagefile;
		}
		else {
			throw new PDFFormException("Invalid Image File $imagefile");
		}

		if($pdffile != '') $this->pdf_file = $pdffile;
	}

	public function setWidth($width)
	{
		$this->width = $width;
	}

	public function setHeight($height)
	{
		$this->height = $height;
	}

	public function setRotation($angle)
	{
		$this->rotate = $angle;
	}

	public function setPoint($x,$y)
	{
		$this->x = $x;
		$this->y = $y;
	}


	protected function compile(&$pdflib)
	{
		global $CFG;

		list($width,$height,$type,$html) = getimagesize($this->image_file);

		$types = Array(
		2 => 'jpeg',
		7 => 'tiff',
		8 => 'tiff',
		1 => 'gif',
		3 => 'png'
		);

		if (!$types[$type]) {
			throw new PDFFormException("Invalid Image File Format (jpeg, tiff, gif, png supported)");
		}

		if ($this->width  == null)
			$this->width  = $width;
		if ($this->height == null)
			$this->height = $height;

		$attribs = 'boxsize {'.$this->width.' '.$this->height.'} position 0 fitmethod meet';

		if ($this->rotate) {
			$attribs .= " rotate $this->rotate";
		}
		
		if(isset($this->pdf_file)){

			// set fonts
			$pdflib->set_parameter("FontOutline", "Helvetica={$CFG->font_dir}Helvetica.ttf");
			$pdflib->set_parameter("FontOutline", "Helvetica-Bold={$CFG->font_dir}Helveticab.ttf");
			$pdflib->set_parameter("FontOutline", "Arial={$CFG->font_dir}arial.ttf");
			$pdflib->set_parameter("FontOutline", "Arial Black={$CFG->font_dir}arialbd.ttf");
			$pdflib->set_parameter("FontOutline", "Arial Bold={$CFG->font_dir}arialbd.ttf");
			$pdflib->set_parameter("FontOutline", "ZapfDingbats={$CFG->font_dir}ZapfDingbats.ttf");
			$default_font = $pdflib->load_font("Helvetica", "winansi", "");

			// open the existing pdf form
			$pdf_form = $pdflib->open_pdi($this->pdf_file, "password bizlic", 0);
			if (!$pdf_form) {
				throw new PDFFormException($pdflib->get_errmsg());
			}

			$endpage = $pdflib->get_pdi_value("/Root/Pages/Count", $pdf_form, 0, 0);
		

			for ($pageno = 1; $pageno <= $endpage; $pageno++) {
				$page = $pdflib->open_pdi_page($pdf_form, $pageno, "");
				$pdflib->begin_page_ext(20, 20, "");

				$pdflib->fit_pdi_page($page, 0, 0, "adjustpage");

				if($pageno == 1){
					$img = $pdflib->load_image($types[$type],$this->image_file,'');
					$pdflib->fit_image($img, $this->x, $this->y, $attribs);
					$pdflib->close_image($img);
				}

				$pdflib->end_page_ext("");
				$pdflib->close_pdi_page($page);
			}

			$pdflib->close_pdi($pdf_form);

		}else{
			$pdflib->begin_page_ext(612,792, "");
			$img = $pdflib->load_image($types[$type],$this->image_file,'');
			$pdflib->fit_image($img,$this->x,$this->y,$attribs);
			$pdflib->close_image($img);
			$pdflib->end_page_ext("");
		}

	}


}

?>