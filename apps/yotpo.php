<?php 
class Yotpo {

	/**
	 * Ping on an order that took place
	 * Mandatory elements:
	 * email, customer_name, product_sku, order_id, product_name, product_url, app_key, utoken, platform
	 * @param unknown $info
	 */
	function alert_mail_after_purchase($info) {
		
		global $CFG;
		
		//send mail after purchase request to yotpo		
		$url = $CFG->YotpoAppUrl . "apps/" . $CFG->YotpoAPPKey . "/purchases";		
		$response = Yotpo::send_curl($url,$info);
		
		//mail('tova@tigerchef.com', 'yotpo', var_export($info,true).'<br>'.json_encode($info).'<br>'.json_encode($response));
		
	} 
	
	/**
	 * Given product_sku (unique identifier of the product within your domain), and a app_key, 
	 * the call returns the total reviews and average score of this product.
	 * @param unknown $product_id
	 */
	function get_bottom_line($product_id){
		global $CFG;
		
		$url = $CFG->YotpoAppUrl . "products/" .$CFG->YotpoAPPKey. "/$product_id/bottomline";
		$response = Yotpo::send_curl($url);

		$response = json_decode($response,true);
		return $response['response']['bottomline'];
	}
	
	function set_uToken(){
		global $CFG;
		
		$url = $CFG->YotpoAppUrl . "oauth/token";
		$info = array(	'client_id' => $CFG->YotpoAPPKey,
						'client_secret' =>$CFG->YotpoSecret,
						'grant_type' => 'client_credentials');
		
		$response = Yotpo::send_curl($url,$info);
		
		//print_ar($response);
		
		$response = json_decode($response,true);
		db_insert_or_update('settings', array('name'=>'yotpoutoken','value'=>$response['access_token']),'','',true) ;
	}
	
	function send_curl($url,$post_fields=''){
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		
		if($post_fields){
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
		}
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}
?>