<? class FraudScoreResponses
{
	public static function insert($info)
	{
		$info['date'] = strftime('%Y-%m-%d %H-%M-%S', time());
		return db_insert('fraud_score_responses', $info);
	}
}