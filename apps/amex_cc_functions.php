<?php

class AmexCC extends AmexCcMerchant{
	
	private $transaction_id;
	private $cvv_match;
	private $avs_code;
	private $token;
	private $orderid;
	private $error_array;
	private $last4;

	function __construct(){
		
		// call parent ctor to init members
		parent::__construct();

		// initialise cURL object/options
		$this->curlObj = curl_init();
		
		// configure cURL proxy options by calling this function
		//$this->ConfigureCurlProxy();
		
		// configure cURL certificate verification settings by calling this function
		$this->ConfigureCurlCerts();
		
		// assigns merchantObj gatewayUrl member
		$this->FormRequestUrl();
			
		
		
		//=================TEMPORARY============
		//$this->SetDebug(TRUE);
		//=================TEMPORARY============
		
	}
	
	function __destruct() {
		// free cURL resources/session
		curl_close($this->curlObj);
	}	
	
	// Send transaction to payment server
	public function SendTransaction($fields) {
		
		// form transaction request
		$request = $this->ParseRequest($fields);
		//print_ar($request);
		//mail('tova@tigerchef.com','parse - request',print_r($request,true));
		
		// print the request pre-send to server if in debug mode
		if ($this->GetDebug())
			echo $request . "<br/><br/>";
		
		// forms the requestUrl and assigns it to the merchantObj gatewayUrl member
		//$requestUrl = $this->FormRequestUrl();
		$requestUrl = $this->GetGatewayUrl();
		
		// this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
		if ($this->GetDebug())
			echo $requestUrl . "<br/><br/>";		
		
		// attempt transaction
		
		// Post - start
		curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
		// Post - end
	
		// SetURL - start
		curl_setopt($this->curlObj, CURLOPT_URL, $this->GetGatewayUrl());
		// SetURL - end
	
		// SetHeaders - start
		// set the content length HTTP header
		curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
	
		// set the charset to UTF-8 (requirement of payment server)
		curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
		// SetHeaders - end
	
		// tells cURL to return the result if successful, of FALSE if the operation failed
		curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);
	
		if ($this->GetDebug()) {
			curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
			curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
		}
	
		// executeSendTransaction  - start
		$response = curl_exec($this->curlObj);
		// executeSendTransaction - end
	
		//mail('tova@tigerchef.com', 'response', $response .'\n'.print_r($this->decodeResponse($response),true));
		
		if ($this->GetDebug()) {
			$requestHeaders = curl_getinfo($this->curlObj);
			$response = $requestHeaders["request_header"] . $response;
		}
	
		// assigns the cURL error to response if something went wrong so the caller can echo the error
		if (curl_error($this->curlObj))
			$response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);
			
		// print response received from server if in debug mode
		if ($this->GetDebug()) {
			// replace the newline chars with html newlines
			$response = str_replace("\n", "<br/>", $response);
				
			echo $response . "<br/><br/>";
			
			//mail('tova@tigerchef.com', 'response', $response);
			//print_ar($this->formErrorArray($response)) . "<br/><br/>";
			
			//die();
		}
		
		// respond with the transaction result, or a cURL error message if it failed
		return $response;
	}
	
	// ConfigureProxy - start
	// Check if proxy config is defined, if so configure cURL object to tunnel through
	protected function ConfigureCurlProxy() {
		// If proxy server is defined, set cURL option
		if ($this->GetProxyServer() != "") {
			curl_setopt($this->curlObj, CURLOPT_PROXY, $this->GetProxyServer());
			curl_setopt($this->curlObj, $this->GetProxyCurlOption(), $this->GetProxyCurlValue());
		}
	
		// If proxy authentication is defined, set cURL option
		if ($this->GetProxyAuth() != "")
			curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, $this->GetProxyAuth());
	}
	// ConfigureProxy - end
	
	// ConfigureSslCert - start
	// configure the certificate verification related settings on the cURL object
	protected function ConfigureCurlCerts() {
		// if user has given a path to a certificate bundle, set cURL object to check against them
		if ($this->GetCertificatePath() != "")
			curl_setopt($this->curlObj, CURLOPT_CAINFO, $this->GetCertificatePath());
	
		curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, $this->GetCertificateVerifyPeer());
		curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, $this->GetCertificateVerifyHost());
	}
	//ConfigureSslCert - end
	
	
	// ConfigureURL - start
	// Modify gateway URL to set the version
	// Assign it to the gatewayUrl member in the merchantObj object
	public function FormRequestUrl() {
		$gatewayUrl = $this->GetGatewayUrl();
		$gatewayUrl .= "/version/" . $this->GetVersion();
		
		$this->SetGatewayUrl($gatewayUrl);
		
		return $gatewayUrl;
	}
	//ConfigureURL - end
	
	// Form NVP formatted request and append merchantId, apiPassword & apiUsername
	public function ParseRequest($formData) {
		$request = "";
	
		if (count($formData) == 0)
			return "";
	
		foreach ($formData as $fieldName => $fieldValue) {
			if (strlen($fieldValue) > 0 && $fieldName != "merchant" && $fieldName != "apiPassword" && $fieldName != "apiUsername") {
				// replace underscores in the fieldnames with decimals
				for ($i = 0; $i < strlen($fieldName); $i++) {
					if ($fieldName[$i] == '_')
						$fieldName[$i] = '.';
				}
				$request .= $fieldName . "=" . urlencode($fieldValue) . "&";
			}
		}
	
		// For NVP, authentication details are passed in the body as Name-Value-Pairs, just like any other data field
		$request .= "merchant=" . urlencode($this->GetMerchantId()) . "&";
		$request .= "apiPassword=" . urlencode($this->GetPassword()) . "&";
		$request .= "apiUsername=" . urlencode($this->GetApiUsername());
	
		return $request;
	}

	function saveCard($cc_num, $exp_month, $exp_year, $auth_fields){
		$fields = array(
				'apiOperation'									=> 'TOKENIZE',

				'sourceOfFunds.type' 							=> 'CARD',
				'sourceOfFunds.provided.card.number' 			=> $cc_num,
				'sourceOfFunds.provided.card.expiry.month' 		=> $exp_month,
				'sourceOfFunds.provided.card.expiry.year' 		=> $exp_year,
				'sourceOfFunds.provided.card.securityCode'		=> $auth_fields['cvv2'],
				
				'transaction.currency'							=> 'USD',
		);
		
		$response = $this->SendTransaction($fields);
		
		//decode the response
		$responseArray = $this->decodeResponse($response);
		
		// parse the response
		$result = $this->parseResponse($responseArray);
		
		if($result !== 'SUCCESS'){ 
			$result = $responseArray['error.explanation'];
		}
		
// 		echo 'saveCard';
// 		print_ar($result);
// 		print_ar($responseArray);
		
		return $result;
		
	}
	
	
	function authorize($cc_num='', $exp_month='', $exp_year='', $grand_total, $auth_fields, $token=''){
		
		//mail('tova@tigerchef.com', 'authorize', print_r($auth_fields,true));
		$transaction_id = $auth_fields['transaction_id'] ? $auth_fields['transaction_id'] : $auth_fields['ordernum'] ;
		
		$fields = array(
				'apiOperation'									=> 'AUTHORIZE',
				'order.id'										=> $auth_fields['ordernum'],
				
				'transaction.id'								=> $transaction_id,
				'transaction.amount'							=> $grand_total,
				'transaction.currency'							=> 'USD',
				'sourceOfFunds.type' 							=> 'CARD',
				
				'billing.address.country'						=> 'USA',								
				'customer.ipAddress'							=> $_SERVER['REMOTE_ADDR'],
		);
		
		if($token){
			$fields['sourceOfFunds.token'] = $token;
		} else {
			$fields['sourceOfFunds.provided.card.number' ] = $cc_num;
			$fields['sourceOfFunds.provided.card.expiry.month'] = $exp_month;
			$fields['sourceOfFunds.provided.card.expiry.year' ] = $exp_year;
			$fields['sourceOfFunds.provided.card.holder.firstName'] = $auth_fields['first_name'];
			$fields['sourceOfFunds.provided.card.holder.lastName'] = $auth_fields['last_name'];
		}
		
		if($auth_fields['cvv2']){
			$fields['sourceOfFunds.provided.card.securityCode'] = $auth_fields['cvv2'];
		}
		if($auth_fields['city']){
			$fields['billing.address.city'] = $auth_fields['city'];
		}
		if($auth_fields['zipcode']){
			$fields['billing.address.postcodeZip'] = $auth_fields['zipcode'];
		}
		if($auth_fields['state']){
			$fields['billing.address.stateProvince'] = $auth_fields['state'];
		}
		if($auth_fields['streetaddress']){
			$fields['billing.address.street'] = $auth_fields['streetaddress'];
		}
		
		
// 		echo 'fields';
// 		print_ar($fields);
		
		$response = $this->SendTransaction($fields);
		
		//decode the response
		$responseArray = $this->decodeResponse($response);
		
		// parse the response
		$result = $this->parseResponse($responseArray);
		
		//check if there is a cvv match
		if($this->cvv_match != 'MATCH' && $this->cvv_match != 'CVV_NOT_PROVIDED'){
			return 'cvv_fail';
		}
		
		return $result;
		
	}
	
	function capture($order_id, $transaction_id, $amount){
		
		//order.id       => ID of the existing Order to capture against
		//transaction.id => ID of the transaction that will be created
		$fields = array(
				'apiOperation'									=> 'CAPTURE',
				'order.id'										=> $order_id,
				'transaction.id'								=> $transaction_id,
	
				'transaction.amount'							=> $amount,
				'transaction.currency'							=> 'USD',
		);
		
		$this->transaction_id = $transaction_id;
	
		$response = $this->SendTransaction($fields);
	
		//decode the response
		$responseArray = $this->decodeResponse($response);
	
		// parse the response
		$result = $this->parseResponse($responseArray);
	
		return $result;
	
	}
	
	function refund($order_id, $transaction_id, $amount, $note){
	
		//order.id       => ID of the existing Order to refund
		//transaction.id => ID of the transaction that will be created
		$fields = array(
				'apiOperation'									=> 'REFUND',
				'order.id'										=> $order_id,
				'transaction.id'								=> $transaction_id,
	
				'transaction.amount'							=> $amount,
				'transaction.currency'							=> 'USD',
				'transaction.merchantNote'						=> $note,
		);
	
		$this->transaction_id = $transaction_id;
	
		$response = $this->SendTransaction($fields);
	
		//decode the response
		$responseArray = $this->decodeResponse($response);
	
		// parse the response
		$result = $this->parseResponse($responseArray);
	
		return $result;
	
	}
	
	function void($order_id, $transaction_id, $target_transaction_id){
	
		//order.id       => ID of the existing Order to void the transaction from
		//transaction.id => ID of the transaction that will be created
		//transaction.targetTransactionId => ID of transaction to void
		$fields = array(
				'apiOperation'									=> 'VOID',
				'order.id'										=> $order_id,
				'transaction.id'								=> $transaction_id,
	
				'transaction.targetTransactionId'				=> $target_transaction_id,
		);
	
		$this->transaction_id = $transaction_id;
	
		$response = $this->SendTransaction($fields);
	
		//decode the response
		$responseArray = $this->decodeResponse($response);
	
		// parse the response
		$result = $this->parseResponse($responseArray);
	
		return $result;
	
	}
	
	function retrieve_transaction($order_id, $transaction_id){
	
		//order.id       => ID of the existing Order to retrieve against
		//transaction.id => ID of the transaction that will be created
		$fields = array(
				'apiOperation'									=> 'RETRIEVE_TRANSACTION',
				'order.id'										=> $order_id,
				'transaction.id'								=> $transaction_id,
		);
	
		$response = $this->SendTransaction($fields);
	
		//decode the response
		$responseArray = $this->decodeResponse($response);
	
		// parse the response
		$result = $this->parseResponse($responseArray);
	
		return $result;
	
	}
	
	function retrieve_order($order_id){
	
		//order.id       => ID of the existing Order to capture against
		//transaction.id => ID of the transaction that will be created
		$fields = array(
				'apiOperation'									=> 'RETRIEVE_ORDER',
				'order.id'										=> $order_id,
		);
	
		$response = $this->SendTransaction($fields);
	
		//decode the response
		$responseArray = $this->decodeResponse($response);
	
		// parse the response
		//$result = $this->parseResponse($responseArray);
	
		return $responseArray;
	
	}
	
	function get_outstanding_authorization_amt($order_id){
		$order = $this->retrieve_order($order_id);
		
		return $order['totalAuthorizedAmount'] - ($order['totalCapturedAmount'] - $order['totalRefundedAmount']);
	}
	
	function decodeResponse($response){
		if (strstr($response, "cURL Error") != FALSE) {
			print("Communication failed."); //Please review payment server return response (put code into debug mode)
			die();
		}		
		// loop through server response and form an associative array
		// name/value pair format
		if (strlen($response) != 0) {
// 			$response_string = explode('/\s/', $response);
			
// 			$response_string = end($response_string);
// 			echo 'decode!';
// 			var_dump($response_string);
			$pairArray = explode("&", $response);
			foreach ($pairArray as $pair) {
				$param = explode("=", $pair);
				$responseArray[urldecode($param[0])] = urldecode($param[1]);
			}
		}
		
		// print array if in debug mode
		if ($this->GetDebug()) {
			print_ar($responseArray);
		}
		return $responseArray;
	}
	
	function parseResponse($responseArray){
// 		echo 'parseResponse';
// 		print_ar($responseArray);
		
		if (array_key_exists("result", $responseArray)){
			$this->error_array = $this->formErrorArray($responseArray);
			$result = $responseArray["result"];
		}
		
		if (array_key_exists("response.cardSecurityCode.gatewayCode", $responseArray)){
			$this->cvv_match = $responseArray["response.cardSecurityCode.gatewayCode"];
		} elseif(!array_key_exists("sourceOfFunds.provided.card.securityCode", $responseArray)) {
			$this->cvv_match = 'CVV_NOT_PROVIDED' ; // we provide CVV for verification only for the first authorization
		}
		
		if (array_key_exists("response.cardholderVerification.avs.gatewayCode", $responseArray))
			$this->avs_code = $responseArray["response.cardholderVerification.avs.gatewayCode"];
		if (array_key_exists("transaction.id", $responseArray))
			$this->transaction_id = $responseArray["transaction.id"];
		if (array_key_exists("token", $responseArray))
			$this->token = $responseArray["token"];
		if (array_key_exists("order.id", $responseArray))
			$this->orderid = $responseArray["order.id"];
		if (array_key_exists("sourceOfFunds.provided.card.number", $responseArray)){
			$this->last4 = substr($responseArray["sourceOfFunds.provided.card.number"],-4); 
		}
		
// 		echo 'parseResponse:';
// 		print_ar($responseArray);
		return $result;
	}
	
	function formErrorArray($responseArray){

// 		echo 'formErrorArray';
// 		print_ar($responseArray);
		
		// Form error string if error is triggered
		if ($responseArray['result'] == "FAIL") { 
			if (array_key_exists("failureExplanation", $responseArray)) {
				$errorMessage = rawurldecode($responseArray["failureExplanation"]);
			}
			else if (array_key_exists("supportCode", $responseArray)) {
				$errorMessage = rawurldecode($responseArray["supportCode"]);
			}
			else {
				$errorMessage = "Reason unspecified.";
			}
		
			if (array_key_exists("failureCode", $responseArray)) {
				$errorCode = "Error (" . $responseArray["failureCode"] . ")";
			}
			else {
				$errorCode = "Error (UNSPECIFIED)";
			}
		} 
		elseif ($responseArray['result'] == "ERROR") {
			$errorCode = "ERROR";
			if (array_key_exists("error.explanation", $responseArray)) {
				$errorMessage = rawurldecode($responseArray["error.explanation"]);
			}
			else {
				$errorMessage = "Reason unspecified.";
			}
		
//			if (array_key_exists("error.cause", $responseArray)) {
//				$errorMessage .= "[" . $responseArray["error.cause"] . "]";
//			}
//			else {
//				$errorMessage .= " [UNSPECIFIED] ";
//			}
		}
		
		else {
			
			if (array_key_exists("response.gatewayCode", $responseArray))
				$gatewayCode = rawurldecode($responseArray["response.gatewayCode"]);
			else
				$gatewayCode = "Response not received.";
		}

		if ($errorCode != "" || $errorMessage != "") {
			
			return array('errorCode'=>$errorCode,'errorMessage'=>$errorMessage);
	    }

	    else {
	    	
	    	return array('gatewayCode'=>$gatewayCode,'result'=>$responseArray["result"]);

	     }
	}
	
	function getNiceErrorMessage(){
		
		$error_array = $this->error_array;
// 		print_ar($response);


		if($error_array['errorCode'])
			return $error_array['errorMessage'];
		
		if($error_array['gatewayCode'])
		switch ($error_array['gatewayCode']) {
			case 'APPROVED':
				return 'Transaction Approved';
				break;
			case 'UNSPECIFIED_FAILURE':
				return 'Transaction could not be processed';
				break;
			case 'DECLINED':
				return 'Transaction declined by issuer';
				break;
			case 'TIMED_OUT':
				return 'Response timed out';
				break;
			case 'EXPIRED_CARD':
				return 'Transaction declined due to expired card';
				break;
			case 'INSUFFICIENT_FUNDS':
				return 'Transaction declined due to insufficient funds';
				break;
			case 'ACQUIRER_SYSTEM_ERROR':
				return 'Acquirer system error occurred processing the transaction';
				break;
			case 'SYSTEM_ERROR':
				return 'Internal system error occurred processing the transaction';
				break;
			case 'NOT_SUPPORTED':
				return 'Transaction type not supported';
				break;
			case 'DECLINED_DO_NOT_CONTACT':
				return 'Transaction declined - do not contact issuer';
				break;
			case 'ABORTED':
				return 'Transaction aborted by payer';
				break;
			case 'BLOCKED':
				return 'Transaction blocked due to Risk or 3D Secure blocking rules';
				break;
			case 'CANCELLED':
				return 'Transaction cancelled by payer';
				break;
			case 'DEFERRED_TRANSACTION_RECEIVED':
				return 'Deferred transaction received and awaiting processing';
				break;
			case 'REFERRED':
				return 'Transaction declined - refer to issuer';
				break;
			case 'AUTHENTICATION_FAILED':
				return '3D Secure authentication failed';
				break;
			case 'INVALID_CSC':
				return 'Invalid card security code';
				break;
			case 'LOCK_FAILURE':
				return 'Order locked - another transaction is in progress for this order';
				break;
			case 'SUBMITTED':
				return 'Transaction submitted - response has not yet been received';
				break;
			case 'NOT_ENROLLED_3D_SECURE':
				return 'Card holder is not enrolled in 3D Secure';
				break;
			case 'PENDING':
				return 'Transaction is pending';
				break;
			case 'EXCEEDED_RETRY_LIMIT':
				return 'Transaction retry limit exceeded';
				break;
			case 'DUPLICATE_BATCH':
				return 'Transaction declined due to duplicate batch';
				break;
			case 'DECLINED_AVS':
				return 'Transaction declined due to address verification';
				break;
			case 'DECLINED_CSC':
				return 'Transaction declined due to card security code';
				break;
			case 'DECLINED_AVS_CSC':
				return 'Transaction declined due to address verification and card security code';
				break;
			case 'DECLINED_PAYMENT_PLAN':
				return 'Transaction declined due to payment plan';
				break;
			case 'APPROVED_PENDING_SETTLEMENT':
				return 'Transaction Approved - pending batch settlement';
				break;
			case 'BASIC_VERIFICATION_SUCCESSFUL':
				return 'The card number format was successfully verified and the card exists in a known range.';
				break;
			case 'NO_VERIFICATION_PERFORMED':
				return 'The card details were not verified.';
				break;case 'EXTERNAL_VERIFICATION_SUCCESSFUL':
			return 'The card details were successfully verified.';
			break;
			case 'EXTERNAL_VERIFICATION_DECLINED':
				return 'The card details were sent for verification, but were was declined.';
				break;
			case 'EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD':
				return 'The card details were sent for verification, but were declined as the card has expired.';
				break;
			case 'EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC':
				return 'The card details were sent for verification, but were declined as the Card Security Code (CSC) was invalid.';
				break;
			case 'EXTERNAL_VERIFICATION_PROCESSING_ERROR':
				return 'There was an error processing the verification.';
				break;  
			case 'EXTERNAL_VERIFICATION_BLOCKED':
				return 'The external verification was blocked due to risk rules.';
				break;
			case 'UNKNOWN':
				return 'Response unknown';
				break; 
			default:
				return $error_array['gatewayCode'];
			break;
		}			
	}
	
	/**
	 * @return the $transaction_id
	 */
	public function getTRANSACTIONID() {
		return $this->transaction_id;
	}
	
	/**
	 * @return the $cvv_match
	 */
	public function getCVV2MATCH() {
		return $this->cvv_match;
	}
	
	/**
	 * @return the $avs_code
	 */
	public function getAVSCODE() {
		return $this->avs_code;
	}
	
	/**
	 * @return the $token
	 */
	public function getTOKEN() {
		return $this->token;
	}
	
	/**
	 * @return the $orderid
	 */
	public function getORDERID() {
		return $this->orderid;
	}
	
	/**
	 * @return the $last4
	 */
	public function getLAST4() {
		return $this->last4;
	}
}

/*
 This class holds all the merchant related variables and proxy
configuration settings
*/
class AmexCcMerchant {
	private $proxyServer = "";
	private $proxyAuth = "";
	private $proxyCurlOption = 0;
	private $proxyCurlValue = 0;	
	
	private $certificatePath = "";
	private $certificateVerifyPeer = FALSE;	
	private $certificateVerifyHost = 0;

	private $gatewayUrl = "";
	private $debug = FALSE;
	private $version = "";
	private $merchantId = "";
	private $password = "";
	private $apiUsername = "";
	
	function __construct() {
		
		global $CFG;
		
		if ($CFG->Amex_API_proxyServer)
			$this->proxyServer = $CFG->Amex_API_proxyServer;
		
		if ($CFG->Amex_API_proxyAuth)
			$this->proxyAuth = $CFG->Amex_API_proxyAuth;
			
		if ($CFG->Amex_API_proxyCurlOption)
			$this->proxyCurlOption = $CFG->Amex_API_proxyCurlOption;
		
		if ($CFG->Amex_API_proxyCurlValue)
			$this->proxyCurlValue = $CFG->Amex_API_proxyCurlValue;
			
		if ($CFG->Amex_API_certificatePath)
			$this->certificatePath = $CFG->Amex_API_certificatePath;
			
		if ($CFG->Amex_API_certificateVerifyPeer)
			$this->certificateVerifyPeer = $CFG->Amex_API_certificateVerifyPeer;
			
		if ($CFG->Amex_API_certificateVerifyHost)
			$this->certificateVerifyHost = $CFG->Amex_API_certificateVerifyHost;
		
		if ($CFG->Amex_API_gatewayUrl)
			$this->gatewayUrl = $CFG->Amex_API_gatewayUrl;
		
		if ($CFG->Amex_API_debug)	
			$this->debug = $CFG->Amex_API_debug;
			
		if ($CFG->Amex_API_version)
			$this->version = $CFG->Amex_API_version;
			
		if ($CFG->Amex_API_merchantId)	
			$this->merchantId = $CFG->Amex_API_merchantId;
		
		if ($CFG->Amex_API_password)
			$this->password = $CFG->Amex_API_password;
			
		if ($CFG->Amex_API_apiUsername)
			$this->apiUsername = $CFG->Amex_API_apiUsername;	

	}
	
	// Get methods to return a specific value
	public function GetProxyServer() { return $this->proxyServer; }
	public function GetProxyAuth() { return $this->proxyAuth; }
	public function GetProxyCurlOption() { return $this->proxyCurlOption; }
	public function GetProxyCurlValue() { return $this->proxyCurlValue; }
	public function GetCertificatePath() { return $this->certificatePath; }
	public function GetCertificateVerifyPeer() { return $this->certificateVerifyPeer; }
	public function GetCertificateVerifyHost() { return $this->certificateVerifyHost; }
	public function GetGatewayUrl() { return $this->gatewayUrl; }
	public function GetDebug() { return $this->debug; }
	public function GetVersion() { return $this->version; }	
	public function GetMerchantId() { return $this->merchantId; }
	public function GetPassword() { return $this->password; }
	public function GetApiUsername() { return $this->apiUsername; }
	
	// Set methods to set a value
	public function SetProxyServer($newProxyServer) { $this->proxyServer = $newProxyServer; }
	public function SetProxyAuth($newProxyAuth) { $this->proxyAuth = $newProxyAuth; }
	public function SetProxyCurlOption($newCurlOption) { $this->proxyCurlOption = $newCurlOption; }
	public function SetProxyCurlValue($newCurlValue) { $this->proxyCurlValue = $newCurlValue; }
	public function SetCertificatePath($newCertificatePath) { $this->certificatePath = $newCertificatePath; }
	public function SetCertificateVerifyPeer($newVerifyHostPeer) { $this->certificateVerifyPeer = $newVerifyHostPeer; }
	public function SetCertificateVerifyHost($newVerifyHostValue) { $this->certificateVerifyHost = $newVerifyHostValue; }
	public function SetGatewayUrl($newGatewayUrl) { $this->gatewayUrl = $newGatewayUrl; }
	public function SetDebug($debugBool) { $this->debug = $debugBool; }
	public function SetVersion($newVersion) { $this->version = $newVersion; }
	public function SetMerchantId($merchantId) {$this->merchantId = $merchantId; }
	public function SetPassword($password) { $this->password = $password; }
	public function SetApiUsername($apiUsername) { $this->apiUsername = $apiUsername; }
}