<?php

class Products {

	static function insert($info)
	{

	    //New change, SKUs can now have Duplicates -- 1/26/2011 - Jimmy

//	    //Check for anything with the same SKU, if it exists fail!
//
//		$check = Products::get1ByModel( $info['vendor_sku'], false );
//		//print_ar( $check );
//		if( $check )
//		    return false;

		if( $info['vendor_sku'] )
		    $info['vendor_sku'] = trim( $info['vendor_sku'] );

		$product_id = db_insert('products',$info,'date_added');

		$product = self::get1( $product_id );
		self::setURL( $product );

		
		Tracking::trackInsert( 'product', $product_id );
		
		return $product_id;
	}

	static function update($id,$info, $update_url=true)
	{
	    if( $info['vendor_sku'] )
		    $info['vendor_sku'] = trim( $info['vendor_sku'] );

	   //No longer a vendor_sku check... can be duplicates
//	    $check = Products::get1ByModel( $info['vendor_sku'], false );
//	    if( $check && $check['id'] != $id && $check['is_active'] == 'Y' && $check['is_deleted'] == 'N' )
//	    {
//		return false;
//	    }
	    
	    $tracking_id = Tracking::trackUpdate( 0, 'product', $id, 'update' );

		if($info['weight']){
		//	$info['weight'] = preg_replace("/[^0-9.]/",'',$info['weight']);
		}
		
		$result = db_update('products',$id,$info, 'id', 'date_updated' );

		if ($update_url)
		{
		  $product = self::get1( $id );
		  self::setURL($product, true);
		}

	    $tracking = Tracking::trackUpdate( $tracking_id );
		
		return $result;
	}

	static function delete($id)
	{
		if( !$id )
			return false;
		$tracking_id = Tracking::trackUpdate( 0, 'product', $id, 'update' );
		$info['is_deleted'] = 'Y';
		$info['is_active'] = 'N';

		$options = Products::getProductOptions( 0, $id );
		if( $options )
			foreach( $options as $option )
			{
				Products::deleteProductOption( $option['id'] );
			}

		$result = Products::update( $id, $info, false );
		
		$tracking = Tracking::trackUpdate( $tracking_id );
		
		return $result;
	}

	static function search($keywords, $active = true, $brand_id=0)
	{
		$keywords = trim( $keywords );
		$search_string = '%'.str_replace(' ','%',  addslashes($keywords) ).'%';
		
		$brand_id = (int) $brand_id;
        $q_sku = '';
//print_ar( $brand_id );
	    if ($keywords && $keywords != '') {
            $q_sku = "SELECT products.*,
                                brands.name AS brand_name,
                                cats.id AS cat_id, cats.name AS cat_name
                         FROM products
                         LEFT JOIN brands ON brands.id = products.brand_id
                         LEFT JOIN product_cats ON product_cats.product_id = products.id
                         LEFT JOIN cats ON cats.id = product_cats.cat_id
                         LEFT JOIN product_options ON product_options.product_id = products.id
                         WHERE products.is_active = 'Y'
                         AND products.website = 'Y'
			 AND products.is_accessory = 'N'
                         AND ";
            $fields = Array('products.vendor_sku', 'product_options.vendor_sku', 'products.aka_sku', 'products.id');

            $q_sku_keywords = db_split_keywords($keywords, $fields, 'OR', true, true);

            $q_sku .= "(" . $q_sku_keywords . ")";

	    if( $brand_id )
		$q_sku .= " AND products.brand_id = $brand_id ";

       		$q_sku .= " GROUP BY products.id ORDER BY products.name";

		//echo $q_sku;
        }

		$q = " SELECT products.*,
						brands.name AS brand_name,
						cats.id AS cat_id,cats.name AS cat_name
					 FROM products
					 LEFT JOIN brands ON brands.id = products.brand_id
					 LEFT JOIN product_cats ON product_cats.product_id = products.id
					 LEFT JOIN cats ON cats.id = product_cats.cat_id
					 LEFT JOIN product_options ON product_options.product_id = products.id
					 WHERE products.is_active = 'Y'
					 AND products.website = 'Y'
					 AND products.is_accessory = 'N'
					  ";
		
		
		if( $brand_id )
			$q .= " AND products.brand_id = $brand_id ";
		
		$q .= " AND
					 		(	";
					 		
	    if ($keywords && $keywords != '') {


            $fields = Array('products.name',
			    //'products.description',
			    'products.vendor_sku',
			    'products.id',
			    'product_options.vendor_sku',
			    'products.tagline',
			    'cats.name',
			    'cats.cat_keywords',
			    'products.aka_sku'  );

            $q .= db_split_keywords($keywords,$fields,'AND',true);

	    //Adding a hardcoded exception to this rule
//	    if( strtolower($keywords) != 'swarovski' ) {
//
//		if(!(strpos($keywords,' ') > 0) && strlen($keywords) > 5){
//		    $i = 0;
////			while(strlen($keywords)-$i > 4){
////			    $i++;
////				$q .= " OR ".db_split_keywords(substr($keywords,0,($i*-1)),$fields,'OR',true)." \n ";
////			}
//		    $q .= " OR ".db_split_keywords(substr($keywords,0,(3)),$fields,'OR',true)." \n ";
//		}
//	    }
        }
        else{
        	$q .= "1";
        }
        
        /* $q .= " 
   								OR
   									products.vendor_sku LIKE ".db_escape($search_string)."
   								OR
   									products.name LIKE ".db_escape($search_string)."
   								OR
   									products.tagline LIKE ".db_escape($search_string)."
   								OR 
   									product_options.vendor_sku LIKE ".db_escape($search_string). "*/
   									
   		$q .= "				) GROUP BY products.id ORDER BY products.name";	
		
        //echo "<pre>$q</pre>";
        //print_ar($q);
        

        if ($q_sku) {
            $sql = '(' . $q_sku . ') UNION DISTINCT (' . $q . ')';
            //print_ar($sql);
            return db_query_array($sql);
        }
        else {
            return db_query_array($q);
        }
	}


	static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='',$cat_id=0,$active='',$homepage='',$featured='', $occ_id=0, $brand_id=0,
				 $start_price = 0, $end_price = 0, $vendor_id = 0, $width = 0, $color = 0, $height=0, $depth=0, $group_by='',
				 $colors=false, $include_subs=false, $filter_query='', $total=false, $sku ="", $website='', $filters='', $limit=0, $start=0, $unbuffered=false,
				 $is_deleted='N',$is_available='', $backend=true, $keyword_search=array(), $upc='',$is_accessory='', $return_cats=false, $cats_is_hidden="N" )
	{
		if( $total )
		{
			$sql .= " SELECT count( distinct products.id) as total ";
			if( $cat_id )
			{
				if( is_array( $cat_id ) )
				{
					$cats = $cat_id;
				}
				else
				{
					if( $include_subs )
					{
						$cats = array();
						$cats[] = $cat_id;
						$cats = Cats::getAllSubs( $cat_id, $cats, $backend );
						//print_ar( $cats );
					}
					else
						$cats = $cat_id;
				}

					//Should I include all the sub cats on the filters too?

					if( !is_array( $filters ) )
						$filters = Filters::getFiltersForCat( $cats );
					//print_ar( $filters );
					$num_filters = count( $filters );
			}
		}
		else
		{

		    if( $return_cats )
		    {
			$sql = " SELECT cats.name, count( distinct products.id ) as total, cats.id, cats.pid, cats.url_name ";
		    }
		    else {
				$sql = "SELECT products.*,
					    brands.name AS brand_name,
					    cats.id AS cat_id,cats.name AS cat_name ";
		    }

	/*		if($keywords){
				print_ar($keywords);

			}
				 /*   $fields = (count($keyword_search) > 0) ? $keyword_search : array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		//		    //$fields = $keyword_search;
		//
		//            //$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		//		    $sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);

				    //Need to use the keyword matching and expand the kewywords to include what I need

				    $keywords = KeywordMatch::buildKeywords( $keywords );


				    $fields = implode( ",", $fields );
				    $sql .= ", MATCH( products.name, products.description ) AGAINST ('" . addslashes($keywords) . "') as relevance ";

				    $search_skus = true;
				} */

//			        		occasions.name as occ_name,
//			       			 occasions.id as occ_id,
//							occasions.id as occasion_id ";
			if( $color || $colors )
			{
				$sql .= " , product_options.value ";
			}

			//When I have a specific cat_id I need to get all of the cat specific filters for that cat, and they need to be
			//sortable, and filterable
			if( $cat_id )
			{
				if( is_array( $cat_id ) )
				{
					$cats = $cat_id;
				}
				else
				{
					$cat = Cats::get1( $cat_id );
					if( $include_subs && $cat['pid'] )
					{
						$cats = array();
						$cats[] = $cat_id;
						//echo'ran this here <br />';
						$cats = Cats::getAllSubs( $cat_id, $cats, $backend );

						//print_ar( $cats );
					}
					else
						$cats = array($cat_id);

				}

					//Should I include all the sub cats on the filters too?
					if( !is_array( $filters ) )
						$filters = Filters::getFiltersForCat( $cats );
					//print_ar( $filters );
					//print_ar( $filters );
					$num_filters = count( $filters );
			}
		}


		$sql .= "FROM products
				LEFT JOIN brands ON brands.id = products.brand_id
				LEFT JOIN product_cats ON product_cats.product_id = products.id ";


		$sql .= "	LEFT JOIN cats ON cats.id = product_cats.cat_id ";
//				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
//				LEFT JOIN occasions ON occasions.id = product_occasions.occasion_id ";
		if( $num_filters )
		{
			$i =1;
			foreach( $filters as $filter )
			{
				if( $filter['display'] != 'N' )
					$sql .= " LEFT JOIN product_filters as filter$filter[filter_id] ON filter$filter[filter_id].product_id = products.id AND filter$filter[filter_id].filter_id = $filter[filter_id] ";
				++$i;
			}
		}


			$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id
				  LEFT JOIN options on options.id = product_options.option_id ";

		$sql .= " WHERE 1 ";

		if ($id > 0) {
			$sql .= " AND products.id = $id ";
		}

		if ($keywords){
		    
			//$sql .= "AND products.name LIKE '%" . addslashes($keywords) . "%' ";
		
		}
		if( is_array( $brand_id ) )
		{
			$sql .= " AND products.brand_id IN ('" . implode("','", $brand_id) . "') ";
		}
		elseif ($brand_id > 0) {
			$sql .= " AND products.brand_id = $brand_id ";
		}
		if ($active !== '') {
			$sql .= " AND products.is_active = '".($active ? 'Y' : 'N')."' ";
		}
		if ($homepage !== '') {
			$sql .= " AND products.show_on_homepage = '".($homepage ? 'Y' : 'N')."' ";
		}
		if ($featured !== '') {
			$sql .= " AND products.is_featured = '".($featured ? 'Y' : 'N')."' ";
		}
		if ($website !== '') {
			$sql .= " AND products.website = '".($website ? 'Y' : 'N')."' ";
		}

		if ($start_price > 0 && $end_price > 0) {
			$sql .= " AND products.price BETWEEN $start_price AND $end_price ";
		}
		else if ($start_price > 0) {
			$sql .= " AND products.price >= $start_price ";
		}
		else if ($end_price > 0) {
			$sql .= " AND products.price <= $end_price ";
		}

		if ($begins_with != '') {
			$sql .= " AND products.vendor_sku LIKE '".addslashes($begins_with)."%'";
		}
		
		if (trim($keywords) != '') {
			
			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}	
		
		/*if (trim($keywords) != '') {

		    $fields = (count($keyword_search) > 0) ? $keyword_search : array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
//		    //$fields = $keyword_search;
//
//            //$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
		    if( $order == 'relevance' )
			$sql .= " AND (" . db_split_keywords($keywords,$fields,'OR',true) . ") ";
		    else
			$sql .= " AND (" . db_split_keywords($keywords,$fields,'AND',false) . ") ";
		}*/
		
		
		if( is_array($cat_id) )
		{
			$sql .= " AND product_cats.cat_id IN ('" . implode("','", $cat_id). "') ";
                        
                                

		}
		elseif ($cat_id > 0) {

			if( $include_subs  )
			{
				if( !$cats )
				{
					//echo'ran this here <br />';
					$cats = array();
					$cats[] = $cat_id;
					$cats = Cats::getAllSubs( $cat_id, $cats, $backend );
				}
				//print_ar( $cats );
				$sql .= " AND product_cats.cat_id IN ('" . implode("','", $cats) . "') ";

			}
			else
			{
				$sql .= " AND product_cats.cat_id = $cat_id ";
                                
                                
			}
		}

		if ($occ_id > 0) {
			$sql .= " AND product_occasions.occasion_id = $occ_id ";
		}

		if ($vendor_id > 0) {
			$sql .= db_restrict('products.vendor_id', $vendor_id);
		}

		if( $width )
		{
			$sql .= " AND products.width = $width ";
		}
		if( $color )
		{
			$sql .= " AND product_options.value like '$color%' ";
		}

		if( $sku )
		{
			$sql .= " AND ( products.vendor_sku = UPPER('$sku') OR products.aka_sku = UPPER('$sku') ) ";
		}

		if( $is_deleted )
			$sql .= " AND products.is_deleted = '$is_deleted' ";

		if( $is_available )
			$sql .= " AND products.is_available = '".addslashes($is_available)."' ";

		if( $upc )
		    $sql .= " AND products.upc = '" . addslashes( $upc ) . "' ";

		if($is_accessory){
			$sql .= " AND products.is_accessory = '".db_esc($is_accessory)."' ";
		}

                if($cats_is_hidden == 'N')
                {
                    $sql .= " AND cats.is_hidden = 'N'";
                }

		$relavance_search = false;
		if( $filter_query )
		{
			$sql .= $filter_query;
		}
		if( !$total )
		{
		     if( $return_cats )
			{
	//		 if( $order == 'relevance' )
//				$relavance_search = true;

			    $group_by = "cats.id";
			    $order = "cats.name";
			    $order_asc = true;
			}

			if( $group_by )
			{
				$sql .= " GROUP BY $group_by ";
			}
			else
			{
				$sql .= " GROUP BY products.id
						   ";
			}

		//	if( ($order == 'relevance' || $relavance_search) && $keywords )
		//	{
		//	    $sql .= " HAVING relevance > 0 ";
	//		}

			$sql .= " ORDER BY ";

			if ($order == '') {
				$sql .= 'products.price,brands.name,products.name';
			}
			else {
				$sql .= addslashes($order);
			}

			if ($order_asc !== '' && !$order_asc) {
				$sql .= ' DESC ';
			}



			if( $limit )
			{
				$start = (int)$start;
				$limit = (int)$limit;
				$sql .=" limit $start, $limit ";
			}


		}
//		if( $keywords )
//		if( $_REQUEST['debug'] == 3 )
//		echo $sql . '<br /><br />';
		//print_ar( $cats );

                //print_ar($sql);
                //echo "\n\n$sql\n\n";
		$xStart = microtime(true);
		if( $return_cats )
		    $result = db_query_array($sql,'id');
		else
		    $result = db_query_array($sql);
		$xDur = (microtime(true) - $xStart)*1000;

		if($_GET['debug'] == 1){
			echo "<!-- product::get $xDur -->";
		}

		return $result;
	}

	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false; 

		$o = (object) array();
		$o->id = $id;

		return self::get1ByO( $o );
	}

	static function get1ByUPC( $upc )
	{
	    if( !$upc )
		return false;

	    $product = db_get1( self::get( 0,'','','','',0,'','','', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', false, false, '', false, "", '', '', 0, 0, false, 'N','', true, array(), $upc  ));

	    if( !$product )
	    {
		$product = db_get1( self::getProductOptions( 0,0,false,true, '', true, 0, 0, 'Y', 'N', $upc ) );
		if( $product )
		{
		    $product['upc_type'] = 'option';
		}
	    }
	    else {
		$product['upc_type'] = 'product';
	    }

	    if( $product )
	        return $product;

	    return false;
	}

	static function get1ByModel( $model='', $only_active_prods = true )
	{
		//On their import sheet they said that I is replaced with 1, want me to understand that
		if( !$model )
			return false;
		$model = addslashes( $model );

		if($only_active_prods){
			$is_active = 'Y';
		}
		else{
			$is_active = '';
		}
		$result = Products::get( 0,'','','','',0,$is_active,'','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, $model,'','',0,0,false,'N','',true,array(),'','',false, '');

		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(vendor_sku) = UPPER('$model') AND is_active = 'Y' and is_deleted='N'  ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }
				$result[0]['is_option'] = true;
				return $result[0];
			}
		}
		//Replace I with 1 and check again, the reason I did this is due to the off chance that 1 of both exsists, I only
		//want to find 1
		$model = strtoupper( $model );
		$model = str_replace( 'I', '1', $model );

		$result = Products::get( 0,'','','','',0,'Y','','',0,0,0,0,0,0,0,0,0,'',false,false,'',false, $model);
		if( $result[0] )
		{
		    if( count( $result ) > 1 )
		    {
			$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
		    }

			$result[0]['is_option'] = false;
			return $result[0];
		}
		else
		{
			//check product options for the model
			$sql = " SELECT * FROM product_options where UPPER(vendor_sku) = UPPER('$model') ";
			$result = db_query_array( $sql );
			if( $result[0] )
			{
			    if( count( $result ) > 1 )
			    {
				$result[0]['warning'] = 'Multiple Products found matching '. $model . '<br />';
			    }

				$result[0]['is_option'] = true;
				return $result[0];
			}

			return false;
		}
	}

	static function getModelList( $only_products = false, $starts_with='' )
	{
		//Get an array of all of the models in 1 query, then I can find the price imports off this
		$sql = " select products.id as product_id, UPPER(products.vendor_sku) as vendor_sku, products.name, 'product' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM products ";
		$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$sql .= " WHERE 1 AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y' ";
		if( $starts_with )
			$sql .= " AND products.vendor_sku like '$starts_with%' ";
		$sql .= " UNION select products.id as product_id, UPPER(products.aka_sku) as vendor_sku, products.name, 'product' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM products ";
		$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
		$sql .= " where products.aka_sku <> '' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y'";
		if( $starts_with )
			$sql .= " AND products.vendor_sku like '$starts_with%' ";
		if( !$only_products )
		{
			$sql .= " UNION select product_options.id as product_id, UPPER(product_options.vendor_sku) as vendor_sku, product_options.value as name, 'option' as type, brands.name as brand_name, products.name as description, products.id as parent_id from product_options ";
			$sql .= " LEFT JOIN products on products.id = product_options.product_id ";
			$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";
			$sql .= " WHERE 1 AND product_options.is_active = 'Y' and product_options.is_deleted = 'N' and product_options.website = 'Y' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y' ";
			if( $starts_with )
				$sql .= " AND products.vendor_sku like '$starts_with%' ";

			$sql .= " UNION select product_options.id as product_id, UPPER(product_options.aka_sku) as vendor_sku, product_options.value as name, 'option' as type, brands.name as brand_name, products.name as description, products.id as parent_id FROM product_options ";
			$sql .= " LEFT JOIN products on products.id = product_options.product_id ";
			$sql .= " LEFT JOIN brands on brands.id = products.brand_id ";

			$sql .= " where product_options.aka_sku <> '' AND product_options.is_active = 'Y' and product_options.is_deleted = 'N' and product_options.website = 'Y' AND products.is_active = 'Y' and products.is_deleted = 'N' and products.website = 'Y'";
			if( $starts_with )
				$sql .= " AND product_options.vendor_sku like '$starts_with%' ";
		}

		$sql .= " ORDER BY brand_name, TRIM(vendor_sku) ";

		return db_query_array( $sql, 'vendor_sku');
	}

	static function getProductWidth( $id=0, $cat_id=0, $total=false, $start_price=0, $end_price=0, $color=0, $brand=0 )
	{
		$sql = " SELECT products.width ";
		if( $total )
		{
			$sql .= ", count(products.id) as total ";
		}
		$sql .= " FROM products ";
		$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
							LEFT JOIN cats ON cats.id = product_cats.cat_id ";
		if( $color )
		{
			$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id
					  LEFT JOIN options on options.id = product_options.option_id ";
		}

		$sql .= " WHERE 1 ";

		if( $id )
		{
			$sql .= " AND id = $id ";
		}
		if ($cat_id > 0) {
			$sql .= " AND product_cats.cat_id = $cat_id ";
		}
		if( $start_price )
		{
			$sql .= " AND products.price >= $start_price ";
		}
		if( $end_price )
		{
			$sql .= " AND products.price <= $end_price ";
		}
		if( $color )
		{
			$sql .= " AND product_options.value = '$color' ";
		}
		if( $brand )
		{
			$sql .= " AND products.brand_id = $brand ";
		}
		if( $total )
		{
			$sql .= " GROUP BY products.width ";
		}

		return db_query_array( $sql );
	}

	static function getProductsByColor( $id=0, $cat_id=0, $total=true, $start_price=0, $end_price=0, $width=0, $brand=0 )
	{
		$sql = " SELECT product_options.value ";
		if( $total )
		{
			$sql .= ", count(products.id) as total ";
		}
		$sql .= " FROM products ";
		$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
							LEFT JOIN cats ON cats.id = product_cats.cat_id
							LEFT JOIN product_options ON product_options.product_id = products.id
							LEFT JOIN options on options.id = product_options.option_id ";

		$sql .= " WHERE 1 ";

		if( $id )
		{
			$sql .= " AND id = $id ";
		}
		if ($cat_id > 0) {
			$sql .= " AND product_cats.cat_id = $cat_id ";
		}
		if( $start_price )
		{
			$sql .= " AND products.price >= $start_price ";
		}
		if( $end_price )
		{
			$sql .= " AND products.price <= $end_price ";
		}
		if( $width )
		{
			$sql .= " AND products.width = $width ";
		}
		if( $brand )
		{
			$sql .= " AND products.brand_id = $brand ";
		}

		$sql .= " AND product_options.value IS NOT NULL ";

		if( $total )
		{
			$sql .= " GROUP BY product_options.value ";
		}


		//echo $sql;
		return db_query_array( $sql );
	}

	static function getProductsByRanges( $ranges, $cat_id = 0, $width=0, $color=0, $brand=0 )
	{
		if( !is_array( $ranges ) )
			return false;
		$ret = array();
		foreach( $ranges as $range )
		{
			$sql = " SELECT count(*) as total FROM products ";
			if( $cat_id )
			{
				$sql .= " LEFT JOIN product_cats ON product_cats.product_id = products.id
									LEFT JOIN cats ON cats.id = product_cats.cat_id ";
			}
			if( $color )
			{
				$sql .= " LEFT JOIN product_options ON product_options.product_id = products.id
						  LEFT JOIN options on options.id = product_options.option_id ";
			}
			$sql .= " where price >= '$range[start]' AND price <= '$range[end]' ";
			if( $width )
			{
				$sql .= " AND products.width = $width ";
			}
			if( $cat_id )
				$sql .= " AND product_cats.cat_id = $cat_id ";
			if( $color )
			{
				$sql .= " AND product_options.value = '$color' ";
			}
			if( $brand )
			{
				$sql .= " AND products.brand_id = $brand ";
			}
			$total = db_get1(db_query_array($sql));
			$ret[] = array('start'=>$range['start'], 'end'=>$range['end'], 'total'=>$total['total'] );
		}

		return $ret;
	}

	static function getHomePageProducts()
	{
		return Products::get(0,'','','products.homepage_order','',0,'Y','Y');
	}

	static function getFeaturedProducts($cat_id = 0)
	{
		return Products::get(0,'','','','',$cat_id,'Y','','Y');
	}

	static function getProductsForCat($cat_id,$occ_id=0,$start_price=0,$end_price=0, $width=0, $color=0, $brand=0, $order='products.tagline, products.name', $order_asc='', $backend=true, $total = false, $limit=0, $start=0, $is_accessory = '',$keywords=null)
	{
		return Products::get(0,'',$keywords,$order,$order_asc,$cat_id,'Y','','',$occ_id,$brand,$start_price,$end_price,0,$width,$color,0,0,'',false,true, '', $total, '', '', '', $limit, $start, false, 'N', '', $backend,'','',$is_accessory);
	}

	static function getProductsForOcc($occ_id)
	{

		return Products::get(0,'','','products.sort','',0,'Y', '', '', $occ_id);
	}

	static function associate2Cats($product_id,$cat_ids,$delete_old_ones='Y')
	{
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";

		if (trim($cat_ids_str) == '') {
			db_delete('product_cats',$product_id,'product_id');
			return true;
		}

		// delete old ones

                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM product_cats WHERE product_id = $product_id AND cat_id NOT IN ($cat_ids_str)");
                }

		

		// insert new ones

		$info = Array('product_id'=>$product_id);

		foreach ($cat_ids as $cat_id) {

		    if( $cat_id <= 0 )
			continue;

			$info['cat_id'] = $cat_id;
			db_insert('product_cats',$info,'',true);
		}

		return true;
	}

	static function associate2Occasions($product_id,$occasion_ids)
	{
		if (!is_array($occasion_ids)) {
			$occasion_ids = Array($occasion_ids);
		}

		$occasion_ids_str = implode(',',$occasion_ids);

		if (trim($occasion_ids_str) == '') {
			db_delete('product_occasions',$product_id,'product_id');
			return true;
		}

		// delete old ones

		$sql = "DELETE FROM product_occasions WHERE product_id = $product_id AND occasion_id NOT IN ($occasion_ids_str)";

		db_query($sql);

		// insert new ones

		$info = Array('product_id'=>$product_id);

		foreach ($occasion_ids as $occasion_id) {
			$info['occasion_id'] = $occasion_id;
			db_insert('product_occasions',$info,'',true);
		}

		return true;
	}

	static function getProductCats($product_id)
	{
		if( !$product_id )
			return false;
		return db_query_array("SELECT cat_id FROM product_cats WHERE product_id = $product_id",'cat_id');
	}

	static function getProductOccasions($product_id)
	{
		return db_query_array("SELECT product_occasions.*, occasions.name as occasion_name, occasions.name_english AS occasion_name_english FROM product_occasions LEFT JOIN occasions ON (occasions.id = product_occasions.occasion_id) WHERE product_id = $product_id",'occasion_id');
	}

	static function insertLinkType($info)
	{
		return db_insert('link_types',$info);
	}

	static function updateLinkType($id,$info)
	{
		return db_update('link_types',$id,$info);
	}

	static function deleteLinkType($id)
	{
		return db_delete('link_types',$id);
	}

	static function getLinkTypes($id=0)
	{
		$sql = "SELECT link_types.*
				FROM link_types
				WHERE 1 ";

		if ($id > 0)
			$sql .= " AND link_types.id = $id ";

		$sql .= " ORDER BY link_types.name ";

		return db_query_array($sql);
	}

	static function getLinkType($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Products::getLinkTypes($id);
		return $result[0];
	}

	static function insertProductLink($product_id1,$product_id2,$link_type_id,$option_id=0)
	{
		$info = Array(
			'product_id1'  => $product_id1,
			'product_id2'  => $product_id2,
			'link_type_id' => $link_type_id,
			'option_id'    => $option_id
		);

		return db_insert('product_links',$info,'',true,false,false);
	}

	static function updateProductLink($orig_product_id1,$orig_product_id2,$product_id1,$product_id2,$link_type_id,$option_id=0)
	{
		db_query("UPDATE product_links
				  SET product_id1=$product_id1, product_id2=$product_id2, link_type_id=$link_type_id, option_id=$option_id
				  WHERE product_id1=$orig_product_id1 AND product_id2=$orig_product_id2");
		return db_affected_rows();
	}

	static function deleteProductLink($id)
	{
		db_query("DELETE FROM product_links WHERE id=$id ");
		return db_affected_rows();
	}

	static function getLinksForProduct($product_id,$group_by_type=false,$type_id=0, $order_by ='')
	{
		if( !$product_id )
			return false;

		$sql = "SELECT products.*,
					link_types.name AS link_type_name,
					product_links.link_type_id,
					product_links.option_id,
					product_links.id as link_id,
					brands.name AS brand_name,
					cats.id AS cat_id,cats.name AS cat_name
				FROM product_links
				LEFT JOIN products ON products.id = product_links.product_id2
				LEFT JOIN link_types ON link_types.id = product_links.link_type_id
				LEFT JOIN brands ON brands.id = products.brand_id
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				LEFT JOIN cats ON cats.id = product_cats.cat_id ";

		$sql .= " WHERE product_id1 = $product_id AND products.is_active = 'Y' ";

		if( $type_id )
		{
			$sql .= " AND product_links.link_type_id = $type_id ";
		}

		$sql .= "	GROUP BY product_links.id
				ORDER BY
				";

		if( $order_by )
		{
			$sql .= $order_by;
		}
		else
		{
			$sql .= " link_types.name,products.price ";
		}

		$result = db_query_array($sql);

		if (!$group_by_type) {
			return $result;
		}
		else {
			if ($result) {
				foreach ($result as $row) {
					$links[$row['link_type_name']][] = $row;
				}
				return $links;
			}
			else {
				return false;
			}
		}
	}

	static function getProductLink($id)
	{
		$sql = "SELECT *
				FROM product_links
				WHERE id = $id ";

		return db_query_array($sql,'',true);
	}

	static function insertOption($info)
	{
		return db_insert('options',$info);
	}

	static function updateOption($id,$info)
	{
		return db_update('options',$id,$info);
	}

	static function deleteOption($id)
	{
		return db_delete('options',$id);
	}

	static function getOptions($id = 0, $is_free = '')
	{
		$sql = "SELECT options.*
				FROM options
				WHERE 1 ";

		if ($id > 0)
			$sql .= " AND options.id = $id ";

		if($is_free != '')
			$sql .= " AND options.is_free = '$is_free' ";

		$sql .= " ORDER BY options.order_fld,options.name ";

		return db_query_array($sql);
	}

	static function getOption($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Products::getOptions($id);
		return $result[0];
	}

	static function insertProductOption($info, $prices = '')
	{
		$id = db_insert('product_options', $info);

		if(is_array($prices)){
			foreach($prices as $qty => $price){
				$sql = "INSERT INTO product_option_prices SET product_option_id = '$id', quantity = '$qty', price = '$price' ";
				db_query($sql);
			}
		}

		return $id;
	}

	static function insertProductFilter( $info )
	{
		return db_insert( 'product_filters', $info );
	}

	 static function insertProductPDF( $info )
	 {
	 	return db_insert( 'product_pdfs', $info );
	 }

	static function updateProductPDF($id,$info)
	{
		return db_update('product_pdfs',$id,$info);
	}

	static function deleteProductPDF($id)
	{
		//db_delete('product_option_prices',$id,'product_option_id');

		return db_delete('product_pdfs',$id);
	}

	static function updateProductOption($id,$info,$prices = '')
	{
		$result = db_update('product_options',$id,$info, 'id', 'date_updated');

		return $result;
	}

	function deleteProductOption($id)
	{
		if( !$id )
			return false;
		$info['is_active'] = "N";
		$info['is_deleted'] = "Y";

		return Products::updateProductOption( $id, $info );
		//db_delete('product_option_prices',$id,'product_option_id');

		//return db_delete('product_options',$id);
	}


	static function getProductOptions($product_option_id=0,$product_id=0,$for_package=false,
					  $with_prices = true, $order='', $associate=true, $start_price=0, $end_price=0,
				          $is_active='Y', $is_deleted='N', $upc='' )
	{
		$sql = "SELECT product_options.*,
					options.name AS option_name,
					options.display_name_override,
					options.is_upgrade,
					options.is_free,
					sub_options.name AS suboption_name,
					products.name AS product_name,
					brands.name as brand_name
				FROM product_options
				LEFT JOIN options ON options.id = product_options.option_id
				LEFT JOIN options AS sub_options ON sub_options.id = product_options.suboption_id
				LEFT JOIN products ON products.id = product_options.product_id
				LEFT JOIN brands ON brands.id = products.brand_id
				WHERE 1";

		if ($product_option_id > 0)
			$sql .= " AND product_options.id = $product_option_id ";
		if ($product_id > 0)
			$sql .= " AND product_options.product_id = $product_id ";
		if ($for_package)
			$sql .= " AND product_options.include_in_packages = 'Y' ";

		if ($start_price > 0 && $end_price > 0) {
			$sql .= " AND product_options.additional_price BETWEEN $start_price AND $end_price ";
		}
		else if ($start_price > 0) {
			$sql .= " AND product_options.additional_price >= $start_price ";
		}
		else if ($end_price > 0) {
			$sql .= " AND product_options.additional_price <= $end_price ";
		}

		if( $is_active )
			$sql .= " AND product_options.is_active = '$is_active' ";
		if( $is_deleted )
			$sql .= " AND product_options.is_deleted = '$is_deleted' ";

		if( $upc )
		    $sql .= " AND product_options.upc = '" . addslashes( $upc ) . "' ";

		if( $order )
		{
			$sql .= " ORDER BY $order ";
		}
		else
		{
			$sql .= " ORDER BY product_options.additional_price ASC ";
		}
		if( $associate )
			$options = db_query_array($sql,'id');
		else
			$options = db_query_array($sql );

		return $options;
	}


	static function getProductOptionsStruct($product_id)
	{
		$result = array();
		$options = self::getProductOptions(0, $product_id);

		if(is_array($options))
		foreach($options as $option){
			$result[$option['option_id']][] = $option;
		}

		return $result;
	}

	static function getOptionsForProduct($product_id, $backend=false)
	{
	    if( $backend )
	    {
		$active = '';
		$deleted = '';
	    } else {
		$active = 'Y';
		$deleted = 'N';
	    }
	    
		return Products::getProductOptions(0,$product_id,false,true,'order_fld ASC',true,0,0,$active,$deleted);
	}


	static function getProductOptionPrices($product_option_id)
	{
		$sql = "SELECT * FROM product_option_prices WHERE product_option_id = '$product_option_id' ";
		return db_query_array($sql,'quantity',false,false,'price');
	}


	static function getProductOption($product_option_id, $backend = false)
	{
		$product_option_id = (int) $product_option_id;
		if (!$product_option_id) return false;

		if( $backend )
		{
		    $active = '';
		    $deleted = '';
		} else {
		    $active = 'Y';
		    $deleted = 'N';
		}
		
		$result = Products::getProductOptions($product_option_id,0,false,true,'',true,0,0,$active, $deleted);
		return $result[$product_option_id];
	}

	static function changeDefault($product_option_id,$info)
	{
		$product_id = $info[product_id];
		//$option_id = $info[option_id];
		if(!$product_id) return false;
		$sql = "UPDATE product_options
					SET is_default = 'N'
						WHERE product_id = $product_id
							AND is_default = 'Y'
							AND option_id = '$info[option_id]'
							AND id <> $product_option_id";
		//echo $sql;
		db_query($sql);
	}

	static function getBizrateCategories() {
		$sql = "SELECT id, category
				FROM bizrate_categories
				ORDER BY category ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

	static function getShoppingComCategories() {
		$sql = "SELECT id, name
				FROM shopping_com_categories
				ORDER BY name ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

	static function getPriceGrabberCategories() {
		$sql = "SELECT id, name
				FROM price_grabber_cats
				ORDER BY name ASC";
		$cats = db_query_array($sql);
		return $cats;
	}

   static function getByOccasionIDVendorID($occ_id=0,$vendor_id=0,$product_id=0)
	{
		$sql = "SELECT products.*, product_cats.cat_id
				FROM products
				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
				LEFT JOIN product_cats ON products.id=product_cats.product_id
				WHERE products.id <> $product_id AND products.is_active = 'Y'";
		if (is_array($vendor_id)) {
			$sql .= " AND products.vendor_id IN (". implode(",", $vendor_id) .")";
		}
		if (is_array($occ_id)) {
			$sql .= " AND product_occasions.occasion_id IN (". implode(",", $occ_id) .")";
		}

		$sql .= " GROUP BY products.id ";

		return db_query_array($sql,"id");
	}

	static function getByOccasionID($occ_id=0, $product_id=0)
	{
		$sql = "SELECT products.*, product_occasions.occasion_id AS occ_id
				FROM products
				LEFT JOIN product_occasions ON product_occasions.product_id = products.id
				WHERE products.is_active = 'Y' AND products.id <> $product_id";

		if ($occ_id <> 0) {
			$sql .= " AND product_occasions.occasion_id IN (". implode(",", $occ_id) .")";
		}

		$sql .= " GROUP BY products.id ";

		return db_query_array($sql,"id");
	}

	static function getByCatID($cat_id=0, $product_id=0, $order='', $order_asc=false )
	{
		$sql = "SELECT products.*, product_cats.cat_id
				FROM products
				LEFT JOIN product_cats ON product_cats.product_id = products.id
				WHERE products.is_active = 'Y' AND products.id <> $product_id";

		if (is_array($cat_id)) {
			$sql .= " AND product_cats.cat_id IN (". implode(",", $cat_id) .")";
		} elseif($cat_id) {
			$sql .= " AND product_cats.cat_id = '$cat_id'";
		}

		$sql .= " GROUP BY products.id ";

		$sql .= " ORDER BY ";

		if ($order == '') {
			$sql .= 'products.price,products.name';
		}
		else {
			$sql .= addslashes($order);
		}

		if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}


		return db_query_array($sql,"id");
	}


	static function getPopularProducts($cat_id = 0, $occ_id = 0, $limit = 12)
	{

		$sql = "SELECT products.*, product_cats.cat_id, COUNT(order_items.id) as num_sold
   				FROM products
   				LEFT JOIN order_items ON order_items.product_id = products.id
   				LEFT JOIN orders ON orders.id = order_items.order_id
   				LEFT JOIN product_cats ON products.id=product_cats.product_id
   				LEFT JOIN product_occasions ON products.id=product_occasions.product_id
   				WHERE products.is_active='Y' AND orders.date > DATE_SUB(CURDATE(),INTERVAL 90 DAY)";

		if($cat_id > 0){
			$sql .= " AND product_cats.cat_id ='$cat_id' ";
		}

		if($occ_id > 0){
			$sql .= " AND product_occasions.occasion_id ='$occ_id' ";
		}

   		$sql .= " GROUP BY products.id ORDER BY num_sold DESC, products.name LIMIT $limit";

		$prods = db_query_array($sql);

		return $prods;
	}


	static function getNewestProducts($cat_id = 0, $occ_id = 0, $limit = 12)
	{

		$sql = "SELECT products.*, IF(products.show_updated_new = 'Y',date_updated,date_added) AS date_added, product_cats.cat_id
   				FROM products
   				LEFT JOIN product_cats ON products.id=product_cats.product_id
   				LEFT JOIN product_occasions ON products.id=product_occasions.product_id
   				WHERE products.is_active='Y' ";

		if($cat_id > 0){
			$sql .= " AND product_cats.cat_id ='$cat_id' ";
		}

		if($occ_id > 0){
			$sql .= " AND product_occasions.occasion_id ='$occ_id' ";
		}

   		$sql .= " GROUP BY products.id ORDER BY date_added DESC, products.name LIMIT $limit";

		$prods = db_query_array($sql);

		return $prods;
	}



   //get suggested prodects
   static function getSuggestedProducts($product_id, $recommendations = false, $customers_who_bought = false){
	  if(!$customers_who_bought){
	   	  global $CFG;

		  $link_id = $recommendations ? $CFG->recommended_link_id : $CFG->related_link_id;

	   	  $result = array();

	      $sql = "SELECT products.id, products.name, products.vendor_sku, price, product_cats.cat_id, link_types.name AS link_type_name, brands.name as brand_name
	   				FROM products
	   				LEFT JOIN brands on brands.id = products.brand_id
	   				LEFT JOIN product_cats ON products.id = product_cats.product_id
	   				LEFT JOIN product_links ON (
	   					product_links.product_id1 = products.id OR
	   					product_links.product_id2 = products.id)
	   				LEFT JOIN link_types ON link_types.id = product_links.link_type_id
	   				WHERE
	   					products.is_active='Y' AND
	   					products.is_available='Y' AND
	   					products.id != '$product_id' AND
	   					( product_links.product_id1 = '$product_id' OR product_links.product_id2 = '$product_id' ) AND product_links.link_type_id = $link_id
	   				GROUP BY products.id
	   				ORDER BY products.name";

	      $data = db_query_array($sql);
	      if(is_array($data) && count($data))
	      foreach($data as $prod){
	      	  $result[$prod['link_type_name']][] = $prod;
	      }

		  return $result;
     }
      else{
	      $sql = "SELECT products.id, products.name, products.vendor_sku, products.price, product_cats.cat_id, COUNT(shared_items.id) AS order_count, SUM(shared_items.qty) AS order_qty, brands.name as brand_name
	   				FROM order_items

	   				LEFT JOIN order_items AS shared_items ON shared_items.order_id = order_items.order_id
	   				LEFT JOIN products ON products.id = shared_items.product_id
	   				LEFT JOIN product_cats ON products.id = product_cats.product_id
	   				LEFT JOIN brands on brands.id = products.brand_id
	   				WHERE
	   					order_items.product_id = '$product_id' AND
	   					shared_items.product_id != '$product_id' AND
	   					products.is_active='Y'
	   				GROUP BY products.id
	   				ORDER BY order_count DESC, order_qty DESC LIMIT 5";

	      $data = db_query_array($sql);
	      if(is_array($data) && count($data))
	      foreach($data as $prod){
	      	  $result['Customers who bought this also bought...'][] = $prod;
	      }
      }

      return $result;
   }

   static function associate2Filters( $product_id, $filters, $delete=true )
   {
		if (!is_array($filters)) {
			$filters = Array($filters);
		}
		//print_ar( $filters );
		$filter_ids_str = '';
		foreach( $filters as $key => $filter )
		{
			if( $filter_ids_str != '' )
			{
				$filter_ids_str .= ', ' . $key;
			}
			else
			{
				$filter_ids_str .= $key;
			}
		}
		//print_ar( $filters );
		if (@trim($filter_ids_str) == '') {
			db_delete('product_filters',$product_id,'product_id');
			return true;
		}

		// delete old ones
		if( $delete )
		{
		//	db_query("DELETE FROM product_filters WHERE product_id = $product_id AND filter_id NOT IN ($filter_ids_str)");
			db_query("DELETE FROM product_filters WHERE product_id = $product_id");
		}
		// insert new ones

		$info = Array('product_id'=>$product_id);
		$currentsql = " SELECT * FROM product_filters where product_id = $product_id ";
		$cur_filters = db_query_array( $currentsql, 'filter_id' );
		//print_ar( $filters );
		foreach ($filters as $key => $filter) {
			if ($filter != "0" && $filter != "")
			{ // don't enter 0 or blank values!!!
				$info['filter_id'] = $key;
				$info['value'] = $filter;
				//print_ar( $info );
				if( $cur_filters[ $key ] )
					db_query( 'UPDATE product_filters set value = "' . addslashes($filter) . '" where product_id = ' . $product_id . ' and filter_id = ' . $key );
				else
					db_insert('product_filters',$info,'',true);
			}
		}

		return true;
   }

   static function getProductFilter( $product_id, $filter_id )
   {
   	if( !$product_id || !$filter_id )
   		return false;
   	$sql .= "SELECT * FROM product_filters where product_id = $product_id and filter_id = $filter_id ";
 		$result = db_query_array( $sql );

 		if( $result )
 			return $result[0];
 		else return false;
   }

   static function getProductCatTree( $product_id )
   {
   	 //Get the all the cats for the product
   	 //return a tree of the cats
   	 $cats = Products::getProductCats( $product_id );
   	 $cat_list = array();
   	 if( $cats )
	   	 foreach( $cats as $cat )
	   	 {
	   	 	 $cat_list[] = $cat['cat_id'] ;
	   	 }
	   else
	   	$cats = array();
   	 //Need to parse the cat_list and return a tree
   	 $cats = Cats::buildCatsTree( $cat_list );
   	 //print_ar( $cats );

   	 return $cats;
   }

   static function getProductFilterValues( $product_id, $cat_id=0 )
   {

 		$sql = " SELECT filters.name, product_filters.value, product_filters.filter_id FROM product_filters ";
   		$sql .= " LEFT JOIN filters on filters.id = product_filters.filter_id ";
   		$sql .= " WHERE product_filters.product_id = $product_id ";

   	if( $cat_id )
   	{
   		$specific_filters = array();
   		$filters = Filters::getFiltersForCat( $cat_id );

   		foreach( $filters as $f )
   		{
   			if( $f['display'] == 'Y' )
   				$specific_filters[] = $f['filter_id'];
   		}
   		$sql .= " and product_filters.filter_id IN ('" . implode( "','", $specific_filters ) . "') ";
   	}

	//echo $sql;

   	return db_query_array( $sql, 'filter_id' );
   }

   static function getProductPdfs( $product_id=0, $limit=0, $start=0, $last_updated=0 )
   {
	   	$sql = "SELECT * FROM product_pdfs WHERE 1 ";

	   	if( $product_id )
	   		$sql .= " AND product_id = $product_id ";


	   	//make sure the db is at least 1 week older than now
		if( $last_updated )
		{
			$sql .= " AND DATEDIFF( curdate(), last_checked ) > 7 ";
		}

		if( $limit )
		{
			$sql .=" limit $start, $limit ";
		}

	//	echo $sql;

	   	return db_query_array( $sql );
   }

   static function get1PDF( $id )
   {
   	$id = (int) $id;
		if (!$id) return false;

		$sql = " SELECT * FROM product_pdfs WHERE id = $id ";
		$result = db_query_array( $sql );

		return $result[0];
   }

   static function updateParentPrices( )
   {

   	//Need to get all of the parent products that have a price of 0.00. Then if they have options, asign the
   	//lowest option price to the parent
   	//This is only for sorting purposes, the parent product can't be purchased
   	//1/4/08 - I changed this to get all parents and update their prices to the lowest option.
   	//This should always be updated in case all options go up, parent shouldnt' display low as (old option)
   	$start = 0;

   	while($products = Products::get( 0,'','','','',0,'','','',0,0,0,0,0,0,0,0,0,'',false,false,'',false,'','','',1000,$start ) )
   	{
   		//print_ar( $products );
   		foreach( $products as $product )
   		{
   				$options = Products::getProductOptions( 0, $product['id'], false, true, '', false );
   				//first one is going to be the lowest price by default
   				if( $options )
   				{

   					//echo' Updating ' . $product['vendor_sku'] . ' = ' . $options[0]['additional_price'] .  ' <br />';
   					$prod_info = array();
   					$prod_info['price'] = $options[0]['additional_price'];
   					$prod_info['retail_price'] = $options[0]['additional_price'];

   					Products::update( $product['id'], $prod_info );
   				}
   				//if no options then this product has no children and not in the price sheet

   		}
   		$start += 1000;
   	}
   }

   static function getProductMarkup( $prod, $back = false )
   {
   	//I need to modify this funciton, now it is going to RETURN the AMOUNT that should be ADDED to the PRODUCT PRICE
   	//This is becasue of the fact that they want to also add a dollar value to certain ranges
   	//     < 100 --- add 25$
   	// 100 - 200 == 20$
   	// 200 - 300 -- $15
   	// 300 - 400 -- $10
  	//print_ar( $prod );

  		$product = Products::get1( $prod['product_id'] );

   		if( $prod['product_id'] ){
   			$product_id = $prod['product_id'];
   			$is_option = true;
   		}
   		else{
   			$product_id = $prod['id'];
   			$product = Products::get1( $product_id );
   			$is_option = false;
   		}

  		$is_unavailable = ($prod["is_available"] == "N" || $product["is_available"] == "N") ? true : false;

   		if( $prod['additional_price'] > 0 )
   		{
   			$price = $prod['additional_price'];
   			//echo $price . ' from add price ';
   			$product = Products::get1( $prod['product_id'] );
   			$prod['brand_id'] = $product['brand_id'];
   		}
   		else{
   			$price = $prod['price'];
   		}

   		if($price == 0.00 && $prod['final_price'] == 0.00){
   			$updateProduct = true;
   			if((int) $product_id){
   				$options = Products::getProductOptions( 0, $product_id, false, true, 'additional_price','','','','Y','N'); //get all options
   				if($options){
   					foreach($options as $option){
   						if($option['additional_price'] > 0 || $option['retail_price'] > 0 || $option['final_price'] > 0){
   							$updateProduct = false; //if the option is active and costs $$$, this is not a 0.00 product!
   							break;
   						}
   					}
   				}
   				if($updateProduct){
   					if($product["is_available"] == "Y"){
   						Products::update($product_id,array("is_available"=>"N"));
   					}
   				}
   				else{
   					if($product["is_available"] == "N"){
   						//Products::update($product_id,array("is_available"=>"Y"));
   					}
   				}
   			}

   			if($updateProduct || $is_option){
   				return 0.00;
   			}
   		}

   		if( $prod['final_price'] > 0 ){
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $prod['final_price'];
   		}


   		if( !$back ){

	   		switch( $price )
	 			{
	 				case $price == 0 :
	 					$extra_markup = 0;
	 					break;
	 				case $price < 100 :
	 					$extra_markup = 25;
	 					break;
	 				case $price >= 100 && $price < 200:
	 					$extra_markup = 20;
	 					break;
	 				case $price >= 200 && $price < 300:
	 					$extra_markup = 15;
	 					break;
	 				case $price >= 300 && $price < 400:
	 					$extra_markup = 10;
	 					break;
	 				default:
	 					$extra_markup = 0;
	 			}
   		}
 			else
 				$extra_markup = 0;

   	//Set back to true to return backend price
   		global $CFG;

   		//First check if the product has an override, then brand then cat, then default
   		//EDITED - cat moved to higher priority than brand - AF 11/19

   		//print_ar( $cats );
   		if( $back )
   			$type = 'backend';
   		else
   			$type = 'frontend';

   		if( $prod[$type .'_mark'] )
   		{
   			$markup = $prod[$type .'_mark'];

   			$price = $price * ($markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		$cat_mark = 0;

   		$cats = Products::getProductCatTree( $product_id ); //GET CAT PRICE

   		$orig_price = $price;
   		foreach( $cats as $cat )
   		{
   			if( $cat[$type . '_mark'] )
   			{
	   			$markup = $cat[$type . '_mark'];

	   			$price = $orig_price * ($markup/100 + 1) + $extra_markup;

	   			$price = self::roundup( $price, -1 );
   				$price = $price - 1; //Set it to the upper 9


	   			$cat_mark = 1;
   			}
   		}
   		unset( $cats );
   		if( $cat_mark ){
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		$brand = Brands::get1( $prod['brand_id'] );

   		if( $brand[$type .'_mark'] ) //GET BRAND PRICE
   		{
   			$markup = $brand[$type .'_mark'];

   			$price = $price * ($markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
   			unset( $brand );
   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}

   		if( $back )
   		{
   			if((($price + $extra_markup) >= $CFG->backend_markup_large_price) && $CFG->backend_markup_large_price > 0){
   				$price_markup = $CFG->backend_markup_large_mark;
   			}
   			else{
   				$price_markup = $CFG->backend_mark;
   			}

   			$price = $price * ($price_markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );
   			$price = $price - 1; //Set it to the upper 9
			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}
   		else
   		{
   			if((($price + $extra_markup) >= $CFG->frontend_markup_large_price) && $CFG->frontend_markup_large_price > 0){
   				$price_markup = $CFG->frontend_markup_large_mark;
   			}
   			else{
   				$price_markup = $CFG->frontend_mark;
   			}

   			$price = $price * ($price_markup/100 + 1) + $extra_markup;
   			$price = self::roundup( $price, -1 );

   			$price = $price - 1; //Set it to the upper 9

   			if($is_unavailable){
   				//Products::update($product_id,array("is_available"=>"Y"));
   			}
   			return $price;
   		}
   }

   static function isAvailableForPurchase($prod_id=0){
   		$prod_id = (int) $prod_id;
   		if(!$prod_id){
   			return false;
   		}
   		else{
   			$product = Products::get1($prod_id);
   			if($product["is_available"] == "N"){
   				return false;
   			}
   			else{
   				return true;
   			}
   		}
   }

   static function roundup ($value, $dp)
	 {
	    // Offset to add to $value to cause round() to round up to nearest significant digit for '$dp' decimal places
	    $offset = pow (10, -($dp + 1)) * 5;
	    return round ($value + $offset, $dp);
	 }

   static function displayDimension( $which, $type, $id, $quotes = false )
   {
   		$which = strtolower( $which );
   	//just need a funciton to take a floating point number and display a fraction
   		if( $type == 'option' )
   		{
   			$option = Products::getProductOption( $id );
   			$product = Products::get1( $option['product_id'] );

   			if( $option[ 'display_' . $which ] )
   			{
			    if( $option['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $option[ 'display_' . $which ] . "&quot;";
   				else
   					return $option[ 'display_' . $which ];
   			}

   			if( $product[ 'display_' . $which ] )
   			{
			    if( $product['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $product[ 'display_' . $which ] . "&quot;";
   				else
   					return $product[ 'display_' . $which ];
   			}

   			if( $option[$which] > 0 )
   				$number = $option[$which];
   			else
   				$number = $product[$which];
   		}
   		elseif( $type == 'product' )
   		{
   			$product = Products::get1( $id );
   			if( $product[ 'display_' . $which ] )
   			{
			    if( $product['display_' . $which] == 0 )
				return "N/A";

   				if( $quotes )
   					return $product[ 'display_' . $which ] . "&quot;";
   				else
   					return $product[ 'display_' . $which ];
   			}

   			$number = $product[$which];
   		}

   		$number_parts = explode('.', $number );
   		$ret = '';
   		if( count( $number_parts ) > 1 && $number_parts[1] > 0 )
   		{
   			if( $number_parts[1] == 1111 )
   			{
   				$ret = $number_parts[0] . ' 1/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 2222 )
   			{
   				$ret = $number_parts[0] . ' 2/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 3333 )
   			{
   				$ret = $number_parts[0] . ' 1/3';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 4444 )
   			{
   				$ret = $number_parts[0] . ' 4/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 5555 )
   			{
   				$ret = $number_parts[0] . ' 5/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 6666 )
   			{
   				$ret = $number_parts[0] . ' 2/3';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 7777 )
   			{
   				$ret = $number_parts[0] . ' 7/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}
   			elseif( $number_parts[1] == 8888 )
   			{
   				$ret = $number_parts[0] . ' 8/9';
   				if( $quotes )
   					$ret .= "&quot;";
   				return $ret;
   			}



   			$gcd = Products::gcd( trim($number_parts[1]), 10000 );
   			//echo 'gcd = ' . $gcd;
   			$numerator = trim($number_parts[1]) / $gcd;
   			//echo ' <br />10000 / ' . $gcd;
   			$denominator = 10000 / $gcd;
   			//print_ar( $number_parts );
   			$ret = $number_parts[0] . ' ' . $numerator . '/' . $denominator;
   		}
   		else
   		{
		    if( $number == 0 )
			return "N/A";
   			$ret = number_format($number, 0,'.','');
   		}

   		if( $quotes )
   			$ret .= "&quot;";

   		return $ret;
   }

   static function gcd( $x, $y)
   {
   	//echo $x . ' ' . $y;
			while ($y != 0) {
				$w = $x % $y;
				$x = $y;
				$y = $w;
		}
			return $x;
	}

	static function getSpecificMarkup( $prod, $mark )
	{
		  if( $prod['additional_price'] )
   			$price = $prod['additional_price'];
   		else
   			$price = $prod['price'];


 			$price = $price * ($mark/100 + 1);
 			//echo'price to return = ' . $price;
 			return $price;
	}

	static function compare( $items, $cat_id )
	{
		global $CFG;
		$numb_products = count($items);
		?>
		    <div class="featured_items category_landing autoflow">
		<?
		echo'<table class="compare_items">';
		$products = array();
		$i = 0;
		if( is_array( $items ) )
			foreach( $items as $item )
			{
				$products[$i] = Products::get1( $item );
				$products[$i]['filters'] = Products::getProductFilterValues( $item );
				//echo $i;
				//print_ar($products[$i]['filters'] );
				$i++;
			}
		//print_ar( $products );
 		$prod_cats[] = $cat_id;
 	//	print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $prod_cats );
 	//	print_ar( $cats );
 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}

 		if( !$headers )
 			return false;

 		//print_ar( $headers );
 		$filters = Filters::getFiltersForCat( $filter_cat );
 		//print_ar( $filters );

		echo'<tr class="comp_product_row">';
		echo'<td  class="comp_product_header_first" align="center"></td>';
		foreach( $products as $p )
		{
		    $name = $p['name'];
		    $name = preg_replace('/[^A-z0-9]/s','-',$name);

		    $path = $CFG->baseurl . 'itempics/' . $name . '-' . $p['id'] . $CFG->thumbnail_suffix;
		    ?>
			<td align="center">
		    <div class="featured_items_box">
			    <div class="iner">
				<a href="<?=Catalog::makeProductLink_( $p )?>">
					<img src="<?=$path?>" alt="<?=$p['name']?>" />
				    </a>

				<span class="description">
					<a href="<?=Catalog::makeProductLink_( $p )?>" title="<?=$p['name']?>">
						    <b><?
						    echo StdLib::addEllipsis($p['name'],50);
						    ?></b>
					    </a>

					by <?=$p['brand_name']?> <br />
					Product #: <?=$p['vendor_sku']?>

				</span>

				<label><a href="<?=Catalog::makeProductLink_( $p )?>">
				    $<?=number_format( $p['price'], 2 )?></a></label>

			</div>


		    </div>
		    </td>
		    <?


//			$prod_link = Catalog::makeProductLink_($prod, true, false);
//			echo'<td class="comp_product_header" align="center" valign="top" ><div id="prod_title"><a href="'. $prod_link .'">' . $prod['name'] . '</a></div>';
//			if (file_exists(Catalog::makeProductImageLink($prod['id'], true, true))) {
//			    echo '<div id="img_container"><a href="'. $prod_link.'"><img id="prod_thumb" src="'. Catalog::makeProductImageLink($prod['id'], true ) .'" /></a></div>';
//			}
//			else
//			{
//
//				echo '<div id="img_container"><img id="prod_thumb" src="/pics/spacer.gif" /></div>';
//			}
//			$markup = Products::getProductMarkUp( $prod );
//			echo'<div class="prod_price">As Low As $' . number_format($markup, 2) . '</div>';
		}
		$row_length = $numb_products + 1;
		echo'</td>';
		echo'</tr>';
		echo'<tr><td height="30px"></td><td colspan="' . ($row_length-1) .'"></td></tr>';

		foreach( $headers as $header )
		{
			$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
			echo'<tr class="comp_header_row">';
			echo'<th class="comp_header_name">' . $header['name'] . '</th>';
			echo'<td colspan="' . ($row_length-1) .'" class="comp_header_holder">&nbsp;</td></tr>';
			if( $specs )
			foreach( $specs as $spec )
			{
				$class = Products::compareSpecs( $products, $spec );

				echo'<tr class="' . $class . '">';
				echo'<td class="comp_spec_name"><nobr>' . $spec['name'] . '</nobr></td>';

				if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
				    trim(strtoupper($spec['name'])) == 'HEIGHT' ||
				    trim(strtoupper($spec['name'])) == 'DEPTH'  )
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
						//print_ar( $products );
						echo'<td class="comp_spec_value">';
						echo Products::displayDimension( $spec['name'], 'product', $products[$i]['id'], true);
						echo'</td>';
					}
				}
				else
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
					    if( $products[$i]['filters'][$spec['filter_id']]['value'] === 0 )
					    {
						$products[$i]['filters'][$spec['filter_id']]['value'] = 'N/A';
					    }
						echo'<td class="comp_spec_value">' . $products[$i]['filters'][$spec['filter_id']]['value'] . '</td>';
					}
				}
				echo'</tr>';
			}
		}
		echo'</table>';
		?>
		    </div>
		<?
	}

	private function compareSpecs( $products, $spec )
	{
		//print_ar( $products );
		//print_ar( $spec );
		$same_class = "comp_spec_row";
		$different_class = "comp_spec_row_diff";

		$numb_products = count( $products );
		$different = false;
		if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
			trim(strtoupper($spec['name'])) == 'HEIGHT' ||
			trim(strtoupper($spec['name'])) == 'DEPTH' )
		{
			for( $i = 0; $i < $numb_products; $i++ )
			{
				$x = $i + 1;
				if( $x >= $numb_products )
					break;
				if( $products[$i][trim(strtolower($spec['name']))] != $products[$x][trim(strtolower($spec['name']))] )
				{
					$different = true;
					break;
				}
			}
		}
		else
		{
			for( $i = 0; $i < $numb_products; $i++ )
			{
				$x = $i + 1;
				if( $x >= $numb_products )
					break;
				if( $products[$i]['filters'][$spec['filter_id']]['value'] != $products[$x]['filters'][$spec['filter_id']]['value'] )
				{
					$different = true;
					break;
				}
			}
		}

		if( $different )
			return $different_class;
		else
			return $same_class;
	}

	static function getAllProductValues( $prod_id )
	{
		$prod_cats = Products::getProductCats( $prod_id );
//		$prod_cats = array();
		if( $prod_cats )
		    foreach( $prod_cats as $key => $val )
		    {
			    $prod_cats[] = $key;
		    }
		else
		    return false;
		//Quick fix here, it should only use compare if it didn't find a filter value, so putting type zspec will make that type show last.
		$sql = " SELECT distinct(cat_filters.filter_id),cat_filters.cat_id, cat_filters.display,  cat_filters.type, filters.name FROM cat_filters, filters WHERE cat_filters.cat_id in ('". implode("','", $prod_cats ) . "') AND filters.id=cat_filters.filter_id ";
		//$sql .= " UNION SELECT distinct(cat_specs.filter_id),cat_specs.cat_id, 'zspec' as 'type', filters.name FROM cat_specs, filters WHERE cat_specs.cat_id in ('". implode("','", $prod_cats ) . "') AND filters.id=cat_specs.filter_id order by name, cat_id DESC, type ASC";
		$sql .= "  order by name, cat_id DESC, type ASC";
		//echo $sql;
		$results = db_query_array( $sql );
		//print_ar( $results );

		$values = array();
		$last_value = 0;
		if( is_array( $results ) )
			foreach( $results as $r )
			{
				if( $r['filter_id'] == $last_value )
					continue;
				$i = $r['name'];
				$last_value = $r['filter_id'];
				$values[$i]['filter_id'] = $r['filter_id'];
				$values[$i]['cat_id'] = $r['cat_id'];
				$values[$i]['name'] = $r['name'];
				$values[$i]['display'] = $r['display'];
				if( $r['type'] == "range" || $r['type'] == 'select'  )
				{
					$values[$i]['type'] = 'text';
					$values[$i]['which'] = 'filter';
				}
				elseif( $r['type'] == "zspec" )
				{
					$values[$i]['type'] = 'text';
					$values[$i]['which'] = 'spec';
				}
				else
				{
					$values[$i]['type'] = $r['type'];
					$values[$i]['which'] = 'filter';
				}
			}
		//print_ar( $values );

		return $values;
	}

	static function editValues( $prod_id )
	{

		if( !$prod_id )
			return false;
		$values = Products::getAllProductValues( $prod_id );
	//print_ar( $values );
		$product = Products::get1( $prod_id );
		//print_ar( $product );
		echo'<table>';

		$prod_cats[] = Products::getProductCats( $prod_id );
		$prod_cats = array_shift( $prod_cats );

		$cat_list = array();
		foreach( $prod_cats as $pc )
		{
			$cat_list[] = $pc['cat_id'];
		}
 		//print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $cat_list );
// 		print_ar( $cats );

 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		$headers = array();
 		foreach( $cats as $cat )
 		{
 			$cur_headers = Specs::getCatHeaders( $cat );
// 			print_ar( $cur_headers );
 			if( $cur_headers )
 				$headers = array_merge( $headers, $cur_headers);
 		}
 		/*
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}*/
//print_ar( $headers );
//if( !$headers )
	if( true )
 		{
 		if( $values )
			foreach( $values as $v )
			{
				echo'<tr><th>';
				if( $v['display'] == "Y" )
					echo '<span style="font-size: 150%">*</span>';
				 echo $v['name'];

				echo '</th>';

				if( trim(strtoupper($v['name'])) == 'WIDTH' ||
                                    trim(strtoupper($v['name'])) == 'HEIGHT' ||
                                    trim(strtoupper($v['name'])) == 'DEPTH' )
				{
					echo'<td>';
						echo Products::displayDimension( $v['name'], 'product', $product['id'], true );

					echo'</td>';

				}
				else if( trim(strtoupper($v['name'])) == 'PRICE' )
				{
				    echo'<td>';
					?>
					$<?=$product['price']?>
					<?
				    echo'</td>';
				}
				else
				{
					$prod_filter = Products::getProductFilter( $prod_id, $v['filter_id'] );
					if( $prod_filter )
						$value = $prod_filter['value'];
					else
						$value = '';
					if( $v['type'] == "text" )
					{
						echo'<td><input type="text" name="info[filters][' . $v['filter_id'] . ']" size="25" value="' . htmlentities($value) . '" /></td>';
					}
					else
					{
						$options = Filters::getCatFilterOptions( $v['cat_id'], $v['filter_id'], true );
						if( !$options )
						{
							echo'<td><a href="category_options.php?cat_id=' . $v['cat_id'] . '&action=edit" target="_blank">
										ADD OPTIONS FOR THIS FILTER FIRST</a></td>';
						}
						else
						{
							echo'<td>' . Filters::getCatFilterOptionSelect($v['cat_id'], $v['filter_id'], $value, 'info[filters][' . $v['filter_id'] . ']') . '</td>';
						}
					}
				}
				echo'</tr>';
			}
 		}
 		else
 		{
 			foreach( $headers as $header )
 			{
 				echo'<tr><th colspan=2 style="font-size: 125%">' . $header['name'] . '</th></tr>';
 				$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
 				//print_ar( $specs );
 				//print_ar( $values );
 				foreach( $specs as $s )
 				{
 					if( $values[$s['name']] )
 						$v = $values[$s['name']];
 					else
 						continue;
 					//print_ar( $v );
	 				echo'<tr><th>';
						if( $v['display'] == "Y" )
							echo '<span style="font-size: 150%">*</span>';
						 echo $v['name'];
					echo '</th>';

					if( trim(strtoupper($v['name'])) == 'WIDTH' ||
									trim(strtoupper($v['name'])) == 'HEIGHT' ||
									trim(strtoupper($v['name'])) == 'DEPTH' )
					{
						echo'<td>';
							echo Products::displayDimension($v['name'], 'product', $product['id'], true );

						echo'</td>';

					}
					else
					{

						$prod_filter = Products::getProductFilter( $prod_id, $v['filter_id'] );
						if( $prod_filter )
							$value = $prod_filter['value'];
						else
							$value = '';
						if( $v['type'] == "text" )
						{
							echo'<td><input type="text" name="info[filters][' . $v['filter_id'] . ']" size="25" value="' . htmlentities($value) . '" /></td>';
						}
						else
						{
							$options = Filters::getCatFilterOptions( $v['cat_id'], $v['filter_id'], true );
							if( !$options )
							{
								echo'<td><a href="category_options.php?cat_id=' . $v['cat_id'] . '&action=edit" target="_blank">
											ADD OPTIONS FOR THIS FILTER FIRST</a></td>';
							}
							else
							{
								echo'<td>' . Filters::getCatFilterOptionSelect($v['cat_id'], $v['filter_id'], $value, 'info[filters][' . $v['filter_id'] . ']') . '</td>';
							}
						}
					}
					echo'</tr>';
 				}
 			}
 		}
		echo'</table>';
	}

	static function getAccessories( $id )
	{
		$link_types = Products::getLinkTypes();
		$acc_id = 0;
		foreach( $link_types as $link )
		{
			if( ($link['name']) == "Product Accessories" )
			{
				$acc_id = $link['id'];
				break;
			}
		}
		if( !$acc_id )
			return 0;

		$accessories = Products::getLinksForProduct( $id, false, $acc_id, 'product_links.option_id DESC, products.name' );

		return $accessories;
	}

	static function getPriceSelect( $product, $name='price_select', $id='price_select',
			$price_options=array(5,10,15,20), $sel_price=0, $msg='Select Price', $onchange='' )
	{
		$ret = "";
		$selected = false;
		$ret .= "<select name='$name' id='$id' onchange='$onchange' >";
		//$ret .="<option value=''>" . $msg . "</option>";
		$web_price = Products::getProductMarkup( $product );
		$ret .= "<option value='" .number_format($web_price,2) . "' ";

			if( number_format($sel_price,2) == number_format($web_price,2) )
			{
				$ret .= "selected";
				$selected = true;
			}
		$ret .= ">Web - " . number_format($web_price,2) . "</option>";

		foreach( $price_options as $opt )
		{
			$prod_price = Products::getSpecificMarkup( $product, $opt );
			$ret .= "<option value='" .number_format($prod_price,2) . "' ";
			if( number_format($sel_price,2) == number_format($prod_price,2) )
			{
				$ret .= "selected";
				$selected = true;
			}
			$ret .= ">$opt% - " . number_format($prod_price,2) . "</option>";
		}
		$ret .= "<option value='' ";
		if( !$selected )
			$ret .=" selected ";
		$ret .= ">Custom</option>";
		$ret .="</select>";

		return $ret;
	}


	static function outputInfoSidebar( $product, $cat_id )
	{
		global $CFG;

		$products = array();
		$products[] = $product;
		$prod_cats = array();
		if( !$cat_id )
		{
			$prod_cats = Products::getProductCats( $product['id'] );
			if( is_array( $prod_cats ) )
				foreach( $prod_cats as $key => $value )
				{
					$prod_cats[$key] = $value['cat_id'];
				}
		}
		else
			$prod_cats[] = $cat_id;

		$i = 0;
		$numb_products = count($products);
		if( is_array( $products ) )
			foreach( $products as $item )
			{
				$products[$i] = Products::get1( $item['id'] );
				$products[$i]['filters'] = Products::getProductFilterValues( $item['id'] );
				//echo $i;
				//print_ar($products[$i]['filters'] );
				$i++;
			}


 	//	print_ar( $prod_cats );
 		$cats = Cats::getFilterSearchCats( $prod_cats );
 		//print_ar( $cats );
 		$filter_cat = $cats[0];
 		//Need to get headers, if no headers check parent, until i find headers
 		$headers = false;
 		$cur_cat = $cats[0];
 		while( !$headers && $cur_cat )
 		{
 			$cat = Cats::get1( $cur_cat );
 			$headers = Specs::getCatHeaders( $cat['id'] );
 			$cur_cat = $cat['pid'];
 		}

 		if( !$headers )
 		{
 			return false;
 		}

 		//print_ar( $headers );
 		$filters = Filters::getFiltersForCat( $filter_cat );
 		//print_ar( $filters );
 		?>
		<div class="main">

      	<?
      	if($headers){
		foreach( $headers as $header )
		{
			$specs = Specs::getValuesForHeader( $header['cat_id'], $header['spec_header_id'] );
			echo'<div class="sidebox_top_grey">';
			echo $header['name'] . '</div><div class="main1">';

			if( $specs ){
			echo '<table>';
			foreach( $specs as $spec )
			{
				$class = Products::compareSpecs( $products, $spec );


				if( trim(strtoupper($spec['name'])) == 'WIDTH' ||
							trim(strtoupper($spec['name'])) == 'HEIGHT' ||
							trim(strtoupper($spec['name'])) == 'DEPTH' )
				{
					echo'<tr><td class="title">' . $spec['name'] . ':</td>';
					for( $i = 0; $i < $numb_products; $i++ )
					{
						//print_ar( $products );
						echo '<td class="value">'. Products::displayDimension($spec['name'], 'product', $products[$i]['id'], true) . '</td></tr>';
						echo'</tr>';
					}
				}
				else
				{
					for( $i = 0; $i < $numb_products; $i++ )
					{
						if( $products[$i]['filters'][$spec['filter_id']]['value'] == '' )
							continue;

						echo'<tr><td class="title">' . $spec['name'] . ':</td>';
						echo'<td class="value">' . $products[$i]['filters'][$spec['filter_id']]['value'] . '</td></tr>';

					}
				}

			}
			echo '</table></div>';
			}else{
				if(count($headers) == 1){
					echo "</div>";
				}
			}
		}}else{
			echo "</div>";
		}
		echo "</div>";
	}

	static function isMap($vendor_sku=""){
		$parent_id = ProductOptions::getParentID($vendor_sku);

		if($parent_id){
			$product = Products::get1($parent_id);
		}
		else{
			$product = Products::get1ByModel($vendor_sku);
		}

		if($product["is_map"] == "Y"){
			return true;
		}
		else{
			return false;
		}
	}

	static function missingImage( $filename )
	{
		$info['filename'] = $filename;

		return db_insert( 'missing_images', $info );
	}

	static function get1ByURLName( $url_name )
	{
	    $sql = " SELECT * FROM products WHERE 1 ";
	    $sql .= " AND UPPER(products.url_name) = UPPER('" . addslashes( $url_name ). "') AND products.is_active = 'Y' AND products.is_deleted = 'N' ";

//	    echo $sql . '<br />';

	    return db_get1( db_query_array( $sql ) );
	}

	static function setURL( $product, $update=false )
	{

	    if( $product['is_active'] == 'N' )
		return;

	    if( $update )
	    {
		//Clear the URL first bc it could be the same;
		$info['url_name'] = '';
		self::update( $product['id'], $info, false);
	    }

	    $info['url_name'] = trim($product['name']);

	    //Need to determine where to cut the string... 25 char limit....
	    $parts = explode( ' ', $info['url_name'] );

	    $count = 0;
	    $short_name='';
	    foreach( $parts as $part )
	    {
		if( strlen( $part ) + $count <=35 )
		{
	    //	echo 'Count = ' . $count . '<br />';
		    $short_name .= $part . ' ';
		    $count = $count + strlen($part) + 1;
		}
		else
		    break;
	    }
	    $info['url_name'] = trim($short_name);

	    $info['url_name'] = str_replace( "&", "and", $info['url_name'] );
	    $info['url_name'] = str_replace( "/", "-", $info['url_name'] );

	    $info['url_name'] = preg_replace('/[^a-zA-Z0-9\- ]/','',$info['url_name']);
	    $info['url_name'] = str_replace(' ','-',$info['url_name']);


	    $info['url_name'] = rtrim( $info['url_name'], '-' );

	    //Now need to check for that URL in products and Cats to make sure it isn't there already
	    $counter = 1;
	    $orig_url = $info['url_name'];

	    $p_url = Products::get1ByURLName( $info['url_name'] );

	    while ( $p_url = Products::get1ByURLName( $info['url_name'] ) )
	    {
		    $info['url_name'] = $orig_url . '-' . $counter;

		    $counter++;
		    //echo'trying... ' . $product['id'];
		    //print_ar( $info['url_name'] );
	    }

	    while ( $c_url = Cats::get1ByURLName( $info['url_name'] ) )
	    {
		    $info['url_name'] = $orig_url . '-' . $counter;

		    $counter++;
		    while ( $p_url = Products::get1ByURLName( $info['url_name'] ) )
		    {
			$info['url_name'] = $orig_url . '-' . $counter;

			$counter++;
			echo'trying... ' . $product['id'];
			print_ar( $info['url_name'] );
		    }
	    }

	    $info['url_name'] = strtolower( $info['url_name'] );

	    Products::update( $product['id'], $info, false);
	}

	static function checkPDFExists($original_url=""){
		if(!$original_url){
			return false;
		}

		$sql = "SELECT * FROM product_pdfs WHERE original_url = '".addslashes($original_url)."';";

		$results = db_query_array($sql);

		if($results){?>
				1----<?=$results[0][url]?>----<?=$results[0][original_url]?>
		<?}else{?>
				0----
			<?
		}
	}


	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT products.*, products.vendor_sku AS product_vendor_sku ";



		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(products.id)) as total ";
		}
		else{
			$select_ .= ',brands.name AS brand_name ';
			$join_ = "LEFT JOIN brands on products.brand_id=brands.id ";
		}

		$from_ = " FROM products ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND products.id = [id] ";
		}

		if($o->brand_id >  0 ){
			$where_ .= " AND `products`.brand_id = [brand_id]";
		}
		if($o->vendor_id >  0 ){
			$where_ .= " AND `products`.vendor_id = [vendor_id]";
		}
		if($o->name != ''){
			$where_ .= " AND `products`.name = [name]";
		}
		if($o->tagline != ''){
			$where_ .= " AND `products`.tagline = [tagline]";
		}
		if($o->price >  0 ){
			$where_ .= " AND `products`.price = [price]";
		}
		if($o->retail_price >  0 ){
			$where_ .= " AND `products`.retail_price = [retail_price]";
		}
		if($o->vendor_price >  0 ){
			$where_ .= " AND `products`.vendor_price = [vendor_price]";
		}
		if($o->description != ''){
			$where_ .= " AND `products`.description = [description]";
		}
		if($o->notes != ''){
			$where_ .= " AND `products`.notes = [notes]";
		}
		if($o->vendor_sku != ''){
			$where_ .= " AND `products`.vendor_sku = [vendor_sku]";
		}
		if($o->vendor_sku_name != ''){
			$where_ .= " AND `products`.vendor_sku_name = [vendor_sku_name]";
		}
		if($o->is_active != ''){
			$where_ .= " AND `products`.is_active = '$o->is_active'";
		}
		if($o->is_available != ''){
			$where_ .= " AND `products`.is_available = '$o->is_available'";
		}
		if($o->is_not_available_description != ''){
			$where_ .= " AND `products`.is_not_available_description = [is_not_available_description]";
		}
		if($o->stock_level >  0 ){
			$where_ .= " AND `products`.stock_level = [stock_level]";
		}
		if($o->show_on_homepage != ''){
			$where_ .= " AND `products`.show_on_homepage = [show_on_homepage]";
		}
		if($o->homepage_order >  0 ){
			$where_ .= " AND `products`.homepage_order = [homepage_order]";
		}
		if($o->is_featured != ''){
			$where_ .= " AND `products`.is_featured = [is_featured]";
		}
		if($o->bizrate_id >  0 ){
			$where_ .= " AND `products`.bizrate_id = [bizrate_id]";
		}
		if($o->shopping_com_id >  0 ){
			$where_ .= " AND `products`.shopping_com_id = [shopping_com_id]";
		}
		if($o->price_grabber_id >  0 ){
			$where_ .= " AND `products`.price_grabber_id = [price_grabber_id]";
		}
		if($o->show_updated_new != ''){
			$where_ .= " AND `products`.show_updated_new = [show_updated_new]";
		}

		if($o->exclude_min != ''){
			$where_ .= " AND `products`.exclude_min = [exclude_min]";
		}

		if($o->exclude_discount != ''){
			$where_ .= " AND `products`.exclude_discount = [exclude_discount]";
		}

		if(is_array($o->date_updated)){
			if($o->date_updated['start']){
				$o->date_updated_start = $o->date_updated['start'];
				$where_ .= " AND `products`.date_updated >= [date_updated_start]";
			}
			if($o->date_updated['end']){
				$o->date_updated_end = $o->date_updated['end'];
				$where_ .= " AND `products`.date_updated <= [date_updated_end]";
			}
		} else 		if($o->date_updated != ''){
			$where_ .= " AND `products`.date_updated = [date_updated]";
		}
		if(is_array($o->date_added)){
			if($o->date_added['start']){
				$o->date_added_start = $o->date_added['start'];
				$where_ .= " AND `products`.date_added >= [date_added_start]";
			}
			if($o->date_added['end']){
				$o->date_added_end = $o->date_added['end'];
				$where_ .= " AND `products`.date_added <= [date_added_end]";
			}
		} else 		if($o->date_added != ''){
			$where_ .= " AND `products`.date_added = [date_added]";
		}
		if($o->sort >  0 ){
			$where_ .= " AND `products`.sort = [sort]";
		}
		if($o->show_in_yahoo_feed != ''){
			$where_ .= " AND `products`.show_in_yahoo_feed = [show_in_yahoo_feed]";
		}
		if($o->width >  0 ){
			$where_ .= " AND `products`.width = [width]";
		}
		if($o->height >  0 ){
			$where_ .= " AND `products`.height = [height]";
		}
		if($o->depth >  0 ){
			$where_ .= " AND `products`.depth = [depth]";
		}
		if($o->weight >  0 ){
			$where_ .= " AND `products`.weight = [weight]";
		}
		if($o->display_height != ''){
			$where_ .= " AND `products`.display_height = [display_height]";
		}
		if($o->display_width != ''){
			$where_ .= " AND `products`.display_width = [display_width]";
		}
		if($o->display_depth != ''){
			$where_ .= " AND `products`.display_depth = [display_depth]";
		}
		if($o->features != ''){
			$where_ .= " AND `products`.features = [features]";
		}
		if($o->specs != ''){
			$where_ .= " AND `products`.specs = [specs]";
		}
		if($o->shipping_type != ''){
			$where_ .= " AND `products`.shipping_type = [shipping_type]";
		}
		if($o->taxable != ''){
			$where_ .= " AND `products`.taxable = [taxable]";
		}
		if($o->website != ''){
			$where_ .= " AND `products`.website = [website]";
		}
		if($o->avail >  0 ){
			$where_ .= " AND `products`.avail = [avail]";
		}
		if($o->on_order >  0 ){
			$where_ .= " AND `products`.on_order = [on_order]";
		}
		if($o->include_singlefeed != ''){
			$where_ .= " AND `products`.include_singlefeed = [include_singlefeed]";
		}
		if($o->include_nextag != ''){
			$where_ .= " AND `products`.include_nextag = [include_nextag]";
		}
		if($o->include_pricegrabber != ''){
			$where_ .= " AND `products`.include_pricegrabber = [include_pricegrabber]";
		}
		if($o->include_shopping_com != ''){
			$where_ .= " AND `products`.include_shopping.com = [include_shopping.com]";
		}
		if($o->include_shopzilla != ''){
			$where_ .= " AND `products`.include_shopzilla = [include_shopzilla]";
		}
		if($o->include_become != ''){
			$where_ .= " AND `products`.include_become = [include_become]";
		}
		if($o->include_smarter != ''){
			$where_ .= " AND `products`.include_smarter = [include_smarter]";
		}
		if($o->promo_message != ''){
			$where_ .= " AND `products`.promo_message = [promo_message]";
		}
		if($o->keywords != ''){
			$where_ .= " AND `products`.keywords = [keywords]";
		}
		if($o->skip_update != ''){
			$where_ .= " AND `products`.skip_update = [skip_update]";
		}
		if($o->add1_caption != ''){
			$where_ .= " AND `products`.add1_caption = [add1_caption]";
		}
		if($o->add2_caption != ''){
			$where_ .= " AND `products`.add2_caption = [add2_caption]";
		}
		if($o->add3_caption != ''){
			$where_ .= " AND `products`.add3_caption = [add3_caption]";
		}
		if($o->add4_caption != ''){
			$where_ .= " AND `products`.add4_caption = [add4_caption]";
		}
		if($o->frontend_mark >  0 ){
			$where_ .= " AND `products`.frontend_mark = [frontend_mark]";
		}
		if($o->backend_mark >  0 ){
			$where_ .= " AND `products`.backend_mark = [backend_mark]";
		}
		if($o->final_price >  0 ){
			$where_ .= " AND `products`.final_price = [final_price]";
		}
		if($o->replace_with != ''){
			$where_ .= " AND `products`.replace_with = [replace_with]";
		}
		if($o->free_shipping != ''){
			$where_ .= " AND `products`.free_shipping = [free_shipping]";
		}
		if($o->aka_sku != ''){
			$where_ .= " AND `products`.aka_sku = [aka_sku]";
		}
		if($o->is_promo_allowed != ''){
			$where_ .= " AND `products`.is_promo_allowed = [is_promo_allowed]";
		}
		if($o->is_deleted != ''){
			$where_ .= " AND `products`.is_deleted = [is_deleted]";
		}
		if($o->is_map != ''){
			$where_ .= " AND `products`.is_map = [is_map]";
		}
		if($o->local_warehouse != ''){
			$where_ .= " AND `products`.local_warehouse = [local_warehouse]";
		}
		if($o->upc != ''){
			$where_ .= " AND `products`.upc = [upc]";
		}
		if($o->unadvertised_price_flag != ''){
			$where_ .= " AND `products`.unadvertised_price_flag = [unadvertised_price_flag]";
		}
		if($o->h2 != ''){
			$where_ .= " AND `products`.h2 = [h2]";
		}
		if($o->special_description != ''){
			$where_ .= " AND `products`.special_description = [special_description]";
		}
		if($o->added_by >  0 ){
			$where_ .= " AND `products`.added_by = [added_by]";
		}
		if($o->added_by_description != ''){
			$where_ .= " AND `products`.added_by_description = [added_by_description]";
		}
		if($o->spec_sheet_url != ''){
			$where_ .= " AND `products`.spec_sheet_url = [spec_sheet_url]";
		}
		if($o->ships_in != ''){
			$where_ .= " AND `products`.ships_in = [ships_in]";
		}
		if($o->is_finance != ''){
			$where_ .= " AND `products`.is_finance = [is_finance]";
		}
		if($o->priced_per != ''){
			$where_ .= " AND `products`.priced_per = [priced_per]";
		}
		if($o->per_price >  0 ){
			$where_ .= " AND `products`.per_price = [per_price]";
		}
		if($o->unit_case != ''){
			$where_ .= " AND `products`.unit_case = [unit_case]";
		}
		if($o->per_order != ''){
			$where_ .= " AND `products`.per_order = [per_order]";
		}
		if($o->line != ''){
			$where_ .= " AND `products`.line = [line]";
		}
		if($o->gift_wrapping != ''){
			$where_ .= " AND `products`.gift_wrapping = [gift_wrapping]";
		}
		if($o->url_name != ''){
			$where_ .= " AND `products`.url_name = [url_name]";
		}
		if($o->promo_msg != ''){
			$where_ .= " AND `products`.promo_msg = [promo_msg]";
		}
		if($o->include_google != ''){
			$where_ .= " AND `products`.include_google = [include_google]";
		}
		if($o->is_accessory != ''){
			$where_ .= " AND `products`.is_accessory = [is_accessory]";
		}

		if( $o->name_like != '' ) {
			$where_ .= " AND `products`.name LIKE '%" . addslashes( $o->name_like ) . "%' ";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY products.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}
		if ($o->search_keywords != '') {
			$join_ .= "LEFT JOIN product_cats ON products.id=product_cats.product_id ";
			$join_ .= "LEFT JOIN cats ON product_cats.cat_id=cats.id ";
			$join_ .= "LEFT JOIN product_options ON products.id=product_options.product_id ";
			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$where_ .= " AND " . db_split_keywords($o->search_keywords,$fields,'AND',true);
		}	
		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function getCatsForSearch( $keyword, $brand_id, $order='', $keyword_search=array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id') )
	{
	    global $CFG;

	     $result = Products::get(0,'',$keyword,$order,'',0,'Y','','','', $brand_id,0,0,0,0,0,0,0,'',false,false,'',false,'','','',0, 0,false,'N','',true, $keyword_search,'','N',true);

	     return $result;
	}
	
	
	static function makeProductBoxes(){
		
	}

	static function chargeShipping( $product_id )
	{
	    global $CFG;
//print_ar($product_id );
	    $product	= Products::get1( $product_id );
	    $brand	= Brands::get1( $product['brand_id'] );
	    $cats	= Cats::getCatsForProduct( $product['id'] );
//print_ar( $product );
//print_ar( $cats );
	    if( $product['free_shipping'] == 'Y' )
		return false;

	    if( $brand['free_shipping'] == 'Y' )
		return false;

	    if( $cats )
		foreach( $cats as $c )
		{
		    if( $c['free_shipping'] == 'Y' )
			return false;
		}

	    return true;
	}
	
	static function displayColorChoices( $product_id , $page_id=null)
	{
	    global $CFG;
	    
	    //Need to check if there are color values for all the options. 
	    $new_display = true;
	    
	    $product_options = Products::getProductOptions( 0, $product_id );
	    foreach( $product_options as $key => $opt )
	    {
		$color_q = new OptionColorMapQuery();
		$color_q->name = $opt['value'];
		
		$color_map = OptionColorMap::get1ByO( $color_q );
		
		if( !$color_map )
		{
			$new_display = false;
		}
		else
		    $product_options[$key]['color_value'] = $color_map['color_value'];
			$product_options[$key]['option_type'] = $color_map['option_type'];		    
	    }
	    
	    if( $new_display ) {
			?>
				<div class="<?=($page_id)?'listing_available_colors_v1':'listing_available_colors'?>">
				<input type="hidden" name="option[<?=$product_id?>]" value="" id="option_<?=$product_id?>" />

				<div class="listing_available_label">
				<?php
					if (!$page_id){
						$p=Products::get1($product_id);


					//echo $p['free_shipping'];
					//if (($CFG->global_free_shipping_switch)&&($p['free_shipping']=='Y'))
					//	echo "<img style=\"float:left;padding:0;margin:0;vertical-align:top;\" src=\"images/free_shipping.jpg\" />";
					}
					?>
					Choose a Color:
				</div>
				<ul>
				<?
					foreach( $product_options as $opt )
					{
					
						if ($opt['option_type'] == "color")
						{?>
					<li id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?> );"style="padding:0;background: <?=$opt['color_value'] ?>">
						<? /*<input style="width:18px;padding:0;background: <?=$opt['color_value'] ?>" disabled="disabled" type="text" size="1" value=" " alt="color"/> */?>
						<a class="color_block" href="javascript:void(0);" title=<?=$opt['value']?>>&nbsp;</a>
					</li>
					<?}
					else if ($opt['option_type'] == "image")
						{?>
						<li>
						<div style="display:none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
					<a class="color_block" href="javascript:void(0);" title=<?=$opt['value']?>>					
					<img class="no_reg" id="<?=$opt['id']?>" class="<?=$product_id?>_options" onclick="updateOptionValue( <?=$product_id?>, <?=$opt['id']?> );" border="0" src="/optionpics/<?=$opt['color_value'] ?>" style="padding-top: 0px" onmouseover="mp_showtrail('/optionpics/<?=str_replace('.jpg', '_larger.jpg', $opt['color_value'])?>','',70,70)" onmouseout="mp_hidetrail()">
					</a>
					</li>
					<?} 
					}
					?>
				</ul>
				<?php
				if (($page_id)&&($product_options)){
					echo "<br/>";
				}
				?>
			</div>
	    <?
		} else {
		$colors_avail = 0;
		foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
		}
		
		?>
		<div class="listing_available_colors_v1">
		<select class="opts" id="option_<?=$product_id?>" name="option[<?=$product_id?>]">
		    <option value="">Options (Select <?php echo $opt_name;?>)</option>
		    <?
		    foreach( $product_options as $option ) {
			$opt_name = $option['option_name'];
			    if( $p['inventory_limit'] == 'Y') {
				    //Check each option for inventory
				    $inv_o = (object) array();
				    $inv_o->product_id = $p['id'];

				    $inv_o->store_id = $CFG->default_inventory_location;
				    $inv_o->product_option_id = $option['id'];
				    $inv = Inventory::get1ByO( $inv_o );

				    if( $inv ){
					if( $inv['qty'] <= 0 ) {
					    continue;
					}
				    } else {
					    continue;
				    }
			    }
			    ++$colors_avail;
			?>												    
			<option value="<?=$option['id']?>" <?=$option['is_default'] == 'Y' ? 'selected="selected"' : ''?>><?=$option['value']?></option>
			<?
		    }
		    ?>
		</select>
				    
		</p><p>
		<?=$colors_avail?> <?=strtolower( $opt_name )?><?=(int)$colors_avail===1?'':'s'?> available
		</div>
		<?
	    }
	}
	static function getSoldAsText( $product_id )	
	{
		$sql = "SELECT priced_per, unit_case
			 FROM products WHERE id = $product_id";

		$result = db_query_array($sql);
		$my_result = $result[0];
		
		$priced_per = $my_result['priced_per'];
		$unit_case = $my_result['unit_case'];
		$text_to_return = "";
		
		if (($unit_case == "" || $unit_case == 0) && ($priced_per == "" || $priced_per == 0))
		{
			$text_to_return = "1 ea";			
		}
		else if ($unit_case == "" || $unit_case == 0)
		{
			// if is some numeral in priced_per, just used priced_per
			// otherwise, use "1" as the unit case number
			$contains = preg_match('~[\d]~i', $priced_per);
			
			if ($contains) $text_to_return = $priced_per;
			else $text_to_return = "1 ". $priced_per; 	
		}
		else if ($priced_per == "" || $priced_per == 0) 
		{
			$text_to_return = $unit_case;
		}	
		else 
		{
			$text_to_return = $unit_case . " " . $priced_per;
		}
		return $text_to_return;
	}
}

?>
