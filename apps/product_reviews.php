<?php class Product_Reviews
{
    public static function get($id = 0, $product_id = 0, $customer_id = 0, $title = '', $order_by = '', $order_asc = '', $approved='',$sentemail='')
    {
    	
    	 
        $sql = "SELECT product_reviews.*
                FROM product_reviews
                WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND product_reviews.id = $id ";
        }

        if ($product_id > 0)
        {
            $sql .= " AND product_reviews.product_id = $product_id ";
        }

        if ($customer_id > 0)
        { 
            $sql .= " AND product_reviews.customer_id = $customer_id ";
        }

        if ($title != '')  
        {
            $sql .= " AND product_reviews.title = $title ";
        }
				if ($approved != '')
        {
            $sql .= " AND product_reviews.approved = $approved ";
        }
				
					if ($sentemail != '')
        {
            $sql .= " AND product_reviews.sentemail = $sentemail ";
        }
        
        
        if ($order_by == '')
        {
			// Default Sort Here
            $sql .= " ORDER BY product_reviews.id DESC ";
        } 
		else 
		{
			$order_by = db_esc($order_by);
			
			$sql .= " ORDER BY $order_by ";

			if($order_asc == "DESC" || $order_asc == "" || $order_asc == false){
				$order_asc = false;
			} else {
				$order_asc = true;
			}

			if ($order_asc)
			{
				$sql .= " ASC ";
			}
			else
			{
				$sql .= ' DESC ';
			}
		}
				return db_query_array($sql);
    }
		
	

   public static function get_review_by_product_and_customer($product_id='', $customer_id='')
    {

//        return self::get(0, $product_id, $customer_id);
				        /* make sure we are dealing with good data */
        if (!$product_id || !is_numeric($product_id))
        {
            return null;
        }
			 	
       if (!is_numeric($customer_id)|| !($customer_id)) return false;
      

			 
			 $sql = "SELECT DISTINCT product_reviews.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name,pending_review.title as edited_title, pending_review.comment as edited_comment
                FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
                LEFT JOIN customers ON product_reviews.customer_id = customers.id
								LEFT JOIN pending_review ON product_reviews.id= pending_review.review_id
                WHERE product_reviews.product_id={$product_id} 
								AND product_reviews.customer_id={$customer_id}
								";
								//echo $sql;
        return db_query_array($sql);

    }

		   public static function get_review_by_email($product_id='' ,$email='')
    {
//        return self::get(0, $product_id, $customer_id);
				        /* make sure we are dealing with good data */
        if (!$product_id || !is_numeric($product_id))
        {
            return null;
        }

			 $sql = "SELECT DISTINCT product_reviews.*, products.name as product_name, pending_review.title as edited_title, pending_review.comment as edited_comment
                FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
								LEFT JOIN pending_review ON product_reviews.id= pending_review.review_id
                WHERE product_reviews.product_id={$product_id} 
								AND product_reviews.email='{$email}'
								";
								//echo $sql;
        return db_query_array($sql);

    }
		public static function get_all_review_info($approved='')
		{
		$sql = "SELECT product_reviews.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name, customers.img_url as customer_img_url
		FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
                LEFT JOIN customers ON product_reviews.customer_id = customers.id
								WHERE 1";
                if ($approved){
		               if ($approved !='I')
								        $sql.="  AND product_reviews.approved='{$approved}'";
	               	}	
														 
        return db_query_array($sql);
		
		}
		
		public static function get1_all_review_info($review_id='')
		{
			if (!is_numeric($review_id) || !$review_id)return false;
//		  $sql = "SELECT product_reviews.*, products.name as product_name		
		$sql = "SELECT product_reviews.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name, customers.img_url as customer_image
                FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
                LEFT JOIN customers ON product_reviews.customer_id = customers.id";
							  $sql=$sql." WHERE product_reviews.id={$review_id}"; 
                //echo $sql;
								$result= db_query_array($sql);
				        return $result[0];
		
		}
    public static function get_reviews_for_product($product_id='',$approved='')
    {
        /* make sure we are dealing with good data */
        if (!$product_id || !is_numeric($product_id))
        {
            return null;
        }

        $sql = "SELECT DISTINCT product_reviews.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name,customers.img_url as customer_img_url
				        FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
                LEFT JOIN customers ON product_reviews.customer_id = customers.id
                WHERE product_reviews.product_id={$product_id}
								";
                if ($approved){
								$sql=$sql." AND product_reviews.approved='{$approved}'"; 
								
								}
        return db_query_array($sql);
    }

    public static function get1($id='')
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info='', $product_id='', $customer_id='')
    {
        if ($product_id) $info['product_id']     = $product_id;
        if ($customer_id) $info['customer_id']    = $customer_id;
			    $safetitle=mysql_real_escape_string($info['title'] );
				$safecomment=mysql_real_escape_string($info['comment'] );
				$info['img_caption']=mysql_real_escape_string($info['img_caption']);
				$info['video_caption']=mysql_real_escape_string($info['video_caption']);
				$info['title']=$safetitle;
				$info['comment']=$safecomment;	
						
				if (!$info['approved']){
				$info['approved']='P';
        }
        return db_insert('product_reviews', $info);
    }

    public static function delete($id='')
    {
    	if (!is_numeric($id))return false;
        /*delete terms relationship to review*/
				$allterms=array();
				Review_Terms::associate2Review($allterms,$id,'Y');
				return db_delete('product_reviews', $id);
    }

    public static function update($id='', $info='')
		
    {    
    	if (!is_numeric($id))return false;
    	if ($info['title']){
    		$safetitle=mysql_real_escape_string($info['title'] );
				$info['title']=$safetitle;
				}
				if ($info['comment']){
				$safecomment=mysql_real_escape_string($info['comment'] );
				$info['comment']=$safecomment;
				}				
				if ($info['img_caption'])	  $info['img_caption']=mysql_real_escape_string($info['img_caption']);
				if ($info['video_caption']) $info['video_caption']=mysql_real_escape_string($info['video_caption']);

		$result= db_update('product_reviews', $id, $info );
		$rating = Product_Ratings::get_rating_for_product($info['product_id']);
					$info_2=array();
			$info_2['rating']=$rating['percentage'];
			db_update('products',$info['product_id'],$info_2);
			return $result;
    }

static function searchProductforReviews($productkeyword='',$customerkeyword='',$approved='',$order_by = '', $order_asc = '')
	{
		$productkeyword = trim($productkeyword);
		$customerkeyword = trim($customerkeyword);
		$productstring = '%'.str_replace(' ','%',  addslashes($productkeyword) ).'%';
		$customerstring = '%'.str_replace(' ','%',  addslashes($customerkeyword) ).'%';

    if (($productkeyword && $productkeyword != '')||($customerkeyword && $customerkeyword != '')) {
    $q = " SELECT DISTINCT  product_reviews.*, products.name as product_name,products.id as product_id, 
 					 customers.first_name AS customer_first_name, customers.last_name AS customer_last_name, customers.id AS cus_id, customers.email AS cus_email
					 FROM product_reviews
					 LEFT JOIN products on products.id=product_reviews.product_id
					 LEFT JOIN customers on customers.id=product_reviews.customer_id
					 WHERE 1 ";
    if ($approved){
	         if ($approved !='I')
				        $q.=" AND product_reviews.approved='{$approved}'";
	      }	
									
									
	if ($customerkeyword ){
			  $q .= " AND ";
		      $fields = Array('customers.first_name',
			    'customers.last_name',
			    'customers.id',
			    'customers.email', 
                'product_reviews.name',
				'product_reviews.email');		

      $q .= db_split_keywords($customerstring,$fields,'OR',true);
		 
		   }			 
		if ($productkeyword){
		
	  $q .= " AND ";
		      $fields = Array('products.name',
			    'products.id',
	         );

      $q .= db_split_keywords($productstring,$fields,'OR',true);

		   }	 
    
    }
    else{
        	$q .= "1";
        }
       if ($order_by == '')
        {
			// Default Sort Here
               $q .= "	GROUP BY product_reviews.id";
        } 
		else 
		{
			$order_by = db_esc($order_by);
			
			$q .= " ORDER BY $order_by ";

			if($order_asc == "DESC" || $order_asc == "" || $order_asc == false){
				$order_asc = false;
			} else {
				$order_asc = true;
			}

			if ($order_asc)
			{
				$q .= " ASC ";
			}
			else
			{
				$q .= ' DESC ';
			}
		}
        //echo "<pre>$q</pre>";
      //  print_ar($q);
        return db_query_array($q);
	}					

    public static function helpful($review_id='',$add='')
    {
     $review=self::get1($review_id);
		 if ($add=='Y'){
		    $review[helpful_grade]++;
				$review[helpful]++;
		 }
		
		 else {
		   $review[helpful_grade]-- ;
			 $review[not_helpful]++;
		 }
     unset ($review['title']);
		 unset ($review['comment']);
		 return self::update($review_id,$review);
    }
		
		public static function mosthelpful($product_id='')
		{
			if (!is_numeric($product_id) || !$product_id) return false;
		$sql="SELECT product_reviews.*, customers.first_name as customer_first_name,customers.last_name as customer_last_name FROM product_reviews  
	 		 LEFT JOIN customers ON product_reviews.customer_id = customers.id
			 WHERE helpful_grade = (SELECT max( helpful_grade )FROM product_reviews WHERE product_reviews.product_id ={$product_id})
			 AND product_reviews.product_id = {$product_id}
			 AND product_reviews.approved = 'Y'";
			 return db_query_array($sql);
		}
		

				public static function mostposneg($product_id='',$posneg='')
  		 { 
  		 	if (!is_numeric($product_id) || !$product_id) return false;
			  $sql="SELECT product_reviews. * , customers.first_name AS customer_first_name, customers.last_name AS customer_last_name
        FROM product_reviews
        LEFT JOIN customers ON product_reviews.customer_id = customers.id
        AND product_reviews.product_id ={$product_id}
        AND product_reviews.approved = 'Y'";

			 if ($posneg=='p'){
			 $sql .= " AND product_reviews.rating >= 3";
			 }
 			 if ($posneg=='n'){
			 $sql .= " AND product_reviews.rating < 3";
			 }
			 
			 $result=db_query_array($sql);
		   $max=0;
			 foreach ($result as $re)
			 {
			 if ($re['helpful_grade']>$max)
			     {
					  $max=$re['helpful_grade'];
						$helpful=$re;
					 }
	
			 }
		   
			 return $helpful;
		
		}
		
		
				public static function posneg($product_id='',$posneg='')
		{
			if (!is_numeric($product_id) || !$product_id) return false;
		   $sql = "SELECT product_reviews.*, customers.first_name as customer_first_name,customers.last_name as customer_last_name,customers.img_url as customer_image FROM product_reviews  
	 		 LEFT JOIN customers ON product_reviews.customer_id = customers.id";
			 $sql .=" AND product_reviews.product_id = {$product_id}
			 			    AND product_reviews.approved = 'Y'";

			 if ($posneg=='p'){
			 $sql .= " AND product_reviews.rating > 2";
		   $sql .=" ORDER BY product_reviews.rating DESC";
		   }
 			 if ($posneg=='n'){
			 $sql .= " AND product_reviews.rating < 4";
			 $sql .=" ORDER BY product_reviews.rating ASC";
		   }
  
			 return db_query_array($sql);
		}
		
		public static function sortReviewsForProduct($product_id='',$term='')
		{
			if (!is_numeric($product_id) || !$product_id) return false;
		   $sql = "SELECT customers.first_name as customer_first_name,customers.last_name as customer_last_name,customers.img_url as customer_image, product_reviews.* FROM product_reviews 
	 		 LEFT JOIN customers ON product_reviews.customer_id = customers.id
 			 WHERE product_reviews.product_id = {$product_id}
			 AND product_reviews.approved = 'Y'";
			 
			 $reviews=db_query_array($sql);
			 
/*			 foreach ($reviews as $review)
			 {
			  $oldrating=Product_Ratings::get_rating_by_product_and_customer($product_id, $review['customer_id']);
				if (!$oldrating){   //there's already a rating for this review, just update it.
	      Product_Ratings::insert(array('rating' => 0),$product_id,$review['customer_id']);
	      }
			 }
*/			 
			 $sql = "SELECT customers.first_name as customer_first_name,customers.last_name as customer_last_name,customers.img_url as customer_image, product_reviews.* FROM product_reviews 
	 		 LEFT JOIN customers ON product_reviews.customer_id = customers.id
 			 WHERE product_reviews.product_id = {$product_id}
			 AND product_reviews.approved = 'Y'";
			 
				 
			 switch ($term) {
			 case 'newest':
		          $sql .=" ORDER BY product_reviews.ts DESC";
       break;
			 case 'oldest':
		          $sql .=" ORDER BY product_reviews.ts ASC";
       break;
			 case 'lrating':
		          $sql .=" ORDER BY product_reviews.rating ASC";
       break;
			 case 'hrating':
		          $sql .=" ORDER BY product_reviews.rating DESC";
       break;
			 case 'mosthelpful':
		          $sql .=" ORDER BY product_reviews.helpful_grade DESC";
       break;
			 case 'leasthelpful':
		          $sql .=" ORDER BY product_reviews.helpful_grade ASC";
       break;			 			 
			 
				}
       return db_query_array($sql);               
	 
		}
	static function getCustomersForReminderEmail(){
 
		 $sql = "SELECT orders.order_tax_option, orders.shipping_first_name AS first_name, orders.shipping_last_name AS last_name, orders.email, orders.id AS oid, orders.review_email_sent AS sent_email, customers.disable_review_alert, customers.id AS cid, orders.date
                 FROM orders
				 LEFT JOIN customers ON orders.customer_id = customers.id
				 WHERE 1
				 AND orders.review_email_sent = 'N'
				 AND (
				 customers.disable_review_alert = 'N'
				 OR customers.disable_review_alert IS NULL
				 )
				 AND DATE_FORMAT( orders.date, '%m/%d/%Y' ) = DATE_FORMAT( DATE_SUB( SYSDATE( ) , INTERVAL 25 DAY ) , '%m/%d/%Y' ) 
				 AND orders.current_status_id =4
				 AND order_tax_option =1";
			
		//sql .=" AND customers.id =5926";
		return db_query_array($sql);
	}
	
	static function SetReminderEmailSent($order_id=''){
		if (!is_numeric($order_id))return false;
		$sql = " UPDATE orders SET review_email_sent = 'Y' WHERE id = '$order_id' " ;
			
		return db_query_array($sql);
		
	}
	
  static function howManyRecommends($productid='')
	{
		if (!is_numeric($productid) || !$productid) return false;
	  $sql = "SELECT DISTINCT product_reviews.*
                FROM product_reviews
                WHERE product_reviews.product_id={$productid}
								AND product_reviews.approved='Y'
								AND product_reviews.recommend='Y'"; 
		$result=db_query_array($sql);
		return count($result);						
	}
	
	   static function getReviewSize($review=''){
			/*caluclate size of review box*/
		  $size[height]=190;
			$desSize=strlen($review['comment']);
			$size[width]=700;
      if ($desSize<120) $size[width]=$desSize*6+40;
			if ($size[width]<320) $size[width]=320;
		  if (($review['img_url'])&&($review['img_approved']=='Y')) $size[height] =$size[height]+150;
		  if (($review['video_url'])&&($review['video_approved']=='Y')) {$size[height]=$size[height]+ 230;$size[width]=$size[width]+ 50;}
			$lines=$desSize/120;
			$size[height]=$size[height]+$lines*14;
			$size[height]=ceil($size[height]);
			$size[width]=ceil($size[width]);
		  if (Customers::isVerifiedBuyer($review['customer_id'],$review['product_id']))
			  $size[height]=$size[height]+35;
		
			return($size);
		
			}
	
	static function display_user_comments($product_id='')
	{
		
		$reviews = Product_Reviews::get("",$product_id,"","","","","'Y'");
		$num_reviews  = 0;
		
		if ($reviews)
		{
			$mosthelpfulp=Product_Reviews::mostposneg($product_id,'p');
			$mosthelpfuln=Product_Reviews::mostposneg($product_id,'n');
			
			if (count($reviews)>1 && (($mosthelpfuln['id'])&&($mosthelpfulp['id']))&&($mosthelpfuln['id']!=$mosthelpfulp['id'])){
				?>
				<div class="review-comments-holder">
					<div class="review-comment-box" id="mosthelpfulp">
						<span class="comment-title">Most Helpful Positive Review</span>
						<ul class="rating">
							<li class="setted" style="width:<?= ($mosthelpfulp['rating']) ? $mosthelpfulp['rating'] * 18 : 0 ?>px;"></li>
						</ul>
						<span class="comment-reason"><?= stripslashes($mosthelpfulp['title']);?></span>
						<p><?= StdLib::addEllipsis(stripslashes($mosthelpfulp['comment']),150,false,false); ?></p>
						<?$size=Product_Reviews::getReviewSize($mosthelpfulp);	?>
						<div class="comment-info">
							<a class="read-more" href="" onclick="TINY.box.show({url:'view_review.php',post:'ids=<?=$product_id?>,<?=$mosthelpfulp['customer_id']?>',width:<?=$size[width]?>,height:<?=$size[height]?>});return false;">Read Full Review &gt;&gt;</a>
							<em class="comment-date">Published on <?=date("F j, Y", strtotime($mosthelpfulp['ts']));?> by <?=$mosthelpfulp['customer_first_name'];?>&nbsp;<?=$mosthelpfulp['customer_last_name'];?></em>
						</div>
					</div>  
					
					<div class="review-comment-box" <? echo ($mosthelpfulp['id'] ? 'id="mosthelpfuln"':'id="mosthelpfulp"');?>>
						<span class="comment-title">Most Helpful Negative Review</span>
						<ul class="rating">
							<li class="setted" style="width:<?= ($mosthelpfuln['rating']) ? $mosthelpfuln['rating'] * 18 : 0 ?>px;"></li>
						</ul>
						<span class="comment-reason"><?= stripslashes($mosthelpfuln['title']);?></span>
						<p><?= StdLib::addEllipsis(stripslashes($mosthelpfuln['comment']),150,false,false); ?></p>
						<?$size=Product_Reviews::getReviewSize($mosthelpfuln);?>
						<div class="comment-info">
							<a class="read-more" href="" onclick="TINY.box.show({url:'view_review.php',post:'ids=<?=$product_id?>,<?=$mosthelpfuln['customer_id']?>',width:<?=$size[width]?>,height:<?=$size[height]?>});return false;">Read Full Review &gt;&gt;</a>
							<em class="comment-date">Published on <?=date("F j, Y", strtotime($mosthelpfuln['ts']));?> by <?=$mosthelpfuln['customer_first_name'];?>&nbsp;<?=$mosthelpfuln['customer_last_name'];?></em>
						</div>
					</div> 
				</div>
			<?php  
			}
			
      		$allterms1=Review_Terms::getProductTerms(0,1,$reviews,"'Y'");
		    $allterms2=Review_Terms::getProductTerms(0,2,$reviews,"'Y'");
			$allterms3=Review_Terms::getProductTerms(0,3,$reviews,"'Y'");

			if (($allterms1)&&($allterms2)&&($allterms3)){
				$totals = array();
				echo"<div class='product-response'><div class='productterms'><label>Pros:</label><ul>";
				foreach ($allterms1 as $term){
                    if (!isset($totals[$term['id']]))
                    	$totals[$term['id']]['count'] = 0;
                	$totals[$term['id']]['count']++;
					$totals[$term['id']]['description']=$term['description'];
				}
              	$i=0;
				foreach($totals as $te)
				{
               		if ($i<5){ 							
					echo "<li>". stripslashes($te['description'])." (".$te['count'].")</li>";
					}
					$i++;
				}
				echo "</ul></div>";									 

				$totals = array();
				echo"<div class='productterms'><label>Cons:</label><ul>";
				foreach ($allterms2 as $term){
                	if (!isset($totals[$term['id']]))
                		$totals[$term['id']]['count'] = 0;
                	$totals[$term['id']]['count']++;
					$totals[$term['id']]['description']=$term['description'];
				}
              	$i=0;
				foreach($totals as $te)
				{ 
					if ($i<5){
						echo "<li>". stripslashes($te['description'])." (".$te['count'].")</li>";
					}
					$i++;	 
				}
				echo "</ul></div>";	
				
				$totals = array();
				echo"<div class='productterms'><label>Best Uses:</label><ul>";
				foreach ($allterms3 as $term){
                	if (!isset($totals[$term['id']]))
                    	$totals[$term['id']]['count'] = 0;
                	$totals[$term['id']]['count']++;
					$totals[$term['id']]['description']=$term['description'];
				}
              	$i=0;
              	foreach($totals as $te)
				{
				 	if ($i<5){
				  		echo "<li>". stripslashes($te['description'])." (".$te['count'].")</li>";
				 	}
				 	$i++;
				}
				echo "</ul></div>";		
				echo "</div><br><br>";							 
			} 
			
			if ($_POST['rid']) $rid=$_POST['rid'];       //get from the login popup the current reivew
			else if ($_GET['rid']) $rid=$_GET['rid'];
			if (!$rid) $rid=0;
			
			if ($_POST['sort']) $sort=$_POST['sort'];       //get from the login popup the current reivew
			else if ($_GET['sort']) $sort=$_GET['sort'];
			if (!$sort) $sort='newest';	
			?>
			
			<div class="review-full-comments-top" id="review-top">
				<h3>Product Reviews <em>Reviewed by <?echo (count($reviews)==1) ? 'one customer' : count($reviews).' customers'?></em></h3>
						</div>
			<?php 
				if ($_SESSION['cust_acc_id']) {
					$review = Product_Reviews::get_review_by_product_and_customer($product_id, $_SESSION['cust_acc_id']);
					$onclick = '';
				} else {
				//	$onclick = "onclick=\"TINY.box.show({url:'popuplogin.php',post:'link=".$CFG->baseurl . "product_review.php?product_id=" . $product_id."'});return false;\"";
				}
				
				if(count($reviews)==0){
					$text_to_write = 'Be the first to review this product';
				} elseif($review){
					$text_to_write = 'Edit your review';
				}else {
					$text_to_write = 'Review this product'; 
				}
			?>
			<span class="product-actions">
				<a href="<?=$CFG->baseurl . "product_review.php?product_id=" . $product_id?>" <?= $onclick?>> <?=$text_to_write?></a>
			</span>
	
				<div class="comments-sort">
					<div class="comment-sort-box">
						<form  id="sort_box">
						<label for="sortby">Sort by:</label>
							<select id="sortby" name="sortby" onchange="showreviews(<?=$product_id?>,this.value)">
								<option  value="newest" <?= ($sort == 'newest') ? ' selected=selected' : '' ?> >Newest</option>
						        <option  value="oldest" <?= ($sort == 'oldest') ? ' selected=selected' : '' ?>>Oldest</option>
						        <option  value="hrating" <?= ($sort == 'hrating') ? ' selected=selected' : '' ?>>Highest Rating</option>
						        <option  value="lrating" <?= ($sort == 'lrating') ? ' selected=selected' : '' ?>>Lowest Rating</option>
								<option  value="p" <?= ($sort == 'p') ? ' selected=selected' : '' ?>>Positive Reviews</option>
								<option  value="n" <?= ($sort == 'n') ? ' selected=selected' : '' ?>>Negative Reviews</option>
						        <option  value="mosthelpful" <?= ($sort == 'mosthelpful') ? ' selected=selected' : '' ?>>Most Helpful</option>
						        <option  value="leasthelpful" <?= ($sort == 'leasthelpful') ? ' selected=selected' : '' ?>>Least Helpful</option>
							</select>
						</form>
					 </div>
				</div>
				<?php 
				$numre=Product_Reviews::howManyRecommends($product_id);
				if ($numre){
					echo ($numre==1 ? "<span class='count-recommend'>One person recommends this product" : $numre." people recommend this product")."</span><br/><br/>";
				}
				?>
			<div id="review-section-wrap">
				<?	listreviews($product_id,$sort,$rid) ?>			 
			</div>
	    <?
		}else{ 
		?>
			<h3><label>No Reviews Yet</label></h3> 
			<p>Be The First To Review This Product!</p>
		<?
		}
	}
}
?>