<? 
class AbleKitchenExtraCharges
{
	static function get($id=0, $order_id = 0, $already_billed = '', $order_already_billed = '', $see_old_and_new = false)
	{
		$sql = "SELECT ";
	    $sql .= " able_kitchen_extra_charges.* ";
	    $sql .= ", `orders`.`3rd_party_order_number`";		
		$sql .= " FROM (able_kitchen_extra_charges, orders)";
		
		if ($order_already_billed != '') $sql .= "LEFT JOIN able_kitchen_billed ON able_kitchen_extra_charges.order_id = able_kitchen_billed.order_id";
		
		$sql .= " WHERE 1 ";
		
		$sql .= " AND able_kitchen_extra_charges.order_id = orders.id";
		if (!$see_old_and_new) $sql .= " AND CAST(orders.`3rd_party_order_number` AS UNSIGNED) < 100000";
		
		if ($order_already_billed == 'Y')
		{
			$sql .= " AND able_kitchen_billed.order_id IS NOT NULL";
		} 
		
		if ($id > 0) {
		    $sql .= " AND able_kitchen_extra_charges.id = $id ";
		}
		if ($order_id > 0) {
		    $sql .= " AND able_kitchen_extra_charges.order_id = $order_id ";
		}
		if ($already_billed != '')
		{
			$sql .= " AND able_kitchen_extra_charges.billed = '".$already_billed ."'";
		}
				
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id, 0, '', '', true);
		
		return $ret[0];
	}
	
	static function getTotalExtraChargesForOrder($order_id)
	{
		if (!$order_id) return '0.00';
		$sql = "SELECT SUM(amount) AS ec_sum FROM able_kitchen_extra_charges WHERE order_id = $order_id";
		$result = db_query_array($sql);		
		if ($result && $result[0]['ec_sum']) return $result[0];
		else return '0.00';
	}
	
	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('able_kitchen_extra_charges', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
		
		db_insert('able_kitchen_extra_charges', $info);
		$id = db_insert_id();

		return true;
		
	}
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
		$ret = db_update('able_kitchen_extra_charges',$id,$info);

		return $ret;
	}	
	static function get_distinct_charge_types()
	{
    	$type = db_query_array( "SHOW COLUMNS FROM able_kitchen_extra_charges WHERE Field = 'charge_type'" );
    	
    	$type_result = $type[0]['Type'];
    	
    	preg_match('/^enum\((.*)\)$/', $type_result, $matches);
    	foreach( explode(',', $matches[1]) as $value )
    	{
        	 $enum[] = trim( $value, "'" );
    	}
    	//print_r($enum);
    	
    	return $enum;
	}

}
?>