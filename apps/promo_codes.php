<?

class PromoCodes
{
	
	function insertCode($info)
	{
		return db_insert('promo_codes', $info);
	}
	
	function updateCode($id, $info)
	{
		return db_update('promo_codes', $id, $info);
	}
	
	function deleteCode($id)
	{
		return db_delete('promo_codes', $id);
	}
	
	function getCodes($id = 0, $code = '', $expire_begin = '', $expire_end = '', $start_begin = '', $start_end = '')
	{
		$q = " SELECT promo_codes.* FROM promo_codes WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('id', $id);
		}
		
		if ($code) {
			$q .= db_restrict('code', $code);
		}	
		
		if ($expire_begin && $expire_end) {
			$q .= db_queryrange('expire_date', $expire_begin, $expire_end);
		}
		
		if ($start_begin && $start_end) {
			$q .= db_queryrange('start_date', $start_begin, $start_end);
		}		
		
		return db_query_array($q);
	}
	
	function get1Code($id)
	{
		return db_get1(PromoCodes::getCodes($id));
	}
	
	function insertCodeRequirement($info) 
	{
		return db_insert('promo_code_require_products', $info);
	}
	
	function updateCodeRequirement($id, $info)
	{
		return db_update('promo_code_require_products', $id, $info);
	}
	
	function deleteCodeRequirement($id) 
	{
		return db_delete('promo_code_require_products', $id);
	}
	
	function getCodeRequirements($id = 0, $promo_code_id = 0, $product_id = 0)
	{
	  $q = " SELECT promo_code_require_products.*, products.name as product_name,products.vendor_sku as vendor_sku FROM promo_code_require_products LEFT JOIN products ON (products.id = promo_code_require_products.product_id) WHERE 1 ";
	  
	  if ($id) {
	  	$q .= db_restrict('promo_code_require_products.id', $id);
	  }
	  
	  if ($promo_code_id) {
	  	$q .= db_restrict('promo_code_id', $promo_code_id);
	  }
	  
	  if ($product_id) {
	  	$q .= db_restrict('product_id', $product_id);
	  }
	  
	  return db_query_array($q);
	}

	function insertCodeCatRequirement($info) 
	{
		$symbolic_cats = Cats::getSymbolicSubs($info['cat_id']);
		$subcats= Cats::getSubCats($info['cat_id']);
		$pid=$info['cat_id'];
	    if ($symbolic_cats) 
			foreach ($symbolic_cats as $s_cat){
				$info['cat_id']=$s_cat['id'];
				if (!self::getCodeCatRequirements("", $info['promo_code_id'], $info['cat_id']))
		           db_insert('promo_code_require_cats', $info);
			}
        foreach ($subcats as $sub){
			$symbolic_cats = Cats::getSymbolicSubs($sub['id']);
			if ($symbolic_cats) 
			foreach ($symbolic_cats as $s_cat){
				$info['cat_id']=$s_cat['id'];
				if (!self::getCodeCatRequirements("", $info['promo_code_id'], $info['cat_id']))
	            db_insert('promo_code_require_cats', $info);
			}
			
		}			
	   
       $promo_info=self::get1Code($info['promo_code_id']);
	  
	   if ($promo_info['req_cat_type']=='Exclude'){                                //if setting is to exclude update all other req_cats with the new qty
           $other_req_cats=self::getCodeCatRequirements("",$promo_info['id']);	   
		   if ($other_req_cats && $other_req_cats[0]['qty']!=$info['qty']){
		      $other_req_cats[0]['qty']=$info['qty'];
			  unset ($other_req_cats[0]['cat_name']);
		      self::updateCodeCatRequirement($other_req_cats[0]['id'],$other_req_cats[0]);
		   }
	   }
       $info['cat_id']=$pid;
	   $alreadythere=self::getCodeCatRequirements("", $info['promo_code_id'], $info['cat_id']);
       if (!$alreadythere)	   
	      return db_insert('promo_code_require_cats', $info);
	   else self::updateCodeCatRequirement($alreadythere[0]['id'],$info);
	}
	function updateCodeCatRequirement($id, $info)
	{
		$symbolic_cats = Cats::getSymbolicSubs($info['cat_id']);
		$pid=$info['cat_id'];
	    if ($symbolic_cats) 
			foreach ($symbolic_cats as $s_cat){
				$temp_info=self::getCodeCatRequirements("", $info['promo_code_id'], $s_cat['id']);
				if ($temp_info)	{
				  $temp_info[0]['qty']=$info['qty'];
				  unset ($temp_info[0]['cat_name']);
				  db_update('promo_code_require_cats', $temp_info[0]['id'], $temp_info[0]);
				}
			}	
		$promo_info=self::get1Code($info['promo_code_id']);
		if ($promo_info['req_cat_type']=='Exclude'){                                //if setting is to exclude update all other req_cats with the latest qty
              $other_req_cats=self::getCodeCatRequirements("",$promo_info['id']);
			  foreach ($other_req_cats as $req_cat){
				  $req_cat['qty']=$info['qty'];
				  unset ($req_cat['cat_name']);
				  db_update('promo_code_require_cats', $req_cat['id'], $req_cat);
			  }               
        }		   
        $info['cat_id']=$pid;	   
		return db_update('promo_code_require_cats', $id, $info);
	}
	
	function deleteCodeCatRequirement($id) 
	{
		$temp_info=self::getCodeCatRequirements($id);
	    $symbolic_cats = Cats::getSymbolicSubs($temp_info[0]['cat_id']);
		$symbolic_ids=array();
		foreach ($symbolic_cats as $s){
			$symbolic_ids[]=$s['id'];
		}
		$req_cats=self::getCodeCatRequirements ("",$temp_info[0]['promo_code_id']);
		$no_del=array();
		foreach ($req_cats as $req){
			 if ($req['id']!=$id){
				 $other_symb=Cats::getSymbolicSubs($req['cat_id']);
				 $other_symbolic_ids=array();
		         foreach ($other_symb as $s){
			       $other_symbolic_ids[]=$s['id'];
		        }
				foreach ($other_symbolic_ids as $sym){
				      if (in_array($sym,$symbolic_ids))
                      $no_del[]=$sym;					 
 			    }	
			}		
		}
		if ($symbolic_cats) {
			foreach ($symbolic_ids as $s_cat){
				if (!(in_array($s_cat,$no_del))){
					$temp_del=self::getCodeCatRequirements ("",$temp_info['promo_code_id'],$s_cat);
					db_delete('promo_code_require_cats', $temp_del[0]['id']);
			    }
			}
		}	
        return db_delete('promo_code_require_cats', $id);
	}
	
	function getCodeCatRequirements($id = 0, $promo_code_id = 0, $cat_id = 0)
	{
	  $q = " SELECT promo_code_require_cats.*, cats.name as cat_name  FROM promo_code_require_cats LEFT JOIN cats ON (cats.id = promo_code_require_cats.cat_id) WHERE 1 ";
	  
	  if ($id) {
	  	$q .= db_restrict('promo_code_require_cats.id', $id);
	  }
	  
	  if ($promo_code_id) {
	  	$q .= db_restrict('promo_code_id', $promo_code_id);
	  }
	  
	  if ($cat_id) {
	  	$q .= db_restrict('cat_id', $cat_id);
	  }
	  return db_query_array($q);
	}
	function insertCodeOccRequirement($info) 
	{
		return db_insert('promo_code_require_occasions', $info);
	}
	
	function updateCodeOccRequirement($id, $info)
	{
		return db_update('promo_code_require_occasions', $id, $info);
	}
	
	function deleteCodeOccRequirement($id) 
	{
		return db_delete('promo_code_require_occasions', $id);
	}
	
	function getCodeOccRequirements($id = 0, $promo_code_id = 0, $occasion_id = 0)
	{
	  $q = " SELECT promo_code_require_occasions.*, occasions.name as occasion_name FROM promo_code_require_occasions LEFT JOIN occasions ON (occasions.id = promo_code_require_occasions.occasion_id) WHERE 1 ";
	  
	  if ($id) {
	  	$q .= db_restrict('promo_code_require_occasions.id', $id);
	  }
	  
	  if ($promo_code_id) {
	  	$q .= db_restrict('promo_code_id', $promo_code_id);
	  }
	  
	  if ($occasion_id) {
	  	$q .= db_restrict('occasion_id', $occasion_id);
	  }
	  
	  return db_query_array($q);
	}	
	
	function insertCodeProductDiscount($info)
	{
		return db_insert('promo_code_product_discounts', $info);
	}
	
	function updateCodeProductDiscount($id, $info)
	{
		return db_update('promo_code_product_discounts', $id, $info);
	}
	
	function deleteCodeProductDiscount($id)
	{
		return db_delete('promo_code_product_discounts', $id);
	}
	
	function getCodeProductDiscount($id = 0, $promo_code_id = 0, $product_id = 0)
	{
		$q = " SELECT promo_code_product_discounts.*, products.name as product_name, products.price as product_price FROM promo_code_product_discounts LEFT JOIN products on (products.id = promo_code_product_discounts.product_id) WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('promo_code_product_discounts.id', $id);
		}
		
		if ($promo_code_id) {
			$q .= db_restrict('promo_code_id', $promo_code_id);
		}
		
		if ($product_id) {
			$q .= db_restrict('product_id', $product_id);
		}
		
		return db_query_array($q);
		
	}
	
	function insertCodeCatDiscount($info)
	{
		return db_insert('promo_code_cat_discounts', $info);
	}
	
	function updateCodeCatDiscount($id, $info)
	{
		return db_update('promo_code_cat_discounts', $id, $info);
	}
	
	function deleteCodeCatDiscount($id)
	{
		return db_delete('promo_code_cat_discounts', $id);
	}
	
	function getCodeCatDiscount($id = 0, $promo_code_id = 0, $cat_id = 0)
	{
		$q = " SELECT promo_code_cat_discounts.*, cats.name as cat_name FROM promo_code_cat_discounts LEFT JOIN cats on (cats.id = promo_code_cat_discounts.cat_id) WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('promo_code_cat_discounts.id', $id);
		}
		
		if ($promo_code_id) {
			$q .= db_restrict('promo_code_id', $promo_code_id);
		}
		
		if ($cat_id) {
			$q .= db_restrict('cat_id', $cat_id);
		}
		
		return db_query_array($q);
		
	}

	function insertCodeOccDiscount($info)
	{
		return db_insert('promo_code_occasion_discounts', $info);
	}
	
	function updateCodeOccDiscount($id, $info)
	{
		return db_update('promo_code_occasion_discounts', $id, $info);
	}
	
	function deleteCodeOccDiscount($id)
	{
		return db_delete('promo_code_occasion_discounts', $id);
	}
	
	function getCodeOccDiscount($id = 0, $promo_code_id = 0, $occasion_id = 0)
	{
		$q = " SELECT promo_code_occasion_discounts.*, occasions.name as occasion_name FROM promo_code_occasion_discounts LEFT JOIN occasions on (occasions.id = promo_code_occasion_discounts.occasion_id) WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('promo_code_occasion_discounts.id', $id);
		}
		
		if ($promo_code_id) {
			$q .= db_restrict('promo_code_id', $promo_code_id);
		}
		
		if ($occasion_id) {
			$q .= db_restrict('occasion_id', $occasion_id);
		}
		
		return db_query_array($q);
		
	}
	
	function checkStartDate($coupon_info,$order_id='')
	{
		if($order_id){
			
			$order_info = Orders::get1($order_id);
			$local_time_zone = ZipCodesUSA::get_local_time_zone($order_info['billing_zip']);
			$now = new DateTime("now");
			 
			$tz_name = timezone_name_from_abbr ('', ($local_time_zone['UTC'] *3600), 0 );
			
			if ($coupon_info['start_date_by_local_time'] == 'Y') 
			{
				$now = $now->setTimezone(new DateTimeZone($tz_name));
			}
			
			$now = $now->format('Y-m-d H:i:s');
			$now = strtotime($now);
			
			if ($coupon_info['start_date'] != "0000-00-00 00:00:00") {
				$coupon_starts = strtotime($coupon_info['start_date']);
			
				if ($now < $coupon_starts)
				  return false;
				
				return true;    
			}
			
		}else{
			global $cart;
			if ($_REQUEST['end_user_time']) $cart->data['end_user_time'] = $_REQUEST['end_user_time'];
			
			if ($cart->data['end_user_time'] && $coupon_info['start_date_by_local_time'] == 'Y') 
			{
				$now = strtotime($cart->data['end_user_time']); 
			}
			else $now = time();
			
			if ($coupon_info['start_date'] != "0000-00-00 00:00:00") {
			
				$coupon_starts = strtotime($coupon_info['start_date']);
				
				if ($now < $coupon_starts)
				  return false;
				
				return true;    
			}
		}
		
		// no start date (old coupon, from before start date added to system)
		return true;
	}
	
	function checkExpiration($coupon_info,$order_id='',$cart_reference='')
	{
		if($order_id || $cart_reference){
			$now = time();
		}else{
			global $cart;
			if ($_REQUEST['end_user_time']) $cart->data['end_user_time'] = $_REQUEST['end_user_time'];
	
			if ($cart->data['end_user_time'] && $coupon_info['expire_date_by_local_time'] == 'Y') 
			{
				$now = strtotime($cart->data['end_user_time']); 
			}
			else $now = time();
		}
		
		
		$real_now = time();
						
		if ($coupon_info['expires'] == 'Y') {
			
			$coupon_expires = strtotime($coupon_info['expire_date']);
			
			if ($now > $coupon_expires || (($real_now - $now) > 25200 && $real_now > $coupon_expires))
			  return false;
			
			return true;    
		}
		
		// no expiration
		return true;
	}
		
	function checkUses($coupon_info,$order_id='')
	{
		if($order_id){
			//need to check if this coupon is on this order
			$order_promo = Orders::getPromoCodes($order_id,'',$coupon_info['id']);
			if($order_promo)
				return true;
		}
		if ($coupon_info['uses_limited'] == 'Y') {
			
			if ($coupon_info['uses_so_far'] < $coupon_info['uses'])
			  return true;
			  
			return false;
			
		}
		
		// no limit;
		return true;
		
	}
	
	function useCoupon($id)
	{
		
		$id = (int) $id;
		
		$q = "UPDATE promo_codes SET uses_so_far = uses_so_far + 1 WHERE id = $id";
		
		return db_query($q);
	}
	
	function unUseCoupon($id)
	{
	
		$id = (int) $id;
	
		$q = "UPDATE promo_codes SET uses_so_far = uses_so_far - 1 WHERE id = $id";
	
		return db_query($q);
	}
	
	function checkRequiredProducts($coupon_info, $order_id='', $cart_reference='')
	{
		
		$req_info = PromoCodes::getCodeRequirements(0, $coupon_info[id]);
		
		if($order_id){
			$q = " SELECT product_id, SUM(qty) as sum FROM order_items WHERE ( order_id = '$order_id' ";
			$q .= " ) AND product_id <> 0 GROUP BY product_id";
			
			$prod_info = db_query_array($q, 'product_id');
		}else{
			if($cart_reference){
				$reference = Cart::get1CartReference($cart_reference);
				$session_id = $reference['session_id'];
				if($reference['customer_id'])
					$cust_acc_id = $reference['customer_id'];
			} else {
				$session_id = session_id();
				if($_SESSION['cust_acc_id'])
					$cust_acc_id = $_SESSION['cust_acc_id'];
			}
						
			$q = " SELECT product_id, SUM(qty) as sum FROM cart WHERE ( session_id = '$session_id' ";
			if($cust_acc_id){
					$q .= " OR cart.customer_id = '" . $cust_acc_id . "' ";
				}
			$q .= " ) AND product_id <> 0 GROUP BY product_id";
			
			$prod_info = db_query_array($q, 'product_id');
		}
		
		if (!$req_info) return true;
		
		foreach ($req_info as $req) {
			
			// avoid division by 0. there should never be a row w/ the requirement of 0 anyway.
			$req_sum = $req[qty] > 0 ? $req[qty] : 1;
			$prod_sum = $prod_info[$req[product_id]][sum];

			if ($prod_sum > 0 && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($req[product_id], "none")))
			{
				$max_applications[] = floor($prod_sum / $req_sum);
			}
			else $max_applications[] = 0;								

		}
		
		//return (int) min($max_applications);
		return (int) max($max_applications); //  made it max cuz even if one is on there make it work.
		
	}
	

	function _check_required_cats_sort($a, $b)
	{
		
		if ($a[depth] == $b[depth]) {
    	return 0;
 		}
 		
 		return ($a[depth] > $b[depth]) ? -1 : 1;
	}
	
	function checkRequiredCats($coupon_info,$order_id='',$cart_reference='')
	{	
		
		$req_info = PromoCodes::getCodeCatRequirements(0, $coupon_info[id]);

		if (!$req_info) return true;
			
		$session_id = session_id();
		
		if($order_id){
			$info = Orders::getItemsGrouped($order_id);
		}else{
			$cart = new Cart();
			$info = $cart->getCartGrouped(false,$cart_reference);
		}
		
		if( !$info )
		    return false;
//print_r($info);
		$output = print_r($info, true);
				//mail("rachel@tigerchef.com", "", $output);
		foreach ($info as $index => $i) {
			
			$info[$index]['depth'] = Cats::getCatDepth($i[cat_id]);
		}
		//print_ar( $info ); 
		// order by DEPTH.  Consume the deeper levels first.	
		uasort($info, array('PromoCodes', '_check_required_cats_sort'));
		
		  $orig_req_info = $req_info;

		  $max_applications = 0;
		  $req_cat_array=array();
		  foreach ( $req_info as $c){
			           $req_cat_array[]=$c['cat_id'];
		  }
		  $req_qty=$req_info[0]['qty'];
		  for (;;) {

			$req_info = $orig_req_info;
			$cat_match = false;

			if ($coupon_info['req_cat_type']=='Include')					
			  foreach ($req_info as $index_i => $req) {

				$req_cat = $req['cat_id'];
				$req_qty = $req['qty'];

				foreach ($info as $index_j => $i) {

					$product_cats = Cats::getCatsForProduct( $i['product_id'], 'id' );
					//$product_cats = Cats::getCatsForProductSimple( $i['product_id']);
					$all_product_cats = $product_cats;
					foreach ($product_cats as $thePC)
					{
						$symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);

						if (count($symbolic_product_cats) > 0)
						{
							foreach($symbolic_product_cats as $spc)
							{
								$all_product_cats[$spc['id']] = $spc;
							}
						}

					}
					$product_cats = $all_product_cats;

					if (!$i['product_option_id']) $i['product_option_id'] = 0;	
                 	                    					 
                    if( $product_cats[$req_cat] && $i['sum'] >= $req['qty'] && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id'])))
					  {
// 						echo "MATCH";
					    $cat_match = true;
					    break;
					  }
				

				}

			}
			
			if ($coupon_info['req_cat_type']=='Exclude'){					
                $total_qty=0;
				foreach ($info as $index_j => $i) {


					$product_cats = Cats::getCatsForProduct( $i['product_id'], 'id' );
					//$product_cats = Cats::getCatsForProductSimple( $i['product_id']);
					$all_product_cats = $product_cats;
					foreach ($product_cats as $thePC)
					{
						$symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);

						if (count($symbolic_product_cats) > 0)
						{
							foreach($symbolic_product_cats as $spc)
							{
								$all_product_cats[$spc['id']] = $spc;
							}
						}

					}
				
				$product_cats = $all_product_cats;
				if (!$i['product_option_id']) $i['product_option_id'] = 0;	
				
				foreach ($product_cats as $cat => $pcat)					
                    	if(!in_array($cat,$req_cat_array)){
							$total_qty +=$i['sum'];
							break;
						}	
					
                 	
                    foreach ($product_cats as $cat => $pcat){					
                    
					if(!in_array($cat,$req_cat_array) && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id'])))
					  {
					    $cat_match = true;
					  }
					 else { 
					        $cat_match=false;    
					        break;
						  } 
                    }					
				

				}
				
				if ($total_qty < $req_qty) $cat_match=false;

			}


				/*foreach ($req_info as $r) {
					if ($r['qty'] > 0) {
					  return $max_applications;
					}
				}
				
				$max_applications++;*/
				return $cat_match;

		  }
	  
	  }
	  
	  function checkRequiredCatsandBrands($coupon_info,$order_id='',$cart_reference='')
	  {
	  	$session_id = session_id();
	  	$req_brand_info = PromoCodes::getCodeBrandRequirementsToCalc(0, $coupon_info[id],"",$coupon_info);
	  	$req_cat_info = PromoCodes::getCodeCatRequirements(0, $coupon_info[id]);
	  
	  	// 	  	echo "brand:".var_export($req_brand_info,true)."@cat:".var_export($req_cat_info,true)."@both:".var_export($brand_and_cat_requirments,true);
	$req_cat_array=array();
		 foreach ( $req_cat_info as $c){
		         $req_cat_array[]=$c['cat_id'];
		     }
	    $req_sum=$req_cat_info[0]['qty'];
		$total_qty_brand=0;
		$req_brand_array=array();
		 foreach ( $req_brand_info as $c){
		         $req_brand_array[]=$c['brand_id'];
		     }
	    $req_brand_sum=$req_brand_info[0]['qty'];
  
	  	if($order_id){
	  		$info = Orders::getItemsGroupedByOptions($order_id);
	  	}else{
	  		$cart = new Cart();
	  		$info = $cart->getCartGroupedByOptions(false,$cart_reference);
	  	}
	  	if( !$info ){
	  		return false;
	  	}
	  	if(!$req_brand_info && !$req_cat_info){
	  		return true;
	  	}
	  	foreach ($info as $key => $i) {
	  		$brand_sums[$i['brand_id']]['sum'] += $i['sum'];
	  		$product_cats = Cats::getCatsForProduct( $i['product_id'], 'id' );
	  		//$product_cats = Cats::getCatsForProductSimple( $i['product_id']);
	  		$all_product_cats = $product_cats;
	  		foreach ($product_cats as $thePC)
	  		{
	  			$cat_sums[$thePC['id']]['sum'] += $i['sum'];
	  			$symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);
	  			if (count($symbolic_product_cats) > 0)
	  			{
	  				foreach($symbolic_product_cats as $spc)
	  				{
	  					$all_product_cats[$spc['id']] = $spc;
	  					$cat_sums[$spc['id']]['sum'] += $i['sum'];
	  				}
	  			}
	  		}
	  		$info[$key]['all_cats'] = $all_product_cats;
			
			
			if($coupon_info['req_cat_type']=='Exclude'){
			foreach ($all_product_cats as $cat => $pcat)					
                      if(in_array($cat,$req_cat_array)){
						   $sum=0;
						   break;
					  }	   
					  else{ 
						  $sum=$i['sum'];
					  }
			}		  
		    
			if($coupon_info['req_brand_type']=='Exclude'){
			if(!in_array($i['brand_id'], $req_brand_array)){
						   $brand_sum=0;
					  }	   
					  else{ 
						  $brand_sum=$i['sum'];
					  }
			}		  
						            
            $total_qty +=$sum;
            $total_qty_brand +=$brand_sum;			
	  	}
		 
	    if (($coupon_info['req_cat_type']=='Exclude' && $coupon_info['req_brand_type']=='Exclude') && ($total_qty < $req_sum || $total_qty_brand < $req_brand_sum)) return false;                
		if (($coupon_info['req_cat_type']=='Exclude') && ($total_qty < $req_sum)) return false;                
        if (($coupon_info['req_brand_type']=='Exclude')&& ($total_qty_brand < $req_brand_sum)) return false;                

        $orig_req_cat_info = $req_cat_info;
	  	$orig_brand_req_info = $req_brand_info;
	  	$total_qty=0;
	  	for (;;) {
	  	
	  		$req_cat_info = $orig_req_cat_info;
	  		$req_brand_info = $orig_brand_req_info;
	  		
	  		$return = false;
			
	  		foreach ($info as $index_j => $i) {
				$cat_match = false;
	  		    $brand_match = false;   
                	  			  			
                if (!$i['product_option_id']){ $i['product_option_id'] = 0; }                
				
				if ($coupon_info['req_cat_type']=='Include')
	  			foreach ($req_cat_info as $index_i => $req) {
	  				
	  				$req_cat = $req['cat_id'];
	  				$req_qty = $req['qty'];
		
	  				 if( $i['all_cats'][$req_cat] && $cat_sums[$req_cat]['sum'] >= $req['qty'] && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id'])))
	  				 {
	  					$cat_match = true;
	  					break;
	  				 }
					 
				}	
				
				if ($coupon_info['req_cat_type']=='Exclude'){
					$allcats=array();
					$allcats=$i['all_cats'];
									   					
					foreach ($allcats as $cat => $pcat){
					  if (!(in_array($cat,$req_cat_array))&& ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id']))) 
	  				      {
	  					    $cat_match = true;
	  				      }
					else  {
						    $cat_match = false;
							break;
					       }	  
   
				      } 
                   		  
					}
			    
				  
	  			foreach ($req_brand_info as $index_i => $req) {
	  	
	  				$req_brand = $req['brand_id'];
	  				$req_qty = $req['qty'];
	  				$cart_brand = $i['brand_id'];
					if (!$i['product_option_id']) {$i['product_option_id'] = 0; }
					if ($coupon_info['req_brand_type']=='Exclude' && ($cart_brand == $req_brand && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id'])))||
	  				    $coupon_info['req_brand_type']=='Include' && ($cart_brand == $req_brand  && $brand_sums[$req_brand]['sum'] >= $req['qty'] && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id'])))) 
						{
	  					$brand_match = true;
					     break;
	  				}
	  			}
			
				
// 	  			echo "<br>catmatch:".var_export($cat_match,true)."@brandmatch:".var_export($brand_match,true)."@return:".var_export($return,true)."<br>";
				
	  			if( ($req_brand_info && $req_cat_info && $cat_match && $brand_match) || (!$req_cat_info && $req_brand_info && $brand_match) || (!$req_brand_info && $req_cat_info && $cat_match)){
	  				$return = true;
	  				break;
	  			}
				
			}
           
			break;
	  	}
 	  	//echo "<br><br>catmatch:".var_export($cat_match,true)."@brandmatch:".var_export($brand_match,true)."@return:".var_export($return,true)."<br>";

        return $return;
	  }
				

    function checkRequiredOccs($coupon_info,$order_id='',$cart_reference='')
	{	
		
		$req_info = PromoCodes::getCodeOccRequirements(0, $coupon_info[id]);
		
		//print_ar($req_info);
		
		if (!$req_info) return true;
			
		$session_id = session_id();
		
		if($order_id){
			$info = Orders::getItemsOccGrouped($order_id);
		}else{
			$cart = new Cart();
			$info = $cart->getCartOccGrouped(false,$cart_reference);
		}
		
			  
	  $max_applications = 0;
	  
	  $orig_req_info = $req_info;
	  
	  for (;;) {
	  
	  	//echo "foo";
	  	flush();
	  	
	  	$req_info = $orig_req_info;
	  	
		  foreach ($req_info as $index_i => $req) {
		  	
		  	$req_occ = $req['occasion_id'];
		  	$req_qty = $req['qty'];
		  	
		  	foreach ($info as $index_j => $i) {
		  		
		  		//print_ar($i);
		  		
		  		$cart_occ = $i['occasion_id'];
		  		
		  		$occ_match = $req_occ == $cart_occ;
		  		
		  		if ($occ_match) {
		  			
		  			if ($req_info[$index_i]['qty'] > 0) {
		  				
		  				for(; $info[$index_j]['sum'] > 0 && $req_info[$index_i]['qty'] > 0; $info[$index_j]['sum']--) {
		  					$req_info[$index_i]['qty']--;
		  				}
		  				
		  			}
		  			
		  		}
		  		
		  	}
		  	
		  	foreach ($req_info as $r) {
		  		if ($r['qty'] > 0) {
		  		  return $max_applications;
		  		}
		  	}
		  	
		  	$max_applications++;
		  	
		  }
	  
	  }	  
		
	}	
	
	
	function checkMinimumTotal($coupon_info,$order_id='',$cart_reference='')
	{
		if($order_id){
			
			if ($coupon_info['global_discount_amt'] && $coupon_info['global_discount_amt'] != "0.00")
			{
				$total = Orders::getPromoOrderTotal($order_id,"total", $coupon_info['code']);
				//var_dump($total);
			}
			else if ($coupon_info['shipping_discount_amt'] && $coupon_info['shipping_discount_amt'] != "0.00") 
			{
				$total = Orders::getPromoOrderTotal($order_id,"shipping", $coupon_info['code']);
			}
			else 
			{			
				$total = Orders::getPromoOrderTotal($order_id);
			}
		}else{			 
			// get cart total 		
			$cart = new Cart();
			
			if ($coupon_info['global_discount_amt'] && $coupon_info['global_discount_amt'] != "0.00")
			{
				$total = $cart->getCartTotal(true,true,"total", $coupon_info['code'],$cart_reference);
			}
			else if ($coupon_info['shipping_discount_amt'] && $coupon_info['shipping_discount_amt'] != "0.00") 
			{
				$total = $cart->getCartTotal(true,true,"shipping", $coupon_info['code'],$cart_reference);
					
			}
			else 
			{			
				$total = $cart->getCartTotal(true,true,'','',$cart_reference);				
			}
		}		
		$info['total'] = $total;

		if ($total) {
			if ($info['total'] >= $coupon_info['global_discount_min'])
			{

				return true;
			}  

			return false;
			
		}

		return false;
	
	}
	function checkCouponConflicts($promo_code_id,$all_codes){
		$this_coupon = PromoCodes::get1Code($promo_code_id);
		//print_ar($this_coupon);
		//print_ar($all_codes);
		$combos = PromoCodeCombine::getCombos( $promo_code_id );
		if( !$combos )
		    $combos = array();
		    
		if($all_codes){
			
			foreach($all_codes as $ac){
				
				//if it is this promo
				if($promo_code_id == $ac['promo_code_id']){
					continue; 
				}
				
				//if either one cannot be combined they cannot be combined
				if ($ac['promo_code_combo_type'] == 'N' || $this_coupon['combo_type'] == 'N'){
					return false;
				}
				
				//check if combos are both 'always allow'
				if($ac['promo_code_combo_type'] == 'A' && $this_coupon['combo_type'] == 'A'){
					return true;
				}
				
				//check if combos are allowed
				if( !in_array( $ac['promo_code_id'], $combos ) ) {
					return false;
				}
			}			
		}
		
//		if ($this_coupon['combo_type'] == 'A'){
//			
//		  return true;
//		}
//
//		$combos = PromoCodeCombine::getCombos( $promo_code_id );
//		if( !$combos )
//		    $combos = array();
//		print_ar($combos);
//		if ($all_codes) foreach ($all_codes as $ac) {
//
//			if( !in_array( $ac['promo_code_id'], $combos ) && $promo_code_id != $ac['promo_code_id'] ) {
//			    return false;
//			}
//
////			if($ac['promo_code_id'] == $promo_code_id){
////				return false;
////			}
//
//			// make sure it's not me!
//			if ($ac['promo_code_combo_type'] == 'N' && $promo_code_id != $ac['promo_code_id'])
//			  return false;
//
//		}
//		
		return true;
	}
		
	function getValidity($coupon_info,$order_id='',$cart_reference='')
	{
		//print_ar($coupon_info);
		$cart = new Cart();
	
		if ($coupon_info['essensa_only'] == "Y") $essensa_ok = PromoCodes::checkIfEssensa($coupon_info,$order_id,$cart_reference);
		else $essensa_ok = true; 
		
		if ($essensa_ok)
		{
		$started_ok = PromoCodes::checkStartDate($coupon_info,$order_id);
		
		$expires_ok = PromoCodes::checkExpiration($coupon_info,$order_id,$cart_reference);

		$uses_ok = PromoCodes::checkUses($coupon_info,$order_id);

		$occs_ok = PromoCodes::checkRequiredOccs($coupon_info,$order_id,$cart_reference);
		
		$brands_and_cats_ok = PromoCodes::checkRequiredCatsandBrands( $coupon_info,$order_id,$cart_reference );				

		$products_ok = PromoCodes::checkRequiredProducts($coupon_info,$order_id,$cart_reference);
		
		if($order_id){
			Orders::setUpProdWithCouponsInOrder($coupon_info,$order_id);
		} else {
			$cart->setUpProdWithCouponsInCart($coupon_info,$cart_reference);
		} 
		 
		$minimum_ok = PromoCodes::checkMinimumTotal($coupon_info, $order_id, $cart_reference);
		
		$gift_ok = PromoCodes::checkGiftStock( $coupon_info );
		
		if($order_id){
			$conflict_ok = Orders::checkOrderCouponConflicts($coupon_info['id'],$order_id);
		} else {
			$conflict_ok = $cart->checkCouponConflicts($coupon_info['id'],$cart_reference);
		}
  		
		if ($coupon_info['one_use_per_account'] == "Y" || $coupon_info['institution_type_restriction'] != "" ||
			$coupon_info['role_restriction'] != "" || $coupon_info['new_accounts_only'] == "Y" ||
			$coupon_info['essensa_only'] == "Y")
		{
			$logged_in_ok = PromoCodes::checkLoggedIn( $coupon_info ,$order_id, $cart_reference);		
			$account_activated_ok = PromoCodes::checkAccountActivated($coupon_info,$order_id,$cart_reference);
		}				
		else 
		{
			$logged_in_ok = true;
			$account_activated_ok = true;
		}
		
		
		if ($coupon_info['one_use_per_account'] == "Y") $one_use_ok = PromoCodes::checkIfAccountUsedAlready($coupon_info,$order_id,$cart_reference);
		else $one_use_ok = true;

		if ($coupon_info['new_accounts_only'] == "Y") $new_account_ok = PromoCodes::checkIfNewAccount($coupon_info,$order_id,$cart_reference);
		else $new_account_ok = true;				
		
		if ($coupon_info['institution_type_restriction'] != "") $institution_type_ok = PromoCodes::checkInstitutionType($coupon_info,$order_id,$cart_reference);
		else $institution_type_ok = true;
		
		if ($coupon_info['role_restriction'] != "") $role_ok = PromoCodes::checkRole($coupon_info,$order_id,$cart_reference);
		else $role_ok = true;		
		}
		
		if($order_id){
			Orders::revertOrderNoProdsQualify($order_id,$coupon_info['code']);
		} else {
			$cart->revertCartNoProdsQualify($cart_reference,$coupon_info['code']);
		}
		
		$ret = array ( 'result' => true, 'msg' => '' , 'auto_coupon' => $coupon_info['auto']);

		if (!$started_ok) {
			$ret['result'] = false;
			$ret['msg'] = "This promotion is not yet active.";
			return $ret;
		}
		if (!$expires_ok) {
			$ret['result'] = false;
			$ret['msg'] = "This promotion has expired.";
			return $ret;
		}

		if (!$uses_ok) {
			$ret['result'] = false;
			$ret['msg'] = "This promotion has ended.";
			return $ret;
		}

		if (!$conflict_ok) {
			$ret['result'] = false;
			$ret['auto_coupon'] = $coupon_info['auto'];
			$ret['msg'] = "This promotion cannot be combined with other promotions.";
			return $ret;
			}

		if (!$occs_ok) {
			$ret['result'] = false;
			$ret['msg'] = "This promotion does not apply to the products in these occasion categories.";
			return $ret;
		}

		if (!$products_ok) {
			$ret['result'] = false;
			if ($coupon_info['able_to_be_combined_with_sales'] == "Y") $ret['msg'] = "This promotion does not apply to these products.";
			else $ret['msg'] = "This promotion is only valid on specific, non-sale items.";
			return $ret;
		}

		if (!$minimum_ok) {
			$ret['result'] = false;
			if ($coupon_info['able_to_be_combined_with_sales'] == "Y") $ret['msg'] = "The minimum purchase amount was not satisfied for this promotion.";
			else $ret['msg'] = "The minimum purchase amount of non-sale items was not satisfied for this promotion.";
			return $ret;
			}

		if( !$brands_and_cats_ok )
		{
		    $ret['result'] = false;
		    if ($coupon_info['able_to_be_combined_with_sales'] == "Y") $ret['msg'] = 'This promo code does not apply to these brands or categories or requires more items of these brands or categories to be valid.';
			else $ret['msg'] = "This promotion may only be used on non-sale items from specific brands or categories or requires more items of these brands or categories to be valid.";		    
		}
		if( !$gift_ok )
		{
			$ret['result'] = false;
			$ret['msg'] = 'This promotion has expired.';
		}		
		if (!$logged_in_ok) {
			$ret['result'] = false;
			$ret['msg'] = "You must be logged into your account to use this coupon code.";
			return $ret;
		}
		if (!$new_account_ok)
		{
			$ret['result'] = false;
			$ret['msg'] = "You must have an updated account in order to use this coupon.";
			return $ret;
		}		
		if (!$essensa_ok)
		{
			$ret['result'] = false;
			$ret['msg'] = "You must be a member of Essensa in order to use this coupon.";
			return $ret;
		}
		if (!$account_activated_ok) {
			$ret['result'] = false;
			$ret['msg'] = "You must first activate your account before you can use this coupon code.  Upon creating your account, you should have received an email containing a link to activate your account.";
			return $ret;
		}				
		if (!$one_use_ok) {
			$ret['result'] = false;
			$ret['msg'] = "You may only use this coupon code once.";
			return $ret;
		}
		
		$vowels = array('A', 'E', 'I', 'O', 'U');
		
		// if the coupon can only be used by a specific role and/or institution type, and the user's account
		// has a different role and/or institution type, show appropriate error message
		if (($coupon_info['institution_type_restriction'] && $coupon_info['role_restriction']) && (!$institution_type_ok || !$role_ok)) {
			$ret['result'] = false;
			
			// figure out if the insitution type and role start with vowels, and if one or both does, use the 
			// appropriate articles
			$institution_type_caption = MyAccount::getInstitutionTypeCaptions($coupon_info['institution_type_restriction']);
			if (in_array(strtoupper(substr($institution_type_caption, 0, 1)), $vowels)) $institution_type_article = "an";
			else $institution_type_article = "a";
			$role_caption_sing = MyAccount::getRoleCaptions($coupon_info['role_restriction']);
			$role_caption_plural = MyAccount::getRoleCaptions($coupon_info['role_restriction'], true);
			if (in_array(strtoupper(substr($role_caption_sing, 0, 1)), $vowels)) $role_article = "an";
			else $role_article = "a";
			
			$ret['msg'] = "The coupon code that you entered is only valid for ". $institution_type_caption . 
						" " . $role_caption_plural . ".  If you are currently $institution_type_article ". 
						$institution_type_caption . " " . $role_caption_sing . 
						", please make sure your \"Type of Company or Institution\" and \"Role\" in the account profile are accurately completed.";
			return $ret;
		}
		// if the coupon can only be used by a specific institution type, and the user's account
		// has a different institution type, show appropriate error message
		else if (!$institution_type_ok) {
			$ret['result'] = false;
			
			// figure out if the insitution type starts with a vowel, and if it does, use the 
			// appropriate article
			$institution_type_caption = MyAccount::getInstitutionTypeCaptions($coupon_info['institution_type_restriction']);
			if (in_array(strtoupper(substr($institution_type_caption, 0, 1)), $vowels)) $institution_type_article = "an";
			else $institution_type_article = "a";
						
			$ret['msg'] = "The coupon code that you entered is only valid for customers associated with $institution_type_article $institution_type_caption.  If you are currently associated with $institution_type_article $institution_type_caption, please make sure your \"Type of Company or Institution\" in the account profile is accurately completed.";
			return $ret;
		}		
		// if the coupon can only be used by a specific role, and the user's account
		// has a different role, show appropriate error message		
		else if (!$role_ok) {
			$ret['result'] = false;
			
			// figure out if the role starts with a vowel, and if it does, use the 
			// appropriate article
			$role_caption_sing = MyAccount::getRoleCaptions($coupon_info['role_restriction']);
			$role_caption_plural = MyAccount::getRoleCaptions($coupon_info['role_restriction'], true);
			if (in_array(strtoupper(substr($role_caption_sing, 0, 1)), $vowels)) $role_article = "an";
			else $role_article = "a";	
					
			$ret['msg'] = "The coupon code that you entered is only valid for ". $role_caption_plural.".  If you are currently $role_article $role_caption_sing, please make sure your \"Role\" in the account profile is accurately completed.";
			return $ret;
		}
										
		return $ret;
		
	}
		
	function getDiscountOnTotal($coupon_info, $already_discounted_totals_array, $already_discounted_shipping, $order_id='',$cart_reference='')
	{  	
		//print_ar($coupon_info);
		if($order_id){
			$items = Orders::getItems('',$order_id,'','','','','','','','','','','','','','','',"qualifying","total",$coupon_info['code']);
		}else{
			$cart = new Cart();
			$items = $cart->get('', '', '', "qualifying","total",$coupon_info['code'],$cart_reference);
		}
		
		//print_ar($items);
		
		//$already_discounted_total -= $already_discounted_shipping;
		
		if ($coupon_info['global_discount_type'] == 'dollars') {
			
			//$items = $cart->get('', '', '', "qualifying","total",$coupon_info['code']);
			if($items){
				$num_items = count($items);
				$total_divided = $coupon_info['global_discount_amt'] / $num_items;
			
				foreach($items as $item){
					$this_product_id = $item['product_id'];
					$already_discounted_totals_array[$this_product_id] += $total_divided;
				}
			}
			$return_array = array();
			$return_array['discount_amount'] = $coupon_info['global_discount_amt'];
			$return_array['already_discounted_totals_array'] = $already_discounted_totals_array;
			return $return_array;						
		}
		else {
			switch ($coupon_info['app_type'])
			{
				case 'both':
				case 'products':
				  $_products = true;
				  break;  			
			}

			$total = 0.00;
			//$items = $cart->get('', '', '', "qualifying","total",$coupon_info['code']);
			
			//merge the totals if there are duplicate product_id lines
			foreach ($items as $key=>$item){
				foreach ($items as $inner_key=>$inner_item){
					if($items[$key]){
						if($item['product_id'] == $inner_item['product_id'] && $key != $inner_key){
							if($item['line_total']){
								$items[$key]['line_total'] += $inner_item['line_total'];
							}else{
								$items[$key]['item_total'] += $inner_item['item_total'];
							}
							unset ($items[$inner_key]);
							continue;
						}
					}
				}
			}
			
			//print_ar($items);
			if($items){
				foreach($items as $item){
					$this_line_total = $item['line_total'] ? $item['line_total'] : $item['item_total'];
					$this_product_id = $item['product_id'];
					$this_line_total -= $already_discounted_totals_array[$this_product_id];
					$total += $this_line_total;
					$discount_amount_this_item = $coupon_info['global_discount_amt'] * $this_line_total * .01;
					$already_discounted_totals_array[$this_product_id] += $discount_amount_this_item;
				}
			}

			//$total = $total['total'];

			//$total -= $already_discounted_total;
		//print_ar($items);

			$discount_amount = $coupon_info['global_discount_amt'] * $total * .01;
			//var_dump($discount_amount);
			if ($coupon_info['global_is_max'] == 'Y' && $discount_amount > $coupon_info['global_discount_max'])
			    $discount_amount = $coupon_info['global_discount_max'];

			//mail("alex@rustybrick.com","TEST", $total['total']." -- ".$already_discounted_total." -- ".$discount_amount." -- ".$coupon_info['global_discount_amt']." -- ".$coupon_info['global_is_max']." -- ".$coupon_info['global_discount_max']); 
			$return_array = array();
			$return_array['discount_amount'] = $discount_amount;
			$return_array['already_discounted_totals_array'] = $already_discounted_totals_array;
			//print_ar($return_array);
			
			return $return_array;
		}
	}
	
	function getDiscountOnShipping($coupon_info, $shipping_amt, $oder_id='')
	{  	
		
		if($oder_id){
			$order_info = Orders::get1($oder_id);
			if($order_info['shipping_method'] !="" && $order_info['shipping_method'] !='Ground'){
				return 0;
			}
		}else{
			$cart = new Cart();
			
			if($cart->data['shipping_method'] != "" && $cart->data['shipping_method'] != 'GND')
			{
				return 0;
			}
		}
		if ($coupon_info['shipping_discount_type'] == 'dollars') {
			return $coupon_info['shipping_discount_amt'];
		}
		else {
			$total = $shipping_amt;
			return $coupon_info['shipping_discount_amt'] * $total * .01;
		}
	}
	
	
	function getDiscountOnItems($coupon_info, $order_id='')
	{
		$max_applications = PromoCodes::checkRequiredProducts($coupon_info,$order_id);
				
		$info = PromoCodes::getCodeProductDiscount(0,$coupon_info['id']);
		
		if($order_id){
			$items = Orders::getItemsGrouped($order_id,true);
		}else{
			$cart = new Cart();
			$items = $cart->getCartGrouped(true);
		}
				//print_ar( $items );
		if ($info) foreach ($info as $i) {
		
			if ($items[$i[product_id]]) {
				
				$reqs = db_get1(PromoCodes::getCodeRequirements(0, $i['promo_code_id'], $i['product_id']));
				
				
				if ($i[is_max_applications] == 'Y')
				  $applications = $max_applications < $i['max_applications'] ? $max_applications : $i['max_applications'];
				else
				  $applications = $max_applications;
				  
				  
				if ($i[product_discount_type] == 'dollars') {
					$discount = $applications * $i['product_discount_amount'];
				} else if ($i[product_discount_type] == 'number') {
					$discount = $applications * $i['product_discount_amount'] * $i['product_price'];
				} else {
					$discount = ($i['product_discount_amount'] * .01) * ($applications * $i['product_price'] * $reqs['qty']);
				}
				//echo ($i['product_discount_amount'] * .01);
                                //echo $i['product_price'];
                                //echo $applications * $i['product_price'] * $reqs['qty'];
				$items[$i[product_id]]['discount'] = $discount;
				
				if(!$order_id){
					if( !isset( $_SESSION['cart_prices'][ $i['product_id'] ] ) ) {
					    //$_SESSION['cart_prices'][$items[$i['product_id']]] = $i['product_price'];
					    $_SESSION['cart_prices'][ $i['product_id']] = ($items[$i[product_id]]['sum'] * $i['product_price']) - $discount;
					    $_SESSION['cart_prices']['promos'][] = $i['promo_code_id'];
					    
					} else {
					    //echo"SHOULD THIS BE DONE AGAIN?";
					    if( !in_array( $i['promo_code_id'], $_SESSION['cart_prices']['promos']) ){
						$_SESSION['cart_prices'][ $i['product_id']] = $_SESSION['cart_prices'][ $i['product_id']] - $discount;
						$_SESSION['cart_prices']['promos'][] = $i['promo_code_id'];
					    }
					}
				}
			}
			
		}
		return $items;
	}

	function getDiscountOnCats($coupon_info,$order_id='')
	{
		//print_ar($coupon_info);
		
		$max_applications = PromoCodes::checkRequiredCats($coupon_info);
				
		$info = PromoCodes::getCodeCatDiscount(0,$coupon_info['id']);
		
		if($order_id){
			$items = Orders::getItemsCatGrouped($order_id,true);
		}else{
			$cart = new Cart();
			$items = $cart->getCartCatGrouped(true);
		}
		//print_ar($items);
							
		if ($info) foreach ($info as $i) {
			//$cats = Products::getProductCatTree( $product_id );

				if ($items[$i[cat_id]]) {	
					
					$reqs = db_get1(PromoCodes::getCodeCatRequirements(0, $i['promo_code_id'], $i['cat_id']));		
					
					if ($i[is_max_applications] == 'Y')
					  $applications = $max_applications < $i['max_applications'] ? $max_applications : $i['max_applications'];
					else
					  $applications = $max_applications;
					  				  
					if ($i[cat_discount_type] == 'dollars') {
						$discount = $applications * $i['cat_discount_amount'];
					} else if ($i[cat_discount_type] == 'number') {
						$discount = $applications * $i['cat_discount_amount'] * $i['product_price'];
					} else {
						
						$discountable_total = 0;
						
						foreach ($items as $item) {
							
							foreach ($info as $i) {
					            
								if ($coupon_info['req_cat_type']=='Include'){

								   if (Cats::isCatChildOfCat($item['cat_id'], $i['cat_id'])) 
								     $discountable_total += $item[sum_price];
								} 
								
								if ($coupon_info['req_cat_type']=='Exclude'){
									$req_cats=getCodeCatRequirements(0, $coupon_info['id']);
									$cat_ids=array();
									foreach ($req_cats as $cat){
										$cat_ids[]=$cat['cat_id'];
									}
								   if (Cats::isCatChildOfCat($item['cat_id'], $i['cat_id']) && (!in_array($item['cat_id'],$cat_ids))) 
								     $discountable_total += $item[sum_price];
								} 
							
							}
							  
						}
						
						$discount = ($i['cat_discount_amount'] * .01) * $discountable_total;
						
					}
					
					$items[$i[cat_id]]['discount'] = $discount;
					
				}
			
			
			
		}
		return $items;
	}
	
	function getCatQualifyingProducts($coupon_info,$order_id='',$cart_reference='')
	{
			$qualifying_prods = array();
			
			if($order_id){
				$items = Orders::getItemsGroupedByOptions($order_id);
			} else {
				$cart = new Cart();
				$items = $cart->getCartGroupedByOptions(false,$cart_reference);
			}	
			
			if ($items)
			{
				foreach($items as $thisItem)
				{
					$product_cats = Cats::getCatsForProduct( $thisItem['product_id'], 'id' );
					$all_product_cats = $product_cats;
                    foreach ($product_cats as $thePC)
                    {
                        $symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);

                        if (count($symbolic_product_cats) > 0)
                        {
                            foreach($symbolic_product_cats as $spc)
                            {
                                $all_product_cats[$spc['id']] = $spc;
                            }
                        }

                    }
                    $product_cats = $all_product_cats;
                    
					foreach($product_cats as $thisCat)
					{
						//$reqs = db_get1(PromoCodes::getCodeCatRequirements(0, $coupon_info['id'], $thisCat['id']));
						$all_reqs = PromoCodes::getCodeCatRequirements(0, $coupon_info['id'], $thisCat['id']);

						if ($coupon_info['req_cat_type']=='Include'){
						  foreach($all_reqs as $reqs)
						  {

							 if ($reqs['cat_id'] == $thisCat['id']) 
							  {

								// if the coupon cannot be combined with sales, and this product has an active sale
								// don't mark that product as qualifying--only mark as qualifying if can be 
								// combined with sales or there is no active, not-sold-out sale
								if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
								if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
								{
									$qualifying_prods[] = $thisItem['the_cart_id'];
									break;
								}
							  }
							  
							
						  }
						}  
						else {                    //exclude the reqired categories
							  $cat_ids=array();
							  foreach ($all_reqs as $cat)
									$cat_ids[]=$cat['cat_id'];
							  if (in_array ($thisCat['id'], $cat_ids)) 								  
							  {
								break;
							  }
							  else {	
								// if the coupon cannot be combined with sales, and this product has an active sale
								// don't mark that product as qualifying--only mark as qualifying if can be 
								// combined with sales or there is no active, not-sold-out sale
								if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
								if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
								{
								  $qualifying_prods[] = $thisItem['the_cart_id'];
								}
							  }
							  
					     		
						                  

						}
					}		
				}
			}
	//		print_r($qualifying_prods);
	//		echo "Done the cats<br><br>";
			return $qualifying_prods;	
	
	}
	function getItemQualifyingProducts($coupon_info,$order_id='',$cart_reference='')
	{
		$qualifying_prods = array();
		if($order_id){
			$items = Orders::getItemsGroupedByOptions($order_id);
		} else {
			$cart = new Cart();
			$items = $cart->getCartGroupedByOptions(false, $cart_reference);
		}	
		
		$found_prod = true;		

		if ($items)
		{
			foreach($items as $thisItem)
			{
				if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
				$req_info = PromoCodes::getCodeRequirements(0, $coupon_info[id]);
				if ($req_info)
				{
					foreach($req_info as $reqs)
					{			
						if ($reqs['product_id'] == $thisItem['product_id']) 
						{
							// if the coupon cannot be combined with sales, and this product has an active sale
							// don't mark that product as qualifying--only mark as qualifying if can be 
							// combined with sales or there is no active, not-sold-out sale							
							if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
							{
								$qualifying_prods[] = $thisItem['the_cart_id'];
								break;
							}
						}
					}
				}				
			}
		}
		//print_r($qualifying_prods);
		//echo "done getShippingDiscountOnProds<br><br>";
		return $qualifying_prods;	
	
	}
	function getTotalQualifyingProducts($coupon_info,$order_id='',$cart_reference='')
	{
		$qualifying_prods = $nonqualifying_prods = array();
		if($order_id){
			$items = Orders::getItemsGroupedByOptions($order_id);
		} else {
			$cart = new Cart();
			$items = $cart->getCartGroupedByOptions(false,$cart_reference);
		}	
		
		$found_prod = true;		

		if ($items)
		{
			foreach($items as $thisItem)
			{
				if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
				// if the coupon cannot be combined with sales, and this product has an active sale
				// don't mark that product as qualifying--only mark as qualifying if can be 
				// combined with sales or there is no active, not-sold-out sale							
				if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
				{
					$qualifying_prods[] = $thisItem['the_cart_id'];					
				}
				else $nonqualifying_prods[] = $thisItem['the_cart_id'];
			}
		}
		
				
		//print_r($qualifying_prods);
		//echo "done getShippingDiscountOnProds<br><br>";
		$return_array = array(0=>$qualifying_prods, 1=>$nonqualifying_prods);
		return $return_array;	
	
	}				

	////// not done
	function getDiscountOnOccs($coupon_info,$order_id)
	{
		
		$max_applications = PromoCodes::checkRequiredOccs($coupon_info);
				
		$info = PromoCodes::getCodeOccDiscount(0,$coupon_info['id']);
				
		if($order_id){
			$items = Orders::getItemsOccGrouped($order_id,true);
		} else {
			$cart = new Cart();
			$items = $cart->getCartOccGrouped(true);
		}
							
		if ($info) foreach ($info as $i) {
	
			if ($items[$i[occasion_id]]) {	
				
				$reqs = db_get1(PromoCodes::getCodeCatRequirements(0, $i['promo_code_id'], $i['occasion_id']));		
				
				if ($i[is_max_applications] == 'Y')
				  $applications = $max_applications < $i['max_applications'] ? $max_applications : $i['max_applications'];
				else
				  $applications = $max_applications;
				  				  
				if ($i[occasion_discount_type] == 'dollars') {
					
					$discount = $applications * $i['occasion_discount_amount'];
				} else if ($i[cat_discount_type] == 'number') {
					$discount = $applications * $i['occasion_discount_amount'] * $i['product_price'];
				} else {
					
					$discountable_total = 0;
					
					foreach ($items as $item) {
						
						foreach ($info as $i) {
						
							if ($item['occasion_id'] == $i['occasion_id'])
							  $discountable_total += $item[sum_price];
						
						}
						  
					}
					
					$discount = ($i['occasion_discount_amount'] * .01) * $discountable_total;
					
				}
				
				$items[$i[occasion_id]]['discount'] = $discount;
				
			}
			
		}
		
		//print_ar($items);
		
		return $items;
	}
	
	
	/* BRAND FUNCTIONS */
		function insertCodeBrandRequirement($info) 
	{
		 $promo_info=self::get1Code($info['promo_code_id']);
		 
		 if ($promo_info['req_brand_type']=='Exclude'){                                //if setting is to exclude update all other req_brands with the new qty
           $other_req_brands=self::getCodeBrandRequirements("",$promo_info['id']);	   
		   if ($other_req_brands && $other_req_brands[0]['qty']!=$info['qty']){
		      $other_req_brands[0]['qty']=$info['qty'];
			  unset ($other_req_brands[0]['brand_name']);
		      self::updateCodeBrandRequirement($other_req_brands[0]['id'],$other_req_brands[0]);
		   }
	   }
	    $alreadythere=self::getCodeBrandRequirements("", $info['promo_code_id'], $info['brand_id']);
		if (!$alreadythere)
		   return db_insert('promo_code_require_brands', $info);
	    else self::updateCodeBrandRequirement($alreadythere[0]['id'],$info);
	   
	}
	
    function updateCodeBrandRequirement($id, $info)
	{
		$promo_info=self::get1Code($info['promo_code_id']);
	    if ($promo_info['req_brand_type']=='Exclude'){                                //if setting is to exclude update all other req_brand with the latest qty
              $other_req_brands=self::getCodeBrandRequirements("",$promo_info['id']);
			  foreach ($other_req_brands as $req_brand){
						  $req_brand['qty']=$info['qty'];
				  unset ($req_brand['brand_name']);
				  db_update('promo_code_require_brands', $req_brand['id'], $req_brand);
			  }               
        }	
		
		return db_update('promo_code_require_brands', $id, $info);
	}
	
	function deleteCodeBrandRequirement($id) 
	{
		return db_delete('promo_code_require_brands', $id);
	}
	
	function getCodeBrandRequirements($id = 0, $promo_code_id = 0, $brand_id = 0)
	{
	  $q = " SELECT promo_code_require_brands.*, brands.name as brand_name  FROM promo_code_require_brands LEFT JOIN brands ON (brands.id = promo_code_require_brands.brand_id) WHERE 1 ";
	  
	  if ($id) {
	  	$q .= db_restrict('promo_code_require_brands.id', $id);
	  }
	  
	  if ($promo_code_id) {
	  	$q .= db_restrict('promo_code_id', $promo_code_id);
	  }
	  
	  if ($brand_id) {
	  	$q .= db_restrict('brand_id', $brand_id);
	  }
	  
	  return db_query_array($q);
	}
	
	function getCodeBrandRequirementsToCalc($id = 0, $promo_code_id = 0, $brand_id = 0, $promo_info="")
	{ 
	    $req_brands=PromoCodes::getCodeBrandRequirements($id, $promo_code_id, $brand_id);
        if ($promo_info['req_brand_type']=='Include')
			 return $req_brands;
		/*exclude required brands from discount*/
		$brand_ids=array(); 
		foreach ($req_brands as $brand)
		         $brand_ids[]= $brand['brand_id'];
		$qty=$req_brands[0]['qty'];
		$brands=implode(",",$brand_ids);
   	    
		$q = " SELECT * from brands WHERE 1 ";
	    if ($brands) {
	  	$q .= " AND brands.id NOT IN ($brands)";
	    }
	
        $temp_brands= db_query_array($q);
		$sale_brands=array();
	    foreach ($temp_brands as $t){
			$temp['brand_id']=$t['id'];
			$temp['promo_code_id']=$promo_code_id;
			$temp['brand_name']=$t['name'];
			$temp['id']=$t['id'];
			$temp['qty']=$qty;
			$sale_brands[]=$temp;
		}
	    return $sale_brands;
	}
	
	
	function insertCodeBrandDiscount($info)
	{
		return db_insert('promo_code_brand_discounts', $info);
	}
	
	function updateCodeBrandDiscount($id, $info)
	{
		return db_update('promo_code_brand_discounts', $id, $info);
	}
	
	function deleteCodeBrandDiscount($id)
	{
		return db_delete('promo_code_brand_discounts', $id);
	}
	
	function getCodeBrandDiscount($id = 0, $promo_code_id = 0, $brand_id = 0)
	{
		$q = " SELECT promo_code_brand_discounts.*, brands.name as brand_name FROM promo_code_brand_discounts LEFT JOIN brands on (brands.id = promo_code_brand_discounts.brand_id) WHERE 1 ";
		
		if ($id) {
			$q .= db_restrict('promo_code_brand_discounts.id', $id);
		}
		
		if ($promo_code_id) {
			$q .= db_restrict('promo_code_id', $promo_code_id);
		}
		
		if ($brand_id) {
			$q .= db_restrict('brand_id', $brand_id);
		}
		
		return db_query_array($q);
		
	}
	
	function checkIfBrandAndCatRequirments($promo_code_id = 0)
	{
		$q = " SELECT * FROM promo_codes,promo_code_require_brands,promo_code_require_cats WHERE promo_codes.id=$promo_code_id AND promo_code_require_brands.promo_code_id=$promo_code_id AND promo_code_require_cats.promo_code_id=$promo_code_id";
		$return = db_query_array($q);
	
		return $return ? true : false;
	}
	
	function getBrandandCatQualifyingProducts($coupon_info,$order_id='',$cart_reference='')
	{
		$qualifying_prods = array();
	
		if($order_id){
			$items = Orders::getItemsGroupedByOptions($order_id);
		} else {
			$cart = new Cart();
			$items = $cart->getCartGroupedByOptions(false,$cart_reference);
		}
	
		if ($items)
		{
        foreach($items as $thisItem)
			{
				$qualifies_brand = false;
				$qualifies_cat = false;
				

				$has_brand_and_cat_requirments = PromoCodes::checkIfBrandAndCatRequirments($coupon_info['id']);


				//Foreach item we are going to check if it qualifies for brand promos and for cat promos. 
				//If it qualifies for both, will be added to qualifing_promos
				

				//Check if qualifies for brand promos
				$all_brand_reqs = PromoCodes::getCodeBrandRequirementsToCalc(0, $coupon_info['id'], $items['brand_id'],$coupon_info);
				foreach($all_brand_reqs as $reqs)
				{
// 					echo "<br>" . $reqs['brand_id'] . " " .$thisItem['brand_id'] . "<br>";
					if ($reqs['brand_id'] == $thisItem['brand_id']) 
					{
						// if the coupon cannot be combined with sales, and this product has an active sale
						// don't mark that product as qualifying--only mark as qualifying if can be 
						// combined with sales or there is no active, not-sold-out sale
						if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
						if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
						{
							$qualifies_brand = true;
							break;
						} 

					}
				}
				

				//now check for cat requirments
				$product_cats = Cats::getCatsForProduct( $thisItem['product_id'], 'id' );
				$all_product_cats = $product_cats;
				

				foreach ($product_cats as $thePC)
				{
					//echo $thePC['name'];
					$symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);
						

					if (count($symbolic_product_cats) > 0)
					{
						foreach($symbolic_product_cats as $spc)
						{
							$all_product_cats[$spc['id']] = $spc;
						}
					}
				}
				$product_cats = $all_product_cats;
				

				//print_ar($product_cats);
				foreach($product_cats as $thisCat)
				{
					$all_cat_reqs = PromoCodes::getCodeCatRequirements(0, $coupon_info['id'], $thisCat['id']);
					//print_ar($all_cat_reqs);
					
					if ($coupon_info['req_cat_type']=='Include'){
					  foreach($all_cat_reqs as $reqs)
					  {

						if ($reqs['cat_id'] == $thisCat['id'])
						{
							$qualifies_cat = $thisCat['name'];
							break;

						}
					  }
					}  
					
					else {                                 //Exclude these categories from promotion
						 $cat_ids=array();
						 
						 foreach($all_cat_reqs as $reqs)
						     $cat_ids[]=$reqs['cat_id'];
						 
						 if (in_array ($thisCat['id'], $cat_ids)){
							 
							 $qualifies_cat = false;
							 break;
						 }
                         else  {
							 $qualifies_cat = $thisCat['name'];
						 }						 
						
						 
					}
				}


// 				echo $thisItem['product_id'] . '- brand:' . var_export($qualifies_brand,true) . ' cat: ' . var_export($qualifies_cat,true) . "<br>";
				

				if($has_brand_and_cat_requirments){
					//if we have both brand and cat requirments - check if it qualifies for both
					if($qualifies_brand && $qualifies_cat){
						$qualifying_prods[] = $thisItem['the_cart_id'];
					}
				}else{
					if($qualifies_brand || $qualifies_cat){
						//if there aren't both cat and brand requirments - check if it qualifies for either brand or cat
						$qualifying_prods[] = $thisItem['the_cart_id'];
					}
				}
			}
		}
// 		print_ar($qualifying_prods);
		//echo "done getShippingDiscountOnBrandsandCats<br><br>";
		return $qualifying_prods;
	
	}	
	
	function getBrandQualifyingProducts($coupon_info,$order_id)
	{
		$qualifying_prods = array();
		
		if($order_id){
			$items = Orders::getItemsGroupedByOptions($order_id);
		} else {
			$cart = new Cart();
			$items = $cart->getCartGroupedByOptions();
		}

		if ($items)
		{
			foreach($items as $thisItem)
			{
				$all_reqs = PromoCodes::getCodeBrandRequirementsToCalc(0, $coupon_info['id'], $items['brand_id'],$coupon_info);
				foreach($all_reqs as $reqs)
				{
					if ($reqs['brand_id'] == $thisItem['brand_id']) 
					{
						// if the coupon cannot be combined with sales, and this product has an active sale
						// don't mark that product as qualifying--only mark as qualifying if can be 
						// combined with sales or there is no active, not-sold-out sale
						if (!$thisItem['product_option_id']) $thisItem['product_option_id'] = 0;
						if ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($thisItem['product_id'], $thisItem['product_option_id']))
						{
							$qualifying_prods[] = $thisItem['the_cart_id'];
							break;
						} 
						
					}
				}
			}
		}		
		//print_r($qualifying_prods);
		//echo "done getShippingDiscountOnBrands<br><br>";
		return $qualifying_prods;	
	
	}
		
	function getDiscountOnBrands($coupon_info,$order_id='')
	{	
		$max_applications = PromoCodes::checkRequiredBrands($coupon_info);
				
		$info = PromoCodes::getCodeBrandDiscount(0,$coupon_info['id']);
		
		if($order_id){
			$items = Orders::getItemsBrandGrouped($order_id,true);
		} else {
			$cart = new Cart();
			$items = $cart->getCartBrandGrouped(true);
		}
		
		if ($info) foreach ($info as $i) {
			//$cats = Products::getProductCatTree( $product_id );

				if ($items[$i[brand_id]]) {
					
					$reqs = db_get1(PromoCodes::getCodeBrandRequirementsToCalc(0, $i['promo_code_id'], $i['brand_id']), $i);
					
					if ($i[is_max_applications] == 'Y')
					  $applications = $max_applications < $i['max_applications'] ? $max_applications : $i['max_applications'];
					else
					  $applications = $max_applications;
					  
			 				  
					if ($i[brand_discount_type] == 'dollars') {
						$discount = $applications * $i['brand_discount_amount'];
					} else if ($i[brand_discount_type] == 'number') {
						$discount = $applications * $i['brand_discount_amount'] * $i['product_price'];
					} else { // percentage
						
						$discountable_total = 0;
						foreach ($items as $item) {
							
							foreach ($info as $i) {
							    if( $item['brand_id'] == $i['brand_id'] )
								  $discountable_total += $item[sum_price];
							}
							  
						}
						$discount = ($i['brand_discount_amount'] * .01) * $discountable_total;
						
					}
					
					$items[$i[brand_id]]['discount'] = $discount;
					
				}
			
			
			
		}		
		return $items;
	}
	
	function checkRequiredBrands($coupon_info,$order_id='',$cart_reference='')
	{	
		
		$req_info = PromoCodes::getCodeBrandRequirementsToCalc(0, $coupon_info[id],"",$coupon_info);

		if (!$req_info) return true;
			
		$session_id = session_id();
		if($order_id){
			$info = Orders::getItemsGroupedByOptions($order_id);
		}else{
			$cart = new Cart();
			$info = $cart->getCartGroupedByOptions(false,$cart_reference);
		}
		
	  $orig_req_info = $req_info;
	  
	  $max_applications = 0;
	  
      $req_brand_array=array();
		 foreach ( $req_brand_info as $c){
		         $req_brand_array[]=$c['brand_id'];
		     }
	  $req_brand_sum=$req_brand_info[0]['qty'];
	  $qty_total=0;
	  for (;;) {
	  
	  	$req_info = $orig_req_info;
	  	$brand_match = false;
		  foreach ($req_info as $index_i => $req) {
		  	
		  	$req_brand = $req['brand_id'];
		  	$req_qty = $req['qty'];
			if( $info )
		  	foreach ($info as $index_j => $i) {
		  		$cart_brand = $i['brand_id'];
				
				if (in_array($cart_brand, $req_brand_info)) $qty_total += $i['sum'];
				
				if (!$i['product_option_id']) $i['product_option_id'] = 0;
					
		  		if ($cart_brand == $req_brand  && $i['sum'] >= $req['qty'] && ($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($i['product_id'], $i['product_option_id']))) {
		  			$brand_match = true;
		  			if ($req_info[$index_i]['qty'] > 0) {
		  				
		  				for(; $info[$index_j]['sum'] > 0 && $req_info[$index_i]['qty'] > 0; $info[$index_j]['sum']--) {
		  					$req_info[$index_i]['qty']--;
		  				}
		  				
		  			}
		  			
		  		}

		  	}
		  	
			/*foreach ($req_info as $r) {
			    if ($r['qty'] > 0) {
			      return $max_applications;
			    }
			}

			
		  	$max_applications++;*/
		  	
		  //}
		  
		 //return $max_applications;
		 if ($coupon_info['req_brand_type']=='Exclude' && $qty_total < $req_brand_sum) $brand_match=false;
	  }
	  return $brand_match;
		
	}	
	}	
	
	/* END BRAND FUNCTIONS */

	/**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT promo_codes.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(promo_codes.id)) as total ";
		}

		$from_ = " FROM promo_codes ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND promo_codes.id = [id] ";
		}

		if( isset( $o->code )  ){
			$where_ .= " AND `promo_codes`.code = [code]";
		}
		if( isset( $o->description )  ){
			$where_ .= " AND `promo_codes`.description = [description]";
		}
		if( isset( $o->expires )  ){
			$where_ .= " AND `promo_codes`.expires = [expires]";
		}
		if(is_array($o->expire_date)){
			if($o->expire_date['start']){
				$o->expire_date_start = $o->expire_date['start'];
				$where_ .= " AND `promo_codes`.expire_date >= [expire_date_start]";
			}
			if($o->expire_date['end']){
				$o->expire_date_end = $o->expire_date['end'];
				$where_ .= " AND `promo_codes`.expire_date <= [expire_date_end]";
			}
		} else 		if( isset( $o->expire_date )  ){
			$where_ .= " AND `promo_codes`.expire_date = [expire_date]";
		}
		if( isset( $o->uses_limited )  ){
			$where_ .= " AND `promo_codes`.uses_limited = [uses_limited]";
		}
		if( isset( $o->uses )  ){
			$where_ .= " AND `promo_codes`.uses = [uses]";
		}
		if( isset( $o->uses_so_far )  ){
			$where_ .= " AND `promo_codes`.uses_so_far = [uses_so_far]";
		}
		if( isset( $o->shipping_discount_type )  ){
			$where_ .= " AND `promo_codes`.shipping_discount_type = [shipping_discount_type]";
		}
		if( isset( $o->shipping_discount_amt )  ){
			$where_ .= " AND `promo_codes`.shipping_discount_amt = [shipping_discount_amt]";
		}
		if( isset( $o->global_is_max )  ){
			$where_ .= " AND `promo_codes`.global_is_max = [global_is_max]";
		}
		if( isset( $o->global_discount_type )  ){
			$where_ .= " AND `promo_codes`.global_discount_type = [global_discount_type]";
		}
		if( isset( $o->global_discount_amt )  ){
			$where_ .= " AND `promo_codes`.global_discount_amt = [global_discount_amt]";
		}
		if( isset( $o->global_discount_min )  ){
			$where_ .= " AND `promo_codes`.global_discount_min = [global_discount_min]";
		}
		if( isset( $o->global_discount_max )  ){
			$where_ .= " AND `promo_codes`.global_discount_max = [global_discount_max]";
		}
		if( isset( $o->combo_type )  ){
			$where_ .= " AND `promo_codes`.combo_type = [combo_type]";
		}
		if( isset( $o->app_type )  ){
			$where_ .= " AND `promo_codes`.app_type = [app_type]";
		}
		if( isset( $o->auto )  ){
			$where_ .= " AND `promo_codes`.auto = [auto]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY promo_codes.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function getColumns(){
		$sql = "SHOW COLUMNS FROM `promo_codes`";

		$fields = db_query_array($sql);

		$ret = array();
		foreach($fields as $field){
			$ret[] = $field['Field'];
		}
		return $ret;
	}

	static function getProductAuto( $id )
	{
	    global $CFG;

	    $sql = " SELECT promo_code_id, promo_codes.combo_type as promo_code_combo_type,
			promo_codes.code as promo_code_code,
			promo_codes.description as promo_code_description ";
	    $sql .= " FROM promo_codes ";
	    $sql .= " LEFT JOIN promo_code_require_products ON promo_code_require_products.promo_code_id = promo_codes.id ";
	    $sql .= " WHERE 1 ";

	    $sql .= " AND promo_codes.auto = 'Y' AND (expires = 'N' OR expire_date > NOW())";
	    $sql .= " AND promo_code_require_products.product_id = " . (int) $id;
            $sql .= " ORDER BY qty desc";

	    $ret = db_query_array( $sql );
	    
	    if($ret){
		return $ret;
	    }else{
		return array();
	    }
	}
	
	static function getCatAuto($item_cats){
		
		$cat_ids = "";
		if(is_array($item_cats)) foreach ($item_cats as $cat) $cat_ids .= $cat['id'] . ",";
		$cat_ids = substr($cat_ids, 0, -1);
	
		$sql = " SELECT promo_code_id, promo_codes.combo_type as promo_code_combo_type,
			promo_codes.code as promo_code_code,
			promo_codes.description as promo_code_description ";
		$sql .= " FROM promo_codes ";
		$sql .= " LEFT JOIN promo_code_require_cats ON promo_code_require_cats.promo_code_id = promo_codes.id ";
		$sql .= " WHERE 1 ";
	   
		$sql .= " AND promo_codes.auto = 'Y' AND (expires = 'N' OR expire_date > NOW())";
		$sql .= " AND promo_code_require_cats.cat_id IN ($cat_ids) ";
		$sql .= " ORDER BY promo_codes.id desc";
	//echo $sql;
		$ret = db_query_array( $sql );
		if($ret){
			return $ret;
		}else{
		    return array();
		}
		
	}
	function removeAutoPromoCodes($order_id=''){
		
		if($order_id){
			$promos = Orders::getPromoCodes($order_id,'', '', 'validate');
		}else{
			global $cart;
			$promos = $cart->getPromoCodes('','','validate');
		}
				
		foreach ($promos as $promo){
			if ($promo['auto'] == 'Y')
			{
				if($order_id)
				{
					Orders::deleteOrderPromoCode($order_id,$promo['id']);
				}else{
					$cart->deletePromoCode('',$promo['promo_code_id']);
				}
			}
		}
		
		if($order_id){
			$temp_promos = Orders::getPromoCodes($order_id,'', '', 'validate');
		}else{
			global $cart;
			$temp_promos = $cart->getPromoCodes('','','validate');
		}
	}
        static function getDiscountText($promotion)
        {
            // This product is eligible for a [X PERCENT / $XXX] discount if you purchase X or more.
            //print_ar($promotion);
            $promotion_details = PromoCodes::getCodeProductDiscount(0, $promotion['promo_code_id']);
            $promotion_requirements = PromoCodes::getCodeRequirements(0, $promotion['promo_code_id']);
            
            $required_purchase_amount = number_format( $promotion_requirements[0]['qty'], 0 );
            $discount_type = $promotion_details[0]['product_discount_type'];
            $discount_amount = number_format( $promotion_details[0]['product_discount_amount'], 0 );
            $product_name = $promotion_details[0]['product_name'];

            if($promotion['text_type'] == 'case')
            {
                $return_text = 'Order by the case (' . $required_purchase_amount .  ' pcs) and receive ';
                if($discount_type=='dollars')
                {
                    $return_text .= '$' . $discount_amount . ' ';
                }
                else
                {
                    $return_text .=  $discount_amount .'% ';
                }

                $return_text .= ' off!';

                }
            else
            {
                $return_text = 'There is a ';
                if($discount_type=='dollars')
                {
                    $return_text .= '$' . $discount_amount . ' ';
                }
                else
                {
                    $return_text .=  $discount_amount .'% ';
                }

                $return_text .= ' discount on this item if you purchase ' . $required_purchase_amount . ' or more!';

            }
            
            return $return_text;

        }

        //use this to pass a new PER ITEM accounting for discount for AMAZON and GOOGLE CHECKOUT
        static function getDiscountedPricePerItem($cart_line,$discount_line,$round=false)
        {
            //print_ar($cart_line);
            //print_ar($discount_line);

            $total_discount = (double) $discount_line['this_overall_discount'];
            $line_total = (double) $cart_line['line_total'];
            $total_items = $cart_line['qty'];

            $discounted_total = (double) $line_total - $total_discount;
            $newPricePerItem = (double) $discounted_total/$total_items;
            //$roundedPricePerItem = round_up($newPricePerItem,2,PHP_ROUND_HALF_UP);
            $roundedPricePerItem = PromoCodes::round_up($newPricePerItem,2);
            //echo 'discounted_total ' . $discounted_total;
            //echo 'new price per item ' . $newPricePerItem;
            //echo 'round/ceil/etc ' . $roundedPricePerItem;
            if($round)
            {
                return $roundedPricePerItem;
            }
            else
            {
                return $newPricePerItem;
            }
            

        }

       static function round_up ($value, $places=0) {
          if ($places < 0) { $places = 0; }
          $mult = pow(10, $places);
          return ceil($value * $mult) / $mult;
        }
		
       function checkLoggedIn ($coupon_info,$order_id='',$cart_reference='')
	   {
			if($order_id){
				$order_info = Orders::get1($order_id);
				if($order_info['customer_id']>0){
					return true;
				}else{
					return false;
				}
			}elseif($cart_reference){
				$reference = Cart::get1CartReference($cart_reference);
				if($reference['customer_id']){
					return true;
				}else{
					return false;
				}
			}
			global $cart;							
	
			if (!$_SESSION['cust_acc_id'])
				return false;
			else 
				return true; 			    
	    }
	    
		function checkIfAccountUsedAlready($coupon_info,$order_id='',$cart_reference='')
		{
			global $cart;							
		
			if ($order_id){
				$order_info = Orders::get1($order_id);
				$cust_id = $order_info['customer_id'];
			}elseif ($cart_reference){
				$refernce_info = Cart::get1CartReference($cart_reference);
				$cust_id = $refernce_info['customer_id'];
			}
			elseif($_SESSION['cust_acc_id']){
				$cust_id = $_SESSION['cust_acc_id'];
			}
			if($cust_id){
				$see_if_used_sql = "SELECT * FROM orders, order_promo_codes 
										WHERE orders.id = order_promo_codes.order_id 
										AND order_promo_codes.promo_code_id = ".$coupon_info['id']." 
										AND orders.customer_id = ".$cust_id;
				if ($order_id) $see_if_used_sql .= " AND orders.id <> ".$order_id ;
				$see_if_used_result = db_query_array($see_if_used_sql);
				if ($see_if_used_result) return false;						
				else return true;
			}
			else return false; 			
		}
		function checkAccountActivated ($coupon_info,$order_id='',$cart_reference='')
		{
			if($cart_reference){
				$reference = Cart::get1CartReference($cart_reference);
				if($reference['customer_id'])
					$cust_acc_id = $reference['customer_id'];
			} else {
				if($_SESSION['cust_acc_id'])
					$cust_acc_id = $_SESSION['cust_acc_id'];
			}
			
			if ($cust_acc_id || $order_id)
			{
				if($order_id){
					$order_info = Orders::get1($order_id);
					$the_cust = $order_info['customer_id'];
					if(!$the_cust) return false;
				}else{
					$the_cust = Customers::get1($cust_acc_id);
				}
				if ($the_cust['account_activated'] == "N")
				{
					return false;
				}
				else return true;				
			}
			else return false;
		}

		function checkInstitutionType ($coupon_info,$order_id='',$cart_reference='')
		{
			if($order_id)
			{
				$order_info = Orders::get1($order_id);
				$cust_id = $order_info['customer_id'];
			}
			elseif ($cart_reference){
				$refernce_info = Cart::get1CartReference($cart_reference);
				$cust_id = $refernce_info['customer_id'];
			}
			elseif ($_SESSION['cust_acc_id'])
			{
				$cust_id = $_SESSION['cust_acc_id'];
			}
			if($cust_id)
			{
				$the_cust = Customers::get1($cust_id);
				if (strpos($coupon_info['institution_type_restriction'], "|".$the_cust['institution_type_id']."|") !== false)
				{
					return true;
				}
				else return false;				
			}
			else return false;	
		}
		
		function checkRole ($coupon_info,$order_id='',$cart_reference='')
		{
			
			if($order_id)
			{
				$order_info = Orders::get1($order_id);
				$cust_id = $order_info['customer_id'];
			} elseif ($cart_reference){
				$refernce_info = Cart::get1CartReference($cart_reference);
				$session_id = $refernce_info['session_id'];
				$cust_id = $refernce_info['customer_id'];
			}
			elseif ($_SESSION['cust_acc_id'])
			{
				$cust_id = $_SESSION['cust_acc_id'];
			}
			
			if($cust_id)
			{
				$the_cust = Customers::get1($cust_id);
				if (strpos($coupon_info['role_restriction'], "|".$the_cust['role_id']."|") !== false)
				{
					return true;
				}
				else return false;				
			}
			else return false;	
		}
		
		function checkIfNewAccount($coupon_info,$order_id='',$cart_reference='')
		{ 
			if($order_id)
			{
				$order_info = Orders::get1($order_id);
				$cust_id = $order_info['customer_id'];
			} 
			elseif ($cart_reference){
				$refernce_info = Cart::get1CartReference($cart_reference);
				$cust_id = $refernce_info['customer_id'];
			}
			elseif($_SESSION['cust_acc_id'])
			{
				$cust_id = $_SESSION['cust_acc_id'];
			}
			if($cust_id)
			{
				$the_cust = Customers::get1($cust_id);
				if ($the_cust['account_system'] == "old")
				{
					return false;
				}
				else return true;				
			}
			else return false;			
		}
		function checkIfEssensa ($coupon_info,$order_id='',$cart_reference='')
		{
			if($order_id)
			{
				$order_info = Orders::get1($order_id);
				$cust_id = $order_info['customer_id'];
			} elseif ($cart_reference){
				$refernce_info = Cart::get1CartReference($cart_reference);
				$session_id = $refernce_info['session_id'];
				$cust_id = $refernce_info['customer_id'];
			}
			elseif ($_SESSION['cust_acc_id'])
			{
				$cust_id = $_SESSION['cust_acc_id'];
			}
			
			//var_dump($cust_id);
			
			if($cust_id)
			{
				$the_cust = Customers::get1($cust_id);
				if ($the_cust['ein_number'] != '')
				{
					// see if that ein_number matches an active listing in the essensa_members
					// table
					$see_if_active_essensa_member_sql = "SELECT * FROM essensa_members WHERE
						essensa_members.gpo_id = '" . $the_cust['ein_number'] ."' AND 
						essensa_members.member_status = 'Active'"; 
					$see_if_active_essensa_member = db_query_array($see_if_active_essensa_member_sql);
					
					if ($see_if_active_essensa_member) return true;
					else return false;
				}
				else return false; // we know they're not Essensa if the EIN is not filled in				
			}
			else return false;	
		}
		function checkCouponCode($order_id,$promo_code){
			$return = array('status'=>'',
					'result'=>true,);
		
			if(!$promo_code){
				$return['status'] = "Sorry, invalid coupon code entered.";
				$return['result'] = false;
				return $return;
			}
			$promo_info = db_get1( PromoCodes::getCodes('',$promo_code) );
		
			if(!$promo_info){
				$return['status'] = "Sorry, that coupon code was not found.";
				$return['result'] = false;
				return $return;
			}
		
			if($order_id){
				$existCode = Orders::getPromoCodes($order_id,'',$promo_info['id']);
				$all_codes = Orders::getPromoCodes($order_id);
			} else {
				global $cart;
				$existCode = $cart->getPromoCodes('',$promo_info['id']);
				$all_codes = $cart->getPromoCodes();
			}
			if($existCode){
				$return['status'] = "Sorry, you have already applied that coupon code.";
				$return['result'] = false;
				return $return;
			}
			
			PromoCodes::removeAutoPromoCodes($order_id);
			
			if($order_id){
				$all_codes = Orders::getPromoCodes($order_id);
			} else {
				global $cart;
				$all_codes = $cart->getPromoCodes();
			}
			
			$coupresult = PromoCodes::getValidity($promo_info,$order_id);
			
			//print_ar($all_codes);
			//print_ar($return);
			
			$conflict = !PromoCodes::checkCouponConflicts($promo_info['id'],$all_codes);
			if($coupresult['result'] == false){
				$return['status'] = $coupresult['msg'];
				$return['result'] = false;
			} else if($conflict){
				$return['status'] = "Sorry, that coupon code causes a conflict with another one.";
				$return['result'] = false;
			} else {
				$return['promo_id'] = (int)$promo_info['id'];
// 				if(!$order_id){
// 					$cart->addPromoCode((int)$promo_info['id']);
// 				}
// 				if($promo_info['send_gift'] != 'N'){
// 					if($promo_info['send_gift'] == 'product'){
// 						//need to check if there is stock available
// 						$can_buy = checkCanBuy($promo_info['gift_product_id']);
// 						if($can_buy){
// 							$cart->add($promo_info['gift_product_id'],1,$promo_info['gift_product_option_id'],0,0,'', 0,'','','gift');
// 						} else {
// 							$promoCodeError = "Sorry, that coupon has expired.";
// 						}
// 					} else {
// 						//need to add functionality to eamil the coupon /after shipping
// 						$email_coupon = '';
// 					}
// 				}
			}
			return $return;
		}
		
		function discountCalc($order_id, $shipping_amt = 0.00, $max_discount = '',$for_checkout=false, $cart_reference='')
		{
			global $essensaMessage;
			global $promoCodeError;
		
			//$_SESSION['cart_prices'] = array();
		
			if($order_id){
				Orders::deleteGiftItems($order_id);
				$info = Orders::getItems(0,$order_id);
				$applied_promos = Orders::getPromoCodes($order_id);
			} else {
				$cart = new Cart();
				$cart->deleteGifts();
				$info = $cart->get('','','','','','',$cart_reference);
				$applied_promos = $cart->getPromoCodes('','','',$cart_reference);
			} 
				
			//mail('tova@tigerchef.com', 'applied promos', print_r($applied_promos,true));
			//mail('tova@tigerchef.com', 'items', print_r($info,true));

			$debug = print_r($applied_promos,true);
			
			$overall_discount = 0.00; 
			$overall_shipping_discount = 0.00;
			$product_discounts = array();

			$got_essensa_automatic_discount = false;
			if(is_array($info))foreach( $info as $item )
			{
				$product = Products::get1( $item['product_id'] );
				//print_ar($item);
				if( $product['exclude_discount'] == 'N' )
				{
					$promotion = Array();
					
					$item_cats = Cats::getCatsForProductSimple($item['product_id']);
					//$auto_product_promotion = PromoCodes::getProductAuto($item['product_id']);
					if ($item_cats) $auto_cat_promotion = PromoCodes::getCatAuto($item_cats);
					$auto_product_promotion = PromoCodes::getProductAuto($item['product_id']);
					
					if ($auto_cat_promotion && $auto_product_promotion)	$promotion = array_merge($auto_cat_promotion,$auto_product_promotion);
					else if ($auto_cat_promotion) $promotion = $auto_cat_promotion;
					else if ($auto_product_promotion) $promotion = $auto_product_promotion;
					
					//print_r($promotion);
					
					if(is_array($promotion))
					{
						foreach($promotion as $single_promotion)
						{
							$original_promo_code = PromoCodes::get1Code($single_promotion['promo_code_id']);
							$valid = PromoCodes::getValidity($original_promo_code,$order_id,$cart_reference); // ONLY WORKS WITH ORIGINAL PROMOCODE OBJECT!
							//print_ar($valid); 
							//mail("rachel@tigerchef.com", "here","here" );
							if($valid['result']) // NEED TO PARSE ALL PROMOS AND RETURN THE ONE THAT APPLIES TO THE CART AS IT IS NOW
							{
								$single_promotion['silent_on_fail'] = true;
		
								if ($single_promotion['promo_code_id'])
									$already_there = false;
								
								foreach ($applied_promos as $the_app_promo)
								{
									if ($the_app_promo['promo_code_id'] == $single_promotion['promo_code_id'])
									{
										$already_there = true;
									}
									if ($the_app_promo['essensa_only'] == 'Y')
									{
										$got_essensa_automatic_discount = true;
										$essensaMessage = "As an Essensa member, you have automatically
											received " . str_replace("for Essensa Members", "", $original_promo_code['description']) .
													" on your order!";
									}
								}
								if (!$already_there && ($original_promo_code['essensa_only'] == 'N' || !$got_essensa_automatic_discount ))
								{
									if ($original_promo_code['essensa_only'] == 'Y')
									{
										$got_essensa_automatic_discount = true;
										$essensaMessage = "As an Essensa member, you have automatically
									received " . str_replace("for Essensa Members", "", $original_promo_code['description']) .
											" on your order!";
									}
									//$single_promotion['auto_applied'] = 'Y';
									$applied_promos[] = $single_promotion;
										
									if($order_id){
										Orders::insertOrderPromoCode(array('order_id' => $order_id, 'promo_code_id' =>(int)$single_promotion['promo_code_id'], 'description' => $original_promo_code['description']));
									} else {
										$cart->addPromoCode((int)$single_promotion['promo_code_id'],$cart_reference);
									}
										
									$product_discounts[$item['product_id']] = $discount['product_discount_amount'];
									break; // get first. Ordered by magnitude in sql call
								}
		
							}
							//else if ($promoCodeError == "") $promoCodeError = $valid['msg'];
							else if ($essensaMessage == "")
							{
								//if ($valid['auto_coupon'] == 'N') // only show the message if the coupon creating the issue was not another automatic one
								//{
								if ($applied_promos) $essensaMessage = "Automatic Essensa discounts cannot be combined with other promotions. <br/><small>Remove current coupon code to receive automatic discount.</small>";
								else $essensaMessage = "Your cart does not currently qualify for any automatic Essensa discounts.";
								//}
							}
						}
					}
				}
				$str .= "\n\n";
			}
			$str .= "\n\nlast=>" . time();
				
			if($order_id){
				Orders::revertOrderNoProdsQualify($order_id);
			} else {
				$cart->revertCartNoProdsQualify($cart_reference);
			}
		
			$already_discounted_totals_array = array();
			if ($applied_promos) foreach ($applied_promos as $ap) {
//print_ar($ap);	 	
				$this_item_discount	= 0;
				$this_cat_discount	= 0;
				$this_brand_discount	= 0;
				$a_info	= PromoCodes::get1Code($ap[promo_code_id]);
		
				$a_details		= PromoCodes::getCodeProductDiscount(0,$ap[promo_code_id]);
				$a_details		= $a_details[0];
				//print_ar($a_details);
				$a_validity		= PromoCodes::getValidity($a_info, $order_id, $cart_reference);
		//$debug .= " === " . print_r($ap[promo_code_id],true) ." info ".print_r($a_info,true) ." details  ".print_r($a_details) ." validity " . print_r($a_validity,true);
				//var_dump($a_validity); 
				if ($a_validity['result']) {
		
					if($order_id){
						Orders::setUpProdWithCouponsInOrder($a_info, $order_id);
					} else {
						$cart->setUpProdWithCouponsInCart($a_info, $cart_reference);
					}
					
					if($a_info['send_gift'] != 'N'){
						if($order_id){
							//add gift item to order
							$order_item_id = Orders::insertItem(array('order_id'=>$order_id,'product_id'=>$a_info['gift_product_id'],'price'=>0,'qty'=>1,'is_gift'=>'Y'));
							if($a_info['gift_product_option_id']){
								Orders::insertItem(array('order_item_id'=>$order_item_id,'product_option_id'=>$a_info['gift_product_option_id'],'additional_price'=>0));
							}
						} else {
							$cart->add($a_info['gift_product_id'],1,$a_info['gift_product_option_id'],0,0,'', 0,'','','gift');
						}
					}
					
					/* discount_amount already_discounted_totals_array */
					$this_total_discount_array = PromoCodes::getDiscountOnTotal($a_info, $already_discounted_totals_array, $overall_shipping_discount,$order_id,$cart_reference);
					//print_r($this_total_discount_array);
					$already_discounted_totals_array = $this_total_discount_array['already_discounted_totals_array'];
					$this_total_discount = $this_total_discount_array['discount_amount'];
					//echo 'this total discount'; print_ar($this_total_discount);
					// note: these 4 never return anything anymore!!!!
					$this_item_discounts = PromoCodes::getDiscountOnItems($a_info,$order_id);
					//echo 'this item discount'; print_ar($this_item_discounts);
					// new code for category discounts ...
					$this_cat_discounts = PromoCodes::getDiscountOnCats($a_info,$order_id);
					//echo 'this cat discount'; print_ar($this_cat_discounts);
					$this_occ_discounts = PromoCodes::getDiscountOnOccs($a_info,$order_id);
					//echo 'this occ discount'; print_ar($this_occ_discounts);
					$this_brand_discounts = PromoCodes::getDiscountOnBrands($a_info,$order_id);
					//	print_r( $this_brand_discounts );
					//	echo 'this brand discount'; print_ar($this_brand_discounts);
					//$this_shipping_discount = PromoCodes::getDiscountOnShipping($a_info, $shipping_amt);
					//$overall_shipping_discount += $this_shipping_discount;
					//echo "this shipping discount with $shipping_amt"; print_ar($this_shipping_discount);
					$full_discount_breakdown = array(
							'total_discount'	=> $this_total_discount,
							'item_discount'	=> $this_item_discounts,
							'cat_discount'	=> $this_cat_discounts,
							'brand_discount'	=> $this_brand_discounts,
					);
					//  removed 'shipping_discount' => $this_shipping_discount,
					if ($this_item_discounts) foreach ($this_item_discounts as $tid) {
						$this_item_discount += $tid[discount];
					}
		
					if ($this_cat_discounts) foreach ($this_cat_discounts as $tcd) {
						$this_cat_discount += $tcd[discount];
					}
		
					//print_ar($this_occ_discounts);
		
					if ($this_occ_discounts) foreach ($this_occ_discounts as $tod) {
						$this_occ_discount += $tod[discount];
					}
					if ($this_brand_discounts) foreach ($this_brand_discounts as $tod) {
						$this_brand_discount += $tod[discount];
					}
		
					$this_overall_discount = $this_total_discount + $this_item_discount + $this_shipping_discount + $this_cat_discount + $this_occ_discount + $this_brand_discount;
					//echo "$this_overall_discount = $this_total_discount + $this_item_discount + $this_shipping_discount + $this_cat_discount + $this_occ_discount + $this_brand_discount";
					//echo 'this overall discount ' . $this_overall_discount;
					$overall_discount += $this_overall_discount;
					//print_ar( $overall_discount );
					// this is very non-MVC ... move this out
					//$ap_list[] = "$ap[promo_code_code] - $ap[promo_code_description]; \$" . number_format($this_overall_discount,2) . " off; &nbsp;<a href='/cart.php?action=remove_promo&id=$ap[id]'>remove</a> ";
		
						
						
					//print_ar($ap);
						
					if($for_checkout)
					{
						$aps[] = array('discount_amount'=>$a_info['global_discount_amt'],'shipping_discount_amount'=>$a_info['shipping_discount_amt'],'discount_type'=>$a_info['global_discount_type'],'shipping_discount_type'=>$a_info['shipping_discount_type'],'promo_code_code' => $ap['promo_code_code'], 'promo_code_description' => $ap['promo_code_description'], 'this_overall_discount' => $this_overall_discount, 'id' => $ap[id],'auto_applied' => 'Y', 'product_id' => $ap['product_id'], 'send_gift' => $a_info['send_gift']);
					}
					elseif($ap['silent_on_fail'])
					{
						//  $aps[] = array('discount_amount'=>$a_details['product_discount_amount'],'discount_type'=>$a_details['product_discount_type'],'promo_code_code' => $ap['code'], 'promo_code_description' => $ap['description'], 'this_overall_discount' => $this_overall_discount, 'id' => $ap[id],'auto_applied' => true);
						$aps[] = array('discount_amount'=>$a_info['global_discount_amt'],'shipping_discount_amount'=>$a_info['shipping_discount_amt'],'discount_type'=>$a_info['global_discount_type'],'shipping_discount_type'=>$a_info['shipping_discount_type'],'promo_code_code' => $ap['promo_code_code'], 'promo_code_description' => $ap['promo_code_description'], 'this_overall_discount' => $this_overall_discount, 'id' => $ap[id],'auto_applied' => 'Y' , 'send_gift' => $a_info['send_gift']);
					}
					else
					{
						//$aps[] = array('discount_amount'=>$a_details['product_discount_amount'],'discount_type'=>$a_details['product_discount_type'],'promo_code_code' => $ap[promo_code_code], 'promo_code_description' => $ap[promo_code_description], 'this_overall_discount' => $this_overall_discount, 'id' => $ap[id]);
						$aps[] = array('discount_amount'=>$a_info['global_discount_amt'],'shipping_discount_amount'=>$a_info['shipping_discount_amt'],'discount_type'=>$a_info['global_discount_type'],'shipping_discount_type'=>$a_info['shipping_discount_type'],'promo_code_code' => $ap['promo_code_code'], 'promo_code_description' => $ap['promo_code_description'], 'this_overall_discount' => $this_overall_discount, 'id' => $ap[id] , 'auto_applied' => $a_info['auto'], 'send_gift' => $a_info['send_gift']);
					}
		
				}elseif(!$ap['silent_on_fail']) {
					// this is very non-MVC ... move this out
					//$ap_list[] = "$ap[promo_code_code] - $ap[promo_code_description]; &nbsp;<font color=red>invalid - $a_validity[msg]</font>; <a href='/cart.php?action=remove_promo&id=$ap[id]'>remove</a> ";
		
					//$aps[] = array('discount_amount'=>$a_details['product_discount_amount'],'discount_type'=>$a_details['product_discount_type'],'promo_code_code' => $ap[promo_code_code], 'promo_code_description' => $ap[promo_code_description], 'invalid_msg' => $a_validity[msg], 'id' => $ap[id]);
		// this was only line here in this elseif before the if-else below
					//$aps[] = array('discount_amount'=>'0.00','shipping_discount_amount'=>'0.00','discount_type'=>'0.00','shipping_discount_type'=>$a_info['shipping_discount_type'],'promo_code_code' => $ap['promo_code_code'], 'promo_code_description' => $ap['promo_code_description'], 'this_overall_discount' => '0.00', 'id' => $ap[id]);
					
					// NEW!  INSTEAD OF INSERTING WITH 0.00, REMOVE IT!!
					
					if($order_id){
						Orders::deleteOrderPromoCode($order_id, $ap['promo_code_id']);
					} else {
						$cart->deletePromoCode((int)$ap['id']);
						$promoCodeError = "  Promo code " . $ap['promo_code_code'] . " was removed because the cart no longer qualifies for that coupon code.";
					}														
				}
			}			
			
			//mail('tova@tigerchef.com','debug',$debug);
		
			//if ($ap_list)
			//  $ap_glued = implode('<br>', $ap_list);
		
			//var_dump($max_discount);
		
			//Need to add brand discounts for bulk purchasing
		
			//if($_SESSION['cust_acc_id'] == '6291')
			//mail('tova@tigerchef.com', 'calculate all ', var_export($str,true).$str2);	
		
			$brands = array();
			if(is_array($info))foreach( $info as $item )
			{
				$product = Products::get1( $item['product_id'] );
		
				if( $product['exclude_discount'] == 'N' )
					$brands[$product['brand_id']] += $item['line_total'];
			}
		
			$brand_discounts = array();
			foreach( $brands as $brand_id => $amount )
			{
				$brand = Brands::get1( $brand_id );
				$o = (object) array();
				$o->brand_id = $brand_id;
				$o->is_active = 'Y';
				$o->order = "amount";
		
				$discounts = BrandDiscount::getByO( $o );
				//print_ar($discounts);
				if( $discounts )
				{
					foreach( $discounts as $discount )
					{
						if( $amount >= $discount['amount'] )
						{
							$brand_discounts[$brand_id] = $amount * ($discount['discount'] / 100 );
							$aps[] = array('discount_amount'=>$discount['amount'],'discount_type'=>'percent','promo_code_code' => $brand['name'], 'promo_code_description' => $brand['name'].' Bulk Discount', 'this_overall_discount' => $brand_discounts[$brand_id], 'auto_applied' => true);
							$overall_discount += $brand_discounts[$brand_id];
							break;
						}
					}
				}
			}
		
			// ALSO need to add product discounts for Product level Promocodes
			/*
			 $product_discounts = array();
			if(is_array($cart_info))foreach( $cart_info as $item )
			{
			$product = Products::get1( $item['product_id'] );
			//print_ar($item);
			if( $product['exclude_discount'] == 'N' )
			{
			$promotion = PromoCodes::getProductAuto($item['product_id']);
			if(is_array($promotion))
			{
			foreach($promotion as $single_promotion)
			{
				
			//print_ar($item);
				
			$discount = PromoCodes::getCodeProductDiscount(0, $single_promotion['promo_code_id']);
			$discount = $discount[0];
			print_ar($discount);
			if($discount['product_discount_type']=='percent')
			{
		
			}
			$original_promo_code = PromoCodes::get1Code($single_promotion['promo_code_id']);
			$valid = PromoCodes::getValidity($original_promo_code); // ONLY WORKS WITH ORIGINAL PROMOCODE OBJECT!
			//print_ar($valid);
			if($valid['result'])
			{
			$product_discounts[$item['product_id']] = $discount['product_discount_amount'];
			$aps[] = array('promo_code_code' => $single_promotion['code'], 'promo_code_description' => $single_promotion['description'], 'this_overall_discount' => $discount['product_discount_amount'], 'auto_applied' => true);
			$overall_discount += $product_discounts[$item['product_id']];
			}
				
			}
			}
				
			}
		
			}
			*/
		
			// end product level discounts
		
			if ($max_discount) {
				if ($overall_discount >= $max_discount) {
					$overall_discount = $max_discount;
				}
			}
		
			$return_array = array( 'aps' => $aps, 'overall_discount' => $overall_discount, 'brand_discounts' => $brand_discounts,'product_discounts' => $product_discounts, 'breakdown' => $full_discount_breakdown, 'shipping_discount' => $overall_shipping_discount );
		
			return $return_array;
		
		}
		function checkGiftStock($coupon_info){
			if($coupon_info['send_gift'] != 'N'){
				if($coupon_info['send_gift'] == 'product'){
					//need to check if there is stock available
					$can_buy = self::checkCanBuy((int)$coupon_info['gift_product_id'],(int)$coupon_info['gift_product_option_id']);
				} else {
					//need to add functionality to email the coupon /after shipping
					$email_coupon = '';
				}
			}
			else $can_buy = true;
			return $can_buy;
		}
		
		function checkCanBuy($product_id, $product_option_id='', $qty=1){
			global $CFG;
			
			$product = Products::get1( $product_id );

			$can_buy_info = Products::getCanBuyInfo($product, $CFG);
    		$can_buy = $can_buy_info['can_buy'];
    		$can_buy_limit = $can_buy_info['can_buy_limit'];
    
    		if( $can_buy_limit < $qty && !$can_buy)
				return false;
			else
				return true;
		}

                static function generateRandomString($length = 10) {
                $characters = '23456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                    return $randomString;
        }

        static function codeExists($code){
            return sizeof(self::getCodes(0, $code)) > 0;
        }

        static function createForNewsletter(){
            $code =self::generateRandomString();
            while(self::codeExists($code)){
                $code = self::generateRandomString();
            }
            $info = array( 'code' => $code,
                'description' => '$10 off $50+ for signing up to mailing list',
                'start_date_by_local_time' => 'N', 'expires' => 'N',
                'expire_date_by_local_time' => 'N', 'uses_limited' => 'Y',
                'uses' => '1', 'global_discount_amt' => '10',
                'global_discount_type' => 'dollars', 'global_discount_min' => '50',
                'global_is_max' => 'N', 'global_discount_max' => '',
                 'shipping_discount_amt' => '', 'shipping_discount_type' => 'dollars',
                 'combo_type' => 'N', 'auto' => 'N', //'req_cat_type' => 'Include',
                // 'req_brand_type' => 'Include',
                 'text_type' => 'standard',
                 'one_use_per_account' => 'N', 'new_accounts_only' => 'N',
                 'essensa_only' => 'N', 'able_to_be_combined_with_sales' => 'N',
                 'gift_product_id' => '', 'gift_product_option_id' => '',
                 'gift_promo_id' => '', 'send_gift' => 'N',
                 'expire_date' => '0000-00- 00:00:00',
                 'start_date' => date('Y-m-d H:i:s'),
                  );
            $result = self::insertCode($info);
            if($result){
               return $code;
            }
            else{
                mail('hgolov@gmail.com', 'Error inserting promo code', var_export($info, 1));
            }
            return false;
        }
		
		
		function checkProductforCoupon($promo_id, $product_id){
			
			if(!$promo_id){
				return $return;
			}
			$promo_info = db_get1( PromoCodes::getCodes($promo_id) );
		
			if(!$promo_info){
				return $return;
			}
			
		$req_brand_info = PromoCodes::getCodeBrandRequirementsToCalc(0, $promo_id,"",$promo_info);
	  	$req_cat_info = PromoCodes::getCodeCatRequirements(0, $promo_id);
	  	$product=Products::get1($product_id);
		
		$req_cat_array=array();
		 foreach ( $req_cat_info as $c){
		         $req_cat_array[]=$c['cat_id'];
		     }
	   
		$req_brand_array=array();
		 foreach ( $req_brand_info as $c){
		         $req_brand_array[]=$c['brand_id'];
		     }
	    
		if(!$req_brand_info && !$req_cat_info){
	  		return true;
	  	}
	  	
	    $product_cats = Cats::getCatsForProduct( $product_id, 'id' );
  		$all_product_cats = $product_cats;
	  		foreach ($product_cats as $thePC)
	  		{
	  			$symbolic_product_cats = Cats::getSymbolicCats($thePC['id']);
	  		
	  			if (count($symbolic_product_cats) > 0)
	  			{
	  				foreach($symbolic_product_cats as $spc)
	  				{
	  					$all_product_cats[$spc['id']] = $spc;
	  			    }
	  			}
	  		}
		
  
			$cat_match = false;
	  		$brand_match = false;   
            
			if ($promo_info['req_cat_type']=='Include')
	  			foreach ($all_product_cats as $thisCat) {
	  				 if (in_array($thisCat['id'],$req_cat_array)&& ($promo_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id)))
	  				 {
	  					$cat_match = true;
	  				 }
  			    }	
				
				if ($promo_info['req_cat_type']=='Exclude'){
					foreach ($all_product_cats as $cat){
					  if (!(in_array($cat['id'],$req_cat_array))&& ($promo_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id))) 
	  				      {
	  					    $cat_match = true;
	  				      }
					else  {
						    $cat_match = false;
							break;
					       }	  
   
				      } 
                   		  
					}
	  	
	  				$req_brand = $product['brand_id'];
	  					  					
	  				if (in_array ($req_brand, $req_brand_array) && (($coupon_info['able_to_be_combined_with_sales'] == 'Y' || !Sales::seeIfCurrentNotSoldOutSaleForProd ($product_id)))) 
						{
	  					$brand_match = true;
   	  				    }
	  			
			
				
	  			//echo "<br>catmatch:".var_export($cat_match,true)."@brandmatch:".var_export($brand_match,true)."@return:".var_export($return,true)."<br>";
				
	  			if( ($req_brand_info && $req_cat_info && $cat_match && $brand_match) || (!$req_cat_info && $req_brand_info && $brand_match) || (!$req_brand_info && $req_cat_info && $cat_match)){
	  				return true;
	  			}
				else return false;
		
        }

}
