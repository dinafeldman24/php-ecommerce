<?php class Pending_Review
{
    public static function get($id = 0, $review_id = 0)
    {
        $sql = "SELECT pending_review.*
                FROM pending_review
                WHERE 1 ";

        if ($id > 0)
        {
            $sql .= " AND pending_review.id = $id ";
        }

        if ($review_id > 0)
        {
            $sql .= " AND pending_review.review_id = $review_id ";
        }

        return db_query_array($sql);
    }
		
	

    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }

    public static function insert($info)
    {
				$safetitle=mysql_real_escape_string($info['title'] );
				$safecomment=mysql_real_escape_string($info['comment'] );
				$info['title']=$safetitle;
				$info['comment']=$safecomment;	
						
        return db_insert('pending_review', $info);
    }


    public static function delete($id)
    {

				return db_delete('pending_review', $id);
    }

    public static function update($id, $info)
		
    {   if ($info['title']){
    		$safetitle=mysql_real_escape_string($info['title'] );
				$info['title']=$safetitle;
				}
				if ($info['comment']){
				$safecomment=mysql_real_escape_string($info['comment'] );
				$info['comment']=$safecomment;
				}				
				
        return db_update('pending_review', $id, $info );
    }
		
    static function getallreviewinfo($review_id){
		$sql = "SELECT product_reviews.*, products.name as product_name,customers.first_name as customer_first_name,customers.last_name as customer_last_name, customers.img_url as customer_img_url, product_ratings.rating as product_rating,product_recommend.recommend as product_recommend,pending_review.title as edited_title, pending_review.comment as edited_comment
                FROM product_reviews
                LEFT JOIN products ON product_reviews.product_id =products.id
                LEFT JOIN customers ON product_reviews.customer_id = customers.id
                LEFT JOIN product_ratings ON product_reviews.product_id = product_ratings.product_id AND product_reviews.customer_id = product_ratings.customer_id
								LEFT JOIN product_recommend ON product_reviews.product_id = product_recommend.product_id AND product_reviews.customer_id = product_recommend.customer_id
								RIGHT JOIN pending_review ON product_reviews.id= pending_review.review_id
								";
                if ($review_id){
								$sql=$sql."WHERE product_reviews.id='{$review_id}'"; 
								
								}
        return db_query_array($sql);
		
		}

}
?>