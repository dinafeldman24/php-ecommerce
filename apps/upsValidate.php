<?php
class upsValidate {
    var $AccessLicenseNumber;  
    var $UserId;  
    var $Password;
    var $ShipperNumber; 
    var $credentials;

    function setCredentials($access,$user,$pass,$shipper) {
    	   
		$this->AccessLicenseNumber = $access;
		$this->UserID = $user;
		$this->Password = $pass;	
		$this->ShipperNumber = $shipper;
		$this->credentials = 1;
    }

    // Define the function validateAddress() - no parameters
    function validateAddress($address) {
		if ($this->credentials != 1) {
			print 'Please set your credentials with the setCredentials function';
			die();
		}
		
		$data .= "	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
					<AccessRequest xml:lang=\"en-US\">
						<AccessLicenseNumber>{$this->AccessLicenseNumber}</AccessLicenseNumber>
						<UserId>{$this->UserID}</UserId>
						<Password>{$this->Password}</Password>
					</AccessRequest>
					<?xml version=\"1.0\" encoding=\"UTF-8\"?>
					<AddressValidationRequest xml:lang=\"en-US\">
					  <Request>
						<TransactionReference>
						  <XpciVersion>1.0</XpciVersion>
						</TransactionReference>
						<RequestAction>XAV</RequestAction>
						<RequestOption>5</RequestOption>
					  </Request>
					  <AddressKeyFormat>
						<AddressLine>{$address['address1']}</AddressLine>
						<AddressLine/>
						<PoliticalDivision2>{$address['city']}</PoliticalDivision2>
						<PoliticalDivision1>{$address['state']}</PoliticalDivision1>
						<PostcodePrimaryLow>{$address['zip']}</PostcodePrimaryLow>
						<CountryCode>US</CountryCode>
					  </AddressKeyFormat>
					</AddressValidationRequest>";

// 			$ch = curl_init("https://www.ups.com/ups.app/xml/XAV");

			//test server
// 			$ch = curl_init("https://wwwcie.ups.com/ups.app/xml/XAV");
		$timeB4 = microtime (true);
		
			//live server
			$ch = curl_init("https://onlinetools.ups.com/ups.app/xml/XAV");
		
			curl_setopt($ch, CURLOPT_HEADER, 1);  
			curl_setopt($ch,CURLOPT_POST,1);  
			curl_setopt($ch,CURLOPT_TIMEOUT, 60);  
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);  
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data);  
			$result=curl_exec ($ch);  
		    //echo '<!-- '. $result. ' -->'; // THIS LINE IS FOR DEBUG PURPOSES ONLY-IT WILL SHOW IN HTML COMMENTS  
			curl_close($ch);

			$timeAfter = microtime(true);
			$dif = $timeAfter - $timeB4;
			
// 			if($_SESSION['cust_acc_id'] == '6056')
// 				mail('tova@tigerchef.com', 'curl ' . $dif ,$timeAfter .'-'. $timeB4);					
			
			$request = $data;
			$data = strstr($result, '<?'); 			
			
			$xml = simplexml_load_string($data);
			$json = json_encode($xml);
			$params = json_decode($json,TRUE);
			//mail('tova@tigerchef.com', 'ups validate', 'request'.var_export($request,true). 'result'.var_export($data,true));
			
			$params['request'] = $request;
			$params['response'] = $result;
			
			return $params;			
	    }  
    }
?>
