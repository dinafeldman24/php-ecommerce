<?php

class HomePageTabs 
{
	public static function insert($info)
	{		
		return db_insert('homepage_tabs', $info);
	}
	
	public static function update($id,$info)
	{
		return db_update('homepage_tabs', $id, $info);
	}
	
	public static function delete($id)
	{
		return db_delete('homepage_tabs', $id);
	}
		
	public static function get($id=0,$begins_with='',$keywords='',$order='',$order_asc='', $name='')
	{
		$sql = "SELECT homepage_tabs.*
				FROM homepage_tabs
				WHERE 1 ";
		
		if ($id > 0) 
		{
			$sql .= " AND homepage_tabs.id = $id ";
		}
		if ($begins_with != '') 
		{
			$sql .= " AND homepage_tabs.name LIKE '".addslashes($begins_with)."%'";
		}
		if ($keywords != '') 
		{
			$fields = Array('homepage_tabs.name');
			$sql .= " AND " . db_split_keywords($keywords,$fields,'AND',true);
		}
		if( $name != '' )
		{
			$sql .= " AND homepage_tabs.name = '$name' ";
		}	
			
		$sql .= " ORDER BY ";
		
		if ($order == '') 
		{
			$sql .= 'homepage_tabs.id';
		}
		else 
		{
			$sql .= addslashes($order);
		}
		
		if ($order_asc !== '' && !$order_asc) 
		{
			$sql .= ' ASC ';
		}
		
		return db_query_array($sql);
	}
	
	public static function get1($id)
	{
		$id = (int) $id;
		
		if (!$id) 
		{
		    return false;
		}
		
		$result = self::get($id);
		
		return $result[0];
	}
	
	public static function get1ByName($name)
	{
		$result = self::get(0, '', '', '', '', $name);
		if (count($result))
		{
			return $result[0];
		}
		else
		{ 
			return false;
		}
	}	
}

?>