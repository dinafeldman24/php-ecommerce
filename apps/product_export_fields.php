<?
class ProductExportFields
{
	static function getFieldCategoryOptions()
	{
    	$type = db_query_array( "SHOW COLUMNS FROM product_export_fields WHERE Field = 'field_category'" );
    	
    	$type_result = $type[0]['Type'];
    	
    	preg_match('/^enum\((.*)\)$/', $type_result, $matches);
    	foreach( explode(',', $matches[1]) as $value )
    	{
        	 $enum[] = trim( $value, "'" );
    	}
    	//print_r($enum);
    	
    	return $enum;
	}

	static function getFieldsForFieldCategory($field_type)
	{
		$result = db_query_array("SELECT * FROM product_export_fields WHERE
									field_category = '" . $field_type . "'");
		return $result;
	}
}
?>