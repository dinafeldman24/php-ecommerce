<?
	class Content {

		static function get($id=0,$page_id='',$featured_content='',$show_randomly_on_cat_pages='', $limit = '', $not_in_list = '', $order_by_rand = false, $resources_category = '')
		{
			$sql = " SELECT content.*
					 FROM content
					WHERE 1 ";
			if ($id > 0){
				$sql .= " AND content.id = $id ";
			}
			if ($page_id != ''){
				$sql .= " AND lower(content.page_id) = '" . strtolower( addslashes($page_id) ) . "' ";
			}
			if ($featured_content != ''){
				$sql .= " AND content.featured_content = '".addslashes($featured_content)."' ";
			}
			if ($show_randomly_on_cat_pages != ''){
				$sql .= " AND content.show_randomly_on_cat_pages = '".addslashes($show_randomly_on_cat_pages)."' ";
			}
			if ($not_in_list != '')
			{
				$sql .= " AND content.id not in ($not_in_list)";
			}
			if ($resources_category != ''){
				$sql .= " AND content.resource_center_classification like '%".addslashes($resources_category)."%' ";
			}
			if ($order_by_rand) $sql .= " ORDER BY RAND()";
			if ($limit != '') $sql .= " LIMIT $limit";
			
			$results = db_query_array($sql);
			return $results;
		}
		

		static function insert($info)
		{
			return db_insert('content',$info);
		}

		static function get1($id)
		{
			$result = Content::get($id);
			return $result[0];
		}

		static function getByPageId($page_id)
		{
			$result = Content::get(0,$page_id);
			return $result[0];
		}

		static function update($id,$info,$pk='')
		{
			db_update('content',$id,$info,$pk);
		}

		static function delete($id)
		{
			return db_delete('content',$id);
		}


		static function getProducts($content_id){
			$content_id = (int) $content_id;
			$sql = "SELECT * FROM content_products WHERE content_id = $content_id";
			return db_query_array($sql);
		}

		static function addAssociation($content_id,$product_id){
			$content_id = (int) $content_id;
			$product_id = (int) $product_id;
			if($content_id > 0 && $product_id > 0){
				db_insert('content_products',array("content_id"=>$content_id,"product_id"=>$product_id),'',true,true);
			}
		}

		static function removeAllAssociations($content_id){
			$content_id = (int) $content_id;
			$sql = "DELETE FROM content_products WHERE content_id = $content_id";
			db_query($sql);
		}

		static function removeAssociation($id,$product_id){
			$id = (int) $id;
			$product_id = (int) $product_id;
			if($id > 0 && $product_id > 0){
				$sql = "DELETE FROM content_products WHERE content_id = $id AND product_id = $product_id";
				return db_query($sql);
			}
		}

static function updateSitemap($show_message = false, $nossl = false)
		{
			global $CFG;
            if (!$nossl){
			   $filename='sitemap-ssl';
			   $fp = fopen($CFG->dirroot.'/'.$filename.'.xml', 'w');
			   $rss = '<?xml version="1.0" encoding="UTF-8"'.'?'.">\r\n";
			   $rss .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\"
				    xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">
					<url>
					<loc>{$CFG->sslurl}</loc>
					<priority>1.0</priority>
					</url>";	
			}
			
			else {
			/* open file first */
			$filename='sitemap';	   
			$fp = fopen($CFG->dirroot.'/'.$filename.'.xml', 'w');

			$rss = '<?xml version="1.0" encoding="UTF-8"'.'?'.">\r\n";
			$rss .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\"
				    xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">
					<url>
					<loc>{$CFG->nosslurl}</loc>
					<priority>1.0</priority>
					</url>";
            }
            fprintf($fp, $rss);

            /*
            *****************************************
            ** CONTENT LINKS
            *****************************************
            */
			$content_links = self::get();
            $links = array();

            foreach ($content_links as $content_link)
            {
            	if (substr($content_link['page_id'], 0,8) != "edit_me_")
            	{
                $links[] =  array
                            (
                                'url' => Catalog::makeContentLink($content_link['page_id'],"", $nossl),
                                'priority' => ".5"
                            );
            	}
            }

			foreach( $links as $link )
			{
			    fprintf($fp, "<url><loc>%s</loc><priority>%s</priority></url>\n", $link['url'], $link['priority']);
			}

			unset($links);


			/*
            *****************************************
            ** CATEGORY LINKS
            *****************************************
            */
			$links = array();
			$x = 0;

			//$cats = Cats::get(0, '', '', 'name', '', 0, '', '', 'N');
			$cats = Cats::get( 0, '', '', '', '', '', '', '', 'N' );

			if ($show_message)
			{
			     echo '<p><b>Category Links (' . count($cats) .')</b><p>';
			}

			if ($cats){
				foreach ($cats as $one_cat){

					$tree = Cats::getCatsTreeIDs( $one_cat['id'] );

					foreach( $tree as $t )
					{
						if( !$t )
						{
							continue 2;
						}
					}

					$links[$x]['url'] = Catalog::makeCatLink($one_cat,false,'',0,false, $nossl);
					$links[$x]['priority'] = ".9";
					$brands = Cats::getBrandsForCat($one_cat['id']);
					$x++;
					$sub_cats = Cats::get(0,'','','','',$one_cat['id'],'','','N');
				/*
					if (!empty($brands)){
						foreach ($brands as $brand){
							$brand_info = Brands::get1($brand['brand_id']);
							$links[$x]['url'] = Catalog::makeCatLink($one_cat, false, '', $brand_info['id'] );
							$links[$x]['priority'] = ".7";
							$x++;
							$brand_cats_count++;
						}
				} */

					if (!empty($sub_cats)){
						foreach ($sub_cats as $sub_cat){
							$links[$x]['url'] = Catalog::makeCatLink($sub_cat,false,'',0,false, $nossl);
							$links[$x]['priority'] = ".9";
							$x++;
							$sub_cats_count++;
						}
					}
				}
			}

			foreach( $links as $link )
			{
			    fprintf($fp, "<url><loc>%s</loc><priority>%s</priority></url>\n", $link['url'], $link['priority']);
			}

			unset($links);

			/*
            *****************************************
            ** PRODUCT LINKS
            *****************************************
            */
			$links = array();
			$x = 0;

			$start = 0;
			$limit = 400;

			//$products = Products::getModelList( false );

			do{
				$products = Products::get( 0, '', '', '', '', 0, 'Y', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', false, false, '', false, '', '', '', $limit, $start, false, 'N' );

				if ($show_message)
				{
				     echo '<p><b>Product Links ('.count($products).')</b><p>';
				}

				$product_option_count = 0;
				//Product SKUs
				//Product AKA SKUs
				//Product Option
				//Product Option AKAs
				if ($products){
					foreach ($products as $product){
						//if( $product['is_active'] != 'Y' ) SQL already limits this
						//	continue;
						$links[$x]['url'] = Catalog::makeProductLink_($product,false,false,'', $nossl);
						$links[$x]['priority'] = ".8";
						$links[$x]['image'] = Catalog::makeProductImageLink( $product['id'],false,false,false);
						$x++;
						
						if( $product['aka_sku'] )
						{
							$product['vendor_sku'] = $product['aka_sku'];
							$links[$x]['url'] = Catalog::makeProductLink_($product,false,false,'', $nossl);
							$links[$x]['priority'] = ".8";
							$x++;
						}


						/*THIS IS CAUSING A MEMORY FAILURE
						$options = Products::getProductOptions( 0, $product['id'], false, true, '', true, 0, 0, 'Y', 'N' );
						if( $options )
						{
							$product_option_count += count( $options );
							foreach( $options as $option )
							{
								$links[$x]['url'] = Catalog::makeProductLink_($option);
								$links[$x]['priority'] = ".8";
								$x++;

								if( $option['aka_sku'] )
								{
									$option['vendor_sku'] = $option['aka_sku'];
									$links[$x]['url'] = Catalog::makeProductLink_($option);
									$links[$x]['priority'] = ".8";
									$x++;
								}
							}
						}*/
					}
				}
				else {
					break;
				}

				$start += $limit;

			} while($products);

			if ($show_message)
			{
			     echo '<p><b>Product Option Links ('.$product_option_count.')</b><p>';
			}
			$link_counter = 0;
			foreach( $links as $link )
			{
			    fprintf($fp, "<url><loc>%s</loc><priority>%s</priority><image:image><image:loc>%s</image:loc></image:image></url>\n", $link['url'], $link['priority'], $link['image']);
			    $link_counter++;
			    if ($link_counter == 20000)
			    {
			    	fprintf($fp, "\n</urlset>");
			    	fclose($fp);
			    
			    	$fp = fopen($CFG->dirroot.'/'.$filename.'2.xml', 'w');

            		$rss = '<?xml version="1.0" encoding="UTF-8"'.'?'.">\r\n";
            		$rss .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\"
							xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">";
					fprintf($fp, $rss);
			    }
				if ($link_counter == 40000)
			    {
			    	fprintf($fp, "\n</urlset>");
			    	fclose($fp);
			    
			    	$fp = fopen($CFG->dirroot.'/'.$filename.'3.xml', 'w');

            		$rss = '<?xml version="1.0" encoding="UTF-8"'.'?'.">\r\n";
            		$rss .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\"
							xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">";
					fprintf($fp, $rss);
			    }
			    
			}

		    fprintf($fp, "\n</urlset>");
			fclose($fp);

            if ($show_message)
            {
			     echo "<p> Sitemap Updated Successfully!</p>\n";
            }
		}
			function getRandomContentPages($limit, $not_in_list)
		{
			$random_pages = Content::get(0,'','','Y', $limit, $not_in_list, true);
			return $random_pages;
		}
		static function get_set($table,$column)
		{
	    	$sql = "SHOW COLUMNS FROM $table LIKE '$column'";
    		if (!($ret = mysql_query($sql)))
        	die("Error: Could not show columns");

    		$line = mysql_fetch_assoc($ret);
    		$set	  = $line['Type'];
		    // Remove "set(" at start and ");" at end.
    		$set  = substr($set,5,strlen($set)-7);
		    // Split into an array.
    		return preg_split("/','/",$set); 
		}
	}

?>