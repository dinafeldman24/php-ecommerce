<?php 
class Wistia{
	
	function get_embed_code($video_id){
		
		global $CFG;
		
		$ch = curl_init();
		
		$options = urlencode(
						"http://home.wistia.com/medias/" . $video_id . "?".
							"embedType=seo"
							//."&width=560"
							//."&height=315"
							."&videoFoam=false"
						);
		
		curl_setopt($ch, CURLOPT_URL, $CFG->WistiaAPIEndpoint . "?url=" . $options);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		
		// Get response from the server.
		$httpResponse = curl_exec($ch);
		curl_close ($ch);

		$video_info = json_decode($httpResponse,true);
		
		return $video_info['html'];		
	}
	
}
?>