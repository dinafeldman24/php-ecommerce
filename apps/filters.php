<?php

class Filters {

	static function insert( $info )
	{
		//echo'hi';
		//print_ar( $info );
		//check if there is a filter by this name and if so return the filter id
		$filter = Filters::get( 0, $info['name'] );
		if( $filter )
		{
			echo'<h3> Filter by that name already found. Next time Choose the Filter from the Existing List.</h3>';
			return $filter[0]['id'];
		}

		return db_insert('filters', $info );
	}
	static function update( $id, $info, $cat_id )
	{
		//check if there is a different filter by this name and if so return the filter id
		$filter = Filters::get( 0, $info['name'] );
		if( $filter )
		{	
			if ($filter[0]['id'] != $id)
			{
				echo'<h3> Filter by that name already found--duplicating values.</h3>';
				// if already a filter by that name, update cat_filters, replacing the old $id with $filter[0]['id']
				// for the current $cat_id
				// Also, get all products that have a record with the old filter_id $id
				// and duplicate those records, but with the other filter id $filter[0]['id']
				$query1 = "UPDATE cat_filters SET filter_id = ".$filter[0]['id']." WHERE filter_id = $id
							AND cat_id = $cat_id";
				db_query($query1);
				
				$query2 = "REPLACE INTO product_filters (product_id, filter_id, value)
							SELECT product_id, '".$filter[0]['id']."', value FROM product_filters 
							WHERE filter_id = $id";
				db_query($query2);
			}
			else db_update('filters',$id,$info);
		}
		// create a new filter with the new name and duplicate the values
		else 
		{
			// creating a new filter and duplicating the values
			//db_update('filters',$id,$info);
			$new_filter_id = self::insert($info);		
			self::insertCatFilter( $new_filter_id, $cat_id);
			$query = "REPLACE INTO product_filters (product_id, filter_id, value)
							SELECT product_id, '".$new_filter_id."', value FROM product_filters 
							WHERE filter_id = $id";
			db_query($query);
			self::removeCatFilter($id, $cat_id);
		}
	}
	
	static function delete( $id )
	{
	    return db_delete( 'filters', $id );
	}

	static function insertCatFilter( $filter, $cat, $type='select', $display='Y' )
	{
		$info = array('filter_id' => $filter,
		'cat_id' => $cat,
		'type' => $type,
		'display' => $display );
		return db_insert( 'cat_filters', $info );
	}


	static function get( $id=0, $name='', $order='', $start=0, $limit=0, $in_specs='' )
	{
		$sql = " SELECT * FROM filters WHERE 1 ";
		if( $id )
			$sql .= " AND id = " .(int)$id;
		if( $name )
			$sql .= " AND UPPER(name) = UPPER('" . addslashes($name) ."') ";

		if( $order !== '' )
		{
			$sql .= " ORDER BY $order ";
		}

		if( $limit )
		{
			$sql .= " LIMIT $start, $limit ";
		}
		//echo $sql;
		return db_query_array( $sql );
	}
	static function get1($id)
	{
		$id = (int) $id;
		if (!$id) return false;
		$result = Filters::get($id);
		return $result[0];
	}

	static function get1ByName( $name )
	{
		$result = Filters::get(0,$name);
		return $result[0];
	}

	static function getFiltersForProduct($product_id = 0)
	{
	   $sql = "SELECT filters.*, product_filters.value as product_filter_value
	           FROM filters
	           LEFT JOIN product_filters ON filters.id = product_filters.filter_id
	           WHERE 1";

	   if ($product_id > 0)
	   {
	       $sql .= " AND product_filters.product_id = " . (int) $product_id;
	   }

	   return db_query_array($sql);
	}

	static function getFiltersForCat( $cat_id=0, $display=false )
	{
		//Redoing this function to get from the primary cat first, then from the tree minus any filters already found
		//This way if you modify a filter at the category level it will override inherted values

		if( !$cat_id )
			return false;

		if( is_array( $cat_id ) )
		{
			//What I need to do here is find the Main categories, then step down to find which category to go off of
			//For this application, there really should only be 1 main category, but in others there could be more

			//2 categories on the same lvl could screw up filters, in this app shouldn't happen

			//I suppose i should make a tree for each one, and merge them together, then get distinct filters

			$search_cats = Cats::getFilterSearchCats( $cat_id );
			//print_ar( $search_cats );
			$results = array();
			$primary_filters = array();
			foreach( $search_cats as $cat_id )
			{
				//First get the passed cat filters
				$sql = "SELECT cat_filters.filter_id, cat_filters.cat_id, cats.name as cat_name, filters.name as filter_name,
								cat_filters.type, cat_filters.display FROM cat_filters, cats, filters where cat_id = '$cat_id' and cat_filters.cat_id = cats.id
								and cat_filters.filter_id = filters.id ";
				if( $display )
					$sql .= " AND cat_filters.display = 'Y' ";
					//mail("rachel@tigerchef.com", "sql2", $sql);
				$main_results = db_query_array( $sql );
				if( $main_results )
				foreach( $main_results as $row )
				{
					$primary_filters[] = $row['filter_id'];
					$results[$row['filter_id']] = $row;
				}

				$tree = Cats::getCatsTreeIDs($cat_id);
				$sql =" SELECT distinct cat_filters.filter_id, cat_filters.cat_id, cats.name as cat_name, filters.name as filter_name,
							  cat_filters.type, cat_filters.display FROM cat_filters, cats, filters where cat_id IN ('".implode("','",$tree)."') and
							  cat_filters.cat_id = cats.id and cat_filters.filter_id = filters.id ";
				if( $display )
					$sql .= " AND cat_filters.display = 'Y' ";
				if( $main_results )
				{
					$sql .= " AND filter_id NOT IN (" .implode(',', $primary_filters).") ";
				}
				$tree_results = db_query_array( $sql );
				if( $tree_results )
				foreach( $tree_results as $row )
				{
					$results[$row['filter_id']] = $row;
				}
			}
		}
		else
		{
			//First get the passed cat filters
			$results = array();
			$primary_filters = array();

			$sql = "SELECT cat_filters.filter_id, cat_filters.cat_id, cats.name as cat_name, filters.name as filter_name,
							cat_filters.type, cat_filters.display FROM cat_filters, cats, filters where cat_id = '$cat_id' and cat_filters.cat_id = cats.id
							and cat_filters.filter_id = filters.id ";
			if( $display )
					$sql .= " AND cat_filters.display = 'Y' ";
					
			$main_results = db_query_array( $sql );
			//echo $sql;
			if( $main_results )
			foreach( $main_results as $row )
			{
				$primary_filters[] = $row['filter_id'];
				$results[$row['filter_id']] = $row;
			}
			
			//print_ar( $primary_filters );
			$tree = Cats::getCatsTreeIDs($cat_id);
			$sql =" SELECT distinct cat_filters.filter_id, cat_filters.cat_id, cats.name as cat_name, filters.name as filter_name,
						  cat_filters.type, cat_filters.display FROM cat_filters, cats, filters where cat_id IN ('".implode("','",$tree)."') and
						  cat_filters.cat_id = cats.id and cat_filters.filter_id = filters.id ";
			if( $display )
					$sql .= " AND cat_filters.display = 'Y' ";
			if( $main_results )
			{
				$sql .= " AND filter_id NOT IN (" .implode(',', $primary_filters).") ";
			}
			$tree_results = db_query_array( $sql );
			if( $tree_results )
			foreach( $tree_results as $row )
			{
				$results[$row['filter_id']] = $row;
			}
		}
		//$results_output = print_r($results, true);
//mail("rachel@tigerchef.com", "sql", $results_output);
		//echo $sql;
		return $results;
	}

	static function get1CatFilter( $cat_id, $filter_id )
	{
		$sql = " SELECT filters.name as filter_name, cat_filters.*, cats.name as cat_name from cat_filters ";
		$sql .= " LEFT JOIN filters on filters.id = cat_filters.filter_id ";
		$sql .= " LEFT JOIN cats on cats.id = cat_filters.cat_id ";
		$sql .= " where cat_filters.cat_id = $cat_id and
						 cat_filters.filter_id = $filter_id  ";

		//echo $sql;
		$results = db_query_array( $sql );
		return $results[0];
	}


	static function getFilterSelect( $name='filter', $msg='Select a Filter', $selected_filter='' )
	{
		$filters = Filters::get(0,'', 'name');

		$ret = '<select size="1" name="' . $name . '" id="filter_select">';
		$ret .= '<option value="0">' . $msg .'</option>';
		if( is_array( $filters ) )
		foreach( $filters as $filter )
		{
			$ret .= '<option value="' . $filter['id'] . '"';

			if( $selected_filter == $filter['id'] )
			{
			    $ret .= " SELECTED ";
			}

			$ret .= '>' . $filter['name'] . '</option>';
		}
		$ret .= "</option>";

		return $ret;
	}
		static function getFilterSelectByName( $name='filter', $msg='Select a Filter', $selected_filter='' )
	{
		$filters = Filters::get(0,'', 'name');

		$ret = '<select size="1" name="' . $name . '" id="filter_select">';
		$ret .= '<option value="0">' . $msg .'</option>';
		if( is_array( $filters ) )
		foreach( $filters as $filter )
		{
			$ret .= '<option value="' . $filter['name'] . '"';

			if( $selected_filter == $filter['name'] )
			{
			    $ret .= " SELECTED ";
			}

			$ret .= '>' . $filter['name'] . '</option>';
		}
		$ret .= "</option>";

		return $ret;
	}
	static function removeCatFilter( $filter, $cat )
	{
		$sql = "delete from cat_filters where cat_id = $cat and filter_id = $filter ";
		db_query( $sql );

		$sql = "delete from cat_filter_ranges where cat_id = $cat and filter_id = $filter ";
		return db_query( $sql );
	}

	static function outputSearch( $filter, $prod_cat, $sel_value )
	{
		$filter_id = $filter['filter_id'];
		$main_cat = $filter['cat_id'];
		//print_ar( $_REQUEST );
//		$filter = Filters::get1( $filter_id );
		$ret = '';
		switch( $filter['type'] )
		{
			//On the consumer side both of these will be the same
			case 'range':
			$ranges = Filters::getCatFilterRanges( $main_cat, $filter_id, true, $prod_cat );
			//print_ar( $ranges );
			unset( $ranges['custom_ranges'] );
				echo'<input type="hidden" name="extra_filters[]" value="' . $filter['filter_id'] . '" />';
				echo'<td>' . $filter['filter_name'] . '</td>';
				echo'<td>';
				echo'<select name="filters[filter_' . $filter_id . ']" size="1">';
				echo'<option value="">All</option>';
				foreach( $ranges as $range )
				{
					//print_ar( $range );
					echo'<option value="' . $range['id'] .'" ';
					if( $range['id'] == $sel_value )
						echo 'SELECTED';
					echo ' >' . $range['start_val'] . ' - ' . $range['end_val'] .'</option>';
				}
				echo'</select>';
				echo'</td>';
			break;

			case 'select':
			$values = Filters::getCatSelectValues( $filter_id, $prod_cat, $extra_filters, $filter_query,$main_cat, $cats );
			//&& !$extra_filters[$filter_id]
				echo'<input type="hidden" name="extra_filters[]" value="' . $filter['filter_id'] . '" />';
				echo'<td>' . $filter['filter_name'] . '</td>';
				echo'<td>';
				echo'<select name="filters[filter_' . $filter_id . ']" size="1">';
				echo'<option value="">All</option>';
				if( is_array( $values ) )
					foreach( $values as $val )
					{
						//print_ar( $opt );
						echo'<option value="' . $val['value'] .'" ';
						if( $val['value'] == $sel_value && $val['value'] != "" )
							echo 'SELECTED';
						echo' >' . $val['value'] .'</option>';
					}
				echo'</select>';
				echo'</td>';
			break;
			case 'select options':
			$options = Filters::getCatFilterOptions( $main_cat, $filter_id, false, $prod_cat );
				//print_ar( $values );
				//print_ar( $options );
				echo'<input type="hidden" name="extra_filters[]" value="' . $filter['filter_id'] . '" />';
				echo'<td>' . $filter['filter_name'] . '</td>';
				echo'<td>';
				echo'<select name="filters[filter_' . $filter_id . ']" size="1">';
				echo'<option value="">All</option>';
				foreach( $options as $opt )
				{
					if( $opt['value'] == $sel_value && $opt['value'] != '' )
					{
						$selected = "SELECTED";
					}
					else
						$selected = "";
					//print_ar( $opt );
					echo'<option value="' . $opt['value'] .'" ' . $selected .'>' . $opt['value'] .'</option>';
				}
				echo'</select>';
				echo'</td>';
			break;
		}
		return $ret;
	}

	static function getDistinctValues( $filter_id='', $order='', $value='')
	{
		$sql = " SELECT distinct value, filters.name FROM product_filters 
		          LEFT JOIN filters ON product_filters.filter_id = filters.id WHERE 1";
				  
     if( $filter_id > 0)				  
				  $sql .=" AND filter_id = $filter_id ";
			  
	 if($value){
		         $sql .=" AND value = '".$value."'";
	 }		  		  
	
	if( $order )
		$sql .= " ORDER BY $order";
	   
	return db_query_array( $sql );
	
	}


	static function buildQueryString( $filters, $values, $cat_id, $backend = false, $cat_filters='' )
	{
		$query = '';
		$ret = '';

		//if( !is_array( $values ) )
		//	$values = array( $values );
		if( !is_array( $cat_filters ) )
			$cat_filters = Filters::getFiltersForCat( $cat_id );
			$cf_output = print_r($cat_filters, true);
			$f_output = print_r($filters, true);

		foreach( $cat_filters as $cf )
		{
			if( in_array( $cf['filter_id'], $filters ) )
			{
				$f = Filters::get1CatFilter( $cf['cat_id'], $cf['filter_id'] );
				$f2 = print_r($f, true);
				
					//print_ar( $f );
					//print_ar( $values );
				switch( $f['type'] )
				{
					case 'range':

					if( $values['filter_' . $f['filter_id']] )
					{
						$ranges = Filters::getRanges( $values['filter_' . $f['filter_id']] );
						$first = true;
						if($ranges){
							$query .= " AND ( ";
							foreach( $ranges as $range )
							{
								if( $first )
									$first = false;
								else
									$query .= ' OR ';
								if( trim(strtoupper($f['filter_name'])) == 'WIDTH' ||
								    trim(strtoupper($f['filter_name'])) == 'HEIGHT' ||
								    trim(strtoupper($f['filter_name'])) == 'LENGTH' ||
								    trim(strtoupper($f['filter_name'])) == 'PRICE')
								{
									$name = strtolower($f['filter_name']);

									$option_name = strtolower( $f['filter_name'] );

									if( trim(strtoupper($f['filter_name'])) == 'PRICE' )
									    $option_name = 'additional_price';
									//Need to handle above and below ranges
									if( !$range['start_value'] )
										$range['start_value'] = 0;
									if( !$range['end_value'] )
										$range['end_value'] = 9999;

									// Fix Bad Data (i.e. convert to number/decimal)
//									if(!is_numeric($range['start_value'])){
//										$range['start_value'] = preg_replace("/[^0-9.]/",'',$range['start_value']);
//									}
//									if(!is_numeric($range['end_value'])){
//										$range['end_value'] = preg_replace("/[^0-9.]/",'',$range['end_value']);
//									}


									$query .= ' ( products.' . $name . ' >= \''. db_esc($range['start_value']) . "' ";
									$query .= ' AND products.'.$name.' <= \''. db_esc($range['end_value']) . '\' ) ';

									$query .= ' OR ( product_options.' . $option_name . ' >= \''. db_esc($range['start_value']). "' ";
									$query .= ' AND product_options.'.$option_name.' <= \''. db_esc($range['end_value']) . "' ";

									$query .= ' ) ';
								}
								else
								{
									$start_found = false;
										$query .= " ( ";
										if( $range['start_value'] > 0 )
										{
											$query .= ' filter' . $f['filter_id'] .'.value >= \''. db_esc($range['start_value']). "' ";
											$start_found = true;
										}

										if( $range['end_value'] > 0 )
										{
											if( $start_found )
												$query .= ' AND filter' . $f['filter_id'] .'.value <= \''. db_esc($range['end_value']) . "' ";
											else
												$query .= ' filter' . $f['filter_id'] .'.value <= \''. db_esc($range['end_value']) . "' ";
										}

										$query .= ' ) ';
								}
							}
							$query .= " ) ";
						}
					}

					break;
					case 'select':
					case 'select options':
					default:

					if( trim(strtoupper($f['filter_name'])) == 'WIDTH' ||
					    trim(strtoupper($f['filter_name'])) == 'HEIGHT' ||
					    trim(strtoupper($f['filter_name'])) == 'LENGTH' ||
					    trim(strtoupper($f['filter_name'])) == 'PRICE')
					{
						if( trim(strtoupper($f['filter_name'])) == 'WIDTH' ||
					    trim(strtoupper($f['filter_name'])) == 'HEIGHT' ||
					    trim(strtoupper($f['filter_name'])) == 'LENGTH')
					    {
							$name = "display_".strtolower($f['filter_name']);
					    	$option_name = "display_".strtolower( $f['filter_name'] );
					    }

						if( trim(strtoupper($f['filter_name'])) == 'PRICE' ){
							$name = strtolower($f['filter_name']);
						    $option_name = 'additional_price';
						}

					    if(is_array($values["filter_$f[filter_id]"])){
					    	$values["filter_$f[filter_id]"] = array_map("db_esc", $values["filter_$f[filter_id]"]);
							$query .= ' AND (( products.' . $name .
								" IN ('" .
								implode( "','", $values["filter_$f[filter_id]"]) ."') ) ";

							$query .= " OR ( product_options.$option_name IN ('"
							 . implode( "','",$values["filter_$f[filter_id]"]) ."') ";

							$query .= ' ) )';
					    }
//var_dump($values);

					}
					else {

					    if( $values['filter_' . $f['filter_id']] )
					    {
						    if( $backend )
						    {
							    if( !is_array($values['filter_' . $f['filter_id']]) )
								    $values['filter_' . $f['filter_id']] = array($values['filter_' . $f['filter_id']]);
						    }
						    //$query .= ' AND filter' . $f['filter_id'] .'.value IN ("' . implode( '","',$values['filter_' . $f['filter_id']]) .'") ';
						    $query .= ' AND filter' . $f['filter_id'] .'.value IN ("' . implode( '","',array_map('db_esc', $values['filter_' . $f['filter_id']])) .'") ';
						    
						    //$ret .= '<input type="hidden" name="filter_' . $f['id'] .'" value="' . $values['filter_' . $f['id']] . '" />';
					    }
					}
					break;
				}
			}
		}
		$val = array( 'query' => $query, 'ret' => $ret );

		return $val;
	}

	static function displayFrontCatFilter( $filter_id, $main_cat, $vars, $prod_cat, $filter_query, $cats, $cat_filters, $manufacturer = 0, $item_keyword='', $min_price = '', $max_price = '' )
	{
		//$always_show = true;
		$always_show = false;
		$display_filter = $always_show;
		$filter = Filters::get1CatFilter($main_cat, $filter_id );
		 
		
		if( $vars['extra_filters'] )
		foreach( $vars['extra_filters'] as $f )
		{
			$extra_filters[$f] = $vars['filters']['filter_' . $f];
		}
		
		else
		$extra_filters= array();

		//Need to check what type of filter it is, and display the right type
		//print_ar( $filter );
		if ($filter['type'] != 'range')
		{
			$values = Filters::getCatSelectValues( $filter_id, $prod_cat, $extra_filters, $filter_query,$main_cat, $cats, $cat_filters, $manufacturer, $item_keyword, $min_price, $max_price);			
			/*if (count($values) >= 14) $filter['type'] = 'select options';
			else */$filter['type'] = 'select';
		}					
		switch( $filter['type'] )
		{			
			//On the consumer side both of these will be the same
			case 'range':

			$ranges = Filters::getCatFilterRanges( $main_cat, $filter_id, true, $prod_cat );

			$values = Filters::getCatRangeValues( $filter_id, $prod_cat, $extra_filters, $filter_query, $main_cat, $cats, $ranges['custom_ranges'], $cat_filters );
			unset( $ranges['custom_ranges'] );
		//	echo' range ' . $main_cat . ' ' . $prod_cat . ' ' . $filter_id . ' <br />';
		//		print_ar( $ranges );
		//	echo' values ' . $main_cat . ' ' . $prod_cat . ' ' . $filter_id . ' <br />';
		//		print_ar( $values );
			if( !is_array( $values ) )
			$values = array();
			$display_filter = $always_show;
			
			foreach( $values as $v )
			{
				if( $ranges )
				foreach( $ranges as $key => $range )
				{
					if( $range['start_value'] == $v['start_value']  &&  $range['end_value'] == $v['end_value'])
						$ranges[$key]['count'] = $v['count'];
				}
				if( $ranges[$key]['count'] )
				{
					//echo $filter['filter_name'];
					//print_ar( $v );
					$display_filter = true;
				}
			}
			if(is_array($ranges) && count($ranges) && $display_filter ){

					$displayCat = false;
					ob_start();

					if( $filter['display'] == 'S' )
				{
					// if none of the results at all have this filter, don't even display the
					// filter heading 
					if (count($values) == 1 && $values[0]['value'] == '') continue;
				    ?>
					<h4><?=$filter['filter_name']?></h4>
					<fieldset>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />

					    <select name="filters[filter_<?=$filter_id?>][]" id="filter_<?=$filter_id?>" onchange="checkUpdate('filter_<?=$filter_id?>', '<?=$filter_id?>');">
						<option value="">All</option>
					<?
					$count = 1;
					foreach( $ranges as $v )
					{
						if ( $v['count'] == 0 || $v['value']=='') continue; //Karen Kerr - don't display facets without values
						//if ( $v['count'] == 0) continue; //RSunness added bec. not want to display if no results for that option
						//print_ar( $v );
						//print_ar( $extra_filters );
						if( is_array( $extra_filters[$filter['filter_id']] ))
							if( in_array( $v['id'], $extra_filters[$filter['filter_id']] ) )
								$checked = 'SELECTED';
							else
								$checked = '';
						else
						$checked ='';
						//print_ar($extra_filters );
						?>

						<option value="<?=$v['id']?>" <?=$checked?>>

						<?
						if( !$v['start_value'] || !$v['end_value'] )
						{
						    if( $v['start_value'] )
						    {
							    echo $filter['prefix'] ." " . $v['start_value'] . " " .$filter['suffix'] . ' and above ';
						    }
						    else
						    {
							    echo $filter['prefix'] ." " . $v['end_value'] . " " . $filter['suffix'] . ' and below ';
						    }

						}
						else
						{
							echo $filter['prefix'] . ' ' . $v['start_value'] . ' ' . $filter['suffix'] . ' - ' . $filter['prefix'] . ' ' . $v['end_value'] . ' ' . $filter['suffix'];
						}

						?> (<? if ( $v['count'] ){
							echo $v['count'];
							$displayCat = true;
						}
							 else
							 	echo'0';
						?>)
						</option>
						<?
						$count++;
					}

					?>
						    </select>
					</fieldset>

				    <?

				}
				else {
					?>
					<h4><?=$filter['filter_name']?></h4>
					<fieldset>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />

					<?
					$count=1;
					foreach( $ranges as $v )
					{
						//print_ar( $v );
						//print_ar( $extra_filters );
						if ( $v['count'] == 0 || $v['value']=='') continue; //RSunness added bec. not want to display if no results for that option
						if( is_array( $extra_filters[$filter['filter_id']] ))
							if( in_array( $v['id'], $extra_filters[$filter['filter_id']] ) )
								$checked = 'checked';
							else
								$checked = '';
						else
						$checked ='';
						//print_ar($extra_filters );
						?>

						<label for="filter_<?=$filter_id?>_<?=$count?>" onclick="checkUpdate('filter_<?=$filter_id?>_<?=$count?>', '<?=$filter_id?>');">
						<?
						if( $v['count'] > 0 )
						{
						?>
							<input id="filter_<?=$filter_id?>_<?=$count?>" name="filters[filter_<?=$filter_id?>][]" type="checkbox" value="<?=$v['id']?>"  <?=$checked?> />
						<?
						}
						else
						{
							?>
							<input id="filter_<?=$filter_id?>_<?=$count?>" disabled name="filters[filter_<?=$filter_id?>][]" type="checkbox" value="<?=$v['id']?>" <?=$checked?> />
							<?
						}
						//If start value is 0 need to do a value and below, if end is 0 need a value and above
						?>

						<?
						if( !$v['start_value'] || !$v['end_value'] )
						{
						    if( $v['start_value'] )
						    {
							    echo $filter['prefix'] ." " . $v['start_value'] . " " .$filter['suffix'] . ' and above ';
						    }
						    else
						    {
							    echo $filter['prefix'] ." " . $v['end_value'] . " " . $filter['suffix'] . ' and below ';
						    }

						}
						else
						{
							echo $filter['prefix'] . ' ' . $v['start_value'] . ' ' . $filter['suffix'] . ' - ' . $filter['prefix'] . ' ' . $v['end_value'] . ' ' . $filter['suffix'];
						}

						?> <span>(<? if ( $v['count'] ){
							echo $v['count'];
							$displayCat = true;
						}
							 else
							 	echo'0';
						?>)</span></label>
						<?
						$count++;
					}

					?>
					</fieldset>
					<?
				}
					$filter = ob_get_contents();
					$displayCat = true;
					ob_end_clean();
					if($displayCat){
						echo $filter;
					}
			}
			break;

			case 'select':

			//&& !$extra_filters[$filter_id]
			$display_filter = $always_show;

			if( $values )
				foreach( $values as $v )
				{
					if( $v['count'] )
					    $display_filter = true;
				}
			if(is_array($values) && count($values) && $display_filter ){

				if( $filter['display'] == 'S' )
				{
					// if none of the results at all have this filter, don't even display the
					// filter heading 
					if (count($values) == 1 && $values[0]['value'] == '') continue;
				    ?>
					<h4><?=$filter['filter_name']?></h4>
					<fieldset>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />

					    <select name="filters[filter_<?=$filter_id?>][]" id="filter_<?=$filter_id?>" onchange="checkUpdate('filter_<?=$filter_id?>', '<?=$filter_id?>');">
					<?
					$count = 1;
					foreach( $values as $v )
					{
						if ( $v['count'] == 0 || $v['value']=='') continue; //RSunness added bec. not want to display if no results for that option
						//						print_ar( $v );
						if( is_array( $extra_filters[$filter['filter_id']] ))
						    if( in_array( $v['value'], $extra_filters[$filter['filter_id']] ) )
						    $checked = 'selected="selected"';
							else
						    $checked = '';
						else
						    $checked ='';
						?>


					<?
					if( $v['count'] > 0 )
					{
						?>
						    <option id="filter_<?=$filter_id?>_<?=$count?>" value="<?=$v['value']?>" <?=$checked?> >
						<?
						?>
						<?=$filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']?> (<? if ( $v['count'] )
							    echo $v['count'];
							 else
							 echo'0';
						?>) </option>
						<?
						$count++;
					}


					}

					?>
						    </select>
					</fieldset>

				    <?
				    break;
				}
								// THIS IS THE MAIN SECTION THAT NEEDS EDITING AS OF FEB '13	
				// if none of the results at all have this filter, don't even display the
					// filter heading 
					if (count($values) == 1 && $values[0]['value'] == '') continue;
					usort($values, function($v1, $v2) { return strnatcmp($v1['value'], $v2['value']); });
					$filters_key = "filter_".$filter_id;
					$filters_key_array = "filters[".$filters_key."][]";															
					foreach( $values as $v )
					{
						if ( $v['count'] == 0 || $v['value'] == '') continue;
						else if ((is_array( $extra_filters[$filter['filter_id']] ) && in_array( $v['value'], $extra_filters[$filter['filter_id']] ))) 
						{
							
							$top_text .= "<div class='fieldset_div_x'><label class='x'><a href='#' onClick=\"undoFilterSelection('$filters_key'); return false;\">[X]</a> ".$filter['filter_name'] . ":" . $filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']."</label></div>";
						?>
					<input type='hidden' name='<?=$filters_key_array?>' id='<?=$filters_key?>' value='<?=$v['value']?>'/>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />										
					<?																			
						}
						else $values2[] = $v;
					}
					if (count($values2) >= 1)					
					{					
						
						?>
					<h4><?=$filter['filter_name']?></h4>
					<div class='fieldset_div'>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />
					<?
					if( trim(strtoupper($filter['filter_name']) ) == 'WIDTH'  ||  trim(strtoupper($filter['filter_name']) ) == 'HEIGHT' || trim(strtoupper($filter['filter_name']) ) == 'LENGTH') {$filter['suffix'] = "&quot;";}; 
						foreach( $values2 as $v )
						{
						if ( $v['count'] == 0 || $v['value'] == '') continue; //RSunness added bec. not want to display if no results for that option
						if( is_array( $extra_filters[$filter['filter_id']] ))
						if( in_array( $v['value'], $extra_filters[$filter['filter_id']] ) )
						$checked = 'checked';
						else
						$checked = '';
						else
						$checked ='';
												
						?>

						<label><a href='#' onClick='updateResultsNew("filters[<?=$filters_key?>][]", "<?= strtr($v['value'], array('\\' => '\\\\', "'" => "\\'", '"' => '\\"', 
                                 "\r" => '\\r', "\n" => '\\n' ));?>");return false;'><?=$filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']?></a>						
						 <span>(<? if ( $v['count'] )
									echo $v['count'];
							 else
							 	echo'0';
						?>)</span> </label>
						<?						
						}
						?>
					</div>
					<?
					}								
					// END MAIN SECTION THAT NEEDS EDITING
			}
			break;
			case 'select options':

			//$options = Filters::getCatFilterOptions( $main_cat, $filter_id, false, $prod_cat );
			$options = $values;
			$filter['display'] = 'S';
				//print_ar( $values );
				//print_ar( $options );
			if( !is_array( $values ) )
				$values = array();

			$display_filter = $always_show;
			if( $values )
				foreach( $values as $v )
				{
					if( $v['count'] > 0 && $v['value'] != "")
						//print_r($v);
						$display_filter = true;
				}
				
			foreach( $values as $v )
			{
				if ( $v['count'] == 0) continue; //RSunness added bec. not want to display if no results for that option
				if( $options )
				{
					//print_r($options);
				foreach( $options as $key => $o )
				{
					if( rtrim(strtoupper($o['value']), "\"") == strtoupper($v['value']) )
					{
						$options[$key]['count'] = $v['count'];
						//echo 'match ' . $key . ' ' . $v['count'] . '<br />';
						//echo $o['value']  . ' == ' . $v['value'] . '<br />';
					}

				}
				if($options[$key]['count'] )
					$display_filter = true;
				}
			}
			if(is_array($options) && count($options) && $display_filter ){

			    if( $filter['display'] == 'S' )
				{
					// if none of the results at all have this filter, don't even display the
					// filter heading 
					if (count($values) == 1 && $values[0]['value'] == '') continue;
					?>
					<h4><?=$filter['filter_name']?></h4>
					<fieldset>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />

					    <select name="filters[filter_<?=$filter_id?>][]" id="filter_<?=$filter_id?>" onchange="checkUpdate('filter_<?=$filter_id?>', '<?=$filter_id?>');">
						<option value="">All</option>
					<?
					$count = 1;
					foreach( $options as $v )
					{
						if ( $v['count'] == 0 || $v['value']=='') continue;
						//print_ar( $v );
						if( is_array( $extra_filters[$filter['filter_id']] ))
						if( in_array( $v['value'], $extra_filters[$filter['filter_id']] ) )
						$checked = 'selected="selected"';
						else
						$checked = '';
						else
						$checked ='';
						//print_ar($extra_filters );
						?>


						<?
						if( $v['count'] > 0 )
						{
						?>
						<option id="filter_<?=$filter_id?>_<?=$count?>"  value='<?=$v['value']?>'  <?=$checked?> >
						<?
						}
						else
						{
						    continue;
							?>
							<option id="filter_<?=$filter_id?>_<?=$count?>" value='<?=$v['value']?>'  <?=$checked?> >
							<?
						}
						?>
						<?=$filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']?> (<? if ( $v['count'] )
									echo $v['count'];
							 else
							 	echo'0';
						?>)</option> <?
						$count++;
					}

					?>
					    </select>
					</fieldset>
					<?
				    break;
				}

				// if none of the results at all have this filter, don't even display the
				// filter heading 
				if (count($values) == 1 && $values[0]['value'] == '') continue;
				?>
					
					<h4><?=$filter['filter_name']?></h4>
					<fieldset>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />

					<?
				$count = 1;
				if( $options )
					foreach( $options as $v )
					{
						if( $v['count'] )
							$display_filter = true;
					}
					foreach( $options as $v )
					{
						if ( $v['count'] == 0 || $v['value']=='') continue; //RSunness added bec. not want to display if no results for that option
						//print_ar( $v );
						if( is_array( $extra_filters[$filter['filter_id']] ))
						if( in_array( $v['value'], $extra_filters[$filter['filter_id']] ) )
						$checked = 'checked';
						else
						$checked = '';
						else
						$checked ='';
						//print_ar($extra_filters );
						?>

						<label for="filter_<?=$filter_id?>_<?=$count?>" onclick="checkUpdate('filter_<?=$filter_id?>_<?=$count?>', '<?=$filter_id?>');">
						<?
						if( $v['count'] > 0 )
						{
						?>
							<input id="filter_<?=$filter_id?>_<?=$count?>" name="filters[filter_<?=$filter_id?>][]" type="checkbox" value='<?=$v['value']?>'  <?=$checked?> />
						<?
						}
						else
						{
							?>
							<input id="filter_<?=$filter_id?>_<?=$count?>" disabled name="filters[filter_<?=$filter_id?>][]" type="checkbox" value='<?=$v['value']?>'  <?=$checked?> />
							<?
						}
						?>
						<?=$filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']?> <span>(<? if ( $v['count'] )
									echo $v['count'];
							 else
							 	echo'0';
						?>)</span></label>
						<?
						$count++;
					}

					?>
					</fieldset>
					<?
			}
			break;
		}
		return $top_text;
	}

	static function getCatSelectValues( $filter_id, $cat_id, $vars, $filter_query, $filter_cat, $cats, $cat_filters='', $manufacturer=0, $item_keywords='', $min_price = '', $max_price = '' )
	{
		unset( $vars['cat_id'] );
		//print_ar( $cats );
		//print_ar( $vars );
	//	$cats = array();
	//	$cats[] = $cat_id;
	//	echo $cat_id;
	//	echo'get cat select value <br />';
	//	$cats = Cats::getAllSubs( $cat_id, $cats );
	//	echo'end<br/>';
		//Need counts of all products that match these vars grouped by filter values
		//echo 'getting for filter id = ' . $filter_id . ' <br />';


		$filter = Filters::get1( $filter_id );

		if( trim(strtoupper($filter['name']) ) == 'WIDTH'  ||
		    trim(strtoupper($filter['name']) ) == 'HEIGHT' ||
		    trim(strtoupper($filter['name']) ) == 'LENGTH'  ||
		    trim(strtoupper($filter['name']) ) == 'PRICE'  )
		{
			if( trim(strtoupper($filter['name']) ) == 'WIDTH'  ||  trim(strtoupper($filter['name']) ) == 'HEIGHT' || trim(strtoupper($filter['name']) ) == 'LENGTH')
			{
				$sql = " SELECT count( distinct products.id) as count, products.display_" . trim(strtolower( $filter['name'] )) . " as value";
			} 
		    else $sql = " SELECT count( distinct products.id) as count, products." . trim(strtolower( $filter['name'] )) . " as value";

		    $sql .= " FROM products ";
		}
		else {
		    $sql = " SELECT count( distinct products.id) as count, product_filters.value ";

		    $sql .= " FROM products ";
		    $where .= " AND product_filters.value IS NOT NULL ";
		}

		$join = " LEFT JOIN product_filters ON product_filters.product_id = products.id AND product_filters.filter_id = $filter_id ";
		$join .= " LEFT JOIN product_cats ON product_cats.product_id = products.id ";
		$join .= " LEFT JOIN cats ON cats.id = product_cats.cat_id ";
		$join .= " LEFT JOIN brands ON brands.id = products.brand_id ";
		$join .= " LEFT JOIN product_options on product_options.product_id = products.id ";

		$where = " WHERE 1 ";
		$old_catid = $cat_id;
		
		if( !is_array( $cat_id ) )
		{			
			$symbolic_subs = Cats::getSymbolicSubs($cat_id);
			$cat_id = array( $cat_id );
			if ($symbolic_subs)
			{
				foreach ($symbolic_subs as $key=>$value)
				{
					$cat_id[] = $key;
				}
			}
		}
		$where .= " AND product_cats.cat_id IN ('" . implode('\',\'', $cat_id) . "') ";

		$where .= " AND products.is_active = 'Y' ";
		$where .= " AND products.is_deleted = 'N' ";

		if ($manufacturer > 0) {			
			$where .= " AND products.brand_id = ".(int)$manufacturer;
		}
		if ($min_price !='' && $max_price != '') {			
			$where .= " AND products.price BETWEEN ".$min_price ." AND ".$max_price;
		}
		if (trim($item_keywords) != '') {
			$item_keywords = mysql_real_escape_string($item_keywords);
			$fields = Array('products.name','products.description','products.vendor_sku','brands.name','cats.name','product_options.vendor_sku', 'products.id', 'products.aka_sku');
			$where .= " AND " . db_split_keywords($item_keywords,$fields,'AND',true);
		}	
			
		$where .= $filter_query;
		//print_r( $vars );
		
		if( !is_array( $cat_filters ) )
			$cat_filters = Filters::getFiltersForCat( $cat_id );
			
		
		if( count( $vars ) )
		{
			foreach( $vars as $key => $value )
			{
				if( $value == '' )
					continue;
				if( trim(strtoupper($cat_filters[$key]['filter_name'])) == 'WIDTH'  ||
				    trim(strtoupper($cat_filters[$key]['filter_name'])) == 'HEIGHT' ||
				    trim(strtoupper($cat_filters[$key]['filter_name'])) == 'LENGTH'  ||
				    trim(strtoupper($cat_filters[$key]['filter_name'])) == 'PRICE'  )
				{

					if( !is_array( $value ) )
							$value = array( $value );
					if( trim(strtoupper($cat_filters[$key]['filter_name'])) == 'WIDTH'  || trim(strtoupper($cat_filters[$key]['filter_name'])) == 'HEIGHT' || trim(strtoupper($cat_filters[$key]['filter_name'])) == 'LENGTH')
					{
						$name = "display_".trim(strtolower($cat_filters[$key]['filter_name']));
						$option_name = "display_".trim(strtolower($cat_filters[$key]['filter_name']));
					}
					
					if( trim(strtoupper($cat_filters[$key]['filter_name'])) == 'PRICE' )
					{
						$name = trim(strtolower($cat_filters[$key]['filter_name']));
					    $option_name = 'additional_price';
					}
					//$where .= " AND products.$name IN (\"" . implode('","', $value) . "\")";

				}
				else
				{

					//echo 'key = ' . $key;

					if( !is_array( $value ) )
							$value = array( $value );
					if( $cat_filters[$key]['type'] !='range' )
					{
						//Think i have to left join the specific ones
						//key will be the filter id
						$join .= " LEFT JOIN product_filters as filter$key ON filter$key.product_id = products.id AND filter$key.filter_id = $key and filter$key.value IN (\"" . implode('","', array_map('db_esc',$value)) . "\")";
					}
					else
					{
						$join .= " LEFT JOIN product_filters as filter$key ON filter$key.product_id = products.id AND filter$key.filter_id = $key ";
					}
				}
			}
		}

		$group = " GROUP BY value ";

		$query = $sql . $join . $where . $group;
		//echo $query . '<br /><br />';
		
		return db_query_array( $query );
	}

	static function getCatRangeValues( $filter_id, $cat_id, $vars, $filter_query, $filter_cat, $cats, $custom_ranges=false, $cat_filters )
	{
		unset( $vars['cat_id'] );
		
		//print_ar( $vars );
		//$cats = array();
		//$cats[] = $cat_id;
		//	echo $cat_id;
		//$cats = Cats::getAllSubs( $cat_id, $cats );
		//Need counts of all products that match these vars grouped by filter values
		if( !is_array( $cat_filters ) )
			$cat_filters = Filters::getFiltersForCat( $cat_id );

		if(	trim(strtoupper($cat_filters[$filter_id]['filter_name'])) == 'WIDTH' ||
			trim(strtoupper($cat_filters[$filter_id]['filter_name'])) == 'HEIGHT' ||
			trim(strtoupper($cat_filters[$filter_id]['filter_name'])) == 'LENGTH' ||
			trim(strtoupper($cat_filters[$filter_id]['filter_name'])) == 'PRICE' )
		{
			$range_cat = ($custom_ranges)?$cat_id:$cat_filters[$filter_id]['cat_id'];
			
			$name = strtolower($cat_filters[$filter_id]['filter_name']);
			$option_name = strtolower($cat_filters[$filter_id]['filter_name']);

			if( trim(strtoupper($cat_filters[$filter_id]['filter_name'])) == 'PRICE' )
			{
			    $option_name = 'additional_price';
			}

			$cats = array( $range_cat );
			
			$sql = " SELECT count( distinct products.id) as count, products.$name, cat_filter_ranges.start_value, cat_filter_ranges.end_value FROM products ";

			$join .= " LEFT JOIN product_cats ON product_cats.product_id = products.id ";
			$join .= " LEFT JOIN cats ON cats.id = product_cats.cat_id ";
			$join .= " LEFT JOIN product_options on product_options.product_id = products.id ";
			$join .= " LEFT JOIN cat_filter_ranges on $filter_id = cat_filter_ranges.filter_id AND
									" . $range_cat . " = cat_filter_ranges.cat_id AND (
								products.$name >= cat_filter_ranges.start_value AND products.$name <= cat_filter_ranges.end_value ";
			$join .= " OR product_options.$option_name >= cat_filter_ranges.start_value AND product_options.$option_name <= cat_filter_ranges.end_value ";
			$join .= " ) ";

			$where = " WHERE 1 ";
			$where .= " AND product_cats.cat_id IN ('" . implode('\',\'', $cats) . "') ";
			$where .= " AND ( ( products.is_active = 'Y'  ) ";
			$where .= " OR ( product_options.is_active = 'Y'  ) ) ";

			$where .= $filter_query;
			//I need to somehow have multiple values for these dimensions. The best solution would be to add dimensions to the options
			//But then I need to join in the options here, or run another query
		}
		else
		{
			$sql = " SELECT count( distinct products.id) as count, product_filters.value, product_filters.filter_id, cat_filter_ranges.start_value, cat_filter_ranges.end_value FROM products ";
			$join = " LEFT JOIN product_filters ON product_filters.product_id = products.id AND product_filters.filter_id = $filter_id ";
			$join .= " LEFT JOIN product_cats ON product_cats.product_id = products.id ";
			$join .= " LEFT JOIN cats ON cats.id = product_cats.cat_id ";
			$join .= " LEFT JOIN cat_filter_ranges on product_filters.filter_id = cat_filter_ranges.filter_id AND

				    CAST(product_filters.value as signed) >= CAST(cat_filter_ranges.start_value as signed) AND
				     ( CAST(product_filters.value as signed) <= CAST(cat_filter_ranges.end_value as signed) OR cat_filter_ranges.end_value = 0 )
				     AND cat_filter_ranges.cat_id = " . (int) $filter_cat;

			$join .= " LEFT JOIN product_options on product_options.product_id = products.id ";
			$where = " WHERE 1 ";
			$where .= " AND product_cats.cat_id IN ('" . implode('\',\'', $cats) . "') ";
			$where .= " AND product_filters.value IS NOT NULL ";
			$where .= " AND ( ( products.is_active = 'Y'  ) ";
			$where .= " OR ( product_options.is_active = 'Y'  ) ) ";
			$where .= $filter_query;
		}


		//print_ar( $cat_filters );
		if( count( $vars ) )
		{
			//print_ar( $vars );
			foreach( $vars as $key => $value )
			{
				if( $value == '' )
					continue;

					//echo 'key = ' . $key;

					if( !is_array( $value ) )
							$value = array( $value );
					if( $cat_filters[$key]['type'] !='range' )
					{
						//key will be the filter id

						$join .= " LEFT JOIN product_filters as filter$key ON filter$key.product_id = products.id AND filter$key.filter_id = $key and filter$key.value IN (\"" . implode('","', array_map('db_esc',$value)) . "\")";
					}
					else
					{
						$join .= " LEFT JOIN product_filters as filter$key ON filter$key.product_id = products.id AND filter$key.filter_id = $key ";
						if( $custom_ranges )
						{

							//$where .= " and cat_filter_ranges.cat_id = " . $cat_id . " ";
						}
						else
						{
							//print_ar( $cat_filters[$key] );
							//$where .= " and cat_filter_ranges.cat_id = " . $cat_filters[$key]['cat_id'] . " ";
						}
					}

			}
		}
		//$group = " GROUP BY product_filters.cat_filter_ranges.start_value ";
		$group = " GROUP BY cat_filter_ranges.start_value ";

		$query = $sql . $join . $where . $group;
		//echo $query;
		return db_query_array( $query );
	}

	static function getCatFilterRanges( $cat_id, $filter_id, $cast=false, $prod_cat=0 )
	{
		if( $prod_cat )
		{
			if( $cast )
				$sql = " SELECT id, cat_id, filter_id, CAST( start_value as SIGNED ) as start_val, CAST( end_value as SIGNED ) as end_val, start_value, end_value FROM cat_filter_ranges WHERE cat_id = $prod_cat and filter_id = $filter_id ORDER by start_val ASC ";
			else
				$sql = " SELECT * FROM cat_filter_ranges WHERE cat_id = $prod_cat and filter_id = $filter_id ORDER by start_value  ASC ";

			$results = db_query_array( $sql );
			if( $results )
			{
				$results['custom_ranges'] = true;
				return $results;
			}
		}

		if( $cast )
		$sql = " SELECT id, cat_id, filter_id, CAST( start_value as SIGNED ) as start_val, CAST( end_value as SIGNED ) as end_val, start_value, end_value FROM cat_filter_ranges WHERE cat_id = $cat_id and filter_id = $filter_id ORDER by start_val ASC ";
		else
		$sql = " SELECT * FROM cat_filter_ranges WHERE cat_id = $cat_id and filter_id = $filter_id ORDER by start_value  ASC ";

		//echo $sql;
		$results = db_query_array( $sql );
		if( $results )
			{
				$results['custom_ranges'] = false;
				return $results;
			}
		else
		return false;
	}

	static function insertCatFilterRange( $cat_id, $filter_id, $info )
	{
		$info['cat_id'] = $cat_id;
		$info['filter_id'] = $filter_id;

		return db_insert('cat_filter_ranges', $info );
	}

	static function insertCatFilterOption( $cat_id, $filter_id, $info )
	{
		$info['cat_id'] = $cat_id;
		$info['filter_id'] = $filter_id;

		return db_insert('cat_filter_options', $info );
	}

	static function updateCatFilterRange( $id, $info )
	{
		return db_update('cat_filter_ranges',$id,$info);
	}
	static function updateCatFilterOption( $id, $info )
	{
		return db_update('cat_filter_options',$id,$info);
	}
	static function getCatFilterOptions( $cat_id, $filter_id, $cast=false, $prod_cat=0 )
	{

		if( !$cat_id || !$filter_id )
			return false;

		if( $prod_cat )
		{
			$sql = " SELECT cat_filter_options.* FROM cat_filter_options LEFT JOIN filters on filters.id = cat_filter_options.filter_id WHERE cat_id = $prod_cat and filter_id = $filter_id ORDER by display_order ASC, value ";
			$results = db_query_array( $sql );

			if( $results )
				return $results;
		}

		$sql = " SELECT cat_filter_options.* FROM cat_filter_options LEFT JOIN filters on filters.id = cat_filter_options.filter_id WHERE cat_id = $cat_id and filter_id = $filter_id ORDER by display_order ASC, value ";

		//echo $sql . '<br />';
		$results = db_query_array( $sql );
		if( $results )
		    return $results;
		else
		    return false;
	}

	static function getCatFilterOptionSelect( $cat_id, $filter_id, $cur_option=0, $name='filter_option', $msg='Please Select an Option' )
	{
		//echo 'using cat = ' . $cat_id . ' filter = ' . $filter_id . '<br />';
		$options = Filters::getCatFilterOptions( $cat_id, $filter_id );
		//print_ar( $options );

		$ret = '<select name="' . $name .'" size="1">';
		$ret .= '<option value=0>' . $msg . '</option>';
		foreach( $options as $opt )
		{
			$ret .= '<option value="' . $opt['value'] .'" ';
			if( trim($opt['value'], "\"") == $cur_option )
			$ret .= ' selected ';
			$ret .= " >" . $opt['value'] . '</option>';
		}
		$ret .= "</select>";

		return $ret;
	}

	static function updateCatFilter( $cat_id, $filter_id, $info )
	{
		$id = array( $cat_id, $filter_id );
		$pk = array( 'cat_id', 'filter_id' );

		return db_update('cat_filters',$id,$info, $pk);
	}

	static function getRanges( $range_ids )
	{
		if( !is_array( $range_ids ) )
			$range_ids = array( $range_ids );

		$sql = " SELECT * FROM cat_filter_ranges where id IN (\"" . implode( '","', $range_ids ) . "\") ";

		return db_query_array( $sql );
	}

	static function deleteCatFilterOption( $id )
	{
		//echo $id;
		return db_delete( 'cat_filter_options', $id );
	}

	static function deleteCatFilterRange( $id )
	{
		return db_delete( 'cat_filter_ranges', $id );
	}

	static function cacheTopLevel()
	{
		//Want to cache the top level (2nd level) of category filters and counts
		//Run this nightly, should be plenty often to keep numbers acurate
		//First page will simply grab the correct side bar content
		$cats = Cats::getAllSubs( 0, '', true );

//		/print_ar( $cats[2] );
		foreach( $cats as $cat )
		{
			foreach( $cat as $key => $value )
			{
				echo $key ."<br />" ;
				//Need to cache each of these
				$cat_filters = Filters::getFiltersForCat( $key, true );
				$cat_list = Cats::getAllSubs( $key, '', true );

				ob_start();
				if( is_array( $cat_filters ) )
				{
						foreach( $cat_filters as $cf )
						{
							//echo' filter = ' . $cf['filter_id'] . '<br />';
							//print_ar( $cf );
							Filters::displayFrontCatFilter( $cf['filter_id'], $cf['cat_id'], $_GET, $key, $filter_query, $cat_list );
						}
				}
				$content = ob_get_contents();
				ob_end_clean();

				$handle = fopen( "../apps/includes/" . $key . "_cache.php", "w+" );
				fwrite( $handle, $content );
				fclose( $handle );

			}
		}
	}

	static function getCatCache( $cat_id, $backend = false )
	{
			if( $backend )
			{
				if( file_exists("/apps/includes/" . $cat_id . "_cache.php" ) )
				{
					$handle = fopen( "../apps/includes/" . $cat_id . "_cache.php", "r" );
					$content = fread( $handle, filesize("../apps/includes/" . $cat_id . "_cache.php"));
					fclose( $handle );
				}
			}
			else
			{
				if( file_exists("apps/includes/" . $cat_id . "_cache.php") && filesize("apps/includes/" . $cat_id . "_cache.php"))
				{
					$handle = fopen( "apps/includes/" . $cat_id . "_cache.php", "r" );
					$content = fread( $handle, filesize("apps/includes/" . $cat_id . "_cache.php"));
					fclose( $handle );
				}
			}

			return $content;
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT filters.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(filters.id)) as total ";
		}

		$from_ = " FROM filters ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND filters.id = [id] ";
		}

		if($o->name != ''){
			$where_ .= " AND `filters`.name = [name]";
		}
		if($o->prefix != ''){
			$where_ .= " AND `filters`.prefix = [prefix]";
		}
		if($o->suffix != ''){
			$where_ .= " AND `filters`.suffix = [suffix]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY filters.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having. $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

 function getFilterlist() {

    $sql = "SELECT DISTINCT filters.name FROM filters LEFT JOIN product_filters ON filters.id = product_filters.filter_id";
     
    return db_query_array( $sql );
		
  }
  
  static function getProdFiltersWithNoCorrespondingCatFilters($brand_id=0, $exclude_top_level = false)
  {
  	$get_no_cat_filt_sql = "SELECT distinct filters.id as the_filter_id, filters.name, product_cats.cat_id
			FROM (filters, product_filters, product_cats, products, cats)
			LEFT JOIN cat_filters ON ( product_cats.cat_id = cat_filters.cat_id
			AND cat_filters.filter_id = filters.id ) 
			WHERE product_filters.product_id = product_cats.product_id
			AND filters.id = product_filters.filter_id
			AND products.id = product_filters.product_id
			AND product_cats.cat_id = cats.id
			AND products.is_active = 'Y'
			AND cat_filters.filter_id IS NULL";
  	
	if ($brand_id > 0) $get_no_cat_filt_sql .= " AND products.brand_id = " . (int) $brand_id;
	if ($exclude_top_level) $get_no_cat_filt_sql .= " AND cats.pid > 0";  	
  	$get_no_cat_filt_sql .= " ORDER BY product_cats.cat_id, filters.name ASC";
  	return db_query_array($get_no_cat_filt_sql);
  }

  static function seeHowManyProdsInCatHaveFilter($cat_id, $filter_id)
  {
  	$sql = "SELECT count(*) AS num FROM products, product_cats, product_filters 
  			WHERE products.id = product_cats.product_id 
			AND product_filters.product_id = products.id AND product_filters.filter_id = $filter_id 
			AND is_active = 'Y' and is_deleted = 'N' and product_cats.cat_id = $cat_id";
	//	echo $sql;
		$result = db_query_array($sql);
		
		if ($result) return $result[0]['num'];
		else return 0;
  }
static function displayFrontCatFilterRedesign( $filter_id, $main_cat, $vars, $prod_cat, $filter_query, $cats, $cat_filters, $manufacturer = 0, $item_keyword='', $min_price = '', $max_price = '' )
	{
		//$always_show = true;
		$always_show = false;
		$display_filter = $always_show;
		$filter = Filters::get1CatFilter($main_cat, $filter_id );
		 
		
		if( $vars['extra_filters'] )
		foreach( $vars['extra_filters'] as $f )
		{
			$extra_filters[$f] = $vars['filters']['filter_' . $f];
		}
		
		else $extra_filters= array();

		//Need to check what type of filter it is, and display the right type
		//print_ar( $filter );
		$values = Filters::getCatSelectValues( $filter_id, $prod_cat, $extra_filters, $filter_query,$main_cat, $cats, $cat_filters, $manufacturer, $item_keyword, $min_price, $max_price);

		$filter['type'] = 'select';
		if( trim(strtoupper($filter['filter_name']) ) == 'WIDTH'  ||  trim(strtoupper($filter['filter_name']) ) == 'HEIGHT' || trim(strtoupper($filter['filter_name']) ) == 'LENGTH') {$filter['suffix'] = "&quot;";};			
		$display_filter = $always_show;

		if( $values )
			foreach( $values as $v )
			{
				if( $v['count'] )
				    $display_filter = true;
			}
		if(is_array($values) && count($values) && $display_filter ){
				// if none of the results at all have this filter, don't even display the
					// filter heading 
					//if (count($values) == 1 && $values[0]['value'] == '') continue;
					if (count($values) == 1 && $values[0]['value'] == '') {};
					usort($values, function($v1, $v2) { return strnatcmp($v1['value'], $v2['value']); });
					$filters_key = "filter_".$filter_id;
					$filters_key_array = "filters[".$filters_key."][]";															
					foreach( $values as $v )
					{
						if ( $v['count'] == 0 || $v['value'] == '') continue;
						else if ((is_array( $extra_filters[$filter['filter_id']] ) && in_array( $v['value'], $extra_filters[$filter['filter_id']] ))) 
						{
							
							$top_text .= "<div class='fieldset_div_x'><label class='x'><a href='#' onClick=\"undoFilterSelection('$filters_key'); return false;\">x</a> ".$filter['filter_name'] . ":" . $filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']."</label></div>";
						?>
					<input type='hidden' name='<?=$filters_key_array?>' id='<?=$filters_key?>' value='<?=$v['value']?>'/>
					<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />										
					<?																			
						}
						else $values2[] = $v;
					}
					if (count($values2) >= 1)					
					{											
						?>
						<div class="filter-box expanded">
						<div class="heading">
							<strong><?=$filter['filter_name']?></strong>
							<a href="#" class="opener"></a>
						</div>
						<div class="slide">
							<div class="holder">		
							<fieldset>
						<input type="hidden" name="extra_filters[]" value="<?=$filter_id?>" />
					<? 					
					$this_filter_counter = 0;
						foreach( $values2 as $v )
						{
						if ( $v['count'] == 0 || $v['value'] == '') continue; //RSunness added bec. not want to display if no results for that option
						if( is_array( $extra_filters[$filter['filter_id']] ))
						if( in_array( $v['value'], $extra_filters[$filter['filter_id']] ) )
						$checked = 'checked';
						else
						$checked = '';
						else
						$checked ='';
						$this_filter_counter++;												
						?>
						<div class="row">
						<input type="checkbox" name="<?=$filter['filter_name']?>" id="<?= $filter['filter_name']?>-<?=$this_filter_counter?>" onClick='window.dataLayer = window.dataLayer || [];dataLayer.push({"event": "gaTriggerEvent","gaEventCategory": "Tools","gaEventAction": "Click","gaEventLabel": "Cat Page - Other Filter - <?=$filter['filter_name']?> - <?=strtr($v['value'], array('\\' => '\\\\', "'" => "\\'", '"' => '\\"',"\r" => '\\r', "\n" => '\\n' )); ?>"});updateResultsNew("filters[<?=$filters_key?>][]", "<?= strtr($v['value'], array('\\' => '\\\\', "'" => "\\'", '"' => '\\"',"\r" => '\\r', "\n" => '\\n' ));?>");return false;'/>
						<label for="<?= $filter['filter_name']?>-<?=$this_filter_counter?>"><?=$filter['prefix'] . ' ' . $v['value'] . ' ' . $filter['suffix']?> (<? if ( $v['count'] )	echo $v['count'];
							 else echo'0';?>) </label>
						</div>
						
						<?						
						}
						?>
					</fieldset>			
							</div>
						</div>
					</div>
					<?
					}								
		}
		return $top_text;
	}
	function getAvailableCategories($product_ids){
		$sql = "SELECT product_cats.cat_id, cats.name, COUNT(*) AS num
				FROM products, product_cats, cats
				WHERE products.id IN (" . implode(',', $product_ids) . ") 
				AND product_cats.cat_id = cats.id
				AND products.id = product_cats.product_id
				AND cats.list_on_brand_page = 'Y'
				GROUP BY product_cats.cat_id
				ORDER BY cats.name ";
				
				return db_query_array( $sql );
	}	 
	
    
	static function getAllValues( $filter_id='', $value='', $filter='', $order, $order_asc)
	{
		$sql = " SELECT distinct product_filters.filter_id, product_filters.value, filters.name, filters.id as filter_id, filter_info.id, filter_info.title_tag, filter_info.meta_desc, filter_info.description FROM product_filters 
		          LEFT JOIN filters ON product_filters.filter_id = filters.id
 				  LEFT JOIN filter_info ON filter_info.filter_id=product_filters.filter_id AND 
				  filter_info.value=product_filters.value WHERE 1";
				  
     if( $filter_id > 0)				  
				  $sql .=" AND filter_info.filter_id = $filter_id ";
			  
	 if($value){
		         $sql .=" AND filter_info.value = '".$value."'";
	 }		  		  
	
	if ($filter){
		$sql .= " AND filters.name = '".$filter."'";
	}
	
	$sql .=" GROUP BY product_filters.value ORDER BY ";
	
	if ($order == '') {
			$sql .= 'product_filters.filter_id';
		}
		else {
			$sql .= addslashes($order);
		}

	if ($order_asc !== '' && !$order_asc) {
			$sql .= ' DESC ';
		}

    //echo $sql;	
	return db_query_array( $sql );
	
	}		
	
	static function updateFilterInfo( $id, $info)
	{
		if (!$id) $id='DEFAULT';
		$info['value']=mysql_real_escape_string($info['value']);
		$info['title_tag']=mysql_real_escape_string($info['title_tag']);
		$info['meta_desc']=mysql_real_escape_string($info['meta_desc']);
		$info['description']=mysql_real_escape_string($info['description']);
		$sql ="INSERT INTO filter_info(id, filter_id, value, title_tag, meta_desc,description)
               VALUES($id , $info[filter_id], '$info[value]', '$info[title_tag]', '$info[meta_desc]', '$info[description]')
               ON DUPLICATE KEY UPDATE filter_info.title_tag = '$info[title_tag]', filter_info.meta_desc = '$info[meta_desc]',filter_info.description = '$info[description]'"; 
		
        //echo $sql;		
		db_query($sql);
		
		
	}
}