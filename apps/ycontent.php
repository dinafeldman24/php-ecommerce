<?

/**
 * YadEliezer-Style Content System
 * with database driven fields (sections)
 *
 */
class YContent {

	function update($id,$info)
	{
		$result = db_update('contentnew',$id,$info);
		YContent::updateSitemap();
		return $result;
	}

	static function getTemplateData($template_id=0)
	{
		$template_id = (int) $template_id;
		if(!$template_id)
			return false;

		if(!$CFG->use_lang)
			$CFG->use_lang = 'eng';

		$sql = "SELECT 	*
				FROM content_templates
				LEFT JOIN content_sections ON content_sections.template_id = content_templates.id
				LEFT JOIN content_pages ON content_sections.template_id = content_pages.template_id
				LEFT JOIN contentnew ON (contentnew.page_id = content_pages.id AND contentnew.section_id = content_sections.id)
				WHERE content_templates.id = $template_id AND contentnew.language = '".$CFG->use_lang."'";

		$arr = db_query_array($sql);

		if(!empty($arr)){
			foreach($arr as $data){
				$return_arr[$data['section_name']]['data'] = $data['text'];
				$return_arr[$data['section_name']]['type'] = $data['section_type'];
				$return_arr[$data['section_name']]['option'] = $data['section_option'];
			}
		}

		return $return_arr;
	}

	static function getAllContent($page_id=0,$language=''){

		$sql = "SELECT contentnew.*,
				content_pages.name AS page_name,
				content_sections.*,
				content_templates.name AS template_name
				FROM contentnew
				RIGHT JOIN content_pages ON contentnew.page_id = content_pages.id
				RIGHT JOIN content_sections ON contentnew.section_id = content_sections.id
				RIGHT JOIN content_templates ON content_pages.template_id = content_templates.id
				WHERE 1 ";

		if((int) $page_id){
			$sql .= " AND contentnew.page_id = $page_id ";
		}

		if($language){
			$sql .= " AND contentnew.language = '$langugage'";
		}

		return db_query_array($sql);
	}

	static function getPageContent($page_id=0,$language='',$page_type='', $order='', $order_asc='', $limit=0, $start=0)
	{

		//this function will return the content for all pages.

		$sql = "SELECT contentnew.text AS content,contentnew.language,contentnew.page_type,
				contentnew.page_id AS page_id,
				content_sections.template_id AS template_id,
				content_sections.section_name AS name
				FROM contentnew
				RIGHT JOIN content_sections ON contentnew.section_id = content_sections.id
				WHERE 1";

		if((int) $page_id){
			$sql .= " AND contentnew.page_id = $page_id ";
		}

		if(trim($page_type) != ''){
			$sql .= " AND contentnew.page_type = '".addslashes($page_type)."' ";
		}

		if($language){
			$sql .= "  AND contentnew.language = '".addslashes($language)."'";
		}

		$sql .= " ORDER BY ";

		if($order){
			$sql .= addslashes($order);
		}
		else{
			$sql .= "content_sections.order_fld ";
		}

		if ($order_asc !== '' && !$order_asc) {
           $sql .= ' DESC ';
        }

        if ($limit > 0) {
            $sql .= db_limit($limit,$start);
        }



		// echo $sql."<p>";

		$arr = db_query_array($sql);

		if($arr){
			$i = 0;
			$first = true;
			foreach($arr as $section){
				$current_page_id = $section['page_id']; //save current page and template
				$current_page_type = $section['page_type'];
				if((($current_page_id != $old_page_id) || ($current_page_type != $old_page_type)) && !$first){
					//if the page id changes or the template id changes, make a new array entry.
					 //increment array counter
					$i++;
				}

				$current_page_id = $section['page_id']; //reset what is 'current'
				$current_page_type = $section['page_type'];
				$ret[$i]['page_id'] = $current_page_id; //save the page id and template id in the thing for all values beyond the first
				$ret[$i]['current_page_type'] = $current_page_type;
				$ret[$i][$section['name']] = $section['content'];
				$old_page_id = $current_page_id;
				$old_page_type = $current_page_type;
				$first = false;

			}
		}

		return $ret;
	}

	static function get1PageContent($page_id=0,$language='',$page_type='')
	{
		global $CFG;
		$page_id = (int) $page_id;
		if(!$page_id || !trim($page_type)){
			return false; //you have to have page id and page type, or else you get nothing.
		}
		if ($language != ''){
			$results = self::getPageContent($page_id,$language,$page_type);
			$arr[$language] = $results[0];
		}else{
			foreach ($CFG->languages as $language){
				$results = self::getPageContent($page_id,$language,$page_type);
				$arr[$language] = $results[0];
			}
		}

		return $arr;
	}


	static function getContentPages($id=0,$template_id=0,$name='',$order='',$limit=0,$start=0,$get_total=false,$template_type='',$order_asc='',$uid="",$in_sitemap = ""){

		if($template_type){
			$join_ = " LEFT JOIN content_templates on content_templates.id = content_pages.template_id ";
			$where_ = " AND content_templates.type_id = '" . db_esc($template_type) . "' ";
		}

		if (!$get_total){
			$sql = "SELECT content_pages.*
					FROM content_pages
					$join_
					WHERE 1 ";
		}
		else{
			$sql = "SELECT COUNT(content_pages.id) as count
					FROM content_pages
					$join_
					WHERE 1 ";
		}

		if($id){
			$id = (int) $id;
			if(!$id) return false;

			$sql .= " AND content_pages.id = $id ";
		}

		if($uid){
			$sql .= " AND content_pages.uid = '".db_esc($uid)."' ";
		}

		$sql .= $where_;

		if($template_id){
			$template_id = (int) $template_id;
			if(!$template_id) return false;

			$sql .= " AND content_pages.template_id = $template_id ";
		}

		if($in_sitemap){
			$sql .= " AND content_pages.in_sitemap = '".db_esc($in_sitemap)."' ";
		}

		if($name){
			$sql .= " AND content_pages.name = '".db_esc($name)."'";
		}
		//echo $sql;
		if (!$get_total){
            $sql .= ' GROUP BY content_pages.id ';

            $sql .= " ORDER BY ";

            if ($order == '') {
                $sql .= 'content_pages.id';
            }
            else {
                $sql .= addslashes($order);
            }

            if ($order_asc) {
                $sql .= ' ASC ';
            } else {
            	$sql .= " DESC ";
            }

            return db_query_array($sql);
        }
        else{
        	$arr = db_query_array($sql);
        	return $arr[0]['count'];
        }

        if ($limit > 0) {
            $sql .= db_limit($limit,$start);
        }
	}

	static function get1ContentPageByName($name=""){
		if(strlen($name) < 2){
			return false;
		}
		$result = self::getContentPages(null,null,$name);
		return $result[0];
	}

	static function get1ContentPage($id=0){
		$id = (int) $id;
		if(!$id) return false;

		$result = self::getContentPages($id);
		return $result[0];
	}

	static function getPageByUID($uid){
		if(!$uid){
			return false;
		}

		$result = self::getContentPages("",'','','','','','','','',$uid);

		return $result[0];
	}

	static function getContentTypes($id=0,$name=''){
		$sql = "SELECT content_types.*
				FROM content_types
				WHERE 1 ";

		if($id){
			$id = (int) $id;
			if(!$id) return false;

			$sql .= " AND content_types.id = $id ";
		}

		if($name){
			$sql .= " AND content_types.name = '".$name."'";
		}

		return db_query_array($sql);
	}

	static function get1ContentType($id=0){
		$id = (int) $id;
		if(!$id) return false;

		$result = self::getContentTypes($id);
		return $result[0];
	}

	static function getContentSections($id=0,$template_id=0,$section_name='',$section_label='',$section_type='',$section_option='',$page_url=''){
		$sql = "SELECT content_sections.*
				FROM content_sections
				WHERE 1 ";

		if($id){
			$id = (int) $id;
			if(!$id) return false;

			$sql .= " AND content_sections.id = $id ";
		}

		if($template_id){
			$template_id = (int) $template_id;
			if(!$template_id) return false;

			$sql .= " AND content_sections.template_id = $template_id ";
		}

		if($section_name){
			$sql .= " AND content_sections.section_name = '".$section_name."'";
		}

		if($section_label){
			$sql .= " AND content_sections.section_label = '".$section_label."'";
		}

		if($section_type){
			$sql .= " AND content_sections.section_type = '".$section_type."'";
		}

		if($section_option){
			$sql .= " AND content_sections.section_option = '".$section_option."'";
		}
		if($page_url){
			$sql .= " AND content_pages.url = '".addslashes($page_url)."'";
		}

		$sql .= " ORDER BY order_fld ASC ";

		return db_query_array($sql);
	}

	static function get1ContentSection($id=0){
		$id = (int) $id;
		if(!$id) return false;

		$result = self::getContentSections($id);
		return $result[0];
	}

	static function getSectionTypeSelect($name="info[template_id]", $id="template_id", $first_value="Select a Template", $selected_type=0)
    {
		$types = array('Image'=>'image', 'Title'=>'title', 'Link'=>'link', 'Short Text'=>'short_text', 'Long Text'=>'text');
		$ret .= '<select name="' . $name . '" id="'.  $id . '">';
		if($first_option)
			$ret .= '<option value="">Select a Type</option>';
		foreach($types as $k=>$type )
		{
			$ret .= '<option value="'. $type . '" ';
			if( $selected_type == $type )
				$ret .= ' SELECTED ';
			$ret .= '>' . $k . '</option>';
		}
		$ret .= '</select>';

		return $ret;
    }

    static function getContentTypeSelect($name="info[type_id]", $id="type_id", $first_value="Select a Type", $selected_template=0)
    {
		$ret = "";
		$types = self::getContentTypes();
		$ret .= '<select name="' . $name . '" id="'.  $id . '">';
		if($first_option)
			$ret .= '<option value="">Select a Template</option>';
		foreach($types as $type )
		{
			$ret .= '<option value="'. $type['id'] . '" ';
			if( $selected_template == $type['id'] )
				$ret .= ' SELECTED ';
			$ret .= '>' . $type['name'] . '</option>';
		}
		$ret .= '</select>';

		return $ret;
    }

    static function insertPage($info){
		$result = db_insert('content_pages',$info);
		YContent::updateSitemap();
		return $result;
    }

    static function insertSections($info){
		return db_insert('content_sections',$info,'date_added');
    }

    static function updatePage($id,$info)
	{
		$result = db_update('content_pages',$id,$info);
		YContent::updateSitemap();
		return $result;
	}

	static function updateSection($id,$info)
	{
		return db_update('content_sections',$id,$info);
	}

	static function deletePage($id)
	{
		return db_delete('content_pages',$id);
	}

	static function deleteSection($id)
	{
		return db_delete('content_sections',$id);
	}

	static function updateContentPage($id,$info){
		return self::updatePage($id,$info);
	}

	static function insertContentPage($info){
		return self::insert($info);
	}

	static function replaceContent($section_id,$value,$language,$page_id,$page_type)
	{
		$new_info = array(
		'text'=>$value,
		'section_id'=>$section_id,
		'language'=>$language,
		'page_id'=>$page_id,
		'page_type'=>$page_type
		);
		return db_replace('contentnew',$new_info);
	}

	static function deleteContent($id=0,$page_id=0,$page_type='',$section_id=0,$language='')
	{
		$id = (int)$id;
		$page_id = (int)$page_id;
		$page_type = trim($page_type);
		if ($id)
			db_delete('contentnew',$id);
		else if ($page_id && $page_type && $section_id && $language)
			db_delete('contentnew',array($page_id,$page_type,$section_id,$language),array('page_id','page_type','section_id','language'));
		else if ($page_id && $page_type && $section_id)
			db_delete('contentnew',array($page_id,$page_type,$section_id),array('page_id','page_type','section_id'));
		else if ($page_id && $page_type && $language)
			db_delete('contentnew',array($page_id,$page_type,$language),array('page_id','page_type','language'));
		else if ($page_id && $page_type)
			db_delete('contentnew',array($page_id,$page_type),array('page_id','page_type'));
		else
			return false;

		YContent::updateSitemap();
	}

	static function updateLandingPage($page_id=0,$info='',$language='eng'){
		if(!(int) $page_id || !$info){
			return false;
		}

		foreach($info as $section_id=>$value){

			$section_id = (int) $section_id;
		 	$sql = "SELECT id FROM contentnew
					WHERE page_id = $page_id
					AND section_id = $section_id
					AND language = '".addslashes($language)."'
					AND page_type = 'content' LIMIT 1";

		 	$arr = db_query_array($sql);

		 	if((int) $arr['0']['id']){
		 		YContent::update($arr['0']['id'],array('text'=>$value));
		 	}
		}
	}

	static function getContentPagesByType($type_id=''){
		$where->type_id = $type_id;

		$sql = "SELECT cp.*,ct.name as template FROM content_pages cp ";
		$sql .= " LEFT JOIN content_templates ct ON ct.id = cp.template_id ";
		$sql .= " WHERE 1 ";
		if($where->type_id){
			$sql .= " AND ct.type_id = [type_id] ";
		}

		return db_template_query($sql,$where);
	}

	static function getContentTemplates($type_id='',$addable=""){
		$where->type_id = $type_id;
		$where->addable = $addable;

		$sql = " SELECT * FROM content_templates WHERE 1 ";

		if($where->type_id){
			$sql .= " AND type_id = [type_id] ";
		}
		if($where->addable){
			$sql .= " AND addable = [addable] ";
		}

		return db_template_query($sql,$where);
	}

	// Find if content-by-URL exists in database.
	static function getPageByURL($url){
		// TODO: Include Content Page
		$url = trim($url,"/");
		$where->url = $url;

		$sql = "SELECT * FROM content_pages WHERE ";
		$sql .= " url = [url] LIMIT 1 ";

		$result = db_template_query($sql,$where);

		return $result[0];
	}

	static function get1SectionTextByUrl($section_key)
	{
		$sql = "SELECT *
			FROM `contentnew`
			LEFT JOIN content_pages ON content_pages.id = contentnew.page_id
			LEFT JOIN content_sections ON content_sections.id = contentnew.section_id
			WHERE 1
			AND contentnew.page_type = 'content'
			AND section_name = 'body'
			AND content_pages.url = '".addslashes($section_key)."'
			ORDER BY `contentnew`.`id` DESC ";
		$results = db_query_array($sql);
		return $results[0]['text'];
	}

	static function updateSitemap()
	{
		global $CFG;
		$fp = fopen($CFG->dirroot.'/sitemap.xml', 'w');
		//Course
		//Schools
		//Programs
		//Content Pages
		//Articles
		// Legacy Content System
		$content_pages = YContent::get(0,'','Y');

		foreach ($content_pages as $page){
			if (substr($page['url'],-5) == '.html' && $page['cat_id'] != $CFG->content_section_cat_id){
				$map_pages[] = $page;
			}
		}
		unset($content_pages);
		$rss = '<?xml version="1.0" encoding="UTF-8"'.'?'.">\r\n";
		$rss .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">
				<url>
				<loc>{$CFG->baseurl}</loc>
				<priority>1.0</priority>
				</url>
				";
		fwrite($fp, $rss);

	    foreach ($map_pages as $map_page) {

	    	if ($map_page['date_modified'] == '0000-00-00 00:00:00' || !$map_page['date_modified']) {
	    		$map_page['date_modified'] = $map_page['date_added'];
	    	}

	    	$rss = "<url>
					<loc>".substr($CFG->baseurl,0,-1).str_replace($CFG->baseurl,'/',$map_page['url'])."</loc>
					<lastmod>".str_replace(' ','T',$map_page['date_modified'])."Z</lastmod>
					</url>
					";
	    	fwrite($fp, $rss);
	    }
	    unset($map_pages);

	    $rss = "
	    	<url>
				<loc>".substr($CFG->baseurl,0,-1)."/schools/</loc>
				<changefreq>monthly</changefreq>
			</url>
			<url>
				<loc>".substr($CFG->baseurl,0,-1)."/degrees/</loc>
				<changefreq>monthly</changefreq>
			</url>
			<url>
				<loc>".substr($CFG->baseurl,0,-1)."/courses/</loc>
				<changefreq>monthly</changefreq>
			</url>
				<url>
				<loc>".substr($CFG->baseurl,0,-1)."/articles/</loc>
				<changefreq>monthly</changefreq>
			</url>
			<url>
				<loc>".substr($CFG->baseurl,0,-1)."/compare-programs.html</loc>
				<changefreq>monthly</changefreq>
			</url>
			<url>
				<loc>".substr($CFG->baseurl,0,-1)."/make-your-own-rankings.html</loc>
				<changefreq>monthly</changefreq>
			</url>

					";


	    $schools_ct = Schools::get(0,'','','','Y','','',0,0,true);
	    $schools_ct = $schools_ct['total'];
	    $total_school_pages = ($schools_ct / $CFG->school_results_ct);
	    if ($schools_ct % $CFG->school_results_ct){
	    	$total_school_pages++;
	    }


	    $rss = "<url>
				<loc>".substr($CFG->baseurl,0,-1)."/university-list.php</loc>
				<changefreq>weekly</changefreq>
				</url>
				";
    	fwrite($fp, $rss);
    	$school_page = 2;

    	while ($total_school_pages >= $school_page){
    		$rss = "<url>
				<loc>".substr($CFG->baseurl,0,-1)."/university-list.php?page=$school_page</loc>
				<changefreq>weekly</changefreq>
				</url>
				";
    		$school_page++;
    		fwrite($fp, $rss);
    	}

	    $schools = Schools::get(0,'','','','Y');

	    foreach ($schools as $school) {

	    	if ($school['last_updated'] == '0000-00-00 00:00:00') {
	    		$school['last_updated'] = $school['date_added'];
	    	}

	    	$rss = "<url>
	    				<loc>".Catalog::makeSchoolLink($school)."</loc>
	    				<lastmod>".str_replace(' ','T',$school['last_updated'])."Z</lastmod>
					</url>
					";
	    	fwrite($fp, $rss);
	    }
	    unset($schools);


	    //Program Landing Page
	    $programs_ct = Program::get(0,'','','','','',0,0,true,0,0,0,0,0,0,'','','Y');
	    $programs_ct = $programs_ct['total'];
	    $total_program_pages = ($programs_ct / $CFG->program_results_ct);
	    if ($programs_ct % $CFG->program_results_ct){
	    	$total_program_pages++;
	    }
    	$rss = "<url>
				<loc>".substr($CFG->baseurl,0,-1)."/degree-programs-list.php</loc>
				<changefreq>weekly</changefreq>
				</url>
				";
    	fwrite($fp, $rss);
    	$prog_page = 2;

    	while ($total_program_pages >= $prog_page){
    		$rss = "<url>
				<loc>".substr($CFG->baseurl,0,-1)."/degree-programs-list.php?page=$prog_page</loc>
				<changefreq>weekly</changefreq>
				</url>
				";
    		$prog_page++;
    		fwrite($fp, $rss);
    	}


	    $program_cats = CategoriesCats::get(0,'','','','','','','',0);

	    foreach ($program_cats as $program_cat) {

	    	//Program Landing Page
		    $programs_ct = Program::get(0,'','','','','',0,0,true,0,0,0,0,$program_cat['id'],0,'','','Y');
		    $programs_ct = $programs_ct['total'];
		    $total_program_pages = ($programs_ct / $CFG->program_results_ct);
		    if ($programs_ct % $CFG->program_results_ct){
		    	$total_program_pages++;
		    }
    		$rss = "<url>
			<loc>".substr($CFG->baseurl,0,-1).Catalog::makeDegreeCatLink($program_cat)."</loc>
			<changefreq>weekly</changefreq>
			</url>
			";
	    	fwrite($fp, $rss);
	    	$prog_page = 2;

	    	while ($total_program_pages >= $prog_page){
	    		$rss = "<url>
					<loc>".substr($CFG->baseurl,0,-1).Catalog::makeDegreeCatLink($program_cat)."?page=$prog_page</loc>
					<changefreq>weekly</changefreq>
					</url>
					";
	    		$prog_page++;
	    		fwrite($fp, $rss);
	    	}
    	}
    	unset($program_cats);

	    $start = 0;
		$limit = 1000;
		do{
	    	$programs = Program::get(0,'','','','','',$limit,$start,false,0,0,0,0,0,0,'','','Y');
	    	if ($programs){
			    foreach ($programs as $program) {

			    	if ($program['last_updated'] == '0000-00-00 00:00:00') {
			    		$program['last_updated'] = $program['date_added'];
			    	}

			    	$rss = "<url>
			    				<loc>".Catalog::makeProgramLink($program,true)."</loc>
			    				<lastmod>".str_replace(' ','T',$program['last_updated'])."Z</lastmod>
							</url>
							";
			    	fwrite($fp, $rss);
			    }
	    	}
	    	$start += $limit;
		}while($programs);




		$course_cats = CourseCats::get(0,'','','','','','');

	    foreach ($course_cats as $course_cat) {

	    	//Program Landing Page
		    $course_ct = Courses::get(0,'','','','',0,'Y',true,0,0,'','',$course_cat['id']);
		    $course_ct = $course_ct['total'];
		    $total_course_pages = ($course_ct / $CFG->course_results_ct);
		    if ($course_ct % $CFG->course_results_ct){
		    	$total_course_pages++;
		    }
    		$rss = "<url>
			<loc>".Catalog::makeCourseCatLink($course_cat)."</loc>
			<changefreq>weekly</changefreq>
			</url>
			";
	    	fwrite($fp, $rss);
	    	$course_page = 2;

	    	while ($total_course_pages >= $course_page){
	    		$rss = "<url>
					<loc>".Catalog::makeCourseCatLink($course_cat)."?page=$course_page</loc>
					<changefreq>weekly</changefreq>
					</url>
					";
	    		$course_page++;
	    		fwrite($fp, $rss);
	    	}
    	}
    	unset($course_cats);
		$courses = Courses::get(0,'','','','',0,'Y',false,0,0);

	    foreach ($courses as $course) {

	    	if ($course['last_updated'] == '0000-00-00 00:00:00') {
	    		$course['last_updated'] = $course['date_added'];
	    	}

	    	$rss = "<url>
	    				<loc>".Catalog::makeCourseLink($course)."</loc>
	    				<lastmod>".str_replace(' ','T',$course['last_updated'])."Z</lastmod>
					</url>
					";
	    	fwrite($fp, $rss);
	    }

	    unset($courses);

	    $article_cats = ArticlesCats::get(0,'','','','','','');

	    foreach ($article_cats as $article_cat) {

	    	//Program Landing Page
		    $article_ct = Articles::get(0,'','','','Y','',0,0,'','','','',0,true,$article_cat['id']);
		    $article_ct = $article_ct['total'];
		    $total_article_pages = ($article_ct / $CFG->article_results_ct);
		    if ($article_ct % $CFG->article_results_ct){
		    	$total_article_pages++;
		    }
    		$rss = "<url>
			<loc>".Catalog::makeArticleCatLink($article_cat)."</loc>
			<changefreq>weekly</changefreq>
			</url>
			";
	    	fwrite($fp, $rss);
	    	$article_page = 2;

	    	while ($total_article_pages >= $article_page){
	    		$rss = "<url>
					<loc>".Catalog::makeCourseCatLink($article_cat)."?page=$article_page</loc>
					<changefreq>weekly</changefreq>
					</url>
					";
	    		$article_page++;
	    		fwrite($fp, $rss);
	    	}
    	}
    	unset($article_cats);

		$articles = Articles::get(0,'','','','Y','',0,0,'','','','',0,false);
		foreach ($articles as $article) {

	    	if ($article['last_updated'] == '0000-00-00 00:00:00') {
	    		$article['last_updated'] = $article['date_added'];
	    	}

	    	$rss = "<url>
	    				<loc>".Catalog::makeArticleLink($article)."</loc>
	    				<lastmod>".str_replace(' ','T',$article['last_updated'])."Z</lastmod>
					</url>
					";
	    	fwrite($fp, $rss);
	    }
	    unset($articles);

		$o = null;
		$o->is_active = 'Y';
		$blogs = Blog::getByO($o);

		if($blogs)foreach ($blogs as $b) {

	    	if ($b['date_modified'] == '0000-00-00 00:00:00') {
	    		$b['date_modified'] = $b['date_active'];
	    	}

	    	$rss = "<url>
	    				<loc>".Catalog::blogLink($b)."</loc>
	    				<lastmod>".str_replace(' ','T',$b['date_modified'])."Z</lastmod>
					</url>
					";
	    	fwrite($fp, $rss);
	    }
	    unset($blogs);

	    $rss = "
	</urlset>";


		fwrite($fp, $rss);

		fclose($fp);
	}

	/* used for updating content (images, links, text, etc) */
	static function preprocessFields($content){
		global $CFG;

		$file_types = ".jpg|.jpeg|.gif|.png";

		$content_m = array();
		if(is_array($content))foreach($content as $lang=>$cont_id){ //go through all the content
			foreach($cont_id as $k=>$cont){
				if(is_array($cont) && array_key_exists("url",$cont)){ //if it has 'url' as a key then it's link stuff.
					unset($content[$lang][$k]);
					if(strlen($cont['url']) > 0){
						$content_m[$lang][$k] = '<a class="link" href="'.$cont['url'].'">'.$cont['text'].'</a>';
					} else {
						$content_m[$lang][$k] = "";
					}
				} else if( $_FILES['content']['size'][$lang][$k] > 1 ) {
					// We have an image uploaded with a valid size
					list($width,$height) = explode("x",$cont['options']);
//					if(!$width || !$height){
//						$width = $height = 200; // default (shouldn't happen)
//					}
					$fname = $_FILES['content']['name'][$lang][$k];
					if(YContent::fileTypeOK($fname,$file_types)){
						$fileprefix = substr( ValidateData::toAlphaNumeric($fname,true,true), 0, -3);
						$file_key = YContent::generateImage($fileprefix.'_',$_FILES['content']['tmp_name'][$lang][$k],$width,$height);
						$content_m[$lang][$k] = $file_key;
					}
					else {
						// dont add to content_m array
						echo "Invalid File Type.<br />";
					}
				} else if(is_array($_FILES['content']['name'][$lang])
					&& array_key_exists($k,$_FILES['content']['name'][$lang])){
						; // don't add to content_m array
				} else {
					$content_m[$lang][$k] = $cont;
				}

				if(strlen($_REQUEST['remove_image_content'][$lang][$k]) > 1){
					ImageDB::delete("",$_REQUEST['remove_image_content'][$lang][$k]);
					$content_m[$lang][$k] = "";
					echo "Deleted an image.<br />";
				}
			//			echo "content $k<br />";
			}
		}

		return $content_m;
	}


	/* Store Image and Return file_key to it */
	static function generateImage($fileprefix,$tmp_filepath,$width,$height)
	{
		global $CFG;

		$imagefile = $tmp_filepath;

		$file_key = uniqid($fileprefix);
		$info = array("file_key" => $file_key);
		$db_id = ImageDB::insert($info);

		echo "Generating Image...$file_key<br />";
	    flush();

	    // include image resize class
		include_once('RB/resize.php');

		$ir = new ImageResize($imagefile);
		$ir->setHighQuality();
		$ir->setAutoCrop(true);
		$ir->setAutoCenter(true);

		if($width > 0 && $height > 0){
			// full size
			$ir->setSize($width,$height);
			$ir->save($CFG->image_upload_dir."/$file_key".$CFG->full_suffix,'jpeg',90);
		} else {
			// Copy orig image, don't resize
			$iinfo = getimagesize($tmp_filepath);
			if($iinfo['mime'] == "image/jpeg"){
				copy($tmp_filepath,$CFG->image_upload_dir."/$file_key".$CFG->full_suffix);
			} else {
				echo "Error: You must upload a JPEG image for this feature to work.<br />";
				return "";
			}
		}

		// thumbnail
		$ir->setSize($CFG->thumbnail_width,$CFG->thumbnail_height);
		$ir->save($CFG->image_upload_dir."/$file_key".$CFG->thumbnail_suffix,'jpeg',70);

	/*
		// large
		$ir->setSize($CFG->large_width,$CFG->large_height);
		$ir->save("../pics/$id".$CFG->large_suffix,'jpeg',90);

		// xlarge

		if ($_POST['image_resizing'] == 'auto')
			$ir->setSize($_POST['image_width'],$_POST['image_height']);
		else {
			$img_size = @getimagesize($imagefile);
			if ($img_size)
				$ir->setSize($img_size[0],$img_size[1]);
			else
				$ir->setSize($_POST['image_width'],$_POST['image_height']);
		}

		$ir->save("../pics/$id".$CFG->xlarge_suffix,'jpeg',90);
		*/

		/* Clean up */
		$ir->_destroy();
		$ir = null;
		@unlink($imagefile);

		return $file_key;
	}

	static function fileTypeOK($fname, $types){
		$extension = strtolower( substr($fname,-4) );

		if(!is_array($types)){
			$arr = explode('|',$types);
		} else {
			$arr = $types;
		}

		if(!in_array($extension,$arr)){
			return false;
		}
		else{
			return true;
		}
	}

}


?>
