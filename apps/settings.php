<?php

class Settings {

	function assign(&$var)
	{
		$all = Settings::getAll();

		if (is_array($all)) {
			if (is_object($var)) {
				foreach ($all as $row) {
					$normalized_name = Settings::_normalizeName($row['name']);
					$var->$normalized_name = $row['value'];
				}
			}
			else {
				foreach ($all as $row) {
					$normalized_name = Settings::_normalizeName($row['name']);
					$var[$normalized_name] = $row['value'];
				}
			}
		}
	}

	function get($name)
	{
		$result = db_query_array("SELECT value
								  FROM settings
								  WHERE name = '".addslashes($name)."'",'',true);
		return $result['value'];
	}

	function set($name,$value)
	{
		$affected_rows = db_update('settings',$name,array('value'=>$value),'name');

		if (!$affected_rows) {
			db_insert('settings',array('name'=>$name,'value'=>$value),'',true);
		}

		return true;
	}

	function getAll()
	{

        $sql = "SELECT *

		FROM settings";

		return db_query_array($sql);
	}

	function _normalizeName($str)
	{
		$str = strtolower($str);
		return preg_replace('/[^a-z0-9_]/','',$str);
	}
}

?>