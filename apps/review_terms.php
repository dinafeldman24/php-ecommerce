<?php

class Review_Terms
{

    public static function insert($info='')
    {
				$info['description']=mysql_real_escape_string($info['description'] );
				return db_insert('review_terms', $info);
		 }

 public static function update($id='', $info='')
		
    {
    		$safedescription=mysql_real_escape_string($info['description'] );
				$info['description']=$safedescription;
							
        return db_update('review_terms', $id, $info );
    }		 
		
			function deleteTerm($id)
	{
		return db_delete('review_terms', $id);
	}


  public static function associate2Cats($term_id='',$cat_ids='',$delete_old_ones='N')
	{
		if (!is_int($term_id)) return false;
		if (!is_array($cat_ids)) {
			$cat_ids = Array($cat_ids);
		}
		
		foreach ($cat_ids as $cid) if (!is_int($cid)) return false;

		$cat_ids_str = "'" . implode("','",$cat_ids) . "'";

		if (trim($cat_ids_str) == '') {
			db_delete('review_terms_cat',$term_id,'term_id');
			return true;
		}

		// delete old ones

                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM review_terms_cat WHERE term_id = $term_id AND category_id NOT IN ($cat_ids_str)");
                }

		

		// insert new ones

		$info = Array('term_id'=>$term_id);

		foreach ($cat_ids as $cat_id) {

		    if( $cat_id <= 0 )
			continue;

			$info['category_id'] = $cat_id;
			db_insert('review_terms_cat',$info,'',true);
		}

		return true;
	}
	
	 public static function associate2Review($term_ids='',$review_ids='',$delete_old_ones='Y')
	{

		
		if ((is_array($review_ids))&&($delete_old_ones == "Y")&&(!is_array($term_ids)))
		{
			foreach ($review_ids as $rid) if (!is_int($rid)) return false;
		
			$review_ids_str = "'" . implode("','",$review_ids) . "'";
		if (trim($review_ids_str) == '') {
			db_delete('review_terms_used',$term_id,'term_id');
		}
		// delete old ones
                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM review_terms_used WHERE term_id = $term_ids AND review_id NOT IN ($review_ids_str)");
        								
                }

		// insert new ones

		foreach ($review_ids as $re) {
	    if( $re <= 0 )
			     continue;
		 $thisreview=Product_Reviews::get($re);
		  $allterms=self::getProductTerms($thisreview[0][product_id]);
		     if (self::searchArray($allterms,$term_ids)){
				     $info = Array('term_id'=>$term_ids,'review_id'=>$re);
			       db_insert('review_terms_used',$info,'',true);
				 }
		else {
		db_query("DELETE FROM review_terms_used WHERE term_id = $term_ids AND review_id =$re");
		echo "The review <b><i>".$thisreview[0][title]."</b></i> wasn't upadate with this term as it's not in an allowed category.<br/>";
		   }
	  }	
	}			 
		if ((!is_array($review_ids))&&($delete_old_ones == "Y")&&(is_array($term_ids)))
		{
			foreach ($term_ids as $tid) if (!is_int($tid)) return false;
		
			$term_ids_str = "'" . implode("','",$term_ids) . "'";
		if (trim($term_ids_str) == '') {
			db_delete('review_terms_used',$reivew_ids,'review_id');
		}
		// delete old ones
                if($delete_old_ones == "Y")
                {
                    db_query("DELETE FROM review_terms_used WHERE review_id = $review_ids AND term_id NOT IN ($term_ids_str)");
        								
                }

		// insert new ones
		foreach ($term_ids as $te) {
	    if( $te <= 0 )
			     continue;
			$te = preg_replace('/\s+/', '', $te);
		  $info = Array('term_id'=>$te,'review_id'=>$review_ids);
			$result=db_insert('review_terms_used',$info,'',true);
		}
	}
	
			if ((!is_array($review_ids))&&($delete_old_ones == "Y")&&(!$term_ids)) //deleting all terms for a review
			 {
			 	if (!is_int($review_ids)) return false;
			 	
         db_query("DELETE FROM review_terms_used WHERE review_id = $review_ids");
			 }

		return true;
	}
	
	
	public static function get($term_id='',$type='',$approved='',$cat_id='',$review_id='')
	{
						
		$sql="SELECT * FROM review_terms"; 

			 	 if ($cat_id)
        {
            $sql .= " RIGHT JOIN review_terms_cat ON review_terms.id = review_terms_cat.term_id";
        }
				
			 if ($review_id)
        {
            $sql .= " RIGHT JOIN review_terms_used ON review_terms.id = review_terms_used.term_id";
        }
 
         $sql .=" WHERE 1";
	

        if ($term_id > 0)
        {
            $sql .= " AND review_terms.id = $term_id";
        }

        if ($type > 0)
        {
            $sql .= " AND review_terms.type = $type";
        }

        if ($approved != '')
        {
            $sql .= " AND review_terms.approved = $approved";
        }
				 if ($cat_id)
        {
            if (!is_array($cat_id))				
                $sql .= " AND review_terms_cat.category_id = $cat_id";
						else
						    {
								$values = implode(',', $cat_id);
						    $sql .= " AND review_terms_cat.category_id IN ($values)";
								}		
						
        }
					 if ($review_id)
        {
				    if (!is_array($review_id))
                $sql .= " AND review_terms_used.review_id = $review_id";
						else{
						    $values = implode(',',$review_id);
						    $sql .= " AND review_terms_used.review_id IN ($values)";
								}		
        }


       return db_query_array($sql);
	}
	
	static function getTermsCats($term_id='')
	{
		if (!is_int($term_id)) return false;
		
		if( !$term_id )
			return false;
		return db_query_array("SELECT category_id FROM review_terms_cat WHERE term_id = $term_id",'category_id');
	}	
	
		static function getTermsUsed($term_id='')
	{
		if (!is_int($term_id)) return false;
		if( !$term_id )
			return false;
		return db_query_array("SELECT review_id FROM review_terms_used WHERE term_id = $term_id",'review_id');
	}	

		static function getReviewsUsed($review_id='',$type='')
	{
		if (!is_int($review_id)) return false;
		if( !$review_id )
			return false;
		return db_query_array("SELECT * FROM review_terms RIGHT JOIN review_terms_used ON review_terms.id=review_terms_used.term_id WHERE review_terms_used.review_id=$review_id AND review_terms.type=$type");
	}	

	    public static function get1($id)
    {
        $id = (int) $id;

        if (!$id)
        {
            return false;
        }

        $result = self::get($id);
        return $result[0];
    }
		


				
		public static function getProductTerms ($product_id='',$type='',$reviews='',$approved='')
		{
			
		$cats=Cats::getCatsForProduct($product_id);
		$i=0;
		foreach ($cats as $cat){
		   $catids[$i]=$cat[id];
			 $i++;
			 }
		$i=0;	 
		foreach ($reviews as $re){
		   $reviewid[$i]=$re['id'];
			 $i++;
		 }
	// $approved="'".$approved."'";	 
	 $terms=self::get("",$type,$approved,$catids,$reviewid);
	 $i=0;	
			 foreach ($terms as $term){
			   unset($term[category_id]);
			   $result[$i]=$term;
			   $i++;
	   }
		 if (is_array($reviewid))
		    {		
				  return $result;
				}
			$result = array_map('unserialize', array_unique(array_map('serialize', $result)));
		  return $result;	 
		}


public static function searchArray($myarray='', $id='') {
    foreach ($myarray[0] as $item) {
        if ($item[id] == $id)
            return true;
    }
    return false;
}


}
?>