<?php

class EbayStore{

	static function insert($info)
	{
	    return db_insert('ebay_store',$info,'date_added');
	}
	
	static function update($id,$info)
	{
	    return db_update('ebay_store',$id,$info,'id','date_modified');
	}
	
	static function delete($id)
	{
	    return db_delete('ebay_store',$id);
	}
	
	static function get($id=0,$ebay_store_id=0,$is_active = '',$order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
		    $sql .= " COUNT(DISTINCT(ebay_store.id)) AS total ";
		} else {
		    $sql .= " ebay_store.* ";
		}
		
		$sql .= " FROM ebay_store ";
		
		$sql .= " WHERE 1 ";
		
		if ($id > 0) {
		    $sql .= " AND ebay_store.id = $id ";
		}
		
		if ($ebay_store_id > 0) {
			$sql .= " AND ebay_store.user_device_id = $user_device_id ";
		}
		
		if ($is_active) {
			$sql .= " AND ebay_store.is_active = '$is_active' ";
		}
		
		if (!$get_total){
		    $sql .= ' GROUP BY ebay_store.id ';
				
		    $sql .= " ORDER BY ";
		    
		    if ($order == '') {
		        $sql .= ' ebay_store.date_added DESC,ebay_store.description ';
		    }
		    else {
		        $sql .= addslashes($order);
		    }
		    
		    if ($order_asc !== '' && !$order_asc) {
		        $sql .= ' DESC ';
		    }
		}
		
		if ($limit > 0) {
		    $sql .= db_limit($limit,$start);
		}
		
		if (!$get_total) {
		    $ret = db_query_array($sql);
		} else {
		    $ret = db_query_array($sql,'',true);
		}
		
		return $ret;
	}

	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);
                
        return $result[0];
    }

}