<?php 
class SweepstakesEntry {

	static function insert($info)
	{
		return db_insert('sweepstakes_entries',$info);
		
	}

	static function get($email="",$contest_name="",$youtube_url = '', $id = 0, $url_tag='')
	{
		$sql = "SELECT sweepstakes_entries.*
				FROM sweepstakes_entries
				WHERE 1 ";

		if ($email != "") {
			$sql .= " AND sweepstakes_entries.email = '$email'";
		}
		
		if ($contest_name != "") {
			$sql .= " AND sweepstakes_entries.sweepstakes_label = '$contest_name'";
		}
		
		if ($youtube_url != "") {
			$sql .= " AND sweepstakes_entries.youtube_url = '$youtube_url'";
		}
		if ($id > 0) {
			$sql .= " AND sweepstakes_entries.id = ". db_esc($id);
		}
		if ($url_tag != "")
		{
			$sql .= " AND sweepstakes_entries.url_tag = '" . db_esc($url_tag) ."'";
		}
		return db_query_array($sql);
	}
}
?>