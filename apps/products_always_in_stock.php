<? 
class ProductsAlwaysInStock
{
	static function get($id=0, $product_id = 0, $product_option_id = 'none', $order_by = '')
	{  
		$sql = "SELECT ";
	    $sql .= " products_always_in_stock.* ";		
		$sql .= " FROM products_always_in_stock ";
		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND products_always_in_stock.id = $id ";
		}
		if ($product_id > 0) {
		    $sql .= " AND products_always_in_stock.product_id = $product_id ";
		}
		
		if ($product_option_id !== 'none') {						
		    $sql .= " AND products_always_in_stock.product_option_id = $product_option_id ";
		}								
	
		if ($order_by != '') $sql .= " ORDER BY $order_by";
	    $ret = db_query_array($sql);
//mail("rachel@tigerchef.com", "products always in stock sql", $sql);
		return $ret;
	}
	
	static function get1($id = 0){		
		if(!$id)
			return false;

		$ret = self::get($id);
		
		return $ret[0];
	}

	static function delete($id = 0)
	{
		if(!$id)
			return false;
			
		return db_delete('products_always_in_stock', $id);
	}
	static function insert($info)
	{
		if(!$info)
			return false;
			
		db_insert('products_always_in_stock', $info);				
	}
	static function update($id = 0, $info = ''){

		if(!$info || !$id)
			return false;
		

			$ret = db_update('products_always_in_stock',$id,$info);

		return $ret;
	}
}
?>