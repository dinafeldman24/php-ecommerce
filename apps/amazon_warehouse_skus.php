<? 
class AmazonWarehouseSkus
{
	static function get($sku='')
	{
		$sql = "SELECT ";
	    $sql .= " amazon_warehouse_skus.* ";		
		$sql .= " FROM amazon_warehouse_skus ";
		$sql .= " WHERE 1 ";

		if ($sku != '') {
		    $sql .= " AND amazon_warehouse_skus.warehouse_sku = '" . $sku . "'";
		}		
				
	    $ret = db_query_array($sql);

		return $ret;
	}
	
	static function get1($sku = ''){
		if(!$sku)
			return false;

		$ret = self::get($sku);
		
		return $ret[0];
	}
	
	static function insert($info)
	{
		if(!$info)
			return false;
			
		return db_insert('amazon_warehouse_skus', $info);
	}	
}
?>