<?php

class Reward_Gifts{

	static function insert($info)
	{
	    return db_insert('reward_gifts',$info);
	}

	static function update($id,$info)
	{
	    return db_update('reward_gifts',$id,$info);
	}

	static function delete($id)
	{
	    return db_delete('reward_gifts',$id);
	}

	static function get($id=0,$current_only=false,$min_dollar_value = '', $min_points = '', $is_active = '', $site='',$date_range_start='',$date_range_end= '',$order='',$order_asc='',$limit=0,$start=0,$get_total=false)
	{
		$sql = "SELECT ";
		if ($get_total) {
		    $sql .= " COUNT(DISTINCT(reward_gifts.id)) AS total ";
		} else {
		    $sql .= " reward_gifts.* ";
		}

		$sql .= " FROM reward_gifts ";

		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND reward_gifts.id = $id ";
		}

		if ($current_only) $sql .= " AND reward_gifts.is_active = 'Y' AND (NOW() BETWEEN reward_gifts.start_date and reward_gifts.end_date
		                           OR NOW()> reward_gifts.start_date and reward_gifts.end_date IS NULL)";
		if ($date_range_start != '' && $date_range_end != '') 
		{
			$sql .= " AND ('$date_range_start' BETWEEN reward_gifts.start_date AND reward_gifts.end_date 
					OR '$date_range_end' BETWEEN reward_gifts.start_date and reward_gifts.end_date 
					OR '$date_range_start' < reward_gifts.start_date AND '$date_range_end' > reward_gifts.start_date)";
		}

		
		if(  $min_value  >  0 ){
			$min_dollar_value = mysql_real_escape_string($min_dollar_value);
			$sql .= " AND reward_gifts.dollar_value < $min_dollar_value";
		}
		if(  $min_points  >  0 ){
			$min_points = mysql_real_escape_string($min_points);
			$sql .= " AND reward_gifts.points < $min_points";
		}
		if(  $is_active  != ''){
			$is_active = mysql_real_escape_string($is_active);
			$sql .= " AND reward_gifts.is_active = '$is_active'";
		}


		if( $site  != ''){
			$site = mysql_real_escape_string($site);
			$sql .= " AND reward_gifts.gift_site = '$site'";
		}
		
		if (!$get_total){
		    $sql .= ' GROUP BY reward_gifts.id ';

		    if($order){
		    	$sql .= " ORDER BY ";

	        	$sql .= addslashes($order);

		    	if ($order_asc !== '' && !$order_asc) {
		        	$sql .= ' DESC ';
		    	}
		    }
		}

		if ($limit > 0) {
		    $sql .= db_limit($limit,$start);
		}

		if (!$get_total) {
		    $ret = db_query_array($sql);
		} else {
		    $ret = db_query_array($sql,'',true);
		}

		return $ret;
	}

	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);

        return $result[0];
    }

    /**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT reward_gifts.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(reward_gifts.id)) as total ";
		}

		$from_ = " FROM reward_gifts ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND reward_gifts.id = [id] ";
		}
		if( isset( $o->min_value )  ){
			$where_ .= " AND `reward_gifts`.dollar_value > [min_dollar_value]";
		}
		if( isset( $o->discount )  ){
			$where_ .= " AND `reward_gifts`.points > [min_points]";
		}
		if( isset( $o->is_active )  ){
			$where_ .= " AND `reward_gifts`.is_active = [is_active]";
		}

		if( isset( $o->gift_site )  ){
			$where_ .= " AND `reward_gifts`.gift_site = [gift_site]";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY reward_gifts.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}


}