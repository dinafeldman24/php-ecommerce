<?php

class EbayListing{

	static function insert($info)
	{
	    return db_insert('ebay_listing',$info,'date_added');
	}

	static function update($id,$info)
	{
	    return db_update('ebay_listing',$id,$info,'id','date_modified');
	}

	static function delete($id)
	{
	    return db_delete('ebay_listing',$id);
	}

	static function get($id=0,$ebay_store_id=0,$is_active = '',$order='',$order_asc='',$limit=0,$start=0,$get_total=false, $item_id='')
	{
		$sql = "SELECT ";
		if ($get_total) {
		    $sql .= " COUNT(DISTINCT(ebay_listing.id)) AS total ";
		} else {
		    $sql .= " ebay_listing.*, store.name as store_name, products.name as product_name ";
		}

		$sql .= " FROM ebay_listing
				LEFT JOIN store ON store.id = ebay_listing.ebay_store_id
				LEFT JOIN products ON products.id = product_id ";

		$sql .= " WHERE 1 ";

		if ($id > 0) {
		    $sql .= " AND ebay_listing.id = $id ";
		}

		if ($ebay_store_id > 0) {
			$sql .= " AND ebay_listing.user_device_id = $user_device_id ";
		}

		if ($is_active) {
			$sql .= " AND ebay_listing.is_active = '$is_active' ";
		}
		if($item_id){
			$sql .= " AND ebay_listing.item_id = '".db_esc($item_id)."' ";
		}

		if (!$get_total){
		    $sql .= ' GROUP BY ebay_listing.id ';

		    $sql .= " ORDER BY ";

		    if ($order == '') {
		        $sql .= ' ebay_listing.date_added DESC,ebay_listing.description ';
		    }
		    else {
		        $sql .= addslashes($order);
		    }

		    if ($order_asc !== '' && !$order_asc) {
		        $sql .= ' DESC ';
		    }
		}

		if ($limit > 0) {
		    $sql .= db_limit($limit,$start);
		}

		if (!$get_total) {
		    $ret = db_query_array($sql);
		} else {
		    $ret = db_query_array($sql,'',true);
		}

		return $ret;
	}

	static function get1($id)
    {
        $id = (int) $id;
        if (!$id) return false;
        $result = self::get($id);

        return $result[0];
    }

	static function addListing($store_id = 0, $inv_id = 0, $category_id = 92301){
		global $CFG;

		if(!$store_id){
			echo'No store ID<br />';
			return false;
		}

		$record = Inventory::get1($inv_id);

		if( $_SESSION['id'] == 1 )
		{
//		    $item = EbayAPI::getItem( '230519477731', Store::get1( $store_id ) );
//		    print_ar( $item );
//		    return true;
		}

		if(!$record) {
			echo'No Inventory Record <br />';
			return false;
		}

		$product = Products::get1($record['product_id']);
		
		if ($store_id == 3 || $store_id == "3") 
		{
			if ($product['suburban27_title'] != '') $product['name'] = $product['suburban27_title'];
			if ($product['suburban27_desc'] != '') $product['description'] = $product['suburban27_desc'];			 
		}
		else if ($store_id == 4 || $store_id == "4") // sellitforless123
		{
			if ($product['sellitforless123_title'] != '') $product['name'] = $product['sellitforless123_title'];
			if ($product['sellitforless123_desc'] != '') $product['description'] = $product['sellitforless123_desc'];			 
		}

		$product['description'] = utf8_encode( $product['description'] );

		if( $record['product_option_id'] )
		{
		    $option = Products::getProductOption($record['product_option_id'] );
		    $product['vendor_sku'] = $option['vendor_sku'];
		    $product['price'] = $option['additional_price'];
		    $product['name'] .= ' ' . $option['value'];
		    //print_r($product);
		}
		
		// add mfr minimum surcharges
    	$this_brand = Brands::get1($product['brand_id']);
    	if ($this_brand && $this_brand['min_amount'] > 0 && $this_brand['min_charge'] > 0 && $product['price'] < $this_brand['min_amount'])
		{
			$product['price'] += $this_brand['min_charge'];
		}
		
		// set lead time of 6 days for Update International
		if ($product['brand_id'] == 83) $dispatch_max_time = 5;
		else $dispatch_max_time = 2;
		
		$categories = Cats::getCatsForProduct( $product['id'] );
		if( $categories )
		{
		    $tree = Cats::getCatsTree( $categories[count($categories)-1]['id']);
		    $multiple = false;
		    $primaryStoreCategory = 0;
		    $secondaryStoreCategory = 0;
		    $category_id = 0;
		    $sc_o = new EbayStoreCategoriesQuery();
		    $sc_o->store_id = $store_id;
		    foreach( $tree as $c )
		    {
			$sc_o->cat_id = $c['id'];
			$store_cats = EbayStoreCategories::get1ByO($sc_o);

			if( $c['ebay_cat_id'] || $store_cats['ebay_store_cat_id'] )
			{
			    if( $c['ebay_cat_id'] )
				$category_id = $c['ebay_cat_id'];

			    if( $store_cats['ebay_store_cat_id'] )
				$primaryStoreCategory = $store_cats['ebay_store_cat_id'];
			    if( $store_cats['ebay_store_cat_id2'] )
				$secondaryStoreCategory = $store_cats['ebay_store_cat_id2'];
			}

		    }
		}

		$product_image_url = Catalog::makeProductImageLink($record['product_id'],false,false,true);
//mail("rachel@tigerchef.com", $product_image_url, "");
		if( $CFG->in_testing )
		{
			return true;
		}
		
		if ($product['price'] < 1.00) $product['price'] = "1.00";
		//Need to check for an existing listing for this product, from this store, if there is one, do not add a duplicate

		$store_o = (object) array();
		$store_o->ebay_store_id = $store_id;
		$store_o->product_id = $product['id'];
		$store_o->product_option_id = $option['id'];
		
		$current_listing = EbayListing::getByO( $store_o );
		
		if( $current_listing )
		{
		    $active_listing = false;
		    $store = store::get1( $store_id );
		    $active_listings = array();
		    foreach( $current_listing as $c )
		    {
			$item = EbayAPI::getItem( $c['item_id'], $store );

			$item_end_date = strtotime( $item['end_time'] );
			$cur_time = time();
			
			if( $item_end_date > $cur_time ) {
				//echo "active_listing true for item ".$c['item_id']." $item_end_date and $cur_time";
			    $active_listing = true;
			    $active_listings[] = $c;
			}
			else {
			    //Update the listing to inactive
			    $update_info['is_active'] = 'N';
			    EbayListing::update( $c['id'], $update_info );
			}
		    }
		    
		    if( $active_listing ) {
		    	
		    		$response = EbayAPI::reviseItem($active_listings[0]['item_id'], false, $product['name'], $product['description'],
				      $product['price'], "0.00", $product_image_url, $category_id, 0,
				      $primaryStoreCategory, $secondaryStoreCategory, $store_id, $record['qty'],
				      $product['vendor_sku'], $product['weight'], $product['length'], $product['width'], $product['height'], $dispatch_max_time, $product['priced_per']);
/*			echo '<b>This item is already listed on eBay for ' . $store['name'] . '<br />';
			foreach( $active_listings as $c )
			{
			    echo 'eBay Item ID: ' . $c['item_id'] . '<br />';
			}
			echo'</b>';
			return false;*/
//		    echo "response is $response";
		    if ($response) echo "<br/>Successfully revised eBay Item ID ". $active_listings[0]['item_id'] . "<br />";
		    else echo "Revision of eBay Item ID ". $c['item_id'] . " did not succeed.<br />";
		    return $ret;
		    }
		}		
		$verify = EbayAPI::addItem($product['name'],$product['description'],$category_id,'United States',
					$product['price'],$product['price'],'FixedPriceItem','GTC',$record['qty'],true,
					$product_image_url,$CFG->ebay_paypal_email, $store_id, $secondaryCategory, 
					$primaryStoreCategory, $secondaryStoreCategory,$product['vendor_sku'], $product['weight'], 
					$product['length'], $product['width'], $product['height'], $dispatch_max_time, $product['priced_per']);

		if($verify){
			$ret = EbayAPI::addItem($product['name'],$product['description'],$category_id,'United States',$product['price'],$product['price'],'FixedPriceItem','GTC',$record['qty'],false,$product_image_url,$CFG->ebay_paypal_email, $store_id, $secondaryCategory,$primaryStoreCategory,$secondaryStoreCategory,$product['vendor_sku'], 
			$product['weight'], $product['length'], $product['width'], $product['height'], $dispatch_max_time, $product['priced_per']);

			$total_fee = 0.0;
			if(is_array($ret['fees']))foreach($ret['fees'] as $feeName => $fee){
				$total_fee += $fee;
			}

			$info = array(
				'product_id' => $product['id'],
				'name' => $product['name'],
				'inventory_id' => $inv_id,
				'ebay_store_id' => $store_id,
				'item_id' => $ret['id'],
				'start_date' => date('Y-m-d H:i:s'),
				'end_date' => $ret['end_time'],
				'quantity' => $record['qty'],
				'estimated_fee' => $total_fee
			);
			$ebay_listing_id =  self::insert($info);
			return Inventory::update( $inv_id, array( 'ebay_listing_id'=>$ebay_listing_id ) );
		}else{
			return false;
		}
	}


    /**
     * Get By Object Parameters
     * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
     */
	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT ebay_listing.* ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(ebay_listing.id)) as total ";
		}

		$from_ = " FROM ebay_listing ";

		if( isset( $o->active_product ) )
			$join_ .= " LEFT JOIN products on products.id = ebay_listing.product_id ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND ebay_listing.id = [id] ";
		}

		if( isset( $o->ebay_store_id )  ){
			$where_ .= " AND `ebay_listing`.ebay_store_id = [ebay_store_id]";
		}
		if( isset( $o->name )  ){
			$where_ .= " AND `ebay_listing`.name = [name]";
		}
		if( isset( $o->description )  ){
			$where_ .= " AND `ebay_listing`.description = [description]";
		}
		if( isset( $o->item_id )  ){
			$where_ .= " AND `ebay_listing`.item_id = [item_id]";
		}
		if(is_array($o->start_date)){
			if($o->start_date['start']){
				$o->start_date_start = $o->start_date['start'];
				$where_ .= " AND `ebay_listing`.start_date >= [start_date_start]";
			}
			if($o->start_date['end']){
				$o->start_date_end = $o->start_date['end'];
				$where_ .= " AND `ebay_listing`.start_date <= [start_date_end]";
			}
		} else 		if( isset( $o->start_date )  ){
			$where_ .= " AND `ebay_listing`.start_date = [start_date]";
		}
		if(is_array($o->end_date)){
			if($o->end_date['start']){
				$o->end_date_start = $o->end_date['start'];
				$where_ .= " AND `ebay_listing`.end_date >= [end_date_start]";
			}
			if($o->end_date['end']){
				$o->end_date_end = $o->end_date['end'];
				$where_ .= " AND `ebay_listing`.end_date <= [end_date_end]";
			}
		} else 		if( isset( $o->end_date )  ){
			$where_ .= " AND `ebay_listing`.end_date = [end_date]";
		}
		if( isset( $o->estimated_fee )  ){
			$where_ .= " AND `ebay_listing`.estimated_fee = [estimated_fee]";
		}
		if( isset( $o->quantity )  ){
			$where_ .= " AND `ebay_listing`.quantity = [quantity]";
		}
		if( isset( $o->quantity_sold )  ){
			$where_ .= " AND `ebay_listing`.quantity_sold = [quantity_sold]";
		}
		if( isset( $o->product_id )  ){
			$where_ .= " AND `ebay_listing`.product_id = [product_id]";
		}
		if( isset( $o->product_option_id )  ){
			$join_ .= " LEFT JOIN inventory on ebay_listing.inventory_id = inventory.id";
			$where_ .= " AND `inventory`.product_option_id = [product_option_id]";
		}
		if( isset( $o->is_active )  ){
			$where_ .= " AND `ebay_listing`.is_active = [is_active]";
		}
		if(is_array($o->date_added)){
			if($o->date_added['start']){
				$o->date_added_start = $o->date_added['start'];
				$where_ .= " AND `ebay_listing`.date_added >= [date_added_start]";
			}
			if($o->date_added['end']){
				$o->date_added_end = $o->date_added['end'];
				$where_ .= " AND `ebay_listing`.date_added <= [date_added_end]";
			}
		} else 		if( isset( $o->date_added )  ){
			$where_ .= " AND `ebay_listing`.date_added = [date_added]";
		}
		if(is_array($o->date_modified)){
			if($o->date_modified['start']){
				$o->date_modified_start = $o->date_modified['start'];
				$where_ .= " AND `ebay_listing`.date_modified >= [date_modified_start]";
			}
			if($o->date_modified['end']){
				$o->date_modified_end = $o->date_modified['end'];
				$where_ .= " AND `ebay_listing`.date_modified <= [date_modified_end]";
			}
		} else 		if( isset( $o->date_modified )  ){
			$where_ .= " AND `ebay_listing`.date_modified = [date_modified]";
		}
		if( isset( $o->inventory_id )  ){
			$where_ .= " AND `ebay_listing`.inventory_id = [inventory_id]";
		}

		if( isset( $o->active_product ) )
		{
		    $where_ .= " AND products.id IS NOT NULL ";
		}

		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY ebay_listing.id DESC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o,'',false,false,'',false);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;
	}

	static function get1ByO( $o )
	{
	    $result = self::getByO( $o );

	    if( $result )
		return $result[0];

	    return false;
	}

	static function getColumns(){
		$sql = "SHOW COLUMNS FROM `ebay_listing`";

		$fields = db_query_array($sql);

		$ret = array();
		foreach($fields as $field){
			$ret[] = $field['Field'];
		}
		return $ret;
	}


}