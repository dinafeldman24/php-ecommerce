<?php

	class HomepageImage {
		
		static function insert( $info )
		{
			return db_insert( 'homepage_image', $info );
		}
		
		static function update( $id, $info )
		{
			return db_update( 'homepage_image', $id, $info );
		}
		
		static function delete( $id )
		{
			return db_delete( 'homepage_image', $id );
		}
		
		static function get( $id=0, $is_active='' )
		{
			$sql = " SELECT * FROM homepage_image ";
			$sql .= " WHERE 1 ";
			
			if( $id )
				$sql .= " AND homepage_image.id = " . (int) $id;
			if( $is_active ){
				$sql .= " AND homepage_image.is_active = '" . addslashes( $is_active ) . "' ";
				$sql .= " AND ( (NOW() BETWEEN homepage_image.start_date AND homepage_image.end_date )
						OR (NOW()> homepage_image.start_date AND homepage_image.end_date = '0000-00-00 00:00:00')) ";
			}
				
			$sql .= " ORDER BY homepage_image.order_fld ";	
				
			return db_query_array( $sql );
		}
		
		static function get1( $id )
		{
			return db_get1( self::get( $id ) );
		}
		
		/**
		 * Get By Object Parameters
		 * Note: to completely ignore a parameter (even empty strings), make sure it is not set.
		 */
		static function getByO( $o=NULL){
			$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";
		
			$select_ = "SELECT homepage_image.* ";
			if($o->total){
				$select_ = "SELECT COUNT(DISTINCT(homepage_image.id)) as total ";
			}
		
			$from_ = " FROM homepage_image ";
		
			$where_ = " WHERE 1 ";
		
			if(isset($o->id)){
				$where_ .= " AND homepage_image.id = [id] ";
			}
		
			if( isset( $o->url )  ){
				$where_ .= " AND `homepage_image`.url = [url]";
			}
			if( isset( $o->alt )  ){
				$where_ .= " AND `homepage_image`.alt = [alt]";
			}
			if( isset( $o->is_active )  ){
				$where_ .= " AND `homepage_image`.is_active = [is_active]";
			}
		
			if( isset( $o->query )  ){
				$where_ .= " AND ( `homepage_image`.url LIKE '%".$o->query."%'
								OR `homepage_image`.alt LIKE '%".$o->query."%'
								OR  `homepage_image`.id LIKE '%".$o->query."%' )";
			}
		
			if(!$o->total){
				if($o->order){
					$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
					if($o->order_asc){
						$orderby_ .= " ASC ";
					} else {
						$orderby_ .= " DESC ";
					}
				}
				else {
					$orderby_ = " ORDER BY homepage_image.id DESC ";
				}
				if($o->limit){
					$limit_ .= db_limit($o->limit,$o->start);
				}
			}
		
			$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;
		
			$result = db_template_query($sql,$o);
		
			if($o->total){
				return (int)$result[0]['total'];
			}
		
			return $result;
		}
		
		static function get1ByO( $o )
		{
			$result = self::getByO( $o );
		
			if( $result )
				return $result[0];
		
			return false;
		}
	}

?>