<?
class UPSRateCalculator{
	static function getZipZone($methodId,$zipcode)
	{	
		$firstThreeDigitsOfZip = substr($zipcode, 0, 3);
		$sql = "SELECT ups_zips_and_zones.$methodId 
				FROM ups_zips_and_zones 
				WHERE '$firstThreeDigitsOfZip' 
				BETWEEN
				ups_zips_and_zones.dest_zip_lower_bound 
				AND
				ups_zips_and_zones.dest_zip_upper_bound";
		
		$result = db_query_array( $sql );
		return $result[0][$methodId]; // returns appropriate zone for inputted method id and zone
	}
	
	static function getUpsRate($rateTableName,$zone, $weight,$comOrRes = "RES")
	{
		$sql = "SELECT $rateTableName.$zone 
				FROM $rateTableName 
				WHERE weight = $weight";
		$result = db_query_array( $sql );
		$basicRate = $result[0][$zone];
		//echo "basicRate is $basicRate";
		return $basicRate;
	}
	static function getUpsRateOver150($zone, $weight,$comOrRes = "RES")
	{
		$sql = "SELECT ups_over_150_rates.$zone 
				FROM ups_over_150_rates";
		$result = db_query_array( $sql );
		$perLbRate = $result[0][$zone];
		$basicRate = $perLbRate * $weight;
		
		return $basicRate;
	}
}
?>