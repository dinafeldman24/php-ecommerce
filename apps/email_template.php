<?

class EmailTemplate{

	static function insert($info){
		return db_insert("email_template",$info);
	}

	static function update($id,$info){

		if(!isset($info['date_modified'])){
			$date_modified = 'date_modified';
		}
		return db_update("email_template",$id,$info,'id',$date_modified);
	}

	static function delete($id){
		return db_delete("email_template",$id);
	}

	static function getByO($o=""){
		$select_ = $from_ = $join_ = $where_ = $groupby_ = $having_ = $orderby_ = $limit_ = "";

		$select_ = "SELECT email_template.*,
		store.name as store_name ";
		if($o->total){
			$select_ = "SELECT COUNT(DISTINCT(email_template.id)) as total ";
		}

		$from_ = " FROM email_template ";

		$join_ .= " LEFT JOIN store ON store.id = email_template.store_id ";

		$where_ = " WHERE 1 ";

		if(isset($o->id)){
			$where_ .= " AND email_template.id = [id] ";
		}
		if(isset($o->name)){
			$where_ .= " AND email_template.name = [name] ";
		}
		if(isset($o->store_id)){
			$where_ .= " AND email_template.store_id = [store_id] ";
		}


		if(!$o->total){
			if($o->order){
				$orderby_ .= " ORDER BY " . db_escape_order_by($o->order);
				if($o->order_asc){
					$orderby_ .= " ASC ";
				} else {
					$orderby_ .= " DESC ";
				}
			}
			else {
				$orderby_ = " ORDER BY email_template.name ASC ";
			}
			if($o->limit){
				$limit_ .= db_limit($o->limit,$o->start);
			}
		}

		$sql = $select_ . $from_. $join_. $where_. $groupby_. $having_ . $orderby_. $limit_;

		$result = db_template_query($sql,$o);

		if($o->total){
			return (int)$result[0]['total'];
		}

		return $result;

	}

	static function get1($id){
		$id = (int) $id;

		if( $id <= 0 ){
			return false;
		}

		$o->id = $id;
		$result = self::getByO($o);

		return $result[0];
	}

	static function getByName($name,$store_id){
		$o->name     = $name;
		$o->store_id = $store_id;
		$ret = self::getByO($o);
		return array_shift($ret);
	}


}