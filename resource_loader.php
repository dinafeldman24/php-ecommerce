<?php
$shut_off_sessions = true;
require_once("application.php");

if($_REQUEST['get_resources'] == 1){
	// Output the resource data

	if( $_REQUEST['type'] == "js" ){
		$type = "js";
		$mimetype = "javascript";
	} else {
		$type = "css";
		$mimetype = "css";
	}

	if(!ResourceLoader::isIE()){
		//ob_start('ob_gzhandler');
		ob_start();
	} else {
		// ob_start();
	}

	header("Content-Type: text/$mimetype");
	header("Expires: ".date(DATE_RFC1123,strtotime("+364 days"))); // Expire in a year
	header("Cache-Control: private, max-age=9999"); // cache for x hours
	header("Pragma:");
	$lmt_arr = array();
	$obj = new ResourceLoader($type,'',$_GET);
	foreach($obj->files as $f){
		$lmt_arr[] = filemtime($f);		
	}
	$output = print_r($lmt_arr, true);
	//mail("rachel@tigerchef.com", 'arr', $output);
	 
	$last_modified_time = max($lmt_arr);
	$etag = md5($last_modified_time); 

	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT"); 
	header("Etag: $etag"); 

	if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time || trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) { 
    		header("HTTP/1.1 304 Not Modified"); 
    		exit; 
	} 
	
	//$output = print_r($_SERVER, true);
	//mail("rachel@tigerchef.com", "output", $output);
	$obj = new ResourceLoader($type,'',$_GET);
	foreach($obj->files as $f){		
		if(!file_exists($f)){
			$sp = explode("/",$f);
			$fname = array_pop($sp);
			echo $fname;
			echo " /* ERROR READING THIS FILE! $fname */ ";
		}
		readfile($f);
	}

	exit;

}
