<?php

include_once('application.php');
include( $CFG->redesign_dirroot . '/includes/review_section.php');

$this_page_type = 'account';
/*
*********************************************
** LOGGED IN
*********************************************
*/
if (MyAccount::logged_in())
{
    $title_override = 'My Account | ' . $CFG->company_name;
    $errors         = '';

    switch ($_REQUEST['action'])
    {
        case 'print_invoice'    :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    //MyAccount::show_title_bar("Invoice For Order # " . $_REQUEST['order_id']);
									echo "<div class='main-section'>";
                                    MyAccount::showInvoice($_REQUEST['order_id'], $_SESSION['cust_acc_id']);
                                    //MyAccount::show_my_account_sidebar();
									echo "</div>";
                                    break;

        case 'show_orders'      :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div>";
									echo "<div class='dashboard main-section'>";
									MyAccount::show_title_bar('Select An Order To Print Invoice');
									MyAccount::show_my_account_sidebar();
									echo "</div>";                 
                                    echo "<div class='account-box main-section'>";			
                                    MyAccount::showOrders('invoice');
									echo "</div>";
									echo "</div>";
                                    break;

        case 'open_orders'      :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Viewing Open Orders');
									MyAccount::show_my_account_sidebar();           
									echo "</div>";
									echo "<div class='account-box main-section'>";
                                    MyAccount::showOrders('open');
									echo "</div>";									
									echo "</div>";
                                    break;

        case 'old_orders'       :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div>";
									echo "<div class='dashboard main-section'>";
									MyAccount::show_title_bar('Viewing All Order History');
									MyAccount::show_my_account_sidebar();
                                    echo "</div>";
									echo "<div class='account-box main-section'>";
                                    MyAccount::showOrders();
									echo "</div>";
									echo "</div>";
									break;
        
        case 'order_details'    :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div>";
									echo "<div class='dashboard main-section'>";
									MyAccount::show_title_bar("Order Details--Order # " . $_REQUEST['order_id']);
									MyAccount::show_my_account_sidebar();
									echo "</div>";
                                    MyAccount::showOrderDetails($_REQUEST['order_id']);
									echo "</div>";
                                    break;                                    
            case 'quick_reorder'	:	$orders          = Orders::get($_REQUEST['order_id'],$_SESSION['cust_acc_id']);
        							if (is_array($orders)) $order = $orders[0];
									$orderitems     = Orders::getItems(0,$_REQUEST['order_id']);
    								global $cart;
									if (!$order) 
									{
										echo "Invalid order number ". $_REQUEST['order_id'];
										break;
									}
        							$addError = "";
									foreach ($orderitems as $this_item)
        							{ 
        								if ($this_item['is_active'] == 'N' || $this_item['is_deleted'] == 'Y')
        								{
        									$addError .= "<p>" . $this_item['product_name'] . " could not be added to the cart because it is no longer available.  All available items from order " . $_REQUEST['order_id'] . " have been added to your cart.</p>";
        									continue;
        								}										$product_id = $this_item['product_id'];
                  						$quantity = $this_item['qty'];

		  								if( $this_item['product_options_id'] )
		  								{
		    								$option = $this_item['product_options_id'];
									  	}        	 
        								$addError = "";

										if ($product_id > 0 && $quantity > 0) 
										{
    										$error = $cart->add($product_id, $quantity, $option);

										    if ($error && !is_numeric($error) ) 
										    {
        										$addError .= "<p>$error</p>";
    										}
										}
									}
									if ($addError) 
									{
										include_once($CFG->redesign_dirroot . '/includes/header.php');
										 echo "<div>";
									    echo "<div class='dashboard main-section'>";
                                    	MyAccount::show_title_bar("My Account");
										MyAccount::show_my_account_sidebar();
										echo "</div>";
										echo "<div class='account-box main-section'>";
                                    	MyAccount::show_my_account_landing($addError);
										echo "</div>";
										echo "</div>";
									}
									else header("location: ".$CFG->baseurl."cart.php"); 								
									break;
									
        case 'change_shipping'  :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Update Shipping Address');
									MyAccount::show_my_account_sidebar();             
									echo "</div>";
									MyAccount::showShippingAddressForm();
									echo "</div>";
                                    break;


        case 'change_billing'   :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Update Billing Address');
									MyAccount::show_my_account_sidebar();
                                    echo "</div>";
                                    MyAccount::showBillingAddressForm();
									echo "</div>";
                                    break;

        case 'track_packages'   :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Track Packages');
									MyAccount::show_my_account_sidebar();
									echo "</div>";
									echo "<div class='account-box main-section'>";
                                    MyAccount::showTrackPackages();
									echo "</div>";                                    
									echo "</div>";
                                    break;

        case 'update_shipping'  :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    if (!MyAccount::update_address($_POST['info'], $errors, 'shipping'))
                                    {
										echo "<div>";
									    echo "<div class='dashboard main-section'>";
                                        MyAccount::show_title_bar("Update Shipping Address");
										MyAccount::show_my_account_sidebar();
										echo "</div>";	
										MyAccount::showShippingAddressForm($_POST['info'], $errors);
										echo "</div>";
									                                   
                                    }
                                    else
                                    {

                                      echo "<div>";
									  echo "<div class='dashboard main-section'>";
                                        MyAccount::show_title_bar("My Account");       
										MyAccount::show_my_account_sidebar();
									  echo "</div>";	
                                        MyAccount::show_my_account_landing("Shipping Address updated successfully.<br/>");
									  echo "</div>";
                                    }

                                    break;

        case 'update_billing'   :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    if (!MyAccount::update_address($_POST['info'], $errors, 'billing'))
                                    {
                                      echo "<div>";
									  echo "<div class='dashboard main-section'>";
									    MyAccount::show_title_bar("Update Billing Address");
										MyAccount::show_my_account_sidebar();
									 echo "</div>";	
                                        MyAccount::showBillingAddressForm($_POST['info'], $errors);
                                    echo "</div>";									
                                    }
                                    else
                                    {
									    echo "<div>";
									    echo "<div class='dashboard main-section'>";
                                        MyAccount::show_title_bar("My Account");
										MyAccount::show_my_account_sidebar();
                                        echo "</div>";
                                        MyAccount::show_my_account_landing("Billing Address updated successfully.<br/>");
										echo "</div>";
                     
                                    }

                                    break;

        case 'change_email'     :
        case 'change_password'  :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Update Account Information');
									MyAccount::show_my_account_sidebar();            
									echo "</div>";
                                    MyAccount::editInfo();
									echo "</div>";
									

                                    break;
		case 'upgrade'  :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar('Update Account Information');
									MyAccount::show_my_account_sidebar();            
									echo "</div>";
									echo "<div class='account-box main-section'>";
                                    MyAccount::editInfo('', '', true);
									echo "</div>";
									echo "</div>";
									

                                    break;
        case 'update_account'   :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    if (!MyAccount::updateInfo($_POST['info'], $errors))
                                    {
										echo "<div>";
									    echo "<div class='dashboard main-section'>";
                                        MyAccount::show_title_bar('Update Account Information');
										MyAccount::show_my_account_sidebar();       
										echo "</div>";
										MyAccount::editInfo($_POST['info'], $errors);
										echo "</div>";
                                    }
                                    else
                                    {
                                        //MyAccount::show_title_bar('Update Account Information');
                                        //MyAccount::editInfo('', "Account updated successfully.<br/>");
                                        echo "<div>";
									    echo "<div class='dashboard main-section'>";
                                    	MyAccount::show_title_bar("My Account");
										MyAccount::show_my_account_sidebar();
										echo  "</div>";
                                        MyAccount::show_my_account_landing("Account updated successfully.<br/>");
										echo "</div>";
										
                                    }

                                    break;
		case 'write_review'    :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
                                    MyAccount::show_title_bar("Write Review");
									MyAccount::show_my_account_sidebar();
									echo "</div>";
                                    MyAccount::showReviewSection();
									echo "</div>";                                    
                                    break;                                    

        case 'logout'           :   MyAccount::logout();
                                    include_once($CFG->redesign_dirroot . '/includes/header.php');
									echo "<div class='main-section'>";
                                    MyAccount::show_title_bar("Account Login");
									//MyAccount::show_my_account_sidebar();
									MyAccount::showLoginForm('You have successfully logged out.<br/>');
									echo "</div>";
                                    break;
                                    
        case 'cancel'           :
        default                 :   include_once($CFG->redesign_dirroot . '/includes/header.php');    
		                            echo "<div>";
									echo "<div class='dashboard main-section'>";
									MyAccount::show_title_bar("My Account");
									MyAccount::show_my_account_sidebar();                
                                    echo "</div>";			
									 MyAccount::show_my_account_landing();
									echo "</div>";
                                    break;
    }
}
/*
*********************************************
** NOT LOGGED IN
*********************************************
*/
else
{
    $title_override = "Join Now | " . $CFG->company_name;
    $errors         = '';
    $missing        = array();

    /* handle the specific action */
    switch ($_REQUEST['action'])
    {
        case 'login'            :   
    						        if($_REQUEST['logged_in_time'] == 'checkout'){
						        		$_SESSION['logged_in_time'] = 'checkout';
						        	}
        							if (!MyAccount::login($_POST['log'], $_POST['pwd'], true))
                                    {
                                    	unset($_SESSION['logged_in_time']);
                                    	if (MyAccount::login($_POST['log'], $_POST['pwd'], false))
                                    	{
                                    		MyAccount::logout();
                                    		include_once($CFG->redesign_dirroot . '/includes/header.php');
		                                    echo "<div class='main-section'>";
                                        	MyAccount::show_title_bar("Account Login");
                                        	MyAccount::showLoginForm("Your account has been created but not activated.  Please activate your account by clicking on the link that was sent to you by email upon completion of account creation.");
                                        	echo "</div>";
                                    	}
                                        else 
                                        {
                                        	include_once($CFG->redesign_dirroot . '/includes/header.php');                        echo "<div class='main-section'>";
                                        	MyAccount::show_title_bar("Account Login");
                                        	MyAccount::showLoginForm("We're sorry, that login is not valid.");
											echo "</div>";                          	
                                        }
                                    }
                                    else
                                    {
                                        /* check to see if we have a redirect first */
                                        if ($_REQUEST['redirect'])
                                        {
                                            header(sprintf("Location: %s", $_REQUEST['redirect']));
                                        }
                                        else
                                        {
                                            header(sprintf("Location: %saccount.php", $CFG->sslurl));
                                        }

                                        exit(0);
                                    }

                                    break; 

        case 'create_account'   :   if (!$_POST['info2']) $_POST['info2'] = array();
        							if (!MyAccount::createAccount($_POST['info'], $errors, $_POST['info2']))
                                    {
                                        include_once($CFG->redesign_dirroot . '/includes/header.php');
	                                    echo "<div class='main-section'>";

                                        MyAccount::show_title_bar("Create Account");
                                        MyAccount::newAccount($_POST['info'], $errors, $_POST['info2']);
										echo "</div>";
                                    }
                                    else
                                    {
                                    	// TE - changed to not require email activation
                                        //header("Location: {$CFG->sslurl}account.php?newacct=Y&email=".$_POST['info']['email']);
                                    	header("Location: {$CFG->sslurl}account.php?track_new_user=y");
                                    }

                                    break;

        case 'new_user'         :   $title_override = 'Create a Restaurant Supply Account, Become a TigerChef | ' . $CFG->company_name;
        							$keywords = '';
        							$description = '';
        							include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div class='main-section'>";
                                    MyAccount::show_title_bar("Create Account");
                                    MyAccount::newAccount();
									echo "</div>";
                                    break;

        case 'forgot_password'  :   include_once($CFG->redesign_dirroot . '/includes/header.php');
		                            echo "<div class='main-section'>";
                                    MyAccount::show_title_bar("Forgot Password");
                                    MyAccount::forgotPassword();
									echo "</div>";                      
                                    break;

        case 'recover_password' :   include_once($CFG->redesign_dirroot . '/includes/header.php');
        							$info = MyAccount::recoverPassword($_REQUEST['email']);
        							$error = $info['error'];
									$message = $info['message'];
									$success = $info['success'];
                                    echo "<div class='main-section'>";
									if ($success)
									{
										MyAccount::show_title_bar("Account Login");
										MyAccount::showLoginForm($message);
										
									}
									else 
									{
										MyAccount::show_title_bar("Recover Password");
                                    	MyAccount::forgotPassword($_REQUEST['email']);
                                    	
									}
									echo "</div>";
                                    break;
        case 'activate_account'  :  include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div class='main-section'>";
                                    MyAccount::show_title_bar("Account Login");
                                    MyAccount::showLoginForm('Your account has been activated.  Please sign in and start shopping!<br/>');
                                    echo "</div>";
                                    break;
        case 'login_screen':                                     
        default                 :   include_once($CFG->redesign_dirroot . '/includes/header.php');
                                    echo "<div class='main-section'>";
									MyAccount::show_title_bar("Account Login");
    								if ($_REQUEST['newacct'] == "Y") 
                                    {
                                    	$msg = "Registration was successful!<br/>
                                    	A confirmation email has been sent to ".$_REQUEST['email'].". You must click the link in the email to finish activating your account and to take advantage of all the site features as a TigerChef subscriber.";
                                    }
                                    MyAccount::showLoginForm($msg);
									echo "</div>";
                                    break;
    }
}

include_once($CFG->redesign_dirroot . '/includes/footer.php');
?>
<script type="text/javascript" src="js/rating.js"></script>