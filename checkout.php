<? 
include_once('application.php');
$this_page_type = 'checkout';


global $cart; 

$pageAction = $_REQUEST['pageAction'];
$passed_validation  = true;
$passed_validation_billing  = true;
$passed_validation_shipping  = true;
$confirm_info = '';
$err_msgs           = array();
$validation         = array();
$tigerchef_cart     = $cart->get();

$paypal_checkout = ($pageAction == 'paypal') ? true : false;

$checkout = new Checkout($paypal_checkout,$_REQUEST);
/* this is just the session data stored from the previous steps */   
$cart1		= unserialize($_SESSION['CART1']);
$cart2		= unserialize($_SESSION['CART2']);
$cart3		= unserialize($_SESSION['CART3']); 
ob_start();
		?>
		<script>
		gtag('event', 'begin_checkout', {
		  "items": [
		<?$i=0;                   
		foreach ($tigerchef_cart as $item){
			$product              = Products::get1($item['product_id']);
			$product_sku          = $product['mfr_part_num'];
			$product_name         = $product['name'];
			?>
			  {
				'name': '<? echo htmlspecialchars(str_replace("'","",$product_name), ENT_QUOTES)?>',     // Name or ID is required.
				'id': '<?=$item['product_id']?>',
				'price': '<?=$item['price']?>',
				'quantity': '<?=$item['qty']?>',
				'category': '<?=str_ireplace(array('"','/'),'',$item['cat_name'])?>',
				'brand': '<?=str_ireplace(array('"','/'),'',$item['brand_name'])?>',
				'list_position':'<?=$i+1?>'
			  }
		   <?
			$i++;
			if ($i<count($tigerchef_cart)) echo ",";
			}
			?>
			],
		  "coupon": ""
		});


	
		</script>
		<?
		$google_tag_mgr_data_layer = ob_get_contents();
		ob_end_clean();
		
include( $CFG->redesign_dirroot . '/includes/header.php');
function build_expiration_year_dropdown()
{ 
	global $cart3;
	?> 
	<select class="select small" name="cc_expiration_year" id="cc_expiration_year">
	<option value="0">Year</option>
	<?
	for ($counter = 0, $year = date("Y"); $counter < 20; ++$year, ++$counter)
	{
		?>
		<option value="<?=$year?>" <?=($cart3['cc_expiration_year'] == $year) ? "selected" : ""?>><?=$year?></option>
		<?
	}
	?>
	</select>
    <?
}

function build_expiration_month_dropdown()
{
	global $cart3;
	
// 	$months = array("Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$months = array("Month", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
	
	?>
	<select class="small select" name="cc_expiration_month" id="cc_expiration_month">
	<?
	for ($i = 0; $i <= 12; ++$i)
	{
		?>
		<option value="<?=$i?>" <?=($cart3['cc_expiration_month'] == $i) ? 'selected' : ''?>><?=$months[$i]?></option>
		<? 
	}
	?>
	</select>
	<?
}
         
function show_error_messages()
{
	global $passed_validation;
	global $validation;
	global $err_msgs;

	return (!$passed_validation) ? implode('<br/>', $err_msgs) : "";
}

function merge_request_with_session()
{
	if ($_SESSION['CART2'])
	{
		$_REQUEST = array_merge($_REQUEST, unserialize($_SESSION['CART2']));
	}                
    return true;
}

function make_continue_shopping_link()
{
	global $CFG;
	$product    = null;

	if ($_REQUEST['product_id'] != '')
	{
		$product = Products::get1($_REQUEST['product_id']);
		if ($product)
		{
			return Catalog::makeProductLink_($product);
		}
	}
	else if ($_SESSION['prev_prod_page_link'] != "") return $_SESSION['prev_prod_page_link'];

	$referer = $_SERVER['HTTP_REFERER'];
	if ($referer != '') return $referer;
	else return "javascript:history.go(-1)";
}

function run()
{
	global $pageAction;
	global $passed_validation;
	global $passed_validation_billing;
	global $passed_validation_shipping;
	global $text_to_print;
	global $CFG;
	global $checkout;
	global $confirm_info;
	global $err_msgs;
    global $cart;
	switch ($pageAction){

		case "change_address"       :   /* this will just simply merge our session data (POST) with the request so we can
			have the fields below filled in automatically */
			merge_request_with_session();
			break;	
		case "paypal"				:
		    
			merge_request_with_session();
			$logged_in = (MyAccount::logged_in())? 'true' : '';
			?>
				<input id="paypal_token" type="hidden" value="<?=$_REQUEST['token']?>"/>
				<input id="payer_id" type="hidden" value="<?=$_REQUEST['PayerID']?>"/>
			
			<?php
			

			$confirm_info = $checkout->createConfirmOrder();
			$cart->data['zipcode'] = $checkout->cart2['shipping_zip'];

			break;
	
		default                     :
			/* do this in case they continue to shop around so they don't have to enter billing
			and shipping informformation again */
			merge_request_with_session();
		    if ($_REQUEST['shipping_zip'])
                        $cart->data['zipcode'] = $checkout->cart2['shipping_zip'] = $_REQUEST['shipping_zip'];	
			/* if we are logged in then get address info from account info */
			if (MyAccount::logged_in())
			{
				if(!$_REQUEST['nopopulate']){
					Checkout::populate_address_fields();

				}
			}
 
			break;
	}
}

run();

show_error_messages();
if (show_error_messages())
	$checkemail = $_REQUEST['email_offer'];
else
	$checkemail = true;

$paypal_class = $checkout->paypal_checkout ? 'paypal-express-checkout' : '';
$cart_items_display = $checkout->display_cart_items();
?>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>-->
<div class="checkout">
	<div class="main-section <?=$paypal_class?>">
		<h1>Checkout</h1>
			<?php 
		/* make sure we have items in the cart */
        Cart::getCartStatsShort($item_count, $subtotal);
		if ($item_count > 0)
		{ 
			/* show cart reference */
			Cart::show_cart_reference(); 
			/* show yom_tov message if applicable */ 
			Checkout::show_yomtov_message();
			?>			
			<div class="checkout-container clearfixed" id="checkout-container">
			<!--Cart Summary
			<div class="cart-summary-wrapper" id="sidecart">
				<h4 class="checkout-cart-header">
					<span>YOUR CART</span>
					<span class="cart-count"><?=$item_count?> Item<?=($item_count>1)?'s':''?></span>
					<a class="edit-cart" href="<?=$CFG->baseurl . 'cart.php'?>" onclick="sendToDataLayer('Checkout', 'Edit cart');">Edit cart</a>
				</h4>
				<div class="cart-items">
					<?php 
					//echo $cart_items_display;
					?>
				</div>
				<h3 class="checkout-cart-header">
					<span>CART SUMMARY</span>
				</h3>
				<div class="cart-cost-summary">
					<?php  
					//echo $checkout->create_cost_breakdown_table(); //calculates the costs for the order
					?>
				</div>
			</div>-->
			
			<!--Checkout Options-->
			<div class="checkout-steps-wrapper">
			 <form class="form checkout-form" id="checkout_form" method="POST" action=" autocomplete="off">
				<div class="logging-bar">
						<?php 
						if(!$_SESSION['cust_acc_id']  &&  (!$paypal_checkout)){
						?> 
						<span>Fill in the fields below to complete your purchase! <a href="#"
							onclick="gtag('event', 'Click to log in', {'event_category': 'checkout', 'value': 'Click to log in'});TINY.box.show({url:'popuplogin.php',post:'link=<?=$_SERVER['PHP_SELF']?>&type=checkout_login',fixed:false});return false;">Already registered? Click here to login </a>
						</span>
						
						<?php 
						}else{
							 echo "You are checking out as ";
							 if ($paypal_checkout){
								 ?>
								<?=$confirm_info['email']?>
						        <a href='<?=$CFG->Paypal_API_Express_Checkout_Redirect . "&token=" .$checkout->paypal_token?>'>Edit</a>
					          <?}	
							else echo $_SESSION['cust_username'];
						}
						?>
					</div>
			
				<!--Shipping Details-->
				<div class="errorBox hidden" id="confirm_order_errors"></div>
				<div class="checkout-col col1">
				<h3 class="checkout-step" id="shipping-details-step">
					Shipping Details
				</h3>
				<div id="shipping-details" class="step-holder">
<!-- 					<div class='errorBox' id='shipping_errors'></div> -->
					<input type="hidden" id="stop_for_no_match_shipping" name="stop_for_no_match_shipping" value="<?= $_REQUEST['stop_for_no_match_shipping']? $_REQUEST['stop_for_no_match_shipping']: "yes"?>">
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="shipping_fname">First Name: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="shipping_fname" id="shipping_fname" value="<?=$_REQUEST['shipping_fname']?>" type="text" autocomplete="nope" />
							</div>
						</div>
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="shipping_lname">Last Name:</label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="shipping_lname" id="shipping_lname" value="<?=$_REQUEST['shipping_lname']?>" type="text" autocomplete="off"/>
							</div>
						</div>
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="shipping_company">Company: </label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="shipping_company" id="shipping_company" value="<?=$_REQUEST['shipping_company']?>" type="text" />
							</div>
						</div>
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="shipping_email">Email: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<span> <input class="text" name="shipping_email"
										id="shipping_email" value="<?=$_REQUEST['shipping_email']?$_REQUEST['shipping_email']:($_SESSION['cust_username']?$_SESSION['cust_username']:'')?>"
										type="email" 
										autocomplete="off"/>
									</span>
<!--									<label class="email_offer"> <input class="check"
										name="email_offer" id="email_offer" type="checkbox" value="1"
										<?//=($checkemail)?"checked":null?> /> <span>Yes, send me e-mail
											offers!</span>
									</label>-->
							</div>
                    </div>
					<div id='shipping-address-info'>
							<div id='shipping_form'>
							<div class="full-row">
									<div class="field-holder checkout">
									<div class="label-holder">
								         <div class="label-box">
									        <label for="shipping_address">Address line 1: 
											<sup class="required-star">*</sup></label>
								         </div>
							          </div>
									<div class="icon place"></div>
									<input class="text with-icon addressshipping-to-billing"
										name="shipping_address"
										id="shipping_address"
										value="<?=$_REQUEST['shipping_address']?>"
										type="text" autocomplete="off"
										/>
									<div class="inline_error_msg with-icon hidden">Enter address line 1</div>
								</div>
							</div>
							<?php //$hide_shipping_fields = ($_REQUEST['shipping_address2'] || $_REQUEST['shipping_city'] || $_REQUEST['shipping_state'] || $_REQUEST['shipping_zip']) ? '' : 'hidden'?>
							<div class="hidden-info-holder shipping <?= $hide_shipping_fields ?>">
								<div class="full-row">
									<div class="field-holder checkout">
									<div class="label-holder">
								         <div class="label-box">
									        <label for="shipping_address2">Address line 2:</label>
								         </div>
							          </div>
										<input class="text shipping-to-billing optional" name="shipping_address2" 
											id="shipping_address2" autocomplete="off"
											value="<?=$_REQUEST['shipping_address2']?>" type="text" />
										<div class="inline_error_msg hidden">Enter address line 2</div>
									</div>
								</div>

								<div class="full-row">
									<div class="field-holder inline">
										<div class="shipping-city-holder holder half-row">
										<div class="label-holder">
								         <div class="label-box">
									        <label for="shipping_city">Shipping city
											<sup class="required-star">*</sup></label>
								         </div>
							          </div>
											<input class="text shipping-to-billing" name="shipping_city"  id="shipping_city" autocomplete="off"
												value="<?=$_REQUEST['shipping_city']?>" type="text" />
											<div class="inline_error_msg hidden">Enter a shipping city</div>
										</div>
										<div class="shipping-state-holder holder half-row">
										<div class="label-holder">
								         <div class="label-box">
									        <label for="shipping_state">State
											<sup class="required-star">*</sup></label>
								         </div>
							          </div>
											<?=stateSelect("shipping_state", $_REQUEST['shipping_state'],'',true,'Please Select:','','','','','','select shipping-to-billing','','','',"shipping_state",'')?>
											<div class="inline_error_msg hidden">Enter a shipping state</div>
										</div>
										
									</div>
								</div>
								
								<div class="shipping-zip-holder holder half-row">
										 <div class="label-holder">
								          <div class="label-box">
									        <label for="shipping_zip">Shipping Zip
											<sup class="required-star">*</sup></label>
								          </div>
							             </div>
											<input class="text shipping-to-billing" name="shipping_zip"  id="shipping_zip" autocomplete="off"
												value="<?=$_REQUEST['shipping_zip']?>" type="tel" />
											<div class="inline_error_msg hidden">Enter a valid zip</div>
								</div>
								<div class="half-row">
							        <div class="label-holder"> 
								       <div class="label-box">
									       <label for="shipping_home_phone">Phone: <sup class="required-star">*</sup></label>
								      </div>
							        </div>
							        <div class="field-holder checkout">
								         <input class="text" name="shipping_home_phone" id="shipping_home_phone" value="<?=$_REQUEST['shipping_home_phone']?>" type="tel" autocomplete="off"/>
							       </div>
						         </div>
							</div>
						</div>
						</div>

						<input name="shipping_location" id="shipping_locationC" value="commercial" type="hidden"/>
									<input name="shipping_location" id="shipping_locationR" value="residential" type="hidden" checked="checked"/>
						
						<div class="half-row">
							<div class="label-holder"> 
								<div class="label-box">
									<label for="institution_type_id">Business Type: </label>
								</div>
							</div>
							<div class="field-holder checkout">
								<?php 
								$institution_types = MyAccount::getAllCustomerInstitutionTypes(true);
								echo StdLib::getSelectTagFromHashtable ( "institution_type_id", $institution_types, $_REQUEST['institution_type_id'], 'id', 'caption','Please Select:','','select',false,false,'institution_type_id',false,'');
								?>
							</div>
						</div>	
						
						<div class="step-bottom">
							<div class="btn-holder">
								<label for="for-shipping">
									<input class="check" id="shipping-same-as-billing" name="shipping-same-as-billing" type="checkbox" value="1" <?=($_REQUEST['shipping-same-as-billing'] == 1) ? "checked" : ""?> onclick="copy_billing_info_to_shipping();"/>
									<span>Billing address same as shipping</span>
								</label>
								<!--<button class="btn continue-btn" onClick="sendToDataLayer('Checkout', 'continue --- step 1');return validateShippingBilling('shipping');">Continue</button>-->
							</div>
						</div> 
				</div>
				<div class="paypal-return-step"	id="shipping-details-paypal-return">
						<div>
							<ul>
								<li><?=$confirm_info['shipping_address']?></li>
							</ul>
						</div>

                      <? if ($paypal_checkout){?>
						<a
							href='<?=$CFG->Paypal_API_Express_Checkout_Redirect . "&token=" .$checkout->paypal_token?>'>Edit</a>
					   <?}?>		
				</div>
				</div>
				<div class="checkout-col col2 ">
				<!--Shipping method-->
				<h3 class="checkout-step " id="shipping-method-step">Shipping method</h3>
				<div id="shipping-method" class="">
						<div class="shipping-holder" style="min-width:300px;">
							<div class="column shipping-method-holder" id="shipping_cost" >
						     <?php  
                             $checkout->calculate_sales_tax_and_shipping();
									if($checkout->cart2['shipping_zip']){
										echo $cart->build_shipping_method_dropdown($checkout->shipping_discount,$checkout->tax_rate,$checkout->cart2['shipping_zip'],true,$checkout->shipping_cost_options,true);
									}
						    ?>
							Type in your your address and your shipping rates will update based on your location.</span>
							</div>
														
							<div class="full-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="shipping_address">Comments:</label>
								</div>
							</div>
							<div class="field-holder">
								<textarea name="special_comments" id="special_comments" cols="30" rows=""><?=$cart->data['special_comments']?></textarea>
							</div>
						</div>

							
						</div>
						<div class="step-bottom"></div>
				</div>
				<?
				$billing_city_state_zip .= ($_REQUEST['billing_city']) ? $_REQUEST['billing_city'] .", " : '';
								$billing_city_state_zip .= ($_REQUEST['billing_state']) ? $_REQUEST['billing_state'] .", " : '';
								$billing_city_state_zip .= ($_REQUEST['billing_zip']) ? $_REQUEST['billing_zip'] : '';

								$has_billing_info = ($_REQUEST['billing_fname'] || $_REQUEST['billing_address'] || $_REQUEST['billing_address2'] || $billing_city_state_zip) ? true : false;

								if($has_billing_info){
									if( ($_REQUEST['billing_fname'] != $_REQUEST['shipping_fname']) ||
										($_REQUEST['billing_address'] != $_REQUEST['shipping_address']) ||
										($_REQUEST['billing_address2'] != $_REQUEST['shipping_address2']) ||
										($_REQUEST['billing_city'] != $_REQUEST['shipping_city']) ||
										($_REQUEST['billing_state'] != $_REQUEST['shipping_state']) ||
										($_REQUEST['billing_zip'] != $_REQUEST['shipping_zip'])
										){
										$shipping_same_as_billing = false;
									}else{
										$shipping_same_as_billing = 'yes';
									}
								}else{
									$shipping_city_state_zip .= ($_REQUEST['shipping_city']) ? $_REQUEST['shipping_city'] .", " : '';
									$shipping_city_state_zip .= ($_REQUEST['shipping_state']) ? $_REQUEST['shipping_state'] .", " : '';
									$shipping_city_state_zip .= ($_REQUEST['shipping_zip']) ? $_REQUEST['shipping_zip'] : '';

									$has_shipping_info = ($_REQUEST['shipping_fname'] || $_REQUEST['shipping_address'] || $_REQUEST['shipping_address2'] || $_REQUEST['shipping_city'] || $_REQUEST['shipping_state'] || $_REQUEST['shipping_zip']) ? true : false;
									if($has_shipping_info){
										$_REQUEST['billing_fname'] = $_REQUEST['shipping_fname'];
										$_REQUEST['billing_address'] = $_REQUEST['shipping_address'];
										$_REQUEST['billing_address2'] = $_REQUEST['shipping_address2'];
										$_REQUEST['billing_city'] = $_REQUEST['shipping_city'];
										$_REQUEST['billing_state'] = $_REQUEST['shipping_state'];
										$_REQUEST['billing_zip'] = $_REQUEST['shipping_zip'];
										$billing_city_state_zip = $shipping_city_state_zip;
										$has_billing_info = true;
									}
									$shipping_same_as_billing = 'yes';
								}
                ?>
				<!--Account & Billing Details-->
				<h3 class="checkout-step step-holder" id="account-details-step">Billing Details</h3>
				<div id="account-details" class="step-holder account-details">
		            
<!-- 						<div class='errorBox' id='billing_errors'></div>  
						<div id='billing_address_verification'></div>
	-->	                   
						<input type="hidden" id="stop_for_no_match_billing" name="stop_for_no_match_billing" value="<?= $_REQUEST['stop_for_no_match_billing']? $_REQUEST['stop_for_no_match_billing']: "yes"?>">
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_fname">First Name: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="billing_fname" id="billing_fname" value="<?=$_REQUEST['billing_fname']?>" type="text" autocomplete="off"/>
							</div>
						</div>
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_lname">Last Name:</label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="billing_lname" id="billing_lname" value="<?=$_REQUEST['billing_lname']?>" type="text" autocomplete="off"/>
							</div>
						</div>
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_company">Company: </label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="billing_company" id="billing_company" value="<?=$_REQUEST['billing_company']?>" type="text" autocomplete="off"/>
							</div>
						</div>
						
						<div class="full-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_address">Address: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<div class="icon place"></div>
											<input class="text with-icon address" name="billing_address" id="billing_address"
												value="<?=$_REQUEST['billing_address']?>" type="text" autocomplete="off" />
							</div>
						</div>
						
						<div class="full-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_address2"></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text optional" name="billing_address2"
													id="billing_address2" 
													value="<?=$_REQUEST['billing_address2']?>" type="text"
                                                    autocomplete="off"/>
							</div>
						</div>
						
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_city">City: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<div class="billing-city-holder holder">
													<input class="text" name="billing_city"  id="billing_city"
														value="<?=$_REQUEST['billing_city']?>" type="text" 
														autocomplete="off"/>
									<div class="inline_error_msg hidden">Enter a billing city</div>
								</div>
							</div>
						</div>
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_state">State: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
							   <div class="shipping-state-holder holder">
								  <?=stateSelect("billing_state", $_REQUEST['billing_state'],'',true,'Please Select:','','','','','','select','','','',"billing_state",'')?>
							    </div>	
							</div>
						</div>
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_zip">Zip Code: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
							  <div class="shipping-zip-holder holder">
								<input class="text" name="billing_zip"  id="billing_zip"
										value="<?=$_REQUEST['billing_zip']?>" type="tel" 
										autocomplete="off"/>
							  </div>			
							</div>
						</div>
						<div class="half-row">
							<div class="label-holder">
								<div class="label-box">
									<label for="billing_home_phone">Phone: <sup class="required-star">*</sup></label>
								</div>
							</div>
							<div class="field-holder checkout">
								<input class="text" name="billing_home_phone" id="billing_home_phone" value="<?=$_REQUEST['billing_home_phone']?>" type="tel" 
								autocomplete="off"/>
							</div>
						</div>	
						
						
                    
						<div id="payment-method" style="overflow:visible;">
							<div class='errorBox' id='payment_errors'></div>
							<div class="payment-row">
								<span class="question-bg">Please select the preferred payment method to use on this order</span>
							</div>
							<div class="payment-row">
								<p>
								<input value="paypal" type="radio" name="payment-type" id="paypal-payment" class="payment-type" onchange="$j('.cc-info-holder').hide();"/>
								<label for="paypal"><img src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" style="margin-right:7px;"><span style="font-size:11px; font-family: Arial, Verdana;">The safer, easier way to pay.</span></label></p>
								<p><input id="crcard" value="crcard" type="radio" name="payment-type" checked="checked" 
								autocomplete="off" class="payment-type"  onchange="$j('.cc-info-holder').show();"/>
								<label for="crcard">Credit Card</label></p>
							</div>
                          <span class='cc-info-holder'>							
							<div class="half-row">
									<div class="label-holder">
										<div class="label-box">
											<label for="cc_name">Name on card: <sup class="required-star">*</sup></label>
										</div>
									</div>
									<div class="field-holder checkout">
										<input class="text" id="cc_name" name="cc_name" type="text" value="<?=$cart3['cc_name']?>" 
										autocomplete="off"/>
									</div>
								</div>
								
								<div class="full-row">
									<div class="label-holder">
										<div class="label-box">
											<label for="cardnumber">Card Number: <sup
												class="required-star">*</sup></label>
										</div>
									</div>
									<div class="field-holder checkout">
										<input class="text" name="cc_number" id="cc_number"
											type="tel" 
											onblur="hide_cc_number(this);"
											onfocus="this.value = '';$j('#cc_number').data('the-number','');" maxlength=""
											autocomplete="off"/>
									</div>
								</div>
								<div class="half-row">
									<div class="label-holder">
										<div class="label-box">
											<label for="expdate">Expiration Date: <sup class="required-star">*</sup></label>
										</div>
									</div>
									<div class="field-holder checkout">
										<?php build_expiration_month_dropdown();?>
										<?php build_expiration_year_dropdown();?>
									</div>
								</div>
								
								<div class="half-row">
									<div class="label-holder">
										<div class="label-box">
											<label for="ccv">CCV: <sup class="required-star">*</sup></label>
										</div>
									</div>
									<div class="field-holder checkout">
										<input class="text small" name="cc_ccv" id="cc_ccv" type="tel" maxlength="4" autocomplete="off"/>
						<!-- 	<a class="ccv_tooltip" href="javascript:void(0);">
											<div class="info-tooltip">
												<span>?</span>
												<div class="info-tooltip-content">
														<img alt="" src="/images/ccv_image.gif">
					    						</div>
											</div>
										</a>-->
									</div>
								</div>
								</span>
							</div>
						</div>
						
						
						<div class="step-bottom">
							
						</div>
				</div>
				<div class="checkout-col col3">
				<!--Confirm order-->
				<div class="step-header" id="confirm-order-step-header">
				<h3 class="checkout-step" id="confirm-order-step">Review Your Order</h3></div>
				<div id="confirm-order" class=" paypal-return-step">
					<div class="review-order-msg">Please review your order on the page and click Place Order to place your order.</div>
					<div class="sign-up-holder">
							<?php 
							/*if($checkout->paypal_checkout){ 
								echo $confirm_info['sign_in'];
							}*/
							?>
						</div>
						<div class="products-list">
							<div class="products-table">
							<?php echo $cart_items_display;?>
							</div>
						</div>
						<div class="cart-cost-summary step-holder" >
							<?php  
							echo $checkout->create_cost_breakdown_table(); //calculates the costs for the order
							?>
						</div>
						<div class="total-cost">
						
							<?php 
								if($checkout->paypal_checkout){
									echo $confirm_info['cost_table'];
								}
							?>
						</div>
						<div class="step-bottom confirm" id="confirm-order-step-bottom">
							<div class="btn-holder">
								<a class="button-brown" id="place_order_button" href="#" onclick="gtag('event', 'Confirm Order', {'event_category': 'checkout', 'value': 'Confirm Order'});place_order_submit_open_steps(); return false;">Place Order</a>
								<a class="edit-cart" href="<?=$CFG->baseurl . 'cart.php'?>" onclick="gtag('event', 'Edit cart', {'event_category': 'checkout', 'value': 'Edit cart'});">Edit cart</a>
							</div>
						</div>
				</div>
			</div>
			</form>
			</div>
			</div>
			
			<?php 
			}
			else
			{ 
				echo "There are no items in your cart.";
				echo '<br><a class="continue_shopping_link" href="'.make_continue_shopping_link().'">Continue Shopping</a>';
			}
			?>
			
		</div>
	</div>
<?
include( $CFG->redesign_dirroot . '/includes/footer.php');

?>

