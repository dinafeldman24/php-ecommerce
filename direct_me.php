<?php
include_once( 'application.php' );

if( !$_REQUEST['url_string'] )
    $_REQUEST['url_string'] = $_SERVER['SCRIPT_URL'];

if ($_REQUEST['list_style'])
{
	$new_url = trim($CFG->baseurl . preg_replace("/([?&])list_style=".$_REQUEST['list_style']."([&]?)"."/","?",substr($_SERVER['REQUEST_URI'],1)), "?");
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: $new_url");
	 exit();
}
if( !$_REQUEST['url_string'] )
{
	//404
	include('404.php');
	exit();
	//die('need 404 page setup.');
}
$url_name = $_REQUEST['url_string'];
unset($_REQUEST['url_string'],$_GET['url_string'],$_POST['url_string']);
$url_name = mysql_real_escape_string($url_name); 
//mail("dina.websites@gmail.com", 'direct me', var_export($url_name,true));

//Going to take the url_string that is passed to this page and check for categories and then products.
//Since there are no ID numbers anywhere and it is all just text, I can't easily tell which is which
$cat = Cats::get1ByURLName( $url_name );

global $is_category;
global $is_product;
global $is_brand;
global $is_filter;

$is_category = $is_product = $is_brand = $is_filter=false;

if( $cat )
{
    $is_category = true;

    $_SESSION['parent_cat_id'] = $cat['id'];

    $_REQUEST['cat_name'] = $url_name;

    $actual_link = Catalog::makeCatLink( $cat );
    
    if($cat['is_hidden']=='Y')
    {
        $actual_link = $CFG->baseurl;
    }
    //print_ar( $actual_link );

    if( $actual_link != $_SERVER['SCRIPT_URI'] )
    {
     $_SESSION['prev_prod_page_link'] = $actual_link;	
	 header("HTTP/1.1 301 Moved Permanently");
	 header("Location: $actual_link");
	 exit();
    } 
    $_SESSION['prev_prod_page_link'] = $actual_link;
    include( 'cat_info.php');
    exit();
}

$my_brand = Brands::get1ByURLName( $url_name );

if( $my_brand )
{
    $is_brand = true;    

    $actual_link = Catalog::makeBrandLinkNew( $my_brand['id'] );

    if($my_brand['is_active']=='N')
    {
        $actual_link = $CFG->baseurl;
    }
    //print_ar( $actual_link );

    if( $actual_link != $_SERVER['SCRIPT_URI'] )
    {
     $_SESSION['prev_prod_page_link'] = $actual_link;	
	 header("HTTP/1.1 301 Moved Permanently");
	 header("Location: $actual_link");
	 exit();
    } 
    $_SESSION['prev_prod_page_link'] = $actual_link;
    include( 'searchbrand.php');
    exit();
}

$product = Products::get1ByURLName( $url_name );

if( $product && $product['is_accessory'] != 'Y' )
{
    $is_product = true;

    $actual_link = Catalog::makeProductLink_($product);

    if( $actual_link != $_SERVER['SCRIPT_URI'] )
    {
     $_SESSION['prev_prod_page_link'] = $actual_link;	
	 header("HTTP/1.1 301 Moved Permanently");
	 header("Location: $actual_link");
	 exit();
    }
	$_SESSION['prev_prod_page_link'] = $actual_link;
    include('product_info.php');
    exit();
}

$content = Content::getByPageId($url_name);
if( $content )
{
	$page_id = $content['page_id'];
	$actual_link = Catalog::makeContentLink($page_id);

    if( $actual_link != $_SERVER['SCRIPT_URI'] )
    {     	
	 header("HTTP/1.1 301 Moved Permanently");
	 header("Location: $actual_link");
	 exit();
    }
    
    include('content.php');
    exit;
}

global $filter;
$brand=$_REQUEST['manufacturer'];
  if ($brand){
	  switch ($brand){
		  case  66 : $value='Anchor Hocking'; break;
		  case  53 : $value='Jay Imports'; break;
		  case  57 : $value='Koyal Wholesale'; break;		  
		  case  65 : $value='Old Dutch'; break;
		  case  64 : $value='Paderno by World Cuisine'; break;
		  case  63 : $value='Service Ideas'; break;
		  case  62 : $value='Ten Strawberry Street'; break;		  
		  case  61 : $value='Tabletop Classics'; break;		  
		  case  60 : $value='Ten Strawberry Street'; break;		  
		  case  59 : $value='Town Equipment'; break;		  
		  case  58 : $value='All'; break;		  
	  }
	  $url_name='brand';
  }  
$quantity=$_REQUEST['set'];
   if ($quantity){
	  switch  ($quantity){
		  case  92:	$value='Set of 12'; break;
		  case  87:	$value='Set of 16'; break;
          case  84:	$value='Set of 18'; break;
          case  86:	$value='Set of 20'; break;
          case  91:	$value='Set of 24'; break;
          case  85:	$value='Set of 36'; break;
          case  89:	$value='Set of 4' ; break;
          case  90:	$value='Set of 6' ; break;
		  case  88:	$value='Set of 8' ; break;
	  }
      $url_name='quantity';
   }	  
$material=$_REQUEST['material'];
   if ($material){
	   switch ($material) {
		  case 80:	$value='bamboo/safari'; break;
          case 78:	$value='faux leather' ; break;
          case 82:	$value='faux wood'    ; break;
          case 77:	$value='glass'        ; break;
          case 74:	$value='gold'         ; break;
          case 67:	$value='lacquered'    ; break;
          case 81:	$value='Mahogany'     ; break;
          case 68:	$value='melamine'     ; break;
          case 70:	$value='metal'        ; break;
          case 79:	$value='mirrored'     ; break;
          case 83:	$value='mosaic'       ; break;
          case 69: 	$value='acrylic'      ; break;
          case 76:	$value='Platinum'     ; break;
          case 75:	$value='rattan'       ; break;
          case 73:	$value='silver'       ; break;
          case 71:	$value='steel'        ; break;
	   }
	   $url_name='material';
   }
$shape=$_REQUEST['shape'];
   if ($shape){
	   switch ($shape){
		 case 22: $value='octagon'; break;
         case 24: $value='round'  ; break;
         case 23: $value='square' ; break;
         case 72: $value='round'  ; break;
	   }
	   $url_name='shape';
   }   
$color=$_REQUEST['color'];
    if ($color){
		switch ($color){
		  case 51: $value='blue'    ; break;
          case 94: $value='bronze'  ; break;
          case 48: $value='brown'   ; break;
          case 43: $value='clear'   ; break;
          case 39: $value='copper'  ; break;
          case 93: $value='cream'   ; break;
          case 42: $value='glass'   ; break;
          case 47: $value='gray'    ; break;
          case 35: $value='green'   ; break;
          case 45: $value='mirror'  ; break;
          case 34: $value='orange'  ; break;
          case 46: $value='other'   ; break;
          case 26: $value='pink'    ; break;
          case 36: $value='platinum'; break;
          case 50: $value='purple'  ; break;
          case 38: $value='steel'   ; break;
          case 33: $value='yellow'  ; break;
          case 3 : $value='gold'    ; break;
          case 52: $value='silver'  ; break;
          case 49: $value='black'   ; break;
          case 44: $value='white'   ; break;
          case 41: $value='red'     ; break;
		}
		$url_name='color';
	}
  $per_page=($_REQUEST['limit']=='all')? '' : $_REQUEST['limit'];
  $sortby=$_REQUEST['order'];

 
$filter=Filters::get1ByName($url_name);
if ($filter){
	$is_filter=true;
	$filter_id=$filter['id'];
	$actual_link=Catalog::makeFilterLink($filter_id);
	
	if ($value)
	{   $actual_link .='?value='.$value;
	    if ($per_page)
		    $actual_link .='&per_page='.$per_page;
	    if ($sortby)
		    $actual_link .='&sortby='.$sortby;
	}

	if( $actual_link != $_SERVER['SCRIPT_URI'] )
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $actual_link");
		exit();
	}
	
	include('filter_info.php');
	exit;
	
}
$old_mapping = Array(
'/jay-imports-beaded-antique-gold-charger-plate-14-d.html'=>'/jay-imports-beaded-antique-gold-charger-plate-14d.html',
'/jay-imports-beaded-antique-silver-charger-plate-14-d.html'=>'/jay-imports-beaded-antique-silver-charger-plate-14d.html',
'/jay-imports-aristocrat-antique-gold-charger-plate-14-d.html'=>'/jay-imports-aristocrat-antique-gold-charger-plate-14d.html',
'/jay-imports-aristocrat-antique-silver-charger-plate-14-d.html'=>'/jay-imports-aristocrat-antique-silver-charger-plate-14d.html',
'/jay-imports-woven-cool-gray-charger-plate-13-d.html'=>'/jay-imports-woven-cool-gray-charger-plate-13d.html',
'/jay-imports-textured-ash-gray-charger-plate-13-d.html'=>'/jay-imports-textured-ash-gray-charger-plate-13d.html',
'/jay-imports-aristocrat-silver-emboss-charger-plate-14-d.html'=>'/jay-imports-aristocrat-silver-emboss-charger-plate-14d.html',
'/jay-imports-aristocrat-gold-emboss-charger-plate-14-d.html'=>'/jay-imports-aristocrat-gold-emboss-charger-plate-14d.html',
'/jay-imports-gold-ancient-square-charger-plate-13-d.html'=>'/jay-imports-gold-ancient-square-charger-plate-13d.html',
'/jay-imports-white-diamond-charger-plate-13-d.html'=>'/jay-imports-white-diamond-charger-plate-13d.html',
'/jay-imports-black-diamond-charger-plate-13-d.html'=>'/jay-imports-black-diamond-charger-plate-13d.html',
'/jay-imports-silver-ancient-square-charger-13-d.html'=>'/jay-imports-silver-ancient-square-charger-13d.html',
'/jay-imports-aristocrat-brown-emboss-charger-plate-14-d.html'=>'/jay-imports-aristocrat-brown-emboss-charger-plate-14d.html',
'/jay-imports-beaded-antique-brown-charger-plate-14-d.html'=>'/jay-imports-beaded-antique-brown-charger-plate-14d.html',
'/jay-imports-arizona-blue-charger-plate-13-d.html'=>'/jay-imports-arizona-blue-charger-plate-13d.html',
'/jay-imports-arizona-gray-charger-plate-13-d.html'=>'/jay-imports-arizona-gray-charger-plate-13d.html',
'/jay-imports-arizona-mulberry-charger-plate-13-d.html'=>'/jay-imports-arizona-mulberry-charger-plate-13d.html',
'/jay-imports-spiro-gold-charger-plate-12-5-d.html'=>'/jay-imports-spiro-gold-charger-plate-125d.html',
'/jay-imports-spiro-silver-charger-plate-12-5-d.html'=>'/jay-imports-spiro-silver-charger-plate-125d.html',
'/jay-imports-halley-gold-charger-plate-12-5-d.html'=>'/jay-imports-halley-gold-charger-plate-125d.html',
'/jay-imports-halley-silver-charger-plate-12-5-d.html'=>'/jay-imports-halley-silver-charger-plate-125d.html',
'/jay-imports-roberta-pearl-luster-charger-plate-13-d.html'=>'/jay-imports-roberta-pearl-luster-charger-plate-13d.html',
'/jay-imports-arizona-gold-charger-plate-13-d.html'=>'/jay-imports-arizona-gold-charger-plate-13d.html',
'/jay-imports-deniz-gold-flower-charger-plate-12-d.html'=>'/jay-imports-deniz-gold-flower-charger-plate-12d.html',
'/jay-imports-deniz-silver-flower-charger-plate-12-d.html'=>'/jay-imports-deniz-silver-flower-charger-plate-12d.html',
'/jay-imports-infinity-gold-charger-plate-12-d.html'=>'/jay-imports-infinity-gold-charger-plate-12d.html',
'/jay-imports-infinity-silver-charger-plate-12-d.html'=>'/jay-imports-infinity-silver-charger-plate-12d.html',
'/jay-imports-glamour-gold-charger-plate-13-d.html'=>'/jay-imports-glamour-gold-charger-plate-13d.html',
'/jay-imports-glamour-silver-charger-plate-13-d.html'=>'/jay-imports-glamour-silver-charger-plate-13d.html',
'/jay-imports-sunray-turquoise-charger-plate-13-d.html'=>'/jay-imports-sunray-turquoise-charger-plate-13d.html',
'/jay-imports-sunray-pearl-white-charger-plate-13-d.html'=>'/jay-imports-sunray-pearl-white-charger-plate-13d.html',
'/jay-imports-sunray-cobalt-charger-plate-13-d.html'=>'/jay-imports-sunray-cobalt-charger-plate-13d.html',
'/jay-imports-sunray-silver-charger-plate-13-d.html'=>'/jay-imports-sunray-silver-charger-plate-13d.html',
'/jay-imports-reef-gold-black-charger-plate-13-5-d.html'=>'/jay-imports-reef-gold-black-charger-plate-135d.html',
'/jay-imports-reef-silver-black-charger-plate-13-5-d.html'=>'/jay-imports-reef-silver-black-charger-plate-135d.html',
'/jay-imports-sunray-gold-charger-plate-13-d.html'=>'/jay-imports-sunray-gold-charger-plate-13d.html',
'/jay-imports-reef-pink-charger-plate-13-5-d.html'=>'/jay-imports-reef-pink-charger-plate-135d.html',
'/jay-imports-reef-red-charger-plate-13-5-d.html'=>'/jay-imports-reef-red-charger-plate-135d.html',
'/jay-imports-reef-turquoise-charger-plate-13-5-d.html'=>'/jay-imports-reef-turquoise-charger-plate-135d.html',
'/jay-imports-reef-white-pearl-charger-plate-13-5-d.html'=>'/jay-imports-reef-white-pearl-charger-plate-135d.html',
'/jay-imports-arizona-clear-gold-charger-plate-13-d.html'=>'/jay-imports-arizona-clear-gold-charger-plate-13d.html',
'/jay-imports-arizona-clear-silver-charger-plate-13-d.html'=>'/jay-imports-arizona-clear-silver-charger-plate-13d.html',
'/12-75-inch-bridal-metal-square-charger-plate-set-of-12.html'=>'/metal-bridal-square-charger-plate.html',
'/12-75-inch-gold-glitter-stars-round-charger-plate-set-of-24.html'=>'/gold-glitter-acrylic-charger-plate.html',
'/12-75-inch-silver-glitter-stars-round-charger-plate-set-of-24.html'=>'/silver-glitter-acrylic-charger-plate.html',
'/12-75-inch-blue-glitter-round-charger-plate-set-of-24.html'=>'/blue-glitter-acrylic-charger-plate.html',
'/sunray-gold-charger-plate.html'=>'/sunray-gold-acrylic-charger-plate.html',
'/sunray-silver-charger-plate.html'=>'/sunray-silver-acrylic-charger-plate.html',
'/plaid-gold-charger-plate.html'=>'/gold-plaid-acrylic-charger-plate.html',
'/plaid-silver-charger-plate.html'=>'/silver-plaid-acrylic-charger-plate.html',
'/royal-red-charger-plate-embossed.html'=>'/royal-red-antique-acrylic-charger-plate.html',
'/royal-antiqued-cream-charger-plate-embossed.html'=>'/royal-antique-cream-acrylic-charger-plate.html',
'/13-inch-gold-ruffled-melamine-round-charger-plate-set-of-24.html'=>'/gold-ruffled-lacquered-charger-plate.html',
'/13-inch-silver-ruffled-melamine-round-charger-plate-set-of-24.html'=>'/silver-ruffled-lacquered-charger-plate.html',
'/13-inch-purple-ruffled-melamine-round-charger-plate-set-of-24.html'=>'/purple-ruffled-lacquered-charger-plate.html',
'/13-inch-silver-saturn-round-charger-plate-set-of-24.html'=>'/saturn-silver-lacquered-charger-plate.html',
'/giraffe-round-charger-plate.html'=>'/giraffe-acrylic-charger-plate.html',
'/13-inch-faux-brown-pine-wood-round-charger-plate-set-of-24.html'=>'/brown-pine-faux-wood-round-charger.html',
'/13-inch-faux-gray-pine-wood-round-charger-plate-set-of-24.html'=>'/gray-pine-faux-wood-round-charger.html',
'/zebra-round-charger-plate.html'=>'/zebra-acrylic-charger-plate.html',
'/black-round-melamine-charger.html'=>'/black-round-melamine-charger-plate.html',
'/13-inch-metallic-purple-round-charger-plate-set-of-24.html'=>'/metallic-purple-round-acrylic-charger-plate.html',
'/aristocrat-red-charger-plate.html'=>'/aristocrat-red-oak-acrylic-charger.html',
'/aristocrat-gold-charger-plate.html'=>'/aristocrat-golden-oak-acrylic-charger.html',
'/aristocrat-brown-charger-plate.html'=>'/aristocrat-brown-oak-acrylic-charger.html',
'/13-inch-mirror-glass-scalloped-round-charger-plate-set-of-6.html'=>'/scalloped-edge-mirror-round-charger-plate.html',
'/mirror-charger-square.html'=>'/mirror-square-charger-plate.html',
'/gem-cut-mirror-charger-plate.html'=>'/large-gem-cut-mirror-charger-plate.html',
'/pebble-mirror-charger-plate.html'=>'/pebble-beaded-mirror-round-charger-plate.html',
'/mirror-charger-square.html'=>'/mirror-square-deco-charger-plate.html',
'/glass-mirror-beveled-block-charger.html'=>'/beveled-block-mirror-round-charger-plate.html',
'/12-inch-gold-mosaic-square-charger-plate-set-of-24.html'=>'/gold-mosaic-square-acrylic-charger-plate.html',
'/12-inch-silver-mosaic-square-charger-plate-set-of-24.html'=>'/silver-mosaic-square-acrylic-charger-plate.html',
'/12-inch-gold-mosaic-round-charger-plate-set-of-24.html'=>'/gold-mosaic-round-acrylic-charger-plate.html',
'/12-inch-silver-mosaic-round-charger-plate-set-of-24.html'=>'/silver-mosaic-round-acrylic-charger-plate.html',
'/pamuk-gold-charger-plate.html'=>'/pamuk-gold-glass-charger-plate.html',
'/13-inch-gold-reflex-glass-round-charger-plate-set-of-8.html'=>'/reflex-gold-glass-charger-plate.html',
'/13-inch-silver-reflex-glass-round-charger-plate-set-of-8.html'=>'/reflex-silver-glass-charger-plate.html',
'/12-5-inch-gold-braided-glass-round-charger-plate-set-of-12.html'=>'/braided-gold-glitter-glass-charger-plate.html',
'/12-5-inch-silver-braided-glass-round-charger-plate-set-of-12.html'=>'/braided-silver-glitter-glass-charger-plate.html',
'/12-5-inch-silver-glitter-circus-round-charger-plate-set-of-12.html'=>'/circus-silver-glass-charger-plate.html',
'/12-5-inch-gold-glitter-circus-round-charger-plate-set-of-12.html'=>'/circus-gold-glass-charger-plate.html',
'/circus-white-w-silver-charger-round.html'=>'/circus-white-glass-charger-plate.html',
'/reef-silver-charger-plate.html'=>'/glass-reef-silver-charger-plate.html',
'/circus-silver-border-charger-round.html'=>'/circus-silver-border-glass-charger.html',
'/circus-gold-border-charger-round.html'=>'/circus-gold-border-glass-charger.html',
'/charger-gold-color-painted.html'=>'/gold-glitter-glass-charger-plate.html',
'/charger-silver-color-with-glitter.html'=>'/silver-glitter-glass-charger-plate.html',
'/13-inch-dark-woven-rattan-square-charger-plate-set-of-8.html'=>'/brick-brown-square-rattan-charger.html',
'/13-inch-dark-woven-rattan-round-charger-plate-set-of-8.html'=>'/brick-brown-round-rattan-charger.html',
'/round-rattan-honey-charger-plate.html'=>'/honey-round-rattan-charger.html',
'/harvest-rattan-charger-plateround.html'=>'/harvest-amber-round-rattan-charger.html',
'/13-inch-gold-rimmed-clear-round-charger-plate-set-of.html'=>'/gold-rimmed-clear-glass-charger-plate.html',
'/13-inch-silver-beaded-clear-glass-round-charger-plate-set-of-4.html'=>'/silver-beaded-clear-glass-charger-plate.html',
'/13-inch-gold-beaded-clear-glass-round-charger-plate-set-of-4.html'=>'/gold-beaded-clear-glass-charger-plate.html',
'/luster-amber-charger.html'=>'/luster-amber-glass-charger-plate.html',
'/light-gold-antique-charger.html'=>'/glass-gold-burst-charger.html',
'/light-silver-antique-charger.html'=>'/glass-silver-burst-charger.html',
'/13-inch-sunray-rim-clear-glass-round-charger-plate-set-of-4.html'=>'/sunray-clear-glass-charger-plate.html',
'/13-inch-beaded-clear-glass-round-charger-plate-set-of-4.html'=>'/clear-beaded-glass-charger-plate.html',
'/roma-charger-plate-silver.html'=>'/roma-glass-charger-plate-silver.html',
'/alinea-charger-plate-gold.html'=>'/alinea-glass-charger-plate-gold.html',
'/alinea-charger-plate-silver.html'=>'/alinea-glass-charger-plate-silver.html',
'/vanessa-charger-plate-gold.html'=>'/vanessa-glass-charger-plate-gold.html',
'/vanessa-charger-plate-silver.html'=>'/vanessa-glass-charger-plate-silver.html',
'/divine-charger-plate-gold.html'=>'/divine-glass-charger-plate-gold.html',
'/divine-charger-plate-silver.html'=>'/divine-glass-charger-plate-silver.html',
'/pearl-charger-plate-silver.html'=>'/pearl-glass-charger-plate-silver.html',
'/pearl-charger-plate-gold.html'=>'/pearl-glass-charger-plate-gold.html',
'/pearl-spiral-charger-round.html'=>'/pearl-spiral-silver-glass-charger-plate.html',
'/gold-spiral-charger-round.html'=>'/pearl-spiral-gold-glass-charger.html',
'/13-inch-gold-rounded-square-charger-plate-set-of-12.html'=>'/gold-rounded-square-glass-charger-plate.html',
'/13-inch-silver-rounded-square-charger-plate-set-of-12.html'=>'/silver-rounded-square-glass-charger-plate.html',
'/baroque-silver-gold-charger-round.html'=>'/baroque-scroll-silver-gold-glass-charger.html',
'/platinum-rimmed-glass-round-charger-plate-set-of-8.html'=>'/platinum-rimmed-clear-glass-charger-plate.html',
'/black-brocade-rimmed-round-white-charger-plate-set-of-24.html'=>'/black-brocade-rimmed-white-lacquered-charger-plate.html',
'/platinum-rope-charger-plate.html'=>'/braided-platinum-rope-lacquered-charger-plate.html',
'/13-inch-black-melamine-round-regency-charger-plate-set-of-24.html'=>'/regency-black-melamine-charger-plate.html',
'/13-inch-gold-melamine-round-regency-charger-plate-set-of-24.html'=>'/regency-gold-melamine-charger-plate.html',
'/13-inch-silver-melamine-round-regency-charger-plate-set-of-24.html'=>'/regency-silver-melamine-charger-plate.html',
'/black-baroque-charger.html'=>'/black-baroque-acrylic-charger.html',
'/13-inch-gold-baroque-charger-plate-set-of-24.html'=>'/gold-baroque-acrylic-charger.html',
'/silver-baroque-charger.html'=>'/silver-baroque-acrylic-charger.html',
'/13-inch-gold-leaf-rimmed-round-white-charger-plate-set-of-24.html'=>'/gold-leaf-rimmed-white-lacquered-charger-plate.html',
'/13-inch-silver-leaf-rimmed-round-white-charger-plate-set-of-24.html'=>'/silver-leaf-rimmed-white-lacquered-charger-plate.html',
'/13-inch-jewel-rimmed-round-black-charger-plate-set-of-24.html'=>'/jewel-rimmed-black-lacquered-charger-plate.html',
'/13-inch-jewel-rimmed-round-gold-charger-plate-set-of-24.html'=>'/jewel-rimmed-gold-lacquered-charger-plate.html',
'/13-inch-jewel-rimmed-round-silver-charger-plate-set-of-24.html'=>'/jewel-rimmed-silver-lacquered-charger-plate.html',
'/black-square-charger-plate-set-of-12.html'=>'/black-lacquered-square-charger-plate.html',
'/gold-square-charger-plate.html'=>'/gold-lacquered-square-charger-plate.html',
'/silver-square-charger-plate.html'=>'/silver-lacquered-square-charger-plate.html',
'/coral-charger-plates.html'=>'/beaded-charger-plates--coral.html',
'/ripple-charger-plates-white.html'=>'/ripple-glass-charger-plates---white.html',
'/ripple-charger-plates-red.html'=>'/ripple-glass-charger-plates---red.html',
'/ripple-charger-plates-fuchsia.html'=>'/ripple-glass-charger-plates---fuchsia.html',
'/ripple-charger-plates-pink.html'=>'/ripple-glass-charger-plates---pink.html',
'/ripple-charger-plates-orange.html'=>'/ripple-glass-charger-plates---orange.html',
'/ripple-charger-plates-yellow.html'=>'/ripple-glass-charger-plates---yellow.html',
'/ripple-charger-plates-lime-green.html'=>'/ripple-glass-charger-plates---lime-green.html',
'/ripple-charger-plates-aqua.html'=>'/ripple-glass-charger-plates---aqua.html',
'/ripple-charger-plates-diamond-blue.html'=>'/ripple-glass-charger-plates---diamond-blu.html',
'/ripple-charger-plates-navy-blue.html'=>'/ripple-glass-charger-plates---navy-blue.html',
'/ripple-charger-plates-royal-blue.html'=>'/ripple-glass-charger-plates---royal-blue.html',
'/ripple-charger-plates-lavender.html'=>'/ripple-glass-charger-plates---lavender.html',
'/ripple-charger-plates-plum.html'=>'/ripple-glass-charger-plates---plum.html',
'/ripple-charger-plates-royal-purple.html'=>'/ripple-glass-charger-plates---royal-purple.html',
'/ripple-charger-plates-brown.html'=>'/ripple-glass-charger-plates--brown.html',
'/ripple-charger-plates-black.html'=>'/ripple-glass-charger-plates---black.html',
'/ripple-charger-plates-charcoal-gray.html'=>'/ripple-glass-charger-plates---charcoal-gray.html',
'/ripple-charger-plates-gold.html'=>'/ripple-glass-charger-plates--gold.html',
'/ripple-charger-plates-silver.html'=>'/ripple-glass-charger-plates---silver.html',
'/flora-charger-plates-white.html'=>'/flora-glass-charger-plates---white.html',
'/flora-charger-plates-red.html'=>'/flora-glass-charger-plates---red.html',
'/flora-charger-plates-fuchsia.html'=>'/flora-glass-charger-plates---fuchsia.html',
'/flora-charger-plates-pink.html'=>'/flora-glass-charger-plates---pink.html',
'/flora-charger-plates-orange.html'=>'/flora-glass-charger-plates---orange.html',
'/flora-charger-plates-yellow.html'=>'/flora-glass-charger-plates---yellow.html',
'/flora-charger-plates-lime-green.html'=>'/flora-glass-charger-plates---lime-green.html',
'/flora-charger-plates-aqua.html'=>'/flora-glass-charger-plates---aqua.html',
'/flora-charger-plates-diamond-blue.html'=>'/flora-glass-charger-plates---diamond-blue.html',
'/flora-charger-plates-navy-blue.html'=>'/flora-glass-charger-plates---navy-blue.html',
'/flora-charger-plates-royal-blue.html'=>'/flora-glass-charger-plates---royal-blue.html',
'/flora-charger-plates-lavender.html'=>'/flora-glass-charger-plates---lavender.html',
'/flora-charger-plates-plum.html'=>'/flora-glass-charger-plates---plum.html',
'/flora-charger-plates-royal-purple.html'=>'/flora-glass-charger-plates---royal-purple.html',
'/flora-charger-plates-brown.html'=>'/flora-glass-charger-plates---brown.html',
'/flora-charger-plates-black.html'=>'/flora-glass-charger-plates---black.html',
'/flora-charger-plates-charcoal-gray.html'=>'/flora-glass-charger-plates---charcoal-gray.html',
'/flora-charger-plates-gold.html'=>'/flora-glass-charger-plates---gold.html',
'/flora-charger-plates-silver.html'=>'/flora-glass-charger-plates---silver.html',
'/round-rattan-charger-plate-honey-brown.html'=>'/round-rattan-charger-plate---honey-brown.html',
'/round-rattan-charger-plate-dark-brown.html'=>'/round-rattan-charger-plate---dark-brown.html',
'/13-inch-beaded-round-lime-green-charger-plates-set-of-24.html'=>'/lime-green-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-forest-green-charger-plates-set-of-24.html'=>'/forest-green-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-olive-green-charger-plates-set-of-24.html'=>'/olive-green-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-orange-charger-plates-set-of-24.html'=>'/orange-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-yellow-charger-plates-set-of-24.html'=>'/yellow-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-peacock-blue-charger-plates-set-of-24.html'=>'/peacock-blue-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-royal-purple-charger-plates-set-of-24.html'=>'/royal-purple-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-lavender-charger-plates-set-of-24.html'=>'/lavender-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-orchid-charger-plates-set-of-24.html'=>'/orchid-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-royal-blue-charger-plates-set-of-24.html'=>'/royal-blue-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-ocean-blue-charger-plates-set-of-24.html'=>'/ocean-blue-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-turquoise-blue-charger-plates-set-of-24.html'=>'/turquoise-blue-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-fuchsia-charger-plates-set-of-24.html'=>'/fuchsia-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-light-pink-charger-plates-set-of-24.html'=>'/light-pink-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-red-charger-plates-set-of-24.html'=>'/red-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-burgundy-charger-plates-set-of-24.html'=>'/burgundy-beaded-acrylic-charger-plates.html',
'/13-inch-beaded-round-black-charger-plates-set-of-24.html'=>'/black-beaded-acrylic-charger-plates.html',
'/beaded-round-gold-acrylic-charger-plates-set-of-24.html'=>'/gold-acrylic-beaded-round-charger-plates-4-pack.html',
'/gold-acrylic-round-charger-plates-set-of-24.html'=>'/gold-acrylic-round-charger-plates-4-pack.html',
'/beaded-round-silver-acrylic-charger-plates-set-of-24.html'=>'/silver-acrylic-beaded-round-charger-plates-4-pack.html',
'/antique-embossed-victoria-charger-plate.html'=>'/antique-embossed-victoria-steel-charger-plate.html',
'/versailles-charger-plate.html'=>'/versailles-iron-charger-plate.html',
'/antique-embossed-heritage-charger-plate.html'=>'/antique-embossed-heritage-copper-charger-plate.html',
'/melamine-crocodile-square-charger-plate-black.html'=>'/crocodile-black-melamine-square-charger-plate.html',
'/bell-tip-gold-charger-plate.html'=>'/gold-bell-tip-clear-glass-charger-plate.html',
'/bell-tip-silver-charger-plate.html'=>'/silver-bell-tip-clear-glass-charger-plate.html',
'/angel-glass-rim-chargerset-of-12.html'=>'/angel-white-glass-rim-charger.html',
'/antique-copper-13-charger-plate-set-of-6.html'=>'/metallic-copper-glass-charger-plate.html',
'/aztec-black-silver-13-charger-plate-set-of-6.html'=>'/aztec-black-and-silver-glass-charger-plate.html',
'/aztec-copper-charger-plate.html'=>'/aztec-copper-glass-charger-plate.html',
'/belmont-clear-13-beaded-charger-plate-set-of-4.html'=>'/belmont-clear-beaded-glass-charger-plate.html',
'/belmont-gold-13-beaded-charger-plate-set-of-4.html'=>'/belmont-gold-beaded-clear-glass-charger-plate.html',
'/metallic-glass-13-brown-charger-plate-set-of-4.html'=>'/metallic-brown-glass-charger-plate.html',
'/belmont-silver-13-beaded-charger-plate-set-of-4.html'=>'/belmont-silver-beaded-clear-glass-charger-plate.html',
'/botanica-glass-chargerset-of-6.html'=>'/botanica-purple-glass-charger-plate.html',
'/bubble-clear-glass-chargerset-of-12.html'=>'/bubble-clear-glass-charger-plate.html',
'/colored-black-rimmed-charger.html'=>'/colored-black-rimmed-glass-charger.html',
'/colored-gold-rimmed-charger.html'=>'/colored-gold-rimmed-glass-charger.html',
'/colored-red-rimmed-charger.html'=>'/colored-red-rimmed-glass-charger.html',
'/colored-silver-rimmed-charger.html'=>'/colored-silver-rimmed-glass-charger.html',
'/metallic-chargers-13-copper-charger-plate-set-of-4.html'=>'/metallic-copper-glass-charger-plate.html',
'/cyclone-beige-and-gold-13-charger-plate-set-of-6.html'=>'/cyclone-beige-and-gold-glass-charger-plate.html',
'/cyclone-red-and-gold-13-charger-plate-set-of-6.html'=>'/cyclone-red-and-gold-glass-charger-plate.html',
'/diamonte-glass-chargerset-of-6.html'=>'/diamonte-gold-glass-charger-plate.html',
'/giardano-gold-glass-chargerset-of-6.html'=>'/giardano-gold-glass-charger-plate.html',
'/lacquer-black-square-charger-plate.html'=>'/lacquer-square-black-charger-plate.html',
'/lacquer-gold-square-charger-plate.html'=>'/lacquer-square-gold-charger-plate.html',
'/lacquer-gold-plain-charger-plate.html'=>'/lacquer-plain-gold-charger-plate.html',
'/lacquer-green-square-charger-plate.html'=>'/lacquer-square-green-charger-plate.html',
'/lacquer-red-square-charger-plate.html'=>'/lacquer-square-red-charger-plate.html',
'/lacquer-silver-square-charger-plate.html'=>'/lacquer-square-silver-charger-plate.html',
'/lacquer-beaded-silver-charger-plate.html'=>'/lacquer-silver-beaded-charger-plate.html',
'/luster-13-charger-plate-set-of-6.html'=>'/luster-clear-glass-charger-plate.html',
'/monaco-beige-white-glass-chargerset-of-6.html'=>'/monaco-beige-and-gold-glass-charger-plate.html',
'/monaco-silver-and-gold-charger-plate.html'=>'/monaco-silver-and-gold-glass-charger-plate.html',
'/saturn-gold-matte-13-charger-plate-set-of-4.html'=>'/saturn-gold-matte-glass-charger-plate.html',
'/reef-black-charger.html'=>'/black-reef-glass-charger-plate.html',
'/reef-gold-charger.html'=>'/gold-reef-glass-charger-plate.html',
'/reef-red-charger.html'=>'/red-reef-glass-charger-plate.html',
'/reef-silver-charger.html'=>'/silver-reef-glass-charger-plate.html',
'/saturn-silver-matte-13-charger-plate-set-of-4.html'=>'/saturn-silver-matte-glass-charger-plate.html',
'/sunburst-bronze-charger-plate.html'=>'/sunburst-metallic-bronze-glass-charger-plate.html',
'/whistler-glass-etched-chargerset-of-6.html'=>'/whistler-clear-glass-etched-charger-plate.html',
'/13-inch-gold-acrylic-charger-plates-set-of-24.html'=>'/gold-acrylic-round-charger-plates.html',
'/13-inch-round-acrylic-gold-beaded-charger-plate-set-of-24.html'=>'/gold-acrylic-beaded-round-charger-plates.html',
'/square-acrylic-gold-charger-plate.html'=>'/square-acrylic-gold-charger-plate-for-square-plates.html',
'/13-inch-round-acrylic-silver-beaded-charger-plate-set-of-24.html'=>'/silver-acrylic-beaded-round-charger-plates.html',
'/13-inch-round-acrylic-silver-charger-plate-set-of-24.html'=>'/silver-acrylic-round-charger-plates.html',
'/square-acrylic-silver-charger-plate.html'=>'/square-acrylic-silver-charger-plate-for-square-plates.html',
'/about-us' => '/about-us/',
'/about-us/' => '/about-us.html',
'/faq/' => '/faq.html',
'/sample.html' => '/products.html'
);


       $url_redirects_result = UrlRedirects::getAll();
        if (count($url_redirects_result) > 0)
        {
			foreach ($url_redirects_result as $this_result)
			{
				$from_url = $this_result['from_url'];
				$to_url = $this_result['to_url'];
        		$old_mapping[$from_url] = $to_url;
			}
        }

$uri_parts = explode("?",$_SERVER['REQUEST_URI']);
//mail("dina.websites@gmail.com", $uri_parts[0], "");
if ($uri_parts[0] == "/searchbrand.php")
{
	$uri_parts2 = explode("&", $uri_parts[1]);
	$uri_parts[0] .= "?" . $uri_parts2[0];
	$uri_parts[1] = "";
}       
if ($old_mapping[$uri_parts[0]]) {
    $new_url = $CFG->baseurl . substr($old_mapping[$uri_parts[0]],1) .($uri_parts[1]? "?". $uri_parts[1]:"");
    $_SESSION['prev_prod_page_link'] = $new_url;
    header("HTTP/1.1 301 Moved Permanently");    
    header("Location: $new_url");
    exit;
}
//echo "a";
include('404.php');

// die( '404 page!' );

?>
